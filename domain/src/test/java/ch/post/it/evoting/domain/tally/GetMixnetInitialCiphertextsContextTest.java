/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.tally;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;

@DisplayName("A GetMixnetInitialCiphertextsContext built with")
class GetMixnetInitialCiphertextsContextTest {

	private static final Random RANDOM_SERVICE = RandomFactory.createRandom();
	private GqGroup encryptionGroup;
	private String electionEventId;
	private String ballotBoxId;

	@BeforeEach
	void setUp() {
		encryptionGroup = GroupTestData.getGqGroup();
		electionEventId = RANDOM_SERVICE.genRandomBase16String(32).toLowerCase();
		ballotBoxId = RANDOM_SERVICE.genRandomBase16String(32).toLowerCase();
	}

	@Test
	@DisplayName("null parameters throws NullPointerException.")
	void nullParametersThrows() {
		assertThrows(NullPointerException.class, () -> new GetMixnetInitialCiphertextsContext(null, electionEventId, ballotBoxId));
		assertThrows(NullPointerException.class, () -> new GetMixnetInitialCiphertextsContext(encryptionGroup, null, ballotBoxId));
		assertThrows(NullPointerException.class, () -> new GetMixnetInitialCiphertextsContext(encryptionGroup, electionEventId, null));
	}

	@Test
	@DisplayName("invalid UUIDs throws FailedValidationException.")
	void invalidUUIDsThrows() {
		final String invalidUUID = "invalid UUID";
		assertThrows(FailedValidationException.class, () -> new GetMixnetInitialCiphertextsContext(encryptionGroup, invalidUUID, ballotBoxId));
		assertThrows(FailedValidationException.class, () -> new GetMixnetInitialCiphertextsContext(encryptionGroup, electionEventId, invalidUUID));
	}

	@Test
	@DisplayName("valid parameters does not throw.")
	void ValidParametersDoesNotThrow() {
		final GetMixnetInitialCiphertextsContext result = assertDoesNotThrow(
				() -> new GetMixnetInitialCiphertextsContext(encryptionGroup, electionEventId, ballotBoxId));

		assertEquals(encryptionGroup, result.encryptionGroup());
		assertEquals(electionEventId, result.electionEventId());
		assertEquals(ballotBoxId, result.ballotBoxId());
	}
}