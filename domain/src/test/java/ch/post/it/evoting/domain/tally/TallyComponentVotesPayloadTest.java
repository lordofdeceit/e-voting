/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.tally;

import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_ACTUAL_VOTING_OPTION_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.domain.SerializationUtils;

class TallyComponentVotesPayloadTest {

	private static final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
	private static final Random random = RandomFactory.createRandom();
	private static final Hash hash = HashFactory.createHash();
	private static ObjectNode rootNode;
	private static TallyComponentVotesPayload tallyComponentVotesPayload;
	private static String electionEventId;
	private static String ballotId;
	private static String ballotBoxId;
	private static GqGroup gqGroup;
	private static GroupVector<GroupVector<PrimeGqElement, GqGroup>, GqGroup> votes;
	private static List<List<String>> decodedWriteInVotes;

	@BeforeAll
	static void setUpAll() {
		// Create payload.
		electionEventId = random.genRandomBase16String(32).toLowerCase();
		ballotId = random.genRandomBase16String(32).toLowerCase();
		ballotBoxId = random.genRandomBase16String(32).toLowerCase();

		gqGroup = GroupTestData.getLargeGqGroup();

		final int desiredNumberOfPrimes = 3;
		final GroupVector<PrimeGqElement, GqGroup> primeGroupMembers =
				PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers(gqGroup, desiredNumberOfPrimes);
		votes = GroupVector.of(GroupVector.from(primeGroupMembers),
				GroupVector.from(primeGroupMembers));

		final List<List<String>> actualSelectedVotingOptions = votes.stream()
				.map(actualSelectedVotingOption -> actualSelectedVotingOption.stream().map(v -> "124124aa-154153").toList()).toList();

		decodedWriteInVotes = votes.stream()
				.map(actualSelectedVotingOption -> actualSelectedVotingOption.stream().map(v -> "James Bond").toList()).toList();

		final TallyComponentVotesPayload payload = new TallyComponentVotesPayload(electionEventId, ballotId, ballotBoxId, gqGroup, votes,
				actualSelectedVotingOptions, decodedWriteInVotes);
		final byte[] payloadHash = hash.recursiveHash(payload);
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature(payloadHash);
		payload.setSignature(signature);

		tallyComponentVotesPayload = payload;

		// Create expected Json.
		rootNode = objectMapper.createObjectNode();

		final JsonNode encryptionGroupNode = SerializationUtils.createEncryptionGroupNode(gqGroup);
		rootNode.put("electionEventId", electionEventId);
		rootNode.put("ballotId", ballotId);
		rootNode.put("ballotBoxId", ballotBoxId);
		rootNode.set("encryptionGroup", encryptionGroupNode);
		rootNode.set("encryptionGroup", encryptionGroupNode);

		final JsonNode votesNode = SerializationUtils.createVotesNode(votes);
		rootNode.set("votes", votesNode);

		final JsonNode actualSelectedVotingOptionsNode = SerializationUtils.createActualSelectedVotingOptionsNode(actualSelectedVotingOptions);
		rootNode.set("actualSelectedVotingOptions", actualSelectedVotingOptionsNode);

		final JsonNode decodedWriteInVotesNode = SerializationUtils.createDecodedWriteInVotesNode(decodedWriteInVotes);
		rootNode.set("decodedWriteInVotes", decodedWriteInVotesNode);

		final JsonNode signatureNode = SerializationUtils.createSignatureNode(signature);
		rootNode.set("signature", signatureNode);
	}

	@Test
	@DisplayName("serialized gives expected json")
	void serializePayload() throws JsonProcessingException {
		final String serializedPayload = objectMapper.writeValueAsString(tallyComponentVotesPayload);

		assertEquals(rootNode.toString(), serializedPayload);
	}

	@Test
	@DisplayName("deserialized gives expected payload")
	void deserializePayload() throws JsonProcessingException {
		final TallyComponentVotesPayload deserializedPayload = objectMapper.readValue(rootNode.toString(),
				TallyComponentVotesPayload.class);

		assertEquals(tallyComponentVotesPayload, deserializedPayload);
	}

	@Test
	@DisplayName("serialized then deserialized gives original payload")
	void cycle() throws JsonProcessingException {
		final TallyComponentVotesPayload deserializedPayload = objectMapper
				.readValue(objectMapper.writeValueAsString(tallyComponentVotesPayload), TallyComponentVotesPayload.class);

		assertEquals(tallyComponentVotesPayload, deserializedPayload);
	}

	@ParameterizedTest
	@MethodSource("invalidActualVotingOptions")
	@DisplayName("created with invalid actual voting options throws IllegalArgumentException.")
	void invalidActualVotingOptionThrows(final List<List<String>> invalidVotingOptions) {
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new TallyComponentVotesPayload(electionEventId, ballotId, ballotBoxId, gqGroup, votes, invalidVotingOptions,
						decodedWriteInVotes));

		assertEquals(
				String.format("The actual selected voting options must be non-blank strings, valid XML tokens and their length must not exceed %s.",
						MAXIMUM_ACTUAL_VOTING_OPTION_LENGTH), Throwables.getRootCause(exception).getMessage());
	}

	private static Stream<Arguments> invalidActualVotingOptions() {
		final List<List<String>> greaterThanMax = List.of(List.of("valid0", "valid1", "valid2"),
				List.of("123456789101112131415161718192021222324252627282930", "valid4", "valid3"));
		final List<List<String>> blank = List.of(List.of("valid0", "valid1", "valid2"), List.of("     ", "valid4", "valid3"));
		final List<List<String>> invalidXml = List.of(List.of("valid0", "valid1", "valid2"),
				List.of("invalid ", "valid4", "valid3"));

		return Stream.of(
				Arguments.of(greaterThanMax),
				Arguments.of(blank),
				Arguments.of(invalidXml)
		);
	}
}
