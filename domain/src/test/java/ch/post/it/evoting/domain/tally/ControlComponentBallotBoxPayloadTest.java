/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.tally;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Collections;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.PlaintextEqualityProof;
import ch.post.it.evoting.domain.ContextIds;
import ch.post.it.evoting.domain.EncryptedVerifiableVote;
import ch.post.it.evoting.domain.SerializationUtils;

class ControlComponentBallotBoxPayloadTest {

	private static final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
	private static final Random random = RandomFactory.createRandom();
	private static final String electionEventId = random.genRandomBase16String(32).toLowerCase();
	private static final String verificationCardSetId = random.genRandomBase16String(32).toLowerCase();
	private static final String verificationCardId = random.genRandomBase16String(32).toLowerCase();
	private static final String ballotBoxId = random.genRandomBase16String(32).toLowerCase();
	private static final int nodeId = 2;
	private static final Hash hash = HashFactory.createHash();

	private static ObjectNode rootNode;
	private static ControlComponentBallotBoxPayload controlComponentBallotBoxPayload;

	@BeforeAll
	static void setUpAll() {
		final GqGroup gqGroup = GroupTestData.getGqGroup();
		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
		final ZqGroupGenerator zqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(gqGroup));

		// Create payload.
		final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);
		final ElGamalMultiRecipientCiphertext encryptedVote = elGamalGenerator.genRandomCiphertext(2);
		final ElGamalMultiRecipientCiphertext exponentiatedEncryptedVote = elGamalGenerator.genRandomCiphertext(1);
		final ElGamalMultiRecipientCiphertext encryptedPartialChoiceReturnCodes = elGamalGenerator.genRandomCiphertext(2);
		final ExponentiationProof exponentiationProof = new ExponentiationProof(zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementMember());
		final PlaintextEqualityProof plaintextEqualityProof = new PlaintextEqualityProof(zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementVector(2));

		final EncryptedVerifiableVote encryptedVerifiableVote = new EncryptedVerifiableVote(contextIds, encryptedVote, exponentiatedEncryptedVote,
				encryptedPartialChoiceReturnCodes, exponentiationProof, plaintextEqualityProof);

		final ControlComponentBallotBoxPayload payload = new ControlComponentBallotBoxPayload(gqGroup, electionEventId, ballotBoxId, nodeId,
				Collections.singletonList(encryptedVerifiableVote));
		final byte[] payloadHash = hash.recursiveHash(payload);
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature(payloadHash);
		payload.setSignature(signature);

		controlComponentBallotBoxPayload = payload;

		// Create expected Json.
		rootNode = objectMapper.createObjectNode();

		final JsonNode encryptionGroupNode = SerializationUtils.createEncryptionGroupNode(gqGroup);
		rootNode.set("encryptionGroup", encryptionGroupNode);
		rootNode.put("electionEventId", electionEventId);
		rootNode.put("ballotBoxId", ballotBoxId);
		rootNode.put("nodeId", nodeId);

		final ObjectNode contextIdNode = objectMapper.createObjectNode();
		contextIdNode.put("electionEventId", electionEventId);
		contextIdNode.put("verificationCardSetId", verificationCardSetId);
		contextIdNode.put("verificationCardId", verificationCardId);

		final ObjectNode confirmedVoteNode = objectMapper.createObjectNode();
		final ObjectNode encryptedVoteNode = SerializationUtils.createCiphertextNode(encryptedVote);
		final ObjectNode exponentiatedEncryptedVoteNode = SerializationUtils.createCiphertextNode(exponentiatedEncryptedVote);
		final ObjectNode encryptedPartialChoiceReturnCodesNode = SerializationUtils.createCiphertextNode(encryptedPartialChoiceReturnCodes);
		final ObjectNode exponentiationProofNode = SerializationUtils.createExponentiationProofNode(exponentiationProof);
		final ObjectNode plaintextEqualityProofNode = SerializationUtils.createPlaintextEqualityProofNode(plaintextEqualityProof);
		confirmedVoteNode.set("contextIds", contextIdNode);
		confirmedVoteNode.set("encryptedVote", encryptedVoteNode);
		confirmedVoteNode.set("exponentiatedEncryptedVote", exponentiatedEncryptedVoteNode);
		confirmedVoteNode.set("encryptedPartialChoiceReturnCodes", encryptedPartialChoiceReturnCodesNode);
		confirmedVoteNode.set("exponentiationProof", exponentiationProofNode);
		confirmedVoteNode.set("plaintextEqualityProof", plaintextEqualityProofNode);

		final ArrayNode confirmedVotesNode = objectMapper.createArrayNode();
		confirmedVotesNode.add(confirmedVoteNode);
		rootNode.set("confirmedEncryptedVotes", confirmedVotesNode);

		final JsonNode signatureNode = SerializationUtils.createSignatureNode(signature);
		rootNode.set("signature", signatureNode);
	}

	@Test
	@DisplayName("serialized gives expected json")
	void serializePayload() throws JsonProcessingException {
		final String serializedPayload = objectMapper.writeValueAsString(controlComponentBallotBoxPayload);

		assertEquals(rootNode.toString(), serializedPayload);
	}

	@Test
	@DisplayName("deserialized gives expected payload")
	void deserializePayload() throws JsonProcessingException {
		final ControlComponentBallotBoxPayload deserializedPayload = objectMapper.readValue(rootNode.toString(),
				ControlComponentBallotBoxPayload.class);

		assertEquals(controlComponentBallotBoxPayload, deserializedPayload);
	}

	@Test
	@DisplayName("serialized then deserialized gives original payload")
	void cycle() throws JsonProcessingException {
		final ControlComponentBallotBoxPayload deserializedPayload = objectMapper
				.readValue(objectMapper.writeValueAsString(controlComponentBallotBoxPayload), ControlComponentBallotBoxPayload.class);

		assertEquals(controlComponentBallotBoxPayload, deserializedPayload);
	}

}
