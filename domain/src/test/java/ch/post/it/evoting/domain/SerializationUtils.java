/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain;

import static ch.post.it.evoting.cryptoprimitives.domain.mixnet.ConversionUtils.bigIntegerToHex;
import static ch.post.it.evoting.cryptoprimitives.math.GqElement.GqElementFactory;

import java.io.UncheckedIOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.VerifiablePlaintextDecryption;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientMessage;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.mixnet.ShuffleArgument;
import ch.post.it.evoting.cryptoprimitives.mixnet.VerifiableShuffle;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.VerifiableShuffleGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.DecryptionProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.PlaintextEqualityProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.SchnorrProof;

public class SerializationUtils {
	private static final BigInteger FIVE = BigInteger.valueOf(5);
	private static final GqGroup gqGroup = getGqGroup();
	private static final ZqGroup zqGroup = ZqGroup.sameOrderAs(gqGroup);
	private static final GqElement gFive = GqElementFactory.fromValue(FIVE, gqGroup);
	private static final ObjectMapper mapper = DomainObjectMapper.getNewInstance();

	private SerializationUtils() {
		// Intentionally left blank.
	}

	public static GqGroup getGqGroup() {
		return new GqGroup(BigInteger.valueOf(11), BigInteger.valueOf(5), BigInteger.valueOf(3));
	}

	// ===============================================================================================================================================
	// Basic objects to serialize creation.
	// ===============================================================================================================================================

	static ElGamalMultiRecipientMessage getMessage() {
		final GroupVector<GqElement, GqGroup> messageElements = GroupVector.of(
				GqElementFactory.fromValue(BigInteger.valueOf(4), gqGroup),
				GqElementFactory.fromValue(BigInteger.valueOf(5), gqGroup));

		return new ElGamalMultiRecipientMessage(messageElements);
	}

	static GroupVector<ElGamalMultiRecipientMessage, GqGroup> getMessages(final int copyNumber) {
		return Collections.nCopies(copyNumber, getMessage()).stream().collect(GroupVector.toGroupVector());
	}

	public static List<ElGamalMultiRecipientCiphertext> getCiphertexts(final int copyNumber) {
		final GqElement gamma = GqElementFactory.fromValue(BigInteger.valueOf(4), gqGroup);
		final List<GqElement> phis = Arrays.asList(GqElementFactory.fromValue(BigInteger.valueOf(5), gqGroup),
				GqElementFactory.fromValue(BigInteger.valueOf(9), gqGroup));
		final ElGamalMultiRecipientCiphertext ciphertext = ElGamalMultiRecipientCiphertext.create(gamma, phis);

		return Collections.nCopies(copyNumber, ciphertext);
	}

	public static GroupVector<DecryptionProof, ZqGroup> getDecryptionProofs(final int copyNumber) {
		final ZqGroup zqGroup = ZqGroup.sameOrderAs(gqGroup);
		final ZqElement e = ZqElement.create(2, zqGroup);
		final GroupVector<ZqElement, ZqGroup> z = GroupVector.of(ZqElement.create(1, zqGroup), ZqElement.create(3, zqGroup));
		final DecryptionProof decryptionProof = new DecryptionProof(e, z);

		return Collections.nCopies(copyNumber, decryptionProof).stream().collect(GroupVector.toGroupVector());
	}

	public static GroupVector<SchnorrProof, ZqGroup> getSchnorrProofs(final int copyNumber) {
		final ZqGroup zqGroup = ZqGroup.sameOrderAs(gqGroup);
		final ZqElement e = ZqElement.create(2, zqGroup);
		final ZqElement z = ZqElement.create(2, zqGroup);
		final SchnorrProof schnorrProof = new SchnorrProof(e, z);

		return Collections.nCopies(copyNumber, schnorrProof).stream().collect(GroupVector.toGroupVector());
	}

	public static GroupVector<GqElement, GqGroup> getLongChoiceCodes(final int copyNumber) {
		return Collections.nCopies(copyNumber, gFive).stream().collect(GroupVector.toGroupVector());
	}

	public static ElGamalMultiRecipientPublicKey getPublicKey() {
		final GroupVector<GqElement, GqGroup> keyElements = GroupVector.of(
				GqElementFactory.fromValue(BigInteger.valueOf(4), gqGroup),
				GqElementFactory.fromValue(BigInteger.valueOf(9), gqGroup));
		return new ElGamalMultiRecipientPublicKey(keyElements);
	}

	public static ElGamalMultiRecipientPrivateKey getPrivateKey() {
		final GroupVector<ZqElement, ZqGroup> keyElements = GroupVector.of(
				ZqElement.create(BigInteger.valueOf(2), zqGroup),
				ZqElement.create(BigInteger.valueOf(3), zqGroup));
		return new ElGamalMultiRecipientPrivateKey(keyElements);
	}

	public static ExponentiationProof createExponentiationProof() {
		final ZqElement e = ZqElement.create(1, zqGroup);
		final ZqElement z = ZqElement.create(3, zqGroup);

		return new ExponentiationProof(e, z);
	}

	// ===============================================================================================================================================
	// Nodes creation.
	// ===============================================================================================================================================

	public static JsonNode createEncryptionGroupNode(final GqGroup gqGroup) {
		final JsonNode encryptionGroupNode;
		try {
			encryptionGroupNode = mapper.readTree(mapper.writeValueAsString(gqGroup));
		} catch (final JsonProcessingException e) {
			throw new RuntimeException("Failed to create encryptionGroup node.");
		}

		return encryptionGroupNode;
	}

	static ObjectNode createMessageNode(final ElGamalMultiRecipientMessage elGamalMultiRecipientMessage) {
		final ObjectNode rootNode = mapper.createObjectNode();

		final ArrayNode messageArrayNode = rootNode.putArray("message");
		elGamalMultiRecipientMessage.stream().forEach(element -> messageArrayNode.add(bigIntegerToHex(element.getValue())));

		return rootNode;
	}

	static ArrayNode createMessagesNode(final List<ElGamalMultiRecipientMessage> messages) {
		final ArrayNode messageArrayNode = mapper.createArrayNode();
		messages.forEach(m -> messageArrayNode.add(createMessageNode(m)));

		return messageArrayNode;
	}

	public static ObjectNode createCiphertextNode(final ElGamalMultiRecipientCiphertext ciphertext) {
		final GqElement gamma = ciphertext.getGamma();
		final List<GqElement> phis = ciphertext.stream().skip(1).toList();
		final ObjectNode ciphertextNode = mapper.createObjectNode().put("gamma", bigIntegerToHex(gamma.getValue()));
		final ArrayNode phisArrayNode = ciphertextNode.putArray("phis");
		for (final GqElement phi : phis) {
			phisArrayNode.add(bigIntegerToHex(phi.getValue()));
		}

		return ciphertextNode;
	}

	public static ArrayNode createCiphertextsNode(final List<ElGamalMultiRecipientCiphertext> ciphertexts) {
		final ArrayNode ciphertextsArrayNode = mapper.createArrayNode();
		for (final ElGamalMultiRecipientCiphertext ciphertext : ciphertexts) {
			ciphertextsArrayNode.add(createCiphertextNode(ciphertext));
		}

		return ciphertextsArrayNode;
	}

	static ArrayNode createDecryptionProofsNode(final GroupVector<DecryptionProof, ZqGroup> decryptionProofs) {
		final ArrayNode decryptionProofsArrayNode = mapper.createArrayNode();

		final List<JsonNode> proofsNodes = decryptionProofs.stream().map(proof -> {
			try {
				return mapper.readTree(mapper.writeValueAsString(proof));
			} catch (final JsonProcessingException e) {
				throw new RuntimeException("Failed to serialize proofs.");
			}
		}).toList();

		for (final JsonNode jsonNode : proofsNodes) {
			decryptionProofsArrayNode.add(jsonNode);
		}

		return decryptionProofsArrayNode;
	}

	public static ArrayNode createPublicKeyNode(final ElGamalMultiRecipientPublicKey publicKey) {
		final ArrayNode keyArrayNode = mapper.createArrayNode();
		for (int i = 0; i < publicKey.size(); i++) {
			keyArrayNode.add(bigIntegerToHex(publicKey.get(i).getValue()));
		}

		return keyArrayNode;
	}

	static ArrayNode createPrivateKeyNode(final ElGamalMultiRecipientPrivateKey privateKey) {
		final ArrayNode keyArrayNode = mapper.createArrayNode();
		for (int i = 0; i < privateKey.size(); i++) {
			keyArrayNode.add(bigIntegerToHex(privateKey.get(i).getValue()));
		}

		return keyArrayNode;
	}

	static ObjectNode createVerifiableShuffleNode(final VerifiableShuffle verifiableShuffle) {
		final ObjectNode rootNode = mapper.createObjectNode();

		final ArrayNode shuffledCiphertextsNode = SerializationUtils.createCiphertextsNode(verifiableShuffle.shuffledCiphertexts());
		rootNode.set("shuffledCiphertexts", shuffledCiphertextsNode);

		final JsonNode shuffleArgumentNode = createShuffleArgumentNode();
		rootNode.set("shuffleArgument", shuffleArgumentNode);

		return rootNode;
	}

	static ObjectNode createVerifiablePlaintextDecryptionNode(final VerifiablePlaintextDecryption verifiablePlaintextDecryption) {
		final ObjectNode rootNode = mapper.createObjectNode();

		final ArrayNode messagesNode = createMessagesNode(verifiablePlaintextDecryption.getDecryptedVotes());
		rootNode.set("decryptedVotes", messagesNode);

		final ArrayNode decryptionProofsNode = createDecryptionProofsNode(verifiablePlaintextDecryption.getDecryptionProofs());
		rootNode.set("decryptionProofs", decryptionProofsNode);

		return rootNode;
	}

	public static ArrayNode createGqGroupVectorNode(final GroupVector<GqElement, GqGroup> groupVector) {
		final ArrayNode arrayNode = mapper.createArrayNode();
		groupVector.forEach(lcc -> arrayNode.add(bigIntegerToHex(lcc.getValue())));

		return arrayNode;
	}

	public static JsonNode createLVCCNode(final GqElement longVoteCastCode) {
		try {
			return mapper.readTree(mapper.writeValueAsString(longVoteCastCode));
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException("Failed to construct long vote cast code node.", e);
		}
	}

	public static ObjectNode createExponentiationProofNode(final ExponentiationProof exponentiationProof) {
		final ObjectNode rootNode = mapper.createObjectNode();
		rootNode.put("e", bigIntegerToHex(exponentiationProof.get_e().getValue()));
		rootNode.put("z", bigIntegerToHex(exponentiationProof.get_z().getValue()));

		return rootNode;
	}

	public static ArrayNode createExponentiationProofsNode(final GroupVector<ExponentiationProof, ZqGroup> exponentiationProofs) {
		final ArrayNode arrayNode = mapper.createArrayNode();
		exponentiationProofs.forEach(proof -> arrayNode.add(createExponentiationProofNode(proof)));

		return arrayNode;
	}

	public static ObjectNode createPlaintextEqualityProofNode(final PlaintextEqualityProof plaintextEqualityProof) {
		final ObjectNode rootNode = mapper.createObjectNode();
		rootNode.put("e", bigIntegerToHex(plaintextEqualityProof.get_e().getValue()));

		final ArrayNode arrayNode = mapper.createArrayNode();
		plaintextEqualityProof.get_z().forEach(el -> arrayNode.add(bigIntegerToHex(el.getValue())));

		rootNode.set("z", arrayNode);

		return rootNode;
	}

	public static JsonNode createVotesNode(final GroupVector<GroupVector<PrimeGqElement, GqGroup>, GqGroup> votes) {
		try {
			return mapper.readTree(mapper.writeValueAsString(votes));
		} catch (JsonProcessingException e) {
			throw new UncheckedIOException("Failed to construct votes node.", e);
		}
	}

	public static JsonNode createActualSelectedVotingOptionsNode(final List<List<String>> actualSelectedVotingOptions) {
		try {
			return mapper.readTree(mapper.writeValueAsString(actualSelectedVotingOptions));
		} catch (JsonProcessingException e) {
			throw new UncheckedIOException("Failed to construct actual selected voting options node.", e);
		}
	}

	public static JsonNode createDecodedWriteInVotesNode(final List<List<String>> decodedWriteInVotes) {
		try {
			return mapper.readTree(mapper.writeValueAsString(decodedWriteInVotes));
		} catch (JsonProcessingException e) {
			throw new UncheckedIOException("Failed to construct decoded write-in votes node.", e);
		}
	}

	public static ObjectNode createRequestPayloadNode(final SetupComponentVerificationDataPayload requestPayload) throws JsonProcessingException {
		final ObjectNode rootNode = mapper.createObjectNode();
		rootNode.put("electionEventId", requestPayload.getElectionEventId());
		rootNode.put("verificationCardSetId", requestPayload.getVerificationCardSetId());
		rootNode.put("chunkId", requestPayload.getChunkId());

		final JsonNode encryptionGroupNode = SerializationUtils.createEncryptionGroupNode(gqGroup);
		rootNode.set("encryptionGroup", encryptionGroupNode);

		final JsonNode returnCodeGenerationInputsNode = mapper.readTree(
				mapper.writeValueAsString(requestPayload.getSetupComponentVerificationData()));
		rootNode.set("setupComponentVerificationData", returnCodeGenerationInputsNode);

		final JsonNode combinedCorrectnessInformationNode = mapper.readTree(
				mapper.writeValueAsString(requestPayload.getCombinedCorrectnessInformation()));
		rootNode.set("combinedCorrectnessInformation", combinedCorrectnessInformationNode);

		final JsonNode signatureNode = SerializationUtils.createSignatureNode(requestPayload.getSignature());
		rootNode.set("signature", signatureNode);

		return rootNode;
	}

	public static ObjectNode createResponsePayloadNode(final ControlComponentCodeSharesPayload responsePayload) throws JsonProcessingException {
		final ObjectNode rootNode = mapper.createObjectNode();
		rootNode.put("electionEventId", responsePayload.getElectionEventId());
		rootNode.put("verificationCardSetId", responsePayload.getVerificationCardSetId());
		rootNode.put("chunkId", responsePayload.getChunkId());

		final JsonNode encryptionGroupNode = SerializationUtils.createEncryptionGroupNode(gqGroup);
		rootNode.set("encryptionGroup", encryptionGroupNode);

		final JsonNode returnCodeGenerationOutputsNode = mapper.readTree(mapper.writeValueAsString(responsePayload.getControlComponentCodeShares()));
		rootNode.set("controlComponentCodeShares", returnCodeGenerationOutputsNode);

		rootNode.put("nodeId", 1);

		final JsonNode signatureNode = SerializationUtils.createSignatureNode(responsePayload.getSignature());
		rootNode.set("signature", signatureNode);

		return rootNode;
	}

	// ===============================================================================================================================================
	// Arguments creation.
	// ===============================================================================================================================================

	public static ShuffleArgument createShuffleArgument() {
		// This is an example for m,n,l = 2.
		final VerifiableShuffleGenerator verifiableShuffleGenerator = new VerifiableShuffleGenerator(gqGroup);
		return verifiableShuffleGenerator.genVerifiableShuffle(4, 2).shuffleArgument();
	}

	static ShuffleArgument createSimplestShuffleArgument() {
		// This is an example for m=1,n=2,l=1.
		final VerifiableShuffleGenerator verifiableShuffleGenerator = new VerifiableShuffleGenerator(gqGroup);
		return verifiableShuffleGenerator.genVerifiableShuffle(2, 1).shuffleArgument();
	}

	static JsonNode createShuffleArgumentNode() {
		final JsonNode jsonNode;
		try {
			jsonNode = mapper.readTree(getShuffleArgumentJson());
		} catch (final JsonProcessingException e) {
			throw new RuntimeException("Failed to read ShuffleArgument json.");
		}

		return jsonNode;
	}

	static String getShuffleArgumentJson() {
		// Return corresponding json to createShuffleArgument.
		return "{\"c_A\":[\"0x9\",\"0x5\"],\"c_B\":[\"0x9\",\"0x5\"],\"productArgument\":{\"c_b\":\"0x4\",\"hadamardArgument\":{\"c_b\":[\"0x9\",\"0x5\"],\"zeroArgument\":{\"c_A_0\":\"0x9\",\"c_B_m\":\"0x5\",\"c_d\":[\"0x5\",\"0x9\",\"0x4\",\"0x1\",\"0x4\"],\"a_prime\":[\"0x3\",\"0x2\"],\"b_prime\":[\"0x4\",\"0x3\"],\"r_prime\":\"0x1\",\"s_prime\":\"0x3\",\"t_prime\":\"0x1\"}},\"singleValueProductArgument\":{\"c_d\":\"0x4\",\"c_delta\":\"0x4\",\"c_Delta\":\"0x5\",\"a_tilde\":[\"0x2\",\"0x1\"],\"b_tilde\":[\"0x2\",\"0x3\"],\"r_tilde\":\"0x1\",\"s_tilde\":\"0x0\"}},\"multiExponentiationArgument\":{\"c_A_0\":\"0x3\",\"c_B\":[\"0x1\",\"0x9\",\"0x1\",\"0x1\"],\"E\":[{\"gamma\":\"0x5\",\"phis\":[\"0x5\",\"0x1\"]},{\"gamma\":\"0x4\",\"phis\":[\"0x1\",\"0x5\"]},{\"gamma\":\"0x5\",\"phis\":[\"0x5\",\"0x5\"]},{\"gamma\":\"0x5\",\"phis\":[\"0x3\",\"0x9\"]}],\"a\":[\"0x4\",\"0x4\"],\"r\":\"0x0\",\"b\":\"0x4\",\"s\":\"0x4\",\"tau\":\"0x0\"}}";
	}

	public static JsonNode createSignatureNode(final CryptoPrimitivesSignature signature) {
		try {
			return mapper.readTree(mapper.writeValueAsString(signature));
		} catch (final JsonProcessingException e) {
			throw new RuntimeException("Failed to serialize signature.");
		}
	}

}
