/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.tally;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;

@DisplayName("A GetMixnetInitialCiphertextsInput built with")
class GetMixnetInitialCiphertextsInputTest {

	private static final Random RANDOM_SERVICE = RandomFactory.createRandom();
	private ElGamalGenerator elGamalGenerator;
	private String verificationCardId;
	private int numberOfAllowedWriteInsPlusOne;
	private Map<String, ElGamalMultiRecipientCiphertext> encryptedConfirmedVotes;
	private ElGamalMultiRecipientPublicKey electionPublicKey;
	private ElGamalMultiRecipientCiphertext encryptedVote;

	@BeforeEach
	void setUp() {
		numberOfAllowedWriteInsPlusOne = 1;

		final GqGroup encryptionGroup = GroupTestData.getGqGroup();
		elGamalGenerator = new ElGamalGenerator(encryptionGroup);
		verificationCardId = RANDOM_SERVICE.genRandomBase16String(32).toLowerCase();
		encryptedVote = elGamalGenerator.genRandomCiphertext(numberOfAllowedWriteInsPlusOne);

		encryptedConfirmedVotes = new HashMap<>();
		encryptedConfirmedVotes.put(verificationCardId, encryptedVote);

		electionPublicKey = elGamalGenerator.genRandomPublicKey(numberOfAllowedWriteInsPlusOne);
	}

	@Test
	@DisplayName("null parameters throws NullPointerException.")
	void nullParametersThrows() {
		assertThrows(NullPointerException.class, () -> new GetMixnetInitialCiphertextsInput(numberOfAllowedWriteInsPlusOne, null, electionPublicKey));
		assertThrows(NullPointerException.class,
				() -> new GetMixnetInitialCiphertextsInput(numberOfAllowedWriteInsPlusOne, encryptedConfirmedVotes, null));
	}

	@Test
	@DisplayName("encrypted confirmed votes with null keys or values throws.")
	void nullInEncryptedConfirmedVotesThrows() {
		final Map<String, ElGamalMultiRecipientCiphertext> nullKey = new HashMap<>();
		nullKey.put(null, encryptedVote);
		assertThrows(NullPointerException.class,
				() -> new GetMixnetInitialCiphertextsInput(numberOfAllowedWriteInsPlusOne, nullKey, electionPublicKey));

		final Map<String, ElGamalMultiRecipientCiphertext> nullValue = new HashMap<>();
		nullValue.put(verificationCardId, null);
		assertThrows(IllegalArgumentException.class,
				() -> new GetMixnetInitialCiphertextsInput(numberOfAllowedWriteInsPlusOne, nullValue, electionPublicKey));
	}

	@Test
	@DisplayName("invalid verification card id throws FailedValidationException.")
	void invalidVerificationCardIdThrows() {
		final Map<String, ElGamalMultiRecipientCiphertext> invalidUUIDVotes = new HashMap<>();
		invalidUUIDVotes.put("invalid UUID", encryptedVote);

		assertThrows(FailedValidationException.class,
				() -> new GetMixnetInitialCiphertextsInput(numberOfAllowedWriteInsPlusOne, invalidUUIDVotes, electionPublicKey));
	}

	@Test
	@DisplayName("too small number of write-ins + 1 throws IllegalArgumentException.")
	void tooSmallNumberOfWriteInsPlusOneThrows() {
		final int tooSmallNumberOfWriteInsPlusOne = 0;

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new GetMixnetInitialCiphertextsInput(tooSmallNumberOfWriteInsPlusOne, encryptedConfirmedVotes, electionPublicKey));
		assertEquals("The number of allowed write-ins + 1 must be strictly positive.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("different size votes throws IllegalArgumentException.")
	void differentSizeVotesThrows() {
		final ElGamalMultiRecipientCiphertext differentSizeEncryptedVote = elGamalGenerator.genRandomCiphertext(encryptedVote.size() + 1);
		final Map<String, ElGamalMultiRecipientCiphertext> differentSizeVotes = new HashMap<>();
		differentSizeVotes.put(RANDOM_SERVICE.genRandomBase16String(32).toLowerCase(), encryptedVote);
		differentSizeVotes.put(RANDOM_SERVICE.genRandomBase16String(32).toLowerCase(), differentSizeEncryptedVote);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new GetMixnetInitialCiphertextsInput(numberOfAllowedWriteInsPlusOne, differentSizeVotes, electionPublicKey));
		assertEquals("All vector elements must be the same size.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("different encryption group votes throws IllegalArgumentException.")
	void differentGroupVotesThrows() {
		final GqGroup otherGqGroup = GroupTestData.getDifferentGqGroup(encryptedVote.getGroup());
		final ElGamalGenerator differentGroupElGamalGenerator = new ElGamalGenerator(otherGqGroup);
		final ElGamalMultiRecipientCiphertext differentGroupEncryptedVote = differentGroupElGamalGenerator.genRandomCiphertext(encryptedVote.size());
		final Map<String, ElGamalMultiRecipientCiphertext> differentGroupVotes = new HashMap<>();
		differentGroupVotes.put(RANDOM_SERVICE.genRandomBase16String(32).toLowerCase(), encryptedVote);
		differentGroupVotes.put(RANDOM_SERVICE.genRandomBase16String(32).toLowerCase(), differentGroupEncryptedVote);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new GetMixnetInitialCiphertextsInput(numberOfAllowedWriteInsPlusOne, differentGroupVotes, electionPublicKey));
		assertEquals("All elements must belong to the same group.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("strictly more number of write-ins + 1 than election public key elements throws IllegalArgumentException.")
	void moreNumberOfWriteInsPlusOneThrows() {
		final int moreNumberOfWriteInsPlusOne = electionPublicKey.size() + 1;

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new GetMixnetInitialCiphertextsInput(moreNumberOfWriteInsPlusOne, encryptedConfirmedVotes, electionPublicKey));
		assertEquals(String.format(
				"The election public key must have at least as many elements as the number of allowed write-ins + 1. [delta_hat: %s, delta: %s]",
				moreNumberOfWriteInsPlusOne, electionPublicKey.size()), Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("size of ciphertexts different than number of write-ins + 1 throws IllegalArgumentException.")
	void differentSizeOfCiphertextsThrows() {
		final ElGamalMultiRecipientCiphertext differentSizeEncryptedVote = elGamalGenerator.genRandomCiphertext(numberOfAllowedWriteInsPlusOne + 1);
		final Map<String, ElGamalMultiRecipientCiphertext> differentSizeVotes = new HashMap<>();
		differentSizeVotes.put(RANDOM_SERVICE.genRandomBase16String(32).toLowerCase(), differentSizeEncryptedVote);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new GetMixnetInitialCiphertextsInput(numberOfAllowedWriteInsPlusOne, differentSizeVotes, electionPublicKey));
		assertEquals("The ciphertexts must be of size number of allowed write-ins + 1.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("valid parameters has immutable encrypted confirmed votes.")
	void immutableEncryptedConfirmedVotes() {
		final GetMixnetInitialCiphertextsInput input = new GetMixnetInitialCiphertextsInput(numberOfAllowedWriteInsPlusOne, encryptedConfirmedVotes,
				electionPublicKey);
		final Map<String, ElGamalMultiRecipientCiphertext> inputEncryptedConfirmedVotes = input.encryptedConfirmedVotes();
		assertEquals(encryptedConfirmedVotes, inputEncryptedConfirmedVotes);

		final String otherVerificationCardId = RANDOM_SERVICE.genRandomBase16String(32).toLowerCase();
		encryptedConfirmedVotes.put(otherVerificationCardId, encryptedVote);
		assertNotEquals(encryptedConfirmedVotes, inputEncryptedConfirmedVotes);
		assertFalse(inputEncryptedConfirmedVotes.containsKey(otherVerificationCardId));

		encryptedConfirmedVotes.remove(otherVerificationCardId);
		encryptedConfirmedVotes.replace(verificationCardId, elGamalGenerator.genRandomCiphertext(numberOfAllowedWriteInsPlusOne + 1));
		assertNotEquals(encryptedConfirmedVotes, inputEncryptedConfirmedVotes);

		assertThrows(UnsupportedOperationException.class,
				() -> inputEncryptedConfirmedVotes.put(verificationCardId, encryptedVote));
	}

	@Test
	@DisplayName("empty encrypted confirmed votes does not throw.")
	void emptyEncryptedConfirmedVotesDoesNotThrow() {
		final GetMixnetInitialCiphertextsInput result = assertDoesNotThrow(
				() -> new GetMixnetInitialCiphertextsInput(numberOfAllowedWriteInsPlusOne, Map.of(), electionPublicKey));

		assertEquals(numberOfAllowedWriteInsPlusOne, result.numberOfAllowedWriteInsPlusOne());
		assertEquals(Map.of(), result.encryptedConfirmedVotes());
		assertEquals(electionPublicKey, result.electionPublicKey());
	}

	@Test
	@DisplayName("valid parameters does not throw.")
	void validParametersDoesNotThrow() {
		final GetMixnetInitialCiphertextsInput result = assertDoesNotThrow(
				() -> new GetMixnetInitialCiphertextsInput(numberOfAllowedWriteInsPlusOne, encryptedConfirmedVotes, electionPublicKey));

		assertEquals(numberOfAllowedWriteInsPlusOne, result.numberOfAllowedWriteInsPlusOne());
		assertEquals(encryptedConfirmedVotes, result.encryptedConfirmedVotes());
		assertEquals(electionPublicKey, result.electionPublicKey());
	}
}