/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.configuration;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.domain.SerializationUtils;

class SetupComponentTallyDataPayloadTest {

	private static final ObjectMapper mapper = DomainObjectMapper.getNewInstance();
	private static final Random random = RandomFactory.createRandom();
	private static final Hash hash = HashFactory.createHash();
	private static SetupComponentTallyDataPayload setupComponentTallyDataPayload;
	private static ObjectNode rootNode;

	@BeforeAll
	static void setupAll() throws JsonProcessingException {
		// Create payload.
		final String electionEventId = random.genRandomBase16String(32).toLowerCase();
		final String verificationCardSetId = random.genRandomBase16String(32).toLowerCase();
		final String ballotBoxDefaultTitle = random.genRandomBase16String(32).toLowerCase();
		final GqGroup encryptionGroup = SerializationUtils.getGqGroup();
		final List<String> verificationCardIds = List.of(
				random.genRandomBase16String(32).toLowerCase(),
				random.genRandomBase16String(32).toLowerCase(),
				random.genRandomBase16String(32).toLowerCase());
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> verificationCardPublicKeys = GroupVector.of(
				SerializationUtils.getPublicKey(),
				SerializationUtils.getPublicKey(),
				SerializationUtils.getPublicKey());

		setupComponentTallyDataPayload = new SetupComponentTallyDataPayload(electionEventId, verificationCardSetId, ballotBoxDefaultTitle, encryptionGroup,
				verificationCardIds, verificationCardPublicKeys);

		final byte[] payloadHash = hash.recursiveHash(setupComponentTallyDataPayload);

		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature(payloadHash);
		setupComponentTallyDataPayload.setSignature(signature);

		// Create expected Json.
		rootNode = mapper.createObjectNode();
		rootNode.set("electionEventId", mapper.readTree(mapper.writeValueAsString(electionEventId)));
		rootNode.set("verificationCardSetId", mapper.readTree(mapper.writeValueAsString(verificationCardSetId)));
		rootNode.set("ballotBoxDefaultTitle", mapper.readTree(mapper.writeValueAsString(ballotBoxDefaultTitle)));
		rootNode.set("encryptionGroup", mapper.readTree(mapper.writeValueAsString(encryptionGroup)));
		rootNode.set("verificationCardIds", mapper.readTree(mapper.writeValueAsString(verificationCardIds)));
		rootNode.set("verificationCardPublicKeys", mapper.readTree(mapper.writeValueAsString(verificationCardPublicKeys)));
		rootNode.set("signature", mapper.readTree(mapper.writeValueAsString(signature)));

	}

	@Test
	@DisplayName("serialized gives expected json")
	void serializePayload() throws JsonProcessingException {
		final String serializedPayload = mapper.writeValueAsString(setupComponentTallyDataPayload);
		assertEquals(rootNode.toString(), serializedPayload);
	}

	@Test
	@DisplayName("deserialized gives expected payload")
	void deserializePayload() throws IOException {
		final SetupComponentTallyDataPayload deserializedPayload = mapper.readValue(rootNode.toString(), SetupComponentTallyDataPayload.class);
		assertEquals(setupComponentTallyDataPayload, deserializedPayload);
	}

	@Test
	@DisplayName("serialized then deserialized gives original payload")
	void cycle() throws IOException {
		final SetupComponentTallyDataPayload deserializedPayload = mapper
				.readValue(mapper.writeValueAsString(setupComponentTallyDataPayload), SetupComponentTallyDataPayload.class);

		assertEquals(setupComponentTallyDataPayload, deserializedPayload);
	}
}
