/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.tally;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.mixnet.VerifiableShuffle;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.utils.Validations;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.DecryptionProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.VerifiableDecryptions;

@DisplayName("A VerifyMixDecInput built with")
class VerifyMixDecInputTest extends TestGroupSetup {

	private static final int N = 2;
	private static final int l = 1;
	private static final int l_ID = 32;

	private final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
	private final Random random = RandomFactory.createRandom();
	private final VerifyMixDecHelper verifyMixDecOnlineHelper = new VerifyMixDecHelper(gqGroup);

	private GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> initialCiphertexts;
	private String electionEventId;
	private String ballotBoxId;
	private VerifiableShuffle verifiableShuffle;
	private VerifiableDecryptions verifiableDecryptions;
	private ElGamalMultiRecipientPublicKey electionPublicKey;
	private GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmElectionPublicKeys;
	private ElGamalMultiRecipientPublicKey electoralBoardPublicKey;

	@BeforeEach
	void setUp() {
		electionEventId = random.genRandomBase16String(l_ID).toLowerCase();
		ballotBoxId = random.genRandomBase16String(l_ID).toLowerCase();
		initialCiphertexts = elGamalGenerator.genRandomCiphertextVector(N, l);
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> ciphertexts = elGamalGenerator.genRandomCiphertextVector(N, l);
		verifiableShuffle = new VerifiableShuffle(ciphertexts, verifyMixDecOnlineHelper.createShuffleArgument(N, l));
		final DecryptionProof decryptionProof = new DecryptionProof(zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementVector(l));
		verifiableDecryptions = new VerifiableDecryptions(ciphertexts, GroupVector.of(decryptionProof, decryptionProof));
		final SetupComponentPublicKeys setupComponentPublicKeys = verifyMixDecOnlineHelper.createSetupComponentPublicKeys(l);
		electionPublicKey = setupComponentPublicKeys.electionPublicKey();
		ccmElectionPublicKeys = setupComponentPublicKeys.combinedControlComponentPublicKeys().stream()
				.map(ControlComponentPublicKeys::ccmjElectionPublicKey)
				.collect(GroupVector.toGroupVector());
		electoralBoardPublicKey = setupComponentPublicKeys.electoralBoardPublicKey();
	}

	@Test
	@DisplayName("valid parameters does not throw")
	void validParams() {
		final VerifyMixDecInput verifyMixDecInput = new VerifyMixDecInput(initialCiphertexts,
				Collections.singletonList(verifiableShuffle), Collections.singletonList(verifiableDecryptions), electionPublicKey,
				ccmElectionPublicKeys, electoralBoardPublicKey);

		final List<GqGroup> gqGroups = Arrays.asList(initialCiphertexts.getGroup(),
				verifyMixDecInput.precedingVerifiableShuffledVotes().get(0).shuffleArgument().getGroup(),
				verifyMixDecInput.precedingVerifiableDecryptedVotes().get(0).getGroup(),
				verifyMixDecInput.electionPublicKey().getGroup(),
				verifyMixDecInput.ccmElectionPublicKeys().getGroup(),
				verifyMixDecInput.electoralBoardPublicKey().getGroup());
		assertTrue(Validations.allEqual(gqGroups.stream(), Function.identity()));
	}

	@Test
	@DisplayName("any null param throws NullPointerException")
	void nullParamsThrows() {
		final List<VerifiableShuffle> precedingVerifiableShuffledVotes = Collections.singletonList(verifiableShuffle);
		final List<VerifiableDecryptions> precedingVerifiableDecryptedVotes = Collections.singletonList(verifiableDecryptions);

		assertAll(
				() -> assertThrows(NullPointerException.class, () -> new VerifyMixDecInput(null, precedingVerifiableShuffledVotes,
						precedingVerifiableDecryptedVotes, electionPublicKey, ccmElectionPublicKeys, electoralBoardPublicKey)),
				() -> assertThrows(NullPointerException.class, () -> new VerifyMixDecInput(initialCiphertexts, null,
						precedingVerifiableDecryptedVotes, electionPublicKey, ccmElectionPublicKeys, electoralBoardPublicKey)),
				() -> assertThrows(NullPointerException.class, () -> new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffledVotes,
						null, electionPublicKey, ccmElectionPublicKeys, electoralBoardPublicKey)),
				() -> assertThrows(NullPointerException.class, () -> new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffledVotes,
						precedingVerifiableDecryptedVotes, null, ccmElectionPublicKeys, electoralBoardPublicKey)),
				() -> assertThrows(NullPointerException.class, () -> new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffledVotes,
						precedingVerifiableDecryptedVotes, electionPublicKey, null, electoralBoardPublicKey)),
				() -> assertThrows(NullPointerException.class, () -> new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffledVotes,
						precedingVerifiableDecryptedVotes, electionPublicKey, ccmElectionPublicKeys, null))
		);
	}

	@Test
	@DisplayName("shuffles and decryptions not having the same size throws IllegalArgumentException")
	void differentSizeShufflesDecryptions() {
		final List<VerifiableShuffle> precedingVerifiableShuffledVotes = Collections.singletonList(verifiableShuffle);
		final List<VerifiableDecryptions> precedingVerifiableDecryptedVotes = Arrays.asList(verifiableDecryptions, verifiableDecryptions);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffledVotes, precedingVerifiableDecryptedVotes,
						electionPublicKey, ccmElectionPublicKeys, electoralBoardPublicKey));
		assertEquals("The verifiable shuffles and verifiable decryptions lists must have the same size.",
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("shuffles containing null throws IllegalArgumentException")
	void shuffleContainingNullThrows() {
		final List<VerifiableShuffle> precedingVerifiableShuffledVotes = Collections.singletonList(null);
		final List<VerifiableDecryptions> precedingVerifiableDecryptedVotes = Collections.singletonList(verifiableDecryptions);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffledVotes, precedingVerifiableDecryptedVotes,
						electionPublicKey, ccmElectionPublicKeys, electoralBoardPublicKey));
		assertEquals("The verifiable shuffles must not contain any null.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("decryptions containing null throws IllegalArgumentException")
	void decryptionsContainingNullThrows() {
		final List<VerifiableShuffle> precedingVerifiableShuffledVotes = Collections.singletonList(verifiableShuffle);
		final List<VerifiableDecryptions> precedingVerifiableDecryptedVotes = Collections.singletonList(null);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffledVotes, precedingVerifiableDecryptedVotes,
						electionPublicKey, ccmElectionPublicKeys, electoralBoardPublicKey));
		assertEquals("The verifiable decryptions must not contain any null.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("shuffles not having the same group throws IllegalArgumentException")
	void differentGroupShufflesThrows() {
		final ElGamalGenerator otherElGamalGenerator = new ElGamalGenerator(otherGqGroup);
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> otherCiphertexts = otherElGamalGenerator.genRandomCiphertextVector(N, l);
		final VerifyMixDecHelper otherVerifyMixDecOnlineHelper = new VerifyMixDecHelper(otherGqGroup);

		final VerifiableShuffle otherVerifiableShuffle = new VerifiableShuffle(otherCiphertexts,
				otherVerifyMixDecOnlineHelper.createShuffleArgument(N, l));

		final List<VerifiableShuffle> precedingVerifiableShuffledVotes = Arrays.asList(verifiableShuffle, otherVerifiableShuffle);
		final List<VerifiableDecryptions> precedingVerifiableDecryptedVotes = Arrays.asList(verifiableDecryptions, verifiableDecryptions);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffledVotes, precedingVerifiableDecryptedVotes,
						electionPublicKey, ccmElectionPublicKeys, electoralBoardPublicKey));
		assertEquals("The verifiable shuffles must all have the same group.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("shuffles ciphertexts not having the same element size throws IllegalArgumentException")
	void differentElementSizeShufflesCiphertextsThrows() {
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> otherCiphertexts = elGamalGenerator.genRandomCiphertextVector(N, l + 1);
		final VerifiableShuffle otherVerifiableShuffle = new VerifiableShuffle(otherCiphertexts,
				verifyMixDecOnlineHelper.createShuffleArgument(N, l + 1));

		final List<VerifiableShuffle> precedingVerifiableShuffledVotes = Arrays.asList(verifiableShuffle, otherVerifiableShuffle);
		final List<VerifiableDecryptions> precedingVerifiableDecryptedVotes = Arrays.asList(verifiableDecryptions, verifiableDecryptions);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffledVotes, precedingVerifiableDecryptedVotes,
						electionPublicKey, ccmElectionPublicKeys, electoralBoardPublicKey));
		assertEquals("The verifiable shuffles ciphertexts must all have the same element size.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("shuffles ciphertexts not having the same size throws IllegalArgumentException")
	void differentSizeShufflesCiphertextsThrows() {
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> otherCiphertexts = elGamalGenerator.genRandomCiphertextVector(N + 1, l);
		final VerifiableShuffle otherVerifiableShuffle = new VerifiableShuffle(otherCiphertexts,
				verifyMixDecOnlineHelper.createShuffleArgument(N + 1, l));

		final List<VerifiableShuffle> precedingVerifiableShuffledVotes = Arrays.asList(verifiableShuffle, otherVerifiableShuffle);
		final List<VerifiableDecryptions> precedingVerifiableDecryptedVotes = Arrays.asList(verifiableDecryptions, verifiableDecryptions);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffledVotes, precedingVerifiableDecryptedVotes,
						electionPublicKey, ccmElectionPublicKeys, electoralBoardPublicKey));
		assertEquals("The verifiable shuffles ciphertexts vectors must all have the same size.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("decryptions not having the same group throws IllegalArgumentException")
	void differentGroupDecryptionsThrows() {
		final ElGamalGenerator otherElGamalGenerator = new ElGamalGenerator(otherGqGroup);
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> otherCiphertexts = otherElGamalGenerator.genRandomCiphertextVector(N, l);

		final DecryptionProof otherDecryptionProof = new DecryptionProof(otherZqGroupGenerator.genRandomZqElementMember(),
				otherZqGroupGenerator.genRandomZqElementVector(l));
		final VerifiableDecryptions otherVerifiableDecryptions = new VerifiableDecryptions(otherCiphertexts, GroupVector.of(otherDecryptionProof,
				otherDecryptionProof));

		final List<VerifiableShuffle> precedingVerifiableShuffledVotes = Arrays.asList(verifiableShuffle, verifiableShuffle);
		final List<VerifiableDecryptions> precedingVerifiableDecryptedVotes = Arrays.asList(verifiableDecryptions, otherVerifiableDecryptions);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffledVotes, precedingVerifiableDecryptedVotes,
						electionPublicKey, ccmElectionPublicKeys, electoralBoardPublicKey));
		assertEquals("The verifiable decryptions must all have the same group.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("decryptions proofs not having the same element size throws IllegalArgumentException")
	void differentElementSizeDecryptionsProofsThrows() {
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> otherCiphertexts = elGamalGenerator.genRandomCiphertextVector(N, l + 1);

		final DecryptionProof otherDecryptionProof = new DecryptionProof(zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementVector(l + 1));
		final VerifiableDecryptions otherVerifiableDecryptions = new VerifiableDecryptions(otherCiphertexts, GroupVector.of(otherDecryptionProof,
				otherDecryptionProof));

		final List<VerifiableShuffle> precedingVerifiableShuffledVotes = Arrays.asList(verifiableShuffle, verifiableShuffle);
		final List<VerifiableDecryptions> precedingVerifiableDecryptedVotes = Arrays.asList(verifiableDecryptions, otherVerifiableDecryptions);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffledVotes, precedingVerifiableDecryptedVotes,
						electionPublicKey, ccmElectionPublicKeys, electoralBoardPublicKey));
		assertEquals("The verifiable decryptions must all have the same element size.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("decryptions not having the same size throws IllegalArgumentException")
	void differentSizeDecryptionsThrows() {
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> otherCiphertexts = elGamalGenerator.genRandomCiphertextVector(1, l);

		final DecryptionProof otherDecryptionProof = new DecryptionProof(zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementVector(l));
		final VerifiableDecryptions otherVerifiableDecryptions = new VerifiableDecryptions(otherCiphertexts, GroupVector.of(otherDecryptionProof));

		final List<VerifiableShuffle> precedingVerifiableShuffledVotes = Arrays.asList(verifiableShuffle, verifiableShuffle);
		final List<VerifiableDecryptions> precedingVerifiableDecryptedVotes = Arrays.asList(verifiableDecryptions, otherVerifiableDecryptions);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffledVotes, precedingVerifiableDecryptedVotes,
						electionPublicKey, ccmElectionPublicKeys, electoralBoardPublicKey));
		assertEquals("The verifiable decryptions proofs must all have the same size.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("initial and shuffled ciphertexts not having the same size throws IllegalArgumentException")
	void initialShuffledCiphertextsDifferentSizeThrows() {
		final List<VerifiableShuffle> precedingVerifiableShuffledVotes = Collections.singletonList(verifiableShuffle);
		final List<VerifiableDecryptions> precedingVerifiableDecryptedVotes = Collections.singletonList(verifiableDecryptions);

		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> otherInitialCiphertexts = elGamalGenerator.genRandomCiphertextVector(N + 1, l);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VerifyMixDecInput(otherInitialCiphertexts, precedingVerifiableShuffledVotes, precedingVerifiableDecryptedVotes,
						electionPublicKey, ccmElectionPublicKeys, electoralBoardPublicKey));
		assertEquals("The initial ciphertexts vector and verifiable shuffles ciphertexts vector must have the same size.",
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("initial and shuffled ciphertexts not having the same element size throws IllegalArgumentException")
	void initialShuffledCiphertextsDifferentElementSizeThrows() {
		final List<VerifiableShuffle> precedingVerifiableShuffledVotes = Collections.singletonList(verifiableShuffle);
		final List<VerifiableDecryptions> precedingVerifiableDecryptedVotes = Collections.singletonList(verifiableDecryptions);

		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> otherInitialCiphertexts = elGamalGenerator.genRandomCiphertextVector(N, l + 1);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VerifyMixDecInput(otherInitialCiphertexts, precedingVerifiableShuffledVotes, precedingVerifiableDecryptedVotes,
						electionPublicKey, ccmElectionPublicKeys, electoralBoardPublicKey));
		assertEquals("The initial ciphertexts and verifiable shuffles ciphertexts must have the same element size.",
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("initial and shuffled ciphertexts not having the same element size throws IllegalArgumentException")
	void initialDecryptionsCiphertextsDifferentSizeThrows() {
		final List<VerifiableShuffle> precedingVerifiableShuffledVotes = Collections.singletonList(verifiableShuffle);

		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> otherCiphertexts = elGamalGenerator.genRandomCiphertextVector(1, l);

		final DecryptionProof otherDecryptionProof = new DecryptionProof(zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementVector(l));
		final VerifiableDecryptions otherVerifiableDecryptions = new VerifiableDecryptions(otherCiphertexts,
				GroupVector.of(otherDecryptionProof));
		final List<VerifiableDecryptions> precedingVerifiableDecryptedVotes = Collections.singletonList(otherVerifiableDecryptions);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffledVotes, precedingVerifiableDecryptedVotes,
						electionPublicKey, ccmElectionPublicKeys, electoralBoardPublicKey));
		assertEquals("The initial ciphertexts vector and verifiable decryptions ciphertexts vector must have the same size.",
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("initial and decryptions ciphertexts not having the same element size throws IllegalArgumentException")
	void initialDecryptionsCiphertextsDifferentElementSizeThrows() {
		final List<VerifiableShuffle> precedingVerifiableShuffledVotes = Collections.singletonList(verifiableShuffle);

		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> otherCiphertexts = elGamalGenerator.genRandomCiphertextVector(N, l + 1);

		final DecryptionProof otherDecryptionProof = new DecryptionProof(zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementVector(l + 1));
		final VerifiableDecryptions otherVerifiableDecryptions = new VerifiableDecryptions(otherCiphertexts,
				GroupVector.of(otherDecryptionProof, otherDecryptionProof));
		final List<VerifiableDecryptions> precedingVerifiableDecryptedVotes = Collections.singletonList(otherVerifiableDecryptions);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffledVotes, precedingVerifiableDecryptedVotes,
						electionPublicKey, ccmElectionPublicKeys, electoralBoardPublicKey));
		assertEquals("The initial ciphertexts and verifiable decryptions ciphertexts must have the same element size.",
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("initial ciphertexts greater than election public key throws IllegalArgumentException")
	void intialCiphertextGreaterElectionPublicKeyThrows() {
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> tooLongInitialCiphertexts = elGamalGenerator.genRandomCiphertextVector(N, l + 1);
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> ciphertexts = elGamalGenerator.genRandomCiphertextVector(N, l + 1);
		final VerifiableShuffle tooLongVerifiableShuffle = new VerifiableShuffle(ciphertexts,
				verifyMixDecOnlineHelper.createShuffleArgument(N, l + 1));
		final DecryptionProof tooLongDecryptionProof = new DecryptionProof(zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementVector(l + 1));
		final VerifiableDecryptions tooLongVerifiableDecryptions = new VerifiableDecryptions(ciphertexts,
				GroupVector.of(tooLongDecryptionProof, tooLongDecryptionProof));
		final List<VerifiableShuffle> tooLongPrecedingVerifiableVotes = Collections.singletonList(tooLongVerifiableShuffle);
		final List<VerifiableDecryptions> tooLongDecryptedVotes = Collections.singletonList(tooLongVerifiableDecryptions);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VerifyMixDecInput(tooLongInitialCiphertexts, tooLongPrecedingVerifiableVotes, tooLongDecryptedVotes, electionPublicKey,
						ccmElectionPublicKeys, electoralBoardPublicKey));
		assertEquals("The initial ciphertexts must not have more elements than the election public key.",
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("election public key and electoral board public key not having the same size throws IllegalArgumentException")
	void electionElectoralBoardPublicKeyDifferentSizeThrows() {
		final List<VerifiableShuffle> precedingVerifiableShuffledVotes = Collections.singletonList(verifiableShuffle);
		final List<VerifiableDecryptions> precedingVerifiableDecryptions = Collections.singletonList(this.verifiableDecryptions);
		final ElGamalMultiRecipientPublicKey tooLongElectoralBoardPublicKey = elGamalGenerator.genRandomPublicKey(l + 1);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffledVotes, precedingVerifiableDecryptions
						, electionPublicKey, ccmElectionPublicKeys, tooLongElectoralBoardPublicKey));
		assertEquals("The election public key and the electoral board public key must have the same size.",
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("election public key having more elements than the CCM election public keys throws IllegalArgumentException")
	void electionPublicKeyMoreElementsThanCCMElectionPublicKeysThrows() {
		final List<VerifiableShuffle> precedingVerifiableShuffledVotes = Collections.singletonList(verifiableShuffle);
		final List<VerifiableDecryptions> precedingVerifiableDecryptions = Collections.singletonList(this.verifiableDecryptions);
		final ElGamalMultiRecipientPublicKey tooLongElectionPublicKey = elGamalGenerator.genRandomPublicKey(l + 1);
		final ElGamalMultiRecipientPublicKey tooLongElectoralBoardPublicKey = elGamalGenerator.genRandomPublicKey(l + 1);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffledVotes, precedingVerifiableDecryptions,
						tooLongElectionPublicKey, ccmElectionPublicKeys, tooLongElectoralBoardPublicKey));
		assertEquals("The election public key must not have more elements than the CCM election public keys.",
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("number of CCM election public keys different 4 throws IllegalArgumentException")
	void ccmElectionPublicKeysBadVectorSizeThrows() {
		final List<VerifiableShuffle> precedingVerifiableShuffles = Collections.singletonList(verifiableShuffle);
		final List<VerifiableDecryptions> precedingVerifiableDecryptions = Collections.singletonList(this.verifiableDecryptions);
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> tooFewCcmElectionPublicKeys = Stream.generate(
						() -> elGamalGenerator.genRandomPublicKey(l))
				.limit(3)
				.collect(GroupVector.toGroupVector());
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffles, precedingVerifiableDecryptions, electionPublicKey,
						tooFewCcmElectionPublicKeys, electoralBoardPublicKey));
		assertEquals("There must be exactly 4 CCM election public keys.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("initial and shuffled ciphertexts not having the same group throws IllegalArgumentException")
	void initialShuffledCiphertextsDifferentGroupThrows() {
		final List<VerifiableShuffle> precedingVerifiableShuffledVotes = Collections.singletonList(verifiableShuffle);
		final List<VerifiableDecryptions> precedingVerifiableDecryptedVotes = Collections.singletonList(verifiableDecryptions);

		final ElGamalGenerator otherElGamalGenerator = new ElGamalGenerator(otherGqGroup);
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> otherInitialCiphertexts = otherElGamalGenerator.genRandomCiphertextVector(N, l);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VerifyMixDecInput(otherInitialCiphertexts, precedingVerifiableShuffledVotes, precedingVerifiableDecryptedVotes,
						electionPublicKey, ccmElectionPublicKeys, electoralBoardPublicKey));
		assertEquals("The initial ciphertexts and verifiable shuffles must have the same group.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("initial and decrypted ciphertexts not having the same group throws IllegalArgumentException")
	void initialDecryptedCiphertextsDifferentGroupThrows() {
		final List<VerifiableShuffle> precedingVerifiableShuffledVotes = Collections.singletonList(verifiableShuffle);

		final ElGamalGenerator otherElGamalGenerator = new ElGamalGenerator(otherGqGroup);
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> otherCiphertexts = otherElGamalGenerator.genRandomCiphertextVector(N, l);

		final DecryptionProof otherDecryptionProof = new DecryptionProof(otherZqGroupGenerator.genRandomZqElementMember(),
				otherZqGroupGenerator.genRandomZqElementVector(l));
		final VerifiableDecryptions otherVerifiableDecryptions = new VerifiableDecryptions(otherCiphertexts,
				GroupVector.of(otherDecryptionProof, otherDecryptionProof));
		final List<VerifiableDecryptions> precedingVerifiableDecryptedVotes = Collections.singletonList(otherVerifiableDecryptions);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffledVotes, precedingVerifiableDecryptedVotes,
						electionPublicKey, ccmElectionPublicKeys, electoralBoardPublicKey));
		assertEquals("The initial ciphertexts and verifiable decryptions must have the same group.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("initial and election public key not having the same group throws IllegalArgumentException")
	void initialCiphertextsElectionPublicKeyDifferentGroupThrows() {
		final List<VerifiableShuffle> precedingVerifiableShuffledVotes = Collections.singletonList(verifiableShuffle);
		final List<VerifiableDecryptions> precedingVerifiableDecryptedVotes = Collections.singletonList(verifiableDecryptions);

		final ElGamalGenerator otherElGamalGenerator = new ElGamalGenerator(otherGqGroup);
		final ElGamalMultiRecipientPublicKey otherElectionPublicKey = otherElGamalGenerator.genRandomPublicKey(l);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffledVotes, precedingVerifiableDecryptedVotes,
						otherElectionPublicKey, ccmElectionPublicKeys, electoralBoardPublicKey));
		assertEquals("The initial ciphertexts and election public key must have the same group.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("initial and CCM election public keys not having the same group throws IllegalArgumentException")
	void initialCiphertextsCcmElectionPublicKeysDifferentGroupThrows() {
		final List<VerifiableShuffle> precedingVerifiableShuffledVotes = Collections.singletonList(verifiableShuffle);
		final List<VerifiableDecryptions> precedingVerifiableDecryptedVotes = Collections.singletonList(verifiableDecryptions);

		final ElGamalGenerator otherElGamalGenerator = new ElGamalGenerator(otherGqGroup);
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> otherCcmElectionPublicKey = Stream.generate(
				() -> otherElGamalGenerator.genRandomPublicKey(l)).limit(4).collect(GroupVector.toGroupVector());

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffledVotes, precedingVerifiableDecryptedVotes,
						electionPublicKey, otherCcmElectionPublicKey, electoralBoardPublicKey));
		assertEquals("The initial ciphertexts and CCM election public keys must have the same group.",
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("initial and electoral board public key not having the same group throws IllegalArgumentException")
	void initialCiphertextsElectoralBoardPublicKeyDifferentGroupThrows() {
		final List<VerifiableShuffle> precedingVerifiableShuffledVotes = Collections.singletonList(verifiableShuffle);
		final List<VerifiableDecryptions> precedingVerifiableDecryptedVotes = Collections.singletonList(verifiableDecryptions);

		final ElGamalGenerator otherElGamalGenerator = new ElGamalGenerator(otherGqGroup);
		final ElGamalMultiRecipientPublicKey otherElectoralBoardPublicKey = otherElGamalGenerator.genRandomPublicKey(l);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffledVotes, precedingVerifiableDecryptedVotes,
						electionPublicKey, ccmElectionPublicKeys, otherElectoralBoardPublicKey));
		assertEquals("The initial ciphertexts and electoral board public key must have the same group.",
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("election public key not correctly constituted throws IllegalArgumentException")
	void electionPublicKeyIncorrectlyConstitutedThrows() {
		ElGamalMultiRecipientPublicKey differentElectionPublicKey;
		do {
			differentElectionPublicKey = elGamalGenerator.genRandomPublicKey(l);
		} while (differentElectionPublicKey.equals(electionPublicKey));
		final ElGamalMultiRecipientPublicKey badElectionPublicKey = differentElectionPublicKey;
		final List<VerifiableShuffle> precedingVerifiableShuffles = Collections.singletonList(verifiableShuffle);
		final List<VerifiableDecryptions> precedingVerifiableDecryptions = Collections.singletonList(this.verifiableDecryptions);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffles, precedingVerifiableDecryptions, badElectionPublicKey, ccmElectionPublicKeys, electoralBoardPublicKey));
		assertEquals("Multiplication of the ccmElectionPublicKeys times the electoralBoardPublicKey must equal the electionPublicKey.", Throwables.getRootCause(exception).getMessage());
	}
}
