/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.voting.confirmvote;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.GqGroupGenerator;
import ch.post.it.evoting.domain.ContextIds;

class ControlComponenthlVCCPayloadTest {

	private static final int LENGTH = 32;
	private static final Random random = RandomFactory.createRandom();

	private final String electionEventId = random.genRandomBase16String(LENGTH).toLowerCase();
	private final String verificationCardSetId = random.genRandomBase16String(LENGTH).toLowerCase();
	private final String verificationCardId = random.genRandomBase16String(LENGTH).toLowerCase();
	private final String hashLongVoteCastCodeShare = random.genRandomBase64String(LENGTH);

	@Nested
	@DisplayName("Check constructor validation")
	class CheckConstructor {

		private final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);
		private final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature("signature".getBytes(StandardCharsets.UTF_8));
		private final Integer nodeId = 1;

		private final GqGroup encryptionGroup = GroupTestData.getGqGroup();

		private final ConfirmationKey confirmationKey = new ConfirmationKey(contextIds, new GqGroupGenerator(encryptionGroup).genMember());

		@Test
		@DisplayName("Check null arguments")
		void nullArgs() {

			assertAll(
					() -> assertThrows(NullPointerException.class,
							() -> new ControlComponenthlVCCPayload(null, nodeId, hashLongVoteCastCodeShare, confirmationKey, signature)),
					() -> assertThrows(NullPointerException.class,
							() -> new ControlComponenthlVCCPayload(encryptionGroup, nodeId, null, confirmationKey, signature)),
					() -> assertThrows(NullPointerException.class,
							() -> new ControlComponenthlVCCPayload(encryptionGroup, nodeId, hashLongVoteCastCodeShare, null, signature)),
					() -> assertThrows(NullPointerException.class,
							() -> new ControlComponenthlVCCPayload(encryptionGroup, nodeId, hashLongVoteCastCodeShare, confirmationKey, null))
			);
		}

		@Test
		@DisplayName("Check hashLongVoteCastCodeShare in format Base64")
		void formatBase64() {
			final String validBase64Value = "Ax/6A+2w";
			final String invalidBase64Value = "$$";

			assertAll(
					() -> assertDoesNotThrow(
							() -> new ControlComponenthlVCCPayload(encryptionGroup, nodeId, validBase64Value, confirmationKey, signature)),
					() -> assertThrows(FailedValidationException.class,
							() -> new ControlComponenthlVCCPayload(encryptionGroup, nodeId, invalidBase64Value, confirmationKey, signature))
			);
		}

		@Test
		@DisplayName("Check nodeIds")
		void nodeIdArgs() {

			assertAll(
					() -> assertThrows(IllegalArgumentException.class,
							() -> new ControlComponenthlVCCPayload(encryptionGroup, 0, hashLongVoteCastCodeShare, confirmationKey, signature)),
					() -> assertDoesNotThrow(
							() -> new ControlComponenthlVCCPayload(encryptionGroup, 1, hashLongVoteCastCodeShare, confirmationKey, signature)),
					() -> assertDoesNotThrow(
							() -> new ControlComponenthlVCCPayload(encryptionGroup, 2, hashLongVoteCastCodeShare, confirmationKey, signature)),
					() -> assertDoesNotThrow(
							() -> new ControlComponenthlVCCPayload(encryptionGroup, 3, hashLongVoteCastCodeShare, confirmationKey, signature)),
					() -> assertDoesNotThrow(
							() -> new ControlComponenthlVCCPayload(encryptionGroup, 4, hashLongVoteCastCodeShare, confirmationKey, signature)),
					() -> assertThrows(IllegalArgumentException.class,
							() -> new ControlComponenthlVCCPayload(encryptionGroup, 5, hashLongVoteCastCodeShare, confirmationKey, signature))
			);
		}
	}

	@Test
	void serialisationDeserialisationCycleWorks() throws IOException {
		final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);
		final int nodeId = 1;
		final GqGroup encryptionGroup = GroupTestData.getGqGroup();
		final GqElement gqElement = new GqGroupGenerator(encryptionGroup).genMember();
		final ConfirmationKey confirmationKey = new ConfirmationKey(contextIds, gqElement);
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature("randomSignatureContents".getBytes(StandardCharsets.UTF_8));
		final ControlComponenthlVCCPayload responsePayload = new ControlComponenthlVCCPayload(encryptionGroup,
				nodeId, hashLongVoteCastCodeShare, confirmationKey, signature);
		final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
		final ControlComponenthlVCCPayload responsePayloadCycled = objectMapper.readValue(
				objectMapper.writeValueAsBytes(responsePayload), ControlComponenthlVCCPayload.class);
		assertEquals(responsePayload, responsePayloadCycled);

	}

}
