/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.hashable;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.domain.xmlns.evotingconfig.Configuration;

class HashableContestConfigurationFactoryTest {

	@Test
	void fromConfiguration() throws JAXBException {
		final InputStream inputXml = this.getClass().getClassLoader()
				.getResourceAsStream("configuration-anonymized.xml");
		final JAXBContext context = JAXBContext.newInstance(Configuration.class);
		final Unmarshaller unmarshaller = context.createUnmarshaller();
		final Configuration configuration = (Configuration) unmarshaller.unmarshal(inputXml);
		final Hashable hashableConfiguration = HashableCantonConfigFactory.fromConfiguration(configuration);

		assertNotNull(hashableConfiguration);
	}
}