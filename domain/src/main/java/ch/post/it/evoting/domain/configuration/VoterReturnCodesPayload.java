/*
 * (c) Copyright 2022 Swiss Post Ltd
 */
package ch.post.it.evoting.domain.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

public record VoterReturnCodesPayload(String electionEventId,
									  String verificationCardSetId,
									  List<VoterReturnCodes> voterReturnCodes) {

	public VoterReturnCodesPayload {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkNotNull(voterReturnCodes);
	}

}
