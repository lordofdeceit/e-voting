/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.configuration;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Arrays;

public record CredentialDataPayload(byte[] content) {

	public CredentialDataPayload {
		checkNotNull(content);
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final CredentialDataPayload that = (CredentialDataPayload) o;
		return Arrays.equals(content, that.content);
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(content);
	}

	@Override
	public String toString() {
		return "CredentialDataPayload{" +
				"content=" + Arrays.toString(content) +
				'}';
	}
}
