/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.voting.confirmvote;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateBase64Encoded;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SignedPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableBigInteger;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

@JsonPropertyOrder({ "encryptionGroup", "nodeId", "hashLongVoteCastCodeShare", "confirmationKey", "signature" })
@JsonDeserialize(using = ControlComponenthlVCCSharePayloadDeserializer.class)
public class ControlComponenthlVCCPayload implements SignedPayload {

	private final GqGroup encryptionGroup;

	private final int nodeId;

	private final String hashLongVoteCastCodeShare;

	private final ConfirmationKey confirmationKey;

	private CryptoPrimitivesSignature signature;

	public ControlComponenthlVCCPayload(
			final GqGroup encryptionGroup,
			final int nodeId,
			final String hashLongVoteCastCodeShare,
			final ConfirmationKey confirmationKey,
			final CryptoPrimitivesSignature signature) {
		this(encryptionGroup, nodeId, hashLongVoteCastCodeShare, confirmationKey);
		this.signature = checkNotNull(signature);
	}

	public ControlComponenthlVCCPayload(final GqGroup encryptionGroup, final int nodeId, final String hashLongVoteCastCodeShare,
			final ConfirmationKey confirmationKey) {
		this.encryptionGroup = checkNotNull(encryptionGroup);
		checkArgument(NODE_IDS.contains(nodeId), "The node id must be part of the known node ids. [nodeId: %s]", nodeId);
		this.nodeId = nodeId;
		this.hashLongVoteCastCodeShare = validateBase64Encoded(hashLongVoteCastCodeShare);
		this.confirmationKey = checkNotNull(confirmationKey);

		checkArgument(this.encryptionGroup.equals(confirmationKey.element().getGroup()),
				"The group of the confirmation key and the control component hlVCC payload must be equal.");
	}

	public GqGroup getEncryptionGroup() {
		return encryptionGroup;
	}

	public int getNodeId() {
		return nodeId;
	}

	public String getHashLongVoteCastCodeShare() {
		return hashLongVoteCastCodeShare;
	}

	public ConfirmationKey getConfirmationKey() {
		return confirmationKey;
	}

	@Override
	public CryptoPrimitivesSignature getSignature() {
		return signature;
	}

	@Override
	public void setSignature(final CryptoPrimitivesSignature signature) {
		this.signature = checkNotNull(signature);
	}

	@Override
	public List<Hashable> toHashableForm() {
		return List.of(
				encryptionGroup,
				HashableBigInteger.from(BigInteger.valueOf(nodeId)),
				HashableString.from(hashLongVoteCastCodeShare),
				confirmationKey);
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final ControlComponenthlVCCPayload that = (ControlComponenthlVCCPayload) o;
		return nodeId == that.nodeId &&
				encryptionGroup.equals(that.encryptionGroup) &&
				hashLongVoteCastCodeShare.equals(that.hashLongVoteCastCodeShare) &&
				confirmationKey.equals(that.confirmationKey) &&
				Objects.equals(signature, that.signature);
	}

	@Override
	public int hashCode() {
		return Objects.hash(encryptionGroup, nodeId, hashLongVoteCastCodeShare, confirmationKey, signature);
	}
}
