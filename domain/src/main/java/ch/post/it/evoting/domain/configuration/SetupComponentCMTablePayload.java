/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Objects;
import java.util.SortedMap;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SignedPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;

@JsonDeserialize(builder = SetupComponentCMTablePayload.Builder.class)
@JsonPropertyOrder({ "electionEventId", "verificationCardSetId", "returnCodesMappingTable", "signature" })
public class SetupComponentCMTablePayload implements SignedPayload {

	private final String electionEventId;
	private final String verificationCardSetId;
	private final SortedMap<String, String> returnCodesMappingTable;
	private CryptoPrimitivesSignature signature;

	private SetupComponentCMTablePayload(final String electionEventId, final String verificationCardSetId,
			final SortedMap<String, String> returnCodesMappingTable, final CryptoPrimitivesSignature signature) {
		this.electionEventId = validateUUID(electionEventId);
		this.verificationCardSetId = validateUUID(verificationCardSetId);
		this.returnCodesMappingTable = checkNotNull(returnCodesMappingTable, "returnCodesMappingTable must not be null");
		this.signature = signature; // signature may be null
	}

	public String getElectionEventId() {
		return electionEventId;
	}

	public String getVerificationCardSetId() {
		return verificationCardSetId;
	}

	public SortedMap<String, String> getReturnCodesMappingTable() {
		return returnCodesMappingTable;
	}

	public CryptoPrimitivesSignature getSignature() {
		return signature;
	}

	@Override
	public void setSignature(final CryptoPrimitivesSignature signature) {
		this.signature = signature;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		} else if (o == null || getClass() != o.getClass()) {
			return false;
		}

		final SetupComponentCMTablePayload that = (SetupComponentCMTablePayload) o;

		return electionEventId.equals(that.electionEventId) &&
				verificationCardSetId.equals(that.verificationCardSetId) &&
				returnCodesMappingTable.equals(that.returnCodesMappingTable) &&
				Objects.equals(signature, that.signature);
	}

	@Override
	public int hashCode() {
		return Objects.hash(electionEventId, verificationCardSetId, returnCodesMappingTable, signature);
	}

	@Override
	public List<Hashable> toHashableForm() {

		final List<HashableList> hashableReturnCodesMappingTable = returnCodesMappingTable.entrySet().stream()
				.map(entry -> HashableList.of(HashableString.from(entry.getKey()), HashableString.from(entry.getValue())))
				.toList();

		return List.of(
				HashableString.from(electionEventId),
				HashableString.from(verificationCardSetId),
				HashableList.from(hashableReturnCodesMappingTable));

	}

	@JsonPOJOBuilder(withPrefix = "set")
	public static class Builder {

		private String electionEventId;
		private String verificationCardSetId;
		private SortedMap<String, String> returnCodesMappingTable;
		private CryptoPrimitivesSignature signature;

		@JsonProperty("electionEventId")
		public Builder setElectionEventId(final String electionEventId) {
			this.electionEventId = electionEventId;
			return this;
		}

		@JsonProperty("verificationCardSetId")
		public Builder setVerificationCardSetId(final String verificationCardSetId) {
			this.verificationCardSetId = verificationCardSetId;
			return this;
		}

		@JsonProperty("returnCodesMappingTable")
		public Builder setReturnCodesMappingTable(final SortedMap<String, String> returnCodesMappingTable) {
			this.returnCodesMappingTable = returnCodesMappingTable;
			return this;
		}

		@JsonProperty("signature")
		public Builder setSignature(final CryptoPrimitivesSignature signature) {
			this.signature = signature;
			return this;
		}

		public SetupComponentCMTablePayload build() {
			return new SetupComponentCMTablePayload(electionEventId, verificationCardSetId, returnCodesMappingTable, signature);
		}
	}
}
