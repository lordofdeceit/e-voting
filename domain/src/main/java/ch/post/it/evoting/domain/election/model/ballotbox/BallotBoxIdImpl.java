/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.election.model.ballotbox;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Default implementation of a ballot box identifier.
 */
public record BallotBoxIdImpl(String tenantId, String electionEventId, @JsonProperty("ballotBoxId") String id) implements BallotBoxId {

	private static final long serialVersionUID = 6368889285991914900L;

	public BallotBoxIdImpl {
		Objects.requireNonNull(tenantId, "The tenant identifier cannot be null");
		Objects.requireNonNull(electionEventId, "The election event identifier cannot be null");
		Objects.requireNonNull(id, "The ballot box identifier cannot be null");
	}

	@Override
	public String getTenantId() {
		return tenantId;
	}

	@Override
	public String getElectionEventId() {
		return electionEventId;
	}

	@Override
	@JsonProperty("ballotBoxId")
	public String getId() {
		return id;
	}

}
