/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.hashable;

import static ch.post.it.evoting.domain.hashable.HashableUtils.fromDate;

import ch.ech.xmlns.ech_0044._4.DatePartiallyKnownType;
import ch.ech.xmlns.ech_0044._4.NamedPersonIdType;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;

interface HashableEch0044Factory {
	static Hashable fromDatePartiallyKnown(final DatePartiallyKnownType dateOfBirth) {
		if (dateOfBirth.getYearMonthDay() != null) {
			return fromDate(dateOfBirth.getYearMonthDay());
		} else if (dateOfBirth.getYearMonth() != null) {
			return fromDate(dateOfBirth.getYearMonth());
		} else {
			return fromDate(dateOfBirth.getYear());
		}
	}

	static Hashable fromNamedPersonId(final NamedPersonIdType namedPersonId) {
		return HashableList.of(
				HashableString.from(namedPersonId.getPersonIdCategory()),
				HashableString.from(namedPersonId.getPersonId())
		);
	}
}
