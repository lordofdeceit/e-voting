/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import java.util.List;

import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;

public record ContextIds(String electionEventId, String verificationCardSetId, String verificationCardId) implements HashableList {

	public ContextIds {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		validateUUID(verificationCardId);
	}

	@Override
	public List<Hashable> toHashableForm() {
		return List.of(
				HashableString.from(electionEventId),
				HashableString.from(verificationCardSetId),
				HashableString.from(verificationCardId));
	}
}
