/*
 * (c) Copyright 2022 Swiss Post Ltd
 */
package ch.post.it.evoting.domain.hashable;

import java.util.List;

import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.domain.xmlns.evotingprint.AnswerType;
import ch.post.it.evoting.domain.xmlns.evotingprint.CandidateListType;
import ch.post.it.evoting.domain.xmlns.evotingprint.CandidateType;
import ch.post.it.evoting.domain.xmlns.evotingprint.ContestType;
import ch.post.it.evoting.domain.xmlns.evotingprint.ElectionType;
import ch.post.it.evoting.domain.xmlns.evotingprint.ListType;
import ch.post.it.evoting.domain.xmlns.evotingprint.QuestionType;
import ch.post.it.evoting.domain.xmlns.evotingprint.VoteType;
import ch.post.it.evoting.domain.xmlns.evotingprint.VotingCardList;
import ch.post.it.evoting.domain.xmlns.evotingprint.VotingCardType;

public interface HashableSetupComponentEvotingPrintFactory {
	static Hashable fromVotingCardList(final VotingCardList votingCardList) {
		return HashableList.of(
				HashableSetupComponentEvotingPrintFactory.fromContest(votingCardList.getContest())
		);
	}

	private static Hashable fromContest(final ContestType contest) {
		return HashableList.of(
				HashableString.from(contest.getContestIdentification()),
				HashableSetupComponentEvotingPrintFactory.fromVotingCards(contest.getVotingCard())
		);
	}

	private static Hashable fromVotingCards(final List<VotingCardType> votingCards) {
		return votingCards.stream().map(HashableSetupComponentEvotingPrintFactory::fromVotingCard).collect(HashableList.toHashableList());
	}

	private static Hashable fromVotingCard(final VotingCardType votingCard) {
		return HashableList.of(
				HashableString.from(votingCard.getVoterIdentification()),
				HashableString.from(votingCard.getVotingCardId()),
				HashableString.from(votingCard.getStartVotingKey()),
				HashableString.from(votingCard.getBallotCastingKey()),
				HashableString.from(votingCard.getVoteCastCode()),
				HashableSetupComponentEvotingPrintFactory.fromVotes(votingCard.getVote()),
				HashableSetupComponentEvotingPrintFactory.fromElections(votingCard.getElection())
		);
	}

	private static Hashable fromVotes(final List<VoteType> votes) {
		return votes.stream().map(HashableSetupComponentEvotingPrintFactory::fromVote).collect(HashableList.toHashableList());
	}

	private static Hashable fromVote(final VoteType vote) {
		return HashableList.of(
				HashableString.from(vote.getVoteIdentification()),
				HashableSetupComponentEvotingPrintFactory.fromQuestions(vote.getQuestion())
		);
	}

	private static Hashable fromQuestions(final List<QuestionType> ballots) {
		return ballots.stream().map(HashableSetupComponentEvotingPrintFactory::fromQuestion).collect(HashableList.toHashableList());
	}

	private static Hashable fromQuestion(final QuestionType question) {
		return HashableList.of(
				HashableString.from(question.getQuestionIdentification()),
				HashableSetupComponentEvotingPrintFactory.fromAnswers(question.getAnswer())
		);
	}

	private static Hashable fromAnswers(final List<AnswerType> answers) {
		return answers.stream().map(HashableSetupComponentEvotingPrintFactory::fromAnswer).collect(HashableList.toHashableList());
	}

	private static Hashable fromAnswer(final AnswerType answer) {
		return HashableList.of(
				HashableString.from(answer.getAnswerIdentification()),
				HashableString.from(answer.getChoiceCode())
		);
	}

	private static Hashable fromElections(final List<ElectionType> elections) {
		return elections.stream().map(HashableSetupComponentEvotingPrintFactory::fromElection).collect(HashableList.toHashableList());
	}

	private static Hashable fromElection(final ElectionType election) {
		return HashableList.of(
				HashableString.from(election.getElectionIdentification()),
				HashableSetupComponentEvotingPrintFactory.fromLists(election.getList()),
				HashableSetupComponentEvotingPrintFactory.fromCandidates(election.getCandidate()),
				election.getWriteInsChoiceCode().stream().collect(HashableUtils.stringsToHashableList())
		);
	}

	private static Hashable fromLists(final List<ListType> lists) {
		return lists.stream().map(HashableSetupComponentEvotingPrintFactory::fromList).collect(HashableList.toHashableList());
	}

	private static Hashable fromList(final ListType list) {
		return HashableList.of(
				HashableString.from(list.getListIdentification()),
				HashableUtils.fromNullableString(list.getChoiceCode(), "choiceCode"),
				HashableSetupComponentEvotingPrintFactory.fromCandidateLists(list.getCandidate())
		);
	}

	private static Hashable fromCandidates(final List<CandidateType> candidates) {
		return candidates.stream().map(HashableSetupComponentEvotingPrintFactory::fromCandidate).collect(HashableList.toHashableList());
	}

	private static Hashable fromCandidate(final CandidateType candidate) {
		return HashableList.of(
				HashableString.from(candidate.getCandidateIdentification()),
				candidate.getChoiceCode().stream().collect(HashableUtils.stringsToHashableList())
		);
	}

	private static Hashable fromCandidateLists(final List<CandidateListType> candidateLists) {
		return candidateLists.stream().map(HashableSetupComponentEvotingPrintFactory::fromCandidateList).collect(HashableList.toHashableList());
	}

	private static Hashable fromCandidateList(final CandidateListType candidateList) {
		return HashableList.of(
				HashableString.from(candidateList.getCandidateListIdentification()),
				candidateList.getChoiceCode().stream().collect(HashableUtils.stringsToHashableList())
		);
	}

}

