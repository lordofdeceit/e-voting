/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.voting.confirmvote;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.domain.ContextIds;

public record ConfirmationKey(ContextIds contextIds,
							  GqElement element) implements HashableList {

	public ConfirmationKey {
		checkNotNull(contextIds);
		checkNotNull(element);
	}

	@Override
	public List<Hashable> toHashableForm() {
		return List.of(contextIds, element);
	}
}
