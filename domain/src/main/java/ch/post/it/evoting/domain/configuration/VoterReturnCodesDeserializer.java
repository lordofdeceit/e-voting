/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.configuration;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.EncryptionGroupUtils;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;

public class VoterReturnCodesDeserializer extends JsonDeserializer<VoterReturnCodes> {

	@Override
	public VoterReturnCodes deserialize(final JsonParser parser, final DeserializationContext context) throws IOException {

		final ObjectMapper mapper = DomainObjectMapper.getNewInstance();

		final JsonNode node = mapper.readTree(parser);

		final String verificationCardId = mapper.readValue(node.get("verificationCardId").toString(), String.class);
		final String voteCastReturnCode = mapper.readValue(node.get("voteCastReturnCode").toString(), String.class);

		final JsonNode encryptionGroupNode = node.get("encryptionGroup");
		final GqGroup encryptionGroup = EncryptionGroupUtils.getEncryptionGroup(mapper, encryptionGroupNode);

		final GroupVector<ChoiceReturnCodeToEncodedVotingOptionEntry, GqGroup> choiceReturnCodesToEncodedVotingOptions =
				GroupVector.from(List.of(mapper.reader()
						.withAttribute("group", encryptionGroup)
						.readValue(node.get("choiceReturnCodesToEncodedVotingOptions").toString(),
								ChoiceReturnCodeToEncodedVotingOptionEntry[].class)));

		return new VoterReturnCodes(verificationCardId, voteCastReturnCode, choiceReturnCodesToEncodedVotingOptions, encryptionGroup);
	}
}
