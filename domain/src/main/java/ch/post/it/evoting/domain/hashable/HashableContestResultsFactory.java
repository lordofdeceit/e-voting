/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.hashable;

import static ch.post.it.evoting.domain.hashable.HashableUtils.fromNullableString;

import java.util.List;

import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableBigInteger;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.domain.xmlns.evotingdecrypt.BallotBoxType;
import ch.post.it.evoting.domain.xmlns.evotingdecrypt.BallotElectionType;
import ch.post.it.evoting.domain.xmlns.evotingdecrypt.BallotVoteType;
import ch.post.it.evoting.domain.xmlns.evotingdecrypt.CountingCircleType;
import ch.post.it.evoting.domain.xmlns.evotingdecrypt.DomainOfInfluenceType;
import ch.post.it.evoting.domain.xmlns.evotingdecrypt.ElectionType;
import ch.post.it.evoting.domain.xmlns.evotingdecrypt.Results;
import ch.post.it.evoting.domain.xmlns.evotingdecrypt.VoteType;

public interface HashableContestResultsFactory {
	static Hashable fromResults(final Results results) {
		return HashableList.of(
				HashableString.from(results.getContestIdentification()),
				HashableBigInteger.from(results.getCastBallots()),
				HashableContestResultsFactory.fromBallotBoxes(results.getBallotsBox())
		);
	}

	private static Hashable fromBallotBoxes(final List<BallotBoxType> ballotBoxes) {
		return ballotBoxes.stream().map(HashableContestResultsFactory::fromBallotBox).collect(HashableList.toHashableList());
	}

	private static Hashable fromBallotBox(final BallotBoxType ballotBox) {
		return HashableList.of(
				HashableString.from(ballotBox.getBallotBoxIdentification()),
				HashableContestResultsFactory.fromCountingCircles(ballotBox.getCountingCircle())
		);
	}

	private static Hashable fromCountingCircles(final List<CountingCircleType> countingCircles) {
		return countingCircles.stream().map(HashableContestResultsFactory::fromCountingCircle).collect(HashableList.toHashableList());
	}

	private static Hashable fromCountingCircle(final CountingCircleType countingCircle) {
		return HashableList.of(
				HashableString.from(countingCircle.getCountingCircleIdentification()),
				HashableContestResultsFactory.fromDomainsOfInfluence(countingCircle.getDomainOfInfluence())
		);
	}

	private static Hashable fromDomainsOfInfluence(final List<DomainOfInfluenceType> domainsOfInfluence) {
		return domainsOfInfluence.stream().map(HashableContestResultsFactory::fromDomainOfInfluence).collect(HashableList.toHashableList());
	}

	private static Hashable fromDomainOfInfluence(final DomainOfInfluenceType domainOfInfluence) {
		return HashableList.of(
				HashableString.from(domainOfInfluence.getDomainOfInfluenceIdentification()),
				HashableContestResultsFactory.fromVotes(domainOfInfluence.getVote()),
				HashableContestResultsFactory.fromElections(domainOfInfluence.getElection())
		);
	}

	private static Hashable fromVotes(final List<VoteType> votes) {
		return votes.stream().map(HashableContestResultsFactory::fromVote).collect(HashableList.toHashableList());
	}

	private static Hashable fromVote(final VoteType vote) {
		return HashableList.of(
				HashableString.from(vote.getVoteIdentification()),
				HashableContestResultsFactory.fromBallotVotes(vote.getBallot())
		);
	}

	private static Hashable fromBallotVotes(final List<BallotVoteType> ballots) {
		return ballots.stream().map(HashableContestResultsFactory::fromBallotVote).collect(HashableList.toHashableList());
	}

	private static Hashable fromBallotVote(final BallotVoteType ballot) {
		return ballot.getChosenAnswerIdentification().stream().collect(HashableUtils.stringsToHashableList());
	}

	private static Hashable fromElections(final List<ElectionType> elections) {
		return elections.stream().map(HashableContestResultsFactory::fromElection).collect(HashableList.toHashableList());
	}

	private static Hashable fromElection(final ElectionType election) {
		return HashableList.of(
				HashableString.from(election.getElectionIdentification()),
				HashableContestResultsFactory.fromBallotElections(election.getBallot())
		);
	}

	private static Hashable fromBallotElections(final List<BallotElectionType> ballots) {
		return ballots.stream().map(HashableContestResultsFactory::fromBallotElection).collect(HashableList.toHashableList());
	}

	private static Hashable fromBallotElection(final BallotElectionType electionBallot) {
		return HashableList.of(
				fromNullableString(electionBallot.getChosenListIdentification(), "chosenListIdentification"),
				electionBallot.getChosenCandidateListIdentification().stream().collect(HashableUtils.stringsToHashableList()),
				electionBallot.getChosenCandidateIdentification().stream().collect(HashableUtils.stringsToHashableList()),
				electionBallot.getChosenWriteInsCandidateValue().stream().collect(HashableUtils.stringsToHashableList())
		);
	}

}

