/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.election;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

/**
 * An object to represent the voting client public keys.
 */
public class VotingClientPublicKeysData {

	private EncryptionParameters encryptionParameters;
	private List<String> electionPublicKey;
	private List<String> choiceReturnCodesEncryptionPublicKey;

	public EncryptionParameters getEncryptionParameters() {
		return encryptionParameters;
	}

	public void setEncryptionParameters(final EncryptionParameters encryptionParameters) {
		checkNotNull(encryptionParameters);
		this.encryptionParameters = encryptionParameters;
	}

	public List<String> getElectionPublicKey() {
		return electionPublicKey;
	}

	public void setElectionPublicKey(final List<String> electionPublicKey) {
		checkNotNull(electionPublicKey);
		this.electionPublicKey = electionPublicKey;
	}

	public List<String> getChoiceReturnCodesEncryptionPublicKey() {
		return choiceReturnCodesEncryptionPublicKey;
	}

	public void setChoiceReturnCodesEncryptionPublicKey(final List<String> choiceReturnCodesEncryptionPublicKey) {
		checkNotNull(choiceReturnCodesEncryptionPublicKey);
		this.choiceReturnCodesEncryptionPublicKey = choiceReturnCodesEncryptionPublicKey;
	}
}
