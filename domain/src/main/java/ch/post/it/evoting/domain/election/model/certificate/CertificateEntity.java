/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.election.model.certificate;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Entity representing the info of a Certificate
 */

public class CertificateEntity {

	/**
	 * The identifier of a tenant for the current certificate.
	 */
	private String tenantId;

	/**
	 * The identifier of an election event for the current certificate.
	 */
	private String electionEventId;

	/**
	 * The name of the certificate
	 */
	private String certificateName;

	/**
	 * the platform name
	 **/
	private String platformName;

	/**
	 * The content of the certificate in json format.
	 */
	private String certificateContent;

	public CertificateEntity(final String tenantId, final String electionEventId, final String certificateName, final String platformName,
			final String certificateContent) {
		this.tenantId = checkNotNull(tenantId);
		this.electionEventId = checkNotNull(electionEventId);
		this.certificateName = checkNotNull(certificateName);
		this.platformName = checkNotNull(platformName);
		this.certificateContent = checkNotNull(certificateContent);
	}

	/**
	 * Gets The name of the certificate.
	 *
	 * @return Value of The name of the certificate.
	 */
	public String getCertificateName() {
		return certificateName;
	}

	/**
	 * Gets The identifier of an election event for the current certificate..
	 *
	 * @return Value of The identifier of an election event for the current ballot..
	 */
	public String getElectionEventId() {
		return electionEventId;
	}

	/**
	 * Gets The content of the certificate in json format..
	 *
	 * @return Value of The content of the certificate in json format..
	 */
	public String getCertificateContent() {
		return certificateContent;
	}

	/**
	 * Gets The identifier of a tenant for the current certificate..
	 *
	 * @return Value of The identifier of a tenant for the current certificate..
	 */
	public String getTenantId() {
		return tenantId;
	}

	/**
	 * Gets the platformName
	 *
	 * @return Value of platformName
	 */
	public String getPlatformName() {
		return platformName;
	}

}
