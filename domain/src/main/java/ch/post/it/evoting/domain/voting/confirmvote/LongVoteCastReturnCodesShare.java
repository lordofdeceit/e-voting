/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.voting.confirmvote;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableBigInteger;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;

public record LongVoteCastReturnCodesShare(String electionEventId,
										   String verificationCardSetId,
										   String verificationCardId,
										   int nodeId,
										   GqElement longVoteCastReturnCodeShare,
										   ExponentiationProof exponentiationProof) implements HashableList {

	public LongVoteCastReturnCodesShare {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		validateUUID(verificationCardId);
		checkArgument(NODE_IDS.contains(nodeId), "The node id must be part of the known node ids. [nodeId: %s]", nodeId);
		checkNotNull(longVoteCastReturnCodeShare);
		checkNotNull(exponentiationProof);
		checkArgument(longVoteCastReturnCodeShare.getGroup().hasSameOrderAs(exponentiationProof.getGroup()),
				"The groups of the long vote cast return code share and the exponentiation proof must be of same order.");
	}

	@Override
	public List<Hashable> toHashableForm() {
		return List.of(
				HashableString.from(electionEventId),
				HashableString.from(verificationCardSetId),
				HashableString.from(verificationCardId),
				HashableBigInteger.from(BigInteger.valueOf(nodeId)),
				longVoteCastReturnCodeShare,
				exponentiationProof);
	}

}
