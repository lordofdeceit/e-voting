/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.tally;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

/**
 * Regroups the context values needed by the GetMixnetInitialCiphertexts<sub>j</sub> algorithm.
 *
 * <ul>
 * <li>encryptionGroup, the encryption group. Not null.</li>
 * <li>ee, the election event id. Not null and valid UUID.</li>
 * <li>bb, the ballot box id. Not null and valid UUID.</li>
 * </ul>
 */
public record GetMixnetInitialCiphertextsContext(GqGroup encryptionGroup, String electionEventId, String ballotBoxId) {

	public GetMixnetInitialCiphertextsContext {
		checkNotNull(encryptionGroup);
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);
	}
}
