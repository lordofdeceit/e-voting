/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.configuration;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.EncryptionGroupUtils;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;

public class SetupComponentTallyDataPayloadDeserializer extends JsonDeserializer<SetupComponentTallyDataPayload> {

	@Override
	public SetupComponentTallyDataPayload deserialize(final JsonParser parser, final DeserializationContext context)
			throws IOException {

		final ObjectMapper mapper = DomainObjectMapper.getNewInstance();

		final JsonNode node = mapper.readTree(parser);
		final String electionEventId = mapper.readValue(node.get("electionEventId").toString(), String.class);
		final String verificationCardSetId = mapper.readValue(node.get("verificationCardSetId").toString(), String.class);
		final String ballotBoxDefaultTitle = mapper.readValue(node.get("ballotBoxDefaultTitle").toString(), String.class);
		final JsonNode encryptionGroupNode = node.get("encryptionGroup");
		final GqGroup encryptionGroup = EncryptionGroupUtils.getEncryptionGroup(mapper, encryptionGroupNode);
		final String[] verificationCardIds = mapper.readValue(node.get("verificationCardIds").toString(), String[].class);
		final ElGamalMultiRecipientPublicKey[] verificationCardPublicKeys = mapper.reader()
				.withAttribute("group", encryptionGroup)
				.readValue(node.get("verificationCardPublicKeys"), ElGamalMultiRecipientPublicKey[].class);
		final CryptoPrimitivesSignature signature = mapper.readValue(node.get("signature").toString(), CryptoPrimitivesSignature.class);

		return new SetupComponentTallyDataPayload(electionEventId, verificationCardSetId, ballotBoxDefaultTitle, encryptionGroup,
				List.of(verificationCardIds), GroupVector.of(verificationCardPublicKeys), signature);
	}
}
