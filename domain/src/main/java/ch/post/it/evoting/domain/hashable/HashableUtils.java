/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.hashable;

import java.math.BigInteger;
import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.xml.datatype.XMLGregorianCalendar;

import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableBigInteger;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;

public interface HashableUtils {

	String NO_TOKENNAME_VALUE = "no %s value";

	static Collector<String, ?, HashableList> stringsToHashableList() {
		return Collectors.mapping(HashableString::from, HashableList.toHashableList());
	}

	static Hashable fromDate(final XMLGregorianCalendar calendar) {
		return HashableString.from(calendar.toXMLFormat());
	}

	static Hashable fromNullableString(final String value, final String tokenName) {
		return HashableString.from(value != null ? value : NO_TOKENNAME_VALUE.formatted(tokenName));
	}

	static Hashable fromNullableDate(final XMLGregorianCalendar value, final String tokenName) {
		return value != null ? fromDate(value) : HashableString.from(NO_TOKENNAME_VALUE.formatted(tokenName));
	}

	static Hashable fromNullableBoolean(final Boolean value, final String tokenName) {
		return HashableString.from(value != null ? value.toString() : NO_TOKENNAME_VALUE.formatted(tokenName));
	}

	static Hashable fromNullableBigInteger(final BigInteger value, final String tokenName) {
		return value != null ? HashableBigInteger.from(value) : HashableString.from(NO_TOKENNAME_VALUE.formatted(tokenName));
	}

	static Hashable fromNullableLong(final Long value, final String tokenName) {
		return value != null ? HashableBigInteger.from(BigInteger.valueOf(value)) : HashableString.from(NO_TOKENNAME_VALUE.formatted(tokenName));
	}

	static <T> Hashable fromNullable(final T value, final String tokenName, Function<T, Hashable> function) {
		return value != null ? function.apply(value) : HashableString.from(NO_TOKENNAME_VALUE.formatted(tokenName));
	}

	static <T extends Collection> Hashable fromNullableCollection(final T value, final String tokenName, Function<T, Hashable> function) {
		return value != null && !value.isEmpty() ? function.apply(value) : HashableString.from(NO_TOKENNAME_VALUE.formatted(tokenName));
	}
}
