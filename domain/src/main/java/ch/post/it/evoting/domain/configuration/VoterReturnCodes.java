/*
 * (c) Copyright 2022 Swiss Post Ltd
 */
package ch.post.it.evoting.domain.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;

@JsonDeserialize(using = VoterReturnCodesDeserializer.class)
public record VoterReturnCodes(String verificationCardId,
							   String voteCastReturnCode,
							   GroupVector<ChoiceReturnCodeToEncodedVotingOptionEntry, GqGroup> choiceReturnCodesToEncodedVotingOptions,
							   GqGroup encryptionGroup) {

	public VoterReturnCodes {
		validateUUID(verificationCardId);
		checkNotNull(voteCastReturnCode);
		checkNotNull(choiceReturnCodesToEncodedVotingOptions);
		checkNotNull(encryptionGroup);

		checkArgument(choiceReturnCodesToEncodedVotingOptions.getGroup().equals(encryptionGroup),
				"The group and the choiceReturnCodesToEncodedVotingOptions must have the same group.");
	}

}

