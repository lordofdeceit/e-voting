/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.hashable;

import static ch.post.it.evoting.domain.hashable.HashableUtils.fromNullableBigInteger;
import static ch.post.it.evoting.domain.hashable.HashableUtils.fromNullableString;

import java.math.BigInteger;

import ch.ech.xmlns.ech_0008._3.CountryType;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;

interface HashableEch0008Factory {
	static Hashable fromCountry(final CountryType country) {
		return HashableList.of(
				fromNullableBigInteger(BigInteger.valueOf(country.getCountryId()), "countryId"),
				fromNullableString(country.getCountryIdISO2(), "countryIdISO2"),
				HashableString.from(country.getCountryNameShort())
		);
	}
}
