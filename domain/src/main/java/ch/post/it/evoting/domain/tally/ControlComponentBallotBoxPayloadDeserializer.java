/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.tally;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.EncryptionGroupUtils;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.domain.EncryptedVerifiableVote;

public class ControlComponentBallotBoxPayloadDeserializer extends JsonDeserializer<ControlComponentBallotBoxPayload> {

	@Override
	public ControlComponentBallotBoxPayload deserialize(final JsonParser parser, final DeserializationContext context) throws IOException {
		final ObjectMapper mapper = DomainObjectMapper.getNewInstance();

		final JsonNode node = mapper.readTree(parser);
		final JsonNode encryptionGroupNode = node.get("encryptionGroup");
		final GqGroup encryptionGroup = EncryptionGroupUtils.getEncryptionGroup(mapper, encryptionGroupNode);

		final String electionEventId = mapper.readValue(node.get("electionEventId").toString(), String.class);
		final String ballotBoxId = mapper.readValue(node.get("ballotBoxId").toString(), String.class);
		final int nodeId = mapper.readValue(node.get("nodeId").toString(), Integer.class);

		final List<EncryptedVerifiableVote> votes = List.of(mapper.reader().withAttribute("group", encryptionGroup)
				.readValue(node.get("confirmedEncryptedVotes"), EncryptedVerifiableVote[].class));

		final CryptoPrimitivesSignature signature = mapper.readValue(node.get("signature").toString(), CryptoPrimitivesSignature.class);

		return new ControlComponentBallotBoxPayload(encryptionGroup, electionEventId, ballotBoxId, nodeId, votes, signature);
	}
}
