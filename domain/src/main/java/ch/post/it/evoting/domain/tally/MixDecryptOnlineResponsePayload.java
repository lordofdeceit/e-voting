/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.tally;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ControlComponentShufflePayload;

public record MixDecryptOnlineResponsePayload(ControlComponentBallotBoxPayload controlComponentBallotBoxPayload,
											  ControlComponentShufflePayload controlComponentShufflePayload) {

	public MixDecryptOnlineResponsePayload {
		checkNotNull(controlComponentBallotBoxPayload);
		checkNotNull(controlComponentShufflePayload);

		checkArgument(controlComponentBallotBoxPayload.getEncryptionGroup().equals(controlComponentShufflePayload.getEncryptionGroup()),
				"The control component ballot box payload and the control component shuffle payload must have the encryption group.");
		checkArgument(controlComponentBallotBoxPayload.getElectionEventId().equals(controlComponentShufflePayload.getElectionEventId()),
				"The control component ballot box payload and the control component shuffle payload must have the same election event id.");
		checkArgument(controlComponentBallotBoxPayload.getBallotBoxId().equals(controlComponentShufflePayload.getBallotBoxId()),
				"The control component ballot box payload and the control component shuffle payload must have the same ballot box id.");
		checkArgument(controlComponentBallotBoxPayload.getNodeId() == controlComponentShufflePayload.getNodeId(),
				"The control component ballot box payload and the control component shuffle payload must have the same node id.");

	}

}
