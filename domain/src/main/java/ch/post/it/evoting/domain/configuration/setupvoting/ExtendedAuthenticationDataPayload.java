/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.configuration.setupvoting;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Arrays;

public record ExtendedAuthenticationDataPayload(byte[] content) {
	public ExtendedAuthenticationDataPayload {
		checkNotNull(content);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ExtendedAuthenticationDataPayload that = (ExtendedAuthenticationDataPayload) o;
		return Arrays.equals(content, that.content);
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(content);
	}

	@Override
	public String toString() {
		return "ExtendedAuthenticationDataPayload{" +
				"content=" + Arrays.toString(content) +
				'}';
	}
}
