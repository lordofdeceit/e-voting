/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.hashable;

import java.security.SignatureException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.ChannelSecurityContextData;
import ch.post.it.evoting.domain.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.domain.xmlns.evotingconfig.RegisterType;
import ch.post.it.evoting.domain.xmlns.evotingconfig.VoterType;

public class ConfigurationSignerListener extends Marshaller.Listener {
	private final Configuration configuration;
	private final SignatureKeystore<Alias> signatureService;
	private final List<Hashable> voterHashes = new ArrayList<>();

	private static final Logger log = LoggerFactory.getLogger(ConfigurationSignerListener.class);

	public ConfigurationSignerListener(final Configuration configuration, final SignatureKeystore<Alias> signatureService) {
		this.configuration = configuration;
		this.signatureService = signatureService;
	}

	@Override
	public void beforeMarshal(final Object source) {
		super.beforeMarshal(source);
		if (source instanceof VoterType voterType) {
			try {
				voterHashes.add(calculateVoterHash(voterType));
			} catch (final Exception e) {
				throw new IllegalStateException("Unable to compute voter hash", e);
			}
		}
	}

	@Override
	public void afterMarshal(final Object source) {
		super.afterMarshal(source);
		if (source instanceof RegisterType) {
			final Hashable configurationHash = calculateConfigurationHash();
			signConfiguration(configuration, configurationHash);
		}
	}

	private Hashable calculateVoterHash(final VoterType source) {
		return HashableCantonConfigFactory.fromVoter(source);
	}

	private Hashable calculateConfigurationHash() {
		return HashableCantonConfigFactory.fromConfigurationStreamedVoters(configuration, voterHashes.stream().collect(
				HashableList.toHashableList()));
	}

	private void signConfiguration(final Configuration configuration, final Hashable configurationHash) {
		final Hashable additionalContextData = ChannelSecurityContextData.cantonConfig();
		try {
			log.info("Signing the configuration file");
			configuration.setSignature(signatureService.generateSignature(configurationHash, additionalContextData));
		} catch (final SignatureException e) {
			throw new IllegalStateException("Unable to generate the signature", e);
		}
	}
}