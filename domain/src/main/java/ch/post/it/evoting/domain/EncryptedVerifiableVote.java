/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.PlaintextEqualityProof;

public record EncryptedVerifiableVote(ContextIds contextIds,
									  ElGamalMultiRecipientCiphertext encryptedVote,
									  ElGamalMultiRecipientCiphertext exponentiatedEncryptedVote,
									  ElGamalMultiRecipientCiphertext encryptedPartialChoiceReturnCodes,
									  ExponentiationProof exponentiationProof,
									  PlaintextEqualityProof plaintextEqualityProof) implements HashableList {

	public EncryptedVerifiableVote {
		checkNotNull(contextIds);
		checkNotNull(encryptedVote);
		checkNotNull(exponentiatedEncryptedVote);
		checkNotNull(encryptedPartialChoiceReturnCodes);
		checkNotNull(exponentiationProof);
		checkNotNull(plaintextEqualityProof);

		checkArgument(exponentiatedEncryptedVote.size() == 1, "The exponentiated encrypted vote size must be one.");
		checkArgument(encryptedVote.size() <= encryptedPartialChoiceReturnCodes.size() + 1,
				"The encrypted vote size must be smaller or equal to the encrypted partial choice return codes size + 1.");

		checkArgument(encryptedVote.getGroup().equals(exponentiatedEncryptedVote.getGroup()),
				"The groups of the encrypted vote and the exponentiated encrypted vote must be the same.");
		checkArgument(encryptedVote.getGroup().equals(encryptedPartialChoiceReturnCodes.getGroup()),
				"The groups of the encrypted vote and the encrypted partial choice return codes must be the same.");
		checkArgument(encryptedVote.getGroup().hasSameOrderAs(exponentiationProof.getGroup()),
				"The groups of the encrypted vote and the exponentiation proof must be of same order.");
		checkArgument(encryptedVote.getGroup().hasSameOrderAs(plaintextEqualityProof.getGroup()),
				"The groups of the encrypted vote and the plaintext equality proof must be of same order.");
	}

	@Override
	public List<Hashable> toHashableForm() {

		return List.of(contextIds,
				encryptedVote,
				exponentiatedEncryptedVote,
				encryptedPartialChoiceReturnCodes,
				exponentiationProof,
				plaintextEqualityProof);

	}
}
