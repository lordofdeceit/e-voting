/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */

const {checkNotNull} = require("crypto-primitives-ts/lib/cjs/validation/preconditions");
const {ImmutableBigInteger} = require("crypto-primitives-ts/lib/cjs/immutable_big_integer");
const {GqGroup} = require("crypto-primitives-ts/lib/cjs/math/gq_group");
const {GqElement} = require("crypto-primitives-ts/lib/cjs/math/gq_element");
const {ElGamalMultiRecipientPublicKey} = require("crypto-primitives-ts/lib/cjs/elgamal/elgamal_multi_recipient_public_key");

module.exports = (function () {
	'use strict';

	const HEX_PREFIX = "0x";

	/**
	 * Deserializes an {ImmutableBigInteger}.
	 *
	 * @param element {string}, the element to deserialize. Must be not null.
	 * @returns {ImmutableBigInteger} the deserialized {ImmutableBigInteger}.
	 */
	function deserializeImmutableBigInteger(element) {
		checkNotNull(element);
		if (element.startsWith(HEX_PREFIX)) {
			return ImmutableBigInteger.fromString(element.replace(HEX_PREFIX, ""), 16);
		}
		return ImmutableBigInteger.fromString(element);
	}

	/**
	 * Deserializes a {GqGroup}.
	 *
	 * @param element {string}, the element to deserialize. Must be not null.
	 * @returns {GqGroup} the deserialized GqGroup.
	 */
	function deserializeGqGroup(element) {
		checkNotNull(element);
		const p = deserializeImmutableBigInteger(element.p);
		const q = deserializeImmutableBigInteger(element.q);
		const g = deserializeImmutableBigInteger(element.g);
		return new GqGroup(p, q, g);
	}

	/**
	 * Deserializes a {GqElement}.
	 *
	 * @param element {string}, the element to deserialize. Must be not null.
	 * @param gqGroup {GqGroup}, the GqGroup. Must be not null.
	 * @returns {GqElement} the deserialized {GqElement}.
	 */
	function deserializeGqElement(element, gqGroup) {
		checkNotNull(element);
		checkNotNull(gqGroup);
		return GqElement.fromValue(deserializeImmutableBigInteger(element), gqGroup)
	}

	/**
	 * Deserializes an ElGamal multi recipient public key.
	 *
	 * @param element {string[]}, the element to deserialize. Must be not null.
	 * @param gqGroup {GqGroup}, the GqGroup. Must be not null.
	 * @returns {ElGamalMultiRecipientPublicKey} the deserialized {ElGamalMultiRecipientPublicKey}.
	 */
	function deserializeElGamalMultiRecipientPublicKey(element, gqGroup) {
		checkNotNull(element);
		checkNotNull(gqGroup);
		return new ElGamalMultiRecipientPublicKey(element.map(gqElement => deserializeGqElement(gqElement, gqGroup)));
	}

	return {
		deserializeImmutableBigInteger: deserializeImmutableBigInteger,
		deserializeGqGroup: deserializeGqGroup,
		deserializeGqElement: deserializeGqElement,
		deserializeElGamalMultiRecipientPublicKey: deserializeElGamalMultiRecipientPublicKey
	}
})();
