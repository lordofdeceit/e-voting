/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */

(function() {
	const model = {
        ballot: require('./ballot')
    };

    module.exports = model;

}).call(this);
