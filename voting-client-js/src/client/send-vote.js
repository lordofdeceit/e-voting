/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
/* jshint ignore: start */
/* zglobal require */
/* zglobal OV */

const createVoteAlgorithm = require('../algorithm/create-vote');
const writeInToQuadraticResidueAlgorithm = require('../algorithm/write-in-to-quadratic-residue');
const {serializeElGamalCiphertext, serializeExponentiationProof, serializePlaintextEqualityProof} = require("../model/primitives-serializer");
const {GroupVector} = require("crypto-primitives-ts/lib/cjs/group_vector");
const {PrimeGqElement} = require("crypto-primitives-ts/lib/cjs/math/prime_gq_element");
const {validateUUID} = require("../validations/validations");
const {GqElement} = require("crypto-primitives-ts/lib/cjs/math/gq_element");
const {ImmutableBigInteger} = require("crypto-primitives-ts/lib/cjs/immutable_big_integer");

module.exports = (function () {
  'use strict';

  const XMLHttpRequest = XMLHttpRequest || require('xhr2');
  const Q = require('q');
  const config = require('./config.js');
  const session = require('./session.js');

  // lenient json parse

  const jsonparse = function (x) {
    try {
      return JSON.parse(x);
    } catch (ignore) {
      return {};
    }
  };

  // create a vote request

  const createVoteRequest = function (
    selectedEncodedVotingOptionsInt,
    encodedWriteIns,
    correctness
  ) {

    const primitivesParams = session('primitivesParams');
    const encryptionGroup = primitivesParams.encryptionGroup;

    // Selected encoded voting options
    const selectedEncodedVotingOptionList = selectedEncodedVotingOptionsInt.map(function (o) {
      // Avoid type coercion in the voter's selections
      const votersSelection = parseInt(o, 10);
      return PrimeGqElement.fromValue(votersSelection, encryptionGroup);
    });
    const selectedEncodedVotingOptions = GroupVector.from(selectedEncodedVotingOptionList);

    // Write-ins
    const writeInsQuadraticResidues = encodedWriteIns.map(function (o) {
      return writeInToQuadraticResidueAlgorithm.writeInToQuadraticResidue({encryptionGroup: encryptionGroup}, o);
    });
    const writeInsDummies = [];
    const dummyValue = GqElement.fromValue(ImmutableBigInteger.ONE, encryptionGroup);
    for (let i = 0; i < primitivesParams.totalNumberOfWriteIns - writeInsQuadraticResidues.length; i++) {
      writeInsDummies[i] = dummyValue;
    }
    const writeIns = GroupVector.of(...writeInsQuadraticResidues, ...writeInsDummies);

    const createVoteOutput = createVoteAlgorithm.createVote(
      {
        encryptionGroup: encryptionGroup,
        electionEventId: validateUUID(config('electionEventId')),
        numberOfSelectableVotingOptions: primitivesParams.correctnessInformationSelections.length,
        encodedVotingOptions: primitivesParams.encodedVotingOptions,
        actualVotingOptions: primitivesParams.actualVotingOptions
      },
      validateUUID(session('verificationCardId')),
      selectedEncodedVotingOptions,
      writeIns,
      primitivesParams.electionPublicKey,
      primitivesParams.choiceReturnCodesEncryptionPublicKey,
      session('verificationCardSecretKey'),
    );

    const correctnessIds = selectedEncodedVotingOptionsInt.map(function (o) {
      return correctness[o] || [];
    });

    // serialize vote elements

    const serializedEncryptedVote = serializeElGamalCiphertext(createVoteOutput.encryptedVote);
    const serializedEncryptedPartialChoiceReturnCodes = serializeElGamalCiphertext(createVoteOutput.encryptedPartialChoiceReturnCodes);
    const serializedExponentiatedEncryptedVote = serializeElGamalCiphertext(createVoteOutput.exponentiatedEncryptedVote);
    const serializedExponentiationProof = serializeExponentiationProof(createVoteOutput.exponentiationProof);
    const serializedPlaintextEqualityProof = serializePlaintextEqualityProof(createVoteOutput.plaintextEqualityProof);

    const serializedCorrectnessIds = JSON.stringify(correctnessIds);

    return {
      encryptedVote: serializedEncryptedVote,
      encryptedPartialChoiceReturnCodes: serializedEncryptedPartialChoiceReturnCodes,
      correctnessIds: serializedCorrectnessIds,
      credentialId: session('credentials').credentialId,
      exponentiationProof: serializedExponentiationProof,
      plaintextEqualityProof: serializedPlaintextEqualityProof,
      exponentiatedEncryptedVote: serializedExponentiatedEncryptedVote
    };
  };

  // process vote response

  const processVoteResponse = function (response) {
    return response.choiceCodes.split(';');
  };

  // process vote

  const processVote = function (deferred, response) {
    if (response.valid && response.choiceCodes) {
      try {
        const result = processVoteResponse(response);
        deferred.resolve(result);
      } catch (e) {
        deferred.reject(e.message);
      }
    } else {
      if (response.validationError) {
        deferred.reject(response);
      } else {
        deferred.reject('invalid vote');
      }
    }
  };

  // encrypt and send the vote

  const sendVote = function (
    options,
    writeIns,
    correctness
  ) {
    const deferred = Q.defer();

    const voteRequestData = createVoteRequest(
      options,
      writeIns,
      correctness
    );

    // send vote

    const endpoint = config('endpoints.votes')
      .replace('{tenantId}', config('tenantId'))
      .replace('{electionEventId}', config('electionEventId'))
      .replace('{votingCardId}', session('votingCardId'));

    const xhr = new XMLHttpRequest();
    xhr.open('POST', config('host') + endpoint);
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          processVote(deferred, JSON.parse(this.responseText));
        } else {
          let response = jsonparse(this.responseText);
          if (!response || typeof response !== 'object') {
            response = {};
          }
          response.httpStatus = xhr.status;
          response.httpStatusText = xhr.statusText;
          deferred.reject(response);
        }
      }
    };
    xhr.onerror = function () {
      try {
        deferred.reject(xhr.status);
      } catch (e) {
        //This block is intentionally left blank
      }
    };
    xhr.setRequestHeader('Accept', 'application/json');
    xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    xhr.setRequestHeader(
      'authenticationToken',
      JSON.stringify(session('authenticationToken')),
    );
    xhr.send(JSON.stringify(voteRequestData));

    return deferred.promise;
  };

  return {
    sendVote: sendVote
  };
})();
