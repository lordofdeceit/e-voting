/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
/* global OV */

const {checkNotNull, checkArgument} = require("crypto-primitives-ts/lib/cjs/validation/preconditions");
const {FailedValidationError} = require("./failed_validation_error");

module.exports = (function () {
  'use strict';

  const UUID_LENGTH = 32;
  const BASE16_LOWERCASE_ALPHABET ='a-f0-9';
  const BASE32_LOWERCASE_ALPHABET_WITHOUT_PADDING = 'a-z2-7';
  const BASE64_REGEX = '^(?:[A-Za-z0-9+\/]{4})*(?:[A-Za-z0-9+\/]{3}=|[A-Za-z0-9+\/]{2}={2})$';

  /**
   * Validates that the input string is in Base16 lowercase alphabet and has length of 32.
   * The alphabet corresponds to the lowercase version of "Table 5: The Base 16 Alphabet" from RFC3548.
   *
   * @param {string} toValidate, the string to validate. Must be non-null.
   * @throws NullPointerError if the input string is null.
   * @throws FailedValidationError if the input string validation fails.
   * @returns the validated input string.
   */
  function validateUUID(toValidate) {
    checkNotNull(toValidate);
    return validateInAlphabet(toValidate, `^[${BASE16_LOWERCASE_ALPHABET}]{${UUID_LENGTH}}$`);
  }

  /**
   * Validates that the input string is in Base32 lowercase alphabet excluding padding "=" with the specified length.
   * The alphabet corresponds to the lowercase version excluding padding "=" of "Table 3: The Base 32 Alphabet" from RFC3548.
   *
   * @param {string} toValidate, the string to validate. Must be non-null.
   * @param {number} length, the length of the string to validate. Must be greater than 0.
   * @throws NullPointerError if the input string is null.
   * @throws IllegalArgumentError if the length is smaller than or equal to zero.
   * @throws FailedValidationError if the input string validation fails.
   * @returns the validated input string.
   */
  function validateBase32StringWithoutPadding(toValidate, length) {
    checkNotNull(toValidate);
    checkArgument(length > 0);
    return validateInAlphabet(toValidate, `^[${BASE32_LOWERCASE_ALPHABET_WITHOUT_PADDING}]{${length.toString()}}$`);
  }

  /**
   * Validates that the input string is in Base64 lowercase alphabet including padding "=".
   * The alphabet corresponds to "Table 1: The Base 64 Alphabet" from RFC3548.
   *
   * @param {string} toValidate, the string to validate. Must be non-null.
   * @throws NullPointerError if the input string is null.
   * @throws IllegalArgumentError if the input length is not a multiple of 4.
   * @throws FailedValidationError if the input string validation fails.
   * @returns the validated input string.
   */
  function validateBase64String(toValidate) {
    checkNotNull(toValidate);
    checkArgument(toValidate.length % 4 === 0);
    return validateInAlphabet(toValidate, BASE64_REGEX);
  }


  const validateInAlphabet = function (toValidate, pattern) {
    checkNotNull(toValidate);
    checkNotNull(pattern);

    const regExp = new RegExp(pattern);
    if (!toValidate.match(regExp)) {
      throw new FailedValidationError(`The given string does not comply with the required format. [string: ${toValidate}, format: ${pattern}].`);
    }
    return toValidate;
  }

  return {
    validateUUID: validateUUID,
    validateBase32StringWithoutPadding: validateBase32StringWithoutPadding,
    validateBase64String: validateBase64String
  };

})();
