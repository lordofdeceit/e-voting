/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
/* global require */
/* global OV */
/* jshint maxlen: 666  */

const _ = require('lodash');

module.exports = (function () {
	const parseCorrectnessIds = function (correctnessIds, option, attributes) {
		const attrMap = _.reduce(
			attributes,
			function (acc, attr) {
				acc[attr.id] = attr;
				return acc;
			},
			{},
		);

		const prime = option.representation;
		const attribute = attrMap[option.attribute];

		if (attribute) {
			if (!correctnessIds[prime]) {
				correctnessIds[prime] = [];
			}

			const related = [attribute.id].concat(attribute.related);

			_.each(related, function (rel) {
				if (
					attrMap[rel] &&
					attrMap[rel].correctness &&
					((attrMap[rel].correctness === true) ||
					(attrMap[rel].correctness === 'true'))
				) {
					correctnessIds[prime].push(rel);
				}
			});
		}
	};

	return parseCorrectnessIds;
})();
