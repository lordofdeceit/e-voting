/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
/* global OV */

const {ElGamalMultiRecipientPublicKey} = require("crypto-primitives-ts/lib/cjs/elgamal/elgamal_multi_recipient_public_key");
const {GqGroup} = require("crypto-primitives-ts/lib/cjs/math/gq_group");
const {ImmutableBigInteger} = require("crypto-primitives-ts/lib/cjs/immutable_big_integer");
const {deserializeElGamalMultiRecipientPublicKey, deserializeGqGroup} = require("../model/primitives-deserializer");
const {checkArgument, checkNotNull} = require("crypto-primitives-ts/lib/cjs/validation/preconditions");
const {GqElement} = require("crypto-primitives-ts/lib/cjs/math/gq_element");
const {GroupVector} = require("crypto-primitives-ts/lib/cjs/group_vector");
const {PrimeGqElement} = require("crypto-primitives-ts/lib/cjs/math/prime_gq_element");

module.exports = (function () {
	'use strict';

	const VALID_XML_TOKEN_PATTERN = new RegExp('^[\\w\\-_]{1,50}$', 'gm');
	const MAX_SIZE = 50;

	/**
	 * Computes the primitives parameters.
	 *
	 * @param {string} tokenResponse, the JSON containing the serialized parameters.
	 * @returns {PrimitivesParams}, the primitives params object.
	 */
	function parsePrimitivesParams(tokenResponse) {
		const votingClientPublicKeys = tokenResponse.votingClientPublicKeys;

		// Encryption group, we assume the signature has been checked and verified
		const encryptionParameters = deserializeGqGroup(votingClientPublicKeys.encryptionParameters);

		// Election public key
		const electionPublicKey = deserializeElGamalMultiRecipientPublicKey(
			votingClientPublicKeys.electionPublicKey,
			encryptionParameters
		);

		// Choice return codes encryption public key
		const choiceReturnCodesEncryptionPublicKey = deserializeElGamalMultiRecipientPublicKey(
			votingClientPublicKeys.choiceReturnCodesEncryptionPublicKey,
			encryptionParameters
		);

		// Encoded voting options
		const encodedVotingOptionsList = tokenResponse.verificationCard.encodedVotingOptions
			.map(encodedVotingOptionString => {
				const encodedVotingOption = parseInt(encodedVotingOptionString, 10);
				return PrimeGqElement.fromValue(encodedVotingOption, encryptionParameters);
			});
		const encodedVotingOptions = GroupVector.from(encodedVotingOptionsList);

		// Actual voting options
		validateActualVotingOptions(tokenResponse.verificationCard.actualVotingOptions);

		/**
		 * @typedef {object} PrimitivesParams
		 * @property {ElGamalMultiRecipientPublicKey} electionPublicKey electionPublicKey, the election public key.
		 * @property {ElGamalMultiRecipientPublicKey} choiceReturnCodesEncryptionPublicKey choiceReturnCodesEncryptionPublicKey, the CCR encryption public key.
		 * @property {GqGroup} encryptionGroup encryptionGroup, the encryption group.
		 * @property {GroupVector<GqElement>} encodedVotingOptions encodedVotingOptions, the ballot encoded voting options.
		 * @property {string[]} actualVotingOptions actualVotingOptions, the ballot actual voting options.
		 * @property {string[]} correctnessInformationSelections ciSelections, the correctness information for selections.
		 * @property {string[]} correctnessInformationVotingOptions ciVotingOptions, the correctness information for voting options.
		 * @property {number} totalNumberOfWriteIns totalNumberOfWriteIns, the total number write-ins.
		 */
		return {
			electionPublicKey: electionPublicKey,
			choiceReturnCodesEncryptionPublicKey: choiceReturnCodesEncryptionPublicKey,
			encryptionGroup: encryptionParameters,
			encodedVotingOptions: encodedVotingOptions,
			actualVotingOptions: tokenResponse.verificationCard.actualVotingOptions,
			correctnessInformationSelections: tokenResponse.verificationCard.ciSelections,
			correctnessInformationVotingOptions: tokenResponse.verificationCard.ciVotingOptions,
			totalNumberOfWriteIns: tokenResponse.verificationCard.totalNumberOfWriteIns,
		};
	}

	/**
	 * @param {string[]} actualVotingOptions, the list of actual voting options. Must be non-null.
	 */
	function validateActualVotingOptions(actualVotingOptions) {
		checkNotNull(actualVotingOptions);
		actualVotingOptions.forEach(actualVotingOption => {
			checkArgument(0 < actualVotingOption.length && actualVotingOption.length <= MAX_SIZE,
				"The length of each actual voting option must be in between 1 and 50.");
			checkArgument(actualVotingOption.match(VALID_XML_TOKEN_PATTERN), "The actual voting options must match a valid xml xs:token.");
		});
	}

	return {
		parsePrimitivesParams: parsePrimitivesParams,
		validateActualVotingOptions: validateActualVotingOptions
	};

})();
