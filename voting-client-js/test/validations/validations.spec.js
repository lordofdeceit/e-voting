/*
 * (c) Copyright 2022 Swiss Post Ltd
 */

/* global OV */
/* jshint maxlen: 6666 */

const authResponse = require("../tools/data/authResponse.json");
const {validateUUID, validateBase32StringWithoutPadding, validateBase64String} = require("../../src/validations/validations");

describe('Validations methods', function () {

  it('validateUUID should validate and return a UUID', async function () {
    expect(() => validateUUID(authResponse.authenticationToken.voterInformation.electionEventId)).not.toThrow();
  });

  it('validateBase32StringWithoutPadding should validate and return base32 string', async function () {
    expect(() => validateBase32StringWithoutPadding('jwwjecjkixjo7r6mdfxcim7x', 24)).not.toThrow();
  });

  it('validateBase64String should validate and return a base64 string', async function () {
    expect(() => validateBase64String(authResponse.verificationCard.verificationCardKeystore)).not.toThrow();
  });
});


