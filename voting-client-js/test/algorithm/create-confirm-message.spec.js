/*
 * (c) Copyright 2022 Swiss Post Ltd
 */

/* global OV */
/* jshint maxlen: 6666 */

const primitivesParamsParser = require("../../src/parsers/primitives-params")
const createConfirmMessageAlgorithm = require("../../src/algorithm/create-confirm-message");
const authResponse = require('../tools/data/authResponse.json');
const verificationCardSecretKeyBytes = require("../tools/data/verificationCardSecretKeyBytes.json");
const {byteArrayToInteger} = require("crypto-primitives-ts/lib/cjs/conversions");
const {ZqElement} = require("crypto-primitives-ts/lib/cjs/math/zq_element");
const {ZqGroup} = require("crypto-primitives-ts/lib/cjs/math/zq_group");
const {ImmutableUint8Array} = require("crypto-primitives-ts/lib/cjs/immutable_uint8Array");

describe('Create confirm message algorithm', function () {

	const primitivesParams = primitivesParamsParser.parsePrimitivesParams(authResponse);
	const k_id = byteArrayToInteger(ImmutableUint8Array.from(verificationCardSecretKeyBytes));
	const verificationCardSecretKey = ZqElement.create(k_id, ZqGroup.sameOrderAs(primitivesParams.encryptionGroup));

	it('should generate a confirmation key', function () {

		const confirmationKey = createConfirmMessageAlgorithm.createConfirmMessage(
			{
				encryptionGroup: primitivesParams.encryptionGroup,
				electionEventId: authResponse.authenticationToken.voterInformation.electionEventId,
				characterLengthOfTheBallotCastingKey: 9
			},
			'712586677',
			verificationCardSecretKey
		);

		expect('748056306261241592909899823473213768367344014236780172678177848026050824431399517453586830362645095403535636630630276325687925060599517' +
			'03868474544474618287760108912086038589107931356372989372001545052915626264562136607147140877105479964368885721591877536420424292877781978738' +
			'39673649857540309708934621308014174747442170859392682622394076434847236952302307582648422026588420548721195575666631239663599440600675322873' +
			'88384174816105513614599378727073633873244807064478677982960255613401382320582874564637370798061153642264792088185355016660797558996857535889' +
			'70871087198340222795170173880597883962292970179370105759811117656279054841194132846927267416821182521958264602316333245434282878429168172868' +
			'06355582738627180136027412876282924815490319914473293944837037014613938469711827606098912685096676251453884033254383970627385004911069985799' +
			'53022154005817565913786783606947313519542847805014687208720928121224766514091063201264530')
			.toBe(confirmationKey.value.toString());
	});

	describe('should fail with', function () {

		const bck_length_error = "The ballot casting key length must be 9";
		const bck_numeric_error = "The ballot casting key must be a numeric value";

		const parameters = [
			{
				description: "a shorter ballot casting key",
				bck: "12345678",
				vcsk: verificationCardSecretKey,
				context: {
					encryptionGroup: primitivesParams.encryptionGroup,
					electionEventId: authResponse.authenticationToken.voterInformation.electionEventId,
					characterLengthOfTheBallotCastingKey: 9
				},
				error: new Error(bck_length_error)
			},
			{
				description: "a longer ballot casting key",
				bck: "1234567890",
				vcsk: verificationCardSecretKey,
				context: {
					encryptionGroup: primitivesParams.encryptionGroup,
					electionEventId: authResponse.authenticationToken.voterInformation.electionEventId,
					characterLengthOfTheBallotCastingKey: 9
				},
				error: new Error(bck_length_error)
			},
			{
				description: "a zero ballot casting key",
				bck: "000000000",
				vcsk: verificationCardSecretKey,
				context: {
					encryptionGroup: primitivesParams.encryptionGroup,
					electionEventId: authResponse.authenticationToken.voterInformation.electionEventId,
					characterLengthOfTheBallotCastingKey: 9
				},
				error: new Error("The ballot casting key must contain one non-zero element")
			},
			{
				description: "a non-numeric ballot casting key",
				bck: "1A3456789",
				vcsk: verificationCardSecretKey,
				context: {
					encryptionGroup: primitivesParams.encryptionGroup,
					electionEventId: authResponse.authenticationToken.voterInformation.electionEventId,
					characterLengthOfTheBallotCastingKey: 9
				},
				error: new Error(bck_numeric_error)
			},
			{
				description: "a space starting ballot casting key",
				bck: " 23456789",
				vcsk: verificationCardSecretKey,
				context: {
					encryptionGroup: primitivesParams.encryptionGroup,
					electionEventId: authResponse.authenticationToken.voterInformation.electionEventId,
					characterLengthOfTheBallotCastingKey: 9
				},
				error: new Error(bck_numeric_error)
			},
			{
				description: "a space ending ballot casting key",
				bck: "12345678 ",
				vcsk: verificationCardSecretKey,
				context: {
					encryptionGroup: primitivesParams.encryptionGroup,
					electionEventId: authResponse.authenticationToken.voterInformation.electionEventId,
					characterLengthOfTheBallotCastingKey: 9
				},
				error: new Error(bck_numeric_error)
			},
			{
				description: "a null ballot casting key",
				bck: null,
				vcsk: verificationCardSecretKey,
				context: {
					encryptionGroup: primitivesParams.encryptionGroup,
					electionEventId: authResponse.authenticationToken.voterInformation.electionEventId,
					characterLengthOfTheBallotCastingKey: 9
				},
				error: new Error()
			},
			{
				description: "a null verification card secret key",
				bck: '123456789',
				vcsk: null,
				context: {
					encryptionGroup: primitivesParams.encryptionGroup,
					electionEventId: authResponse.authenticationToken.voterInformation.electionEventId,
					characterLengthOfTheBallotCastingKey: 9
				},
				error: new Error()
			},
			{
				description: "a null encryption params",
				bck: 123456789,
				vcsk: verificationCardSecretKey,
				context: {
					encryptionGroup: null,
					electionEventId: authResponse.authenticationToken.voterInformation.electionEventId,
					characterLengthOfTheBallotCastingKey: 9
				},
				error: new Error()
			}
		];

		parameters.forEach((parameter) => {
			it(parameter.description, function () {
				expect(function () {
					createConfirmMessageAlgorithm.createConfirmMessage(
						parameter.context,
						parameter.bck,
						parameter.vcsk
					)
				}).toThrow(parameter.error);
			});
		});
	});

});


