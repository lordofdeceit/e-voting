/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */

const createVoteAlgorithm = require('../../src/algorithm/create-vote');
const {ElGamalMultiRecipientPublicKey} = require("crypto-primitives-ts/lib/cjs/elgamal/elgamal_multi_recipient_public_key");
const {ZqGroup} = require("crypto-primitives-ts/lib/cjs/math/zq_group");
const authResponse = require("../tools/data/authResponse.json");
const {getGqGroup} = require("../tools/data/group-test-data");
const GqGroupGenerator = require("../tools/generators/gq-group-generator");
const ZqGroupGenerator = require("../tools/generators/zq-group-generator");
const {ElGamalMultiRecipientCiphertext} = require("crypto-primitives-ts/lib/cjs/elgamal/elgamal_multi_recipient_ciphertext");
const {ExponentiationProof} = require("crypto-primitives-ts/lib/cjs/zeroknowledgeproofs/exponentiation_proof");
const {PlaintextEqualityProof} = require("crypto-primitives-ts/lib/cjs/zeroknowledgeproofs/plaintext_equality_proof");
const {GroupVector} = require("crypto-primitives-ts/lib/cjs/group_vector");
const primitivesParamsParser = require("../../src/parsers/primitives-params");

describe('Create vote algorithm', function () {
  'use strict';

  const primitivesParams = primitivesParamsParser.parsePrimitivesParams(authResponse);
  const gqGroupGenerator = new GqGroupGenerator(primitivesParams.encryptionGroup);

  const gqElementsEl = gqGroupGenerator.genRandomGqElements(2);
  const electionPublicKey = new ElGamalMultiRecipientPublicKey(gqElementsEl);

  const gqElementsCcr = gqGroupGenerator.genRandomGqElements(6);
  const choiceReturnCodesEncryptionPublicKey = new ElGamalMultiRecipientPublicKey(gqElementsCcr);

  const zqGroup = ZqGroup.sameOrderAs(primitivesParams.encryptionGroup);
  const zqGroupGenerator = new ZqGroupGenerator(zqGroup);
  const verificationCardSecretKey = zqGroupGenerator.genRandomZqElementMember();

  function getEncodedVotingOptionsList(length) {
    return gqGroupGenerator.getSmallPrimeMembers(length);
  }

  function getSelectedEncodedVotingOptions(encodedVotingOptions, length) {
    return GroupVector.from(encodedVotingOptions.slice(0, length));
  }

  function getEncodedVotingOptions(rawEncodedVotingOptions) {
    return GroupVector.from(rawEncodedVotingOptions);
  }

  function getEncodedWriteIns(length) {
    return new GroupVector(gqGroupGenerator.genRandomGqElements(length));
  }

  const encodedVotingOptionsList = getEncodedVotingOptionsList(9);

  function runCreateVote(testParameters = {}) {
    return createVoteAlgorithm.createVote(
      {
        encryptionGroup: primitivesParams.encryptionGroup,
        electionEventId: testParameters.electionEventId || authResponse.authenticationToken.voterInformation.electionEventId,
        numberOfSelectableVotingOptions: 2,
        encodedVotingOptions: getEncodedVotingOptions(encodedVotingOptionsList),
        actualVotingOptions: primitivesParams.actualVotingOptions,
      },
      testParameters.verificationCardId || authResponse.verificationCard.id,
      testParameters.selectedEncodedVotingOptions || getSelectedEncodedVotingOptions(encodedVotingOptionsList, 2),
      testParameters.encodedWriteIns || getEncodedWriteIns(1),
      testParameters.electionPublicKey || electionPublicKey,
      testParameters.choiceReturnCodesEncryptionPublicKey || choiceReturnCodesEncryptionPublicKey,
      testParameters.verificationCardSecretKey || verificationCardSecretKey
    );
  }

  it('must create a vote with correct information', function () {
    const createVoteResult = runCreateVote();
    const expectedResultInterface = {
      encryptedVote: ElGamalMultiRecipientCiphertext,
      encryptedPartialChoiceReturnCodes: ElGamalMultiRecipientCiphertext,
      exponentiatedEncryptedVote: ElGamalMultiRecipientCiphertext,
      exponentiationProof: ExponentiationProof,
      plaintextEqualityProof: PlaintextEqualityProof
    };

    Object.keys(expectedResultInterface).forEach(propertyName => {
      const expectedDataType = expectedResultInterface[propertyName];
      expect(createVoteResult[propertyName]).toEqual(jasmine.any(expectedDataType));
    });
  });


  describe('should fail with', function () {
    const otherGqGroup = getGqGroup();
    const otherGqGroupGenerator = new GqGroupGenerator(otherGqGroup);

    const context_group_EL_pk_group_error = "The encryption group of the context must equal the encryption group of the election public key.";
    const context_group_pk_CCR_group_error =
      "The encryption group of the context must equal the encryption group of the choice return codes encryption public key.";
    const EL_pk_k_id_group_error = "The encryption group of the context must equal the order of the verification card secret key.";
    const s_id_length_error = "The size of the vector of selected encoded voting options must be equal to the number of selectable voting options";

    it('a different group between context and election public key', function () {
      const differentGroupGqElements = otherGqGroupGenerator.genRandomGqElements(2);
      const differentGroupKey = new ElGamalMultiRecipientPublicKey(differentGroupGqElements);

      expect(() => runCreateVote({electionPublicKey: differentGroupKey}))
        .toThrow(new Error(context_group_EL_pk_group_error));
    });

    it('a different group between context and choice return codes encryption public key', function () {
      const differentGroupGqElements = otherGqGroupGenerator.genRandomGqElements(2);
      const differentGroupKey = new ElGamalMultiRecipientPublicKey(differentGroupGqElements);

      expect(() => runCreateVote({choiceReturnCodesEncryptionPublicKey: differentGroupKey}))
        .toThrow(new Error(context_group_pk_CCR_group_error));
    });

    it('a verification card secret key from a different group', function () {
      const otherZqGroup = ZqGroup.sameOrderAs(otherGqGroup);
      const otherZqGroupGenerator = new ZqGroupGenerator(otherZqGroup);
      const differentGroupZqElement = otherZqGroupGenerator.genRandomZqElementMember();

      expect(() => runCreateVote({verificationCardSecretKey: differentGroupZqElement}))
        .toThrow(new Error(EL_pk_k_id_group_error));
    });

    it('no selections', function () {
      expect(() => runCreateVote({selectedEncodedVotingOptions: getSelectedEncodedVotingOptions(encodedVotingOptionsList, 0)}))
        .toThrow(new Error(s_id_length_error));
    });

    it('more than phi selections', function () {
      expect(() => runCreateVote({selectedEncodedVotingOptions: getSelectedEncodedVotingOptions(encodedVotingOptionsList, 3)}))
        .toThrow(new Error(s_id_length_error));
    });

    it('duplicate selections', function () {
      const selectionWithDuplicates = [encodedVotingOptionsList[0], ...encodedVotingOptionsList];

      expect(() => runCreateVote({selectedEncodedVotingOptions: getSelectedEncodedVotingOptions(selectionWithDuplicates, 2)}))
        .toThrow(new Error("The voter cannot select the same option twice."));
    });

    it('negative selections', function () {
      const selectionWithNegativeElements = [-1 * encodedVotingOptionsList[0], encodedVotingOptionsList.slice(1)];

      expect(() => runCreateVote({selectedEncodedVotingOptions: getSelectedEncodedVotingOptions(selectionWithNegativeElements, 2)})).toThrow();
    });

    it('a zero selected', function () {
      const selectionWithAZero = [0, ...encodedVotingOptionsList];
      expect(() => runCreateVote({selectedEncodedVotingOptions: getSelectedEncodedVotingOptions(selectionWithAZero, 2)})).toThrow();
    });

    it('more write-ins than election public key elements', function () {
      const writeIns = getEncodedWriteIns(2);
      expect(() => runCreateVote({encodedWriteIns: writeIns})).toThrow();
    });

  });
});


