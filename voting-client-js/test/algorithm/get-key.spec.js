/*
 * (c) Copyright 2022 Swiss Post Ltd
 */

/* global OV */
/* jshint maxlen: 6666 */

const primitivesParamsParser = require("../../src/parsers/primitives-params");
const getKeyAlgorithm = require("../../src/algorithm/get-key");
const authResponse = require("../tools/data/authResponse-get-key.json");

describe('Get key algorithm', function () {

	it('should return a private key', async function () {
		const primitivesParams = primitivesParamsParser.parsePrimitivesParams(authResponse);

		const key = await getKeyAlgorithm.getKey(
			{
				encryptionGroup: primitivesParams.encryptionGroup,
				electionEventId: authResponse.authenticationToken.voterInformation.electionEventId,
				verificationCardSetId: authResponse.authenticationToken.voterInformation.verificationCardSetId,
				electionPublicKey: primitivesParams.electionPublicKey,
				choiceReturnCodesEncryptionPublicKey: primitivesParams.choiceReturnCodesEncryptionPublicKey,
				encodedVotingOptions: primitivesParams.encodedVotingOptions,
				actualVotingOptions: primitivesParams.actualVotingOptions,
				ciSelections: primitivesParams.correctnessInformationSelections,
				ciVotingOptions: primitivesParams.correctnessInformationVotingOptions,
				characterLengthOfTheStartVotingKey: 24
			},
			'4d65ej2adb4ia6ghhzb52kg6',
			authResponse.verificationCard.verificationCardKeystore,
			authResponse.verificationCard.id
		);

		expect(null).not.toBe(key);
	}, 30000);

});


