/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
/* global OV */

const primitivesParamsParser = require("../../src/parsers/primitives-params");
const authResponse = require('../tools/data/authResponse.json');

describe('Primitives params parser', function () {
	'use strict';

	it('should parse all the primitives', function () {
		expect(() => primitivesParamsParser.parsePrimitivesParams(authResponse)).not.toThrow();
	});

	it('should fail with invalid XML xs:token actual voting option', function () {
		const invalidXMLTokens = ['aposidvnbq13458zœ¶@¼←“þ“ ¢@]œ“→”@µ€ĸ@{þ', 'sfasdfa ', 'pk23]', 'asdacà!32', ' a p2o3m', '<xyz>'];

		const temp = authResponse.verificationCard.actualVotingOptions[0];
		invalidXMLTokens.forEach(invalidXMLToken => {
			authResponse.verificationCard.actualVotingOptions[0] = invalidXMLToken;
			expect(() => primitivesParamsParser.parsePrimitivesParams(authResponse))
				.toThrow(new Error("The actual voting options must match a valid xml xs:token."));
		});
		authResponse.verificationCard.actualVotingOptions[0] = temp;
	});

	it('should accept actual voting option when valid XML xs:token', function () {
		const validXMLTokens = ['asdfoublaj', 'vaner82', 'xyz', 'a_token', 'another-token'];

		const temp = authResponse.verificationCard.actualVotingOptions[0];
		validXMLTokens.forEach(validXMLToken => {
			authResponse.verificationCard.actualVotingOptions[0] = validXMLToken;
			expect(() => primitivesParamsParser.parsePrimitivesParams(authResponse)).not.toThrow();
		});
		authResponse.verificationCard.actualVotingOptions[0] = temp;
	});
});
