/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */

/* jshint node:true */
'use strict';

const {RandomService} = require("crypto-primitives-ts/lib/cjs/math/random_service");
const {ZqElement} = require("crypto-primitives-ts/lib/cjs/math/zq_element");
module.exports = function (zqGroup) {
    const random = new RandomService();

    function genRandomZqElementMember() {
        const value = random.genRandomInteger(zqGroup.q);
        return ZqElement.create(value, zqGroup);
    }

    return {
        genRandomZqElementMember,
    };

};
