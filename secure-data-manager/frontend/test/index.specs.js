/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */

import "angular";
import "angular-mocks";

const testsContext = require.context(".", true, /\.spec$/);
testsContext.keys().forEach(testsContext);
