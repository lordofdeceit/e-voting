/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */

import '../src/app/services/endpoints';
import '../src/app/services/voting-card-set';

describe("Voting card set service", function () {
	let _votingCardSetService, httpBackend;

	beforeEach(angular.mock.module("endpoints"));
	beforeEach(angular.mock.module("votingCardSet"));

    beforeEach(inject(function (votingCardSetService, $httpBackend) {
        _votingCardSetService = votingCardSetService;
        httpBackend = $httpBackend;
    }));

    it("should change status", function () {
        // Define a minimal voting card set.
		const votingCardSet = {
			id: "votingCard",
			electionEvent: {
				id: "electionEvent"
			},
			status: 'LOCKED'
		};
		// Attempt to change status.
        const endpoint = "/sdm-backend/setup/votingcardsets/electionevent/electionEvent/votingcardset/votingCard/precompute";
        const mockResponse = {
            data: {
                status: 'PRECOMPUTED'
            }
        };
        httpBackend.whenPUT(endpoint).respond(mockResponse);

		const endpointPrecomputed = 'setup/votingcardsets/electionevent/{electionEventId}/votingcardset/{votingCardSetId}/precompute';
        _votingCardSetService.changeStatus(endpointPrecomputed, votingCardSet).then(function (data) {
            expect(data).toEqual(mockResponse);
        });
        httpBackend.flush();
    });

});
