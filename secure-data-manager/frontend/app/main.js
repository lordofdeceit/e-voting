/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
const {app, BrowserWindow, Menu} = require('electron')
const {createLogger, format, transports} = require('winston');
const fs = require('fs');
const path = require('path');
const dateFormat = require('date-format');

// Logger config
const logDir = 'logs';
if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir);
}
const filename = path.join(logDir, 'sdm_' + dateFormat("yyyy-MM-dd-hhmmss", new Date()) + '.log');
const logFormat = format.printf(({level, message, timestamp}) => {
  return `${timestamp} [${level}] ${message}`;
});
const logger = createLogger({
  format: format.combine(
    format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss'
    }),
    logFormat
  ),
  transports: [new transports.File({filename})]
});

const frontendOnly = process.argv.length > 1 && process.argv[1] === "--fe";
const appPath = path.dirname(process.execPath);
let serverProcess;

if (!frontendOnly) {
  // Backend server process
  let platform = process.platform;

  if (platform === 'win32') {
    logger.log('info', 'Starting SDM application.');
    logger.log('info', "Working path: " + appPath);

    serverProcess = require('child_process').spawn('cmd.exe', ['/c', 'backend-server.bat startup'],
      {
        cwd: appPath
      });
  } else {
    logger.log('error', 'Non windows OS is currently not implemented.');
  }

  if (!serverProcess) {
    logger.log('error', 'Unable to start server from ' + appPath);
    app.quit();
    return;
  }

  serverProcess.stdout.on('message', function (data) {
    logger.log('info', 'Server message: ' + data);
  });

  logger.log('info', 'Backend server PID: ' + serverProcess.pid);
  logger.log('info', 'SDM application is started.');
}

let win;
const prepareWindow = function () {

  // Create the browser window.
  win = new BrowserWindow({
    show: false,
    width: 1350,
    height: 800,
    webPreferences: {
      plugins: true
    },
  });
  win.webContents.on('did-finish-load', () => win.setTitle(`Secure Data Manager (${app.getVersion()})`));
  const menu = Menu.buildFromTemplate([
    {
      label: 'File',
      submenu: [
        {
          label: 'Toggle developer tools', click() {
            win.webContents.toggleDevTools();
          },
          accelerator: 'F12'
        },
        {
          label: 'Exit', click() {
            app.quit();
          }
        }
      ]
    }
  ]);
  Menu.setApplicationMenu(menu);

  // Before window is closed.
  win.on('close', function (e) {
    if (serverProcess) {
      e.preventDefault(); // prevent the window close process.
      logger.log('info', 'Killing backend server PID: ' + serverProcess.pid);
      const shutdown = require('child_process').spawn('cmd.exe', ['/c', 'backend-server.bat shutdown'], {cwd: appPath});
      shutdown.on('exit', function () {
        logger.log('info', "Backend server process is killed.");
        serverProcess = null;
        win.close(); // recall window close method.
      })
    }
  });

  // Event when the window is closed.
  win.on('closed', function () {
    win = null
    app.quit();
    logger.log('info', 'Application is closed.');
  });
};

app.on('ready', function () {
  prepareWindow();
  win.loadURL(`file://${__dirname}/index.html`);
  win.show();
});
