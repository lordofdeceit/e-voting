/*
 * (c) Copyright 2022 Swiss Post Ltd
 */

(function () {
	'use strict';

	angular
		.module('membersPasswords', [])
		.controller('membersPasswords', function (
			$scope,
			$mdDialog,
			endpoints,
			$http,
			$interval,
			$timeout,
			$mdToast,
			gettextCatalog,
			toastCustom,
		) {
			const electionEventIdPattern = '{electionEventId}';
			const electoralAuthorityIdPattern = '{electoralAuthorityId}';
			const memberIndexPattern = '{memberIndex}';
			const membersPasswordsValues = [];

			$scope.selectedMember = {};
			$scope.membersStatusDone = {};
			$scope.numberOfPasswordsSet = 0;
			$scope.isPasswordHasBeenValidated = false;
			$scope.isPasswordVisible = false;
			$scope.isPasswordLengthError = false;
			$scope.isPasswordDigitError = false;
			$scope.isPasswordSpecialError = false;
			$scope.isPasswordLowercaseError = false;
			$scope.isPasswordUppercaseError = false;
			$scope.isMemberInSelectionProcess = false;

			// Two step dialog closing handling
			$scope.twoStepsClosing = false;
			$scope.closeDialog = () => $scope.twoStepsClosing = true;
			$scope.exitDialog = () => $mdDialog.cancel();
			$scope.discardClose = () => $scope.twoStepsClosing = false;

			$scope.selectMember = function (member) {
				$scope.selectedMember = member;
				clearPassword();
				$scope.isMemberInSelectionProcess = false;
				setInputFocus('1', 500);
			};

			// Passwords input keypress
			$scope.pwd1_keypress = function (ev) {
				if (isEmptyPassword()) {
					return;
				}
				if (ev && ev.which === 13 && checkPasswordPolicy()) {
					if ($scope.isMode('setup')) {
						setInputFocus(2);
					} else {
						$scope.validatePassword();
					}
				}
			};
			$scope.pwd2_keypress = function (ev) {
				const confirm = $scope.confirmPassword.value;
				if (isEmptyPassword() || confirm == null || confirm.length === 0) {
					return;
				}
				if (ev && ev.which === 13) {
					if (checkPasswordsMatch()) {
						$scope.validatePassword();
					}
				}
			};

			$scope.togglePasswordVisibility = function () {
				$scope.isPasswordVisible = !$scope.isPasswordVisible;
			}

			// Password validation
			$scope.validatePassword = function () {
				if (isEmptyPassword() || $scope.isMemberInSelectionProcess) {
					return;
				}
				if ($scope.isMode('setup')) {
					if (checkPasswordPolicy() && checkPasswordsMatch()) {
						setMemberStatusToDone();
					}
				} else {
					if (checkPasswordPolicy()) {
						validatePasswordHash();
					}
				}
			}

			$scope.isMode = function (modeValue) {
				return $scope.membersPasswordsMode === modeValue;
			}

			function initialize() {
				clearPassword();
				// Initialize members status
				$scope.listOfElectoralAuthorityMembers.forEach(function (member) {
					$scope.membersStatusDone[member] = false;
				});
				// Select first member
				$scope.selectMember($scope.listOfElectoralAuthorityMembers[0]);
			}

			// Initializing the status of each member
			initialize();

			function checkPasswordPolicy() {
				const pwd = $scope.password.value;
				let isValid = true;

				// Length validation
				const lengthValid = /^(.{12,64}$)/gm.test(pwd);
				isValid &&= lengthValid;
				$scope.isPasswordLengthError = !lengthValid;

				// Digit validation
				const digitValid = /^(?=.*\d).+$/gm.test(pwd);
				isValid &&= digitValid;
				$scope.isPasswordDigitError = !digitValid;

				// Lowercase validation
				const lowerValid = /^(?=.*[a-z]).+$/gm.test(pwd);
				isValid &&= lowerValid;
				$scope.isPasswordLowercaseError = !lowerValid;

				// Uppercase validation
				const upperValid = /^(?=.*[A-Z]).+$/gm.test(pwd);
				isValid &&= upperValid;
				$scope.isPasswordUppercaseError = !upperValid;

				// Special validation
				const specialValid = /^(?=.*[^a-zA-Z0-9]).+$/gm.test(pwd);
				isValid &&= specialValid;
				$scope.isPasswordSpecialError = !specialValid;

				if (isValid) {
					$scope.isSelectedMemberPasswordValid = true;
				} else {
					$scope.isSelectedMemberPasswordValid = false;
					const errorToast = 'Your password does not satisfy the policy.';
					toast(errorToast, 'error');
				}

				$scope.isPasswordHasBeenValidated = true;
				return isValid;
			}

			function checkPasswordsMatch() {
				if ($scope.password.value === $scope.confirmPassword.value) {
					$scope.isSelectedMemberPasswordMatch = true;
					return true;
				} else {
					$scope.isSelectedMemberPasswordMatch = false;
					toast(gettextCatalog.getString('The passwords do not match'), 'error');
					return false;
				}
			}

			function validatePasswordHash() {
				const url = (endpoints.host() + endpoints.electoralAuthorityPasswordValidate)
					.replace(electionEventIdPattern, $scope.selectedElectionEventId)
					.replace(electoralAuthorityIdPattern, $scope.selectedAuthority.id)
					.replace(memberIndexPattern, $scope.listOfElectoralAuthorityMembers.indexOf($scope.selectedMember).toString());

				$scope.isMemberInSelectionProcess = true;
				$http.post(url, [...$scope.password.value]).then(
					() => setMemberStatusToDone(),
					() => {
						toast('The password is wrong', 'error');
						$scope.isMemberInSelectionProcess = false;
					}
				);
			}

			function setMemberStatusToDone() {
				if (isEmptyPassword()) {
					return;
				}
				const index = $scope.listOfElectoralAuthorityMembers.indexOf($scope.selectedMember);
				membersPasswordsValues[index] = $scope.password.value;
				$scope.membersStatusDone[$scope.selectedMember] = true;
				$scope.numberOfPasswordsSet++;

				clearPassword();

				if ($scope.numberOfPasswordsSet === $scope.listOfElectoralAuthorityMembers.length) {
					$timeout(() => $mdDialog.hide(membersPasswordsValues), 1000);
				} else {
					$timeout(() => {
						if (index < $scope.listOfElectoralAuthorityMembers.length - 1) {
							if (!$scope.membersStatusDone[$scope.listOfElectoralAuthorityMembers[index + 1]]) {
								$scope.selectMember($scope.listOfElectoralAuthorityMembers[index + 1]);
							} else {
								selectNextMember();
							}
						} else {
							selectNextMember();
						}
					}, 1000);
				}
			}

			function selectNextMember() {
				for (const element of $scope.listOfElectoralAuthorityMembers) {
					if (!$scope.membersStatusDone[element]) {
						$scope.selectMember(element);
						break;
					}
				}
			}

			function setInputFocus(x, delay) {
				delay = delay || 100;
				$timeout(() => document.getElementById('pwd' + x).focus(), delay);
			}

			function isEmptyPassword() {
				const pwd = $scope.password.value;
				return pwd == null || pwd.length === 0;
			}

			function clearPassword() {
				$scope.password = {
					value: '',
				};
				$scope.confirmPassword = {
					value: '',
				};
				$scope.isSelectedMemberPasswordValid = false;
				$scope.isSelectedMemberPasswordMatch = false;
				$scope.isPasswordVisible = false;
				$scope.isPasswordHasBeenValidated = false;
				$scope.isPasswordLengthError = false;
				$scope.isPasswordDigitError = false;
				$scope.isPasswordSpecialError = false;
				$scope.isPasswordLowercaseError = false;
				$scope.isPasswordUppercaseError = false;
			}

			function toast(text, type) {
				$mdToast.show(
					toastCustom.topCenter(text, type),
				);
			}
		});
})();
