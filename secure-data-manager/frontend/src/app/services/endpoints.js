/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
/* jshint maxlen: 666 */
/* global process */

angular
	.module('endpoints', [])

	.factory('endpoints', function () {

		function isElectron() {
			'use strict';
			var userAgent = navigator.userAgent.toLowerCase();
			return userAgent.indexOf(' electron/') > -1;
		}

		function host() {
			'use strict';
			return isElectron() ? 'http://localhost:8090/sdm-backend/' : '/sdm-backend/';
		}

		return {
			host: host,
			status: 'status',
			close: 'close',
			sdmConfig: 'sdm-config',
			electionEvents: 'electionevents',
			administrationBoards: 'adminboards',
			constituteAdminBoard: 'adminboards/constitute/{adminBoardId}',
			checkAdminBoardShareStatus: 'adminboards/shares/status',
			writeAdminBoardShare: 'adminboards/{adminBoardId}/shares/{shareNum}',
			activateAdminBoardShare: 'adminboards/{adminBoardId}/activate',
			readAdminBoardShare: 'adminboards/{adminBoardId}/read/{shareNum}',
			reconstructAdminBoardShare: 'adminboards/{adminBoardId}/reconstruct',
			electionEvent: 'electionevents/{electionEventId}',
			requestKeys: 'electionevents/{electionEventId}/keys',
			existingCCKeys: 'electionevents/{electionEventId}/existingCCKeys',
			preconfiguration: 'preconfiguration',
			ballots: 'ballots/electionevent/{electionEventId}',
			ballottexts:
				'ballottexts/electionevent/{electionEventId}/ballottext/{ballotId}',
			ballotSign: 'ballots/electionevent/{electionEventId}/ballot/{ballotId}', // PUT
			votingCardSets: 'votingcardsets/electionevent/{electionEventId}',
			votingCardSetPrecompute:
				'setup/votingcardsets/electionevent/{electionEventId}/votingcardset/{votingCardSetId}/precompute',
			votingCardSetCompute:
				'online/votingcardsets/electionevent/{electionEventId}/votingcardset/{votingCardSetId}/compute',
			votingCardSetDownload:
				'online/votingcardsets/electionevent/{electionEventId}/votingcardset/{votingCardSetId}/download',
			votingCardSetSign:
				'setup/votingcardsets/electionevent/{electionEventId}/votingcardset/{votingCardSetId}/sign', // PUT
			votingCardSet:
				'votingcardsets/electionevent/{electionEventId}/votingcardset/{votingCardSetId}',
			electoralAuthorities: 'electoralauthorities/electionevent/{electionEventId}',
			electoralAuthoritySign: 'electoralauthorities/electionevent/{electionEventId}/electoralauthority/{electoralAuthorityId}', // PUT
			electoralAuthorityConstitute: 'electoralauthorities/electionevent/{electionEventId}/electoralauthority/{electoralAuthorityId}/constitute',
			electoralAuthorityPasswordValidate: 'electoralauthorities/electionevent/{electionEventId}/electoralauthority/{electoralAuthorityId}/index/{memberIndex}/validate',
			ballotboxes: 'ballotboxes/electionevent/{electionEventId}',
			ballotBoxSign: 'ballotboxes/electionevent/{electionEventId}/ballotbox/{ballotBoxId}', // PUT
			mixingOnline: 'mixing/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/mixonline',
			updateMixingStatus: 'mixing/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/updateStatus',
			downloadBallotBox: 'mixing/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/download',
			mixingOffline: 'mixing/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/mixoffline',
			synchronizeVoterPortal: 'configurations',
			synchronizeVoterPortalEEID:
				'configurations/electionevent/{electionEventId}',
			updateComputationStatus:
				'choicecodes/electionevent/{electionEventId}/status',
			export: 'operation/export/{electionEventId}',
			import: 'operation/import',
			generateTallyFiles:
				'operation/electionevent/{electionEventId}/tally-files/generate',
			generatePrintFiles:
				'operation/electionevent/{electionEventId}/print-files/generate',
			languages: 'sdm-config/langs/'
		};
	});
