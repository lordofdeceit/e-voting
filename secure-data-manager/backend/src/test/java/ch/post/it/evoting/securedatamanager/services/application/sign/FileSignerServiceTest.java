/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.sign;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import ch.post.it.evoting.cryptolib.asymmetric.service.AsymmetricService;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.securedatamanager.services.application.service.AliasSignatureConstants;

@DisplayName("Use MetadataFileSigner to")
class FileSignerServiceTest {

	private static final Map<String, String> SIGNED_FIELDS = new LinkedHashMap<>();
	private final static byte[] SOURCE_BYTES = "File content".getBytes(StandardCharsets.UTF_8);
	private final static String PRIVATE_KEY_MODULUS = "4699471975944807277640274632667041607974181783597611062133845835672950056248879393979440525734100712313666093533521680662204311445153665136319856175785932702766077312568623845743247777176094554054668587689765354726449141684182138640484707822934212030992715425921712282192734419034979036256564652427300897118140316960670103373764278797195554205345276024304043643021492712802257131263003372834356353258442554524342814413292050755257306212302137362202242525766135592072045514036314504462889514919340349221085881472753709933128528396362600527515616077315692941501398748324279241436735036277939382112998047710465468805339217991646084737538670497520185714404155149704977449681375214181154910533853468403901624890535224149516659913890242493726128428942141271606656625113134263443037500804054034887957321167540999280172384049881981510475731402386375757384606493601901704346108100398341089465413955106566507828301617064091115700048483";
	private final static String PRIVATE_KEY_EXPONENT = "1789416259879376431784705331658295158554583202142096605734456198884609185325276726689655440429553019293397325649362448389230167538307935008381431114311763700370417875345129068443777967492269276121232697125352847162994569573796835050490190905403234724314627112870033250621459497075062702904686552924257751150260654891054550782620515056298220202592256138494213300727525518434226857837597352142987100681871381462039958531463089256024359489675964520882491739158506347926511702747753071858572379381122632686521653830678034194404520486609055856791960532775923856185232922365407324368654827155142506961671916200719557534407234180136403701313452951689087471472024216466102674410735220075707217527909400446233462142623737087215688156476668638188771756018835621401247113028574811595858696678864901354541519978849166396840308889035150840492608527083262030400341630056583594095190530320134825383040743831215440239854349419184545437123281";

	private final AsymmetricService asymmetricService = new AsymmetricService();
	private final Hash hash = HashFactory.createHash();

	private PrivateKey privateKey;
	private FileSignerService fileSignerService;

	@BeforeEach
	void setUp() throws NoSuchAlgorithmException, InvalidKeySpecException {
		fileSignerService = new FileSignerService(asymmetricService, hash);

		SIGNED_FIELDS.put(AliasSignatureConstants.TIMESTAMP, DateTimeFormatter.ISO_INSTANT.format(Instant.ofEpochSecond(1550000001)));
		SIGNED_FIELDS.put(AliasSignatureConstants.COMPONENT, "Secure Data Manager");

		final KeyFactory fact = KeyFactory.getInstance("RSA");
		final RSAPrivateKeySpec privateKeySpec = new RSAPrivateKeySpec(new BigInteger(PRIVATE_KEY_MODULUS), new BigInteger(PRIVATE_KEY_EXPONENT));
		privateKey = fact.generatePrivate(privateKeySpec);
	}

	@Test
	@DisplayName("create a file signature")
	void createSignatureTest(
			@TempDir
			final Path tempDir) throws IOException {

		final Path sourceFile = Files.write(tempDir.resolve("sourceFile"), SOURCE_BYTES);

		assertDoesNotThrow(() -> fileSignerService.createSignature(privateKey, sourceFile, SIGNED_FIELDS));
	}

	@Test
	@DisplayName("create a file signature with null or invalid parameters")
	void createSignatureWithNullParametersTest(
			@TempDir
			final Path tempDir) throws IOException {

		final Path sourceFile = Files.write(tempDir.resolve("sourceFile"), SOURCE_BYTES);
		final Map<String, String> emptyMap = new LinkedHashMap<>();

		assertAll(() -> assertThrows(NullPointerException.class, () -> fileSignerService.createSignature(null, sourceFile, SIGNED_FIELDS)),
				() -> assertThrows(NullPointerException.class, () -> fileSignerService.createSignature(privateKey, null, SIGNED_FIELDS)),
				() -> assertThrows(IllegalArgumentException.class, () -> fileSignerService.createSignature(privateKey, sourceFile, emptyMap)));
	}

}
