/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.mixoffline;

import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.GqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.PlaintextEqualityProof;
import ch.post.it.evoting.domain.ContextIds;
import ch.post.it.evoting.domain.EncryptedVerifiableVote;
import ch.post.it.evoting.securedatamanager.commons.Constants;

class VerifyVotingClientProofsInputTest {

	private static final SecureRandom SRAND = new SecureRandom();
	private static final int PSI = SRAND.nextInt(2, MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS + 1);
	private static final int PHI = PSI;
	private static final int DELTA_HAT = 1;
	private static final int DELTA = 2;
	private static final int N_C = 2;

	private final Random rand = RandomFactory.createRandom();
	private final GqGroup gqGroup = GroupTestData.getGqGroup();
	private final GqGroup gqGroupDifferent = GroupTestData.getDifferentGqGroup(gqGroup);
	private final GqGroupGenerator gqGroupGeneratorDifferent = new GqGroupGenerator(gqGroupDifferent);
	private final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
	private final GqGroupGenerator gqGroupGenerator = new GqGroupGenerator(gqGroup);
	private final ElGamalMultiRecipientPublicKey electionPublicKey = new ElGamalMultiRecipientPublicKey(
			gqGroupGenerator.genRandomGqElementVector(DELTA));
	private final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey = new ElGamalMultiRecipientPublicKey(
			gqGroupGenerator.genRandomGqElementVector(PHI));
	private final ZqGroupGenerator zqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(gqGroup));
	private final ExponentiationProof exponentiationProof = new ExponentiationProof(zqGroupGenerator.genRandomZqElementMember(),
			zqGroupGenerator.genRandomZqElementMember());
	private final PlaintextEqualityProof plaintextEqualityProof = new PlaintextEqualityProof(zqGroupGenerator.genRandomZqElementMember(),
			zqGroupGenerator.genRandomZqElementVector(2));
	private final ContextIds contextIds1 = new ContextIds(rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase(),
			rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase(),
			rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase());
	private final ContextIds contextIds2 = new ContextIds(contextIds1.electionEventId(),
			rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase(),
			rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase());
	private final List<EncryptedVerifiableVote> encryptedVerifiableVotes = List.of(
			new EncryptedVerifiableVote(contextIds1, elGamalGenerator.genRandomCiphertext(DELTA_HAT), elGamalGenerator.genRandomCiphertext(1),
					elGamalGenerator.genRandomCiphertext(PSI), exponentiationProof, plaintextEqualityProof),
			new EncryptedVerifiableVote(contextIds2, elGamalGenerator.genRandomCiphertext(DELTA_HAT), elGamalGenerator.genRandomCiphertext(1),
					elGamalGenerator.genRandomCiphertext(PSI), exponentiationProof, plaintextEqualityProof));
	private final Map<String, ElGamalMultiRecipientPublicKey> verificationCardPublicKeys = new HashMap<>();

	@BeforeEach
	void setUp() {
		verificationCardPublicKeys.put(contextIds1.verificationCardId(),
				new ElGamalMultiRecipientPublicKey(gqGroupGenerator.genRandomGqElementVector(N_C)));
		verificationCardPublicKeys.put(contextIds2.verificationCardId(),
				new ElGamalMultiRecipientPublicKey(gqGroupGenerator.genRandomGqElementVector(N_C)));
	}

	@Test
	@DisplayName("Valid parameters do not throw")
	void validParametersTest() {

		final VerifyVotingClientProofsInput input = assertDoesNotThrow(() -> new VerifyVotingClientProofsInput.Builder()
				.setEncryptedVerifiableVotes(encryptedVerifiableVotes)
				.setVerificationCardPublicKeys(verificationCardPublicKeys)
				.setElectionPublicKey(electionPublicKey)
				.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
				.build());

		assertEquals(input.getVerificationCardPublicKeys(), verificationCardPublicKeys, "Input verificationCardPublicKeys not equal");
		assertEquals(input.getElectionPublicKey(), electionPublicKey, "Input electionPublicKey not equal");
		assertEquals(input.getChoiceReturnCodesEncryptionPublicKey(), choiceReturnCodesEncryptionPublicKey,
				"Input choiceReturnCodesEncryptionPublicKey not equal");
	}

	@Nested
	@DisplayName("Throws NullPointerException when")
	class NullArgumentTest {

		@Test
		@DisplayName("all arguments are null")
		void constructWithoutArguments() {
			final VerifyVotingClientProofsInput.Builder builder = new VerifyVotingClientProofsInput.Builder();

			final NullPointerException ex = assertThrows(NullPointerException.class, builder::build);

			assertNull(ex.getMessage());
		}

		@Test
		@DisplayName("encryptedVerifiableVotes is null")
		void constructWithNullEncryptedVerifiableVotes() {
			final VerifyVotingClientProofsInput.Builder builder = new VerifyVotingClientProofsInput.Builder()
					.setEncryptedVerifiableVotes(null)
					.setVerificationCardPublicKeys(verificationCardPublicKeys)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey);

			final NullPointerException ex = assertThrows(NullPointerException.class, builder::build);

			assertNull(ex.getMessage());
		}

		@Test
		@DisplayName("verificationCardPublicKeys is null")
		void constructWithNullVerificationCardPublicKeys() {
			final VerifyVotingClientProofsInput.Builder builder = new VerifyVotingClientProofsInput.Builder()
					.setEncryptedVerifiableVotes(encryptedVerifiableVotes)
					.setVerificationCardPublicKeys(null)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey);

			final NullPointerException ex = assertThrows(NullPointerException.class, builder::build);

			assertNull(ex.getMessage());
		}

		@Test
		@DisplayName("electionPublicKey is null")
		void constructWithNullElectionPublicKey() {
			final VerifyVotingClientProofsInput.Builder builder = new VerifyVotingClientProofsInput.Builder()
					.setEncryptedVerifiableVotes(encryptedVerifiableVotes)
					.setVerificationCardPublicKeys(verificationCardPublicKeys)
					.setElectionPublicKey(null)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey);

			final NullPointerException ex = assertThrows(NullPointerException.class, builder::build);

			assertNull(ex.getMessage());
		}

		@Test
		@DisplayName("choiceReturnCodesEncryptionPublicKey is null")
		void constructWithNullChoiceReturnCodesEncryptionPublicKey() {
			final VerifyVotingClientProofsInput.Builder builder = new VerifyVotingClientProofsInput.Builder()
					.setEncryptedVerifiableVotes(encryptedVerifiableVotes)
					.setVerificationCardPublicKeys(verificationCardPublicKeys)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(null);

			final NullPointerException ex = assertThrows(NullPointerException.class, builder::build);

			assertNull(ex.getMessage());
		}
	}

	@Nested
	@DisplayName("Throws IllegalArgumentException when")
	class EmptyListsArgumentTest {

		@Test
		@DisplayName("encryptedVerifiableVotes is empty")
		void constructWithEmptyEncryptedVerifiableVotes() {
			final VerifyVotingClientProofsInput.Builder builder = new VerifyVotingClientProofsInput.Builder()
					.setEncryptedVerifiableVotes(List.of())
					.setVerificationCardPublicKeys(verificationCardPublicKeys)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey);

			final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, builder::build);

			assertEquals("The list of encrypted verifiable votes must not be empty.", ex.getMessage());
		}

		@Test
		@DisplayName("verificationCardPublicKeys is empty")
		void constructWithEmptyVerificationCardPublicKeys() {
			final VerifyVotingClientProofsInput.Builder builder = new VerifyVotingClientProofsInput.Builder()
					.setEncryptedVerifiableVotes(encryptedVerifiableVotes)
					.setVerificationCardPublicKeys(Map.of())
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey);

			final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, builder::build);

			assertEquals("The map of verification card public keys must not be empty.", ex.getMessage());
		}

		@Test
		@DisplayName("electionPublicKey has different group")
		void constructWithDifferentGroupElectionPublicKey() {
			final ElGamalMultiRecipientPublicKey electionPublicKeyDifferent = new ElGamalMultiRecipientPublicKey(
					gqGroupGeneratorDifferent.genRandomGqElementVector(DELTA));

			final VerifyVotingClientProofsInput.Builder builder = new VerifyVotingClientProofsInput.Builder()
					.setEncryptedVerifiableVotes(encryptedVerifiableVotes)
					.setVerificationCardPublicKeys(verificationCardPublicKeys)
					.setElectionPublicKey(electionPublicKeyDifferent)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey);

			final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, builder::build);

			assertEquals("All input must have the same group.", ex.getMessage());
		}

		@Test
		@DisplayName("choiceReturnCodesEncryptionPublicKey has different group")
		void constructWithDifferentGroupChoiceReturnCodesEncryptionPublicKey() {
			final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKeyDifferent = new ElGamalMultiRecipientPublicKey(
					gqGroupGeneratorDifferent.genRandomGqElementVector(PHI));

			final VerifyVotingClientProofsInput.Builder builder = new VerifyVotingClientProofsInput.Builder()
					.setEncryptedVerifiableVotes(encryptedVerifiableVotes)
					.setVerificationCardPublicKeys(verificationCardPublicKeys)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKeyDifferent);

			final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, builder::build);

			assertEquals("All input must have the same group.", ex.getMessage());
		}

		@Test
		@DisplayName("N_E < N_C")
		void constructWithDifferentSizeVerificationCardPublicKeys() {
			final Map<String, ElGamalMultiRecipientPublicKey> verificationCardPublicKeysM1 = new HashMap<>();
			verificationCardPublicKeysM1.put(contextIds1.verificationCardId(),
					new ElGamalMultiRecipientPublicKey(gqGroupGenerator.genRandomGqElementVector(DELTA)));

			final VerifyVotingClientProofsInput.Builder builder = new VerifyVotingClientProofsInput.Builder()
					.setEncryptedVerifiableVotes(encryptedVerifiableVotes)
					.setVerificationCardPublicKeys(verificationCardPublicKeysM1)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey);

			final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, builder::build);

			assertEquals(String.format("N_E should be greater or equal to N_C. [N_E: %s, N_C: %s]", verificationCardPublicKeysM1.size(), N_C),
					ex.getMessage());
		}
	}
}