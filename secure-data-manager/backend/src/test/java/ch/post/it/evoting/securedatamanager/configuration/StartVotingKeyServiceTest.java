/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@DisplayName("An StartVotingKeyFileRepository")
class StartVotingKeyServiceTest {

	private final Random random = RandomFactory.createRandom();

	private final String ELECTION_EVENT_ID = random.genRandomBase16String(32).toLowerCase();
	private final String VERIFICATION_CARD_SET_ID = random.genRandomBase16String(32).toLowerCase();
	private final String VERIFICATION_CARD_ID = random.genRandomBase16String(32).toLowerCase();

	private Path storePath;

	private StartVotingKeyService startVotingKeyService;

	@BeforeEach
	void setup(
			@TempDir
			final Path tempDir) {
		final PathResolver pathResolver = new PathResolver(tempDir.toString());

		storePath = pathResolver.resolveOfflinePath(ELECTION_EVENT_ID)
				.resolve(Constants.CONFIG_START_VOTING_KEYS_DIRECTORY)
				.resolve(VERIFICATION_CARD_SET_ID);

		storePath.toFile().mkdirs();

		startVotingKeyService = new StartVotingKeyService(new StartVotingKeyFileRepository(pathResolver));
	}

	@Test
	void saveTest() throws IOException {
		final String KEY_VALUE = "key-value";
		final Path startVotingKeyFile = storePath.resolve(VERIFICATION_CARD_ID + Constants.KEY);

		startVotingKeyService.save(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID, KEY_VALUE);

		assertTrue(storePath.toFile().isDirectory(), "storePath of StartVotingKey is not a directory");
		assertTrue(startVotingKeyFile.toFile().isFile(), "file of saved StartVotingKey not a file");

		final String value = Files.readString(startVotingKeyFile);

		assertEquals(KEY_VALUE, value, "saved StartVotingKey is invalid");
	}

	@Test
	void findByIdTest() throws IOException {
		final String KEY_VALUE = "key-value";
		final Path startVotingKeyFile = storePath.resolve(VERIFICATION_CARD_ID + Constants.KEY);

		Files.writeString(startVotingKeyFile, KEY_VALUE);

		final String key = startVotingKeyService.load(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID);

		assertEquals(KEY_VALUE, key, "Invalid saved StartVotingKey found");
	}

	@Test
	void loadTest() throws IOException {
		final String KEY_VALUE = "key-value";
		final Path startVotingKeyFile = storePath.resolve(VERIFICATION_CARD_ID + Constants.KEY);

		Files.writeString(startVotingKeyFile, KEY_VALUE);

		final String key = startVotingKeyService.load(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID);

		assertEquals(KEY_VALUE, key, "Invalid saved StartVotingKey found");
	}

	@Test
	void saveAndLoadTest() {
		final String KEY_VALUE = "key-value";

		startVotingKeyService.save(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID, KEY_VALUE);

		final String key = startVotingKeyService.load(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID);

		assertEquals(KEY_VALUE, key, "Invalid saved StartVotingKey found");
	}

}
