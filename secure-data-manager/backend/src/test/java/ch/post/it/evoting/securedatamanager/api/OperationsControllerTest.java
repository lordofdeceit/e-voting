/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;

import ch.post.it.evoting.securedatamanager.ControlComponentPublicKeysService;
import ch.post.it.evoting.securedatamanager.services.application.service.ElectionEventService;
import ch.post.it.evoting.securedatamanager.services.domain.model.operation.OperationResult;
import ch.post.it.evoting.securedatamanager.services.domain.model.operation.OperationsData;
import ch.post.it.evoting.securedatamanager.services.domain.model.operation.OperationsOutputCode;
import ch.post.it.evoting.securedatamanager.services.infrastructure.importexport.ImportExportService;

@ExtendWith(MockitoExtension.class)
class OperationsControllerTest {

	private static final String ERROR = "Error";
	private static final String TEST_PATH = "testPath";
	private static final String ELECTION_EVENT_ALIAS = "eeAlias";
	private static final String ELECTION_EVENT_ID = "a9d805a0d9ef4deeb4b14838f7e8ed8a";
	@Mock
	private ImportExportService importExportService;
	@Mock
	private ElectionEventService electionEventService;
	@Mock
	private ControlComponentPublicKeysService controlComponentPublicKeysService;

	@InjectMocks
	private OperationsController operationsController;

	@Test
	void testExportOperationPathParameterIsRequired() {
		final ResponseEntity<byte[]> exportOperation = operationsController.exportOperation(ELECTION_EVENT_ID, new OperationsData());

		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, exportOperation.getStatusCode());
	}

	@Test
	void testExportOperationElectionEventNotFound() {
		// given
		final String electionEventId = "17ccbe962cf341bc93208c26e911090c";
		when(controlComponentPublicKeysService.exist(anyString())).thenReturn(true);

		// when
		final ResponseEntity<byte[]> exportOperation = operationsController.exportOperation(electionEventId, new OperationsData());

		// then
		assertEquals(HttpStatus.NOT_FOUND, exportOperation.getStatusCode());
	}

	@Test
	void testExportOperationCCKeysMissing() {
		when(controlComponentPublicKeysService.exist(anyString())).thenReturn(false);

		final OperationsData requestBody = new OperationsData();
		requestBody.setPath(TEST_PATH);
		final ResponseEntity<byte[]> exportOperation = operationsController.exportOperation(ELECTION_EVENT_ID, requestBody);

		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, exportOperation.getStatusCode());
	}

	@Test
	void testExportOperationErrorInService() {
		final OperationsData requestBody = new OperationsData();
		doThrow(new IllegalStateException("test")).when(importExportService).exportSdmData(anyString(), any());

		final ResponseEntity<byte[]> importOperation = operationsController.exportOperation(ELECTION_EVENT_ID, requestBody);

		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, importOperation.getStatusCode());
	}

	@Test
	void testExportOperationHappyPath() {
		final OperationsData requestBody = new OperationsData();

		when(controlComponentPublicKeysService.exist(anyString())).thenReturn(true);
		when(electionEventService.getElectionEventAlias(ELECTION_EVENT_ID)).thenReturn(ELECTION_EVENT_ALIAS);

		final ResponseEntity<byte[]> exportOperation = operationsController.exportOperation(ELECTION_EVENT_ID, requestBody);

		assertEquals(HttpStatus.OK, exportOperation.getStatusCode());
		assertNull(exportOperation.getBody());
	}

	@Test
	void testImportOperationPathParameterMissing() {
		final MockMultipartFile multipartFile = new MockMultipartFile("test", (byte[]) null);
		final ResponseEntity<OperationResult> importOperation = operationsController.importOperation(multipartFile);

		assertEquals(HttpStatus.BAD_REQUEST, importOperation.getStatusCode());
		assertEquals(OperationsOutputCode.MISSING_PARAMETER.value(), importOperation.getBody().getError());
	}

	@Test
	void testImportOperationErrorInService() {
		doThrow(new IllegalStateException("test")).when(importExportService).importSdmData(any());

		final MockMultipartFile multipartFile = new MockMultipartFile("test", new byte[] { 1, 2, 3, 4 });
		final ResponseEntity<OperationResult> importOperation = operationsController.importOperation(multipartFile);

		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, importOperation.getStatusCode());
		assertEquals(OperationsOutputCode.GENERAL_ERROR.value(), importOperation.getBody().getError());
	}

	@Test
	void testImportOperationHappyPath() {
		final MockMultipartFile multipartFile = new MockMultipartFile("test", new byte[] { 1, 2, 3, 4 });
		final ResponseEntity<OperationResult> importOperation = operationsController.importOperation(multipartFile);

		assertEquals(HttpStatus.OK, importOperation.getStatusCode());
		assertNull(importOperation.getBody());
	}
}
