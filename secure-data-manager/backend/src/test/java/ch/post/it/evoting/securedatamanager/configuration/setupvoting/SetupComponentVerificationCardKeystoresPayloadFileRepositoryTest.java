/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.securedatamanager.commons.Constants.CONFIG_FILE_NAME_SETUP_COMPONENT_VERIFICATION_CARD_KEYSTORES_PAYLOAD;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.io.TempDir;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.domain.SecurityLevelTestingOnly;
import ch.post.it.evoting.domain.configuration.SetupComponentVerificationCardKeystoresPayload;
import ch.post.it.evoting.domain.configuration.VerificationCardKeystore;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@SecurityLevelTestingOnly
@DisplayName("A SetupComponentVerificationCardKeystoresPayloadFileRepository")
class SetupComponentVerificationCardKeystoresPayloadFileRepositoryTest {
	private static final Random random = RandomFactory.createRandom();
	private static final Hash hashService = HashFactory.createHash();

	private static final String NON_EXISTING_ELECTION_EVENT_ID = random.genRandomBase16String(32).toLowerCase();
	private static final String EXISTING_ELECTION_EVENT_ID = random.genRandomBase16String(32).toLowerCase();
	private static final String VERIFICATION_CARD_SET_ID = random.genRandomBase16String(32).toLowerCase();

	private static ObjectMapper objectMapper;
	private static SetupComponentVerificationCardKeystoresPayloadFileRepository setupComponentVerificationCardKeystoresPayloadFileRepository;

	@BeforeAll
	static void setUpAll(
			@TempDir
			final Path tempDir) throws IOException {

		objectMapper = DomainObjectMapper.getNewInstance();

		createDirectories(tempDir, EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID);

		final PathResolver pathResolver = new PathResolver(tempDir.toString());

		setupComponentVerificationCardKeystoresPayloadFileRepository = new SetupComponentVerificationCardKeystoresPayloadFileRepository(objectMapper,
				pathResolver);

		final SetupComponentVerificationCardKeystoresPayloadFileRepository repository = new SetupComponentVerificationCardKeystoresPayloadFileRepository(
				objectMapper, pathResolver);

		repository.save(validSetupComponentVerificationCardKeystoresPayload(EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
	}

	private static SetupComponentVerificationCardKeystoresPayload validSetupComponentVerificationCardKeystoresPayload(final String electionEventId,
			final String verificationCardSetId) {

		final List<VerificationCardKeystore> verificationCardKeystores = List.of(
				new VerificationCardKeystore(random.genRandomBase16String(32).toLowerCase(), "dmVyaWZpY2F0aW9uQ2FyZEtleXN0b3JlMQ=="),
				new VerificationCardKeystore(random.genRandomBase16String(32).toLowerCase(), "dmVyaWZpY2F0aW9uQ2FyZEtleXN0b3JlMg=="),
				new VerificationCardKeystore(random.genRandomBase16String(32).toLowerCase(), "dmVyaWZpY2F0aW9uQ2FyZEtleXN0b3JlMw==")
		);

		final SetupComponentVerificationCardKeystoresPayload setupComponentVerificationCardKeystoresPayload = new SetupComponentVerificationCardKeystoresPayload(
				electionEventId, verificationCardSetId, verificationCardKeystores);

		final byte[] payloadHash = hashService.recursiveHash(setupComponentVerificationCardKeystoresPayload);
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature(payloadHash);
		setupComponentVerificationCardKeystoresPayload.setSignature(signature);

		return setupComponentVerificationCardKeystoresPayload;
	}

	private static void createDirectories(final Path tempDir, final String electionEventId, final String verificationCardSetId) throws IOException {
		Files.createDirectories(
				tempDir.resolve("sdm/config").resolve(electionEventId).resolve("ONLINE").resolve("voteVerification").resolve(verificationCardSetId));
	}

	@Nested
	@DisplayName("saving")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class SaveTest {

		private SetupComponentVerificationCardKeystoresPayloadFileRepository setupComponentVerificationCardKeystoresPayloadFileRepositoryTemp;
		private SetupComponentVerificationCardKeystoresPayload setupComponentVerificationCardKeystoresPayload;

		@BeforeAll
		void setUpAll(
				@TempDir
				final Path tempDir) throws IOException {

			createDirectories(tempDir, EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID);

			final PathResolver pathResolver = new PathResolver(tempDir.toString());
			setupComponentVerificationCardKeystoresPayloadFileRepositoryTemp = new SetupComponentVerificationCardKeystoresPayloadFileRepository(
					objectMapper, pathResolver);
		}

		@BeforeEach
		void setUp() {
			setupComponentVerificationCardKeystoresPayload = validSetupComponentVerificationCardKeystoresPayload(EXISTING_ELECTION_EVENT_ID,
					VERIFICATION_CARD_SET_ID);
		}

		@Test
		@DisplayName("valid setup component verification card keystores payload creates file")
		void save() {
			final Path savedPath = setupComponentVerificationCardKeystoresPayloadFileRepositoryTemp.save(
					setupComponentVerificationCardKeystoresPayload);

			assertTrue(Files.exists(savedPath));
		}

		@Test
		@DisplayName("null setup component verification card keystores payload throws NullPointerException")
		void saveNullSetupComponentVerificationCardKeystoresPayload() {
			assertThrows(NullPointerException.class, () -> setupComponentVerificationCardKeystoresPayloadFileRepositoryTemp.save(null));
		}

		@Test
		@DisplayName("invalid path throws UncheckedIOException")
		void invalidPath() {
			final PathResolver pathResolver = new PathResolver("invalidPath");
			final SetupComponentVerificationCardKeystoresPayloadFileRepository repository =
					new SetupComponentVerificationCardKeystoresPayloadFileRepository(DomainObjectMapper.getNewInstance(), pathResolver);

			final UncheckedIOException exception = assertThrows(UncheckedIOException.class, () -> repository.save(
					setupComponentVerificationCardKeystoresPayload));

			final Path voteVerificationPath = pathResolver.resolveElectionEventPath(EXISTING_ELECTION_EVENT_ID).resolve("ONLINE")
					.resolve("voteVerification").resolve(VERIFICATION_CARD_SET_ID)
					.resolve(CONFIG_FILE_NAME_SETUP_COMPONENT_VERIFICATION_CARD_KEYSTORES_PAYLOAD);
			final String errorMessage = String.format(
					"Failed to serialize setup component verification card keystores payload. [electionEventId: %s, verificationCardSetId: %s, path: %s]",
					EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, voteVerificationPath);

			assertEquals(errorMessage, exception.getMessage());
		}
	}

	@Nested
	@DisplayName("calling existsById")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class ExistsByIdTest {

		@Test
		@DisplayName("for existing setup component verification card keystores payload returns true")
		void existingSetupComponentVerificationCardKeystoresPayload() {
			assertTrue(setupComponentVerificationCardKeystoresPayloadFileRepository.existsById(EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
		}

		@Test
		@DisplayName("with null input throws NullPointerException")
		void nullInput() {
			assertThrows(NullPointerException.class,
					() -> setupComponentVerificationCardKeystoresPayloadFileRepository.existsById(null, VERIFICATION_CARD_SET_ID));
			assertThrows(NullPointerException.class,
					() -> setupComponentVerificationCardKeystoresPayloadFileRepository.existsById(EXISTING_ELECTION_EVENT_ID, null));
		}

		@Test
		@DisplayName("with invalid input throws FailedValidationException")
		void invalidInput() {
			assertThrows(FailedValidationException.class,
					() -> setupComponentVerificationCardKeystoresPayloadFileRepository.existsById("invalidId", VERIFICATION_CARD_SET_ID));
			assertThrows(FailedValidationException.class,
					() -> setupComponentVerificationCardKeystoresPayloadFileRepository.existsById(EXISTING_ELECTION_EVENT_ID, "invalidId"));
		}

		@Test
		@DisplayName("for non existing setup component verification card keystores payload returns false")
		void nonExistingSetupComponentVerificationCardKeystoresPayload() {
			assertFalse(
					setupComponentVerificationCardKeystoresPayloadFileRepository.existsById(NON_EXISTING_ELECTION_EVENT_ID,
							VERIFICATION_CARD_SET_ID));
		}

	}

	@Nested
	@DisplayName("calling findById")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class FindByIdTest {

		@Test
		@DisplayName("for existing setup component verification card keystores payload returns it")
		void existingSetupComponentVerificationCardKeystoresPayload() {
			assertTrue(setupComponentVerificationCardKeystoresPayloadFileRepository.findById(EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID)
					.isPresent());
		}

		@Test
		@DisplayName("for non existing setup component verification card keystores payload return empty optional")
		void nonExistingSetupComponentVerificationCardKeystoresPayload() {
			assertFalse(
					setupComponentVerificationCardKeystoresPayloadFileRepository.findById(NON_EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID)
							.isPresent());
		}

	}

}
