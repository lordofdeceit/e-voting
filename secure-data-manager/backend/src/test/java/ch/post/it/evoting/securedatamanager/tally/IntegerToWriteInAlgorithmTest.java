/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.internal.securitylevel.SecurityLevelConfig;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.cryptoprimitives.test.tools.serialization.JsonData;
import ch.post.it.evoting.cryptoprimitives.test.tools.serialization.TestParameters;

/**
 * Tests of IntegerToWriteInAlgorithm.
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DisplayName("integerToWriteIn with")
class IntegerToWriteInAlgorithmTest extends TestGroupSetup {

	private IntegerToWriteInAlgorithm integerToWriteInAlgorithm;
	private ZqElement x;

	@BeforeAll
	void setUp() {
		integerToWriteInAlgorithm = new IntegerToWriteInAlgorithm();

		x = ZqElement.create(BigInteger.TWO, zqGroup);
	}

	@Test
	@DisplayName("null input throws NullPointerException")
	void verifyIntegerToWriteInWithNullValues() {
		assertThrows(NullPointerException.class, () -> integerToWriteInAlgorithm.integerToWriteIn(null));
	}

	@Test
	@DisplayName("invalid x throws IllegalArgumentException")
	void verifyIntegerToWriteInWithInvalidX() {
		final ZqElement invalidX = ZqElement.create(BigInteger.ZERO, zqGroup);
		assertThrows(IllegalArgumentException.class, () -> integerToWriteInAlgorithm.integerToWriteIn(invalidX));
	}

	@Test
	@DisplayName("valid values gives expected output")
	void verifyIntegerToWriteInWithValidValues() {
		final String expected = "'"; // ' (U+0027) -> 002
		final String actual = integerToWriteInAlgorithm.integerToWriteIn(x);

		assertEquals(expected, actual);
	}

	@ParameterizedTest()
	@MethodSource("jsonFileArgumentProvider")
	@DisplayName("real values gives expected result")
	void verifyIntegerToWriteInWithRealValues(final ZqElement x, final String expected, final String description) {
		final String actual = integerToWriteInAlgorithm.integerToWriteIn(x);

		assertEquals(expected, actual, String.format("assertion failed for: %s", description));
	}

	private Stream<Arguments> jsonFileArgumentProvider() throws IOException {
		final URL url = QuadraticResidueToWriteInAlgorithmTest.class.getResource("/toWriteIn/integer-to-write-in.json");
		final List<TestParameters> parametersList = Arrays.asList(DomainObjectMapper.getNewInstance().readValue(url, TestParameters[].class));

		return parametersList.stream().parallel().map(testParameters -> {
			try (MockedStatic<SecurityLevelConfig> mockedSecurityLevel = Mockito.mockStatic(SecurityLevelConfig.class)) {
				mockedSecurityLevel.when(SecurityLevelConfig::getSystemSecurityLevel).thenReturn(testParameters.getSecurityLevel());

				// Context.
				final JsonData context = testParameters.getContext();
				final BigInteger p = context.get("p", BigInteger.class);
				final BigInteger q = context.get("q", BigInteger.class);
				final BigInteger g = context.get("g", BigInteger.class);
				final GqGroup gqGroup = new GqGroup(p, q, g);

				// Input.
				final JsonData input = testParameters.getInput();
				final ZqGroup zqGroup = ZqGroup.sameOrderAs(gqGroup);
				final ZqElement x = ZqElement.create(input.get("x", BigInteger.class), zqGroup);

				// Output.
				final JsonData output = testParameters.getOutput();
				final String s = output.get("output", String.class);

				return Arguments.of(x, s, testParameters.getDescription());
			}
		});
	}

}