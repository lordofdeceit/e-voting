/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS;
import static ch.post.it.evoting.cryptoprimitives.math.GroupVector.toGroupVector;
import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.integerToString;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.SchnorrProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProofFactory;

/**
 * Tests of GenVerCardSetKeysAlgorithm.
 */
@DisplayName("A GenVerCardSetKeysAlgorithm calling genVerCardSetKeys with")
class GenVerCardSetKeysAlgorithmTest {

	private static final int NODES_NUMBER = NODE_IDS.size();
	private static final int PHI = MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS;
	private static final String ELECTION_EVENT_ID = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase();

	private static ElGamalGenerator elGamalGenerator;
	private static ZeroKnowledgeProof zeroKnowledgeProof;
	private static List<ElGamalMultiRecipientKeyPair> ccrjKeyPairs;
	private static GenVerCardSetKeysAlgorithm genVerCardSetKeysAlgorithm;
	private static GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccrjPublicKeys;
	private static GroupVector<GroupVector<SchnorrProof, ZqGroup>, ZqGroup> ccrjSchnorrProofs;

	@BeforeAll
	static void setup() {
		final GqGroup gqGroup = GroupTestData.getLargeGqGroup();
		elGamalGenerator = new ElGamalGenerator(gqGroup);
		zeroKnowledgeProof = ZeroKnowledgeProofFactory.createZeroKnowledgeProof();

		ccrjKeyPairs = genKeyPairs(PHI);

		ccrjPublicKeys = ccrjKeyPairs.stream()
				.map(ElGamalMultiRecipientKeyPair::getPublicKey)
				.collect(toGroupVector());

		ccrjSchnorrProofs = IntStream.range(0, ccrjKeyPairs.size())
				.mapToObj(j -> {
					final ElGamalMultiRecipientKeyPair ccrjKeyPair = ccrjKeyPairs.get(j);
					final List<String> i_aux = Arrays.asList(ELECTION_EVENT_ID, "GenKeysCCR", integerToString(j + 1));
					return IntStream.range(0, ccrjKeyPair.size())
							.parallel()
							.mapToObj(i -> {
								final ZqElement EL_sk_j_i = ccrjKeyPair.getPrivateKey().get(i);
								final GqElement EL_pk_j_i = ccrjKeyPair.getPublicKey().get(i);
								return zeroKnowledgeProof.genSchnorrProof(EL_sk_j_i, EL_pk_j_i, i_aux);
							}).collect(toGroupVector());
				})
				.collect(toGroupVector());

		genVerCardSetKeysAlgorithm = new GenVerCardSetKeysAlgorithm(ElGamalFactory.createElGamal(),
				ZeroKnowledgeProofFactory.createZeroKnowledgeProof());
	}

	@Test
	@DisplayName("valid parameter does not throw")
	void validParamDoesNotThrow() {
		assertDoesNotThrow(() -> genVerCardSetKeysAlgorithm.genVerCardSetKeys(ELECTION_EVENT_ID, ccrjPublicKeys, ccrjSchnorrProofs));
	}

	@Test
	@DisplayName("invalid election event id throws FailedValidationException")
	void invalidElectionEventId() {
		assertThrows(FailedValidationException.class, () -> genVerCardSetKeysAlgorithm.genVerCardSetKeys("123", ccrjPublicKeys, ccrjSchnorrProofs));
	}

	@Test
	@DisplayName("null argument throws a NullPointerException")
	void testGenVerCardSetKeysWithNullArgumentThrows() {
		assertThrows(NullPointerException.class, () -> genVerCardSetKeysAlgorithm.genVerCardSetKeys(ELECTION_EVENT_ID, null, ccrjSchnorrProofs));
		assertThrows(NullPointerException.class, () -> genVerCardSetKeysAlgorithm.genVerCardSetKeys(ELECTION_EVENT_ID, ccrjPublicKeys, null));
	}

	@Test
	@DisplayName("a too short Choice Return Codes encryption public key vector throws")
	void testGenVerCardSetKeysWithTooShortChoiceReturnCodesEncryptionPublicKeysVectorThrows() {
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> tooFewPublicKeys = ccrjPublicKeys.subVector(0, NODES_NUMBER - 1);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> genVerCardSetKeysAlgorithm.genVerCardSetKeys(ELECTION_EVENT_ID, tooFewPublicKeys, ccrjSchnorrProofs));
		final String errorMessage = String.format("Wrong number of CCR_j Choice Return Codes encryption public keys. [expected: %s, found :%s]",
				NODES_NUMBER, NODES_NUMBER - 1);

		assertEquals(errorMessage, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("a too long Choice Return Codes encryption public key vector throws")
	void testGenVerCardSetKeysWithTooLongChoiceReturnCodesEncryptionPublicKeysVectorThrows() {
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> tooManyPublicKeys = ccrjPublicKeys.append(ccrjPublicKeys.get(0));

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> genVerCardSetKeysAlgorithm.genVerCardSetKeys(ELECTION_EVENT_ID, tooManyPublicKeys, ccrjSchnorrProofs));
		final String errorMessage = String.format("Wrong number of CCR_j Choice Return Codes encryption public keys. [expected: %s, found :%s]",
				NODES_NUMBER, NODES_NUMBER + 1);

		assertEquals(errorMessage, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("too few Schnorr proofs throws")
	void tooFewSchnorrProofs() {
		final GroupVector<GroupVector<SchnorrProof, ZqGroup>, ZqGroup> tooFewProofs = ccrjSchnorrProofs.subVector(0, NODES_NUMBER - 1);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> genVerCardSetKeysAlgorithm.genVerCardSetKeys(ELECTION_EVENT_ID, ccrjPublicKeys, tooFewProofs));
		final String errorMessage = String.format("Wrong number of CCR_j Schnorr proofs. [expected: %s, found :%s]",
				NODES_NUMBER, NODES_NUMBER - 1);

		assertEquals(errorMessage, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("a too few Choice Return Codes encryption public key elements throws")
	void testGenVerCardSetKeysWithTooFewKeyElementsThrows() {
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> tooFewElementsPublicKeys = genKeyPairs(1).stream()
				.map(ElGamalMultiRecipientKeyPair::getPublicKey)
				.collect(GroupVector.toGroupVector());

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> genVerCardSetKeysAlgorithm.genVerCardSetKeys(ELECTION_EVENT_ID, tooFewElementsPublicKeys, ccrjSchnorrProofs));
		final String expectedErrorMessage = String.format("The CCR_j Choice Return Codes encryption public keys must be of size %d", PHI);

		assertEquals(expectedErrorMessage, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("a too many Choice Return Codes encryption public key elements throws")
	void testGenVerCardSetKeysWithTooManyKeyElementsThrows() {
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> tooManyElementsPublicKeys = genKeyPairs(PHI + 1).stream()
				.map(ElGamalMultiRecipientKeyPair::getPublicKey)
				.collect(GroupVector.toGroupVector());

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> genVerCardSetKeysAlgorithm.genVerCardSetKeys(ELECTION_EVENT_ID, tooManyElementsPublicKeys, ccrjSchnorrProofs));
		final String expectedErrorMessage = String.format("The CCR_j Choice Return Codes encryption public keys must be of size %d", PHI);

		assertEquals(expectedErrorMessage, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("Schnorr proofs with too few elements throws")
	void schnorrProofsTooFewElements() {
		final GroupVector<GroupVector<SchnorrProof, ZqGroup>, ZqGroup> tooFewElementsProofs = IntStream.range(0, ccrjKeyPairs.size())
				.mapToObj(j -> {
					final ElGamalMultiRecipientKeyPair ccrjKeyPair = ccrjKeyPairs.get(j);
					final List<String> i_aux = Arrays.asList(ELECTION_EVENT_ID, "GenKeysCCR", integerToString(j));
					return IntStream.range(0, 2)
							.parallel()
							.mapToObj(i -> {
								final ZqElement EL_sk_j_i = ccrjKeyPair.getPrivateKey().get(i);
								final GqElement EL_pk_j_i = ccrjKeyPair.getPublicKey().get(i);
								return zeroKnowledgeProof.genSchnorrProof(EL_sk_j_i, EL_pk_j_i, i_aux);
							}).collect(toGroupVector());
				})
				.collect(toGroupVector());

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> genVerCardSetKeysAlgorithm.genVerCardSetKeys(ELECTION_EVENT_ID, ccrjPublicKeys, tooFewElementsProofs));
		final String expectedErrorMessage = String.format("The CCR_j Schnorr proofs must be of size %d", PHI);

		assertEquals(expectedErrorMessage, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("public keys and proofs of different groups throws")
	void differentGroupsKeysAndProofs() {
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> otherPublicKeys = spy(ccrjPublicKeys);
		when(otherPublicKeys.getGroup()).thenReturn(GroupTestData.getGqGroup());

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> genVerCardSetKeysAlgorithm.genVerCardSetKeys(ELECTION_EVENT_ID, otherPublicKeys, ccrjSchnorrProofs));
		final String expectedErrorMessage = "The CCR_j Choice Return Codes encryption public keys must have the same group order as the CCR_j Schnorr proofs.";

		assertEquals(expectedErrorMessage, Throwables.getRootCause(exception).getMessage());
	}

	private static List<ElGamalMultiRecipientKeyPair> genKeyPairs(final int keyElementsSize) {
		return IntStream.range(0, NODES_NUMBER)
				.mapToObj(i -> elGamalGenerator.genRandomKeyPair(keyElementsSize))
				.toList();
	}

}
