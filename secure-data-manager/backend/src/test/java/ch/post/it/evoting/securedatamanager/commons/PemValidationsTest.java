/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.commons;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.junit.jupiter.api.Test;

/**
 * @author bianchimaur
 */
class PemValidationsTest {

	private final String RSA_PUBLIC_KEY = """
			-----BEGIN PUBLIC KEY-----
			MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsjtGIk8SxD+OEiBpP2/T
			JUAF0upwuKGMk6wH8Rwov88VvzJrVm2NCticTk5FUg+UG5r8JArrV4tJPRHQyvqK
			wF4NiksuvOjv3HyIf4oaOhZjT8hDne1Bfv+cFqZJ61Gk0MjANh/T5q9vxER/7TdU
			NHKpoRV+NVlKN5bEU/NQ5FQjVXicfswxh6Y6fl2PIFqT2CfjD+FkBPU1iT9qyJYH
			A38IRvwNtcitFgCeZwdGPoxiPPh1WHY8VxpUVBv/2JsUtrB/rAIbGqZoxAIWvijJ
			Pe9o1TY3VlOzk9ASZ1AeatvOir+iDVJ5OpKmLnzc46QgGPUsjIyo6Sje9dxpGtoG
			QQIDAQAB
			-----END PUBLIC KEY-----""";
	private final String RSA_PRIVATE_KEY = """
			-----BEGIN RSA PRIVATE KEY-----
			MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCyO0YiTxLEP44S
			IGk/b9MlQAXS6nC4oYyTrAfxHCi/zxW/MmtWbY0K2JxOTkVSD5QbmvwkCutXi0k9
			EdDK+orAXg2KSy686O/cfIh/iho6FmNPyEOd7UF+/5wWpknrUaTQyMA2H9Pmr2/E
			RH/tN1Q0cqmhFX41WUo3lsRT81DkVCNVeJx+zDGHpjp+XY8gWpPYJ+MP4WQE9TWJ
			P2rIlgcDfwhG/A21yK0WAJ5nB0Y+jGI8+HVYdjxXGlRUG//YmxS2sH+sAhsapmjE
			Aha+KMk972jVNjdWU7OT0BJnUB5q286Kv6INUnk6kqYufNzjpCAY9SyMjKjpKN71
			3Gka2gZBAgMBAAECggEAFlPam12wiik0EQ1CYhIOL3JvyFZaPKbwR2ebrxbJ/A1j
			OgqE69TZgGxWWHDxui/9a9/kildb2CG40Q+0SllMnICrzZFRj5TWx5ZKOz//vRsk
			4c/CuLwKInC/Cw9V30bhEM61VZJzJ0j/BWVXaU4vHEro+ScKIoDHDWOzwJiQn6m9
			C+Ti5lFpax3hx8ZrgPqmBCFYNvErrWkOr7mCYl0jS+E22c68yn8+LjdlF1LWUa6N
			zutk3MPj5UwEyR0h7EZReCeGkPTMQNyOBhDcmAtlEno4fjtZzUDHRjh8/QpG1Mz/
			alavvrkjswc1DmRUOdgiYu+Waxan5noBhxEAvd/hyQKBgQDjYJD0n+m0tUrpNtX0
			+mdzHstClHrpx5oNxs4sIBjCoCwEXaSpeY8+JxCdnZ6n29mLZLq/wPXxZ3EJcOSZ
			PYUvZJfV/IUvoLPFbtT3ILzDTcAAeHj2GAOpzYP8J1JSFsc78ZjKMF1XeNjXcq8T
			XNXoWfY7N/fShoycVeG42JJCFwKBgQDIqvHL0QfJ8r6yM8Efj7Zq6Wa4C9okORes
			8UVWfBoO6UOWvpK+D9IjnaEisJcnEalwNi8/eKudR9hfvmzATV+t3YJIgktto3TT
			BWLsEyniNU4vSTl7GPBrV2xabWogbChlt7TXUfw6YogaBKm43snYXBbJFc+NcpQH
			ONB5igppZwKBgGDyYHvc3wGsttb/CXTde1RLUfD+a/XXpCixlmCcAtKhBoOKBdY4
			vUmL0HrTpLz/cR8NAM8XkAWwzDJxTxbDc1EEu/SCKatoAp5wph8Ed1dyhCXvN+v9
			yzoQJXFStrfHfIVjenji7DmKjjI2dM11rMLX8LPJJkI+Gh/iQk7VEG9bAoGAH/aS
			sztleTZwR6RUw7k5fkgVM4W3xoNNkR+RQthbsjpXqMBMUXflqgSmsQbd3LxEd/o5
			hmurMk9KWN3VJsBsWB5rbS9L4nfh2OcHvcDDsCN7g66vODtduEthl/nLqMRxnton
			NRD7EzW0pihN/IOINS1d98PAnrA8gfX7xxBE3ksCgYBvoljHGjvy3bPJ++vDGKJK
			y6JuEeRVzgdPXEb60uU+BR7kdh+MMsZLmgfFTgza3R+/xeZcC/cuOPsbzeooRQi/
			9NpKwSCXjVNk9nglUWBoPRh4uYqrArWn+HoR7MI/BxeRJm5e1+ii8P19Y9joX5s0
			Q3OLn8GeH56ClJmNiWDhsA==
			-----END RSA PRIVATE KEY-----""";

	@Test
	void validPEMTest() {
		assertAll(
				() -> assertDoesNotThrow(() -> PemValidations.validatePEM(RSA_PUBLIC_KEY)),
				() -> assertDoesNotThrow(() -> PemValidations.validatePEM(RSA_PRIVATE_KEY))
		);
	}

	@Test
	void nullTest() {
		final NullPointerException exception = assertThrows(NullPointerException.class,
				() -> PemValidations.validatePEM(null));

		assertNull(exception.getMessage());
	}

	@Test
	void emptyInputTest() {
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> PemValidations.validatePEM(""));

		assertEquals("Certificate PEM parameter is required.", exception.getMessage());
	}

	@Test
	void typeTagsNotMatchingTest() {
		final String invalidTagPEM = RSA_PRIVATE_KEY
				.replace("END RSA PRIVATE KEY", "PUBLIC KEY");

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> PemValidations.validatePEM(invalidTagPEM));

		final String expectedMessage = "The given PEM content could not be parsed.";
		assertEquals(expectedMessage, exception.getMessage().substring(0, expectedMessage.length()));
	}

	@Test
	void invalidTagTest() {
		final String invalidTagPEM = RSA_PRIVATE_KEY
				.replace("RSA PRIVATE KEY", "X")
				.replace("RSA PRIVATE KEY", "X");

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> PemValidations.validatePEM(invalidTagPEM));

		final String expectedMessage = String.format("The given value is not an handled PEM type. [string: %s]", "X");
		assertEquals(expectedMessage, exception.getMessage());
	}

	@Test
	void emptyCertificateTest() {
		final String emptyPEM = """
				-----BEGIN RSA PRIVATE KEY-----


				-----END RSA PRIVATE KEY-----""";

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> PemValidations.validatePEM(emptyPEM));

		final String expectedMessage = String.format("The given PEM content is not valid. [string: %s]", emptyPEM);
		assertEquals(expectedMessage, exception.getMessage());
	}

	@Test
	void firstCharacterNotAMTest() {
		final String clearText = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+/";
		final byte[] clearTextBase64 = Base64.getMimeEncoder().encode(clearText.getBytes(StandardCharsets.UTF_8));
		final String firstCharacterNotAMPEM = "-----BEGIN RSA PRIVATE KEY-----\n"
				+ new String(clearTextBase64, StandardCharsets.UTF_8)
				+ "\n-----END RSA PRIVATE KEY-----";

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> PemValidations.validatePEM(firstCharacterNotAMPEM));

		final String expectedMessage = String.format("The given PEM content is not valid. [string: %s]", firstCharacterNotAMPEM);
		assertEquals(expectedMessage, exception.getMessage());
	}

	@Test
	void invalidBase64Test() {
		final String invalidCharacters = "%@*&";
		final int endTagPos = "-----BEGIN RSA PRIVATE KEY-----\nM".length();
		final String notBase64PEM = RSA_PRIVATE_KEY.substring(0, endTagPos)
				+ invalidCharacters
				+ RSA_PRIVATE_KEY.substring(endTagPos + invalidCharacters.length());

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> PemValidations.validatePEM(notBase64PEM));

		final String expectedMessage = String.format("The given PEM content is not a valid Base64 encoded. [string: %s]", notBase64PEM);
		assertEquals(expectedMessage, exception.getMessage());
	}

	@Test
	void invalidRSAPrivateKey() {
		final String pem = RSA_PUBLIC_KEY.replace("PUBLIC", "RSA PRIVATE");

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> PemValidations.validatePEM(pem));

		final String expectedMessage = String.format("The given PEM content is not valid. [string: %s]", pem);
		assertEquals(expectedMessage, exception.getMessage());
	}

	@Test
	void invalidRSAPublicKey() {
		final String pem = RSA_PRIVATE_KEY.replace("RSA PRIVATE", "PUBLIC");

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> PemValidations.validatePEM(pem));

		final String expectedMessage = String.format("The given PEM content is not valid. [string: %s]", pem);
		assertEquals(expectedMessage, exception.getMessage());
	}
}
