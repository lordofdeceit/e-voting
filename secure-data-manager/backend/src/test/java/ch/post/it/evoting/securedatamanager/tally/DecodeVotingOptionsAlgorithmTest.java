/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.stream.IntStream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;

/**
 * Tests of DecodeVotingOptionsAlgorithm.
 */
@DisplayName("A DecodeVotingOptionsAlgorithm calling decodeVotingOptions")
class DecodeVotingOptionsAlgorithmTest {

	private static DecodeVotingOptionsAlgorithm decodeVotingOptionsAlgorithm;

	private static final List<String> actualVotingOptions = List.of(
			"57a30570-1722-3a7e-a8f9-7dd643d7f33",
			"b9c57a40-a555-35e1-972c-a1a6b7e03381",
			"1-3");
	private static final GqGroup gqGroup = GroupTestData.getGroupP59();
	private static final int desiredNumberOfPrimes = 3;

	private static GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions;
	private static GroupVector<PrimeGqElement, GqGroup> biggerEncodedVotingOptions;
	private static PrimesMappingTable primesMappingTable;

	@BeforeAll
	static void setUp() {

		decodeVotingOptionsAlgorithm = new DecodeVotingOptionsAlgorithm();
		biggerEncodedVotingOptions = PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers(gqGroup, desiredNumberOfPrimes + 1);
		encodedVotingOptions = GroupVector.from(biggerEncodedVotingOptions.subVector(0, desiredNumberOfPrimes));

		final List<PrimesMappingTableEntry> primesMappingTableEntries = IntStream.range(0, desiredNumberOfPrimes)
				.mapToObj(i -> new PrimesMappingTableEntry(actualVotingOptions.get(i), encodedVotingOptions.get(i)))
				.toList();

		primesMappingTable = PrimesMappingTable.from(primesMappingTableEntries);
	}

	@DisplayName("with any null input throws a NullPointerException.")
	@Test
	void nullInputThrows() {

		assertAll(() -> assertThrows(NullPointerException.class, () -> decodeVotingOptionsAlgorithm.decodeVotingOptions(null, primesMappingTable)),
				() -> assertThrows(NullPointerException.class, () -> decodeVotingOptionsAlgorithm.decodeVotingOptions(encodedVotingOptions, null)));

	}

	@DisplayName("with any invalid input throws an IllegalArgumentException.")
	@Test
	void invalidInputThrows() {

		final IllegalArgumentException illegalArgumentException1 = assertThrows(IllegalArgumentException.class,
				() -> decodeVotingOptionsAlgorithm.decodeVotingOptions(biggerEncodedVotingOptions, primesMappingTable));

		assertEquals(String.format("The size of the encoded voting options must be smaller than the size of the primes mapping table. [m: %s, n: %s]",
						biggerEncodedVotingOptions.size(), primesMappingTable.size()),
				Throwables.getRootCause(illegalArgumentException1).getMessage());

		final PrimesMappingTable invalidPrimesMappingTable = PrimesMappingTable.from(primesMappingTable.getPTable().subVector(0, 1));

		final IllegalArgumentException illegalArgumentException2 = assertThrows(IllegalArgumentException.class,
				() -> decodeVotingOptionsAlgorithm.decodeVotingOptions(encodedVotingOptions, invalidPrimesMappingTable));

		assertEquals(String.format("The size of the encoded voting options must be smaller than the size of the primes mapping table. [m: %s, n: %s]",
						encodedVotingOptions.size(), invalidPrimesMappingTable.size()),
				Throwables.getRootCause(illegalArgumentException2).getMessage());

		final PrimeGqElement five = PrimeGqElement.PrimeGqElementFactory.fromValue(5, GroupTestData.getLargeGqGroup());
		final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptionsFromOtherGroup = IntStream.range(0, desiredNumberOfPrimes)
				.mapToObj(i -> five)
				.collect(GroupVector.toGroupVector());

		final IllegalArgumentException illegalArgumentException3 = assertThrows(IllegalArgumentException.class,
				() -> decodeVotingOptionsAlgorithm.decodeVotingOptions(encodedVotingOptionsFromOtherGroup, primesMappingTable));

		assertEquals("The groups of the primes mapping table and the encoded voting options must be equal.",
				Throwables.getRootCause(illegalArgumentException3).getMessage());

		final GroupVector<PrimeGqElement, GqGroup> smallerEncodedVotingOption = encodedVotingOptions.subVector(1, desiredNumberOfPrimes - 1);
		final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptionWithDuplication = smallerEncodedVotingOption.append(
				smallerEncodedVotingOption.get(0));

		final IllegalArgumentException illegalArgumentException4 = assertThrows(IllegalArgumentException.class,
				() -> decodeVotingOptionsAlgorithm.decodeVotingOptions(encodedVotingOptionWithDuplication, primesMappingTable));

		assertEquals("The encoded voting options must all be distinct.", Throwables.getRootCause(illegalArgumentException4).getMessage());
	}

	@DisplayName("with valid inputs behaves as expected.")
	@Test
	void happyPath() {

		final int upperBound = desiredNumberOfPrimes - 1;

		final List<String> decodeVotingOptions =
				decodeVotingOptionsAlgorithm.decodeVotingOptions(encodedVotingOptions.subVector(0, upperBound), primesMappingTable);

		assertEquals(actualVotingOptions.subList(0, upperBound), decodeVotingOptions);
	}

}
