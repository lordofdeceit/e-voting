/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.batch.batch;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptolib.api.asymmetric.AsymmetricServiceAPI;
import ch.post.it.evoting.cryptolib.api.derivation.CryptoAPIPBKDFDeriver;
import ch.post.it.evoting.cryptolib.api.exceptions.GeneralCryptoLibException;
import ch.post.it.evoting.cryptolib.api.primitives.PrimitivesServiceAPI;
import ch.post.it.evoting.cryptolib.asymmetric.service.AsymmetricService;
import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamal;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.domain.election.EncryptionParameters;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.commons.PathResolver;
import ch.post.it.evoting.securedatamanager.commons.domain.CreateVotingCardSetCertificatePropertiesContainer;
import ch.post.it.evoting.securedatamanager.commons.domain.VerificationCardIdValues;
import ch.post.it.evoting.securedatamanager.config.commons.config.model.authentication.ExtendedAuthInformation;
import ch.post.it.evoting.securedatamanager.config.engine.actions.ExtendedAuthenticationService;
import ch.post.it.evoting.securedatamanager.config.engine.commands.voters.JobExecutionObjectContext;
import ch.post.it.evoting.securedatamanager.config.engine.commands.voters.VotersParametersHolder;
import ch.post.it.evoting.securedatamanager.config.engine.commands.voters.datapacks.beans.VotingCardCredentialDataPack;
import ch.post.it.evoting.securedatamanager.config.engine.commands.voters.datapacks.beans.VotingCardCredentialInputDataPack;
import ch.post.it.evoting.securedatamanager.config.engine.commands.voters.datapacks.generators.VotingCardCredentialDataPackGenerator;
import ch.post.it.evoting.securedatamanager.configuration.StartVotingKeyService;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
class VoterAuthenticationDataProcessorTest {

	private static final String RESOURCES_FOLDER_NAME = "VoterAuthenticationDataProcessorTest";
	private static final String BALLOT_JSON = "ballot.json";
	private static final String P = "3736902868501299049840448965872822110993224917234896665965452485988811621118821041162066391527167967040747839524276751920049256101873074337343241558495680369584846946635121762506479775123882370706626306527961298841467168259924857124830263888084891691200871300884717333117172635601962738079272493640716650829719782494995804532666615431774964812319183306308935515141279905200789444573996821158380001908161769225369261063428625650578502892851212744246529230804923376939888069435686782837197190688731444553143168763013491571058836325821552631526047011124237553142199525923974367423240487155839928389964567319272342202732727045137302610434221076916491613745507708612389669721367562252230886619155385835463186908060875629610305227364890215549001154287785529194410281718810764231222357222676883369183848276196464256898654575902082042801240810965633917557839744860352536662383114345400381564825476419455375071936387619506394664830483";
	private static final String Q = "1868451434250649524920224482936411055496612458617448332982726242994405810559410520581033195763583983520373919762138375960024628050936537168671620779247840184792423473317560881253239887561941185353313153263980649420733584129962428562415131944042445845600435650442358666558586317800981369039636246820358325414859891247497902266333307715887482406159591653154467757570639952600394722286998410579190000954080884612684630531714312825289251446425606372123264615402461688469944034717843391418598595344365722276571584381506745785529418162910776315763023505562118776571099762961987183711620243577919964194982283659636171101366363522568651305217110538458245806872753854306194834860683781126115443309577692917731593454030437814805152613682445107774500577143892764597205140859405382115611178611338441684591924138098232128449327287951041021400620405482816958778919872430176268331191557172700190782412738209727687535968193809753197332415241";
	private static final String G = "3";

	private static final String ELECTION_EVENT_ID = "be658216a38b421b943457846c6a68f9";
	private static final String VERIFICATION_CARD_SET_ID = "verificationCardSetId";
	private static final String VERIFICATION_CARD_ID = "verificationCardId";
	@Autowired
	private VoterAuthenticationDataProcessor voterAuthenticationDataProcessor;
	@Autowired
	private ElGamalGenerator elGamalGenerator;
	@Autowired
	private Random random;
	@Autowired
	private ElGamal elGamal;

	@Test
	void generatesVotingCardOutput() throws Exception {

		final VerificationCardIdValues verificationCardIdValuesMock =
				mock(VerificationCardIdValues.class);

		when(verificationCardIdValuesMock.getVerificationCardId()).thenReturn(VERIFICATION_CARD_ID);

		final GeneratedVotingCardOutput output = voterAuthenticationDataProcessor.process(verificationCardIdValuesMock);

		assertNotNull(output);
		assertNotNull(output.getBallotBoxId());
		assertNotNull(output.getBallotId());
		assertNotNull(output.getCredentialId());
		assertNotNull(output.getElectionEventId());
		assertNull(output.getError());
		assertNotNull(output.getExtendedAuthInformation());
		assertNotNull(output.getStartVotingKey());
		assertNotNull(output.getVerificationCardId());
		assertNotNull(output.getVoterCredentialDataPack());
		assertNotNull(output.getVotingCardId());
		assertNotNull(output.getVotingCardSetId());
		assertEquals(Constants.NUM_DIGITS_BALLOT_CASTING_KEY, output.getBallotCastingKey().length());
	}

	@Test
	void retrieveBallotCastingKeyTest() throws URISyntaxException {

		final Path basePath = Paths.get(VoterAuthenticationDataProcessorTest.class.getClassLoader().getResource(RESOURCES_FOLDER_NAME).toURI())
				.resolve("electionEventId");

		assertAll(() -> assertEquals("ballotCastingKey1",
						VoterAuthenticationDataProcessor.retrieveBallotCastingKey("verificationCardSetId", "verificationCardId1", basePath)),
				() -> assertEquals("ballotCastingKey2",
						VoterAuthenticationDataProcessor.retrieveBallotCastingKey("verificationCardSetId", "verificationCardId2", basePath)),
				() -> assertEquals("ballotCastingKey3",
						VoterAuthenticationDataProcessor.retrieveBallotCastingKey("verificationCardSetId", "verificationCardId3", basePath)));
	}

	private static Ballot getBallotFromResources() throws IOException {
		return new ObjectMapper().readValue(
				VoterAuthenticationDataProcessorTest.class.getClassLoader().getResource(RESOURCES_FOLDER_NAME + File.separator + BALLOT_JSON),
				Ballot.class);
	}

	@Configuration
	static class PrivateConfiguration {

		@Bean
		ElGamalGenerator elGamalGenerator() {
			return new ElGamalGenerator(new GqGroup(new BigInteger(P), new BigInteger(Q), new BigInteger(G)));
		}

		@Bean
		JobExecutionObjectContext executionObjectContext() throws IOException, URISyntaxException {
			final JobExecutionObjectContext jobExecutionObjectContextMock = mock(JobExecutionObjectContext.class);
			final VotersParametersHolder votersParametersHolderMock = mock(VotersParametersHolder.class);
			final EncryptionParameters encryptionParameters = new EncryptionParameters(P, Q, G);

			when(votersParametersHolderMock.getVotingCardCredentialInputDataPack()).thenReturn(mock(VotingCardCredentialInputDataPack.class));
			when(votersParametersHolderMock.getVotingCardCredentialInputDataPack().getEeid()).thenReturn("1");
			when(votersParametersHolderMock.getBallot()).thenReturn(getBallotFromResources());
			when(votersParametersHolderMock.getEncryptionParameters()).thenReturn(encryptionParameters);
			when(votersParametersHolderMock.getAbsoluteBasePath())
					.thenReturn(Paths.get(VoterAuthenticationDataProcessorTest.class.getClassLoader().getResource(RESOURCES_FOLDER_NAME).toURI())
							.resolve("1"));
			when(votersParametersHolderMock.getCreateVotingCardSetCertificateProperties())
					.thenReturn(mock(CreateVotingCardSetCertificatePropertiesContainer.class));

			when(jobExecutionObjectContextMock.get(anyString(), eq(VotersParametersHolder.class))).thenReturn(votersParametersHolderMock);

			return jobExecutionObjectContextMock;
		}

		@Bean
		ExtendedAuthenticationService extendedAuthenticationService() {
			final ExtendedAuthenticationService extendedAuthenticationServiceMock = mock(ExtendedAuthenticationService.class);

			when(extendedAuthenticationServiceMock.create(any(), any())).thenReturn(mock(ExtendedAuthInformation.class));

			return extendedAuthenticationServiceMock;
		}

		@Bean
		VotingCardCredentialDataPackGenerator votingCardCredentialDataPackGenerator() throws GeneralCryptoLibException {
			final VotingCardCredentialDataPackGenerator votingCardCredentialDataPackGeneratorMock = mock(VotingCardCredentialDataPackGenerator.class);

			when(votingCardCredentialDataPackGeneratorMock.generate(any(), any(), any(), any(), any(), any(), any(), any()))
					.thenReturn(mock(VotingCardCredentialDataPack.class));

			return votingCardCredentialDataPackGeneratorMock;
		}

		@Bean
		AsymmetricServiceAPI asymmetricServiceAPI() {
			return new AsymmetricService();
		}

		@Bean
		VotingCardGenerationJobExecutionContext jobContext() {
			final VotingCardGenerationJobExecutionContext context = mock(VotingCardGenerationJobExecutionContext.class);
			when(context.getVerificationCardSetId()).thenReturn("1");
			when(context.getVotingCardSetId()).thenReturn("1");
			return context;
		}

		@Bean
		CryptoAPIPBKDFDeriver deriver() {
			return mock(CryptoAPIPBKDFDeriver.class);
		}

		@Bean
		PrimitivesServiceAPI primitivesServiceAPI() throws GeneralCryptoLibException {

			final CryptoAPIPBKDFDeriver cryptoAPIPBKDFDeriverMock = mock(CryptoAPIPBKDFDeriver.class);
			when(cryptoAPIPBKDFDeriverMock.deriveKey(any(), any())).thenReturn("1"::getBytes);

			final PrimitivesServiceAPI primitivesServiceAPIMock = mock(PrimitivesServiceAPI.class);
			when(primitivesServiceAPIMock.getPBKDFDeriver()).thenReturn(cryptoAPIPBKDFDeriverMock);

			return primitivesServiceAPIMock;
		}

		@Bean
		JobExecution jobExecution() {
			final JobExecution jobExecutionMock = mock(JobExecution.class);
			final ExecutionContext executionContextMock = mock(ExecutionContext.class);

			when(executionContextMock.get(Constants.VERIFICATION_CARD_SET_ID)).thenReturn(VERIFICATION_CARD_SET_ID);
			when(executionContextMock.get(Constants.VOTING_CARD_SET_ID)).thenReturn("1");
			when(executionContextMock.get(Constants.NUMBER_VOTING_CARDS)).thenReturn(1);
			when(executionContextMock.get(Constants.SALT_KEYSTORE_SYM_ENC_KEY)).thenReturn("11");
			when(executionContextMock.get(Constants.SALT_CREDENTIAL_ID)).thenReturn("11");
			when(executionContextMock.get(Constants.BALLOT_ID)).thenReturn("1");
			when(executionContextMock.get(Constants.BALLOT_BOX_ID)).thenReturn("1");
			when(executionContextMock.get(Constants.VOTING_CARD_SET_ID)).thenReturn("1");
			when(executionContextMock.get(Constants.JOB_INSTANCE_ID)).thenReturn("1");
			when(executionContextMock.get(Constants.ELECTION_EVENT_ID)).thenReturn(ELECTION_EVENT_ID);

			when(jobExecutionMock.getExecutionContext()).thenReturn(executionContextMock);

			return jobExecutionMock;
		}

		@Bean
		StartVotingKeyService startVotingKeyService() {
			final StartVotingKeyService startVotingKeyService = mock(StartVotingKeyService.class);

			when(startVotingKeyService.load(any(), any(), any())).thenReturn("startVotingKey");

			return startVotingKeyService;
		}

		@Bean
		VoterAuthenticationDataProcessor voterAuthenticationDataProcessor(
				final ExtendedAuthenticationService extendedAuthenticationService,
				final StartVotingKeyService startVotingKeyService,
				final VotingCardCredentialDataPackGenerator votingCardCredentialDataPackGenerator,
				final PrimitivesServiceAPI primitivesService,
				@Qualifier("executionObjectContext")
				final JobExecutionObjectContext objectContext,
				final JobExecution jobExecution,
				final Random random) {
			return new VoterAuthenticationDataProcessor(extendedAuthenticationService, startVotingKeyService, votingCardCredentialDataPackGenerator,
					primitivesService, objectContext, jobExecution, random);
		}

		@Bean
		PathResolver pathResolver() throws URISyntaxException {
			final PathResolver pathResolverMock = mock(PathResolver.class);

			final Path setupKeyPath = Paths.get(VoterAuthenticationDataProcessorTest.class.getResource("/setupSecretKey.json").toURI());
			when(pathResolverMock.resolve(Constants.CONFIG_FILES_BASE_DIR, "1", Constants.CONFIG_DIR_NAME_OFFLINE,
					Constants.SETUP_SECRET_KEY_FILE_NAME)).thenReturn(setupKeyPath);

			return pathResolverMock;
		}

		@Bean
		Random random() {
			return RandomFactory.createRandom();
		}

		@Bean
		ElGamal elGamal() {
			return ElGamalFactory.createElGamal();
		}
	}
}
