/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SignatureException;
import java.util.List;

import org.apache.xerces.impl.dv.util.Base64;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.ControlComponentCodeShare;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationData;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.PayloadStorageException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.SetupComponentVerificationDataPayloadFileSystemRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

import io.jsonwebtoken.lang.Strings;

@DisplayName("EncryptedNodeLongCodeSharesService")
class EncryptedNodeLongReturnCodeSharesServiceTest {

	private static final String WRONG_ID = "0123456789abcdef0123456789abcdef";
	private static final String ELECTION_EVENT_ID = "9428588bfa504d37bd2a61088346d53d";
	private static final String VERIFICATION_CARD_SET_ID = "3880a1b0f49341d68f3c9fec15782063";

	private EncryptedNodeLongReturnCodeSharesService encryptedNodeLongReturnCodeSharesService;
	private SetupComponentVerificationDataPayloadFileSystemRepository setupComponentVerificationDataPayloadFileSystemRepository;

	private SignatureKeystore<Alias> sdmSignatureKeystoreServiceSdmConfig;
	private VotingCardSetRepository votingCardSetRepository;

	@BeforeEach
	void setup() throws URISyntaxException, IOException, PayloadStorageException {
		prepare("valid");
	}

	private void prepare(final String dataset) throws URISyntaxException, IOException, PayloadStorageException {
		final Path path = Paths.get(EncryptedNodeLongReturnCodeSharesService.class.getResource("/encryptedNodeLongCodeSharesServiceTest").toURI());
		final PathResolver pathResolver = new PathResolver(path.resolve(dataset).toString());
		final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
		final NodeContributionsResponsesFileRepository nodeContributionsResponsesFileRepository = new NodeContributionsResponsesFileRepository(
				objectMapper, pathResolver);
		final NodeContributionsResponsesService nodeContributionsResponsesService = new NodeContributionsResponsesService(
				nodeContributionsResponsesFileRepository);

		final SetupComponentVerificationDataPayload setupComponentVerificationDataPayload = objectMapper.readValue(
				path.resolve("setupComponentVerificationDataPayload.0.json").toFile(), SetupComponentVerificationDataPayload.class);
		setupComponentVerificationDataPayloadFileSystemRepository = mock(SetupComponentVerificationDataPayloadFileSystemRepository.class);
		when(setupComponentVerificationDataPayloadFileSystemRepository.getCount(anyString(), anyString())).thenReturn(1);
		when(setupComponentVerificationDataPayloadFileSystemRepository.retrieve(anyString(), anyString(), anyInt()))
				.thenReturn(setupComponentVerificationDataPayload);

		sdmSignatureKeystoreServiceSdmConfig = mock(SignatureKeystore.class);
		votingCardSetRepository = mock(VotingCardSetRepository.class);

		encryptedNodeLongReturnCodeSharesService = new EncryptedNodeLongReturnCodeSharesService(nodeContributionsResponsesService,
				sdmSignatureKeystoreServiceSdmConfig, votingCardSetRepository, setupComponentVerificationDataPayloadFileSystemRepository);
	}

	@Test
	@DisplayName("Combine node contributions")
	void combine() throws SignatureException, ResourceNotFoundException {
		final int expectedNodeSize = 4;
		final int expectedListSize = 5;

		when(sdmSignatureKeystoreServiceSdmConfig.verifySignature(any(), any(), any(), any())).thenReturn(true);
		when(votingCardSetRepository.getNumberOfVotingCards(any(), any())).thenReturn(expectedListSize);

		final EncryptedNodeLongReturnCodeShares encryptedNodeLongReturnCodeShares = encryptedNodeLongReturnCodeSharesService.load(ELECTION_EVENT_ID,
				VERIFICATION_CARD_SET_ID);

		assertAll(
				() -> assertEquals(ELECTION_EVENT_ID, encryptedNodeLongReturnCodeShares.getElectionEventId()),
				() -> assertEquals(VERIFICATION_CARD_SET_ID, encryptedNodeLongReturnCodeShares.getVerificationCardSetId()),
				() -> assertEquals(expectedListSize, encryptedNodeLongReturnCodeShares.getVerificationCardIds().size(), "Verification card ids size"),
				() -> assertEquals(expectedNodeSize, encryptedNodeLongReturnCodeShares.getNodeReturnCodesValues().size(),
						"Node return codes values size"),
				() -> assertEquals(expectedListSize,
						encryptedNodeLongReturnCodeShares.getNodeReturnCodesValues().get(0).getExponentiatedEncryptedConfirmationKeys().size(),
						"Exponentiated confirmation key size"),
				() -> assertEquals(expectedListSize,
						encryptedNodeLongReturnCodeShares.getNodeReturnCodesValues().get(0).getExponentiatedEncryptedPartialChoiceReturnCodes()
								.size(),
						"Exponentiated partial choice return codes size")
		);
	}

	@Test
	@DisplayName("Combine node contributions with wrong ids throws")
	void combineWithWrongIds() {
		final String expectedMessage = "No node contributions responses.";

		final IllegalStateException ex1 = assertThrows(IllegalStateException.class,
				() -> encryptedNodeLongReturnCodeSharesService.load(WRONG_ID, VERIFICATION_CARD_SET_ID));
		assertTrue(ex1.getMessage().startsWith(expectedMessage));

		final IllegalStateException ex2 = assertThrows(IllegalStateException.class,
				() -> encryptedNodeLongReturnCodeSharesService.load(ELECTION_EVENT_ID, WRONG_ID));
		assertTrue(ex2.getMessage().startsWith(expectedMessage));
	}

	@Test
	@DisplayName("Combine node contributions with invalid signature checks")
	void combineWithInvalidSignature() throws SignatureException {
		when(sdmSignatureKeystoreServiceSdmConfig.verifySignature(any(), any(), any(), any())).thenReturn(false);

		final IllegalStateException ex1 = assertThrows(IllegalStateException.class,
				() -> encryptedNodeLongReturnCodeSharesService.load(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
		assertTrue(ex1.getMessage().startsWith("All return code generation response payloads must have a valid signature."));
	}

	@Test
	@DisplayName("Combine node contributions with different length")
	void combineWithDifferentLengthFromNodes()
			throws SignatureException, URISyntaxException, ResourceNotFoundException, IOException, PayloadStorageException {
		final int expectedListSize = 10;

		prepare("invalid-different-length");
		when(sdmSignatureKeystoreServiceSdmConfig.verifySignature(any(), any(), any(), any())).thenReturn(true);
		when(votingCardSetRepository.getNumberOfVotingCards(any(), any())).thenReturn(expectedListSize);

		final IllegalStateException exception = assertThrows(IllegalStateException.class,
				() -> encryptedNodeLongReturnCodeSharesService.load(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));

		assertTrue(Throwables.getRootCause(exception).getMessage().startsWith(
				"The ControlComponentCodeSharesPayload does not have the same verification card ids as the SetupComponentVerificationDataPayload."));
	}

	@Test
	@DisplayName("Combine node contributions with interrupted chunks")
	void combineWithInterruptedChunks() throws SignatureException, URISyntaxException, IOException, PayloadStorageException {
		prepare("invalid-interrupted-chunks");
		when(sdmSignatureKeystoreServiceSdmConfig.verifySignature(any(), any(), any(), any())).thenReturn(true);

		final IllegalStateException ex1 = assertThrows(IllegalStateException.class,
				() -> encryptedNodeLongReturnCodeSharesService.load(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
		System.out.println(ex1.getMessage());
		assertTrue(ex1.getMessage().startsWith("The chunk ID sequence is interrupted or incomplete."));
	}

	@Nested
	class NodeContributionsResponsesConsistencyTest {

		private static final Random RANDOM = RandomFactory.createRandom();
		private static final String ELECTION_EVENT_ID = RANDOM.genRandomBase16String(32).toLowerCase();
		private static final String VERIFICATION_CARD_SET_ID = RANDOM.genRandomBase16String(32).toLowerCase();
		private static final String WRONG_ID = RANDOM.genRandomBase16String(32).toLowerCase();
		private static final List<String> VERIFICATION_CARD_IDS = List.of(RANDOM.genRandomBase16String(32).toLowerCase(),
				RANDOM.genRandomBase16String(32).toLowerCase(), RANDOM.genRandomBase16String(32).toLowerCase(),
				RANDOM.genRandomBase16String(32).toLowerCase());
		private static final GqGroup ENCRYPTION_GROUP = GroupTestData.getGqGroup();
		private NodeContributionsResponsesService nodeContributionsResponsesService;
		private List<ControlComponentCodeShare> codeSharesChunk0;
		private List<ControlComponentCodeShare> codeSharesChunk1;
		private ControlComponentCodeShare extraCodeShare;
		private List<ControlComponentCodeSharesPayload> nodeContributionsChunk0;
		private List<ControlComponentCodeSharesPayload> otherGroupNodeContributionsChunk0;
		private List<ControlComponentCodeSharesPayload> otherGroupNodeContributionsChunk1;

		@BeforeEach
		void setUp() throws PayloadStorageException {
			nodeContributionsResponsesService = mock(NodeContributionsResponsesService.class);
			encryptedNodeLongReturnCodeSharesService = new EncryptedNodeLongReturnCodeSharesService(nodeContributionsResponsesService,
					sdmSignatureKeystoreServiceSdmConfig, votingCardSetRepository, setupComponentVerificationDataPayloadFileSystemRepository);

			// Construct SetupComponentVerificationDataPayloads
			final ElGamalMultiRecipientCiphertext ciphertext = ElGamalMultiRecipientCiphertext.create(ENCRYPTION_GROUP.getGenerator(),
					List.of(ENCRYPTION_GROUP.getIdentity()));
			final ElGamalMultiRecipientPublicKey pk = new ElGamalMultiRecipientPublicKey(GroupVector.of(ENCRYPTION_GROUP.getGenerator()));
			final List<String> allowList = List.of(Base64.encode(new byte[] { 1, 2 }), Base64.encode(new byte[] { 3, 4 }));
			final List<SetupComponentVerificationData> verificationData = VERIFICATION_CARD_IDS.stream()
					.map(verificationCardId -> new SetupComponentVerificationData(verificationCardId, ciphertext, ciphertext, pk))
					.toList();
			final CombinedCorrectnessInformation combinedCorrectnessInformation = mock(CombinedCorrectnessInformation.class);
			when(combinedCorrectnessInformation.getTotalNumberOfVotingOptions()).thenReturn(1);

			final SetupComponentVerificationDataPayload chunk0 = new SetupComponentVerificationDataPayload(ELECTION_EVENT_ID,
					VERIFICATION_CARD_SET_ID, allowList, 0, ENCRYPTION_GROUP, List.of(verificationData.get(0), verificationData.get(1)),
					combinedCorrectnessInformation);
			final SetupComponentVerificationDataPayload chunk1 = new SetupComponentVerificationDataPayload(ELECTION_EVENT_ID,
					VERIFICATION_CARD_SET_ID, allowList, 1, ENCRYPTION_GROUP, List.of(verificationData.get(2), verificationData.get(3)),
					combinedCorrectnessInformation);

			when(setupComponentVerificationDataPayloadFileSystemRepository.getCount(anyString(), anyString())).thenReturn(2);
			when(setupComponentVerificationDataPayloadFileSystemRepository.retrieve(anyString(), anyString(), eq(0))).thenReturn(chunk0);
			when(setupComponentVerificationDataPayloadFileSystemRepository.retrieve(anyString(), anyString(), eq(1))).thenReturn(chunk1);

			// Construct ControlComponentCodeSharesPayloads
			final ZqGroup zqGroup = ZqGroup.sameOrderAs(ENCRYPTION_GROUP);
			final ExponentiationProof proof = new ExponentiationProof(ZqElement.create(1, zqGroup), ZqElement.create(2, zqGroup));
			final List<ControlComponentCodeShare> controlComponentCodeShares = VERIFICATION_CARD_IDS.stream()
					.map(verificationCardId -> new ControlComponentCodeShare(verificationCardId, pk, pk, ciphertext, proof, ciphertext, proof))
					.toList();
			extraCodeShare = new ControlComponentCodeShare(RANDOM.genRandomBase16String(32).toLowerCase(), pk, pk, ciphertext, proof, ciphertext,
					proof);
			codeSharesChunk0 = List.of(controlComponentCodeShares.get(0), controlComponentCodeShares.get(1));
			codeSharesChunk1 = List.of(controlComponentCodeShares.get(2), controlComponentCodeShares.get(3));
			nodeContributionsChunk0 = NODE_IDS.stream()
					.map(nodeId -> new ControlComponentCodeSharesPayload(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, 0, ENCRYPTION_GROUP,
							codeSharesChunk0, nodeId))
					.toList();

			final GqGroup otherGroup = GroupTestData.getLargeGqGroup();
			final ElGamalMultiRecipientPublicKey otherGroupPk = new ElGamalMultiRecipientPublicKey(GroupVector.of(otherGroup.getGenerator()));
			final ElGamalMultiRecipientCiphertext otherCiphertext = ElGamalMultiRecipientCiphertext.create(otherGroup.getGenerator(),
					List.of(otherGroup.getIdentity()));
			final ZqGroup otherZqGroup = ZqGroup.sameOrderAs(otherGroup);
			final ExponentiationProof otherProof = new ExponentiationProof(ZqElement.create(1, otherZqGroup), ZqElement.create(2, otherZqGroup));
			final List<ControlComponentCodeShare> otherGroupCodeShares = VERIFICATION_CARD_IDS.stream()
					.map(verificationCardId -> new ControlComponentCodeShare(verificationCardId, otherGroupPk, otherGroupPk, otherCiphertext,
							otherProof, otherCiphertext, otherProof))
					.toList();
			otherGroupNodeContributionsChunk0 = NODE_IDS.stream()
					.map(nodeId -> new ControlComponentCodeSharesPayload(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, 0, otherGroup,
							List.of(otherGroupCodeShares.get(0), otherGroupCodeShares.get(1)), nodeId))
					.toList();
			otherGroupNodeContributionsChunk1 = NODE_IDS.stream()
					.map(nodeId -> new ControlComponentCodeSharesPayload(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, 1, otherGroup,
							List.of(otherGroupCodeShares.get(2), otherGroupCodeShares.get(3)), nodeId))
					.toList();
		}

		@Test
		@DisplayName("empty node contribution responses throws IllegalStateException")
		void emptyNodeContributionResponsesThrows() {
			when(nodeContributionsResponsesService.load(anyString(), anyString())).thenReturn(List.of());

			final Throwable e = assertThrows(IllegalStateException.class,
					() -> encryptedNodeLongReturnCodeSharesService.load(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
			assertEquals(String.format("No node contributions responses. [electionEventId: %s, verificationCardSetId: %s]",
					ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID), Throwables.getRootCause(e).getMessage());
		}

		@Test
		@DisplayName("wrong electionEventId in ControlComponentCodeSharesPayload throws IllegalStateException")
		void wrongElectionEventIdThrows() {
			final List<ControlComponentCodeSharesPayload> wrongElectionEventIdChunk = NODE_IDS.stream()
					.map(nodeId -> new ControlComponentCodeSharesPayload(WRONG_ID, VERIFICATION_CARD_SET_ID, 0, ENCRYPTION_GROUP,
							codeSharesChunk0, nodeId))
					.toList();

			when(nodeContributionsResponsesService.load(anyString(), anyString())).thenReturn(List.of(wrongElectionEventIdChunk));

			final Throwable e = assertThrows(IllegalStateException.class,
					() -> encryptedNodeLongReturnCodeSharesService.load(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
			assertEquals(String.format("All return code generation response payloads must be related to the correct election event id and "
							+ "verification card set id. [electionEventId: %s, verificationCardSetId: %s]",
					ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID), Throwables.getRootCause(e).getMessage());
		}

		@Test
		@DisplayName("wrong verificationCardSetId in ControlComponentCodeSharesPayload throws IllegalStateException")
		void wrongVerificationCardSetIdThrows() {
			final List<ControlComponentCodeSharesPayload> wrongVerificationCardSetIdChunk = NODE_IDS.stream()
					.map(nodeId -> new ControlComponentCodeSharesPayload(ELECTION_EVENT_ID, WRONG_ID, 0, ENCRYPTION_GROUP, codeSharesChunk0,
							nodeId))
					.toList();

			when(nodeContributionsResponsesService.load(anyString(), anyString())).thenReturn(List.of(wrongVerificationCardSetIdChunk));

			final Throwable e = assertThrows(IllegalStateException.class,
					() -> encryptedNodeLongReturnCodeSharesService.load(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
			assertEquals(String.format("All return code generation response payloads must be related to the correct election event id and "
							+ "verification card set id. [electionEventId: %s, verificationCardSetId: %s]",
					ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID), Throwables.getRootCause(e).getMessage());
		}

		@Test
		@DisplayName("missing nodes in ControlComponentCodeSharesPayload throws IllegalStateException")
		void missingNodesThrows() {
			final List<ControlComponentCodeSharesPayload> missingNodesChunk = List.of(new ControlComponentCodeSharesPayload(ELECTION_EVENT_ID,
					VERIFICATION_CARD_SET_ID, 0, ENCRYPTION_GROUP, codeSharesChunk0, 1));

			when(nodeContributionsResponsesService.load(anyString(), anyString())).thenReturn(List.of(missingNodesChunk));

			final Throwable e = assertThrows(IllegalStateException.class,
					() -> encryptedNodeLongReturnCodeSharesService.load(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
			assertEquals(String.format("The chunk ID sequence is interrupted or incomplete. [electionEventId: %s, verificationCardSetId: %s]",
					ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID), Throwables.getRootCause(e).getMessage());
		}

		@Test
		@DisplayName("wrong chunkId in ControlComponentCodeSharesPayload throws IllegalStateException")
		void wrongChunkIdThrows() {
			final int wrongChunkId = 3;
			final List<ControlComponentCodeSharesPayload> wrongChunkIdChunk = NODE_IDS.stream()
					.map(nodeId -> new ControlComponentCodeSharesPayload(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, wrongChunkId, ENCRYPTION_GROUP,
							codeSharesChunk0, nodeId))
					.toList();

			when(nodeContributionsResponsesService.load(anyString(), anyString())).thenReturn(List.of(wrongChunkIdChunk));

			final Throwable e = assertThrows(IllegalStateException.class,
					() -> encryptedNodeLongReturnCodeSharesService.load(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
			assertEquals(String.format("The chunk ID sequence is interrupted or incomplete. [electionEventId: %s, verificationCardSetId: %s]",
					ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID), Throwables.getRootCause(e).getMessage());
		}

		@Test
		@DisplayName("different chunk count between ControlComponentCodeShares and SetupComponentVerificationData payloads throws IllegalStateException")
		void differentChunkCountThrows() {
			final List<ControlComponentCodeSharesPayload> wrongChunkCount = NODE_IDS.stream()
					.map(nodeId -> new ControlComponentCodeSharesPayload(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, 0, ENCRYPTION_GROUP,
							codeSharesChunk0, nodeId))
					.toList();

			when(nodeContributionsResponsesService.load(anyString(), anyString())).thenReturn(List.of(wrongChunkCount));

			final Throwable e = assertThrows(IllegalStateException.class,
					() -> encryptedNodeLongReturnCodeSharesService.load(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
			assertEquals(
					String.format("The SetupComponentVerificationDataPayloads and ControlComponentCodeSharesPayloads have different chunk counts. "
									+ "[electionEventId: %s, verificationCardSetId: %s]",
							ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID), Throwables.getRootCause(e).getMessage());
		}

		@Test
		@DisplayName("different GqGroup between ControlComponentCodeShares and SetupComponentVerificationData payloads throws IllegalStateException")
		void differentGqGroupThrows() {
			when(nodeContributionsResponsesService.load(anyString(), anyString())).thenReturn(List.of(otherGroupNodeContributionsChunk0,
					otherGroupNodeContributionsChunk1));

			final Throwable e = assertThrows(IllegalStateException.class,
					() -> encryptedNodeLongReturnCodeSharesService.load(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
			assertTrue(Strings.startsWithIgnoreCase(Throwables.getRootCause(e).getMessage(),
					"The ControlComponentCodeSharesPayload does not have the same encryption group as the SetupComponentVerificationDataPayload."));
		}

		@Test
		@DisplayName("different GqGroup among chunks of ControlComponentCodeShares payloads throws IllegalStateException")
		void differentGqGroupChunksThrows() {
			when(nodeContributionsResponsesService.load(anyString(), anyString())).thenReturn(
					List.of(nodeContributionsChunk0, otherGroupNodeContributionsChunk1));

			final Throwable e = assertThrows(IllegalStateException.class,
					() -> encryptedNodeLongReturnCodeSharesService.load(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
			assertTrue(Strings.startsWithIgnoreCase(Throwables.getRootCause(e).getMessage(),
					"The ControlComponentCodeSharesPayload does not have the same encryption group as the SetupComponentVerificationDataPayload."));
		}

		@Test
		@DisplayName("wrong order of verificationCardIds inside chunks of the ControlComponentCodeShares payloads throws IllegalStateException")
		void wrongVerificationCardIdsOrderInsideChunksThrows() {
			final List<ControlComponentCodeSharesPayload> wrongVerificationCardIdsOrder = NODE_IDS.stream()
					.map(nodeId -> new ControlComponentCodeSharesPayload(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, 1, ENCRYPTION_GROUP,
							List.of(codeSharesChunk1.get(1), codeSharesChunk1.get(0)), nodeId))
					.toList();

			when(nodeContributionsResponsesService.load(anyString(), anyString())).thenReturn(
					List.of(nodeContributionsChunk0, wrongVerificationCardIdsOrder));

			final Throwable e = assertThrows(IllegalStateException.class,
					() -> encryptedNodeLongReturnCodeSharesService.load(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
			assertTrue(Strings.startsWithIgnoreCase(Throwables.getRootCause(e).getMessage(),
					"The ControlComponentCodeSharesPayload does not have the same verification card ids as the SetupComponentVerificationDataPayload."));
		}

		@Test
		@DisplayName("wrong order of verificationCardIds between chunks of the ControlComponentCodeShares payloads throws IllegalStateException")
		void wrongVerificationCardIdsOrderBetweenChunksThrows() {
			final List<ControlComponentCodeSharesPayload> wrongVerificationCardIdsOrderChunk0 = NODE_IDS.stream()
					.map(nodeId -> new ControlComponentCodeSharesPayload(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, 0, ENCRYPTION_GROUP,
							codeSharesChunk1, nodeId))
					.toList();
			final List<ControlComponentCodeSharesPayload> wrongVerificationCardIdsOrderChunk1 = NODE_IDS.stream()
					.map(nodeId -> new ControlComponentCodeSharesPayload(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, 1, ENCRYPTION_GROUP,
							codeSharesChunk0, nodeId))
					.toList();

			when(nodeContributionsResponsesService.load(anyString(), anyString()))
					.thenReturn(List.of(wrongVerificationCardIdsOrderChunk0, wrongVerificationCardIdsOrderChunk1));

			final Throwable e = assertThrows(IllegalStateException.class,
					() -> encryptedNodeLongReturnCodeSharesService.load(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
			assertTrue(Strings.startsWithIgnoreCase(Throwables.getRootCause(e).getMessage(),
					"The ControlComponentCodeSharesPayload does not have the same verification card ids as the SetupComponentVerificationDataPayload."));
		}

		@Test
		@DisplayName("more verificationCardIds in ControlComponentCodeShares than SetupComponentVerificationData payloads throws IllegalStateException")
		void moreVerificationCardIdsThrows() {
			final List<ControlComponentCodeSharesPayload> moreVerificationCardIdsOrderChunk1 = NODE_IDS.stream()
					.map(nodeId -> new ControlComponentCodeSharesPayload(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, 1, ENCRYPTION_GROUP,
							List.of(codeSharesChunk1.get(0), codeSharesChunk1.get(1), extraCodeShare), nodeId))
					.toList();

			when(nodeContributionsResponsesService.load(anyString(), anyString()))
					.thenReturn(List.of(nodeContributionsChunk0, moreVerificationCardIdsOrderChunk1));

			final Throwable e = assertThrows(IllegalStateException.class,
					() -> encryptedNodeLongReturnCodeSharesService.load(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
			assertTrue(Strings.startsWithIgnoreCase(Throwables.getRootCause(e).getMessage(),
					"The ControlComponentCodeSharesPayload does not have the same verification card ids as the SetupComponentVerificationDataPayload."));
		}

		@Test
		@DisplayName("less verificationCardIds in ControlComponentCodeShares than SetupComponentVerificationData payloads throws IllegalStateException")
		void lessVerificationCardIdsThrows() {
			final List<ControlComponentCodeSharesPayload> lessVerificationCardIdsOrderChunk1 = NODE_IDS.stream()
					.map(nodeId -> new ControlComponentCodeSharesPayload(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, 1, ENCRYPTION_GROUP,
							List.of(codeSharesChunk1.get(0)), nodeId))
					.toList();

			when(nodeContributionsResponsesService.load(anyString(), anyString()))
					.thenReturn(List.of(nodeContributionsChunk0, lessVerificationCardIdsOrderChunk1));

			final Throwable e = assertThrows(IllegalStateException.class,
					() -> encryptedNodeLongReturnCodeSharesService.load(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
			assertTrue(Strings.startsWithIgnoreCase(Throwables.getRootCause(e).getMessage(),
					"The ControlComponentCodeSharesPayload does not have the same verification card ids as the SetupComponentVerificationDataPayload."));
		}
	}
}
