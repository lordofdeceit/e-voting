/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.PublicKey;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptolib.api.exceptions.GeneralCryptoLibException;
import ch.post.it.evoting.cryptolib.certificates.utils.PemUtils;
import ch.post.it.evoting.cryptolib.commons.serialization.JsonSignatureService;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.domain.election.BallotBox;
import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.config.commons.domain.common.SignedObject;
import ch.post.it.evoting.securedatamanager.services.application.exception.DatabaseException;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.SynchronizeStatus;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballotbox.BallotBoxRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.service.ConfigurationEntityStatusService;

class BallotBoxServiceTest {

	public static final String BALLOT_BOX_READY = "{\"result\": [{\"id\": \"96e\", \"ballot\" : { " + "\"id\": \"96f\"}, \"status\": \"READY\"}]}";
	private static final String BALLOT_BOX_ID = "96e";
	private static final String BALLOT_ID = "96f";
	private static final String ELECTION_EVENT_ID = "989";

	private static PathResolver pathResolver;
	private static BallotBoxService ballotBoxService;
	private static BallotBoxRepository ballotBoxRepositoryMock;
	private static ConfigurationEntityStatusService statusServiceMock;

	@BeforeAll
	static void setUpAll() {
		pathResolver = mock(PathResolver.class);
		statusServiceMock = mock(ConfigurationEntityStatusService.class);
		ballotBoxRepositoryMock = mock(BallotBoxRepository.class);

		ballotBoxService = new BallotBoxService(DomainObjectMapper.getNewInstance(), pathResolver, ballotBoxRepositoryMock, statusServiceMock);
	}

	@AfterEach
	void tearDown() {
		reset(pathResolver, statusServiceMock, ballotBoxRepositoryMock);
	}

	@Test
	void getBallotBoxesId() {
		// given
		when(ballotBoxRepositoryMock.list(anyMap())).thenReturn(BALLOT_BOX_READY);
		when(pathResolver.resolveBallotBoxPath(any(), any(), any())).thenReturn(Paths.get(
				"src/test/resources/ballotboxservice/" + ELECTION_EVENT_ID + "/ONLINE/electionInformation/ballots/" + BALLOT_ID + "/ballotBoxes/"
						+ BALLOT_BOX_ID));
		when(statusServiceMock.updateWithSynchronizedStatus(BallotBoxStatus.SIGNED.name(), BALLOT_BOX_ID, ballotBoxRepositoryMock, SynchronizeStatus.PENDING))
				.thenReturn("");

		// when
		final List<String> ballotBoxes = ballotBoxService.getBallotBoxesId("12e590cc85ad49af96b15ca761dfe49d");

		// then
		Assertions.assertThat(ballotBoxes)
				.isNotNull()
				.hasSize(1)
				.containsExactly("96e");
	}

	@Test
	void sign() throws IOException, GeneralCryptoLibException {
		when(ballotBoxRepositoryMock.list(anyMap())).thenReturn(BALLOT_BOX_READY);
		when(pathResolver.resolveBallotBoxPath(any(), any(), any())).thenReturn(Paths.get(
				"src/test/resources/ballotboxservice/" + ELECTION_EVENT_ID + "/ONLINE/electionInformation/ballots/" + BALLOT_ID + "/ballotBoxes/"
						+ BALLOT_BOX_ID));
		when(statusServiceMock.updateWithSynchronizedStatus(BallotBoxStatus.SIGNED.name(), BALLOT_BOX_ID, ballotBoxRepositoryMock, SynchronizeStatus.PENDING))
				.thenReturn("");

		assertDoesNotThrow(() -> ballotBoxService.sign(ELECTION_EVENT_ID, BALLOT_BOX_ID, SigningTestData.PRIVATE_KEY_PEM));

		final Path outputPath = Paths
				.get("src/test/resources/ballotboxservice/989/ONLINE/electionInformation" + "/ballots/" + BALLOT_ID + "/ballotBoxes/"
						+ BALLOT_BOX_ID);
		final Path signedBallotBox = Paths.get(outputPath.toString(), Constants.CONFIG_FILE_NAME_SIGNED_BALLOTBOX_JSON);

		assertTrue(Files.exists(signedBallotBox));

		final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
		final SignedObject signedBallotBoxObject = objectMapper.readValue(signedBallotBox.toFile(), SignedObject.class);
		final String signatureBallotBox = signedBallotBoxObject.getSignature();

		final PublicKey publicKey = PemUtils.publicKeyFromPem(SigningTestData.PUBLIC_KEY_PEM);

		JsonSignatureService.verify(publicKey, signatureBallotBox, BallotBox.class);

		Files.deleteIfExists(signedBallotBox);
	}

	@Test
	void throwExceptionWhenBallotBoxIsLocked() {
		when(ballotBoxRepositoryMock.list(any())).thenThrow(DatabaseException.class);

		assertThrows(DatabaseException.class, () -> ballotBoxService.sign(ELECTION_EVENT_ID, BALLOT_BOX_ID, SigningTestData.PRIVATE_KEY_PEM));
	}

}
