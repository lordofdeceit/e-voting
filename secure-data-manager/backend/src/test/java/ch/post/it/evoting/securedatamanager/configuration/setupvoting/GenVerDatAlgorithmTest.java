/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement.PrimeGqElementFactory;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.domain.SecurityLevelTestingOnly;
import ch.post.it.evoting.securedatamanager.VotingCardSetServiceTestBase;
import ch.post.it.evoting.securedatamanager.commons.VerificationCardSet;

/**
 * Tests of GenVerDatAlgorithm.
 */
@SecurityLevelTestingOnly
@DisplayName("GenVerDatAlgorithm")
@ExtendWith(MockitoExtension.class)
class GenVerDatAlgorithmTest extends VotingCardSetServiceTestBase {

	private static final List<String> actualVotingOptions = List.of("actualVotingOption1", "actualVotingOption2", "actualVotingOption3");

	private static final Random random = RandomFactory.createRandom();

	@InjectMocks
	private GenVerDatAlgorithm genVerDatAlgorithm;
	private static VerificationCardSet verificationCardSet;
	private static ElGamalMultiRecipientPublicKey setupPublicKey;
	private static GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions;

	@Test
	@DisplayName("correctly orders allow list")
	void orderAllowList() {
		final List<String> allowList = asList("fm9i32f", "9sdfjl==", "as2sdf", "77sdfk");

		genVerDatAlgorithm.order(allowList);

		final List<String> expectedOrder = asList("77sdfk", "9sdfjl==", "as2sdf", "fm9i32f");
		assertEquals(expectedOrder, allowList);
	}

	@BeforeAll
	static void  setup() {
		final BigInteger p = BigInteger.valueOf(59L);
		final BigInteger q = BigInteger.valueOf(29L);
		final BigInteger g = BigInteger.valueOf(3L);
		final GqGroup gqGroup = new GqGroup(p, q, g);

		final PrimeGqElement five = PrimeGqElementFactory.fromValue(5, gqGroup);
		final PrimeGqElement seven = PrimeGqElementFactory.fromValue(7, gqGroup);
		final PrimeGqElement seventeen = PrimeGqElementFactory.fromValue(17, gqGroup);
		encodedVotingOptions = GroupVector.of(five, seven, seventeen);

		final String adminBoardId = random.genRandomBase16String(32).toLowerCase();
		final String ballotBoxId = random.genRandomBase16String(32).toLowerCase();
		final String electionEventId = random.genRandomBase16String(32).toLowerCase();
		final String verificationCardSetId = random.genRandomBase16String(32).toLowerCase();
		final String votingCardSetId = random.genRandomBase16String(32).toLowerCase();
		verificationCardSet = new VerificationCardSet(adminBoardId, ballotBoxId, electionEventId, verificationCardSetId,
				votingCardSetId);

		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
		setupPublicKey = elGamalGenerator.genRandomPublicKey(2);

	}

	@Test
	@DisplayName("calling genVerDat with encoded voting option greater than secret key size throws IllegalArgumentException")
	void generatorEncodedVotingOptionGreaterSecretKesThrows() {

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> genVerDatAlgorithm.genVerDat(2, encodedVotingOptions, actualVotingOptions, verificationCardSet, setupPublicKey));
		assertEquals("The number of voting options must be smaller than or equal to the setup secret key length.",
				Throwables.getRootCause(exception).getMessage());
	}

	@ParameterizedTest
	@MethodSource("generateActualVotingOptionsData")
	void testInvalidVotingOptions(List<String> actualVotingOptions ) {
		IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> genVerDatAlgorithm.genVerDat(2, encodedVotingOptions, actualVotingOptions, verificationCardSet, setupPublicKey));
		assertEquals(String.format("Voting options should match a valid xml xs:token",
				actualVotingOptions), Throwables.getRootCause(illegalArgumentException).getMessage());
	}

	static Stream<Arguments> generateActualVotingOptionsData() {
		return Stream.of(
				Arguments.of(List.of("a  b", "b", "c")),
				Arguments.of(List.of("x", "<xyz>", "z"))
		);
	}

	@Test
	void invalidActualVotingOptionsLengthCheck() {
		List<String> fiftyOne = List.of("a","b","c", "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXY");
		IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> genVerDatAlgorithm.genVerDat(2, encodedVotingOptions, fiftyOne, verificationCardSet, setupPublicKey));
		assertEquals(String.format("The actual voting option length must not exceed 50 characters.",
				actualVotingOptions), Throwables.getRootCause(illegalArgumentException).getMessage());
	}
}
