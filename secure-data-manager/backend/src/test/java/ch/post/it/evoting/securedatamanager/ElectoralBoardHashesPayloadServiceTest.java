/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.SignatureException;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.io.TempDir;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.configuration.ElectoralBoardHashesPayload;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@DisplayName("An ElectoralBoardHashesPayloadService")
class ElectoralBoardHashesPayloadServiceTest {

	private static final String ELECTION_EVENT_ID = "df384f4747a0819718c22cc57ed8183d";
	private static final String MISSING_ELECTION_EVENT_ID = "414bd34dcf6e4de4b771a92fa3849d3d";
	private static final String INVALID_SIGNATURE_ELECTION_EVENT_ID = "b771a92fa3849d3d414bd34dcf6e4de4";
	private static final String INVALID_ID = "invalidId";

	private static ObjectMapper objectMapper;
	private static PathResolver pathResolver;
	private static ElectoralBoardHashesPayloadService electoralBoardHashesPayloadService;
	private static ElectoralBoardHashesPayload electoralBoardHashesPayload;

	@BeforeAll
	static void setUpAll(
			@TempDir
			final Path tempDir) throws IOException {
		objectMapper = DomainObjectMapper.getNewInstance();

		Files.createDirectories(tempDir.resolve("sdm/config").resolve(ELECTION_EVENT_ID).resolve("OFFLINE"));

		final PathResolver pathResolver = new PathResolver(tempDir.toString());

		final ElectoralBoardHashesPayloadFileRepository electoralBoardHashesPayloadFileRepository =
				new ElectoralBoardHashesPayloadFileRepository(objectMapper, pathResolver);

		electoralBoardHashesPayload = validElectoralBoardHashesPayload();
		electoralBoardHashesPayloadFileRepository.save(electoralBoardHashesPayload);

		electoralBoardHashesPayloadService = new ElectoralBoardHashesPayloadService(electoralBoardHashesPayloadFileRepository
		);
	}

	private static ElectoralBoardHashesPayload validElectoralBoardHashesPayload() {
		final List<byte[]> electoralBoardHashes = List.of("Password_EB1_2".getBytes(StandardCharsets.UTF_8),
				"Password_EB2_2".getBytes(StandardCharsets.UTF_8));
		final ElectoralBoardHashesPayload electoralBoardHashesPayload =
				new ElectoralBoardHashesPayload(ELECTION_EVENT_ID, electoralBoardHashes);

		final byte[] signature = new byte[] { 1, 2 };
		final CryptoPrimitivesSignature electoralBoardHashesPayloadSignature = new CryptoPrimitivesSignature(signature);
		electoralBoardHashesPayload.setSignature(electoralBoardHashesPayloadSignature);

		return electoralBoardHashesPayload;
	}

	@Nested
	@DisplayName("saving")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class SaveTest {

		private List<byte[]> electoralBoardHashes;
		private ElectoralBoardHashesPayload electoralBoardHashesPayload;

		private ElectoralBoardHashesPayloadService electoralBoardHashesPayloadServiceTemp;

		@BeforeAll
		void setUpAll(
				@TempDir
				final Path tempDir) throws IOException {

			Files.createDirectories(tempDir.resolve("sdm/config").resolve(ELECTION_EVENT_ID).resolve("OFFLINE"));

			pathResolver = new PathResolver(tempDir.toString());
			final ElectoralBoardHashesPayloadFileRepository electoralBoardHashesPayloadFileRepositoryTemp = new ElectoralBoardHashesPayloadFileRepository(
					objectMapper, pathResolver);

			electoralBoardHashesPayloadServiceTemp = new ElectoralBoardHashesPayloadService(electoralBoardHashesPayloadFileRepositoryTemp
			);
		}

		@BeforeEach
		void setUp() {
			electoralBoardHashes = List.of(new byte[] { 9 }, new byte[] { 2 });
			electoralBoardHashesPayload = new ElectoralBoardHashesPayload(ELECTION_EVENT_ID, electoralBoardHashes);
		}

		@Test
		@DisplayName("a valid payload does not throw")
		void saveValidPayload() throws SignatureException {
			assertDoesNotThrow(() -> electoralBoardHashesPayloadServiceTemp.save(electoralBoardHashesPayload));

			assertTrue(Files.exists(
					pathResolver.resolveOfflinePath(ELECTION_EVENT_ID).resolve(ElectoralBoardHashesPayloadFileRepository.PAYLOAD_FILE_NAME)));
		}

		@Test
		@DisplayName("a invalid election event id or electoral board hashes throws")
		void saveNullPayload() {
			assertThrows(NullPointerException.class, () -> electoralBoardHashesPayloadServiceTemp.save(null));
		}
	}

	@Nested
	@DisplayName("calling exist")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class ExistTest {

		@Test
		@DisplayName("for valid election event returns true")
		void existValidElectionEvent() {
			assertTrue(electoralBoardHashesPayloadService.exist(ELECTION_EVENT_ID));
		}

		@Test
		@DisplayName("for invalid election event id throws FailedValidationException")
		void existInvalidElectionEvent() {
			assertThrows(FailedValidationException.class, () -> electoralBoardHashesPayloadService.exist(INVALID_ID));
		}

		@Test
		@DisplayName("for non existing election event returns false")
		void existNonExistingElectionEvent() {
			assertFalse(electoralBoardHashesPayloadService.exist(MISSING_ELECTION_EVENT_ID));
		}

	}

	@Nested
	@DisplayName("loading")
	@TestInstance(TestInstance.Lifecycle.PER_METHOD)
	class LoadTest {

		@Test
		@DisplayName("existing election event with valid signature returns expected electoral board hashes")
		void loadExistingElectionEventValidSignature() {
			final ElectoralBoardHashesPayload electoralBoardHashesPayload = electoralBoardHashesPayloadService.load(ELECTION_EVENT_ID);
			final List<byte[]> electoralBoardHashes = electoralBoardHashesPayload.getElectoralBoardHashes();

			assertNotNull(electoralBoardHashesPayload);
			assertArrayEquals(ElectoralBoardHashesPayloadServiceTest.electoralBoardHashesPayload.getElectoralBoardHashes().get(0),
					electoralBoardHashes.get(0));
			assertArrayEquals(ElectoralBoardHashesPayloadServiceTest.electoralBoardHashesPayload.getElectoralBoardHashes().get(1),
					electoralBoardHashes.get(1));
		}

		@Test
		@DisplayName("invalid election event id throws FailedValidationException")
		void loadInvalidElectionEventId() {
			assertThrows(NullPointerException.class, () -> electoralBoardHashesPayloadService.load(null));
			assertThrows(FailedValidationException.class, () -> electoralBoardHashesPayloadService.load(INVALID_ID));
		}

		@Test
		@DisplayName("existing election event with missing payload throws IllegalStateException")
		void loadMissingPayload() {
			final IllegalStateException exception = assertThrows(IllegalStateException.class,
					() -> electoralBoardHashesPayloadService.load(MISSING_ELECTION_EVENT_ID));

			final String errorMessage = String.format("Requested electoral board hashes payload is not present. [electionEventId: %s]",
					MISSING_ELECTION_EVENT_ID);
			assertEquals(errorMessage, Throwables.getRootCause(exception).getMessage());
		}

	}

}
