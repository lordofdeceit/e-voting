/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import static java.time.LocalDate.now;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.stream.Stream;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.signing.AuthorityInformation;
import ch.post.it.evoting.cryptoprimitives.signing.GenKeysAndCert;
import ch.post.it.evoting.cryptoprimitives.signing.KeysAndCert;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureFactory;

public final class KeystoreFilesCreator {

	private static final Random random = RandomFactory.createRandom();

	private KeystoreFilesCreator() {
		// utility class should not be instantiated
	}

	public static void create(final String keystoreLocation, final String keystorePasswordLocation, final String alias) {
		try {
			final String password = random.genRandomBase64String(20);
			final KeyStore keystore = createKeystore(password, alias);

			try (final FileOutputStream fosPassword = new FileOutputStream(keystorePasswordLocation);
					final FileOutputStream fosKeystore = new FileOutputStream(keystoreLocation)) {

				fosPassword.write(password.getBytes(StandardCharsets.UTF_8));
				keystore.store(fosKeystore, password.toCharArray());
			}
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static KeyStore createKeystore(final String password, final String alias)
			throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException {

		final KeysAndCert keyAndCertificate = generatePrivateKeyAndCertificate();
		final KeyStore keyStore = KeyStore.getInstance("PKCS12");
		keyStore.load(null, password.toCharArray());
		keyStore.setKeyEntry(alias, keyAndCertificate.privateKey(), "".toCharArray(), new Certificate[] { keyAndCertificate.certificate() });

		createEntriesForOtherComponents(keyStore, alias);

		return keyStore;
	}

	private static void createEntriesForOtherComponents(final KeyStore keyStore, final String excludedAlias) {
		Stream.of(Alias.values()).filter(alias -> !alias.get().equals(excludedAlias)).forEach(alias -> {
			final KeysAndCert keyAndCertificate = generatePrivateKeyAndCertificate();
			try {
				keyStore.setKeyEntry(alias.get(), keyAndCertificate.privateKey(), "".toCharArray(),
						new Certificate[] { keyAndCertificate.certificate() });
			} catch (KeyStoreException e) {
				throw new RuntimeException(e);
			}
		});
	}

	private static KeysAndCert generatePrivateKeyAndCertificate() {
		final SignatureFactory factory = SignatureFactory.getInstance();
		final GenKeysAndCert digitalSignatures = factory.createGenKeysAndCert(
				AuthorityInformation.builder().setCountry("dummy-C").setCommonName("dummy-Cn").setOrganisation("dummy-O").setLocality("dummy-L")
						.setState("dummy-St").build());
		return digitalSignatures.genKeysAndCert(now(), now().plusDays(1));
	}
}
