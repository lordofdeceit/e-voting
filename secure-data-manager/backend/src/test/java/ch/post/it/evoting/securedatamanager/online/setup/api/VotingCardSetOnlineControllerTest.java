/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.setup.api;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.security.SignatureException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;

import ch.post.it.evoting.securedatamanager.api.online.VotingCardSetOnlineController;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.application.service.VotingCardSetComputationService;
import ch.post.it.evoting.securedatamanager.services.application.service.VotingCardSetDownloadService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.PayloadStorageException;

@ExtendWith(MockitoExtension.class)
@ContextConfiguration
class VotingCardSetOnlineControllerTest {

	private final String VALID_ELECTION_EVENT_ID = "17ccbe962cf341bc93208c26e911090c";
	private final String VALID_VOTING_CARD_SET_ID = "17ccbe962cf341bc93208c26e911090c";

	@Mock
	VotingCardSetDownloadService votingCardSetDownloadService;

	@Mock
	VotingCardSetComputationService votingCardSetComputationService;
	@InjectMocks
	VotingCardSetOnlineController votingCardSetOnlineController;

	@Test
	void testCorrectStatusTransition()
			throws PayloadStorageException, ResourceNotFoundException, IOException, SignatureException {

		final HttpStatus statusCodeComputing = votingCardSetOnlineController.setVotingCardSetStatusComputing(VALID_ELECTION_EVENT_ID,
				VALID_VOTING_CARD_SET_ID).getStatusCode();
		assertEquals(HttpStatus.NO_CONTENT, statusCodeComputing);

		final HttpStatus statusCodeDownloaded = votingCardSetOnlineController.setVotingCardSetStatusDownloaded(VALID_ELECTION_EVENT_ID,
				VALID_VOTING_CARD_SET_ID).getStatusCode();
		assertEquals(HttpStatus.NO_CONTENT, statusCodeDownloaded);
	}
}
