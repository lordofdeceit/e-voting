/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.internal.securitylevel.SecurityLevelConfig;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqElement.GqElementFactory;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.cryptoprimitives.test.tools.serialization.JsonData;
import ch.post.it.evoting.cryptoprimitives.test.tools.serialization.TestParameters;

/**
 * Tests of QuadraticResidueToWriteInAlgorithm.
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DisplayName("quadraticResidueToWriteIn with")
class QuadraticResidueToWriteInAlgorithmTest extends TestGroupSetup {

	private QuadraticResidueToWriteInAlgorithm quadraticResidueToWriteInAlgorithm;

	@BeforeAll
	void setUp() {
		final IntegerToWriteInAlgorithm integerToWriteInAlgorithm = new IntegerToWriteInAlgorithm();
		quadraticResidueToWriteInAlgorithm = new QuadraticResidueToWriteInAlgorithm(integerToWriteInAlgorithm);
	}

	@Test
	@DisplayName("null input throw NullPointerException")
	void verifyQrToWriteInWithNullValues() {
		assertThrows(NullPointerException.class, () -> quadraticResidueToWriteInAlgorithm.quadraticResidueToWriteIn(null));
	}

	@ParameterizedTest()
	@MethodSource("jsonFileArgumentProvider")
	@DisplayName("with real values gives expected result")
	void verifyQrToWriteInWithRealValues(final GqElement y, final String expected, final String description) {
		final String actual = quadraticResidueToWriteInAlgorithm.quadraticResidueToWriteIn(y);

		assertEquals(expected, actual, String.format("assertion failed for: %s", description));
	}

	private Stream<Arguments> jsonFileArgumentProvider() throws IOException {
		final URL url = QuadraticResidueToWriteInAlgorithmTest.class.getResource("/toWriteIn/qr-to-write-in.json");
		final List<TestParameters> parametersList = Arrays.asList(DomainObjectMapper.getNewInstance().readValue(url, TestParameters[].class));

		return parametersList.stream().parallel().map(testParameters -> {
			try (MockedStatic<SecurityLevelConfig> mockedSecurityLevel = Mockito.mockStatic(SecurityLevelConfig.class)) {
				mockedSecurityLevel.when(SecurityLevelConfig::getSystemSecurityLevel).thenReturn(testParameters.getSecurityLevel());

				// Context.
				final JsonData context = testParameters.getContext();
				final BigInteger p = context.get("p", BigInteger.class);
				final BigInteger q = context.get("q", BigInteger.class);
				final BigInteger g = context.get("g", BigInteger.class);
				final GqGroup gqGroup = new GqGroup(p, q, g);

				// Input.
				final JsonData input = testParameters.getInput();
				final GqElement y = GqElementFactory.fromValue(input.get("y", BigInteger.class), gqGroup);

				// Output.
				final JsonData output = testParameters.getOutput();
				final String s = output.get("output", String.class);

				return Arguments.of(y, s, testParameters.getDescription());
			}
		});
	}

}
