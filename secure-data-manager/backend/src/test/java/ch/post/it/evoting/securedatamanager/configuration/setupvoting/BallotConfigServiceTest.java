/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.Security;

import javax.json.JsonObject;

import org.apache.commons.io.IOUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ch.post.it.evoting.securedatamanager.config.commons.utils.JsonUtils;
import ch.post.it.evoting.securedatamanager.services.application.exception.DatabaseException;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.application.service.SigningTestData;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballot.BallotRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballottext.BallotTextRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.service.ConfigurationEntityStatusService;

import uk.org.webcompere.systemstubs.environment.EnvironmentVariables;
import uk.org.webcompere.systemstubs.jupiter.SystemStub;
import uk.org.webcompere.systemstubs.jupiter.SystemStubsExtension;

@ExtendWith({ MockitoExtension.class, SystemStubsExtension.class })
class BallotConfigServiceTest {

	private static final String ELECTION_EVENT_ID = "bf346e85f64747dda4f37a64439bc942";

	private static String JSON_BALLOT_WITHOUT_REPRESENTATIONS;
	private static String BALLOT_TEXT;

	@SystemStub
	private static EnvironmentVariables environmentVariables;

	@InjectMocks
	private BallotConfigService ballotConfigService;

	@Mock
	private ConfigurationEntityStatusService statusServiceMock;

	@Mock
	private BallotRepository ballotRepositoryMock;

	@Mock
	private BallotTextRepository ballotTextRepositoryMock;

	@Mock
	private BallotUpdateService ballotUpdateService;

	@BeforeAll
	public static void init() throws IOException {

		environmentVariables.set("SECURITY_LEVEL", "TESTING_ONLY");

		final ClassLoader classLoader = BallotConfigServiceTest.class.getClassLoader();
		JSON_BALLOT_WITHOUT_REPRESENTATIONS = IOUtils
				.toString(classLoader.getResource("ballot_result_without_representations.json").openStream(), StandardCharsets.UTF_8);
		BALLOT_TEXT = IOUtils.toString(classLoader.getResource("ballotText_result.json").openStream(), StandardCharsets.UTF_8);

		Security.addProvider(new BouncyCastleProvider());
	}

	@Test
	void sign() {

		final String electionEventId = "db033b3f729c45719db8aba15d24043c";
		final String ballotId = "8365b61d3f194c11bbd03e421462b6ad";
		final String value = """
				{
				      "id": "37169153d9ed42b5add2862149452a78",
				      "electionEvent": {
				        "id": "101549c5a4a04c7b88a0cb9be8ab3df7"
				      },
				      "status": "SIGNED",
				      "synchronized": "true",
				      "defaultTitle": "Test e2e 20161013 CC 345",
				      "defaultDescription": "03",
				      "alias": "345",
				      "contests": [
				        {
				          "id": "5611fd41eb7546408d459d01308a1824",
				          "defaultTitle": "Test e2e 20161013 CC 345",
				          "defaultDescription": "03",
				          "alias": "345",
				          "electionEvent": {
				            "id": "101549c5a4a04c7b88a0cb9be8ab3df6"
				          },
				          "type": "votation",
				          "options": [
				            {
				              "id": "4fecc1d05f254d808fa61f047f79253b",
				              "defaultText": "eyJhbHRlcm5hdGUiOnsidGl0bGUiOiJNbWUuIiwic3VybmFtZSI6IkxFR0lTTEFUSVZFIFMgMDMwMSIsIm5hbWUiOiJQcsOpbm9tIFMgMDMwMSJ9LCJidWxsZXRpbiI6bnVsbCwiY2lyY3VsYXIiOm51bGwsImNhbmRpZGF0ZSI6eyJ0aXRsZSI6Ik0uIiwic3VybmFtZSI6IkxFR0lTTEFUSVZFIDAzMDEiLCJuYW1lIjoiUHLDqW5vbSAwMzAxIn0sIm9yZGVyIjoxfQ==",
				              "alias": "031",
				              "representation": "401",
				              "attributes": [
				                "a51a0d572f1d47668f3debe879234d01"
				              ]
				            },
				            {
				              "id": "a29d118a5bea43128be604419d6db4e7",
				              "defaultText": "eyJhbHRlcm5hdGUiOnsidGl0bGUiOiJNbWUuIiwic3VybmFtZSI6IkxFR0lTTEFUSVZFIFMgMDMwMiIsIm5hbWUiOiJQcsOpbm9tIFMgMDMwMiJ9LCJidWxsZXRpbiI6bnVsbCwiY2lyY3VsYXIiOm51bGwsImNhbmRpZGF0ZSI6eyJ0aXRsZSI6Ik0uIiwic3VybmFtZSI6IkxFR0lTTEFUSVZFIDAzMDIiLCJuYW1lIjoiUHLDqW5vbSAwMzAyIn0sIm9yZGVyIjoyfQ==",
				              "alias": "032",
				              "representation": "463",
				              "attributes": [
				                "a51a0d572f1d47668f3debe879234d01"
				              ]
				            },
				            {
				              "id": "fc60537a97a64919b1d4cc52b34ac54d",
				              "defaultText": "eyJhbHRlcm5hdGUiOnsidGl0bGUiOiJNbWUuIiwic3VybmFtZSI6IkxFR0lTTEFUSVZFIFMgMDMwMyIsIm5hbWUiOiJQcsOpbm9tIFMgMDMwMyJ9LCJidWxsZXRpbiI6bnVsbCwiY2lyY3VsYXIiOm51bGwsImNhbmRpZGF0ZSI6eyJ0aXRsZSI6Ik0uIiwic3VybmFtZSI6IkxFR0lTTEFUSVZFIDAzMDMiLCJuYW1lIjoiUHLDqW5vbSAwMzAzIn0sIm9yZGVyIjozfQ==",
				              "alias": "033",
				              "representation": "373",
				              "attributes": [
				                "a51a0d572f1d47668f3debe879234d01"
				              ]
				            },
				            {
				              "id": "861f51dc7f6e4843bd79d45902a1f0e7",
				              "defaultText": "eyJhbHRlcm5hdGUiOnsidGl0bGUiOiJNbWUuIiwic3VybmFtZSI6IkxFR0lTTEFUSVZFIFMgMDMwNCIsIm5hbWUiOiJQcsOpbm9tIFMgMDMwNCJ9LCJidWxsZXRpbiI6bnVsbCwiY2lyY3VsYXIiOm51bGwsImNhbmRpZGF0ZSI6eyJ0aXRsZSI6Ik1tZS4iLCJzdXJuYW1lIjoiTEVHSVNMQVRJVkUgMDMwNCIsIm5hbWUiOiJQcsOpbm9tIDAzMDQifSwib3JkZXIiOjR9",
				              "alias": "034",
				              "representation": "509",
				              "attributes": [
				                "a51a0d572f1d47668f3debe879234d01"
				              ]
				            },
				            {
				              "id": "d194fa20339d49779e82028fefc7547f",
				              "defaultText": "eyJhbHRlcm5hdGUiOnsidGl0bGUiOiJNLiIsInN1cm5hbWUiOiJMRUdJU0xBVElWRSBTIDAzMDUiLCJuYW1lIjoiUHLDqW5vbSBTIDAzMDUifSwiYnVsbGV0aW4iOm51bGwsImNpcmN1bGFyIjpudWxsLCJjYW5kaWRhdGUiOnsidGl0bGUiOiJNLiIsInN1cm5hbWUiOiJMRUdJU0xBVElWRSAwMzA1IiwibmFtZSI6IlByw6lub20gMDMwNSJ9LCJvcmRlciI6NX0=",
				              "alias": "035",
				              "representation": "461",
				              "attributes": [
				                "a51a0d572f1d47668f3debe879234d01"
				              ]
				            },
				            {
				              "id": "f99b3bb8c59245e9b48bc7a0959c44a5",
				              "defaultText": "eyJibGFuayI6IDEsICJ0ZXh0IjogIlZvdGUgYmxhbmMiIH0=",
				              "alias": "BLANK",
				              "representation": "541",
				              "attributes": [
				                "a51a0d572f1d47668f3debe879234d01"
				              ]
				            }
				          ],
				          "optionsAttributes": [
				            {
				              "id": "a51a0d572f1d47668f3debe879234d01",
				              "defaultText": "hola",
				              "type": "group",
				              "alias": "Test_Question_1",
				              "correctness": true
				            }
				          ],
				          "clauses": [
				            {
				              "id": "54bad8abe5fb4ed5a8c5b5e2e4764aff",
				              "refersTo": "a51a0d572f1d47668f3debe879234d01",
				              "type": "MIN",
				              "value": 1
				            },
				            {
				              "id": "ac79f099b7224bd080d00b740382c972",
				              "refersTo": "a51a0d572f1d47668f3debe879234d01",
				              "type": "MAX",
				              "value": 1
				            }
				          ]
				        }
				      ],
				      "details": "Synchronized",
				      "ballotBoxes": "56,70,72,85,119,152,176,177,198,203,222",
				      "signedObject": "eyJhbGciOiJQUzI1NiJ9.eyJvYmplY3RUb1NpZ24iOiJ7XCJpZFwiOlwiMzcxNjkxNTNkOWVkNDJiNWFkZDI4NjIxNDk0NTJhNzhcIixcImVsZWN0aW9uRXZlbnRcIjp7XCJpZFwiOlwiMTAxNTQ5YzVhNGEwNGM3Yjg4YTBjYjliZThhYjNkZjZcIn0sXCJkZWZhdWx0VGl0bGVcIjpcIlRlc3QgZTJlIDIwMTYxMDEzIENDIDM0NVwiLFwiZGVmYXVsdERlc2NyaXB0aW9uXCI6XCIwM1wiLFwiYWxpYXNcIjpcIjM0NVwiLFwiY29udGVzdHNcIjpbe1wiaWRcIjpcIjU2MTFmZDQxZWI3NTQ2NDA4ZDQ1OWQwMTMwOGExODI0XCIsXCJkZWZhdWx0VGl0bGVcIjpcIlRlc3QgZTJlIDIwMTYxMDEzIENDIDM0NVwiLFwiZGVmYXVsdERlc2NyaXB0aW9uXCI6XCIwM1wiLFwiYWxpYXNcIjpcIjM0NVwiLFwiZWxlY3Rpb25FdmVudFwiOntcImlkXCI6XCIxMDE1NDljNWE0YTA0YzdiODhhMGNiOWJlOGFiM2RmNlwifSxcInR5cGVcIjpcInZvdGF0aW9uXCIsXCJvcHRpb25zXCI6W3tcImlkXCI6XCI0ZmVjYzFkMDVmMjU0ZDgwOGZhNjFmMDQ3Zjc5MjUzYlwiLFwiZGVmYXVsdFRleHRcIjpcImV5SmhiSFJsY201aGRHVWlPbnNpZEdsMGJHVWlPaUpOYldVdUlpd2ljM1Z5Ym1GdFpTSTZJa3hGUjBsVFRFRlVTVlpGSUZNZ01ETXdNU0lzSW01aGJXVWlPaUpRY3NPcGJtOXRJRk1nTURNd01TSjlMQ0ppZFd4c1pYUnBiaUk2Ym5Wc2JDd2lZMmx5WTNWc1lYSWlPbTUxYkd3c0ltTmhibVJwWkdGMFpTSTZleUowYVhSc1pTSTZJazB1SWl3aWMzVnlibUZ0WlNJNklreEZSMGxUVEVGVVNWWkZJREF6TURFaUxDSnVZVzFsSWpvaVVITERxVzV2YlNBd016QXhJbjBzSW05eVpHVnlJam94ZlE9PVwiLFwiYWxpYXNcIjpcIjAzMVwiLFwicmVwcmVzZW50YXRpb25cIjpcIjQwMVwiLFwiYXR0cmlidXRlc1wiOltcImE1MWEwZDU3MmYxZDQ3NjY4ZjNkZWJlODc5MjM0ZDAxXCJdfSx7XCJpZFwiOlwiYTI5ZDExOGE1YmVhNDMxMjhiZTYwNDQxOWQ2ZGI0ZTdcIixcImRlZmF1bHRUZXh0XCI6XCJleUpoYkhSbGNtNWhkR1VpT25zaWRHbDBiR1VpT2lKTmJXVXVJaXdpYzNWeWJtRnRaU0k2SWt4RlIwbFRURUZVU1ZaRklGTWdNRE13TWlJc0ltNWhiV1VpT2lKUWNzT3BibTl0SUZNZ01ETXdNaUo5TENKaWRXeHNaWFJwYmlJNmJuVnNiQ3dpWTJseVkzVnNZWElpT201MWJHd3NJbU5oYm1ScFpHRjBaU0k2ZXlKMGFYUnNaU0k2SWswdUlpd2ljM1Z5Ym1GdFpTSTZJa3hGUjBsVFRFRlVTVlpGSURBek1ESWlMQ0p1WVcxbElqb2lVSExEcVc1dmJTQXdNekF5SW4wc0ltOXlaR1Z5SWpveWZRPT1cIixcImFsaWFzXCI6XCIwMzJcIixcInJlcHJlc2VudGF0aW9uXCI6XCI0NjNcIixcImF0dHJpYnV0ZXNcIjpbXCJhNTFhMGQ1NzJmMWQ0NzY2OGYzZGViZTg3OTIzNGQwMVwiXX0se1wiaWRcIjpcImZjNjA1MzdhOTdhNjQ5MTliMWQ0Y2M1MmIzNGFjNTRkXCIsXCJkZWZhdWx0VGV4dFwiOlwiZXlKaGJIUmxjbTVoZEdVaU9uc2lkR2wwYkdVaU9pSk5iV1V1SWl3aWMzVnlibUZ0WlNJNklreEZSMGxUVEVGVVNWWkZJRk1nTURNd015SXNJbTVoYldVaU9pSlFjc09wYm05dElGTWdNRE13TXlKOUxDSmlkV3hzWlhScGJpSTZiblZzYkN3aVkybHlZM1ZzWVhJaU9tNTFiR3dzSW1OaGJtUnBaR0YwWlNJNmV5SjBhWFJzWlNJNklrMHVJaXdpYzNWeWJtRnRaU0k2SWt4RlIwbFRURUZVU1ZaRklEQXpNRE1pTENKdVlXMWxJam9pVUhMRHFXNXZiU0F3TXpBekluMHNJbTl5WkdWeUlqb3pmUT09XCIsXCJhbGlhc1wiOlwiMDMzXCIsXCJyZXByZXNlbnRhdGlvblwiOlwiMzczXCIsXCJhdHRyaWJ1dGVzXCI6W1wiYTUxYTBkNTcyZjFkNDc2NjhmM2RlYmU4NzkyMzRkMDFcIl19LHtcImlkXCI6XCI4NjFmNTFkYzdmNmU0ODQzYmQ3OWQ0NTkwMmExZjBlN1wiLFwiZGVmYXVsdFRleHRcIjpcImV5SmhiSFJsY201aGRHVWlPbnNpZEdsMGJHVWlPaUpOYldVdUlpd2ljM1Z5Ym1GdFpTSTZJa3hGUjBsVFRFRlVTVlpGSUZNZ01ETXdOQ0lzSW01aGJXVWlPaUpRY3NPcGJtOXRJRk1nTURNd05DSjlMQ0ppZFd4c1pYUnBiaUk2Ym5Wc2JDd2lZMmx5WTNWc1lYSWlPbTUxYkd3c0ltTmhibVJwWkdGMFpTSTZleUowYVhSc1pTSTZJazF0WlM0aUxDSnpkWEp1WVcxbElqb2lURVZIU1ZOTVFWUkpWa1VnTURNd05DSXNJbTVoYldVaU9pSlFjc09wYm05dElEQXpNRFFpZlN3aWIzSmtaWElpT2pSOVwiLFwiYWxpYXNcIjpcIjAzNFwiLFwicmVwcmVzZW50YXRpb25cIjpcIjUwOVwiLFwiYXR0cmlidXRlc1wiOltcImE1MWEwZDU3MmYxZDQ3NjY4ZjNkZWJlODc5MjM0ZDAxXCJdfSx7XCJpZFwiOlwiZDE5NGZhMjAzMzlkNDk3NzllODIwMjhmZWZjNzU0N2ZcIixcImRlZmF1bHRUZXh0XCI6XCJleUpoYkhSbGNtNWhkR1VpT25zaWRHbDBiR1VpT2lKTkxpSXNJbk4xY201aGJXVWlPaUpNUlVkSlUweEJWRWxXUlNCVElEQXpNRFVpTENKdVlXMWxJam9pVUhMRHFXNXZiU0JUSURBek1EVWlmU3dpWW5Wc2JHVjBhVzRpT201MWJHd3NJbU5wY21OMWJHRnlJanB1ZFd4c0xDSmpZVzVrYVdSaGRHVWlPbnNpZEdsMGJHVWlPaUpOTGlJc0luTjFjbTVoYldVaU9pSk1SVWRKVTB4QlZFbFdSU0F3TXpBMUlpd2libUZ0WlNJNklsQnl3Nmx1YjIwZ01ETXdOU0o5TENKdmNtUmxjaUk2TlgwPVwiLFwiYWxpYXNcIjpcIjAzNVwiLFwicmVwcmVzZW50YXRpb25cIjpcIjQ2MVwiLFwiYXR0cmlidXRlc1wiOltcImE1MWEwZDU3MmYxZDQ3NjY4ZjNkZWJlODc5MjM0ZDAxXCJdfSx7XCJpZFwiOlwiZjk5YjNiYjhjNTkyNDVlOWI0OGJjN2EwOTU5YzQ0YTVcIixcImRlZmF1bHRUZXh0XCI6XCJleUppYkdGdWF5STZJREVzSUNKMFpYaDBJam9nSWxadmRHVWdZbXhoYm1NaUlIMD1cIixcImFsaWFzXCI6XCJCTEFOS1wiLFwicmVwcmVzZW50YXRpb25cIjpcIjU0MVwiLFwiYXR0cmlidXRlc1wiOltcImE1MWEwZDU3MmYxZDQ3NjY4ZjNkZWJlODc5MjM0ZDAxXCJdfV0sXCJvcHRpb25zQXR0cmlidXRlc1wiOlt7XCJpZFwiOlwiYTUxYTBkNTcyZjFkNDc2NjhmM2RlYmU4NzkyMzRkMDFcIixcImRlZmF1bHRUZXh0XCI6XCJob2xhXCIsXCJ0eXBlXCI6XCJncm91cFwiLFwiYWxpYXNcIjpcIk1hZWRpX1F1ZXN0aW9uXzFcIixcImNvcnJlY3RuZXNzXCI6dHJ1ZX1dLFwiY2xhdXNlc1wiOlt7XCJpZFwiOlwiNTRiYWQ4YWJlNWZiNGVkNWE4YzViNWUyZTQ3NjRhZmZcIixcInJlZmVyc1RvXCI6XCJhNTFhMGQ1NzJmMWQ0NzY2OGYzZGViZTg3OTIzNGQwMVwiLFwidHlwZVwiOlwiTUlOXCIsXCJ2YWx1ZVwiOjF9LHtcImlkXCI6XCJhYzc5ZjA5OWI3MjI0YmQwODBkMDBiNzQwMzgyYzk3MlwiLFwicmVmZXJzVG9cIjpcImE1MWEwZDU3MmYxZDQ3NjY4ZjNkZWJlODc5MjM0ZDAxXCIsXCJ0eXBlXCI6XCJNQVhcIixcInZhbHVlXCI6MX1dfV0sXCJiYWxsb3RCb3hlc1wiOlwiNTYsNzAsNzIsODUsMTE5LDE1MiwxNzYsMTc3LDE5OCwyMDMsMjIyXCJ9In0.fRFKtBZM1hITGYghQ9bVhG4f4yMF3R7yzbi2NDsjPNzbXJV8sZVfEcRFBX9l4JljVXoFOkozmEij-BBDBmYkH0i3UDZWpfRDbJTc7nFw7dfNK0LqbLTAksXZ-tEuYf9Yx2_Sh54Y8JH4iPmHlHrVygqN7E0SAao6HMLtnCyGtdutgC5ekaOU4SrCOFu59aNm6IODeE5x8OlTGDmzUFpWK3DrR39Uz1KCNRpvXiq51yec_kjyOmqM9izjIcsrz6joeA9pOQg59z8PjqGXj4zeQkLXQKUOnWLoZxFB92TKYa8_i1PcJtYMcO7MjNHe1x1QmFKdx8fq1f4DiNfpq_dNtA"
				    }
					""";
		final JsonObject jsonObject = JsonUtils.getJsonObject(value);

		when(ballotUpdateService.updateOptionsRepresentation(anyString(), any())).thenReturn(jsonObject);
		when(ballotUpdateService.updateBlankVotingOptionsAlias(anyString(), any())).thenReturn(jsonObject);

		when(ballotRepositoryMock.list(anyMap())).thenReturn(JSON_BALLOT_WITHOUT_REPRESENTATIONS);
		when(ballotTextRepositoryMock.list(anyMap())).thenReturn(BALLOT_TEXT);

		assertDoesNotThrow(() -> ballotConfigService.sign(electionEventId, ballotId, SigningTestData.PRIVATE_KEY_PEM));
	}

	@Test
	void signFails() {
		final String ballotId = "8365b61d3f194c11bbd03e421462b6ad";

		when(ballotRepositoryMock.list(anyMap())).thenThrow(DatabaseException.class);

		assertThrows(DatabaseException.class, () -> ballotConfigService.sign(ELECTION_EVENT_ID, ballotId, SigningTestData.PRIVATE_KEY_PEM));
	}

	@Test
	void signEmptyJsonBallot() {
		final String ballotId = "8365b61d3f194c11bbd03e421462b6ad";

		when(ballotRepositoryMock.list(anyMap())).thenReturn("{}");

		assertThrows(ResourceNotFoundException.class, () -> ballotConfigService.sign(ELECTION_EVENT_ID, ballotId, SigningTestData.PRIVATE_KEY_PEM));
	}

	@Test
	void signEmptyStringBallot() {
		final String ballotId = "8365b61d3f194c11bbd03e421462b6ad";

		when(ballotRepositoryMock.list(anyMap())).thenReturn("");

		assertThrows(ResourceNotFoundException.class, () -> ballotConfigService.sign(ELECTION_EVENT_ID, ballotId, SigningTestData.PRIVATE_KEY_PEM));
	}

	@Test
	void signNullBallot() {
		final String ballotId = "8365b61d3f194c11bbd03e421462b6ad";

		when(ballotRepositoryMock.list(anyMap())).thenReturn(null);

		assertThrows(ResourceNotFoundException.class, () -> ballotConfigService.sign(ELECTION_EVENT_ID, ballotId, SigningTestData.PRIVATE_KEY_PEM));
	}
}
