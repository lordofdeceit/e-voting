/*
 * (c) Copyright 2022 Swiss Post Ltd
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.domain.SecurityLevelTestingOnly;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@SecurityLevelTestingOnly
@DisplayName("PrintingDataGenerationService")
class PrintingDataGenerationServiceTest {

	private static final String ELECTION_EVENT_ID = "2df0f3440bf04b9fb229813a561604ba";
	private static final String VOTING_CARD_SET = "1b930ec49ca44e19b6b132fd2d4a4b76";
	private static final String VOTING_CARD_SET_WRONG_FORMAT = "8e39964f1f754911a8907c1d5e8eff1f";
	private static final String VOTING_CARD_SET_NULL_VOTER_RETURN_CODES = "520462a9bec04aa2a15ee77e1dec4000";

	private static PrintingDataGenerationService printingDataGenerationService;

	@BeforeAll
	static void setUpAll() throws URISyntaxException {
		final Path path = Paths.get(PrintingDataGenerationServiceTest.class.getResource("/PrintingDataGenerationService/").toURI());

		final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
		final PathResolver pathResolver = new PathResolver(path.toString());

		final VoterReturnCodesPayloadFileRepository voterReturnCodesPayloadFileRepository = new VoterReturnCodesPayloadFileRepository(objectMapper,
				pathResolver);
		final VoterReturnCodesPayloadService voterReturnCodesPayloadService = new VoterReturnCodesPayloadService(
				voterReturnCodesPayloadFileRepository);
		printingDataGenerationService = new PrintingDataGenerationService(pathResolver, voterReturnCodesPayloadService);
	}

	@Test
	@DisplayName("calling generate with valid values does not throw")
	void generate() {
		assertDoesNotThrow(() -> printingDataGenerationService.generate(ELECTION_EVENT_ID, VOTING_CARD_SET));
	}

	@Test
	@DisplayName("calling generate with wrong partial line format throws")
	void generateWrongPartialLineThrows() {
		final IllegalStateException illegalStateException = assertThrows(IllegalStateException.class,
				() -> printingDataGenerationService.generate(ELECTION_EVENT_ID, VOTING_CARD_SET_WRONG_FORMAT));

		assertTrue(illegalStateException.getMessage().startsWith("Partial printing data line does not match expected format."));
	}

	@Test
	@DisplayName("calling generate with a null VoterReturnCodes throws")
	void generateNullVoterReturnCodesPartialLineThrows() {
		final IllegalStateException illegalStateException = assertThrows(IllegalStateException.class,
				() -> printingDataGenerationService.generate(ELECTION_EVENT_ID, VOTING_CARD_SET_NULL_VOTER_RETURN_CODES));

		assertTrue(illegalStateException.getMessage().startsWith("VoterReturnCodes must not be null."));
	}
}