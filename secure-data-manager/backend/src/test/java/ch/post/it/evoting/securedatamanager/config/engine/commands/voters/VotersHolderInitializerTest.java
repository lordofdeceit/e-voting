/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.config.engine.commands.voters;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Properties;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptolib.extendedkeystore.service.ExtendedKeyStoreService;
import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.securedatamanager.EncryptionParametersPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.EncryptionParametersPayloadService;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.config.commons.readers.ConfigurationInputReader;
import ch.post.it.evoting.securedatamanager.config.commons.utils.ConfigObjectMapper;
import ch.post.it.evoting.securedatamanager.config.commons.utils.X509CertificateLoader;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

class VotersHolderInitializerTest {

	private static final String ELECTION_EVENT_ID = "be658216a38b421b943457846c6a68f9";
	private static final String NUMBER_VOTING_CARDS = "10";
	private static final String BALLOT_ID = "1234";
	private static final String BALLOT_BOX_ID = "5678";
	private static final String VOTING_CARD_SET_ID = "111111";
	private static final String VERIFICATION_CARD_SET_ID = "111111";
	private static final String VOTING_CARD_SET_ALIAS = "TESTALIAS";
	private static final String ELECTORAL_AUTHORITY_ID = "222222";
	private static final String PLATFORM_ROOT_CA_PEM = "platformRootCAPem";
	private static final String BASE_PATH = "/votingCardSet/";
	private static final String OUTPUT_PATH = "output";
	private static final String ENRICHED_BALLOT_PATH = "input/enrichedBallot.json";
	private static final String TEST_EE_PROPS = "input/input.properties";

	private static ConfigObjectMapper configObjectMapper;
	private static EncryptionParametersPayloadService encryptionParametersPayloadService;
	private static ConfigurationInputReader configurationInputReader;
	private static ExtendedKeyStoreService extendedKeyStoreService;
	private static X509CertificateLoader x509CertificateLoader;
	private static Path root;
	private static ObjectMapper objectMapper;

	@BeforeAll
	public static void setUp() throws Exception {
		configObjectMapper = new ConfigObjectMapper();
		configurationInputReader = new ConfigurationInputReader();
		root = Path.of(VotersHolderInitializerTest.class.getResource(BASE_PATH).toURI());
		objectMapper = DomainObjectMapper.getNewInstance();
		final Path encryptionParametersPath = Path.of(VotersHolderInitializerTest.class.getResource("/encryptionParametersTest").toURI());
		final PathResolver pathResolver = new PathResolver(encryptionParametersPath.toString());
		final EncryptionParametersPayloadFileRepository encryptionParametersRepository = new EncryptionParametersPayloadFileRepository(pathResolver,
				objectMapper);

		encryptionParametersPayloadService = new EncryptionParametersPayloadService(encryptionParametersRepository);
		extendedKeyStoreService = new ExtendedKeyStoreService();
		x509CertificateLoader = new X509CertificateLoader();
	}

	@Test
	void adaptTheParametersCorrectly() throws Exception {

		final VotersHolderInitializer votersHolderInitializer = new VotersHolderInitializer(configurationInputReader, x509CertificateLoader,
				extendedKeyStoreService, encryptionParametersPayloadService);

		final Properties props = new Properties();
		final Path path = Path.of(root.toString(), TEST_EE_PROPS);
		try (final BufferedReader bufferedReader = Files.newBufferedReader(path)) {
			props.load(bufferedReader);
		}

		final String start = (String) props.get("start");
		final int validityPeriod = Integer.parseInt((String) props.get("validityPeriod"));

		final ZonedDateTime startValidityPeriod = ZonedDateTime.now(ZoneOffset.UTC);
		final ZonedDateTime electionStartDate = ZonedDateTime.ofInstant(Instant.parse(start), ZoneOffset.UTC);
		final ZonedDateTime endValidityPeriod = electionStartDate.plusYears(validityPeriod);

		VotersParametersHolder holder = new VotersParametersHolder(Integer.parseInt(NUMBER_VOTING_CARDS), BALLOT_ID, getBallot(root), BALLOT_BOX_ID,
				VOTING_CARD_SET_ID, VERIFICATION_CARD_SET_ID, ELECTORAL_AUTHORITY_ID,
				Path.of(root.toString(), OUTPUT_PATH, "sdm", "config", ELECTION_EVENT_ID),
				ELECTION_EVENT_ID, startValidityPeriod, endValidityPeriod, VOTING_CARD_SET_ALIAS, PLATFORM_ROOT_CA_PEM, null);

		holder = votersHolderInitializer.init(holder, getConfigurationFile(root));

		assertNotNull(holder.getVotingCardCredentialInputDataPack());
		assertEquals("auth_sign", holder.getVotingCardCredentialInputDataPack().getCredentialAuthProperties().getAlias().get("privateKey"));
		assertNotNull(holder.getVotingCardSetCredentialInputDataPack());
	}

	private Ballot getBallot(final Path root) {
		final File ballotFile = Path.of(root.toString(), ENRICHED_BALLOT_PATH).toAbsolutePath().toFile();

		return getBallotFromFile(ENRICHED_BALLOT_PATH, ballotFile);
	}

	private Ballot getBallotFromFile(final String ballotPath, final File ballotFile) {
		try {
			return configObjectMapper.fromJSONFileToJava(ballotFile, Ballot.class);
		} catch (final IOException e) {
			throw new IllegalArgumentException("An error occurred while mapping \"" + ballotPath + "\" to a Ballot: " + e.getMessage());
		}
	}

	private File getConfigurationFile(final Path root) {
		return Path.of(root.toString(), "..", Constants.KEYS_CONFIG_FILENAME).toFile();
	}
}
