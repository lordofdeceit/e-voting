/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.importexport;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.services.application.service.SignaturesVerifierService;

@ExtendWith(MockitoExtension.class)
class ImportExportSignatureServiceTest {

	private static final String EXPORT = "export";
	private static final char[] PASSWORD = new char[] { '2', '2', '2', '2', '2', '2' };
	@Mock
	private SignaturesVerifierService signaturesVerifierService;
	@TempDir
	private Path workingDirectory;

	private ImportExportSignatureService importExportSignatureService;

	@BeforeEach
	void setUp() throws URISyntaxException {
		final Path keyStoreOnlinePath = getPathOfFileInResources(EXPORT).resolve(Constants.INTEGRATION_KEYSTORE_ONLINE_FILE);
		importExportSignatureService = new ImportExportSignatureService(signaturesVerifierService, keyStoreOnlinePath.toString());
	}

	@Test
	void signAndVerify() throws IOException {

		// given
		final Path file = Files.createFile(workingDirectory.resolve("file-to-sign.txt"));

		// when
		importExportSignatureService.signFile(file, PASSWORD);

		// then
		Assertions.assertDoesNotThrow(() -> importExportSignatureService.verifyFile(file));
	}

	private static Path getPathOfFileInResources(final String path) throws URISyntaxException {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		URL resource = classLoader.getResource(path);
		return Paths.get(resource.toURI());
	}
}