/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.api;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;

import ch.post.it.evoting.securedatamanager.api.setup.VotingCardSetSetupController;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.application.service.IdleStatusService;
import ch.post.it.evoting.securedatamanager.services.application.service.VotingCardSetPrecomputationService;
import ch.post.it.evoting.securedatamanager.services.application.service.VotingCardSetPreparationService;
import ch.post.it.evoting.securedatamanager.services.application.service.VotingCardSetSignService;
import ch.post.it.evoting.securedatamanager.services.domain.model.votingcardset.VotingCardSetUpdateInputData;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.PayloadStorageException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

@ExtendWith(MockitoExtension.class)
@ContextConfiguration
class VotingCardSetSetupControllerTest {

	private final String VALID_ELECTION_EVENT_ID = "17ccbe962cf341bc93208c26e911090c";
	private final String VALID_VOTING_CARD_SET_ID = "17ccbe962cf341bc93208c26e911090c";

	@Mock
	IdleStatusService idleStatusService;

	@Mock
	VotingCardSetRepository votingCardSetRepository;

	@Mock
	VotingCardSetPrecomputationService votingCardSetPrecomputationService;

	@Mock
	VotingCardSetSignService votingCardSetSignService;

	@Mock
	VotingCardSetPreparationService votingCardSetPreparationService;

	@InjectMocks
	VotingCardSetSetupController votingCardSetSetupController;

	@Test
	void testCorrectStatusTransition()
			throws PayloadStorageException, ResourceNotFoundException {

		final HttpStatus statusCodePrecomputed = votingCardSetSetupController.setVotingCardSetStatusPrecomputed(VALID_ELECTION_EVENT_ID,
				VALID_VOTING_CARD_SET_ID, new VotingCardSetUpdateInputData(null, null)).getStatusCode();
		assertEquals(HttpStatus.NO_CONTENT, statusCodePrecomputed);
	}
}
