/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.mixoffline;

import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS;
import static ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement.PrimeGqElementFactory;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.security.SecureRandom;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.securedatamanager.commons.Constants;

class VerifyVotingClientProofsContextTest {

	private final int MIN_SElECTABLE_VOTING_OPTIONS = 1;

	private final Random rand = RandomFactory.createRandom();
	private final SecureRandom srand = new SecureRandom();

	private GqGroup encryptionGroup;
	private String electionEventId;
	private PrimesMappingTable pTable;

	@BeforeEach
	void setup() {
		encryptionGroup = GroupTestData.getGqGroup();
		electionEventId = rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase();
		pTable = PrimesMappingTable.from(
				List.of(new PrimesMappingTableEntry("57a30570-1722-3a7e-a8f9-7dd643d7f33",
						PrimeGqElementFactory.getSmallPrimeGroupMembers(encryptionGroup, 1).get(0))));
	}

	@Test
	@DisplayName("Valid parameters do not throw")
	void validParametersTest() {
		final int selectableVotingOptions = srand.nextInt(MIN_SElECTABLE_VOTING_OPTIONS, MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS + 1);
		final int allowedWriteIns = srand.nextInt(1, Integer.MAX_VALUE);

		final VerifyVotingClientProofsContext context = assertDoesNotThrow(() -> new VerifyVotingClientProofsContext.Builder()
				.setEncryptionGroup(encryptionGroup)
				.setElectionEventId(electionEventId)
				.setNumberOfSelectableVotingOptions(selectableVotingOptions)
				.setNumberOfAllowedWriteInsPlusOne(allowedWriteIns)
				.setPrimesMappingTable(pTable)
				.build());

		assertEquals(context.getElectionEventId(), electionEventId, "Context electionEventId no equal");
		assertEquals(context.getNumberOfSelectableVotingOptions(), selectableVotingOptions, "Context numberOfSelectableVotingOptions no equal");
		assertEquals(context.getNumberOfAllowedWriteInsPlusOne(), allowedWriteIns, "Context numberOfAllowedWriteInsPlusOne no equal");
	}

	@Nested
	@DisplayName("Throws NullPointerException when")
	class NullArgumentTest {
		private final int selectableVotingOptions = srand.nextInt(MIN_SElECTABLE_VOTING_OPTIONS, MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS + 1);
		private final int allowedWriteIns = srand.nextInt(1, Integer.MAX_VALUE);

		@Test
		@DisplayName("all arguments are null")
		void constructWithNullArguments() {
			final VerifyVotingClientProofsContext.Builder builder = new VerifyVotingClientProofsContext.Builder();

			final NullPointerException ex = assertThrows(NullPointerException.class, builder::build);

			assertNull(ex.getMessage());
		}

		@Test
		@DisplayName("encryptionGroup is null")
		void constructWithoutEncryptionGroup() {
			final VerifyVotingClientProofsContext.Builder builder = new VerifyVotingClientProofsContext.Builder()
					.setElectionEventId(electionEventId)
					.setNumberOfSelectableVotingOptions(selectableVotingOptions)
					.setNumberOfAllowedWriteInsPlusOne(allowedWriteIns)
					.setPrimesMappingTable(pTable);

			final NullPointerException ex = assertThrows(NullPointerException.class, builder::build);

			assertNull(ex.getMessage());
		}

		@Test
		@DisplayName("electionEventId is null")
		void constructWithoutElectionEventId() {
			final VerifyVotingClientProofsContext.Builder builder = new VerifyVotingClientProofsContext.Builder()
					.setEncryptionGroup(encryptionGroup)
					.setNumberOfSelectableVotingOptions(selectableVotingOptions)
					.setNumberOfAllowedWriteInsPlusOne(allowedWriteIns)
					.setPrimesMappingTable(pTable);

			final NullPointerException ex = assertThrows(NullPointerException.class, builder::build);

			assertNull(ex.getMessage());
		}

		@Test
		@DisplayName("pTable is null")
		void constructWithoutPrimesMappingTable() {
			final VerifyVotingClientProofsContext.Builder builder = new VerifyVotingClientProofsContext.Builder()
					.setEncryptionGroup(encryptionGroup)
					.setElectionEventId(electionEventId)
					.setNumberOfSelectableVotingOptions(selectableVotingOptions)
					.setNumberOfAllowedWriteInsPlusOne(allowedWriteIns);

			final NullPointerException ex = assertThrows(NullPointerException.class, builder::build);

			assertNull(ex.getMessage());
		}
	}

	@Nested
	@DisplayName("Throws IllegalArgumentException when")
	class InvalidArgumentTest {
		private final int numberOfSelectableVotingOptions = srand.nextInt(MIN_SElECTABLE_VOTING_OPTIONS,
				MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS + 1);
		private final int numberOfAllowedWriteInsPlusOne = srand.nextInt(1, Integer.MAX_VALUE);

		@Test
		@DisplayName("electionEventId is invalid UUID")
		void invalidElectionEventIdTest() {
			final String invalidElectionEventId = electionEventId.toUpperCase();

			final VerifyVotingClientProofsContext.Builder builder = new VerifyVotingClientProofsContext.Builder()
					.setEncryptionGroup(encryptionGroup)
					.setElectionEventId(invalidElectionEventId)
					.setNumberOfSelectableVotingOptions(numberOfSelectableVotingOptions)
					.setNumberOfAllowedWriteInsPlusOne(numberOfAllowedWriteInsPlusOne)
					.setPrimesMappingTable(pTable);

			final FailedValidationException ex = assertThrows(FailedValidationException.class, builder::build);

			final String expectedMessage =
					String.format("The given string does not comply with the required format. [string: %s, format: ^[0123456789abcdef]{32}$].",
							invalidElectionEventId);

			assertEquals(expectedMessage, ex.getMessage());

		}

		@Test
		@DisplayName("numberOfSelectableVotingOptions is not between [0, 120]")
		void invalidNumberOfSelectableVotingOptionsTest() {
			final VerifyVotingClientProofsContext.Builder builder = new VerifyVotingClientProofsContext.Builder()
					.setEncryptionGroup(encryptionGroup)
					.setElectionEventId(electionEventId)
					.setNumberOfAllowedWriteInsPlusOne(numberOfAllowedWriteInsPlusOne)
					.setPrimesMappingTable(pTable);

			assertAll(
					() -> {
						final VerifyVotingClientProofsContext.Builder invalidBuilder = builder.setNumberOfSelectableVotingOptions(
								MIN_SElECTABLE_VOTING_OPTIONS - 1);
						final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, invalidBuilder::build);

						assertEquals(String.format("The number of selectable voting options should be greater or equal to one. [psi: %s]",
								MIN_SElECTABLE_VOTING_OPTIONS - 1), ex.getMessage());
					},
					() -> {
						final VerifyVotingClientProofsContext.Builder invalidBuilder = builder.setNumberOfSelectableVotingOptions(
								MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS + 1);
						final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, invalidBuilder::build);

						assertEquals(String.format("The number of selectable voting options should be smaller or equal to %s. [psi: %s]",
								MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS, MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS + 1), ex.getMessage());
					}
			);
		}

		@Test
		@DisplayName("numberOfAllowedWriteInsPlusOne < 1")
		void invalidNumberOfAllowedWriteInsPlusOneTest() {
			final VerifyVotingClientProofsContext.Builder builder = new VerifyVotingClientProofsContext.Builder()
					.setEncryptionGroup(encryptionGroup)
					.setElectionEventId(electionEventId)
					.setNumberOfSelectableVotingOptions(numberOfSelectableVotingOptions)
					.setPrimesMappingTable(pTable);

			assertAll(
					() -> {
						final VerifyVotingClientProofsContext.Builder invalidBuilder = builder.setNumberOfAllowedWriteInsPlusOne(-1);
						final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, invalidBuilder::build);

						assertEquals(
								String.format("The number of allowed write ins plus one must be a strictly positive number. [delta_hat: %s]", -1),
								ex.getMessage());
					},
					() -> {
						final VerifyVotingClientProofsContext.Builder invalidBuilder = builder.setNumberOfAllowedWriteInsPlusOne(0);
						final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, invalidBuilder::build);

						assertEquals(String.format("The number of allowed write ins plus one must be a strictly positive number. [delta_hat: %s]", 0),
								ex.getMessage());
					},
					() -> {
						final VerifyVotingClientProofsContext.Builder invalidBuilder = builder.setNumberOfAllowedWriteInsPlusOne(1);
						assertDoesNotThrow(invalidBuilder::build);
					}
			);
		}

		@Test
		@DisplayName("encryptionGroup different from primesMappingTable entries' group")
		void incompatibleEncryptionAndPrimesMappingTableGroups() {
			final GqGroup differentGqGroup = GroupTestData.getDifferentGqGroup(encryptionGroup);
			final VerifyVotingClientProofsContext.Builder invalidBuilder = new VerifyVotingClientProofsContext.Builder()
					.setEncryptionGroup(differentGqGroup)
					.setElectionEventId(electionEventId)
					.setNumberOfSelectableVotingOptions(numberOfSelectableVotingOptions)
					.setNumberOfAllowedWriteInsPlusOne(numberOfAllowedWriteInsPlusOne)
					.setPrimesMappingTable(pTable);
			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, invalidBuilder::build);
			assertEquals(
					String.format("The primes mapping table's entries must belong to the encryption group. [encryptionGroup: %s]", differentGqGroup),
					exception.getMessage());
		}
	}
}
