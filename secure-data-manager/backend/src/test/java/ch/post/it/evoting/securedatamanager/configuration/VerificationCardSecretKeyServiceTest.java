/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration;

import static ch.post.it.evoting.securedatamanager.commons.Constants.CONFIG_FILE_NAME_VERIFICATION_CARD_SECRET_KEY;
import static ch.post.it.evoting.securedatamanager.commons.Constants.CONFIG_VERIFICATION_CARD_SECRET_KEYS_DIRECTORY;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.io.TempDir;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.domain.SecurityLevelTestingOnly;
import ch.post.it.evoting.domain.SerializationUtils;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.VerificationCardSecretKey;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@SecurityLevelTestingOnly
@DisplayName("A VerificationCardSecretKeyService")
class VerificationCardSecretKeyServiceTest {

	private static final Random random = RandomFactory.createRandom();

	private static final String ELECTION_EVENT_ID = random.genRandomBase16String(32).toLowerCase();
	private static final String MISSING_ELECTION_EVENT_ID = random.genRandomBase16String(32).toLowerCase();
	private static final String EXISTING_ELECTION_EVENT_ID = random.genRandomBase16String(32).toLowerCase();
	private static final String VERIFICATION_CARD_SET_ID = random.genRandomBase16String(32).toLowerCase();
	private static final String VERIFICATION_CARD_ID = random.genRandomBase16String(32).toLowerCase();
	private static final String INVALID_ID = "invalidId";

	private static ObjectMapper objectMapper;
	private static PathResolver pathResolver;
	private static VerificationCardSecretKeyService verificationCardSecretKeyService;

	@BeforeAll
	static void setUpAll(
			@TempDir
			final Path tempDir) throws IOException {
		objectMapper = DomainObjectMapper.getNewInstance();

		createDirectories(tempDir, ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID);

		final PathResolver pathResolver = new PathResolver(tempDir.toString());

		final VerificationCardSecretKeyFileRepository verificationCardSecretKeyFileRepository =
				new VerificationCardSecretKeyFileRepository(objectMapper, pathResolver);

		final VerificationCardSecretKey verificationCardSecretKey = validVerificationCardSecretKeyPayload(ELECTION_EVENT_ID,
				VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID);
		verificationCardSecretKeyFileRepository.save(verificationCardSecretKey);

		verificationCardSecretKeyService = new VerificationCardSecretKeyService(verificationCardSecretKeyFileRepository);
	}

	private static VerificationCardSecretKey validVerificationCardSecretKeyPayload(final String electionEventId,
			final String verificationCardSetId, final String verificationCardId) {
		return new VerificationCardSecretKey(electionEventId, verificationCardSetId, verificationCardId, SerializationUtils.getPrivateKey(),
				SerializationUtils.getGqGroup());
	}

	private static void createDirectories(final Path tempDir, final String electionEventId, final String verificationCardSetId) throws IOException {
		Files.createDirectories(tempDir.resolve("sdm/config").resolve(electionEventId).resolve("OFFLINE")
				.resolve(CONFIG_VERIFICATION_CARD_SECRET_KEYS_DIRECTORY).resolve(verificationCardSetId));
	}

	@Nested
	@DisplayName("saving")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class SaveTest {

		private VerificationCardSecretKey verificationCardSecretKey;

		private VerificationCardSecretKeyService verificationCardSecretKeyServiceTemp;

		@BeforeAll
		void setUpAll(
				@TempDir
				final Path tempDir) throws IOException {

			createDirectories(tempDir, ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID);

			pathResolver = new PathResolver(tempDir.toString());

			final VerificationCardSecretKeyFileRepository verificationCardSecretKeyFileRepositoryTemp =
					new VerificationCardSecretKeyFileRepository(objectMapper, pathResolver);

			verificationCardSecretKeyServiceTemp = new VerificationCardSecretKeyService(
					verificationCardSecretKeyFileRepositoryTemp);
		}

		@BeforeEach
		void setUp() {
			verificationCardSecretKey = validVerificationCardSecretKeyPayload(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID,
					VERIFICATION_CARD_ID);
		}

		@Test
		@DisplayName("a valid payload does not throw")
		void saveValidPayload() {
			assertDoesNotThrow(() -> verificationCardSecretKeyServiceTemp.save(verificationCardSecretKey));

			assertTrue(Files.exists(pathResolver.resolveOfflinePath(ELECTION_EVENT_ID)
					.resolve(CONFIG_VERIFICATION_CARD_SECRET_KEYS_DIRECTORY).resolve(VERIFICATION_CARD_SET_ID)
					.resolve(String.format(CONFIG_FILE_NAME_VERIFICATION_CARD_SECRET_KEY, VERIFICATION_CARD_ID))));
		}

		@Test
		@DisplayName("a null verification card secret key throws")
		void saveNullPayload() {
			assertThrows(NullPointerException.class, () -> verificationCardSecretKeyServiceTemp.save(null));
		}
	}

	@Nested
	@DisplayName("calling exist")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class ExistTest {

		@Test
		@DisplayName("for valid election event and verification card set returns true")
		void existValidElectionEvent() {
			assertTrue(verificationCardSecretKeyService.exist(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID));
		}

		@Test
		@DisplayName("for null input throws NullPointerException")
		void existNullInput() {
			assertThrows(NullPointerException.class,
					() -> verificationCardSecretKeyService.exist(null, VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID));
			assertThrows(NullPointerException.class,
					() -> verificationCardSecretKeyService.exist(EXISTING_ELECTION_EVENT_ID, null, VERIFICATION_CARD_ID));
			assertThrows(NullPointerException.class,
					() -> verificationCardSecretKeyService.exist(EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, null));

		}

		@Test
		@DisplayName("for invalid input throws FailedValidationException")
		void existInvalidInput() {
			assertThrows(FailedValidationException.class,
					() -> verificationCardSecretKeyService.exist(INVALID_ID, VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID));
			assertThrows(FailedValidationException.class,
					() -> verificationCardSecretKeyService.exist(EXISTING_ELECTION_EVENT_ID, INVALID_ID, VERIFICATION_CARD_ID));
			assertThrows(FailedValidationException.class,
					() -> verificationCardSecretKeyService.exist(EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, INVALID_ID));
		}

		@Test
		@DisplayName("for non existing election event returns false")
		void existNonExistingElectionEvent() {
			assertFalse(verificationCardSecretKeyService.exist(MISSING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID));
		}

	}

	@Nested
	@DisplayName("loading")
	@TestInstance(TestInstance.Lifecycle.PER_METHOD)
	class LoadTest {

		@Test
		@DisplayName("existing election event and verification card set returns expected verification card secret key")
		void loadExistingElectionEventValidSignature() {
			final VerificationCardSecretKey verificationCardSecretKey = verificationCardSecretKeyService.load(ELECTION_EVENT_ID,
					VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID);

			assertEquals(ELECTION_EVENT_ID, verificationCardSecretKey.electionEventId());
		}

		@Test
		@DisplayName("null input throws NullPointerException")
		void loadNullInput() {
			assertThrows(NullPointerException.class,
					() -> verificationCardSecretKeyService.load(null, VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID));
			assertThrows(NullPointerException.class,
					() -> verificationCardSecretKeyService.load(ELECTION_EVENT_ID, null, VERIFICATION_CARD_ID));
			assertThrows(NullPointerException.class,
					() -> verificationCardSecretKeyService.load(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, null));
		}

		@Test
		@DisplayName("invalid input throws FailedValidationException")
		void loadInvalidInput() {
			assertThrows(FailedValidationException.class,
					() -> verificationCardSecretKeyService.load(INVALID_ID, VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID));
			assertThrows(FailedValidationException.class,
					() -> verificationCardSecretKeyService.load(ELECTION_EVENT_ID, INVALID_ID, VERIFICATION_CARD_ID));
			assertThrows(FailedValidationException.class,
					() -> verificationCardSecretKeyService.load(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, INVALID_ID));
		}

		@Test
		@DisplayName("existing election event and verification card set but with missing payload throws IllegalStateException")
		void loadMissingPayload() {
			final IllegalStateException exception = assertThrows(IllegalStateException.class,
					() -> verificationCardSecretKeyService.load(MISSING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID));

			final String errorMessage = String.format(
					"Requested verification card secret key is not present. [electionEventId: %s, verificationCardSetId: %s, verificationCardId: %s]",
					MISSING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID);
			assertEquals(errorMessage, Throwables.getRootCause(exception).getMessage());
		}

	}

}
