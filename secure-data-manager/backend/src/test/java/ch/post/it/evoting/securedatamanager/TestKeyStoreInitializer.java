/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;

public class TestKeyStoreInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

	public static final Path KEYSTORE_DIRECTORY_PATH = Path.of("target", "direct-trust");
	public static final Path TALLY_DIRECTORY = Path.of("tally");
	public static final Path CONFIG_DIRECTORY = Path.of("config");
	public static final Path KEYSTORE_FILENAME = Path.of("signing_keystore_test.p12");
	public static final Path KEYSTORE_PASSWORD_FILENAME = Path.of("signing_pw_test.txt");

	@Override
	public void initialize(final ConfigurableApplicationContext applicationContext) {
		createSdmKeystore(CONFIG_DIRECTORY, applicationContext, Alias.SDM_CONFIG);
		createSdmKeystore(TALLY_DIRECTORY, applicationContext, Alias.SDM_TALLY);
	}

	private void createSdmKeystore(Path directoryName, ConfigurableApplicationContext applicationContext, Alias alias) {

		final Path keystoreFilenamePath = KEYSTORE_DIRECTORY_PATH.resolve(directoryName).resolve(KEYSTORE_FILENAME);
		final Path keystorePasswordFilenamePath = KEYSTORE_DIRECTORY_PATH.resolve(directoryName).resolve(KEYSTORE_PASSWORD_FILENAME);

		try {
			if (Files.notExists(keystoreFilenamePath) || Files.notExists(keystorePasswordFilenamePath)) {
				FileUtils.deleteDirectory(KEYSTORE_DIRECTORY_PATH.resolve(directoryName).toFile());
			}

			Files.createDirectories(KEYSTORE_DIRECTORY_PATH.resolve(directoryName));

			final String keystoreLocation = KEYSTORE_DIRECTORY_PATH.resolve(directoryName).resolve(KEYSTORE_FILENAME).toString();
			final String keystorePasswordLocation = KEYSTORE_DIRECTORY_PATH.resolve(directoryName).resolve(KEYSTORE_PASSWORD_FILENAME).toString();

			KeystoreFilesCreator.create(keystoreLocation, keystorePasswordLocation, alias.get());

			final Map<String, String> properties = new HashMap<>();
			properties.put("direct.trust.keystore.location." + directoryName, keystoreLocation);
			properties.put("direct.trust.keystore.password.location." + directoryName, keystorePasswordLocation);
			TestPropertyValues.of(properties).applyTo(applicationContext);

		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}
}

