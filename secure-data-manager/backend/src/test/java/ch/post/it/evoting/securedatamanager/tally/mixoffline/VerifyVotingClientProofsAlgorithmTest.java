/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.mixoffline;

import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS;
import static ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement.PrimeGqElementFactory;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.GqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.PlaintextEqualityProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;
import ch.post.it.evoting.domain.ContextIds;
import ch.post.it.evoting.domain.EncryptedVerifiableVote;
import ch.post.it.evoting.securedatamanager.commons.Constants;

/**
 * Tests of VerifyVotingClientProofsAlgorithm.
 */
class VerifyVotingClientProofsAlgorithmTest {

	private static final SecureRandom SRAND = new SecureRandom();
	private static final int PSI = SRAND.nextInt(2, MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS + 1);
	private static final int PHI = PSI;
	private static final int DELTA_HAT = 1;
	private static final int DELTA = 2;
	private static final int N_C = 2;

	private final Random rand = RandomFactory.createRandom();
	private final GqGroup gqGroup = GroupTestData.getGroupP59();
	private final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
	private final GqGroupGenerator gqGroupGenerator = new GqGroupGenerator(gqGroup);
	private final ZqGroupGenerator zqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(gqGroup));
	private final ZeroKnowledgeProof mockZeroKnowledgeProof = mock(ZeroKnowledgeProof.class);
	private final int desiredNumberOfPrimes = 3;
	private final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions = PrimeGqElementFactory.getSmallPrimeGroupMembers(gqGroup,
			desiredNumberOfPrimes);
	private final List<String> actualVotingOptions = List.of(
			"57a30570-1722-3a7e-a8f9-7dd643d7f33",
			"b9c57a40-a555-35e1-972c-a1a6b7e03381",
			"1-3");

	private VerifyVotingClientProofsContext verifyVotingClientProofsContext;
	private VerifyVotingClientProofsInput verifyVotingClientProofsInput;
	private VerifyVotingClientProofsAlgorithm verifyVotingClientProofsAlgorithm;

	@BeforeEach
	void setup() {
		final List<PrimesMappingTableEntry> primesMappingTableEntries = IntStream.range(0, desiredNumberOfPrimes)
				.mapToObj(i -> new PrimesMappingTableEntry(actualVotingOptions.get(i), encodedVotingOptions.get(i)))
				.toList();

		final ContextIds contextIds1 = new ContextIds(getValidUUID(), getValidUUID(), getValidUUID());
		final ContextIds contextIds2 = new ContextIds(contextIds1.electionEventId(), getValidUUID(), getValidUUID());
		verifyVotingClientProofsContext = new VerifyVotingClientProofsContext.Builder()
				.setEncryptionGroup(gqGroup)
				.setElectionEventId(contextIds1.electionEventId())
				.setNumberOfSelectableVotingOptions(PSI)
				.setNumberOfAllowedWriteInsPlusOne(DELTA_HAT)
				.setPrimesMappingTable(PrimesMappingTable.from(primesMappingTableEntries))
				.build();
		final ExponentiationProof exponentiationProof = new ExponentiationProof(zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementMember());
		final PlaintextEqualityProof plaintextEqualityProof = new PlaintextEqualityProof(zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementVector(2));

		final List<EncryptedVerifiableVote> encryptedVerifiableVotes = List.of(
				new EncryptedVerifiableVote(contextIds1, elGamalGenerator.genRandomCiphertext(DELTA_HAT), elGamalGenerator.genRandomCiphertext(1),
						elGamalGenerator.genRandomCiphertext(PSI), exponentiationProof, plaintextEqualityProof),
				new EncryptedVerifiableVote(contextIds2, elGamalGenerator.genRandomCiphertext(DELTA_HAT), elGamalGenerator.genRandomCiphertext(1),
						elGamalGenerator.genRandomCiphertext(PSI), exponentiationProof, plaintextEqualityProof));
		final ElGamalMultiRecipientPublicKey electionPublicKey = new ElGamalMultiRecipientPublicKey(
				gqGroupGenerator.genRandomGqElementVector(DELTA));
		final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey = new ElGamalMultiRecipientPublicKey(
				gqGroupGenerator.genRandomGqElementVector(PHI));
		final Map<String, ElGamalMultiRecipientPublicKey> verificationCardPublicKeys = new HashMap<>();
		verificationCardPublicKeys.put(contextIds1.verificationCardId(),
				new ElGamalMultiRecipientPublicKey(gqGroupGenerator.genRandomGqElementVector(N_C)));
		verificationCardPublicKeys.put(contextIds2.verificationCardId(),
				new ElGamalMultiRecipientPublicKey(gqGroupGenerator.genRandomGqElementVector(N_C)));

		verifyVotingClientProofsInput = new VerifyVotingClientProofsInput.Builder()
				.setEncryptedVerifiableVotes(encryptedVerifiableVotes)
				.setVerificationCardPublicKeys(verificationCardPublicKeys)
				.setElectionPublicKey(electionPublicKey)
				.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
				.build();

		verifyVotingClientProofsAlgorithm = new VerifyVotingClientProofsAlgorithm(mockZeroKnowledgeProof);
	}

	@Test
	void nullArguments() {
		final VerifyVotingClientProofsContext context = null;
		final VerifyVotingClientProofsInput input = null;

		assertThrows(NullPointerException.class, () -> verifyVotingClientProofsAlgorithm.verifyVotingClientProofs(context, input));
	}

	@Test
	void successfulProofsReturnsTrue() {
		when(mockZeroKnowledgeProof.verifyExponentiation(any(), any(), any(), any())).thenReturn(true);
		when(mockZeroKnowledgeProof.verifyPlaintextEquality(any(), any(), any(), any(), any(), any())).thenReturn(true);
		assertTrue(verifyVotingClientProofsAlgorithm.verifyVotingClientProofs(verifyVotingClientProofsContext, verifyVotingClientProofsInput));
	}

	@Test
	void oneWrongProofReturnsFalse() {
		when(mockZeroKnowledgeProof.verifyExponentiation(any(), any(), any(), any())).thenReturn(false);
		when(mockZeroKnowledgeProof.verifyPlaintextEquality(any(), any(), any(), any(), any(), any())).thenReturn(true);
		assertFalse(verifyVotingClientProofsAlgorithm.verifyVotingClientProofs(verifyVotingClientProofsContext, verifyVotingClientProofsInput));

		when(mockZeroKnowledgeProof.verifyExponentiation(any(), any(), any(), any())).thenReturn(true);
		when(mockZeroKnowledgeProof.verifyPlaintextEquality(any(), any(), any(), any(), any(), any())).thenReturn(false);
		assertFalse(verifyVotingClientProofsAlgorithm.verifyVotingClientProofs(verifyVotingClientProofsContext, verifyVotingClientProofsInput));
	}

	@Test
	void wrongProofsReturnsFalse() {
		when(mockZeroKnowledgeProof.verifyExponentiation(any(), any(), any(), any())).thenReturn(false);
		when(mockZeroKnowledgeProof.verifyPlaintextEquality(any(), any(), any(), any(), any(), any())).thenReturn(false);
		assertFalse(verifyVotingClientProofsAlgorithm.verifyVotingClientProofs(verifyVotingClientProofsContext, verifyVotingClientProofsInput));
	}

	private String getValidUUID() {
		return rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase();
	}
}
