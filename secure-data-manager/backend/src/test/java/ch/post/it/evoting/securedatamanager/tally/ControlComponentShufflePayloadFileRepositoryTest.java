/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.nio.file.Files;
import java.nio.file.Path;
import java.security.SecureRandom;
import java.util.Random;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mockito;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.generators.ControlComponentShufflePayloadGenerator;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.domain.SecurityLevelTestingOnly;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@SecurityLevelTestingOnly
@DisplayName("Use ControlComponentShufflePayloadFileRepository to ")
class ControlComponentShufflePayloadFileRepositoryTest {

	private static final String ballotId = "f0642fdfe4864f4985ac07c057da54b7";
	private static final int nodeId = 1;
	private static final Random random = new SecureRandom();

	private static ControlComponentShufflePayload expectedControlComponentShufflePayload;
	private static ControlComponentShufflePayloadFileRepository controlComponentShufflePayloadFileRepository;
	private static PathResolver payloadResolver;

	@TempDir
	Path tempDir;

	@BeforeAll
	static void setUpAll() {
		final int numVotes = random.nextInt(10) + 2; // Should be at least 2
		final int voteSize = random.nextInt(10) + 1;
		final GqGroup group = GroupTestData.getGqGroup();
		expectedControlComponentShufflePayload = new ControlComponentShufflePayloadGenerator(group).genPayload(numVotes, voteSize, nodeId);
		payloadResolver = Mockito.mock(PathResolver.class);
		controlComponentShufflePayloadFileRepository = new ControlComponentShufflePayloadFileRepository(DomainObjectMapper.getNewInstance(),
				payloadResolver);
	}

	@Test
	@DisplayName("read ControlComponentShufflePayload file")
	void readControlComponentShufflePayload() {
		// Mock payloadResolver path and write payload
		final String electionEventId = expectedControlComponentShufflePayload.getElectionEventId();
		final String ballotBoxId = expectedControlComponentShufflePayload.getBallotBoxId();
		when(payloadResolver.resolveBallotBoxPath(electionEventId, ballotId, ballotBoxId)).thenReturn(tempDir);
		controlComponentShufflePayloadFileRepository.savePayload(ballotId, expectedControlComponentShufflePayload);

		// Read payload and check
		final ControlComponentShufflePayload actualMixnetPayload = controlComponentShufflePayloadFileRepository.getPayload(electionEventId,
				ballotId, ballotBoxId,
				nodeId);

		assertEquals(expectedControlComponentShufflePayload, actualMixnetPayload);
	}

	@Test
	@DisplayName("save ControlComponentShufflePayload file")
	void saveControlComponentShufflePayload() {
		// Mock payloadResolver path
		final String electionEventId = expectedControlComponentShufflePayload.getElectionEventId();
		final String ballotBoxId = expectedControlComponentShufflePayload.getBallotBoxId();
		when(payloadResolver.resolveBallotBoxPath(electionEventId, ballotId, ballotBoxId)).thenReturn(tempDir);
		final Path expectedPayloadPath = tempDir.resolve("controlComponentShufflePayload_" + nodeId + Constants.JSON);

		assertFalse(Files.exists(expectedPayloadPath), "The mixnet payload file should not exist at this point");

		// Write payload
		final Path actualPayloadPath = controlComponentShufflePayloadFileRepository.savePayload(ballotId, expectedControlComponentShufflePayload);

		assertTrue(Files.exists(actualPayloadPath), "The mixnet payload file should exist at this point");
		assertEquals(expectedPayloadPath, actualPayloadPath, "Both payload paths should resolve to the same file");
	}

}
