/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.mixoffline;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.security.SecureRandom;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;

@DisplayName("Constructing a VerifyMixDecOfflineContext object with")
class VerifyMixDecOfflineContextTest {

	private static final SecureRandom RANDOM = new SecureRandom();
	private static final Random RANDOM_SERVICE = RandomFactory.createRandom();
	private static final int ID_LENGTH = 32;

	private GqGroup encryptionGroup;
	private String electionEventId;
	private String ballotBoxId;
	private int numberOfAllowedWriteInsPlusOne;

	@BeforeEach
	void setup() {
		encryptionGroup = GroupTestData.getGqGroup();
		electionEventId = RANDOM_SERVICE.genRandomBase16String(ID_LENGTH).toLowerCase();
		ballotBoxId = RANDOM_SERVICE.genRandomBase16String(ID_LENGTH).toLowerCase();
		numberOfAllowedWriteInsPlusOne = RANDOM.nextInt(5) + 1;
	}

	@Test
	@DisplayName("null arguments throws a NullPointerException")
	void constructWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class,
				() -> new VerifyMixDecOfflineContext(null, electionEventId, ballotBoxId, numberOfAllowedWriteInsPlusOne));
		assertThrows(NullPointerException.class,
				() -> new VerifyMixDecOfflineContext(encryptionGroup, null, ballotBoxId, numberOfAllowedWriteInsPlusOne));
		assertThrows(NullPointerException.class,
				() -> new VerifyMixDecOfflineContext(encryptionGroup, electionEventId, null, numberOfAllowedWriteInsPlusOne));
	}

	@Test
	@DisplayName("too small number of allowed write-ins + 1 throws an IllegalArgumentException")
	void constructWithTooSmallNumberOfWriteInsPlusOneThrows() {
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VerifyMixDecOfflineContext(encryptionGroup, electionEventId, ballotBoxId, 0));
		assertEquals("The number of allowed write-ins + 1 must be strictly positive.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("invalid UUIDs throws a FailedValidationExeption")
	void constructWithInvalidUUIDThrows() {
		final String badId = "bad ID";
		assertThrows(FailedValidationException.class,
				() -> new VerifyMixDecOfflineContext(encryptionGroup, badId, ballotBoxId, numberOfAllowedWriteInsPlusOne));
		assertThrows(FailedValidationException.class,
				() -> new VerifyMixDecOfflineContext(encryptionGroup, electionEventId, badId, numberOfAllowedWriteInsPlusOne));
	}

	@Test
	@DisplayName("valid arguments does not throw")
	void constructWithValidArgumentsDoesNotThrow() {
		assertDoesNotThrow(() -> new VerifyMixDecOfflineContext(encryptionGroup, electionEventId, ballotBoxId, numberOfAllowedWriteInsPlusOne));
	}
}