/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.io.TempDir;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.SchnorrProof;
import ch.post.it.evoting.domain.SecurityLevelTestingOnly;
import ch.post.it.evoting.domain.SerializationUtils;
import ch.post.it.evoting.domain.configuration.ControlComponentPublicKeysPayload;
import ch.post.it.evoting.securedatamanager.configuration.ControlComponentPublicKeysPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@SecurityLevelTestingOnly
@DisplayName("A ControlComponentPublicKeysService")
class ControlComponentPublicKeysServiceTest {

	private static final String ELECTION_EVENT_ID = "314bd34dcf6e4de4b771a92fa3849d3d";
	private static final String WRONG_ELECTION_EVENT_ID = "414bd34dcf6e4de4b771a92fa3849d3d";
	private static final String NOT_ENOUGH_ELECTION_EVENT_ID = "614bd34dcf6e4de4b771a92fa3849d3d";
	private static final String INVALID_ID = "invalidId";
	private static final int NODE_ID = 1;
	private static final int KEY_SIZE = 3;

	private static GqGroup gqGroup;
	private static ObjectMapper objectMapper;
	private static ElGamalGenerator elGamalGenerator;
	private static ControlComponentPublicKeysService controlComponentPublicKeysService;

	@BeforeAll
	static void setUpAll() throws URISyntaxException {
		gqGroup = SerializationUtils.getGqGroup();
		elGamalGenerator = new ElGamalGenerator(gqGroup);
		objectMapper = DomainObjectMapper.getNewInstance();
		final Path path = Paths.get(
				Objects.requireNonNull(ControlComponentPublicKeysServiceTest.class.getResource("/controlComponentPublicKeysTest/")).toURI());
		final PathResolver pathResolver = new PathResolver(path.toString());

		final ControlComponentPublicKeysPayloadFileRepository controlComponentPublicKeysPayloadFileRepository = new ControlComponentPublicKeysPayloadFileRepository(
				objectMapper, pathResolver);

		controlComponentPublicKeysService = new ControlComponentPublicKeysService(controlComponentPublicKeysPayloadFileRepository);
	}

	@Nested
	@DisplayName("saving")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class SaveTest {

		private ControlComponentPublicKeysService controlComponentPublicKeysServiceTemp;

		private ControlComponentPublicKeysPayload controlComponentPublicKeysPayload;

		@BeforeAll
		void setUpAll(
				@TempDir
				final Path tempDir) throws IOException {

			Files.createDirectories(tempDir.resolve("sdm/config").resolve(ELECTION_EVENT_ID));

			final PathResolver pathResolver = new PathResolver(tempDir.toString());
			final ControlComponentPublicKeysPayloadFileRepository controlComponentPublicKeysPayloadFileRepositoryTemp = new ControlComponentPublicKeysPayloadFileRepository(
					objectMapper, pathResolver);

			controlComponentPublicKeysServiceTemp = new ControlComponentPublicKeysService(controlComponentPublicKeysPayloadFileRepositoryTemp);
		}

		@BeforeEach
		void setUp() {
			// Create keys.
			final ElGamalMultiRecipientPublicKey ccrChoiceReturnCodesEncryptionPublicKey = elGamalGenerator.genRandomPublicKey(KEY_SIZE);
			final ElGamalMultiRecipientPublicKey ccmElectionPublicKey = elGamalGenerator.genRandomPublicKey(KEY_SIZE);
			final GroupVector<SchnorrProof, ZqGroup> schnorrProofs = SerializationUtils.getSchnorrProofs(KEY_SIZE);
			final ControlComponentPublicKeys controlComponentPublicKeys = new ControlComponentPublicKeys(NODE_ID,
					ccrChoiceReturnCodesEncryptionPublicKey, schnorrProofs, ccmElectionPublicKey, schnorrProofs);

			// Create payload.
			controlComponentPublicKeysPayload = new ControlComponentPublicKeysPayload(gqGroup, ELECTION_EVENT_ID, controlComponentPublicKeys);
		}

		@Test
		@DisplayName("a valid payload does not throw")
		void saveValidPayload() {
			assertDoesNotThrow(() -> controlComponentPublicKeysServiceTemp.save(controlComponentPublicKeysPayload));
		}

		@Test
		@DisplayName("a null payload throws NullPointerException")
		void saveNullPayload() {
			assertThrows(NullPointerException.class, () -> controlComponentPublicKeysServiceTemp.save(null));
		}

	}

	@Nested
	@DisplayName("calling exist")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class ExistTest {

		@Test
		@DisplayName("for valid election event returns true")
		void existValidElectionEvent() {
			assertTrue(controlComponentPublicKeysService.exist(ELECTION_EVENT_ID));
		}

		@Test
		@DisplayName("for invalid election event id throws FailedValidationException")
		void existInvalidElectionEvent() {
			assertThrows(FailedValidationException.class, () -> controlComponentPublicKeysService.exist(INVALID_ID));
		}

		@Test
		@DisplayName("for non existing election event returns false")
		void existNonExistingElectionEvent() {
			assertFalse(controlComponentPublicKeysService.exist(WRONG_ELECTION_EVENT_ID));
		}

		@Test
		@DisplayName("for missing payloads return false")
		void existMissingPayloads() {
			assertFalse(controlComponentPublicKeysService.exist(NOT_ENOUGH_ELECTION_EVENT_ID));
		}

	}
}
