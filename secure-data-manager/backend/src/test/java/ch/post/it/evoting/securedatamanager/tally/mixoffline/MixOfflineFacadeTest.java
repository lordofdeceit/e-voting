/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.mixoffline;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatCode;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.assertj.core.api.ThrowableAssert.ThrowingCallable;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URL;
import java.security.SignatureException;
import java.time.LocalDateTime;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.cryptoprimitives.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.securedatamanager.SetupComponentTallyDataPayloadService;
import ch.post.it.evoting.securedatamanager.configuration.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.configuration.SetupComponentPublicKeysPayloadService;
import ch.post.it.evoting.securedatamanager.services.application.exception.CheckedIllegalStateException;
import ch.post.it.evoting.securedatamanager.services.application.service.BallotBoxService;
import ch.post.it.evoting.securedatamanager.services.application.service.IdentifierValidationService;
import ch.post.it.evoting.securedatamanager.tally.ControlComponentBallotBoxPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.tally.ControlComponentShufflePayloadFileRepository;
import ch.post.it.evoting.securedatamanager.tally.EncryptionParametersTallyService;

@ExtendWith(MockitoExtension.class)
class MixOfflineFacadeTest {

	private static ElectionEventContextPayload electionEventContextPayload;

	@Mock
	private IdentifierValidationService identifierValidationService;

	@Mock
	private BallotBoxService ballotBoxService;

	@Mock
	private BallotTallyService ballotTallyService;

	@Mock
	private ControlComponentBallotBoxPayloadFileRepository controlComponentBallotBoxPayloadFileRepository;

	@Mock
	private ControlComponentShufflePayloadFileRepository controlComponentShufflePayloadFileRepository;

	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private ElectionEventContextPayloadService electionEventContextPayloadService;

	@Mock
	private SetupComponentPublicKeysPayloadService setupComponentPublicKeysPayloadService;

	@Mock
	private SetupComponentTallyDataPayloadService setupComponentTallyDataPayloadService;

	@Mock
	private EncryptionParametersTallyService encryptionParametersTallyService;

	@Mock
	private SignatureKeystore<Alias> signatureKeystoreSdmTallyService;

	@Mock
	private VerifyMixDecOfflineAlgorithm verifyMixDecOfflineAlgorithm;

	@Mock
	private VerifyVotingClientProofsAlgorithm verifyVotingClientProofsAlgorithm;

	@InjectMocks
	private MixOfflineFacade mixFacade;

	@BeforeAll
	static void setUpAll() throws IOException {
		final ObjectMapper mapper = DomainObjectMapper.getNewInstance();
		final URL electionContextPayloadUrl = MixOfflineFacadeTest.class.getResource(
				"/" + MixOfflineFacadeTest.class.getSimpleName() + "/electionEventContextPayload.json");
		electionEventContextPayload = mapper.readValue(electionContextPayloadUrl, ElectionEventContextPayload.class);
	}

	@Nested
	class ValidateInputTest {

		private static final List<char[]> PASSWORDS = List.of("Password_EB1".toCharArray(), "Password_EB2".toCharArray());
		private static String electionEventId;
		private static String ballotBoxId;

		@BeforeEach
		void setUp() {
			final GetMixnetInitialCiphertextsAlgorithm getMixnetInitialCiphertextsAlgorithm = new GetMixnetInitialCiphertextsAlgorithm(
					ElGamalFactory.createElGamal());
			final ProcessPlaintextsService processPlaintextsService = mock(ProcessPlaintextsService.class);
			final MixDecOfflineService mixDecOfflineService = mock(MixDecOfflineService.class);
			final VerifyMixOfflineService verifyMixOfflineService = new VerifyMixOfflineService(controlComponentBallotBoxPayloadFileRepository, controlComponentShufflePayloadFileRepository, getMixnetInitialCiphertextsAlgorithm,
					signatureKeystoreSdmTallyService, verifyMixDecOfflineAlgorithm, verifyVotingClientProofsAlgorithm, identifierValidationService, encryptionParametersTallyService);

			mixFacade = new MixOfflineFacade(ballotBoxService, ballotTallyService, electionEventContextPayloadService,
					setupComponentPublicKeysPayloadService, mixDecOfflineService, processPlaintextsService, setupComponentTallyDataPayloadService,
					signatureKeystoreSdmTallyService, verifyMixOfflineService,
					encryptionParametersTallyService);

			electionEventId = electionEventContextPayload.getElectionEventContext().electionEventId();
			ballotBoxId = electionEventContextPayload.getElectionEventContext().verificationCardSetContexts().get(0).ballotBoxId();
		}

		@Test
		void mixFacadeThrowsForInvalidUUID() {
			assertThrows(FailedValidationException.class, () -> mixFacade.mixOffline("", ballotBoxId, PASSWORDS));
			assertThrows(FailedValidationException.class, () -> mixFacade.mixOffline(electionEventId, "", PASSWORDS));
		}

		@Test
		void mixFacadeThrowsWhenBallotBoxIsNotDownloaded() throws SignatureException {
			when(ballotBoxService.isDownloaded(ballotBoxId)).thenReturn(false);
			when(signatureKeystoreSdmTallyService.verifySignature(any(), any(), any(), any())).thenReturn(true);
			assertThrows(CheckedIllegalStateException.class, () -> mixFacade.mixOffline(electionEventId, ballotBoxId, PASSWORDS));
		}
	}

	@Nested
	class ValidateMixIsAllowedTest {
		private static final String ANY_ID = "53";
		private static final int GRACE_PERIOD = 3600;

		private LocalDateTime electionEndTime;
		private LocalDateTime currentTime;

		@BeforeEach
		void setUp() {
			currentTime = LocalDateTime.now();
			electionEndTime = currentTime.plusSeconds(3600);

			when(ballotBoxService.getGracePeriod(anyString())).thenReturn(GRACE_PERIOD);
			when(electionEventContextPayloadService.load(anyString()).getElectionEventContext().finishTime()).thenReturn(electionEndTime);
		}

		@Test
		@DisplayName("Test ballot box can be mixed at any time.")
		void testBallotBox() {
			// given
			when(ballotBoxService.isTestBallotBox(anyString())).thenReturn(true);

			// when
			final ThrowingCallable validation = () -> mixFacade.validateMixIsAllowed(ANY_ID, ANY_ID, () -> currentTime);

			// then
			assertThatCode(validation).doesNotThrowAnyException();
		}

		@Test
		@DisplayName("Possible to mix after the grace period has expired.")
		void prodBallotBoxWithElectionFinished() {
			// given
			when(ballotBoxService.isTestBallotBox(anyString())).thenReturn(false);

			// when
			final ThrowingCallable validation = () -> mixFacade.validateMixIsAllowed(ANY_ID, ANY_ID,
					() -> electionEndTime.plusSeconds(GRACE_PERIOD).plusSeconds(1));

			// then
			assertThatCode(validation).doesNotThrowAnyException();
		}

		@Test
		@DisplayName("Not possible to mix before the election finish, including grace period has expired")
		void prodBallotBoxWithElectionNotFinished() {
			// given
			when(ballotBoxService.isTestBallotBox(anyString())).thenReturn(false);

			// when
			final ThrowingCallable validation = () -> mixFacade.validateMixIsAllowed(ANY_ID, ANY_ID,
					() -> electionEndTime.plusSeconds(GRACE_PERIOD));

			// then
			assertThatThrownBy(validation)
					.isInstanceOf(IllegalStateException.class)
					.hasMessage("The ballot box can not be mixed. [isTestBallotBox: %s, finishTime: %s, electionEventId: %s, ballotBoxId: %s]",
							ballotBoxService.isTestBallotBox(anyString()), electionEndTime, ANY_ID, ANY_ID);
		}
	}

	@Nested
	class ValidateBallotBoxConsistencyTest {

		private static final String ANY_ID = "12e590cc85ad49af96b15ca761dfe49d";
		private static final String ID_0 = "00000000000000000000000000000000";
		private static final String ID_1 = "11111111111111111111111111111111";
		private static final String ID_2 = "22222222222222222222222222222222";
		private static final String ID_3 = "33333333333333333333333333333333";

		@Test
		@DisplayName("Same unordered content in DB and in context pass validation.")
		void eventAndDbContainSameIds() {
			// given
			when(ballotBoxService.getBallotBoxesId(any())).thenReturn(List.of(ID_1, ID_2, ID_3));
			when(electionEventContextPayloadService.load(anyString()).getElectionEventContext().verificationCardSetContexts()).thenReturn(List.of(
					createVerificationCardSet(ID_3),
					createVerificationCardSet(ID_1),
					createVerificationCardSet(ID_2)
			));

			// when
			final ThrowingCallable validation = () -> mixFacade.validateBallotBoxConsistency(ANY_ID);

			// then
			assertThatCode(validation).doesNotThrowAnyException();
		}

		@Test
		@DisplayName("Break if there is a count mismatch between the DB and context.")
		void eventAndDbContainDifferentIdsCount() {
			// given
			when(ballotBoxService.getBallotBoxesId(any())).thenReturn(List.of(ID_1, ID_2, ID_3, ID_3));
			when(electionEventContextPayloadService.load(anyString()).getElectionEventContext().verificationCardSetContexts()).thenReturn(List.of(
					createVerificationCardSet(ID_3),
					createVerificationCardSet(ID_1),
					createVerificationCardSet(ID_2)
			));

			// when
			final ThrowingCallable validation = () -> mixFacade.validateBallotBoxConsistency(ANY_ID);

			// then
			assertThatThrownBy(validation)
					.isInstanceOf(IllegalStateException.class)
					.hasMessage(
							"The number of ballot boxes in the DB and in the context mismatch. [electionEventId: %s, dbCount: %s, contextCount: %s]",
							ANY_ID, 4, 3);
		}

		@Test
		@DisplayName("Break if there is a different ID between the DB and context.")
		void eventAndDbContainDifferentIds() {
			// given
			when(ballotBoxService.getBallotBoxesId(any())).thenReturn(List.of(ID_1, ID_0, ID_3));
			when(electionEventContextPayloadService.load(anyString()).getElectionEventContext().verificationCardSetContexts()).thenReturn(List.of(
					createVerificationCardSet(ID_3),
					createVerificationCardSet(ID_1),
					createVerificationCardSet(ID_2)
			));

			// when
			final ThrowingCallable validation = () -> mixFacade.validateBallotBoxConsistency(ANY_ID);

			// then
			assertThatThrownBy(validation)
					.isInstanceOf(IllegalStateException.class)
					.hasMessage(
							"The ballot boxes are not the same in the DB and in the context. [electionEventId: %s, dbContent: %s, contextContent: %s]",
							ANY_ID, List.of(ID_1, ID_3, ID_0), List.of(ID_1, ID_3, ID_2));
		}

		@Test
		@DisplayName("Break if there is a duplicate value in the DB.")
		void dDbContainDuplicate() {
			// given
			when(ballotBoxService.getBallotBoxesId(any())).thenReturn(List.of(ID_1, ID_3, ID_3, ID_2));
			when(electionEventContextPayloadService.load(anyString()).getElectionEventContext().verificationCardSetContexts()).thenReturn(List.of(
					createVerificationCardSet(ID_3),
					createVerificationCardSet(ID_1),
					createVerificationCardSet(ID_0),
					createVerificationCardSet(ID_2)
			));

			// when
			final ThrowingCallable validation = () -> mixFacade.validateBallotBoxConsistency(ANY_ID);

			// then
			assertThatThrownBy(validation)
					.isInstanceOf(IllegalStateException.class)
					.hasMessage(
							"There are duplicate values. [electionEventId: %s, dbContent: %s, contextContent: %s]",
							ANY_ID, List.of(ID_1, ID_3, ID_3, ID_2), List.of(ID_3, ID_1, ID_0, ID_2));
		}

		@Test
		@DisplayName("Break if there is a duplicate value in the context.")
		void eventContainDuplicate() {
			// given
			when(ballotBoxService.getBallotBoxesId(any())).thenReturn(List.of(ID_1, ID_3, ID_0, ID_2));
			when(electionEventContextPayloadService.load(anyString()).getElectionEventContext().verificationCardSetContexts()).thenReturn(List.of(
					createVerificationCardSet(ID_3),
					createVerificationCardSet(ID_1),
					createVerificationCardSet(ID_3),
					createVerificationCardSet(ID_2)
			));

			// when
			final ThrowingCallable validation = () -> mixFacade.validateBallotBoxConsistency(ANY_ID);

			// then
			assertThatThrownBy(validation)
					.isInstanceOf(IllegalStateException.class)
					.hasMessage(
							"There are duplicate values. [electionEventId: %s, dbContent: %s, contextContent: %s]",
							ANY_ID, List.of(ID_1, ID_3, ID_0, ID_2), List.of(ID_3, ID_1, ID_3, ID_2));
		}

		private VerificationCardSetContext createVerificationCardSet(final String uuid) {
			final GqGroup encryptionGroup = electionEventContextPayload.getEncryptionGroup();
			final GroupVector<PrimeGqElement, GqGroup> smallPrimeGroupMembers = PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers(
					encryptionGroup, 1);
			final PrimesMappingTable primesMappingTable = PrimesMappingTable.from(
					List.of(new PrimesMappingTableEntry("actualVotingOption", smallPrimeGroupMembers.get(0))));
			return new VerificationCardSetContext(ANY_ID, uuid, false, 0, 0, 0, primesMappingTable);
		}
	}

}
