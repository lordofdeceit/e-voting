/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.mixoffline;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URL;
import java.security.SignatureException;
import java.util.List;

import org.assertj.core.api.ThrowableAssert.ThrowingCallable;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.MoreCollectors;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.EncryptionParametersPayload;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamal;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.mixnet.Mixnet;
import ch.post.it.evoting.cryptoprimitives.mixnet.MixnetFactory;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProofFactory;
import ch.post.it.evoting.domain.configuration.SetupComponentTallyDataPayload;
import ch.post.it.evoting.domain.election.BallotBox;
import ch.post.it.evoting.domain.tally.ControlComponentBallotBoxPayload;
import ch.post.it.evoting.securedatamanager.SetupComponentTallyDataPayloadService;
import ch.post.it.evoting.securedatamanager.configuration.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.configuration.SetupComponentPublicKeysPayloadService;
import ch.post.it.evoting.securedatamanager.services.application.exception.CheckedIllegalStateException;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.application.service.BallotBoxService;
import ch.post.it.evoting.securedatamanager.services.application.service.ElectionEventService;
import ch.post.it.evoting.securedatamanager.services.application.service.IdentifierValidationService;
import ch.post.it.evoting.securedatamanager.tally.ControlComponentBallotBoxPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.tally.ControlComponentShufflePayloadFileRepository;
import ch.post.it.evoting.securedatamanager.tally.DecodeVotingOptionsAlgorithm;
import ch.post.it.evoting.securedatamanager.tally.EncryptionParametersTallyService;
import ch.post.it.evoting.securedatamanager.tally.IntegerToWriteInAlgorithm;
import ch.post.it.evoting.securedatamanager.tally.QuadraticResidueToWriteInAlgorithm;

@DisplayName("MixOffline with")
class MixOfflineTest {

	private static String electionEventId;
	private static String ballotBoxId;
	private static String ballotId;
	private static BallotBox ballotBox;
	private static ElectionEventContextPayload electionEventContextPayload;
	private static EncryptionParametersPayload encryptionParametersPayload;
	private static SetupComponentPublicKeysPayload setupComponentPublicKeysPayload;
	private static SetupComponentTallyDataPayload setupComponentTallyDataPayload;
	private static Ballot ballot;
	private static List<ControlComponentBallotBoxPayload> controlComponentBallotBoxPayloads;
	private static List<ControlComponentBallotBoxPayload> invalidVotingClientProofsPayloads;
	private static List<ControlComponentBallotBoxPayload> invalidNodeIdsPayloads;
	private static List<ControlComponentShufflePayload> controlComponentShufflePayloads;
	private final ElectionEventService electionEventService = mock(ElectionEventService.class);
	private final BallotBoxService ballotBoxService = mock(BallotBoxService.class);
	private final IdentifierValidationService identifierValidationService = new IdentifierValidationService(electionEventService, ballotBoxService);
	private final ControlComponentBallotBoxPayloadFileRepository controlComponentBallotBoxPayloadFileRepository = mock(
			ControlComponentBallotBoxPayloadFileRepository.class);
	private final TallyComponentShufflePayloadFileRepository tallyComponentShufflePayloadFileRepository = mock(
			TallyComponentShufflePayloadFileRepository.class);
	private final EncryptionParametersTallyService encryptionParametersTallyService = mock(EncryptionParametersTallyService.class);
	private final TallyComponentVotesService tallyComponentVotesService = mock(TallyComponentVotesService.class);
	private final SignatureKeystore<Alias> signatureKeystore = mock(SignatureKeystore.class);
	private VerifyMixDecOfflineAlgorithm verifyMixDecOfflineAlgorithm;
	private VerifyMixOfflineService verifyMixOfflineService;
	private MixDecOfflineService mixDecOfflineService;
	private ProcessPlaintextsService processPlaintextsService;
	private MixOfflineFacade mixOfflineFacade;

	@BeforeAll
	static void load() throws IOException {
		loadTestData();
	}

	@BeforeEach
	void setUp() throws SignatureException {
		setUpVerifyMixOffline();
		setUpMixDecOffline();
		setUpProcessPlaintexts();
		setUpMixOfflineFacade();
	}

	@Test
	@DisplayName("invalid control component ballot box payload throws IllegalStateException.")
	void failedConsistencyOfPayloadsThrows() {
		NODE_IDS.forEach(nodeId -> when(
				controlComponentBallotBoxPayloadFileRepository.getPayload(electionEventId, ballotId, ballotBoxId, nodeId)).thenReturn(
				invalidNodeIdsPayloads.get(nodeId - 1)));

		final ThrowingCallable mixOffline = () -> mixOfflineFacade.mixOffline(electionEventId, ballotBoxId,
				List.of("Password_EB1".toCharArray(), "Password_EB2".toCharArray()));

		assertThatThrownBy(mixOffline)
				.isInstanceOf(IllegalStateException.class)
				.hasMessage("Wrong number of control component ballot box payloads.");
	}

	@Test
	@DisplayName("invalid voting client proofs throws IllegalStateException.")
	void failedVerifyVotingClientProofsThrows() {
		NODE_IDS.forEach(nodeId -> when(
				controlComponentBallotBoxPayloadFileRepository.getPayload(electionEventId, ballotId, ballotBoxId, nodeId)).thenReturn(
				invalidVotingClientProofsPayloads.get(nodeId - 1)));

		final ThrowingCallable failedVerifyVotingClientProofs = () -> mixOfflineFacade.mixOffline(electionEventId, ballotBoxId,
				List.of("Password_EB1".toCharArray(), "Password_EB2".toCharArray()));

		assertThatThrownBy(failedVerifyVotingClientProofs)
				.isInstanceOf(IllegalStateException.class)
				.hasMessage(
						String.format("The voting client's zero-knowledge proofs are invalid. [electionEventId: %s, ballotId: %s, ballotBoxId: %s]",
								electionEventId, ballotId, ballotBoxId));
	}

	@Test
	@DisplayName("invalid mixing and decryption proofs throws IllegalStateException.")
	void failedVerifyMixDecOfflineThrows() {
		when(verifyMixDecOfflineAlgorithm.verifyMixDecOffline(any(), any())).thenReturn(false);

		final ThrowingCallable failedVerifyMixDecOffline = () -> mixOfflineFacade.mixOffline(electionEventId, ballotBoxId,
				List.of("Password_EB1".toCharArray(), "Password_EB2".toCharArray()));

		assertThatThrownBy(failedVerifyMixDecOffline)
				.isInstanceOf(IllegalStateException.class)
				.hasMessage(
						String.format(
								"The online control-component's mixing and decryption proofs are invalid. [electionEventId: %s, ballotId: %s, ballotBoxId: %s]",
								electionEventId, ballotId, ballotBoxId));
	}

	@Test
	@DisplayName("ballot box already decrypted before mix decrypt throws IllegalStateException.")
	void failedMixDecOfflineThrows() {
		when(ballotBoxService.isDecrypted(ballotBoxId)).thenReturn(true);

		final ThrowingCallable failedVerifyMixDecOffline = () -> mixOfflineFacade.mixOffline(electionEventId, ballotBoxId,
				List.of("Password_EB1".toCharArray(), "Password_EB2".toCharArray()));

		assertThatThrownBy(failedVerifyMixDecOffline)
				.isInstanceOf(IllegalArgumentException.class)
				.hasMessage(String.format("The ballot box has already been decrypted. [ballotBoxId: %s]", ballotBoxId));

		verify(tallyComponentShufflePayloadFileRepository, times(0)).savePayload(any(), any(), any(), any());
	}

	@Test
	@DisplayName("happy path does not throw.")
	void happyPathDoesNotThrow() throws CheckedIllegalStateException, ResourceNotFoundException {
		final List<char[]> passwords = List.of("Password_EB1".toCharArray(), "Password_EB2".toCharArray());
		mixOfflineFacade.mixOffline(electionEventId, ballotBoxId, passwords);

		passwords.forEach(pw -> assertArrayEquals(new char[] { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0' }, pw));
		verify(tallyComponentShufflePayloadFileRepository, times(1)).savePayload(any(), any(), any(), any());
		verify(tallyComponentVotesService, times(1)).save(any());
	}

	private static void loadTestData() throws IOException {
		final ObjectMapper mapper = DomainObjectMapper.getNewInstance();

		final URL electionContextPayloadUrl = MixOfflineFacadeTest.class.getResource(
				"/" + MixOfflineFacadeTest.class.getSimpleName() + "/electionEventContextPayload.json");
		electionEventContextPayload = mapper.readValue(electionContextPayloadUrl, ElectionEventContextPayload.class);
		final URL encryptionParametersPayloadUrl = MixOfflineFacadeTest.class.getResource(
				"/" + MixOfflineFacadeTest.class.getSimpleName() + "/encryptionParametersPayload.json");
		encryptionParametersPayload = mapper.readValue(encryptionParametersPayloadUrl, EncryptionParametersPayload.class);
		electionEventId = electionEventContextPayload.getElectionEventContext().electionEventId();
		final List<VerificationCardSetContext> verificationCardSetContexts = electionEventContextPayload.getElectionEventContext()
				.verificationCardSetContexts();
		// The test files controlComponentBallotBoxPayloads and controlComponentShufflePayloads corresponds to this id.
		ballotBoxId = verificationCardSetContexts.get(2).ballotBoxId();

		final URL setupComponentPublicKeysPayloadUrl = MixOfflineFacadeTest.class.getResource(
				"/" + MixOfflineFacadeTest.class.getSimpleName() + "/setupComponentPublicKeysPayload.json");
		setupComponentPublicKeysPayload = mapper.readValue(setupComponentPublicKeysPayloadUrl, SetupComponentPublicKeysPayload.class);

		final Resource invalidVotingClientProofsResource = new ClassPathResource(
				"/" + MixOfflineFacadeTest.class.getSimpleName() + "/controlComponentBallotBoxPayloads_invalidVotingClientProofs.json");
		invalidVotingClientProofsPayloads = mapper.readValue(invalidVotingClientProofsResource.getFile(), new TypeReference<>() {
		});

		final Resource invalidNodeIdsResource = new ClassPathResource(
				"/" + MixOfflineFacadeTest.class.getSimpleName() + "/controlComponentBallotBoxPayloads_invalidNodeIds.json");
		invalidNodeIdsPayloads = mapper.readValue(invalidNodeIdsResource.getFile(), new TypeReference<>() {
		});

		final Resource ballotBoxPayloadsResource = new ClassPathResource(
				"/" + MixOfflineFacadeTest.class.getSimpleName() + "/controlComponentBallotBoxPayloads.json");
		controlComponentBallotBoxPayloads = mapper.readValue(ballotBoxPayloadsResource.getFile(), new TypeReference<>() {
		});

		final Resource shufflePayloadsResource = new ClassPathResource(
				"/" + MixOfflineFacadeTest.class.getSimpleName() + "/controlComponentShufflePayloads.json");
		controlComponentShufflePayloads = mapper.readValue(shufflePayloadsResource.getFile(), new TypeReference<>() {
		});

		final Resource ballotResource = new ClassPathResource(
				"/" + MixOfflineFacadeTest.class.getSimpleName() + "/ballot.json");
		ballot = mapper.readValue(ballotResource.getFile(), Ballot.class);
		ballotId = ballot.id();

		final Resource ballotBoxResource = new ClassPathResource(
				"/" + MixOfflineFacadeTest.class.getSimpleName() + "/ballotBox.json");
		ballotBox = mapper.readValue(ballotBoxResource.getFile(), BallotBox.class);

		final Resource setupComponentTallyDataPayloadResource = new ClassPathResource(
				"/" + MixOfflineFacadeTest.class.getSimpleName() + "/setupComponentTallyDataPayload.json");
		setupComponentTallyDataPayload = mapper.readValue(setupComponentTallyDataPayloadResource.getFile(), SetupComponentTallyDataPayload.class);
	}

	void setUpVerifyMixOffline() {
		NODE_IDS.forEach(nodeId -> when(
				controlComponentBallotBoxPayloadFileRepository.getPayload(electionEventId, ballotId, ballotBoxId, nodeId)).thenReturn(
				controlComponentBallotBoxPayloads.get(nodeId - 1)));

		final ControlComponentShufflePayloadFileRepository controlComponentShufflePayloadFileRepository = mock(
				ControlComponentShufflePayloadFileRepository.class);
		NODE_IDS.forEach(
				nodeId -> when(
						controlComponentShufflePayloadFileRepository.getPayload(electionEventId, ballotId, ballotBoxId, nodeId)).thenReturn(
						controlComponentShufflePayloads.get(nodeId - 1)));

		final ZeroKnowledgeProof zeroKnowledgeProof = ZeroKnowledgeProofFactory.createZeroKnowledgeProof();
		final VerifyVotingClientProofsAlgorithm verifyVotingClientProofsAlgorithm = new VerifyVotingClientProofsAlgorithm(zeroKnowledgeProof);

		verifyMixDecOfflineAlgorithm = mock(VerifyMixDecOfflineAlgorithm.class);
		when(verifyMixDecOfflineAlgorithm.verifyMixDecOffline(any(), any())).thenReturn(true);

		final GetMixnetInitialCiphertextsAlgorithm getMixnetInitialCiphertextsAlgorithm = new GetMixnetInitialCiphertextsAlgorithm(
				ElGamalFactory.createElGamal());

		when(electionEventService.exists(electionEventId)).thenReturn(true);
		when(encryptionParametersTallyService.loadEncryptionGroup(electionEventId)).thenReturn(encryptionParametersPayload.getEncryptionGroup());

		verifyMixOfflineService = new VerifyMixOfflineService(controlComponentBallotBoxPayloadFileRepository,
				controlComponentShufflePayloadFileRepository, getMixnetInitialCiphertextsAlgorithm, signatureKeystore,
				verifyMixDecOfflineAlgorithm, verifyVotingClientProofsAlgorithm, identifierValidationService, encryptionParametersTallyService);
	}

	void setUpMixDecOffline() {
		final Hash hash = HashFactory.createHash();
		final Mixnet mixnet = MixnetFactory.createMixnet();
		final ZeroKnowledgeProof zeroKnowledgeProof = ZeroKnowledgeProofFactory.createZeroKnowledgeProof();
		final MixDecOfflineAlgorithm mixDecOfflineAlgorithm = new MixDecOfflineAlgorithm(hash, mixnet, ballotBoxService, zeroKnowledgeProof);

		mixDecOfflineService = new MixDecOfflineService(tallyComponentShufflePayloadFileRepository, encryptionParametersTallyService,
				mixDecOfflineAlgorithm, signatureKeystore, identifierValidationService);
	}

	void setUpProcessPlaintexts() {
		final FactorizeService factorizeService = new FactorizeService();
		final DecodeVotingOptionsAlgorithm decodeVotingOptionsAlgorithm = new DecodeVotingOptionsAlgorithm();
		final IntegerToWriteInAlgorithm integerToWriteInAlgorithm = new IntegerToWriteInAlgorithm();
		final QuadraticResidueToWriteInAlgorithm quadraticResidueToWriteInAlgorithm = new QuadraticResidueToWriteInAlgorithm(
				integerToWriteInAlgorithm);
		final ElGamal elGamal = ElGamalFactory.createElGamal();
		final DecodeWriteInsAlgorithm decodeWriteInsAlgorithm = new DecodeWriteInsAlgorithm(new IsWriteInOptionAlgorithm(),
				quadraticResidueToWriteInAlgorithm);
		final ProcessPlaintextsAlgorithm processPlaintextsAlgorithm = new ProcessPlaintextsAlgorithm(elGamal, factorizeService,
				decodeWriteInsAlgorithm, decodeVotingOptionsAlgorithm
		);

		when(encryptionParametersTallyService.loadEncryptionGroup(electionEventId)).thenReturn(encryptionParametersPayload.getEncryptionGroup());

		processPlaintextsService = new ProcessPlaintextsService(processPlaintextsAlgorithm,
				signatureKeystore, tallyComponentVotesService, encryptionParametersTallyService);
	}

	void setUpMixOfflineFacade() throws SignatureException {
		when(ballotBoxService.getBallotId(ballotBoxId)).thenReturn(ballotId);
		when(ballotBoxService.isDownloaded(ballotBoxId)).thenReturn(true);
		when(ballotBoxService.getGracePeriod(ballotBoxId)).thenReturn(Integer.parseInt(ballotBox.getGracePeriod()));
		when(ballotBoxService.isTestBallotBox(ballotBoxId)).thenReturn(true);

		final List<VerificationCardSetContext> verificationCardSetContexts = electionEventContextPayload.getElectionEventContext()
				.verificationCardSetContexts();
		when(ballotBoxService.getBallotBoxesId(electionEventId)).thenReturn(List.of(
				verificationCardSetContexts.get(0).ballotBoxId(),
				verificationCardSetContexts.get(1).ballotBoxId(),
				verificationCardSetContexts.get(2).ballotBoxId(),
				verificationCardSetContexts.get(3).ballotBoxId()));

		final BallotTallyService ballotTallyService = mock(BallotTallyService.class);
		when(ballotTallyService.getBallot(electionEventId, ballotId)).thenReturn(ballot);

		final ElectionEventContextPayloadService electionEventContextPayloadService = mock(ElectionEventContextPayloadService.class);
		when(electionEventContextPayloadService.load(electionEventId)).thenReturn(electionEventContextPayload);

		final SetupComponentPublicKeysPayloadService setupComponentPublicKeysPayloadService = mock(SetupComponentPublicKeysPayloadService.class);
		when(setupComponentPublicKeysPayloadService.load(electionEventId)).thenReturn(setupComponentPublicKeysPayload);

		final String verificationCardSetId = verificationCardSetContexts.stream()
				.filter(verificationCardSetContext -> verificationCardSetContext.ballotBoxId().equals(ballotBoxId))
				.collect(MoreCollectors.onlyElement())
				.verificationCardSetId();
		final SetupComponentTallyDataPayloadService setupComponentTallyDataPayloadService = mock(SetupComponentTallyDataPayloadService.class);
		when(setupComponentTallyDataPayloadService.load(electionEventId, verificationCardSetId)).thenReturn(setupComponentTallyDataPayload);

		when(signatureKeystore.verifySignature(any(), any(), any(), any())).thenReturn(true);
		when(signatureKeystore.generateSignature(any(), any())).thenReturn(electionEventContextPayload.getSignature().signatureContents());

		mixOfflineFacade = new MixOfflineFacade(ballotBoxService, ballotTallyService, electionEventContextPayloadService,
				setupComponentPublicKeysPayloadService, mixDecOfflineService, processPlaintextsService, setupComponentTallyDataPayloadService,
				signatureKeystore, verifyMixOfflineService,
				encryptionParametersTallyService);
	}

}
