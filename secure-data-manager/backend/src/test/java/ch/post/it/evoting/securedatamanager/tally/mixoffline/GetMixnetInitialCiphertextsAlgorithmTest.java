/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.mixoffline;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.math.BigInteger;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamal;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientMessage;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.PlaintextEqualityProof;
import ch.post.it.evoting.domain.ContextIds;
import ch.post.it.evoting.domain.EncryptedVerifiableVote;
import ch.post.it.evoting.domain.tally.GetMixnetInitialCiphertextsContext;
import ch.post.it.evoting.domain.tally.GetMixnetInitialCiphertextsInput;
import ch.post.it.evoting.securedatamanager.tally.EncryptionParametersTallyService;

/**
 * Tests of GetMixnetInitialCiphertextsAlgorithm.
 */
@DisplayName("A GetMixnetInitialCiphertextsAlgorithm calling")
class GetMixnetInitialCiphertextsAlgorithmTest extends TestGroupSetup {

	private static final Random RANDOM_SERVICE = RandomFactory.createRandom();

	private static GetMixnetInitialCiphertextsAlgorithm getMixnetInitialCiphertextsAlgorithm;
	private static EncryptionParametersTallyService encryptionParametersTallyService;
	private static ElGamal elGamal;

	private final Map<String, ElGamalMultiRecipientCiphertext> listOfConfirmedVotes = new LinkedHashMap<>();

	private ElGamalGenerator elGamalGenerator;
	private int numberOfWriteInsPlusOne;
	private ElGamalMultiRecipientPublicKey electionPublicKey;
	private EncryptedVerifiableVote encryptedVerifiableVote1;
	private EncryptedVerifiableVote encryptedVerifiableVote2;
	private GetMixnetInitialCiphertextsContext getMixnetInitialCiphertextsContext;
	private GetMixnetInitialCiphertextsInput getMixnetInitialCiphertextsInput;

	@BeforeAll
	static void setupAll() {
		encryptionParametersTallyService = mock(EncryptionParametersTallyService.class);
		elGamal = ElGamalFactory.createElGamal();
		getMixnetInitialCiphertextsAlgorithm = new GetMixnetInitialCiphertextsAlgorithm(elGamal);
	}

	@BeforeEach
	void setup() throws JsonProcessingException {
		final String electionEventId = RANDOM_SERVICE.genRandomBase16String(32).toLowerCase();
		final String ballotBoxId = RANDOM_SERVICE.genRandomBase16String(32).toLowerCase();
		numberOfWriteInsPlusOne = 1;
		final String verificationCardSetId = RANDOM_SERVICE.genRandomBase16String(32).toLowerCase();

		// To be able to verify order.
		final String verificationCardId1 = "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb";
		final ContextIds contextIds1 = new ContextIds(electionEventId, verificationCardSetId, verificationCardId1);
		final String verificationCardId2 = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
		final ContextIds contextIds2 = new ContextIds(electionEventId, verificationCardSetId, verificationCardId2);

		elGamalGenerator = new ElGamalGenerator(gqGroup);
		encryptedVerifiableVote1 = genEncryptedVerifiableVote(elGamalGenerator, contextIds1);
		encryptedVerifiableVote2 = genEncryptedVerifiableVote(elGamalGenerator, contextIds2);

		listOfConfirmedVotes.put(encryptedVerifiableVote1.contextIds().verificationCardId(), encryptedVerifiableVote1.encryptedVote());
		listOfConfirmedVotes.put(encryptedVerifiableVote2.contextIds().verificationCardId(), encryptedVerifiableVote2.encryptedVote());

		electionPublicKey = elGamalGenerator.genRandomPublicKey(numberOfWriteInsPlusOne);

		when(encryptionParametersTallyService.loadEncryptionGroup(any())).thenReturn(gqGroup);

		getMixnetInitialCiphertextsContext = new GetMixnetInitialCiphertextsContext(gqGroup, electionEventId, ballotBoxId);
		getMixnetInitialCiphertextsInput = new GetMixnetInitialCiphertextsInput(numberOfWriteInsPlusOne, listOfConfirmedVotes, electionPublicKey);
	}

	@Test
	@DisplayName("getMixnetInitialCiphertexts with null arguments throws a NullPointerException")
	void nullArgumentsThrows() {
		assertThrows(NullPointerException.class,
				() -> getMixnetInitialCiphertextsAlgorithm.getMixnetInitialCiphertexts(getMixnetInitialCiphertextsContext, null));
		assertThrows(NullPointerException.class,
				() -> getMixnetInitialCiphertextsAlgorithm.getMixnetInitialCiphertexts(null, getMixnetInitialCiphertextsInput));
	}

	@Test
	@DisplayName("getMixnetInitialCiphertexts with different encryption group for context and input throws IllegalArgumentException")
	void differentEncryptionGroupThrows() {
		final GetMixnetInitialCiphertextsContext contextDifferentGroup = spy(getMixnetInitialCiphertextsContext);
		doReturn(otherGqGroup).when(contextDifferentGroup).encryptionGroup();

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> getMixnetInitialCiphertextsAlgorithm.getMixnetInitialCiphertexts(contextDifferentGroup, getMixnetInitialCiphertextsInput));
		assertEquals("The context and input must have the same encryption group.", exception.getMessage());
	}

	@Test
	@DisplayName("getMixnetInitialCiphertexts with number of write-ins plus one too small throws an IllegalArgumentException")
	void tooSmallNumberOfWriteInsPlusOneThrows() {
		final int tooSmallDeltaHat = 0;
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new GetMixnetInitialCiphertextsInput(tooSmallDeltaHat, listOfConfirmedVotes, electionPublicKey));
		assertEquals("The number of allowed write-ins + 1 must be strictly positive.", exception.getMessage());
	}

	@Test
	@DisplayName("getMixnetInitialCiphertexts with number of write-ins plus one greater than election public key length throws an IllegalArgumentException")
	void tooSmallElectionPublicKey() {
		final int numberOfWriteInsPlusOneTmp = 5;
		final ElGamalMultiRecipientPublicKey tooShortEpk = elGamalGenerator.genRandomPublicKey(numberOfWriteInsPlusOneTmp - 1);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new GetMixnetInitialCiphertextsInput(numberOfWriteInsPlusOneTmp, listOfConfirmedVotes, tooShortEpk));
		assertEquals(String.format(
				"The election public key must have at least as many elements as the number of allowed write-ins + 1. [delta_hat: %s, delta: %s]",
				numberOfWriteInsPlusOneTmp, numberOfWriteInsPlusOneTmp - 1), exception.getMessage());
	}

	@Test
	@DisplayName("getMixnetInitialCiphertexts with one vote adds trivial votes")
	void oneVoteAddsTrivialVotes() {
		final Map<String, ElGamalMultiRecipientCiphertext> oneVote = Collections.singletonMap(
				encryptedVerifiableVote1.contextIds().verificationCardId(),
				encryptedVerifiableVote1.encryptedVote());
		final GetMixnetInitialCiphertextsInput oneVoteInput = new GetMixnetInitialCiphertextsInput(numberOfWriteInsPlusOne, oneVote,
				electionPublicKey);
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> initialCiphertexts = assertDoesNotThrow(
				() -> getMixnetInitialCiphertextsAlgorithm.getMixnetInitialCiphertexts(getMixnetInitialCiphertextsContext, oneVoteInput));
		assertEquals(3, initialCiphertexts.size());

		final GqGroup group = electionPublicKey.getGroup();
		final ElGamalMultiRecipientMessage oneMessage = elGamal.ones(group, numberOfWriteInsPlusOne);
		final ZqElement oneExponent = ZqElement.create(1, ZqGroup.sameOrderAs(group));
		final ElGamalMultiRecipientCiphertext E_trivial = elGamal.getCiphertext(oneMessage, oneExponent, electionPublicKey);
		assertEquals(E_trivial, initialCiphertexts.get(1));
		assertEquals(E_trivial, initialCiphertexts.get(2));
	}

	@Test
	@DisplayName("two votes gets valid ciphertexts")
	void twoVotesGetsValidOutput() {
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> initialCiphertexts = assertDoesNotThrow(
				() -> getMixnetInitialCiphertextsAlgorithm.getMixnetInitialCiphertexts(getMixnetInitialCiphertextsContext,
						getMixnetInitialCiphertextsInput));
		assertEquals(2, initialCiphertexts.size());

		// Ensure ordering has been done.
		assertEquals(initialCiphertexts.get(0), encryptedVerifiableVote2.encryptedVote());
		assertEquals(initialCiphertexts.get(1), encryptedVerifiableVote1.encryptedVote());
	}

	@Test
	@DisplayName("order correctly orders confirmed votes")
	void correctlyOrdersVotes() {
		Map<String, ElGamalMultiRecipientCiphertext> votes = new LinkedHashMap<>();
		votes.put("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb", elGamalGenerator.genRandomCiphertext(numberOfWriteInsPlusOne));
		votes.put("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", elGamalGenerator.genRandomCiphertext(numberOfWriteInsPlusOne));
		votes.put("11111111111111111111111111111111", elGamalGenerator.genRandomCiphertext(numberOfWriteInsPlusOne));
		votes.put("CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC", elGamalGenerator.genRandomCiphertext(numberOfWriteInsPlusOne));

		final Map<String, ElGamalMultiRecipientCiphertext> orderedVotes = getMixnetInitialCiphertextsAlgorithm.order(votes);

		final List<String> verificationCardIds = List.copyOf(orderedVotes.keySet());
		assertEquals("11111111111111111111111111111111", verificationCardIds.get(0));
		assertEquals("CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC", verificationCardIds.get(1));
		assertEquals("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", verificationCardIds.get(2));
		assertEquals("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb", verificationCardIds.get(3));
	}

	private EncryptedVerifiableVote genEncryptedVerifiableVote(final ElGamalGenerator elGamalGenerator, final ContextIds contextIds) {
		final ElGamalMultiRecipientCiphertext encryptedVote = elGamalGenerator.genRandomCiphertext(numberOfWriteInsPlusOne);
		final GqGroup encryptionGroup = encryptedVote.getGroup();
		final ZqGroup zqGroup = ZqGroup.sameOrderAs(encryptionGroup);
		final BigInteger exponentValue = RandomFactory.createRandom().genRandomInteger(encryptionGroup.getQ());
		final ZqElement exponent = ZqElement.create(exponentValue, zqGroup);
		final ElGamalMultiRecipientCiphertext exponentiatedEncryptedVote = encryptedVote.getCiphertextExponentiation(exponent);
		final ZqGroupGenerator zqGroupGenerator = new ZqGroupGenerator(zqGroup);
		final ExponentiationProof exponentiationProof = new ExponentiationProof(zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementMember());
		final PlaintextEqualityProof plaintextEqualityProof = new PlaintextEqualityProof(zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementVector(2));
		final ElGamalMultiRecipientCiphertext encryptedPartialChoieReturnCodes = elGamalGenerator.genRandomCiphertext(numberOfWriteInsPlusOne);
		return new EncryptedVerifiableVote(contextIds, encryptedVote, encryptedPartialChoieReturnCodes, exponentiatedEncryptedVote,
				exponentiationProof, plaintextEqualityProof);
	}
}
