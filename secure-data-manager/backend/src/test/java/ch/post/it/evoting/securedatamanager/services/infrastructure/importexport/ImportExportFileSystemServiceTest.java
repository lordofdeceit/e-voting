/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.importexport;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@ExtendWith(MockitoExtension.class)
class ImportExportFileSystemServiceTest {

	@Mock
	private PathResolver pathResolver;
	@TempDir
	private Path sdmDirectory;
	@TempDir
	private Path usbDirectory;
	private ImportExportFileSystemService importExportFilesystemService;

	@BeforeEach
	void setUp() {
		importExportFilesystemService = new ImportExportFileSystemService(pathResolver);
		when(pathResolver.resolveSdmPath()).thenReturn(sdmDirectory);
	}

	@Test
	void importFileSystem() throws IOException {
		// given
		initializeSourceDirectory(usbDirectory);

		// when
		importExportFilesystemService.importFileSystem(usbDirectory);

		// then
		validateTargetDirectory(sdmDirectory);
	}

	@Test
	void exportFileSystem() throws IOException {
		// given
		initializeSourceDirectory(sdmDirectory);

		// when
		importExportFilesystemService.exportFileSystem("ddfd6b5267534b47ba7ff1bbe8bee855", usbDirectory);

		// then
		validateTargetDirectory(usbDirectory);
	}

	private void initializeSourceDirectory(final Path baseDirectory) throws IOException {
		final String electionEventDirectory = "config/ddfd6b5267534b47ba7ff1bbe8bee855/";
		final String customer = electionEventDirectory + "CUSTOMER/";
		final String online = electionEventDirectory + "ONLINE/";
		final String offline = electionEventDirectory + "OFFLINE/";

		/*sdmConfig root*/
		createTestFile(baseDirectory, "sdmConfig/elections_config.json");
		createTestFile(baseDirectory, "sdmConfig/elections_config.json.p7");

		/*csr*/
		createTestFile(baseDirectory, "config/csr/0c8966b70f1342c39691d17c9921efa7.pem");

		/*election root*/
		createTestFile(baseDirectory, electionEventDirectory + "controlComponentPublicKeysPayload.0.json"); // unwanted file
		createTestFile(baseDirectory, electionEventDirectory + "controlComponentPublicKeysPayload.1.json");
		createTestFile(baseDirectory, electionEventDirectory + "controlComponentPublicKeysPayload.2.json");
		createTestFile(baseDirectory, electionEventDirectory + "controlComponentPublicKeysPayload.3.json");
		createTestFile(baseDirectory, electionEventDirectory + "controlComponentPublicKeysPayload.4.json");
		createTestFile(baseDirectory, electionEventDirectory + "controlComponentPublicKeysPayload.5.json"); // unwanted file
		createTestFile(baseDirectory, electionEventDirectory + "electionEventContextPayload.json");

		/*customer directory*/
		createTestFile(baseDirectory, customer + "input/configuration-anonymized.xml");
		createTestFile(baseDirectory, customer + "input/configuration-anonymized.xml.p7");
		createTestFile(baseDirectory, customer + "input/encryptionParametersPayload.json");
		createTestFile(baseDirectory, customer + "output/AP_election_import_Post_E2E_DEV.json");
		createTestFile(baseDirectory, customer + "output/AP_election_import_Post_E2E_DEV.json.p7");

		/*offline directory*/
		createTestFile(baseDirectory, offline + "setupComponentElectoralBoardHashesPayload.json");

		/*online directory*/
		createTestFile(baseDirectory, online + "authentication/authenticationContextData.json");
		createTestFile(baseDirectory, online + "authentication/authenticationContextData.json.sign");
		createTestFile(baseDirectory, online + "authentication/authenticationVoterData.json");
		createTestFile(baseDirectory, online + "authentication/authenticationVoterData.json.sign");

		for (String ballotId : List.of("81edb65ff0504270816063ad7de5ea49", "73c78a58a3574db7ae1df6daa0a6f2af")) {
			final String ballotDirectory = online + "electionInformation/ballots/" + ballotId + "/";

			createTestFile(baseDirectory, ballotDirectory + "ballot.json");

			final String ballotBoxDirectory = ballotDirectory + "ballotBoxes/21b65581e63c4fa19f6c646b1fed8c9a/";

			createTestFile(baseDirectory, ballotBoxDirectory + "ballotBox.json");
			createTestFile(baseDirectory, ballotBoxDirectory + "ballotBox.json.sign");
			createTestFile(baseDirectory, ballotBoxDirectory + "controlComponentBallotBoxPayload_0.json"); // unwanted file
			createTestFile(baseDirectory, ballotBoxDirectory + "controlComponentBallotBoxPayload_1.json");
			createTestFile(baseDirectory, ballotBoxDirectory + "controlComponentBallotBoxPayload_2.json");
			createTestFile(baseDirectory, ballotBoxDirectory + "controlComponentBallotBoxPayload_3.json");
			createTestFile(baseDirectory, ballotBoxDirectory + "controlComponentBallotBoxPayload_4.json");
			createTestFile(baseDirectory, ballotBoxDirectory + "controlComponentBallotBoxPayload_5.json"); // unwanted file
			createTestFile(baseDirectory, ballotBoxDirectory + "controlComponentShufflePayload_0.json"); // unwanted file
			createTestFile(baseDirectory, ballotBoxDirectory + "controlComponentShufflePayload_1.json");
			createTestFile(baseDirectory, ballotBoxDirectory + "controlComponentShufflePayload_2.json");
			createTestFile(baseDirectory, ballotBoxDirectory + "controlComponentShufflePayload_3.json");
			createTestFile(baseDirectory, ballotBoxDirectory + "controlComponentShufflePayload_4.json");
			createTestFile(baseDirectory, ballotBoxDirectory + "controlComponentShufflePayload_5.json"); // unwanted file
			createTestFile(baseDirectory, ballotBoxDirectory + "tallyComponentShufflePayload.json");
			createTestFile(baseDirectory, ballotBoxDirectory + "tallyComponentVotesPayload.json");
		}

		for (final String cardSetId : List.of("8aecc62886e947779df7258ec5c1ec9d", "8522c65f78694d6ebc309e9f9608e716")) {
			final String extendedAuthentication = online + "extendedAuthentication/" + cardSetId + "/";

			createTestFile(baseDirectory, extendedAuthentication + "extendedAuthentication.0.csv");
			createTestFile(baseDirectory, extendedAuthentication + "extendedAuthentication.0.csv.sign");
			createTestFile(baseDirectory, extendedAuthentication + "extendedAuthentication.1.csv");
			createTestFile(baseDirectory, extendedAuthentication + "extendedAuthentication.1.csv.sign");
		}

		// create unwanted files
		Files.walkFileTree(baseDirectory, new SimpleFileVisitor<>() {
			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				Files.createFile(dir.resolve("unwanted"));
				return FileVisitResult.CONTINUE;
			}
		});
	}

	private void validateTargetDirectory(final Path baseDirectory) throws IOException {
		final String electionEventDirectory = "config/ddfd6b5267534b47ba7ff1bbe8bee855/";
		final String customer = electionEventDirectory + "CUSTOMER/";
		final String online = electionEventDirectory + "ONLINE/";
		final String offline = electionEventDirectory + "OFFLINE/";

		/*sdmConfig root*/
		assertThat(baseDirectory.resolve("sdmConfig/elections_config.json")).exists();
		assertThat(baseDirectory.resolve("sdmConfig/elections_config.json.p7")).exists();

		/*csr*/
		assertThat(baseDirectory.resolve("config/csr/0c8966b70f1342c39691d17c9921efa7.pem")).exists();

		/*election root*/
		assertThat(baseDirectory.resolve(electionEventDirectory + "controlComponentPublicKeysPayload.0.json")).doesNotExist();
		assertThat(baseDirectory.resolve(electionEventDirectory + "controlComponentPublicKeysPayload.1.json")).exists();
		assertThat(baseDirectory.resolve(electionEventDirectory + "controlComponentPublicKeysPayload.2.json")).exists();
		assertThat(baseDirectory.resolve(electionEventDirectory + "controlComponentPublicKeysPayload.3.json")).exists();
		assertThat(baseDirectory.resolve(electionEventDirectory + "controlComponentPublicKeysPayload.4.json")).exists();
		assertThat(baseDirectory.resolve(electionEventDirectory + "controlComponentPublicKeysPayload.5.json")).doesNotExist();
		assertThat(baseDirectory.resolve(electionEventDirectory + "electionEventContextPayload.json")).exists();

		/*customer directory*/
		assertThat(baseDirectory.resolve(customer + "input/configuration-anonymized.xml")).exists();
		assertThat(baseDirectory.resolve(customer + "input/configuration-anonymized.xml.p7")).exists();
		assertThat(baseDirectory.resolve(customer + "input/encryptionParametersPayload.json")).exists();
		assertThat(baseDirectory.resolve(customer + "output/AP_election_import_Post_E2E_DEV.json")).exists();
		assertThat(baseDirectory.resolve(customer + "output/AP_election_import_Post_E2E_DEV.json.p7")).exists();

		/*offline directory*/
		assertThat(baseDirectory.resolve(offline + "setupComponentElectoralBoardHashesPayload.json")).exists();

		/*online directory*/
		assertThat(baseDirectory.resolve(online + "authentication/authenticationContextData.json")).exists();
		assertThat(baseDirectory.resolve(online + "authentication/authenticationContextData.json.sign")).exists();
		assertThat(baseDirectory.resolve(online + "authentication/authenticationVoterData.json")).exists();
		assertThat(baseDirectory.resolve(online + "authentication/authenticationVoterData.json.sign")).exists();

		for (final String ballotId : List.of("81edb65ff0504270816063ad7de5ea49", "73c78a58a3574db7ae1df6daa0a6f2af")) {

			final String ballotDirectory = online + "electionInformation/ballots/" + ballotId + "/";

			assertThat(baseDirectory.resolve(ballotDirectory + "ballot.json")).exists();

			final String ballotBoxDirectory = ballotDirectory + "ballotBoxes/21b65581e63c4fa19f6c646b1fed8c9a/";

			assertThat(baseDirectory.resolve(ballotBoxDirectory + "ballotBox.json")).exists();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "ballotBox.json.sign")).exists();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "controlComponentBallotBoxPayload_0.json")).doesNotExist();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "controlComponentBallotBoxPayload_1.json")).exists();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "controlComponentBallotBoxPayload_2.json")).exists();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "controlComponentBallotBoxPayload_3.json")).exists();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "controlComponentBallotBoxPayload_4.json")).exists();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "controlComponentBallotBoxPayload_5.json")).doesNotExist();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "controlComponentShufflePayload_0.json")).doesNotExist();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "controlComponentShufflePayload_1.json")).exists();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "controlComponentShufflePayload_2.json")).exists();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "controlComponentShufflePayload_3.json")).exists();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "controlComponentShufflePayload_4.json")).exists();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "controlComponentShufflePayload_5.json")).doesNotExist();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "tallyComponentShufflePayload.json")).exists();
			assertThat(baseDirectory.resolve(ballotBoxDirectory + "tallyComponentVotesPayload.json")).exists();
		}

		for (final String cardSetId : List.of("8aecc62886e947779df7258ec5c1ec9d", "8522c65f78694d6ebc309e9f9608e716")) {
			final String extendedAuthentication = online + "extendedAuthentication/" + cardSetId + "/";

			assertThat(baseDirectory.resolve(extendedAuthentication + "extendedAuthentication.0.csv")).exists();
			assertThat(baseDirectory.resolve(extendedAuthentication + "extendedAuthentication.0.csv.sign")).exists();
			assertThat(baseDirectory.resolve(extendedAuthentication + "extendedAuthentication.1.csv")).exists();
			assertThat(baseDirectory.resolve(extendedAuthentication + "extendedAuthentication.1.csv.sign")).exists();
		}

		final String sdmConfig = "sdmConfig/";
		assertThat(baseDirectory.resolve(sdmConfig + "elections_config.json")).exists();
		assertThat(baseDirectory.resolve(sdmConfig + "elections_config.json.p7")).exists();

		// check unwanted files
		Files.walkFileTree(baseDirectory, new SimpleFileVisitor<>() {
			@Override
			public FileVisitResult postVisitDirectory(final Path dir, final IOException exc) {
				assertThat(dir.resolve("unwanted")).doesNotExist();
				return FileVisitResult.CONTINUE;
			}
		});
	}

	private void createTestFile(final Path baseDirectory, final String pathName) throws IOException {
		final Path path = baseDirectory.resolve(pathName);
		Files.createDirectories(path.getParent());
		Files.createFile(path);
	}
}