/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.mixoffline;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

import java.security.SecureRandom;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamal;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.mixnet.Mixnet;
import ch.post.it.evoting.cryptoprimitives.mixnet.MixnetFactory;
import ch.post.it.evoting.cryptoprimitives.mixnet.VerifiableShuffle;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.DecryptionProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.VerifiableDecryptions;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProofFactory;
import ch.post.it.evoting.domain.tally.VerifyMixDecHelper;
import ch.post.it.evoting.domain.tally.VerifyMixDecInput;

/**
 * Tests of VerifyMixDecOfflineAlgorithm.
 */
@DisplayName("VerifyMixDecOnline with")
class VerifyMixDecOfflineAlgorithmTest extends TestGroupSetup {

	private static final Random RANDOM_SERVICE = RandomFactory.createRandom();
	private static final SecureRandom RANDOM = new SecureRandom();
	private static final int ID_LENGTH = 32;
	private static final int N = 2;

	private final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
	private final VerifyMixDecHelper verifyMixDecOnlineHelper = new VerifyMixDecHelper(gqGroup);
	private VerifyMixDecOfflineAlgorithm verifyMixDecOfflineAlgorithm;
	private VerifyMixDecOfflineContext context;
	private VerifyMixDecInput input;

	@BeforeEach
	void setup() {
		final ElGamal elGamal = ElGamalFactory.createElGamal();
		final Mixnet mixnet = MixnetFactory.createMixnet();
		final ZeroKnowledgeProof zeroKnowledgeProof = ZeroKnowledgeProofFactory.createZeroKnowledgeProof();
		verifyMixDecOfflineAlgorithm = new VerifyMixDecOfflineAlgorithm(elGamal, mixnet, zeroKnowledgeProof);

		final String electionEventId = RANDOM_SERVICE.genRandomBase16String(ID_LENGTH).toLowerCase();
		final String ballotBoxId = RANDOM_SERVICE.genRandomBase16String(ID_LENGTH).toLowerCase();
		final int l = RANDOM.nextInt(5) + 1;
		context = new VerifyMixDecOfflineContext(gqGroup, electionEventId, ballotBoxId, l);

		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> initialCiphertexts = elGamalGenerator.genRandomCiphertextVector(N, l);
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> ciphertexts = elGamalGenerator.genRandomCiphertextVector(N, l);
		final VerifiableShuffle verifiableShuffle = new VerifiableShuffle(ciphertexts, verifyMixDecOnlineHelper.createShuffleArgument(N, l));
		final DecryptionProof decryptionProof = new DecryptionProof(zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementVector(l));
		final VerifiableDecryptions verifiableDecryptions = new VerifiableDecryptions(ciphertexts, GroupVector.of(decryptionProof, decryptionProof));
		final SetupComponentPublicKeys setupComponentPublicKeys = verifyMixDecOnlineHelper.createSetupComponentPublicKeys(l);
		final ElGamalMultiRecipientPublicKey electionPublicKey = setupComponentPublicKeys.electionPublicKey();
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmElectionPublicKeys = setupComponentPublicKeys.combinedControlComponentPublicKeys()
				.stream()
				.map(ControlComponentPublicKeys::ccmjElectionPublicKey)
				.collect(GroupVector.toGroupVector());
		final ElGamalMultiRecipientPublicKey electoralBoardPublicKey = setupComponentPublicKeys.electoralBoardPublicKey();
		input = new VerifyMixDecInput(initialCiphertexts, Arrays.asList(verifiableShuffle, verifiableShuffle, verifiableShuffle, verifiableShuffle),
				Arrays.asList(verifiableDecryptions, verifiableDecryptions, verifiableDecryptions, verifiableDecryptions), electionPublicKey,
				ccmElectionPublicKeys, electoralBoardPublicKey);
	}

	@Test
	@DisplayName("null parameters throws a NullPointerException")
	void verifyMixDecOnlineWithNullParametersThrows() {
		assertThrows(NullPointerException.class, () -> verifyMixDecOfflineAlgorithm.verifyMixDecOffline(null, input));
		assertThrows(NullPointerException.class, () -> verifyMixDecOfflineAlgorithm.verifyMixDecOffline(context, null));
	}

	@Test
	@DisplayName("initial ciphertexts not having same number of allowed write-ins + 1 elements throws an IllegalArgumentException")
	void verifyMixDecOfflineWithBadNumberOfInitialCiphertextsThrows() {
		final VerifyMixDecInput tooLongInitialCiphertextsInput = spy(input);
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> tooLongInitialCiphertexts = elGamalGenerator.genRandomCiphertextVector(N,
				context.numberOfAllowedWriteInsPlusOne() + 1);
		doReturn(tooLongInitialCiphertexts).when(tooLongInitialCiphertextsInput).initialCiphertexts();
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> verifyMixDecOfflineAlgorithm.verifyMixDecOffline(context, tooLongInitialCiphertextsInput));
		assertEquals("The votes must have exactly the number of allowed write-ins + 1 elements.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("different group in context and input throws an IllegalArgumentException")
	void verifyMixDecOfflineWithDifferentGroupThrows() {
		final VerifyMixDecOfflineContext otherGroupContext = spy(context);
		final GqGroup otherGroup = GroupTestData.getDifferentGqGroup(gqGroup);
		doReturn(otherGroup).when(otherGroupContext).encryptionGroup();
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> verifyMixDecOfflineAlgorithm.verifyMixDecOffline(otherGroupContext, input));
		assertEquals("The context and input must have the same encryption group.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("with valid parameters does not throw")
	void verifyMixDecOfflineValidParametersDoesNotThrow() {
		final GqGroup largeGqGroup = GroupTestData.getLargeGqGroup();
		final ElGamalGenerator largeElGamalGenerator = new ElGamalGenerator(largeGqGroup);
		final ZqGroupGenerator largeZqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(largeGqGroup));
		final VerifyMixDecHelper largeVerifyMixDecHelper = new VerifyMixDecHelper(largeGqGroup);
		final int l = context.numberOfAllowedWriteInsPlusOne();
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> initialCiphertexts = largeElGamalGenerator.genRandomCiphertextVector(N, l);
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> ciphertexts = largeElGamalGenerator.genRandomCiphertextVector(N, l);
		final VerifiableShuffle verifiableShuffle = new VerifiableShuffle(ciphertexts, largeVerifyMixDecHelper.createShuffleArgument(N, l));
		final DecryptionProof decryptionProof = new DecryptionProof(largeZqGroupGenerator.genRandomZqElementMember(),
				largeZqGroupGenerator.genRandomZqElementVector(l));
		final VerifiableDecryptions verifiableDecryptions = new VerifiableDecryptions(ciphertexts, GroupVector.of(decryptionProof, decryptionProof));
		final SetupComponentPublicKeys setupComponentPublicKeys = largeVerifyMixDecHelper.createSetupComponentPublicKeys(l);
		final ElGamalMultiRecipientPublicKey electionPublicKey = setupComponentPublicKeys.electionPublicKey();
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmElectionPublicKeys = setupComponentPublicKeys.combinedControlComponentPublicKeys()
				.stream()
				.map(ControlComponentPublicKeys::ccmjElectionPublicKey)
				.collect(GroupVector.toGroupVector());
		final ElGamalMultiRecipientPublicKey electoralBoardPublicKey = setupComponentPublicKeys.electoralBoardPublicKey();
		input = new VerifyMixDecInput(initialCiphertexts, Arrays.asList(verifiableShuffle, verifiableShuffle, verifiableShuffle, verifiableShuffle),
				Arrays.asList(verifiableDecryptions, verifiableDecryptions, verifiableDecryptions, verifiableDecryptions), electionPublicKey,
				ccmElectionPublicKeys, electoralBoardPublicKey);
		context = new VerifyMixDecOfflineContext(largeGqGroup, context.electionEventId(), context.ballotBoxId(),
				context.numberOfAllowedWriteInsPlusOne());

		assertDoesNotThrow(() -> verifyMixDecOfflineAlgorithm.verifyMixDecOffline(context, input));
	}
}
