/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.Base64;
import ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.symmetric.Symmetric;
import ch.post.it.evoting.cryptoprimitives.symmetric.SymmetricFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.GqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.securedatamanager.commons.Constants;

/**
 * Tests of GenCredDatAlgorithm.
 */
@DisplayName("GenCredDatAlgorithm")
class GenCredDatAlgorithmTest {

	private final static int bound = 12;
	private final static int maxSize = 4;

	private final Random rand = RandomFactory.createRandom();
	private final SecureRandom srand = new SecureRandom();
	private final int size = srand.nextInt(bound) + 1;
	private final List<String> verificationCardIds = Stream.generate(() -> rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase())
			.limit(size).toList();
	private final List<String> startVotingKeys = Stream.generate(() -> rand.genRandomBase32String(Constants.SVK_LENGTH).toLowerCase()).limit(size)
			.toList();
	private final int delta = srand.nextInt(bound) + 1;
	private final int psi = srand.nextInt(bound) + 1;
	private final GqGroup gqGroup = GroupTestData.getGqGroup();
	private final ZqGroup zqGroup = new ZqGroup(gqGroup.getQ());
	private final ZqGroupGenerator zqGroupGenerator = new ZqGroupGenerator(zqGroup);
	private final GroupVector<ZqElement, ZqGroup> verificationCardSecretKeys = Stream.generate(zqGroupGenerator::genRandomZqElementMember).limit(size)
			.collect(GroupVector.toGroupVector());
	private final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
	private final ElGamalMultiRecipientPublicKey electionPublicKey = elGamalGenerator.genRandomPublicKey(delta);
	private final GqGroupGenerator generator = new GqGroupGenerator(electionPublicKey.getGroup());
	private final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions = Stream.generate(generator::genSmallPrimeMember).limit(maxSize)
			.collect(GroupVector.toGroupVector());
	private final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey = elGamalGenerator.genRandomPublicKey(psi);
	private final String electionEventId = rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase();
	private final String verificationCardSetId = rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase();
	private final List<String> actualVotingOptions = Stream.generate(() -> rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase())
			.limit(maxSize).toList();
	private final List<String> ciSelections = Stream.generate(() -> rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase())
			.limit(maxSize).toList();
	private final List<String> ciVotingOptions = Stream.generate(() -> rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase())
			.limit(maxSize).toList();
	private final Hash hash = HashFactory.createHash();
	private final Symmetric symmetric = SymmetricFactory.createSymmetric();
	private final Base64 base64 = BaseEncodingFactory.createBase64();

	private GenCredDatContext context;
	private GenCredDatInput input;
	private GenCredDatAlgorithm genCredDatAlgorithm;

	@BeforeEach
	void setup() {
		context = buildCredDataContextWithSuppliedVotingOptions(actualVotingOptions);
		input = new GenCredDatInput(verificationCardIds, verificationCardSecretKeys, startVotingKeys);

		assertNotNull(hash, "hashService");
		assertNotNull(symmetric, "symmetricService");

		genCredDatAlgorithm = new GenCredDatAlgorithm(hash, symmetric, base64);
	}

	@Test
	void happyPath() {
		GenCredDatOutput output = genCredDatAlgorithm.genCredDat(context, input);

		assertNotNull(output, "output is null");
		assertNotNull(output.verificationCardKeystores(), "output.verificationCardKeystores() is null");
		assertEquals(output.verificationCardKeystores().size(), input.verificationCardIds().size());
	}

	@Test
	void nullContextArgument() {
		assertThrows(NullPointerException.class, () -> genCredDatAlgorithm.genCredDat(null, input));
	}

	@Test
	void nullInputArgument() {
		assertThrows(NullPointerException.class, () -> genCredDatAlgorithm.genCredDat(context, null));
	}

	@ParameterizedTest
	@MethodSource("generateActualVotingOptionsData")
	void test(List<String> actualVotingOptions ) {
		context = buildCredDataContextWithSuppliedVotingOptions(actualVotingOptions);
		input = new GenCredDatInput(verificationCardIds, verificationCardSecretKeys, startVotingKeys);

		assertNotNull(hash, "hashService");
		assertNotNull(symmetric, "symmetricService");

		genCredDatAlgorithm = new GenCredDatAlgorithm(hash, symmetric, base64);

		IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, ()->genCredDatAlgorithm.genCredDat(context, input));
		assertEquals(String.format("Voting options should match a valid xml xs:token",
				actualVotingOptions), Throwables.getRootCause(illegalArgumentException).getMessage());
	}

	static Stream<Arguments> generateActualVotingOptionsData() {
		return Stream.of(
				Arguments.of(Arrays.asList("a  b", "b", "c","d")),
				Arguments.of(Arrays.asList("x", "<xyz>", "z", "a"))
		);
	}

	@Test
	void invalidActualVotingOptionsLengthCheck() {
		List<String> fiftyOne = List.of("a","b","c", "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXY");

		context = buildCredDataContextWithSuppliedVotingOptions(fiftyOne);

		input = new GenCredDatInput(verificationCardIds, verificationCardSecretKeys, startVotingKeys);
		IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> genCredDatAlgorithm.genCredDat(context, input));
		assertEquals(String.format("The actual voting option length must not exceed 50 characters.",
				actualVotingOptions), Throwables.getRootCause(illegalArgumentException).getMessage());
	}

	private GenCredDatContext buildCredDataContextWithSuppliedVotingOptions(List<String> actualVotingOptions) {
		return new GenCredDatContext
				.Builder()
				.setElectionEventId(electionEventId)
				.setVerificationCardSetId(verificationCardSetId)
				.setElectionPublicKey(electionPublicKey)
				.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
				.setEncodedVotingOptions(encodedVotingOptions)
				.setActualVotingOptions(actualVotingOptions)
				.setCorrectnessInformationSelections(ciSelections)
				.setCorrectnessInformationVotingOptions(ciVotingOptions)
				.build();
	}

}