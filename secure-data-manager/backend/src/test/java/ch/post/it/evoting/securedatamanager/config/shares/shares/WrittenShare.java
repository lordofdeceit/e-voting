/*
 * (c) Original Developers indicated in attribution.txt, 2022. All Rights Reserved.
 */
package ch.post.it.evoting.securedatamanager.config.shares.shares;

import java.util.Base64;

/**
 * Meant to hold the share values for testing. Makes copies of the values since part of the testing consists on deleting the values.
 *
 * @param secretKeyBytes          private part
 * @param encryptedShare          public part
 * @param encryptedShareSignature public part
 */
public record WrittenShare(byte[] secretKeyBytes, byte[] encryptedShare, byte[] encryptedShareSignature) {

	public WrittenShare(final byte[] secretKeyBytes, final byte[] encryptedShare, final byte[] encryptedShareSignature) {

		this.secretKeyBytes = secretKeyBytes.clone();
		this.encryptedShare = encryptedShare.clone();
		this.encryptedShareSignature = encryptedShareSignature.clone();
	}

	/**
	 * @return Returns the secretKeyBytes.
	 */
	@Override
	public byte[] secretKeyBytes() {
		return secretKeyBytes.clone();
	}

	/**
	 * @return Returns the encryptedShare.
	 */
	@Override
	public byte[] encryptedShare() {
		return encryptedShare.clone();
	}

	/**
	 * @return Returns the encryptedShareSignature.
	 */
	@Override
	public byte[] encryptedShareSignature() {
		return encryptedShareSignature.clone();
	}

	/**
	 * @see Object#toString()
	 */
	@Override
	public String toString() {
		return "WrittenShare [_secretKeyBytes=" + Base64.getEncoder().encodeToString(secretKeyBytes) + ", _encryptedShare=" + Base64.getEncoder()
				.encodeToString(encryptedShare) + ", _encryptedShareSignature=" + Base64.getEncoder().encodeToString(encryptedShareSignature) + "]";
	}

}
