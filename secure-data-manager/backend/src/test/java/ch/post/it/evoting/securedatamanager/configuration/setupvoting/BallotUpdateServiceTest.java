/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.io.StringReader;
import java.io.UncheckedIOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.stream.Stream;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionOption;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.EncryptionParametersPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.securedatamanager.EncryptionParametersPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.EncryptionParametersPayloadService;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

import uk.org.webcompere.systemstubs.environment.EnvironmentVariables;
import uk.org.webcompere.systemstubs.jupiter.SystemStub;
import uk.org.webcompere.systemstubs.jupiter.SystemStubsExtension;

@ExtendWith({ SystemStubsExtension.class })
class BallotUpdateServiceTest {

	private static final String encryptionParametersPayload_JSON = """
			{
			  "encryptionGroup": {
			  	"p": "0xCE9E0307D2AE75BDBEEC3E0A6E71A279417B56C955C602FFFD067586BACFDAC3BCC49A49EB4D126F5E9255E57C14F3E09492B6496EC8AC1366FC4BB7F678573FA2767E6547FA727FC0E631AA6F155195C035AF7273F31DFAE1166D1805C8522E95F9AF9CE33239BF3B68111141C20026673A6C8B9AD5FA8372ED716799FE05C0BB6EAF9FCA1590BD9644DBEFAA77BA01FD1C0D4F2D53BAAE965B1786EC55961A8E2D3E4FE8505914A408D50E6B99B71CDA78D8F9AF1A662512F8C4C3A9E72AC72D40AE5D4A0E6571135CBBAAE08C7A2AA0892F664549FA7EEC81BA912743F3E584AC2B2092243C4A17EC98DF079D8EECB8B885E6BBAFA452AAFA8CB8C08024EFF28DE4AF4AC710DCD3D66FD88212101BCB412BCA775F94A2DCE18B1A6452D4CF818B6D099D4505E0040C57AE1F3E84F2F8E07A69C0024C05ACE05666A6B63B0695904478487E78CD0704C14461F24636D7A3F267A654EEDCF8789C7F627C72B4CBD54EED6531C0E54E325D6F09CB648AE9185A7BDA6553E40B125C78E5EAA867","q": "0x674F0183E9573ADEDF761F053738D13CA0BDAB64AAE3017FFE833AC35D67ED61DE624D24F5A68937AF492AF2BE0A79F04A495B24B7645609B37E25DBFB3C2B9FD13B3F32A3FD393FE07318D5378AA8CAE01AD7B939F98EFD708B368C02E429174AFCD7CE71991CDF9DB40888A0E10013339D3645CD6AFD41B976B8B3CCFF02E05DB757CFE50AC85ECB226DF7D53BDD00FE8E06A796A9DD574B2D8BC3762ACB0D47169F27F4282C8A52046A8735CCDB8E6D3C6C7CD78D3312897C6261D4F3956396A0572EA50732B889AE5DD570463D15504497B322A4FD3F7640DD4893A1F9F2C256159049121E250BF64C6F83CEC7765C5C42F35DD7D229557D465C60401277F946F257A563886E69EB37EC4109080DE5A095E53BAFCA516E70C58D32296A67C0C5B684CEA282F002062BD70F9F42797C703D34E0012602D6702B33535B1D834AC8223C243F3C66838260A230F9231B6BD1F933D32A776E7C3C4E3FB13E395A65EAA776B298E072A7192EB784E5B245748C2D3DED32A9F205892E3C72F55433","g": "0x2"
			  },
			  "seed": "22800",
			  "smallPrimes": [5,17,19,37,41,43,53,59,61,67,71,73,79,89,97,101,109,113,127,131,137,139,149,151,163,173,197,199,211,229,239,241,251,263,269,271,277,283,293,307,311,313,337,353,359,379,383,397,401,409,421,431,463,467,491,499,521,523,541,557,569,587,593,607,617,631,641,643,661,677,683,701,739,743,751,761,773,787,797,809,811,823,827,829,839,857,859,863,887,911,937,947,967,971,983,991,1013,1031,1051,1061,1063,1069,1091,1097,1103,1109,1123,1129,1151,1163,1217,1259,1277,1283,1289,1291,1297,1321,1423,1429,1439,1453,1471,1481,1483,1487,1489,1543,1579,1583,1601,1613,1621,1663,1693,1699,1741,1747,1759,1787,1789,1801,1823,1861,1867,1871,1873,1879,1889,1913,1973,1979,1993,1997,1999,2003,2011,2027,2063,2069,2081,2083,2087,2089,2111,2113,2137,2153,2161,2207,2221,2239,2273,2281,2287,2339,2341,2347,2351,2357,2377,2389,2393,2411,2417,2423,2437,2441,2467,2473,2477,2521,2531,2539,2557,2591,2609,2633,2647,2659,2663,2677,2683,2693,2731,2741,2753,2777,2797,2803,2819,2843,2851,2857,2879,2897,2903,2909,2927,2957,2971,3001,3023,3041,3061,3079,3089,3119,3137,3169,3187,3203,3209,3221,3229,3251,3253,3271,3301,3307,3313,3329,3347,3371,3391,3449,3461,3463,3467,3469,3517,3527,3547,3557,3581,3583,3607,3623,3637,3677,3701,3719,3727,3733,3739,3767,3793,3797,3821,3823,3833,3851,3853,3877,3911,3923,3931,3943,3967,4001,4003,4021,4027,4049,4051,4091,4099,4111,4129,4133,4139,4177,4219,4243,4253,4261,4273,4283,4289,4327,4349,4357,4373,4421,4441,4457,4481,4507,4517,4519,4567,4591,4603,4637,4643,4651,4673,4679,4729,4751,4759,4789,4793,4831,4861,4903,4909,4919,4969,4973,5051,5081,5087,5099,5113,5119,5147,5153,5179,5233,5273,5297,5309,5323,5333,5347,5381,5387,5393,5417,5431,5437,5443,5477,5483,5501,5503,5507,5519,5527,5563,5569,5581,5623,5639,5641,5653,5659,5683,5693,5701,5743,5791,5801,5807,5839,5849,5869,5879,5897,5923,5927,5939,6007,6037,6043,6047,6053,6073,6079,6101,6113,6121,6131,6143,6151,6163,6197,6199,6203,6211,6217,6247,6257,6263,6311,6361,6367,6397,6421,6427,6449,6451,6469,6481,6521,6547,6551,6553,6571,6581,6599,6607,6637,6659,6661,6691,6701,6703,6761,6763,6791,6827,6829,6833,6841,6869,6871,6883,6907,6911,6917,6947,6961,6977,6983,7001,7013,7019,7039,7069,7079,7103,7121,7129,7151,7177,7187,7207,7219,7243,7253,7283,7321,7331,7349,7393,7411,7417,7433,7481,7487,7523,7529,7537,7547,7549,7559,7583,7589,7591,7603,7607,7621,7643,7649,7669,7691,7741,7753,7823,7829,7841,7877,7883,7901,7907,7927,7937,7993,8009,8011,8017,8069,8087,8089,8093,8111,8167,8179,8191,8219,8221,8263,8269,8273,8287,8291,8297,8311,8317,8353,8377,8387,8423,8443,8461,8501,8521,8537,8539,8543,8563,8597,8609,8669,8677,8681,8707,8713,8719,8731,8741,8753,8803,8807,8821,8839,8887,8893,8923,8929,8951,8963,8969,9007,9011,9029,9049,9059,9067,9091,9103,9109,9133,9137,9157,9161,9181,9199,9209,9227,9241,9257,9283,9293,9337,9341,9343,9371,9391,9403,9419,9421,9431,9439,9461,9463,9467,9473,9511,9587,9613,9623,9629,9643,9649,9661,9697,9719,9787,9803,9833,9851,9857,9859,9883,9923,9929,9931,9949,9967,9973,10009,10037,10061,10099,10103,10111,10139,10141,10151,10163,10169,10177,10181,10247,10253,10259,10289,10301,10303,10313,10321,10343,10369,10391,10399,10427,10433,10457,10487,10501,10529,10567,10589,10613,10627,10667,10687,10771,10781,10789,10799,10847,10853,10861,10867,10883,10903,10909,10939,10949,10973,10979,10987,10993,11003,11027,11059,11069,11071,11087,11117,11159,11273,11317,11329,11351,11369,11383,11423,11437,11443,11467,11471,11483,11489,11491,11503,11519,11527,11579,11593,11617,11677,11699,11701,11717,11731,11777,11783,11807,11821,11833,11867,11897,11927,11969,11971,11981,12041,12049,12073,12109,12119,12143,12163,12203,12211,12239,12241,12253,12263,12277,12289,12301,12323,12343,12347,12379,12413,12421,12457,12479,12487,12491,12497,12511,12539,12541,12569,12577,12583,12589,12611,12613,12619,12653,12659,12671,12703,12713,12757,12763,12781,12823,12829,12893,12907,12953,12959,12967,12979,12983,13003,13033,13109,13127,13147,13151,13163,13183,13187,13217,13219,13229,13241,13259,13267,13297,13327,13331,13339,13367,13399,13421,13451,13463,13477,13487,13513,13523,13577,13591,13597,13627,13633,13709,13711,13721,13723,13729,13751,13757,13763,13789,13799,13841,13873,13877,13879,13901,13963,13997,14009,14011,14029,14033,14051,14057,14081,14087,14143,14149,14159,14173,14197,14293,14321,14323,14341,14347,14369,14387,14401,14407,14411,14419,14437,14449,14489,14543,14557,14563,14621,14627,14633,14653,14699,14717,14723,14731,14737,14747,14759,14771,14779,14783,14813,14821,14827,14879,14897,14923,14929,14951,14969,14983,15013,15017,15053,15061,15091,15107,15121,15131,15139,15149,15161,15193,15199,15233,15271,15289,15299,15307,15313,15329,15331,15359,15361,15377,15383,15391,15427,15439,15443,15473,15511,15551,15581,15583,15601,15607,15619,15629,15647,15671,15683,15727,15731,15761,15773,15797,15803,15809,15859,15881,15889,15907,15923,15973,15991,16057,16067,16069,16087,16103,16111,16127,16183,16187,16189,16193,16217,16223,16249,16253,16267,16333,16339,16349,16369,16381,16411,16427,16447,16477,16481,16519,16529,16561,16619,16631,16633,16649,16651,16657,16661,16673,16691,16703,16729,16741,16747,16759,16811,16829,16843,16879,16883,16903,16921,16931,16937,16993,17011,17027,17033,17041,17077,17093,17099,17107,17117,17137,17189,17191,17203,17239,17257,17293,17317,17321,17333,17341,17351,17377,17383,17387,17393,17401,17417,17419,17471,17477,17483,17489,17491,17497,17539,17551,17569,17573,17579,17581,17599,17623,17657,17683,17713,17747,17783,17791,17827,17837,17839,17891,17909,17923,17957,18043,18047,18061,18119,18149,18169,18191,18199,18217,18223,18229,18251,18269,18287,18301,18307,18329,18371,18379,18401,18427,18433,18439,18493,18503,18521,18523,18539,18541,18553,18587,18617,18671,18679,18713,18719,18731,18773,18859,18911,18917,18919,18947,18959,18973,18979,19001,19009,19051,19073,19079,19081,19087,19139,19141,19157,19163,19207,19213,19249,19259,19273,19289,19301,19319,19373,19379,19381,19387,19403,19417,19423,19433,19457,19463,19469,19471,19489,19501,19531,19543,19553,19559,19571,19577,19597,19681,19697,19699,19717,19727,19751,19759,19801,19819,19841,19843,19853,19861,19889,19937,19973,19979,19991,20011,20021,20029,20047,20113,20129,20143,20149,20161,20231,20249,20261,20287,20297,20323,20333,20353,20357,20389,20407,20411,20479,20507,20521,20543,20549,20563,20593,20641,20681,20707,20717,20747,20759,20773,20807,20857,20873,20887,20921,20929,20947,20963,20983,21011,21017,21019,21059,21089,21101,21149,21169,21179,21191,21227,21247,21269,21283,21313,21323,21341,21379,21433,21467],
			  "signature": {
			  	"signatureContents": "IP1gw2ML960RtFU85PSOcFBpyNl6JKhfyl1CcAiyLliifmw7lcUd5YqQKGP8+kCuaLv84ST66umPSz6zc4K3slPsKjTc6NSOZsFTnGYIMK2MPukq1mDaHSfAqmhZb/UdCFcj5wCx66/nFmnkqCbttAwMpeUpz8k9KMIQokw1V29DusTQXoOf8uADpvdDF3k3O4+bziqTTehBibfgfweif5udVbUrvdeLQuygZHwxCODBBJPmAhp942QY7Rvm9BQpfLh/bPFhdN1d/CHv+3tWsaVXSIv6rxjNuFcXr7azrEjUmanRzrDc2D8wi+PHd//ayBzxnbxcCIZqXdGbO6rg0HhjjippfPMymemchNRNrAz1cdQmJBzAXPtv04pgsumyeWjSzB5tBGY+F5nUuUVs3kCDVQwXToYeD2hGBM5vKXA54NP4ag8YffQQzLmV1nDowIbu4E4ytuWDzF+ji/4qm7HDZWc2l1zrRDN4MpdND6Grf1HhYwmLIlJ9Kdv07hQQ"
			  }
			}
			""";
	private static final String electionEventId = "be658216a38b421b943457846c6a68f9";
	private static String JSON_BALLOT_1;
	private static String JSON_BALLOT_2;
	@SystemStub
	private static EnvironmentVariables environmentVariables;
	private static PathResolver pathResolver;
	private static Path payloadPath;
	private static ObjectMapper objectMapper;
	private JsonObject ballotObject1;
	private JsonObject ballotObject2;
	private BallotUpdateService ballotUpdateService;

	@BeforeAll
	static void setAll(
			@TempDir
			final Path tempDir) throws IOException {

		pathResolver = new PathResolver(tempDir.toString());

		final Path basePath = tempDir.resolve(Constants.CONFIG_FILES_BASE_DIR)
				.resolve(electionEventId)
				.resolve(Constants.CONFIG_DIR_NAME_CUSTOMER)
				.resolve(Constants.CONFIG_DIR_NAME_INPUT);

		payloadPath = basePath.resolve(Constants.CONFIG_FILE_NAME_ENCRYPTION_PARAMETERS_PAYLOAD_JSON);
		objectMapper = DomainObjectMapper.getNewInstance();

		Files.createDirectories(basePath);

		final ClassLoader classLoader = BallotUpdateServiceTest.class.getClassLoader();
		JSON_BALLOT_1 = IOUtils.toString(Objects.requireNonNull(classLoader.getResource("ballot.json")).openStream(),
						StandardCharsets.UTF_8)
				.replaceAll("(\"representation\": \"\\d+\")", "\"representation\": \"1\"");

		JSON_BALLOT_2 = IOUtils.toString(Objects.requireNonNull(classLoader.getResource("ballot2.json")).openStream(),
						StandardCharsets.UTF_8)
				.replaceAll("(\"representation\": \"\\d+\")", "\"representation\": \"1\"");
	}

	@BeforeEach
	void setup() throws IOException {
		try (final JsonReader jsonReader = Json.createReader(new StringReader(JSON_BALLOT_1))) {

			ballotObject1 = jsonReader.readObject();
		}
		try (final JsonReader jsonReader = Json.createReader(new StringReader(JSON_BALLOT_2))) {

			ballotObject2 = jsonReader.readObject();
		}

		Files.deleteIfExists(payloadPath);
		Files.writeString(payloadPath, encryptionParametersPayload_JSON);

		final EncryptionParametersPayloadFileRepository encryptionParametersPayloadFileRepository = new EncryptionParametersPayloadFileRepository(
				pathResolver, objectMapper);
		final EncryptionParametersPayloadService encryptionParametersPayloadService = new EncryptionParametersPayloadService(
				encryptionParametersPayloadFileRepository);

		final SignatureKeystore<Alias> signatureKeystoreServiceSdmConfig = mock(SignatureKeystore.class);
		final CantonConfigConfigFileRepository cantonConfigConfigFileRepository = new CantonConfigConfigFileRepository(
				pathResolver, signatureKeystoreServiceSdmConfig);
		final CantonConfigConfigService cantonConfigConfigService = new CantonConfigConfigService(
				cantonConfigConfigFileRepository);

		ballotUpdateService = new BallotUpdateService(encryptionParametersPayloadService, objectMapper, cantonConfigConfigService);
	}

	@Test
	void happyPathForOneBallot() {
		environmentVariables.set("SECURITY_LEVEL", null);

		final Ballot ballot = toBallot(ballotObject1);
		final Map<String, String> mapRepresentation = new TreeMap<>();

		// Extract original representation values of ballot1
		ballot.getOrderedElectionOptions().forEach(option -> mapRepresentation.put(option.getId(), option.getRepresentation()));

		final JsonObject updatedBallotObject = ballotUpdateService.updateOptionsRepresentation(electionEventId, ballotObject1);

		assertNotNull(updatedBallotObject);

		final Ballot updatedBallot = toBallot(updatedBallotObject);

		assertTrue(optionStream(updatedBallot).noneMatch(option -> option.getRepresentation().equals(mapRepresentation.get(option.getId()))),
				"Not all representation of ballot have been set.");

		final List<Long> representationValues = optionStream(updatedBallot)
				.map(ElectionOption::getRepresentation)
				.map(Long::valueOf)
				.toList();

		assertEquals(representationValues, representationValues.stream().sorted(Comparator.comparing(Long::valueOf)).toList(),
				"The representation values are not ascending.");
	}

	@Test
	void happyPathWithTwoBallots() throws ResourceNotFoundException {
		environmentVariables.set("SECURITY_LEVEL", null);
		final Map<String, String> mapRepresentation = new TreeMap<>();

		final Ballot ballot1 = toBallot(ballotObject1);

		// Extract original representation values of ballot1
		ballot1.getOrderedElectionOptions().forEach(option -> mapRepresentation.put(option.getId(), option.getRepresentation()));

		// Update the representations of ballot1
		final JsonObject updatedBallotObject1 = ballotUpdateService.updateOptionsRepresentation(electionEventId, ballotObject1);
		final Ballot updatedBallot1 = toBallot(updatedBallotObject1);

		// Check that all representation of ballot1 have been set
		assertTrue(optionStream(updatedBallot1).noneMatch(option -> option.getRepresentation().equals(mapRepresentation.get(option.getId()))),
				"Not all representation of ballot1 have been set.");

		// Check that the representation in ballot1 are in ascending order
		final List<Long> ballot1RepresentationValues = optionStream(updatedBallot1)
				.map(ElectionOption::getRepresentation)
				.map(Long::valueOf)
				.toList();

		assertEquals(ballot1RepresentationValues, ballot1RepresentationValues.stream().sorted(Comparator.comparing(Long::valueOf)).toList(),
				"The representation in ballot1 values are not in ascending order.");

		// Get higher representation value of ballot1
		final long greaterRepresentationId = Long.parseLong(optionStream(updatedBallot1)
				.map(ElectionOption::getRepresentation)
				.max(Comparator.comparingLong(Long::valueOf))
				.orElseThrow());

		final Ballot ballot2 = toBallot(ballotObject2);

		// Extract ID of the options that are in both ballots
		final List<String> commonOptionIds = optionStream(ballot2)
				.map(ElectionOption::getId)
				.filter(mapRepresentation::containsKey)
				.toList();

		// Extract original representation values of ballot2 (but not in ballot1)
		optionStream(ballot2)
				.filter(option -> !commonOptionIds.contains(option.getId()))
				.forEach(option -> mapRepresentation.put(option.getId(), option.getRepresentation()));

		// Update the representations of ballot2
		final JsonObject updatedBallotObject2 = ballotUpdateService.updateOptionsRepresentation(electionEventId, ballotObject2);
		final Ballot updatedBallot2 = toBallot(updatedBallotObject2);

		// Check that all representation of ballot2 have been set
		assertTrue(optionStream(updatedBallot2)
						.noneMatch(option -> option.getRepresentation().equals(mapRepresentation.get(option.getId()))),
				"Not all representation of ballot2 have been set.");

		// Check that the representation in ballot2 are in ascending order
		final List<Long> ballot2RepresentationValues = optionStream(updatedBallot2)
				.filter(option -> !commonOptionIds.contains(option.getId()))
				.map(ElectionOption::getRepresentation)
				.map(Long::valueOf)
				.toList();

		assertEquals(ballot2RepresentationValues, ballot2RepresentationValues.stream().sorted(Comparator.comparing(Long::valueOf)).toList(),
				"The representation in ballot2 values are not in ascending order.");

		// Check that all representation of ballot2 have higher values than in ballot1
		assertTrue(optionStream(updatedBallot2)
						.filter(option -> !commonOptionIds.contains(option.getId()))
						.map(ElectionOption::getRepresentation)
						.map(Long::parseLong)
						.allMatch(representation -> representation > greaterRepresentationId),
				String.format("Representation of Ballot2 must be greater than Ballot1 higher value (except for id=%s)", commonOptionIds));
	}

	private Stream<ElectionOption> optionStream(final Ballot ballot) {
		return ballot.contests().stream().flatMap(c -> c.options().stream());
	}

	private Ballot toBallot(final JsonObject ballotObject) {
		final Ballot ballot;
		try {
			ballot = objectMapper.readValue(ballotObject.toString(), Ballot.class);
		} catch (final IOException e) {
			throw new UncheckedIOException("Cannot deserialize the ballot box json string to a valid Ballot object.", e);
		}
		return ballot;
	}

	@Test
	void moreOptionsThanPrimes() throws IOException {
		environmentVariables.set("SECURITY_LEVEL", "TESTING_ONLY");

		final GqGroup gqGroup = new GqGroup(new BigInteger("3B", 16), new BigInteger("1D", 16), new BigInteger("3", 16));
		final EncryptionParametersPayload encryptionParametersPayload = new EncryptionParametersPayload(gqGroup, "13",
				PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers(gqGroup, 7), new CryptoPrimitivesSignature(new byte[] {}));
		Files.deleteIfExists(payloadPath);
		Files.write(payloadPath, objectMapper.writeValueAsBytes(encryptionParametersPayload));

		final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class,
				() -> ballotUpdateService.updateOptionsRepresentation(electionEventId, ballotObject1));

		final String message = "The number of ballot options must be smaller than or equal to the available primes. [#options:432, #primes:7]";
		assertEquals(message, ex.getMessage());
	}

}