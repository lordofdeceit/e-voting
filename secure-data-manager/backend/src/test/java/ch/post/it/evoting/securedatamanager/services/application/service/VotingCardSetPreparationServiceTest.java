/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static ch.post.it.evoting.securedatamanager.commons.Constants.CONFIG_FILE_NAME_CONFIGURATION_ANONYMIZED;
import static ch.post.it.evoting.securedatamanager.services.application.service.VotingCardSetPreparationService.CHALLENGE_PROFILE;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptolib.api.asymmetric.AsymmetricServiceAPI;
import ch.post.it.evoting.cryptolib.api.exceptions.GeneralCryptoLibException;
import ch.post.it.evoting.cryptolib.asymmetric.service.AsymmetricService;
import ch.post.it.evoting.cryptolib.certificates.utils.PemUtils;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.securedatamanager.services.application.sign.FileSignerService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

@ExtendWith(MockitoExtension.class)
@SpringJUnitConfig(VotingCardSetPreparationServiceTest.PrivateConfiguration.class)
@DisplayName("Use VotingCardSetPreparationService to run prepare step")
class VotingCardSetPreparationServiceTest {

	private final String electionEventId = "3a2434c5a1004d71ac53b55d3ccdbfb8";
	private final String votingCardSetId = "b7e28ca876364dfa9a9315d795f59172";
	private String privateKeyPEM;

	@Mock
	private PathResolver pathResolverMock;

	@Mock
	private VotingCardSetRepository votingCardSetRepositoryMock;

	@Autowired
	private FileSignerService fileSignerService;

	@Autowired
	private ObjectMapper objectMapper;

	private VotingCardSetPreparationService votingCardSetPreparationService;

	@BeforeEach
	void setUp() throws GeneralCryptoLibException {
		final AsymmetricService asymmetricService = new AsymmetricService();
		final KeyPair keyPairForSigning = asymmetricService.getKeyPairForSigning();
		final PrivateKey privateKey = keyPairForSigning.getPrivate();
		privateKeyPEM = PemUtils.privateKeyToPem(privateKey);

		votingCardSetPreparationService = new VotingCardSetPreparationService(pathResolverMock, fileSignerService,
				objectMapper, CHALLENGE_PROFILE, votingCardSetRepositoryMock);
	}

	@Test
	@DisplayName("with an invalid profiles")
	void prepareInvalidProfileTest() {
		votingCardSetPreparationService = new VotingCardSetPreparationService(pathResolverMock,
				fileSignerService, objectMapper, "invalidProfile1,invalidProfile2", votingCardSetRepositoryMock);

		votingCardSetPreparationService.prepare(electionEventId, votingCardSetId, privateKeyPEM);

		verify(pathResolverMock, Mockito.times(0)).resolvePrintingPath(electionEventId);
	}

	@Test
	@DisplayName("with an empty profile")
	void prepareEmptyProfileTest() {
		votingCardSetPreparationService = new VotingCardSetPreparationService(pathResolverMock, fileSignerService,
				objectMapper, "", votingCardSetRepositoryMock);

		votingCardSetPreparationService.prepare(electionEventId, votingCardSetId, privateKeyPEM);

		verify(pathResolverMock, Mockito.times(0)).resolvePrintingPath(electionEventId);
	}

	@Test
	@DisplayName("with null parameters")
	void prepareNullParametersTest() {
		assertAll(() -> assertThrows(NullPointerException.class, () -> votingCardSetPreparationService.prepare(null, votingCardSetId, privateKeyPEM)),
				() -> assertThrows(NullPointerException.class, () -> votingCardSetPreparationService.prepare(electionEventId, null, privateKeyPEM)),
				() -> assertThrows(NullPointerException.class, () -> votingCardSetPreparationService.prepare(electionEventId, votingCardSetId, null))
		);
	}

	@Test
	@DisplayName("with invalid parameters")
	void prepareInvalidParametersTest() {
		assertAll(() -> assertThrows(FailedValidationException.class,
						() -> votingCardSetPreparationService.prepare("a12", votingCardSetId, privateKeyPEM)),
				() -> assertThrows(FailedValidationException.class,
						() -> votingCardSetPreparationService.prepare(electionEventId, "b34", privateKeyPEM)));
	}

	@Test
	@DisplayName("with configuration with birthDate")
	void extractVcAliasesAndEaCodesWithBirthDateTest() throws URISyntaxException, IOException {
		// given
		final URL xml = this.getClass().getResource(
				"/ch/post/it/evoting/services/application/service/VotingCardSetPreparationServiceTest/configuration-anonymize-birthDate.xml");
		final Path pathMock = mock(Path.class);
		when(pathMock.resolve(CONFIG_FILE_NAME_CONFIGURATION_ANONYMIZED)).thenReturn(Path.of(xml.toURI()));
		when(pathResolverMock.resolveInputPath(anyString())).thenReturn(pathMock);

		// when
		List<List<String>> actual = votingCardSetPreparationService.extractVcAliasesAndEaCodes("a12", "vcs_BE-CH-1|BE-MU-10004");

		// then
		Assertions.assertThat(actual).containsExactly(
				List.of("10000001", "01061971"),
				List.of("10000002", "01061972")
		);
	}

	@Test
	@DisplayName("with configuration with birthDate")
	void extractVcAliasesAndEaCodesWithBirthYearTest() throws URISyntaxException, IOException {
		// given
		URL xml = this.getClass().getResource(
				"/ch/post/it/evoting/services/application/service/VotingCardSetPreparationServiceTest/configuration-anonymize-birthYear.xml");
		Path pathMock = mock(Path.class);
		when(pathMock.resolve(CONFIG_FILE_NAME_CONFIGURATION_ANONYMIZED)).thenReturn(Path.of(xml.toURI()));
		when(pathResolverMock.resolveInputPath(anyString())).thenReturn(pathMock);

		// when
		List<List<String>> actual = votingCardSetPreparationService.extractVcAliasesAndEaCodes("a12", "vcs_BE-CH-1|BE-MU-10004");

		// then
		Assertions.assertThat(actual).containsExactly(
				List.of("10000001", "1971"),
				List.of("10000002", "1972")
		);
	}

	@Configuration
	static class PrivateConfiguration {

		@Bean
		AsymmetricServiceAPI asymmetricServiceAPI() {
			return new AsymmetricService();
		}

		@Bean
		Hash hash() {
			return HashFactory.createHash();
		}

		@Bean
		FileSignerService metadataFileSigner(final AsymmetricServiceAPI asymmetricServiceAPI, final Hash hash) {
			return new FileSignerService(asymmetricServiceAPI, hash);
		}

		@Bean
		public ObjectMapper objectMapper() {
			return DomainObjectMapper.getNewInstance();
		}
	}
}
