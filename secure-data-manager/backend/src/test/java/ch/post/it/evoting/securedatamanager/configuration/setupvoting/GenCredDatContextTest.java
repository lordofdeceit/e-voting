/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.security.SecureRandom;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.GqGroupGenerator;
import ch.post.it.evoting.securedatamanager.commons.Constants;

class GenCredDatContextTest {

	private final static int bound = 12;
	private final static int start = 21;
	private final static int differentStart = 41;
	private final static int maxSize = 4;

	private final Random rand = RandomFactory.createRandom();
	private final SecureRandom srand = new SecureRandom();
	private final int delta = srand.nextInt(bound) + 1;
	private final int psi = srand.nextInt(bound) + 1;

	private final GqGroup gqGroup = GroupTestData.getGqGroup();
	private final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);

	private String electionEventId;
	private String verificationCardSetId;
	private ElGamalMultiRecipientPublicKey electionPublicKey;
	private ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey;
	private GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions;
	private List<String> actualVotingOptions;
	private List<String> ciSelections;
	private List<String> ciVotingOptions;

	@BeforeEach
	void setup() {
		electionEventId = rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase();
		verificationCardSetId = rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase();

		electionPublicKey = elGamalGenerator.genRandomPublicKey(delta);
		choiceReturnCodesEncryptionPublicKey = elGamalGenerator.genRandomPublicKey(psi);
		GqGroupGenerator generator = new GqGroupGenerator(electionPublicKey.getGroup());
		encodedVotingOptions = Stream.generate(generator::genSmallPrimeMember).limit(maxSize).collect(GroupVector.toGroupVector());
		actualVotingOptions = Stream.generate(() -> rand.genRandomBase16String(Constants.BASE16_ID_LENGTH)).limit(maxSize).toList();
		ciSelections = Stream.generate(() -> rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase()).limit(maxSize).toList();
		ciVotingOptions = Stream.generate(() -> rand.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase()).limit(maxSize).toList();
	}

	@Test
	@DisplayName("happyPath")
	void happyPath() {
		GenCredDatContext context = assertDoesNotThrow(() -> new GenCredDatContext.Builder()
				.setElectionEventId(electionEventId)
				.setVerificationCardSetId(verificationCardSetId)
				.setElectionPublicKey(electionPublicKey)
				.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
				.setEncodedVotingOptions(encodedVotingOptions)
				.setActualVotingOptions(actualVotingOptions)
				.setCorrectnessInformationSelections(ciSelections)
				.setCorrectnessInformationVotingOptions(ciVotingOptions)
				.build());

		assertEquals(context.electionEventId(), electionEventId, "Context electionEventId no equal");
		assertEquals(context.verificationCardSetId(), verificationCardSetId, "Context verificationCardSetId no equal");
		assertEquals(context.electionPublicKey().getGroup(), electionPublicKey.getGroup(),
				"Input electionPublicKey has different GqGroup");
		assertTrue(context.electionPublicKey().stream().allMatch(elt -> electionPublicKey.stream().anyMatch(elt2 -> elt2.equals(elt))),
				"Input electionPublicKey does not contains all elements");
		assertTrue(context.encodedVotingOptions().containsAll(encodedVotingOptions.stream().toList()),
				"Input encodedVotingOptions does not contains all elements");
		assertTrue(context.actualVotingOptions().stream().allMatch(elt -> actualVotingOptions.stream().anyMatch(elt2 -> elt2.equals(elt))),
				"Input actualVotingOptions does not contains all elements");
	}

	@Nested
	@DisplayName("A null arguments throws a NullPointerException")
	class NullArgumentThrowsNullPointerException {

		@Test
		@DisplayName("all arguments null")
		void constructWithNullArguments() {
			final GenCredDatContext.Builder builder = new GenCredDatContext.Builder();

			final NullPointerException ex = assertThrows(NullPointerException.class, builder::build);

			final String expectedMessage = null;

			assertEquals(expectedMessage, ex.getMessage());
		}

		@Test
		@DisplayName("electionEventId argument null")
		void constructWithElectionEventId() {
			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setVerificationCardSetId(verificationCardSetId)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
					.setEncodedVotingOptions(encodedVotingOptions)
					.setActualVotingOptions(actualVotingOptions);

			final NullPointerException ex = assertThrows(NullPointerException.class, builder::build);

			final String expectedMessage = null;

			assertEquals(expectedMessage, ex.getMessage());
		}

		@Test
		@DisplayName("verificationCardSetId argument null")
		void constructWithVerificationCardSetId() {
			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setElectionEventId(electionEventId)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
					.setEncodedVotingOptions(encodedVotingOptions)
					.setActualVotingOptions(actualVotingOptions);

			final NullPointerException ex = assertThrows(NullPointerException.class, builder::build);

			final String expectedMessage = null;

			assertEquals(expectedMessage, ex.getMessage());
		}

		@Test
		@DisplayName("electionPublicKey argument null")
		void constructWithElectionPublicKey() {
			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
					.setEncodedVotingOptions(encodedVotingOptions)
					.setActualVotingOptions(actualVotingOptions);

			final NullPointerException ex = assertThrows(NullPointerException.class, builder::build);

			final String expectedMessage = null;

			assertEquals(expectedMessage, ex.getMessage());
		}

		@Test
		@DisplayName("choiceReturnCodesEncryptionPublicKey argument null")
		void constructWithChoiceReturnCodesEncryptionPublicKey() {
			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setElectionPublicKey(electionPublicKey)
					.setEncodedVotingOptions(encodedVotingOptions)
					.setActualVotingOptions(actualVotingOptions);

			final NullPointerException ex = assertThrows(NullPointerException.class, builder::build);

			final String expectedMessage = null;

			assertEquals(expectedMessage, ex.getMessage());
		}

		@Test
		@DisplayName("encodedVotingOptions argument null")
		void constructWithEncodedVotingOptions() {
			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
					.setActualVotingOptions(actualVotingOptions);

			final NullPointerException ex = assertThrows(NullPointerException.class, builder::build);

			final String expectedMessage = null;

			assertEquals(expectedMessage, ex.getMessage());
		}

		@Test
		@DisplayName("actualVotingOptions argument null")
		void constructWithActualVotingOptions() {
			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
					.setEncodedVotingOptions(encodedVotingOptions);

			final NullPointerException ex = assertThrows(NullPointerException.class, builder::build);

			final String expectedMessage = null;

			assertEquals(expectedMessage, ex.getMessage());
		}
	}

	@Nested
	@DisplayName("Invalid arguments throws an Exception")
	class InvalidArgumentThrowsException {

		private ElGamalGenerator elGamalGeneratorGroup2;

		@BeforeEach
		void setUp() {
			GqGroup differentGroup = GroupTestData.getDifferentGqGroup(gqGroup);
			elGamalGeneratorGroup2 = new ElGamalGenerator(differentGroup);
		}

		@Test
		@DisplayName("electionEventId argument invalid")
		void invalidElectionEventIdTest() {
			final String invalidElectionEventId = electionEventId.toUpperCase();

			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setElectionEventId(invalidElectionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
					.setEncodedVotingOptions(encodedVotingOptions)
					.setActualVotingOptions(actualVotingOptions);

			FailedValidationException ex = assertThrows(FailedValidationException.class, builder::build);

			final String expectedMessage =
					String.format("The given string does not comply with the required format. [string: %s, format: ^[0123456789abcdef]{32}$].",
							invalidElectionEventId);

			assertEquals(expectedMessage, ex.getMessage());

		}

		@Test
		@DisplayName("verificationCardSetId argument invalid")
		void invalidVerificationCardSetIdTest() {
			final String invalidVerificationCardSetId = verificationCardSetId.toUpperCase();

			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(invalidVerificationCardSetId)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
					.setEncodedVotingOptions(encodedVotingOptions)
					.setActualVotingOptions(actualVotingOptions)
					.setCorrectnessInformationSelections(ciSelections)
					.setCorrectnessInformationVotingOptions(ciVotingOptions);

			final FailedValidationException ex = assertThrows(FailedValidationException.class, builder::build);

			final String expectedMessage =
					String.format("The given string does not comply with the required format. [string: %s, format: ^[0123456789abcdef]{32}$].",
							invalidVerificationCardSetId);

			assertEquals(expectedMessage, ex.getMessage());
		}

		@Test
		@DisplayName("electionPublicKey argument invalid")
		void invalidElectionPublicKeyTest() {
			final ElGamalMultiRecipientPublicKey electionPublicKey2 = elGamalGeneratorGroup2.genRandomPublicKey(srand.nextInt(bound) + start);

			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setElectionPublicKey(electionPublicKey2)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
					.setEncodedVotingOptions(encodedVotingOptions)
					.setActualVotingOptions(actualVotingOptions)
					.setCorrectnessInformationSelections(ciSelections)
					.setCorrectnessInformationVotingOptions(ciVotingOptions);

			final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "The choiceReturnCodesEncryptionPublicKey and electionPublicKey must have the same group.";

			assertEquals(expectedMessage, ex.getMessage());
		}

		@Test
		@DisplayName("choiceReturnCodesEncryptionPublicKey invalid")
		void invalidChoiceReturnCodesEncryptionPublicKeyTest() {
			final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey2 =
					elGamalGeneratorGroup2.genRandomPublicKey(srand.nextInt(bound) + differentStart);

			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey2)
					.setEncodedVotingOptions(encodedVotingOptions)
					.setActualVotingOptions(actualVotingOptions)
					.setCorrectnessInformationSelections(ciSelections)
					.setCorrectnessInformationVotingOptions(ciVotingOptions);

			final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "The choiceReturnCodesEncryptionPublicKey and electionPublicKey must have the same group.";

			assertEquals(expectedMessage, ex.getMessage());
		}

		@Test
		@DisplayName("encodedVotingOptions and electionPublicKey have the same GqGroup.")
		void encodedVotingOptionsGqGroupTest() {
			GqGroup gqGroup2 = GroupTestData.getGqGroup();

			while (gqGroup2.equals(gqGroup)) {
				gqGroup2 = GroupTestData.getGqGroup();
			}
			final GqGroupGenerator gqGroupGenerator2 = new GqGroupGenerator(gqGroup2);

			GroupVector<PrimeGqElement, GqGroup> encodedVotingOptionsDifferentGqGroup =
					Stream.generate(gqGroupGenerator2::genSmallPrimeMember).limit(4).collect(GroupVector.toGroupVector());

			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
					.setEncodedVotingOptions(encodedVotingOptionsDifferentGqGroup)
					.setActualVotingOptions(actualVotingOptions)
					.setCorrectnessInformationSelections(ciSelections)
					.setCorrectnessInformationVotingOptions(ciVotingOptions);

			final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "Vectors encodedVotingOptions elements and electionPublicKey must have the same group.";

			assertEquals(expectedMessage, ex.getMessage());
		}

		@Test
		@DisplayName("actualVotingOptions argument empty")
		void emptyActualVotingOptionsTest() {
			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
					.setEncodedVotingOptions(encodedVotingOptions)
					.setActualVotingOptions(List.of())
					.setCorrectnessInformationSelections(ciSelections)
					.setCorrectnessInformationVotingOptions(ciVotingOptions);

			final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "The actualVotingOptions must not be empty.";

			assertEquals(expectedMessage, ex.getMessage());
		}

		@Test
		@DisplayName("actualVotingOptions argument elements")
		void emptyElementActualVotingOptionsTest() {
			List<String> actualVotingOptions = List.of(
					rand.genRandomBase16String(Constants.BASE16_ID_LENGTH),
					rand.genRandomBase16String(Constants.BASE16_ID_LENGTH),
					"",
					rand.genRandomBase16String(Constants.BASE16_ID_LENGTH));

			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
					.setEncodedVotingOptions(encodedVotingOptions)
					.setActualVotingOptions(actualVotingOptions)
					.setCorrectnessInformationSelections(ciSelections)
					.setCorrectnessInformationVotingOptions(ciVotingOptions);

			final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "The elements of the actualVotingOptions must contain at least one character.";

			assertEquals(expectedMessage, ex.getMessage());
		}

		@Test
		@DisplayName("electionPublicKey and encodedVotingOptions must have same GqGroup")
		void invalidGroupEncodedVotingOptionsTest() {
			final GqGroup gqGroup2 = GroupTestData.getDifferentGqGroup(gqGroup);
			final GqGroupGenerator generator = new GqGroupGenerator(gqGroup2);
			final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions2 = Stream.generate(generator::genSmallPrimeMember).limit(maxSize)
					.collect(GroupVector.toGroupVector());

			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
					.setEncodedVotingOptions(encodedVotingOptions2)
					.setActualVotingOptions(actualVotingOptions)
					.setCorrectnessInformationSelections(ciSelections)
					.setCorrectnessInformationVotingOptions(ciVotingOptions);

			final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "Vectors encodedVotingOptions elements and electionPublicKey must have the same group.";

			assertEquals(expectedMessage, ex.getMessage());
		}

		@Test
		@DisplayName("encodedVotingOptions argument /= size")
		void invalidEncodedVotingOptionsTest() {
			final GqGroupGenerator groupGenerator = new GqGroupGenerator(gqGroup);
			final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions2 =
					Stream.generate(groupGenerator::genSmallPrimeMember)
							.limit(encodedVotingOptions.size() + 1)
							.collect(GroupVector.toGroupVector());

			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
					.setEncodedVotingOptions(encodedVotingOptions2)
					.setActualVotingOptions(actualVotingOptions)
					.setCorrectnessInformationSelections(ciSelections)
					.setCorrectnessInformationVotingOptions(ciVotingOptions);

			final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "Vectors encodedVotingOptions and actualVotingOptions must have the same size.";

			assertEquals(expectedMessage, ex.getMessage());
		}

		@Test
		@DisplayName("actualVotingOptions argument /= size")
		void invalidActualVotingOptionsTest() {
			final List<String> actualVotingOptions2 =
					List.copyOf(
							Stream.generate(() -> rand.genRandomBase16String(Constants.BASE16_ID_LENGTH)).limit(actualVotingOptions.size() + 1)
									.toList());

			final GenCredDatContext.Builder builder = new GenCredDatContext
					.Builder()
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
					.setEncodedVotingOptions(encodedVotingOptions)
					.setActualVotingOptions(actualVotingOptions2)
					.setCorrectnessInformationSelections(ciSelections)
					.setCorrectnessInformationVotingOptions(ciVotingOptions);

			final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "Vectors encodedVotingOptions and actualVotingOptions must have the same size.";

			assertEquals(expectedMessage, ex.getMessage());
		}
	}
}
