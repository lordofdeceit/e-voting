/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.securedatamanager.commons.Constants.CONFIG_DIR_NAME_ONLINE;
import static ch.post.it.evoting.securedatamanager.commons.Constants.CONFIG_DIR_NAME_VOTEVERIFICATION;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;
import java.util.Collections;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.domain.configuration.setupvoting.SetupComponentLVCCAllowListPayload;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;
import ch.post.it.evoting.securedatamanager.services.infrastructure.clients.MessageBrokerOrchestratorClient;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

@DisplayName("A LongVoteCastReturnCodesAllowListUploadService")
@ExtendWith(MockitoExtension.class)
class LongVoteCastReturnCodesAllowListUploadServiceTest {

	private static final Random random = RandomFactory.createRandom();
	private static final String ELECTION_EVENT_ID = random.genRandomBase16String(32).toLowerCase();
	private static final String VERIFICATION_CARD_SET_ID = random.genRandomBase16String(32).toLowerCase();
	private static final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
	private static final Hash hash = HashFactory.createHash();
	private static SetupComponentLVCCAllowListPayloadService setupComponentLVCCAllowListPayloadService;

	@BeforeAll
	static void setUpAll(
			@TempDir
			final Path tempDir) throws IOException {

		Files.createDirectories(
				tempDir.resolve("sdm/config").resolve(ELECTION_EVENT_ID).resolve(CONFIG_DIR_NAME_ONLINE).resolve(CONFIG_DIR_NAME_VOTEVERIFICATION)
						.resolve(VERIFICATION_CARD_SET_ID));
		final PathResolver pathResolver = new PathResolver(tempDir.toString());

		final SetupComponentLVCCAllowListPayloadFileRepository setupComponentLVCCAllowListPayloadFileRepository = new SetupComponentLVCCAllowListPayloadFileRepository(
				objectMapper, pathResolver);

		setupComponentLVCCAllowListPayloadService =
				new SetupComponentLVCCAllowListPayloadService(setupComponentLVCCAllowListPayloadFileRepository);

		setupComponentLVCCAllowListPayloadService.save(validLongVoteCastReturnCodesAllowListPayload());
	}

	@DisplayName("uploads successfully and does not throw.")
	@Test
	void uploadHappyPath() throws IOException {

		@SuppressWarnings("unchecked")
		final Call<Void> responseBody = mock(Call.class);
		when(responseBody.execute()).thenReturn(Response.success(null));

		final MessageBrokerOrchestratorClient messageBrokerOrchestratorClientMock = mock(MessageBrokerOrchestratorClient.class);
		when(messageBrokerOrchestratorClientMock.uploadLongVoteCastReturnCodesAllowList(any(), any(), any())).thenReturn(responseBody);

		final LongVoteCastReturnCodesAllowListUploadService longVoteCastReturnCodesAllowListUploadService = new LongVoteCastReturnCodesAllowListUploadService(
				setupComponentLVCCAllowListPayloadService, messageBrokerOrchestratorClientMock, true);

		assertDoesNotThrow(() -> longVoteCastReturnCodesAllowListUploadService.upload(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
	}

	@DisplayName("uploading throws upon unsuccessful response.")
	@Test
	void uploadUnsuccessfulThrows() throws IOException {

		@SuppressWarnings("unchecked")
		final Call<Void> responseBody = mock(Call.class);
		when(responseBody.execute()).thenReturn(Response.error(500, ResponseBody.create(null, new byte[0])));

		final MessageBrokerOrchestratorClient messageBrokerOrchestratorClientMock = mock(MessageBrokerOrchestratorClient.class);
		when(messageBrokerOrchestratorClientMock.uploadLongVoteCastReturnCodesAllowList(any(), any(), any())).thenReturn(responseBody);

		final LongVoteCastReturnCodesAllowListUploadService longVoteCastReturnCodesAllowListUploadService = new LongVoteCastReturnCodesAllowListUploadService(
				setupComponentLVCCAllowListPayloadService, messageBrokerOrchestratorClientMock, true);

		final IllegalStateException illegalStateException = assertThrows(IllegalStateException.class,
				() -> longVoteCastReturnCodesAllowListUploadService.upload(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));

		assertEquals(
				String.format("Request for uploading long vote cast return codes allow list failed. [electionEventId: %s, verificationCardSetId: %s]",
						ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID), illegalStateException.getMessage());
	}

	@DisplayName("provided with different inputs behaves as expected.")
	@Test
	void differentInputsThrowing() {

		final LongVoteCastReturnCodesAllowListUploadService longVoteCastReturnCodesAllowListUploadService = new LongVoteCastReturnCodesAllowListUploadService(
				mock(SetupComponentLVCCAllowListPayloadService.class), mock(MessageBrokerOrchestratorClient.class), true);

		assertAll(
				() -> assertThrows(NullPointerException.class,
						() -> longVoteCastReturnCodesAllowListUploadService.upload(null, VERIFICATION_CARD_SET_ID)),
				() -> assertDoesNotThrow(
						() -> longVoteCastReturnCodesAllowListUploadService.upload("", VERIFICATION_CARD_SET_ID)),
				() -> assertDoesNotThrow(
						() -> longVoteCastReturnCodesAllowListUploadService.upload(" ", VERIFICATION_CARD_SET_ID)),
				() -> assertThrows(FailedValidationException.class,
						() -> longVoteCastReturnCodesAllowListUploadService.upload("invalidElectionEventId", VERIFICATION_CARD_SET_ID)),
				() -> assertThrows(NullPointerException.class,
						() -> longVoteCastReturnCodesAllowListUploadService.upload(ELECTION_EVENT_ID, null)),
				() -> assertThrows(FailedValidationException.class,
						() -> longVoteCastReturnCodesAllowListUploadService.upload(ELECTION_EVENT_ID, "invalidVerificationCardSetId")));
	}

	private static SetupComponentLVCCAllowListPayload validLongVoteCastReturnCodesAllowListPayload() {
		final SetupComponentLVCCAllowListPayload setupComponentLVCCAllowListPayload =
				new SetupComponentLVCCAllowListPayload(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID,
						Collections.singletonList(Base64.getEncoder().encodeToString(new byte[] { 1 })));

		final byte[] payloadHash = hash.recursiveHash(setupComponentLVCCAllowListPayload);

		setupComponentLVCCAllowListPayload.setSignature(new CryptoPrimitivesSignature(payloadHash));

		return setupComponentLVCCAllowListPayload;
	}
}
