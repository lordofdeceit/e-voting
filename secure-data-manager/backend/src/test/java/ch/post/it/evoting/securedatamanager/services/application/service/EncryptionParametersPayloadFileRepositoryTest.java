/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.EncryptionParametersPayload;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.securedatamanager.EncryptionParametersPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

import uk.org.webcompere.systemstubs.environment.EnvironmentVariables;
import uk.org.webcompere.systemstubs.jupiter.SystemStub;
import uk.org.webcompere.systemstubs.jupiter.SystemStubsExtension;

@ExtendWith({ SystemStubsExtension.class })
class EncryptionParametersPayloadFileRepositoryTest extends TestGroupSetup {

	private static final Random random = RandomFactory.createRandom();
	private static final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
	private static final String electionEventId = random.genRandomBase16String(32).toLowerCase();

	private static final String TEST_JSON = """
			{
			  "encryptionGroup":{"p":"0x3B","q":"0x1D","g":"0x3"},
			  "seed":"13",
			  "smallPrimes":[5,7,17,19,41,53],
			  "signature":{"signatureContents":"c2lnbmF0dXJl"}
			}
			""";
	private static final String BAD_JSON = """
			{
			  "encryptionGroup":null,
			  "seed":"13",
			  "smallPrimes":[5,7,17,19,41,53],
			  "signature":{"signatureContents":"c2lnbmF0dXJl"}
			}
			""";

	@SystemStub
	private static EnvironmentVariables environmentVariables;

	private static PathResolver pathResolver;

	private static EncryptionParametersPayloadFileRepository encryptionParametersPayloadFileRepository;

	private static EncryptionParametersPayload encryptionParametersPayload;

	private static Path payloadPath;

	@BeforeAll
	static void setUpAll(
			@TempDir
			final Path tempDir) throws IOException {

		environmentVariables.set("SECURITY_LEVEL", "TESTING_ONLY");

		pathResolver = new PathResolver(tempDir.toString());

		final Path basePath = tempDir.resolve(Constants.CONFIG_FILES_BASE_DIR)
				.resolve(electionEventId)
				.resolve(Constants.CONFIG_DIR_NAME_CUSTOMER)
				.resolve(Constants.CONFIG_DIR_NAME_INPUT);

		payloadPath = basePath.resolve(Constants.CONFIG_FILE_NAME_ENCRYPTION_PARAMETERS_PAYLOAD_JSON);

		Files.createDirectories(basePath);

		encryptionParametersPayload = objectMapper.readValue(TEST_JSON, EncryptionParametersPayload.class);

		encryptionParametersPayloadFileRepository = new EncryptionParametersPayloadFileRepository(pathResolver, objectMapper);
	}

	@Test
	void saveEncryptionParametersPayloadHappyPath() {
		final Path savedPath = encryptionParametersPayloadFileRepository.save(electionEventId, encryptionParametersPayload);
		assertTrue(Files.exists(savedPath));
	}

	@Test
	void retrieveEncryptionParametersPayloadHappyPath() {
		encryptionParametersPayloadFileRepository.save(electionEventId, encryptionParametersPayload);
		final Optional<EncryptionParametersPayload> payload = encryptionParametersPayloadFileRepository.findById(electionEventId);

		assertTrue(payload.isPresent());
		assertEquals(encryptionParametersPayload, payload.get());

	}
}