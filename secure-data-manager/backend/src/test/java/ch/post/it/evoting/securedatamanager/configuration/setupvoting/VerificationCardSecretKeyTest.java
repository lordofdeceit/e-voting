/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.security.SecureRandom;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.domain.SecurityLevelTestingOnly;
import ch.post.it.evoting.domain.SerializationUtils;
import ch.post.it.evoting.securedatamanager.commons.Constants;

@SecurityLevelTestingOnly
@DisplayName("A VerificationCardSecretKey")
class VerificationCardSecretKeyTest {

	private static final int LENGTH = Constants.BASE16_ID_LENGTH;
	private static final int BOUND = 12;

	private static final ObjectMapper mapper = DomainObjectMapper.getNewInstance();
	private static final Random random = RandomFactory.createRandom();
	private static final SecureRandom srand = new SecureRandom();
	private static final int SIZE = srand.nextInt(BOUND) + 1;
	private static final String ELECTION_EVENT_ID = random.genRandomBase16String(LENGTH).toLowerCase();
	private static final String VERIFICATION_CARD_SET_ID = random.genRandomBase16String(LENGTH).toLowerCase();
	private static final String VERIFICATION_CARD_ID = random.genRandomBase16String(LENGTH).toLowerCase();

	private static VerificationCardSecretKey verificationCardSecretKey;
	private static ObjectNode rootNode;

	@BeforeAll
	static void setupAll() throws JsonProcessingException {
		verificationCardSecretKey = new VerificationCardSecretKey(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID,
				SerializationUtils.getPrivateKey(), SerializationUtils.getGqGroup());

		// Create expected Json.
		rootNode = mapper.createObjectNode();
		rootNode.set("electionEventId", mapper.readTree(mapper.writeValueAsString(ELECTION_EVENT_ID)));
		rootNode.set("verificationCardSetId", mapper.readTree(mapper.writeValueAsString(VERIFICATION_CARD_SET_ID)));
		rootNode.set("verificationCardId", mapper.readTree(mapper.writeValueAsString(VERIFICATION_CARD_ID)));
		rootNode.set("privateKey", mapper.readTree(mapper.writeValueAsString(SerializationUtils.getPrivateKey())));
		rootNode.set("gqGroup", mapper.readTree(mapper.writeValueAsString(SerializationUtils.getGqGroup())));
	}

	@Test
	@DisplayName("serialized gives expected json")
	void serializePayload() throws JsonProcessingException {
		final String serializedPayload = mapper.writeValueAsString(verificationCardSecretKey);
		assertEquals(rootNode.toString(), serializedPayload);
	}

	@Test
	@DisplayName("deserialized gives expected payload")
	void deserializePayload() throws IOException {
		final VerificationCardSecretKey deserializedPayload = mapper.readValue(rootNode.toString(), VerificationCardSecretKey.class);
		assertEquals(verificationCardSecretKey, deserializedPayload);
	}

	@Test
	@DisplayName("serialized then deserialized gives original payload")
	void cycle() throws IOException {
		final VerificationCardSecretKey deserializedPayload = mapper
				.readValue(mapper.writeValueAsString(verificationCardSecretKey), VerificationCardSecretKey.class);

		assertEquals(verificationCardSecretKey, deserializedPayload);
	}

	@Nested
	@DisplayName("constructed with")
	class CheckConstructor {

		private final GqGroup gqGroup = GroupTestData.getGqGroup();
		private final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
		private final ElGamalMultiRecipientPrivateKey privateKey = elGamalGenerator.genRandomPrivateKey(SIZE);

		@Test
		@DisplayName("any null input throws a NullPointerException.")
		void checksNulls() {

			assertAll(
					() -> assertThrows(NullPointerException.class, () ->
							new VerificationCardSecretKey(null, VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID, privateKey, gqGroup)),
					() -> assertThrows(NullPointerException.class, () ->
							new VerificationCardSecretKey(ELECTION_EVENT_ID, null, VERIFICATION_CARD_ID, privateKey, gqGroup)),
					() -> assertThrows(NullPointerException.class, () ->
							new VerificationCardSecretKey(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, null, privateKey, gqGroup)),
					() -> assertThrows(NullPointerException.class, () ->
							new VerificationCardSecretKey(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID, null, gqGroup)),
					() -> assertThrows(NullPointerException.class, () ->
							new VerificationCardSecretKey(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID, privateKey, null))
			);
		}

		@Test
		@DisplayName("any invalid input throws a FailedValidationException.")
		void checksInvalidValues() {

			final String invalidElectionEventId = "invalidElectionEventId";
			final String invalidVerificationCardSetId = "invalidVerificationCardSetId";
			final String invalidVerificationCardId = "invalidVerificationCardId";

			assertAll(
					() -> assertThrows(FailedValidationException.class,
							() -> new VerificationCardSecretKey(invalidElectionEventId, VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID, privateKey,
									gqGroup)),
					() -> assertThrows(FailedValidationException.class, () ->
							new VerificationCardSecretKey(ELECTION_EVENT_ID, invalidVerificationCardSetId, VERIFICATION_CARD_ID, privateKey,
									gqGroup)),
					() -> assertThrows(FailedValidationException.class, () ->
							new VerificationCardSecretKey(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, invalidVerificationCardId, privateKey,
									gqGroup))
			);
		}

		@Test
		@DisplayName("inputs with different groups throws an IllegalArgumentException.")
		void checksDifferentGroup() {
			final GqGroup gqGroupDifferent = GroupTestData.getDifferentGqGroup(gqGroup);
			final ElGamalGenerator elGamalGeneratorDifferentGroup = new ElGamalGenerator(gqGroupDifferent);
			final ElGamalMultiRecipientPrivateKey privateKeyDifferentGroup = elGamalGeneratorDifferentGroup.genRandomPrivateKey(SIZE);

			assertAll(
					() -> {
						final IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () ->
								new VerificationCardSecretKey(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID,
										privateKeyDifferentGroup, gqGroup));
						assertEquals("The private key group must be of same order as the gqGroup.", illegalArgumentException.getMessage());
					},

					() -> {
						final IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () ->
								new VerificationCardSecretKey(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID,
										privateKey, gqGroupDifferent));
						assertEquals("The private key group must be of same order as the gqGroup.", illegalArgumentException.getMessage());
					}
			);
		}
	}
}
