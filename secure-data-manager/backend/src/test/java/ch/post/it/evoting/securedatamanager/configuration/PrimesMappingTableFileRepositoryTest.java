/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.io.TempDir;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.domain.SecurityLevelTestingOnly;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@SecurityLevelTestingOnly
@DisplayName("A PrimesMappingTableFileRepository")
class PrimesMappingTableFileRepositoryTest {

	private static final Random random = RandomFactory.createRandom();
	@TempDir
	private static Path tempDir;
	private static PrimesMappingTableFileRepository primesMappingTableFileRepository;

	@BeforeAll
	static void setUpAll() throws IOException {
		final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
		final Path configPath = tempDir.resolve("sdm/config");
		final PathResolver pathResolver = new PathResolver(tempDir.toString());
		Files.createDirectories(configPath);

		primesMappingTableFileRepository = new PrimesMappingTableFileRepository(objectMapper, pathResolver);
	}

	@Nested
	@DisplayName("saving")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class SaveTest {

		private PrimesMappingTableFileEntity primesMappingTableFileEntity;

		@BeforeEach
		void setUp() {
			final GqGroup encryptionGroup = GroupTestData.getGqGroup();
			final String electionEventId = random.genRandomBase16String(32).toLowerCase();
			final String verificationCardSetId = random.genRandomBase16String(32).toLowerCase();

			final GroupVector<PrimeGqElement, GqGroup> smallPrimeGroupMembers = PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers(
					encryptionGroup, 1);
			final PrimesMappingTable primesMappingTable = PrimesMappingTable.from(
					List.of(new PrimesMappingTableEntry("actualVotingOption", smallPrimeGroupMembers.get(0))));

			primesMappingTableFileEntity = new PrimesMappingTableFileEntity(encryptionGroup, electionEventId, verificationCardSetId,
					primesMappingTable);
		}

		@Test
		@DisplayName("valid entity creates file")
		void save() {
			final Path savePath = primesMappingTableFileRepository.save(primesMappingTableFileEntity);

			assertTrue(Files.exists(savePath));
		}

		@Test
		@DisplayName("null entity throws NullPointerException")
		void saveNullEntity() {
			assertThrows(NullPointerException.class, () -> primesMappingTableFileRepository.save(null));
		}

	}

	@Nested
	@DisplayName("finding")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class FindByVerificationCardSetIdTest {

		private String electionEventId;
		private String verificationCardSetId;

		@BeforeAll
		void setUpAll() {
			final GqGroup encryptionGroup = GroupTestData.getGqGroup();
			electionEventId = random.genRandomBase16String(32).toLowerCase();
			verificationCardSetId = random.genRandomBase16String(32).toLowerCase();

			final GroupVector<PrimeGqElement, GqGroup> smallPrimeGroupMembers = PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers(
					encryptionGroup, 1);
			final PrimesMappingTable primesMappingTable = PrimesMappingTable.from(
					List.of(new PrimesMappingTableEntry("actualVotingOption", smallPrimeGroupMembers.get(0))));

			final PrimesMappingTableFileEntity primesMappingTableFileEntity = new PrimesMappingTableFileEntity(encryptionGroup, electionEventId,
					verificationCardSetId, primesMappingTable);

			primesMappingTableFileRepository.save(primesMappingTableFileEntity);
		}

		@Test
		@DisplayName("existing entity returns it")
		void findByVerificationCardSetId() {
			assertTrue(primesMappingTableFileRepository.findByVerificationCardSetId(electionEventId, verificationCardSetId).isPresent());
		}

		@Test
		@DisplayName("invalid ids throws FailedValidationException")
		void invalidIds() {
			assertThrows(FailedValidationException.class,
					() -> primesMappingTableFileRepository.findByVerificationCardSetId(electionEventId, "1234"));
			assertThrows(FailedValidationException.class,
					() -> primesMappingTableFileRepository.findByVerificationCardSetId("1234", verificationCardSetId));
		}

		@Test
		@DisplayName("non existing entity returns empty")
		void nonExistingEntity() {
			final String otherElectionEventId = random.genRandomBase16String(32).toLowerCase();

			assertTrue(primesMappingTableFileRepository.findByVerificationCardSetId(otherElectionEventId, verificationCardSetId).isEmpty());
		}

	}

}