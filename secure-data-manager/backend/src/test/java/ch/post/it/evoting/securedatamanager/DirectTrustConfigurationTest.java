/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystoreFactory;

/**
 * Validate that the SDM context can be booted with a combination of direct trust key stores.
 */
class DirectTrustConfigurationTest {

	@TempDir
	static Path tempKeystorePath;

	static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

		@Override
		public void initialize(final ConfigurableApplicationContext applicationContext) {
			createSdmKeystore("config", applicationContext, Alias.SDM_CONFIG);
			createSdmKeystore("tally", applicationContext, Alias.SDM_TALLY);
		}

		private void createSdmKeystore(final String directoryName, final ConfigurableApplicationContext applicationContext, final Alias alias) {
			try {
				Files.createDirectories(tempKeystorePath.resolve(directoryName));
			} catch (IOException e) {
				throw new RuntimeException(e);
			}

			final String keystoreLocation = tempKeystorePath.resolve(directoryName).resolve("signing_keystore_test.p12").toString();
			final String keystorePasswordLocation = tempKeystorePath.resolve(directoryName).resolve("signing_pw_test.txt").toString();

			final Map<String, String> properties = new HashMap<>();
			properties.put("direct.trust.keystore.location." + directoryName, keystoreLocation);
			properties.put("direct.trust.keystore.password.location." + directoryName, keystorePasswordLocation);
			TestPropertyValues.of(properties).applyTo(applicationContext);

			KeystoreFilesCreator.create(keystoreLocation, keystorePasswordLocation, alias.get());
		}
	}

	@Configuration
	static class InternalTestConfiguration {

		@Value("${direct.trust.keystore.location.config}")
		private String keystoreLocationConfig;
		@Value("${direct.trust.keystore.password.location.config}")
		private String keystorePasswordLocationConfig;
		@Value("${direct.trust.keystore.location.tally}")
		private String keystoreLocationTally;
		@Value("${direct.trust.keystore.password.location.tally}")
		private String keystorePasswordLocationTally;

		@Bean
		public KeystoreRepository keystoreRepository() {
			return new KeystoreRepository(keystoreLocationConfig, keystorePasswordLocationConfig, keystoreLocationTally,
					keystorePasswordLocationTally);
		}

		@Bean
		Hash getHash() {
			return HashFactory.createHash();
		}

		@Bean(name = "sdmConfig")
		@ConditionalOnProperty("role.isConfig")
		SignatureKeystore<Alias> keystoreServiceSdmConfig(final KeystoreRepository repository) throws
				IOException {
			final Alias sdmConfig = Alias.SDM_CONFIG;
			return SignatureKeystoreFactory.createSignatureKeystore(repository.getConfigKeyStore(), "PKCS12", repository.getConfigKeystorePassword(),
					keystore -> KeyStoreValidator.validateKeyStore(keystore, sdmConfig), sdmConfig);
		}

		@Bean(name = "sdmTally")
		@ConditionalOnProperty("role.isTally")
		SignatureKeystore<Alias> keystoreServiceSdmTally(final KeystoreRepository repository)
				throws IOException {
			final Alias sdmTally = Alias.SDM_TALLY;
			return SignatureKeystoreFactory.createSignatureKeystore(repository.getTallyKeyStore(), "PKCS12", repository.getTallyKeystorePassword(),
					keystore -> KeyStoreValidator.validateKeyStore(keystore, sdmTally), sdmTally);
		}
	}

	private final ApplicationContextRunner contextRunner = new ApplicationContextRunner()
			.withInitializer(new Initializer())
			.withUserConfiguration(InternalTestConfiguration.class);

	@Test
	void sdmConfigOnly() {
		contextRunner
				.withPropertyValues("role.isConfig=true")
				.run(context -> assertThat(context)
						.hasBean("sdmConfig")
						.doesNotHaveBean("sdmTally")
						.hasSingleBean(SignatureKeystore.class));
	}

	@Test
	void sdmTally() {
		contextRunner
				.withPropertyValues("role.isTally=true")
				.run(context -> assertThat(context)
						.hasBean("sdmTally")
						.doesNotHaveBean("sdmConfig")
						.hasSingleBean(SignatureKeystore.class));
	}

	@Test
	void sdmTallyAndConfig() {
		contextRunner
				.withPropertyValues("role.isConfig=true")
				.withPropertyValues("role.isTally=true")
				.run(context -> assertThat(context)
						.hasBean("sdmConfig")
						.hasBean("sdmTally"));
	}

	@Test
	void sdmNoTallyAndNoConfig() {
		contextRunner
				.run(context -> assertThat(context)
						.doesNotHaveBean("sdmConfig")
						.doesNotHaveBean("sdmTally")
						.doesNotHaveBean(SignatureKeystore.class));
	}
}
