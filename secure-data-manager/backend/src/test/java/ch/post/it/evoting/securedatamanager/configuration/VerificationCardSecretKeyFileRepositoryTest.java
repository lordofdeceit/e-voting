/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration;

import static ch.post.it.evoting.securedatamanager.commons.Constants.CONFIG_FILE_NAME_VERIFICATION_CARD_SECRET_KEY;
import static ch.post.it.evoting.securedatamanager.commons.Constants.CONFIG_VERIFICATION_CARD_SECRET_KEYS_DIRECTORY;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.io.TempDir;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.domain.SecurityLevelTestingOnly;
import ch.post.it.evoting.domain.SerializationUtils;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.VerificationCardSecretKey;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@SecurityLevelTestingOnly
@DisplayName("A VerificationCardSecretKeyFileRepository")
class VerificationCardSecretKeyFileRepositoryTest {
	private static final Random random = RandomFactory.createRandom();

	private static final String NON_EXISTING_ELECTION_EVENT_ID = random.genRandomBase16String(32).toLowerCase();
	private static final String EXISTING_ELECTION_EVENT_ID = random.genRandomBase16String(32).toLowerCase();
	private static final String VERIFICATION_CARD_SET_ID = random.genRandomBase16String(32).toLowerCase();
	private static final String VERIFICATION_CARD_ID = random.genRandomBase16String(32).toLowerCase();

	private static ObjectMapper objectMapper;
	private static VerificationCardSecretKeyFileRepository verificationCardSecretKeyFileRepository;

	@BeforeAll
	static void setUpAll(
			@TempDir
			final Path tempDir) throws IOException {

		objectMapper = DomainObjectMapper.getNewInstance();

		createDirectories(tempDir, EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID);

		final PathResolver pathResolver = new PathResolver(tempDir.toString());

		verificationCardSecretKeyFileRepository = new VerificationCardSecretKeyFileRepository(objectMapper, pathResolver);

		final VerificationCardSecretKeyFileRepository repository = new VerificationCardSecretKeyFileRepository(objectMapper,
				pathResolver);

		repository.save(validVerificationCardSecretKeyPayload(EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID));
	}

	private static VerificationCardSecretKey validVerificationCardSecretKeyPayload(final String electionEventId,
			final String verificationCardSetId, final String verificationCardId) {
		return new VerificationCardSecretKey(electionEventId, verificationCardSetId, verificationCardId, SerializationUtils.getPrivateKey(),
				SerializationUtils.getGqGroup());
	}

	private static void createDirectories(final Path tempDir, final String electionEventId, final String verificationCardSetId) throws IOException {
		Files.createDirectories(
				tempDir.resolve("sdm/config").resolve(electionEventId).resolve("OFFLINE")
						.resolve(CONFIG_VERIFICATION_CARD_SECRET_KEYS_DIRECTORY).resolve(verificationCardSetId));
	}

	@Nested
	@DisplayName("saving")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class SaveTest {

		private VerificationCardSecretKeyFileRepository verificationCardSecretKeyFileRepositoryTemp;
		private VerificationCardSecretKey verificationCardSecretKey;

		@BeforeAll
		void setUpAll(
				@TempDir
				final Path tempDir) throws IOException {

			createDirectories(tempDir, EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID);

			final PathResolver pathResolver = new PathResolver(tempDir.toString());
			verificationCardSecretKeyFileRepositoryTemp = new VerificationCardSecretKeyFileRepository(objectMapper, pathResolver);
		}

		@BeforeEach
		void setUp() {
			verificationCardSecretKey = validVerificationCardSecretKeyPayload(EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID,
					VERIFICATION_CARD_ID);
		}

		@Test
		@DisplayName("valid verification card secret key creates file")
		void save() {
			final Path savedPath = verificationCardSecretKeyFileRepositoryTemp.save(verificationCardSecretKey);

			assertTrue(Files.exists(savedPath));
		}

		@Test
		@DisplayName("null verification card secret key throws NullPointerException")
		void saveNullVerificationCardSecretKeyPayload() {
			assertThrows(NullPointerException.class, () -> verificationCardSecretKeyFileRepositoryTemp.save(null));
		}

		@Test
		@DisplayName("invalid path throws UncheckedIOException")
		void invalidPath() {
			final PathResolver pathResolver = new PathResolver("invalidPath");
			final VerificationCardSecretKeyFileRepository repository =
					new VerificationCardSecretKeyFileRepository(DomainObjectMapper.getNewInstance(), pathResolver);

			final UncheckedIOException exception = assertThrows(UncheckedIOException.class, () -> repository.save(verificationCardSecretKey));

			final Path path = pathResolver.resolveElectionEventPath(EXISTING_ELECTION_EVENT_ID).resolve("OFFLINE")
					.resolve(CONFIG_VERIFICATION_CARD_SECRET_KEYS_DIRECTORY).resolve(VERIFICATION_CARD_SET_ID)
					.resolve(String.format(CONFIG_FILE_NAME_VERIFICATION_CARD_SECRET_KEY, VERIFICATION_CARD_ID));
			final String errorMessage = String.format(
					"Failed to serialize verification card secret key. [electionEventId: %s, verificationCardSetId: %s, verificationCardId: %s, path: %s]",
					EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID, path);

			assertEquals(errorMessage, exception.getMessage());
		}
	}

	@Nested
	@DisplayName("calling existsById")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class ExistsByIdTest {

		@Test
		@DisplayName("for existing verification card secret key returns true")
		void existingVerificationCardSecretKeyPayload() {
			assertTrue(verificationCardSecretKeyFileRepository.existsById(EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID,
					VERIFICATION_CARD_ID));
		}

		@Test
		@DisplayName("with null input throws NullPointerException")
		void nullInput() {
			assertThrows(NullPointerException.class,
					() -> verificationCardSecretKeyFileRepository.existsById(null, VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID));
			assertThrows(NullPointerException.class,
					() -> verificationCardSecretKeyFileRepository.existsById(EXISTING_ELECTION_EVENT_ID, null, VERIFICATION_CARD_ID));
			assertThrows(NullPointerException.class,
					() -> verificationCardSecretKeyFileRepository.existsById(EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, null));
		}

		@Test
		@DisplayName("with invalid input throws FailedValidationException")
		void invalidInput() {
			assertThrows(FailedValidationException.class,
					() -> verificationCardSecretKeyFileRepository.existsById("invalidId", VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID));
			assertThrows(FailedValidationException.class,
					() -> verificationCardSecretKeyFileRepository.existsById(EXISTING_ELECTION_EVENT_ID, "invalidId", VERIFICATION_CARD_ID));
			assertThrows(FailedValidationException.class,
					() -> verificationCardSecretKeyFileRepository.existsById(EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID,
							"invalidId"));

		}

		@Test
		@DisplayName("for non existing verification card secret key returns false")
		void nonExistingVerificationCardSecretKeyPayload() {
			assertFalse(verificationCardSecretKeyFileRepository.existsById(NON_EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID,
					VERIFICATION_CARD_ID));
		}

	}

	@Nested
	@DisplayName("calling findById")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class FindByIdTest {

		@Test
		@DisplayName("for existing verification card secret key returns it")
		void existingVerificationCardSecretKeyPayload() {
			assertTrue(verificationCardSecretKeyFileRepository.findById(EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID,
					VERIFICATION_CARD_ID).isPresent());
		}

		@Test
		@DisplayName("for non existing verification card secret key return empty optional")
		void nonExistingVerificationCardSecretKeyPayload() {
			assertFalse(
					verificationCardSecretKeyFileRepository.findById(NON_EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID,
							VERIFICATION_CARD_ID).isPresent());
		}

	}
}
