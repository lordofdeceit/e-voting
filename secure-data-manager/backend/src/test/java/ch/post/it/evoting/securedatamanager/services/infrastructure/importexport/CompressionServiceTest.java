/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.importexport;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

class CompressionServiceTest {

	private CompressionService compressionService;

	@TempDir
	private Path testDirectory;

	@BeforeEach
	void setUp() {
		compressionService = new CompressionService();
	}

	@Test
	void zipAndUnzip() throws IOException {

		// given
		final char[] password = "test".toCharArray();
		final Path inputDirectory = testDirectory.resolve("input");
		final Path outputDirectory = testDirectory.resolve("output");

		Files.createDirectories(inputDirectory);
		Files.createDirectories(outputDirectory);

		createTestFile(inputDirectory.resolve("1.txt"));
		createTestFile(inputDirectory.resolve("2.txt"));
		createTestFile(inputDirectory.resolve("3/31.txt"));
		createTestFile(inputDirectory.resolve("3/32.txt"));
		createTestFile(inputDirectory.resolve("33/331.txt"));

		// when
		final byte[] zip = compressionService.zipDirectory(inputDirectory, password);
		compressionService.unzipToDirectory(zip, password, outputDirectory);

		// then
		assertDirectoryContent(outputDirectory, inputDirectory);
		assertDirectoryContent(inputDirectory, outputDirectory);
	}

	private static void assertDirectoryContent(final Path expectedStructure, final Path actualStructure) throws IOException {
		try (Stream<Path> files = Files.walk(expectedStructure)) {
			assertTrue(files.filter(Files::isRegularFile).allMatch(p -> Files.exists(actualStructure.resolve(expectedStructure.relativize(p)))));
		}
	}

	private void createTestFile(final Path file) throws IOException {
		Files.createDirectories(file.getParent());
		Files.createFile(file);
	}
}