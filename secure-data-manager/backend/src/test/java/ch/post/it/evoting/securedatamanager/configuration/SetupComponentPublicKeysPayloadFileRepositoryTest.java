/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.securedatamanager.configuration.SetupComponentPublicKeysPayloadFileRepository.PAYLOAD_FILE_NAME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.io.TempDir;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Streams;

import ch.post.it.evoting.cryptoprimitives.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamal;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.SchnorrProof;
import ch.post.it.evoting.domain.SecurityLevelTestingOnly;
import ch.post.it.evoting.domain.SerializationUtils;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@SecurityLevelTestingOnly
@DisplayName("SetupComponentPublicKeysPayloadFileRepository")
class SetupComponentPublicKeysPayloadFileRepositoryTest {

	private static final String NON_EXISTING_ELECTION_EVENT_ID = "414bd34dcf6e4de4b771a92fa3849d3d";
	private static final String EXISTING_ELECTION_EVENT_ID = "b643acd93ccc453db5b1ed3ed910f4b2";
	private static final String CORRUPTED_ELECTION_EVENT_ID = "514bd34dcf6e4de4b771a92fa3849d3d";

	private static PathResolver pathResolver;
	private static ObjectMapper objectMapper;
	private static SetupComponentPublicKeysPayloadFileRepository setupComponentPublicKeysPayloadFileRepository;

	@BeforeAll
	static void setUpAll() throws URISyntaxException {
		objectMapper = DomainObjectMapper.getNewInstance();

		final Path path = Paths.get(
				Objects.requireNonNull(SetupComponentPublicKeysPayloadFileRepositoryTest.class.getClassLoader()
								.getResource("SetupComponentPublicKeysPayloadFileRepositoryTest/"))
						.toURI());
		pathResolver = new PathResolver(path.toString());
		setupComponentPublicKeysPayloadFileRepository = new SetupComponentPublicKeysPayloadFileRepository(objectMapper, pathResolver);
	}

	private SetupComponentPublicKeysPayload validSetupComponentPublicKeysPayload() {
		final List<ControlComponentPublicKeys> combinedControlComponentPublicKeys = new ArrayList<>();

		NODE_IDS.forEach((nodeId) -> combinedControlComponentPublicKeys.add(generateCombinedControlComponentPublicKeys(nodeId)));

		final ElGamalMultiRecipientPublicKey electoralBoardPublicKey = SerializationUtils.getPublicKey();
		final GroupVector<SchnorrProof, ZqGroup> electoralBoardSchnorrProofs = SerializationUtils.getSchnorrProofs(2);

		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccrChoiceReturnCodePublicKeys = combinedControlComponentPublicKeys.stream()
				.map(ControlComponentPublicKeys::ccrjChoiceReturnCodesEncryptionPublicKey).collect(GroupVector.toGroupVector());

		final ElGamal elGamal = ElGamalFactory.createElGamal();
		final ElGamalMultiRecipientPublicKey choiceReturnCodesPublicKey = elGamal.combinePublicKeys(ccrChoiceReturnCodePublicKeys);

		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmElectionPublicKeys = Streams.concat(
				combinedControlComponentPublicKeys.stream()
						.map(ControlComponentPublicKeys::ccmjElectionPublicKey),
				Stream.of(electoralBoardPublicKey)).collect(GroupVector.toGroupVector());

		final ElGamalMultiRecipientPublicKey electionPublicKey = elGamal.combinePublicKeys(ccmElectionPublicKeys);

		final SetupComponentPublicKeys setupComponentPublicKeys = new SetupComponentPublicKeys(combinedControlComponentPublicKeys,
				electoralBoardPublicKey, electoralBoardSchnorrProofs, electionPublicKey, choiceReturnCodesPublicKey);

		return new SetupComponentPublicKeysPayload(electionPublicKey.getGroup(), EXISTING_ELECTION_EVENT_ID, setupComponentPublicKeys);
	}

	private ControlComponentPublicKeys generateCombinedControlComponentPublicKeys(final int nodeId) {
		final ElGamalMultiRecipientPublicKey ccrChoiceReturnCodesEncryptionPublicKey = SerializationUtils.getPublicKey();
		final ElGamalMultiRecipientPublicKey ccmElectionPublicKey = SerializationUtils.getPublicKey();
		final GroupVector<SchnorrProof, ZqGroup> schnorrProofs = SerializationUtils.getSchnorrProofs(2);
		return new ControlComponentPublicKeys(nodeId, ccrChoiceReturnCodesEncryptionPublicKey, schnorrProofs, ccmElectionPublicKey, schnorrProofs);
	}

	@Nested
	@DisplayName("saving")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class SaveTest {

		private SetupComponentPublicKeysPayloadFileRepository setupComponentPublicKeysPayloadFileRepository1;

		private SetupComponentPublicKeysPayload setupComponentPublicKeysPayload;

		@BeforeAll
		void setUpAll(
				@TempDir
				final Path tempDir) throws IOException {

			Files.createDirectories(tempDir.resolve("sdm/config").resolve(EXISTING_ELECTION_EVENT_ID));

			final PathResolver pathResolver = new PathResolver(tempDir.toString());
			setupComponentPublicKeysPayloadFileRepository1 = new SetupComponentPublicKeysPayloadFileRepository(objectMapper, pathResolver);
		}

		@BeforeEach
		void setUp() {
			setupComponentPublicKeysPayload = validSetupComponentPublicKeysPayload();
		}

		@Test
		@DisplayName("valid election event context payload creates file")
		void save() {
			final Path savedPath = setupComponentPublicKeysPayloadFileRepository1.save(setupComponentPublicKeysPayload);

			assertTrue(Files.exists(savedPath));
		}

		@Test
		@DisplayName("null election event context payload throws NullPointerException")
		void saveNullElectionEventContext() {
			assertThrows(NullPointerException.class, () -> setupComponentPublicKeysPayloadFileRepository1.save(null));
		}

		@Test
		@DisplayName("invalid path throws UncheckedIOException")
		void invalidPath() {
			final PathResolver pathResolver = new PathResolver("invalidPath");
			final SetupComponentPublicKeysPayloadFileRepository repository = new SetupComponentPublicKeysPayloadFileRepository(
					DomainObjectMapper.getNewInstance(), pathResolver);

			final UncheckedIOException exception = assertThrows(UncheckedIOException.class, () -> repository.save(setupComponentPublicKeysPayload));

			final Path setupComponentPublicKeysPath = pathResolver.resolveElectionEventPath(EXISTING_ELECTION_EVENT_ID).resolve(PAYLOAD_FILE_NAME);
			final String errorMessage = String.format(
					"Failed to serialize setup component public keys payload. [electionEventId: %s, path: %s]", EXISTING_ELECTION_EVENT_ID,
					setupComponentPublicKeysPath);

			assertEquals(errorMessage, exception.getMessage());
		}

	}

	@Nested
	@DisplayName("calling existsById")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class ExistsByIdTest {

		@Test
		@DisplayName("for existing election event context payload returns true")
		void existingElectionEventContext() {
			assertTrue(setupComponentPublicKeysPayloadFileRepository.existsById(EXISTING_ELECTION_EVENT_ID));
		}

		@Test
		@DisplayName("with invalid election event id throws FailedValidationException")
		void invalidElectionEventId() {
			assertThrows(FailedValidationException.class, () -> setupComponentPublicKeysPayloadFileRepository.existsById("invalidId"));
		}

		@Test
		@DisplayName("for non existing election event context payload returns false")
		void nonExistingElectionEventContext() {
			assertFalse(setupComponentPublicKeysPayloadFileRepository.existsById(NON_EXISTING_ELECTION_EVENT_ID));
		}

	}

	@Nested
	@DisplayName("calling findById")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class FindByIdTest {

		@Test
		@DisplayName("for existing election event context payload returns it")
		void existingElectionEventContext() {
			assertTrue(setupComponentPublicKeysPayloadFileRepository.findById(EXISTING_ELECTION_EVENT_ID).isPresent());
		}

		@Test
		@DisplayName("for non existing election event context payload return empty optional")
		void nonExistingElectionEventContext() {
			assertFalse(setupComponentPublicKeysPayloadFileRepository.findById(NON_EXISTING_ELECTION_EVENT_ID).isPresent());
		}

		@Test
		@DisplayName("for corrupted election event context payload throws UncheckedIOException")
		void corruptedElectionEventContext() {
			final UncheckedIOException exception = assertThrows(UncheckedIOException.class,
					() -> setupComponentPublicKeysPayloadFileRepository.findById(CORRUPTED_ELECTION_EVENT_ID));

			final Path electionEventPath = pathResolver.resolveElectionEventPath(CORRUPTED_ELECTION_EVENT_ID);
			final Path setupComponentPublicKeysPath = electionEventPath.resolve(PAYLOAD_FILE_NAME);
			final String errorMessage = String.format("Failed to deserialize setup component public keys payload. [electionEventId: %s, path: %s]",
					CORRUPTED_ELECTION_EVENT_ID, setupComponentPublicKeysPath);

			assertEquals(errorMessage, exception.getMessage());
		}

	}

}
