/*
 * (c) Copyright 2022 Swiss Post Ltd
 */
package ch.post.it.evoting.securedatamanager.batch.batch.writers;

import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import ch.post.it.evoting.securedatamanager.commons.domain.VerificationCardIdValues;

@Component
public class VerificationCardIdValuesOutputWriter implements ItemWriter<List<VerificationCardIdValues>> {

	private final BlockingQueue<VerificationCardIdValues> queue;

	public VerificationCardIdValuesOutputWriter(final BlockingQueue<VerificationCardIdValues> queue) {
		this.queue = queue;
	}

	@Override
	public void write(final List<? extends List<VerificationCardIdValues>> itemsList) throws Exception {
		for (final List<VerificationCardIdValues> items : itemsList) {
			for (final VerificationCardIdValues item : items) {
				queue.put(item);
			}
		}
	}

}
