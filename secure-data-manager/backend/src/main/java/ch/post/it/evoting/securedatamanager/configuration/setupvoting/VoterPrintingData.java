/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

// The JsonPropertyOrder annotation is needed to parse the printingData.csv files.
@JsonPropertyOrder({
		"verificationCardSetId",
		"votingCardId",
		"verificationCardId",
		"electionEventId",
		"encodedVotingOptionToChoiceCodeMap",
		"ballotCastingKey",
		"voteCastCode",
		"ballotId",
		"startVotingKey",
		"voterId"
})
public record VoterPrintingData(String verificationCardSetId,
								String votingCardId,
								String verificationCardId,
								String electionEventId,
								@JsonDeserialize(using = JsonToMapDeserializer.class)
								Map<Integer, String> encodedVotingOptionToChoiceCodeMap,
								String ballotCastingKey,
								String voteCastCode,
								String ballotId,
								String startVotingKey,
								String voterId) {
	public VoterPrintingData {
		validateUUID(verificationCardSetId);
		validateUUID(votingCardId);
		validateUUID(verificationCardId);
		validateUUID(electionEventId);
		checkNotNull(encodedVotingOptionToChoiceCodeMap);
		checkNotNull(ballotCastingKey);
		checkNotNull(voteCastCode);
		validateUUID(ballotId);
		checkNotNull(startVotingKey);
		checkNotNull(voterId);
	}

	private static class JsonToMapDeserializer extends JsonDeserializer<Map<Integer, String>> {
		@Override
		public Map<Integer, String> deserialize(final JsonParser jsonParser, final DeserializationContext deserializationContext) throws IOException {
			final ObjectMapper objectMapper = new ObjectMapper();
			final String mapAsString = objectMapper.readValue(jsonParser, String.class);
			final Map<String, Integer> stringIntegerMap = objectMapper.readValue(mapAsString, new TypeReference<>() {
			});
			return stringIntegerMap.entrySet()
					.stream()
					.collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));
		}
	}
}
