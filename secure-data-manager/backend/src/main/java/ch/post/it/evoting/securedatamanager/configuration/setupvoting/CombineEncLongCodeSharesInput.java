/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Set;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.domain.validations.Validations;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupMatrix;

/**
 * Regroups the inputs needed by the CombineEncLongCodeShares algorithm.
 *
 * <ul>
 *    <li>C<sub>expPCC</sub>, the matrix of exponentiated, encrypted, hashed partial Choice Return Codes. Must be non-null.</li>
 *    <li>C<sub>expCK</sub>, the matrix of exponentiated, encrypted, hashed Confirmation Keys. Must be non-null.</li>
 *    <li>vc, the list of verification card ids.</li>
 * </ul>
 **/
public class CombineEncLongCodeSharesInput {

	private final GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix;
	private final GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedHashedConfirmationKeysMatrix;
	private final List<String> verificationCardIds;

	private CombineEncLongCodeSharesInput(
			final GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix,
			final GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedHashedConfirmationKeysMatrix,
			final List<String> verificationCardIds) {
		this.exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix = exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix;
		this.exponentiatedEncryptedHashedConfirmationKeysMatrix = exponentiatedEncryptedHashedConfirmationKeysMatrix;
		this.verificationCardIds = verificationCardIds;
	}

	public GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> getExponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix() {
		return this.exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix;
	}

	public GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> getExponentiatedEncryptedHashedConfirmationKeysMatrix() {
		return this.exponentiatedEncryptedHashedConfirmationKeysMatrix;
	}

	public List<String> getVerificationCardIds() {
		return this.verificationCardIds;
	}

	public static class Builder {
		private GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix;
		private GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedHashedConfirmationKeysMatrix;
		private List<String> verificationCardIds;

		public Builder setExponentiatedEncryptedChoiceReturnCodesMatrix(
				final GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedChoiceReturnCodesMatrix) {
			this.exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix = exponentiatedEncryptedChoiceReturnCodesMatrix;
			return this;
		}

		public Builder setExponentiatedEncryptedConfirmationKeysMatrix(
				final GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedConfirmationKeysMatrix) {
			this.exponentiatedEncryptedHashedConfirmationKeysMatrix = exponentiatedEncryptedConfirmationKeysMatrix;
			return this;
		}

		public Builder setVerificationCardIds(final List<String> verificationCardIds) {
			this.verificationCardIds = verificationCardIds;
			return this;
		}

		/**
		 * Creates the CombineEncLongCodeSharesInput. All fields must have been set and be non-null.
		 *
		 * @throws NullPointerException      if any of the fields is null.
		 * @throws IllegalArgumentException  if
		 *                                   <ul>
		 *                                     <li>The matrix of exponentiated, encrypted, hashed partial Choice Return Codes does not have exactly four columns.</li>
		 *                                     <li>The matrix of exponentiated, encrypted, hashed Confirmation Keys does not have exactly four columns.</li>
		 *                                     <li>The matrix of exponentiated, encrypted, hashed partial Choice Return Codes does not have any rows.</li>
		 *                                     <li>The matrix of exponentiated, encrypted, hashed Confirmation Keys does not have any rows.</li>
		 *                                     <li>The exponentiated, encrypted, hashed partial Choice Return Codes matrix and exponentiated, encrypted, hashed Confirmation Keys matrix
		 *                                     do not have the same number of rows.</li>
		 *                                     <li>The Vector of verification card ids does not have the same size as the number of rows of the matrix's</li>
		 *                                     <li>All inputs do not have the same Gq group.</li>
		 *                                   </ul>
		 * @throws FailedValidationException if any verification card id does not comply with the required UUID format.
		 */
		public CombineEncLongCodeSharesInput build() {
			checkNotNull(exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix);
			checkNotNull(exponentiatedEncryptedHashedConfirmationKeysMatrix);
			checkNotNull(verificationCardIds);

			final List<String> verificationCardIdsCopy = List.copyOf(verificationCardIds);
			verificationCardIdsCopy.forEach(Validations::validateUUID);
			checkArgument(Set.copyOf(verificationCardIdsCopy).size() == verificationCardIdsCopy.size(),
					"The verification card id list must not have duplicates.");
			checkArgument(!verificationCardIdsCopy.isEmpty(), "The vector of verification card Ids must have at least one element.");

			// Size checks.
			checkArgument(exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix.numColumns()
							== exponentiatedEncryptedHashedConfirmationKeysMatrix.numColumns(),
					"C_expPCC and C_expCK must have the same number of columns.");
			checkArgument(exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix.numColumns() == NODE_IDS.size(),
					"The matrix of exponentiated, encrypted, hashed partial Choice Return Codes does not have the expected number of columns. [expected: %s, actual: %s]",
					NODE_IDS.size(), exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix.numColumns());
			checkArgument(exponentiatedEncryptedHashedConfirmationKeysMatrix.numColumns() == NODE_IDS.size(),
					"The matrix of exponentiated, encrypted, hashed Confirmation Keys does not have the expected number of columns. [expected: %s, actual: %s]",
					NODE_IDS.size(), exponentiatedEncryptedHashedConfirmationKeysMatrix.numColumns());

			checkArgument(exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix.numRows()
							== exponentiatedEncryptedHashedConfirmationKeysMatrix.numRows(),
					"C_expPCC and C_expCK must have the same number of rows.");
			checkArgument(exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix.numRows() > 0,
					"The matrix of exponentiated, encrypted, hashed partial Choice Return Codes must have at least one row.");
			checkArgument(exponentiatedEncryptedHashedConfirmationKeysMatrix.numRows() > 0,
					"The matrix of exponentiated, encrypted, hashed Confirmation Keys must have at least one row.");
			checkArgument(exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix.numRows() == verificationCardIdsCopy.size(),
					"The number of rows of each matrix C_expPCC and C_expCK must be equal to the number of verification card ids.");

			final int ciphertextSize = exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix.get(0, 0).size();
			checkArgument(exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix.rowStream()
							.allMatch(columns -> columns.stream().allMatch(ciphertext -> ciphertext.size() == ciphertextSize)),
					"All ciphertext in the matrix of exponentiated, encrypted, hashed partial Choice Return Codes must have the same size.");

			checkArgument(exponentiatedEncryptedHashedConfirmationKeysMatrix.rowStream()
							.allMatch(columns -> columns.stream().allMatch(ciphertext -> ciphertext.size() == 1)),
					"All ciphertext in the matrix of exponentiated, encrypted, hashed Confirmation Keys must be of size 1.");

			// Cross group checks.
			checkArgument(exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix.getGroup()
					.equals(exponentiatedEncryptedHashedConfirmationKeysMatrix.getGroup()), "All input must have the same encryption group.");

			return new CombineEncLongCodeSharesInput(exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix,
					exponentiatedEncryptedHashedConfirmationKeysMatrix, verificationCardIdsCopy);
		}
	}
}
