/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@Service
public class VoterPrintingDataService {

	private static final String PRINTING_DATA_FILE = Constants.PRINTING_DATA + Constants.CSV;
	private static final char SEMICOLON_SEPARATOR = ';';
	private static final CsvMapper CSV_MAPPER = new CsvMapper();
	private static final CsvSchema CSV_SCHEMA = CSV_MAPPER.schemaFor(VoterPrintingData.class).withColumnSeparator(SEMICOLON_SEPARATOR)
			.withoutQuoteChar();

	private final PathResolver pathResolver;

	public VoterPrintingDataService(final PathResolver pathResolver) {
		this.pathResolver = pathResolver;
	}

	/**
	 * Loads a map of voterId and voter printing data corresponding to the given election event id.
	 *
	 * @param electionEventId the corresponding election event id.
	 * @return the map of voterId and voter printing data.
	 * @throws FailedValidationException if {@code electionEventId} is invalid.
	 * @throws IllegalStateException     if no voter printing data is found for the given id.
	 */
	@Cacheable("AllVoterPrintingData")
	public Map<String, VoterPrintingData> loadAll(final String electionEventId) {
		validateUUID(electionEventId);

		final List<Path> votingCarSetIdPaths;
		final Path printingPath = pathResolver.resolvePrintingPath(electionEventId);

		try (final Stream<Path> paths = Files.walk(printingPath, 1)) {
			votingCarSetIdPaths = paths
					.filter(path -> !printingPath.equals(path))
					.filter(Files::isDirectory)
					.toList();
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Unable to find voter printing data. [electionEventId: %s, path: %s]", electionEventId, printingPath), e);
		}

		return votingCarSetIdPaths.stream().parallel()
				.map(votingCarSetIdPath -> votingCarSetIdPath.resolve(PRINTING_DATA_FILE))
				.map(this::readPrintingDataFile)
				.flatMap(Collection::stream)
				.collect(Collectors.toMap(VoterPrintingData::voterId, voterPrintingData -> voterPrintingData));
	}

	private List<VoterPrintingData> readPrintingDataFile(final Path printingDataFilePath) {
		try {
			final List<VoterPrintingData> voterPrintingDataList;
			try (final MappingIterator<VoterPrintingData> csvIterator = CSV_MAPPER
					.readerFor(VoterPrintingData.class)
					.with(CSV_SCHEMA)
					.readValues(printingDataFilePath.toFile())) {

				voterPrintingDataList = csvIterator.readAll();
			}
			return voterPrintingDataList;
		} catch (final IOException e) {
			throw new UncheckedIOException(String.format("Unable to read printing data file. [path: %s]", printingDataFilePath), e);
		}
	}

}
