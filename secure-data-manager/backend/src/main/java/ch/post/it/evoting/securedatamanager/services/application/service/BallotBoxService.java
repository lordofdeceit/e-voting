/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.security.PrivateKey;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ch.post.it.evoting.cryptolib.api.exceptions.GeneralCryptoLibException;
import ch.post.it.evoting.cryptolib.certificates.utils.PemUtils;
import ch.post.it.evoting.cryptolib.commons.serialization.JsonSignatureService;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.election.BallotBox;
import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.config.commons.domain.common.SignedObject;
import ch.post.it.evoting.securedatamanager.config.commons.utils.JsonUtils;
import ch.post.it.evoting.securedatamanager.services.application.exception.DatabaseException;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.SynchronizeStatus;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballotbox.BallotBoxRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.service.ConfigurationEntityStatusService;

/**
 * Service to operate with ballot boxes.
 */
@Service
public class BallotBoxService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BallotBoxService.class);

	private final ObjectMapper objectMapper;
	private final PathResolver pathResolver;
	private final BallotBoxRepository ballotBoxRepository;
	private final ConfigurationEntityStatusService statusService;

	public BallotBoxService(
			final ObjectMapper objectMapper,
			final PathResolver pathResolver,
			final BallotBoxRepository ballotBoxRepository,
			final ConfigurationEntityStatusService statusService) {
		this.objectMapper = objectMapper;
		this.pathResolver = pathResolver;
		this.ballotBoxRepository = ballotBoxRepository;
		this.statusService = statusService;
	}

	/**
	 * Sign the ballot box configuration and change the state of the ballot box from ready to SIGNED for a given election event and ballot box id.
	 *
	 * @param electionEventId the election event id.
	 * @param ballotBoxId     the ballot box id.
	 * @param privateKeyPEM   the administration board private key in PEM format.
	 * @throws ResourceNotFoundException if the ballot box is not found.
	 * @throws GeneralCryptoLibException if the private key cannot be read.
	 */
	public void sign(final String electionEventId, final String ballotBoxId, final String privateKeyPEM)
			throws ResourceNotFoundException, GeneralCryptoLibException, IOException {

		final PrivateKey privateKey = PemUtils.privateKeyFromPem(privateKeyPEM);

		final JsonObject ballotBoxJsonObject = getValidBallotBox(electionEventId, ballotBoxId);
		final String ballotId = ballotBoxJsonObject.getJsonObject(JsonConstants.BALLOT).getString(JsonConstants.ID);

		final Path ballotBoxConfigurationFilesPath = pathResolver.resolveBallotBoxPath(electionEventId, ballotId, ballotBoxId);

		signBallotBoxJSON(privateKey, ballotBoxConfigurationFilesPath);
		LOGGER.info("Ballot box signed. [ballotBoxId: {}]", ballotBoxId);

		statusService.updateWithSynchronizedStatus(BallotBoxStatus.SIGNED.name(), ballotBoxId, ballotBoxRepository, SynchronizeStatus.PENDING);
		LOGGER.info("Ballot box status updated. [ballotBoxId: {}]", ballotBoxId);
	}

	/**
	 * Query ballot boxes to process in the synchronization process.
	 *
	 * @return JsonArray with ballot boxes to upload
	 */
	public JsonArray getBallotBoxesReadyToSynchronize(final String electionEvent) {

		final Map<String, Object> params = new HashMap<>();

		params.put(JsonConstants.STATUS, BallotBoxStatus.SIGNED.name());
		params.put(JsonConstants.SYNCHRONIZED, SynchronizeStatus.PENDING.getIsSynchronized().toString());
		// If there is an election event as parameter, it will be included in the query.
		if (!Constants.NULL_ELECTION_EVENT_ID.equals(electionEvent)) {
			params.put(JsonConstants.ELECTION_EVENT_DOT_ID, electionEvent);
		}
		final String serializedBallotBoxes = ballotBoxRepository.list(params);

		return JsonUtils.getJsonObject(serializedBallotBoxes).getJsonArray(JsonConstants.RESULT);
	}

	/**
	 * Updates the state of the synchronization status of the ballot box
	 */
	public void updateSynchronizationStatus(final String ballotBoxId, final boolean success) {
		final JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
		jsonObjectBuilder.add(JsonConstants.ID, ballotBoxId);
		if (success) {
			jsonObjectBuilder.add(JsonConstants.SYNCHRONIZED, SynchronizeStatus.SYNCHRONIZED.getIsSynchronized().toString());
			jsonObjectBuilder.add(JsonConstants.DETAILS, SynchronizeStatus.SYNCHRONIZED.getStatus());
		} else {
			jsonObjectBuilder.add(JsonConstants.DETAILS, SynchronizeStatus.FAILED.getStatus());
		}
		ballotBoxRepository.update(jsonObjectBuilder.build().toString());
	}

	/**
	 * Checks that the ballot box has status {@link BallotBoxStatus#DOWNLOADED}.
	 */
	public boolean isDownloaded(final String ballotBoxId) {
		validateUUID(ballotBoxId);

		return hasStatus(ballotBoxId, BallotBoxStatus.DOWNLOADED);
	}

	/**
	 * Checks that the ballot box has {@code expectedStatus}
	 *
	 * @throws IllegalStateException if the ballot box cannot be found in the ballot box repository or the subsequent retrieved JSON can't be
	 *                               correctly parsed.
	 */
	public boolean hasStatus(final String ballotBoxId, final BallotBoxStatus expectedStatus) {
		validateUUID(ballotBoxId);
		checkNotNull(expectedStatus);

		final BallotBoxStatus actualStatus = getBallotBoxStatus(ballotBoxId);

		return expectedStatus.equals(actualStatus);
	}

	/**
	 * Gets the status of a ballot box.
	 *
	 * @param ballotBoxId the ballot box id to get the status.
	 * @return the status of the ballot box.
	 * @throws IllegalStateException if the ballot box cannot be found in the ballot box repository or the subsequent retrieved JSON can't be
	 *                               correctly parsed.
	 */
	public BallotBoxStatus getBallotBoxStatus(final String ballotBoxId) {
		validateUUID(ballotBoxId);

		final JsonNode ballotBox = getBallotBox(ballotBoxId);
		final JsonNode status = ballotBox.path("status");
		checkState(!status.isMissingNode(), "Can't find status for [ballotBoxId: %s] ", ballotBoxId);

		final BallotBoxStatus actualStatus;
		try {
			actualStatus = objectMapper.readValue(status.toString(), BallotBoxStatus.class);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(String.format("Can't deserialize the status [ballotBoxId: %s] ", ballotBoxId), e);
		}

		return actualStatus;
	}

	/**
	 * Gets the ballot id associated with this ballot box.
	 */
	public String getBallotId(final String ballotBoxId) {
		validateUUID(ballotBoxId);

		final JsonNode ballotBox = getBallotBox(ballotBoxId);
		final JsonNode id = ballotBox.path(JsonConstants.BALLOT).path(JsonConstants.ID);
		checkState(!id.isMissingNode(), "Can't find id for ballotBox. [ballotBoxId: %s]", ballotBoxId);

		return id.textValue();
	}

	/**
	 * Returns the grace period of the ballot box identified by the given ballotBoxId.
	 *
	 * @param ballotBoxId identifies the ballot box where to search. Must be non-null and a valid UUID.
	 * @return the grace period.
	 * @throws FailedValidationException if the given ballot box is null or not a valid UUID.
	 * @throws IllegalArgumentException  if the found ballot box is a {@link JsonConstants#EMPTY_OBJECT}.
	 * @throws DatabaseException         if no ballot box is found.
	 */
	public int getGracePeriod(final String ballotBoxId) {
		validateUUID(ballotBoxId);
		return ballotBoxRepository.getGracePeriod(ballotBoxId);
	}

	/**
	 * Indicates if the ballot box corresponding to the given ballot box id is a test ballot box.
	 *
	 * @param ballotBoxId the ballot box id. Must be non-null and a valid UUID.
	 * @return true if the corresponding ballot box is a test ballot box, false otherwise.
	 * @throws FailedValidationException if the given ballot box is null or not a valid UUID.
	 * @throws IllegalArgumentException  if the found ballot box is a {@link JsonConstants#EMPTY_OBJECT}.
	 * @throws DatabaseException         if no ballot box is found.
	 */
	public boolean isTestBallotBox(final String ballotBoxId) {
		validateUUID(ballotBoxId);
		return ballotBoxRepository.isTestBallotBox(ballotBoxId);
	}

	/**
	 * Checks if the given ballot box has the {@link BallotBoxStatus#DECRYPTED} status.
	 *
	 * @param ballotBoxId the ballot box id to check.
	 * @return {@code true} if the ballot box has the decrypted status, {@code false} otherwise.
	 * @throws NullPointerException      if {@code ballotBoxId} is null.
	 * @throws FailedValidationException if {@code ballotBoxId} is invalid.
	 */
	public boolean isDecrypted(final String ballotBoxId) {
		validateUUID(ballotBoxId);

		return hasStatus(ballotBoxId, BallotBoxStatus.DECRYPTED);
	}

	/**
	 * Sets the status of the given ballot box to {@link BallotBoxStatus#DECRYPTED}.
	 *
	 * @param ballotBoxId the ballot box id to set the status.
	 * @throws NullPointerException      if {@code ballotBoxId} is null.
	 * @throws FailedValidationException if {@code ballotBoxId} is invalid.
	 */
	public void setDecrypted(final String ballotBoxId) {
		validateUUID(ballotBoxId);

		updateBallotBoxStatus(ballotBoxId, BallotBoxStatus.DECRYPTED);
	}

	/**
	 * Updates the status of a ballot box with {@code newStatus}.
	 *
	 * @param ballotBoxId the ballot box if to update the status.
	 * @param newStatus   the new status of the ballot box.
	 * @return the new status after update.
	 */
	public BallotBoxStatus updateBallotBoxStatus(final String ballotBoxId, final BallotBoxStatus newStatus) {

		final JsonNode ballotBox = getBallotBox(ballotBoxId);

		((ObjectNode) ballotBox).put(JsonConstants.STATUS, newStatus.toString());
		ballotBoxRepository.update(ballotBox.toString());

		return newStatus;
	}

	/**
	 * Retrieves all ballot boxes associated to the election event with {@code electionEventId}.
	 *
	 * @param electionEventId the election event id for which to retrieve the ballot boxes.
	 * @return all ballot boxes as a JSON string.
	 */
	public String getBallotBoxes(final String electionEventId) {
		validateUUID(electionEventId);

		final Map<String, Object> attributeValueMap = new HashMap<>();
		attributeValueMap.put(JsonConstants.ELECTION_EVENT_DOT_ID, electionEventId);

		return ballotBoxRepository.list(attributeValueMap);
	}

	/**
	 * Retrieves all ballot boxes associated to the election event with {@code electionEventId}.
	 *
	 * @param electionEventId the election event id for which to retrieve the ballot boxes. Must be a valid UUID.
	 * @return all ballot boxes as a JSON string.
	 */
	public List<String> getBallotBoxesId(final String electionEventId) {
		validateUUID(electionEventId);

		final JsonArray ballotBoxResultList = JsonUtils.getJsonObject(getBallotBoxes(electionEventId)).getJsonArray(JsonConstants.RESULT);

		return ballotBoxResultList.stream().map(jsonValue -> jsonValue.asJsonObject().getString(JsonConstants.ID)).toList();
	}

	/**
	 * Retrieves the ballot box with {@code ballotBoxId}.
	 *
	 * @param ballotBoxId the ballot box id to retrieve.
	 * @return the ballot box as a {@link JsonNode}.
	 * @throws UncheckedIOException if the deserialization of the ballot box fails.
	 */
	public JsonNode getBallotBox(final String ballotBoxId) {
		try {
			return objectMapper.readTree(ballotBoxRepository.find(ballotBoxId));
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(String.format("Failed to deserialize ballot box. [ballotBoxId: %s] ", ballotBoxId), e);
		}
	}

	private void signBallotBoxJSON(final PrivateKey privateKey, final Path ballotBoxConfigurationFilesPath) throws IOException {
		final Path ballotBoxJSONPath = ballotBoxConfigurationFilesPath.resolve(Constants.CONFIG_DIR_NAME_BALLOTBOX_JSON).toAbsolutePath();
		final Path signedBallotBoxJSONPath = ballotBoxConfigurationFilesPath.resolve(Constants.CONFIG_FILE_NAME_SIGNED_BALLOTBOX_JSON)
				.toAbsolutePath();

		final BallotBox ballotBox = objectMapper.readValue(new File(ballotBoxJSONPath.toString()), BallotBox.class);

		final String signedBallotBox = JsonSignatureService.sign(privateKey, ballotBox);
		final SignedObject signedBallotBoxObject = new SignedObject();
		signedBallotBoxObject.setSignature(signedBallotBox);
		objectMapper.writeValue(new File(signedBallotBoxJSONPath.toString()), signedBallotBoxObject);
	}

	private JsonObject getValidBallotBox(final String electionEventId, final String ballotBoxId) throws ResourceNotFoundException {

		final Optional<JsonObject> possibleBallotBox = getPossibleValidBallotBox(electionEventId, ballotBoxId);

		if (!possibleBallotBox.isPresent()) {
			throw new ResourceNotFoundException("Ballot box not found");
		}

		return possibleBallotBox.get();
	}

	private Optional<JsonObject> getPossibleValidBallotBox(final String electionEventId, final String ballotBoxId) {

		Optional<JsonObject> ballotBox = Optional.empty();

		final Map<String, Object> attributeValueMap = new HashMap<>();
		attributeValueMap.put(JsonConstants.ELECTION_EVENT_DOT_ID, electionEventId);
		attributeValueMap.put(JsonConstants.ID, ballotBoxId);
		attributeValueMap.put(JsonConstants.STATUS, BallotBoxStatus.READY.name());
		final String ballotBoxResultListAsJson = ballotBoxRepository.list(attributeValueMap);
		if (StringUtils.isEmpty(ballotBoxResultListAsJson)) {
			return ballotBox;
		} else {
			final JsonArray ballotBoxResultList = JsonUtils.getJsonObject(ballotBoxResultListAsJson).getJsonArray(JsonConstants.RESULT);
			// Assume that there is just one element as result of the search.
			if (ballotBoxResultList != null && !ballotBoxResultList.isEmpty()) {
				ballotBox = Optional.of(ballotBoxResultList.getJsonObject(0));
			} else {
				return ballotBox;
			}
		}
		return ballotBox;
	}

}
