/*
 * (c) Copyright 2022 Swiss Post Ltd
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import java.security.SignatureException;
import java.util.List;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SignedPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.ChannelSecurityContextData;
import ch.post.it.evoting.domain.configuration.ChoiceReturnCodeToEncodedVotingOptionEntry;
import ch.post.it.evoting.domain.configuration.SetupComponentCMTablePayload;
import ch.post.it.evoting.domain.configuration.VoterReturnCodes;
import ch.post.it.evoting.domain.configuration.VoterReturnCodesPayload;
import ch.post.it.evoting.domain.configuration.setupvoting.SetupComponentLVCCAllowListPayload;
import ch.post.it.evoting.securedatamanager.configuration.PrimesMappingTableService;
import ch.post.it.evoting.securedatamanager.task.ExecutionTaskService;
import ch.post.it.evoting.securedatamanager.task.ExecutionTaskType;

@Service
@ConditionalOnProperty("role.isConfig")
public class ReturnCodesPayloadsGeneratedService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReturnCodesPayloadsGeneratedService.class);

	private final ExecutionTaskService executionTaskService;
	private final VotingCardSetDataGenerationService votingCardSetDataGenerationService;
	private final ReturnCodesPayloadsPersistenceService returnCodesPayloadsPersistenceService;
	private final ApplicationEventPublisher applicationEventPublisher;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final PrimesMappingTableService primesMappingTableService;

	public ReturnCodesPayloadsGeneratedService(
			final ExecutionTaskService executionTaskService,
			final VotingCardSetDataGenerationService votingCardSetDataGenerationService,
			final ReturnCodesPayloadsPersistenceService returnCodesPayloadsPersistenceService,
			final ApplicationEventPublisher applicationEventPublisher,
			@Qualifier("keystoreServiceSdmConfig")
			final SignatureKeystore<Alias> signatureKeystoreService,
			final PrimesMappingTableService primesMappingTableService) {
		this.executionTaskService = executionTaskService;
		this.votingCardSetDataGenerationService = votingCardSetDataGenerationService;
		this.returnCodesPayloadsPersistenceService = returnCodesPayloadsPersistenceService;
		this.signatureKeystoreService = signatureKeystoreService;
		this.applicationEventPublisher = applicationEventPublisher;
		this.primesMappingTableService = primesMappingTableService;
	}

	/**
	 * Generates and persists on the file system the following payloads:
	 * <ul>
	 *     <li>Setup component CMTable payload</li>
	 *     <li>Voter Return Codes payload</li>
	 *     <li>Setup component LVCC allow list payload</li>
	 * </ul>
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @param votingCardSetId the voting card set id. Must be non-null and a valid UUID.
	 * @throws NullPointerException      if {@code electionEventId} or {@code votingCardSetId} is null.
	 * @throws FailedValidationException if {@code electionEventId} or {@code votingCardSetId} is invalid.
	 */
	public void generate(final String electionEventId, final String votingCardSetId) {
		validateUUID(electionEventId);
		validateUUID(votingCardSetId);

		executionTaskService.executeTask(votingCardSetId, ExecutionTaskType.RETURN_CODES_PAYLOADS, "Return codes payloads generation task",
				() -> generateAndSavePayloads(electionEventId, votingCardSetId));
	}

	/**
	 * This method is asynchronously executed through the {@link ExecutionTaskService}.
	 */
	private void generateAndSavePayloads(final String electionEventId, final String votingCardSetId) {
		LOGGER.info("Generating return codes payloads... [electionEventId: {}, votingCardSetId: {}]", electionEventId, votingCardSetId);

		// ReturnCodesGenerationOutput
		final ReturnCodesGenerationOutput returnCodesGenerationOutput = votingCardSetDataGenerationService.generate(electionEventId, votingCardSetId);

		// Setup component CMTable payload
		final SetupComponentCMTablePayload setupComponentCMTablePayload = new SetupComponentCMTablePayload.Builder()
				.setElectionEventId(returnCodesGenerationOutput.getElectionEventId())
				.setVerificationCardSetId(returnCodesGenerationOutput.getVerificationCardSetId())
				.setReturnCodesMappingTable(returnCodesGenerationOutput.getReturnCodesMappingTable())
				.build();

		final String verificationCardSetId = setupComponentCMTablePayload.getVerificationCardSetId();
		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentCMTable(electionEventId, verificationCardSetId);

		final CryptoPrimitivesSignature setupComponentCMTablePayloadSignature = getPayloadSignature(setupComponentCMTablePayload,
				additionalContextData);
		setupComponentCMTablePayload.setSignature(setupComponentCMTablePayloadSignature);

		LOGGER.debug("Successfully signed setup component CMTable payload [electionEventId: {}, verificationCardSetId: {}]",
				electionEventId, returnCodesGenerationOutput.getVerificationCardSetId());

		// Voter Return Codes payload
		final VoterReturnCodesPayload voterReturnCodesPayload = generateVoterReturnCodesPayload(returnCodesGenerationOutput);

		// Setup component LVCC allow list payload
		final SetupComponentLVCCAllowListPayload setupComponentLVCCAllowListPayload =
				generateLongVoteCastReturnCodesAllowListPayload(returnCodesGenerationOutput);

		// Persist the payloads
		returnCodesPayloadsPersistenceService.save(returnCodesGenerationOutput.getElectionEventId(), votingCardSetId,
				setupComponentCMTablePayload, voterReturnCodesPayload, setupComponentLVCCAllowListPayload);

		LOGGER.info("Return codes payloads are successfully generated and persisted. [electionEventId: {}, votingCardSetId: {}]",
				returnCodesGenerationOutput.getElectionEventId(), votingCardSetId);

		final VotingCardSetGenerationStatusEvent votingCardSetGenerationStatusEvent = new VotingCardSetGenerationStatusEvent.Builder()
				.setElectionEventId(electionEventId)
				.setVotingCardSetId(votingCardSetId)
				.setGenerationType(GenerationType.EXECUTION_TASK).build();
		applicationEventPublisher.publishEvent(votingCardSetGenerationStatusEvent);
		LOGGER.debug("ExecutionTask generation event is published. [votingCardSetId: {}]", votingCardSetId);
	}

	private VoterReturnCodesPayload generateVoterReturnCodesPayload(final ReturnCodesGenerationOutput returnCodesGenerationOutput) {
		final String electionEventId = returnCodesGenerationOutput.getElectionEventId();
		final String verificationCardSetId = returnCodesGenerationOutput.getVerificationCardSetId();

		final PrimesMappingTable primesMappingTable = primesMappingTableService.load(electionEventId, verificationCardSetId);
		final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions = primesMappingTable.getPTable().stream()
				.map(PrimesMappingTableEntry::encodedVotingOption)
				.collect(GroupVector.toGroupVector());

		final List<String> verificationCardIds = returnCodesGenerationOutput.getVerificationCardIds();
		final List<List<String>> shortChoiceReturnCodesList = returnCodesGenerationOutput.getShortChoiceReturnCodes();
		final List<String> shortVoteCastReturnCodes = returnCodesGenerationOutput.getShortVoteCastReturnCodes();

		final List<VoterReturnCodes> voterReturnCodes = IntStream.range(0, verificationCardIds.size()).parallel()
				.mapToObj(i -> {
					final List<String> shortChoiceReturnCodes = shortChoiceReturnCodesList.get(i);

					final GroupVector<ChoiceReturnCodeToEncodedVotingOptionEntry, GqGroup> choiceReturnCodesToEncodedVotingOptions =
							IntStream.range(0, encodedVotingOptions.size())
									.mapToObj(idx ->
											new ChoiceReturnCodeToEncodedVotingOptionEntry(shortChoiceReturnCodes.get(idx),
													encodedVotingOptions.get(idx)))
									.collect(GroupVector.toGroupVector());

					return new VoterReturnCodes(verificationCardIds.get(i), shortVoteCastReturnCodes.get(i), choiceReturnCodesToEncodedVotingOptions,
							encodedVotingOptions.getGroup());
				}).toList();

		return new VoterReturnCodesPayload(electionEventId, verificationCardSetId, voterReturnCodes);
	}

	private SetupComponentLVCCAllowListPayload generateLongVoteCastReturnCodesAllowListPayload(
			final ReturnCodesGenerationOutput returnCodesGenerationOutput) {

		final String electionEventId = returnCodesGenerationOutput.getElectionEventId();
		final String verificationCardSetId = returnCodesGenerationOutput.getVerificationCardSetId();
		final List<String> longVoteCastReturnCodesAllowList = returnCodesGenerationOutput.getLongVoteCastReturnCodesAllowList();

		final SetupComponentLVCCAllowListPayload setupComponentLVCCAllowListPayload =
				new SetupComponentLVCCAllowListPayload(electionEventId, verificationCardSetId, longVoteCastReturnCodesAllowList);
		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentLVCCAllowList(electionEventId, verificationCardSetId);

		setupComponentLVCCAllowListPayload.setSignature(getPayloadSignature(setupComponentLVCCAllowListPayload, additionalContextData));

		LOGGER.debug("Successfully signed setup component LVCC allow list payload. [electionEventId: {}, verificationCardSetId: {}]",
				electionEventId, verificationCardSetId);

		return setupComponentLVCCAllowListPayload;
	}

	private CryptoPrimitivesSignature getPayloadSignature(final SignedPayload payload, final Hashable additionalContextData) {
		try {
			final byte[] signature = signatureKeystoreService.generateSignature(payload, additionalContextData);

			return new CryptoPrimitivesSignature(signature);
		} catch (final SignatureException e) {
			throw new IllegalStateException(String.format(
					"Failed to generate the payload signature. [name: %s]", payload.getClass().getName()), e);
		}
	}

}
