/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.EncryptionParametersPayload;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

/**
 * Allows loading the encryption parameters associated to an election event.
 */
@Service
public class EncryptionParametersPayloadService {

	private final EncryptionParametersPayloadFileRepository encryptionParametersPayloadFileRepository;

	public EncryptionParametersPayloadService(final EncryptionParametersPayloadFileRepository encryptionParametersPayloadFileRepository) {
		this.encryptionParametersPayloadFileRepository = encryptionParametersPayloadFileRepository;
	}

	/**
	 * Saves an encryption parameters payload in the corresponding election event folder.
	 *
	 * @param electionEventId             the election event id. Must be non-null and a valid UUID.
	 * @param encryptionParametersPayload the encryption parameters payload to save. Must be non-null.
	 * @throws NullPointerException if the payload is null.
	 */
	public void save(final String electionEventId, final EncryptionParametersPayload encryptionParametersPayload) {
		validateUUID(electionEventId);
		checkNotNull(encryptionParametersPayload);

		encryptionParametersPayloadFileRepository.save(electionEventId, encryptionParametersPayload);
	}

	/**
	 * Loads the encryption parameters for the given {@code electionEventId}.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @return the encryption parameters for this {@code electionEventId}.
	 * @throws NullPointerException      if any input is null.
	 * @throws FailedValidationException if {@code electionEventId} is an invalid UUID.
	 * @throws IllegalStateException     if the requested encryption parameters payload is not present. </li>
	 */
	public EncryptionParametersPayload load(final String electionEventId) {
		validateUUID(electionEventId);

		return encryptionParametersPayloadFileRepository.findById(electionEventId)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Requested encryption parameters payload is not present. [electionEventId: %s]", electionEventId)));
	}

	/**
	 * Loads the encryption parameters for the given {@code electionEventId}. The result of this method is cached.
	 *
	 * @param electionEventId the election event id for which to get the encryption parameters.
	 * @return the encryption parameters.
	 * @throws FailedValidationException if {@code electionEventId} is invalid.
	 * @throws IllegalStateException     if the encryption parameters are not found for this {@code electionEventId}.
	 */
	@Cacheable("gqGroups")
	public GqGroup loadEncryptionGroup(final String electionEventId) {
		validateUUID(electionEventId);

		return encryptionParametersPayloadFileRepository.findById(electionEventId)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Requested encryption parameters payload is not present. [electionEventId: %s]", electionEventId)))
				.getEncryptionGroup();
	}

}
