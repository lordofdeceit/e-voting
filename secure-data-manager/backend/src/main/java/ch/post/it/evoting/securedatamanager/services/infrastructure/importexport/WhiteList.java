/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.importexport;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Define regexp patterns that will match the files needed to import/export the SDM files.
 * </p>
 * The importation whitelist will match all the elections present, while the exportation whitelist will match only the given elections ID.
 */
class WhiteList {

	// enough to reach the end of SDM structure while preventing wasting time in incorrect deep tree.
	public static final int MAX_DEPTH = 10;

	private WhiteList() {
		// utility class
	}

	public static List<Pattern> getImportList() {
		return getList("[a-z0-9]{32}");
	}

	public static List<Pattern> getExportList(final String electionEventId) {
		return getList(electionEventId);
	}

	private static List<Pattern> getList(final String electionEventId) {
		final String electionEventDirectory = String.format("config/%s/", electionEventId);
		final String customer = electionEventDirectory + "CUSTOMER/";
		final String online = electionEventDirectory + "ONLINE/";
		final String offline = electionEventDirectory + "OFFLINE/";

		return List.of(
				/*sdmConfig root*/
				createEntry("sdmConfig/elections_config\\.json"),
				createEntry("sdmConfig/elections_config\\.json\\.p7"),

				/*csr*/
				createEntry("config/csr/[a-z0-9]{32}\\.pem"),

				/*election root*/
				createEntry(electionEventDirectory + "controlComponentPublicKeysPayload\\.[1-4]{1}\\.json"),
				createEntry(electionEventDirectory + "electionEventContextPayload\\.json"),
				createEntry(electionEventDirectory + "setupComponentPublicKeysPayload\\.json"),

				/*customer directory*/
				createEntry(customer + "input/configuration-anonymized\\.xml"),
				createEntry(customer + "input/configuration-anonymized\\.xml\\.p7"),
				createEntry(customer + "input/encryptionParametersPayload\\.json"),
				createEntry(customer + "output/AP_election_import_Post_E2E_DEV\\.json"),
				createEntry(customer + "output/AP_election_import_Post_E2E_DEV\\.json\\.p7"),

				/*offline directory*/
				createEntry(offline + "setupComponentElectoralBoardHashesPayload\\.json"),

				/*online directory*/
				createEntry(
						online + "authentication/authentication(ContextData|VoterData)\\.json(\\.sign)?"),
				createEntry(online + "electionInformation/ballots/[a-z0-9]{32}/ballot\\.json"),
				createEntry(online + "electionInformation/ballots/[a-z0-9]{32}/ballotBoxes/[a-z0-9]{32}/ballotBox\\.json"),
				createEntry(online + "electionInformation/ballots/[a-z0-9]{32}/ballotBoxes/[a-z0-9]{32}/ballotBox\\.json\\.sign"),
				createEntry(
						online
								+ "electionInformation/ballots/[a-z0-9]{32}/ballotBoxes/[a-z0-9]{32}/controlComponentBallotBoxPayload_[1-4]{1}\\.json"),
				createEntry(
						online + "electionInformation/ballots/[a-z0-9]{32}/ballotBoxes/[a-z0-9]{32}/controlComponentShufflePayload_[1-4]{1}\\.json"),
				/*check if possible to remove*/
				createEntry(online + "electionInformation/ballots/[a-z0-9]{32}/ballotBoxes/[a-z0-9]{32}/tallyComponent(Shuffle|Votes)Payload\\.json"),
				createEntry(online + "extendedAuthentication/[a-z0-9]{32}/extendedAuthentication\\.[0-9]+\\.csv"),
				createEntry(online + "extendedAuthentication/[a-z0-9]{32}/extendedAuthentication\\.[0-9]+\\.csv\\.sign"),
				createEntry(online + "voterMaterial/[a-z0-9]{32}/credentialData\\.[0-9]+\\.csv(\\.sign)?"),
				createEntry(online + "voterMaterial/[a-z0-9]{32}/voterInformation\\.[0-9]+\\.csv(\\.sign)?"),
				createEntry(online + "voteVerification/[a-z0-9]{32}/controlComponentCodeSharesPayload\\.[0-9]+\\.json"),
				createEntry(online + "voteVerification/[a-z0-9]{32}/setupComponentCMtablePayload\\.json"),
				createEntry(online + "voteVerification/[a-z0-9]{32}/setupComponentLVCCAllowListPayloadFileRepository\\.json"),
				createEntry(online + "voteVerification/[a-z0-9]{32}/setupComponentTallyDataPayload\\.json"),
				createEntry(online + "voteVerification/[a-z0-9]{32}/setupComponentVerificationCardKeystoresPayload\\.json"),
				createEntry(online + "voteVerification/[a-z0-9]{32}/setupComponentVerificationDataPayload\\.[0-9]+\\.json")
		);
	}

	private static Pattern createEntry(final String regex) {
		return Pattern.compile(regex);
	}
}
