/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api.setup;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.cryptolib.api.exceptions.GeneralCryptoLibException;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.application.service.IdleStatusService;
import ch.post.it.evoting.securedatamanager.services.application.service.VotingCardSetPrecomputationService;
import ch.post.it.evoting.securedatamanager.services.application.service.VotingCardSetPreparationService;
import ch.post.it.evoting.securedatamanager.services.application.service.VotingCardSetSignService;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.InvalidStatusTransitionException;
import ch.post.it.evoting.securedatamanager.services.domain.model.votingcardset.VotingCardSetUpdateInputData;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.PayloadStorageException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * The voting card set end-point for the offline SDM.
 */
@RestController
@RequestMapping("/sdm-backend/setup/votingcardsets")
@Api(value = "Voting card set REST API")
@ConditionalOnProperty("role.isConfig")
public class VotingCardSetSetupController {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetSetupController.class);

	private static final String VOTING_CARD_SET_URL_PATH = "/electionevent/{electionEventId}/votingcardset/{votingCardSetId}";

	private final IdleStatusService idleStatusService;
	private final VotingCardSetSignService votingCardSetSignService;
	private final VotingCardSetPreparationService votingCardSetPreparationService;

	private final VotingCardSetPrecomputationService votingCardSetPrecomputationService;

	public VotingCardSetSetupController(final IdleStatusService idleStatusService,
			final VotingCardSetSignService votingCardSetSignService,
			final VotingCardSetPreparationService votingCardSetPreparationService,
			final VotingCardSetPrecomputationService votingCardSetPrecomputationService) {
		this.idleStatusService = idleStatusService;
		this.votingCardSetSignService = votingCardSetSignService;
		this.votingCardSetPreparationService = votingCardSetPreparationService;
		this.votingCardSetPrecomputationService = votingCardSetPrecomputationService;
	}

	/**
	 * Changes the status of a list of voting card sets by performing the prepare and precompute operations. The HTTP call uses a PUT request to the
	 * voting card set endpoint. If the requested status cannot be transitioned to precomputed from the current one, the call will fail.
	 *
	 * @param electionEventId the election event id.
	 * @param votingCardSetId the voting card set id.
	 * @return a list of ids of the created voting card sets.
	 */
	@PutMapping(value = VOTING_CARD_SET_URL_PATH + "/precompute", consumes = "application/json", produces = "application/json")
	@ApiOperation(value = "Change the status of a voting card set to precomputed",
			notes = "Change the status of a voting card set to precomputed, performing the prepare and precompute operations")
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Not Found") })
	public ResponseEntity<Object> setVotingCardSetStatusPrecomputed(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electionEventId,
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String votingCardSetId,
			@ApiParam(required = true)
			@RequestBody
			final VotingCardSetUpdateInputData requestBody) throws PayloadStorageException, ResourceNotFoundException {
		validateUUID(electionEventId);
		validateUUID(votingCardSetId);

		ResponseEntity<Object> response;
		try {
			votingCardSetPreparationService.prepare(electionEventId, votingCardSetId, requestBody.privateKeyPEM());
			votingCardSetPrecomputationService.precompute(votingCardSetId, electionEventId, requestBody.adminBoardId());
			response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (final InvalidStatusTransitionException e) {
			LOGGER.info("Error trying to set voting card set status to precomputed. [electionEventId: {}, votingCardSetId: {}]", electionEventId,
					votingCardSetId, e);
			response = ResponseEntity.badRequest().build();
		}
		return response;
	}

	/**
	 * Changes the status of a list of voting card sets by performing the sign operation. The HTTP call uses a PUT request to the voting card set
	 * endpoint. If the requested status cannot be transitioned to computing from the current one, the call will fail.
	 *
	 * @param electionEventId the election event id.
	 * @param votingCardSetId the voting card set id.
	 * @return a list of ids of the created voting card sets.
	 */
	@PutMapping(value = VOTING_CARD_SET_URL_PATH + "/sign", consumes = "application/json", produces = "application/json")
	@ApiOperation(value = "Change the status of a voting card set to signed",
			notes = "Change the status of a voting card set to signed, performing the sign operation")
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Not Found") })
	public ResponseEntity<Object> setVotingCardSetStatusSigned(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electionEventId,
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String votingCardSetId,
			@ApiParam(required = true)
			@RequestBody
			final VotingCardSetUpdateInputData requestBody) {

		validateUUID(electionEventId);
		validateUUID(votingCardSetId);

		ResponseEntity<Object> response;
		response = new ResponseEntity<>(signVotingCardSet(electionEventId, votingCardSetId, requestBody.privateKeyPEM()));
		return response;
	}

	/**
	 * Change the state of the voting card set from generated to signed for a given election event and voting card set id.
	 *
	 * @param electionEventId the election event id.
	 * @param votingCardSetId the voting card set id.
	 * @return HTTP status code 200 - If the voting card set is successfully signed. HTTP status code 404 - If the resource is not found. HTTP status
	 * code 412 - If the votig card set is already signed.
	 */
	private HttpStatus signVotingCardSet(final String electionEventId, final String votingCardSetId, final String privateKeyPEM) {

		if (!idleStatusService.getIdLock(votingCardSetId)) {
			return HttpStatus.OK;
		}

		final String fetchingErrorMessage = "An error occurred while fetching the given voting card set to sign";
		final String signingErrorMessage = "An error occurred while signing the given voting card set";
		try {
			if (votingCardSetSignService.sign(electionEventId, votingCardSetId, privateKeyPEM)) {
				return HttpStatus.OK;
			} else {
				LOGGER.error(fetchingErrorMessage);

				return HttpStatus.PRECONDITION_FAILED;
			}
		} catch (final ResourceNotFoundException e) {
			LOGGER.error(fetchingErrorMessage, e);
			return HttpStatus.NOT_FOUND;
		} catch (final GeneralCryptoLibException | IOException e) {
			LOGGER.error(signingErrorMessage, e);

			return HttpStatus.PRECONDITION_FAILED;
		} finally {
			idleStatusService.freeIdLock(votingCardSetId);
		}
	}
}
