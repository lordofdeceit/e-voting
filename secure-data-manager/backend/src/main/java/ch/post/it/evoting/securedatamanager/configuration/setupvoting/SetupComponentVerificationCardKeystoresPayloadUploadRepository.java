/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.PrivateKey;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.domain.configuration.SetupComponentVerificationCardKeystoresPayload;
import ch.post.it.evoting.securedatamanager.commons.ChunkedRequestBodyCreator;
import ch.post.it.evoting.securedatamanager.services.application.service.KeyStoreService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.RestClientService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.clients.VoteVerificationClient;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Retrofit;

@Repository
public class SetupComponentVerificationCardKeystoresPayloadUploadRepository {
	private final KeyStoreService keystoreService;
	private final ObjectMapper objectMapper;

	@Value("${voting.portal.enabled}")
	private boolean isVotingPortalEnabled;

	@Value("${VV_URL}")
	private String voteVerificationUrl;

	public SetupComponentVerificationCardKeystoresPayloadUploadRepository(final KeyStoreService keystoreService, final ObjectMapper objectMapper) {
		this.keystoreService = keystoreService;
		this.objectMapper = objectMapper;
	}

	/**
	 * Uploads the setup component verification card keystores payload to the vote verification.
	 *
	 * @param setupComponentVerificationCardKeystoresPayload the {@link SetupComponentVerificationCardKeystoresPayload} to upload. Must be non-null.
	 * @throws NullPointerException if the input is null.
	 */
	public void upload(final SetupComponentVerificationCardKeystoresPayload setupComponentVerificationCardKeystoresPayload) {

		checkNotNull(setupComponentVerificationCardKeystoresPayload);

		checkState(isVotingPortalEnabled, "The voting-portal connection is not enabled.");

		final String electionEventId = setupComponentVerificationCardKeystoresPayload.getElectionEventId();
		final String verificationCardSetId = setupComponentVerificationCardKeystoresPayload.getVerificationCardSetId();
		final RequestBody requestBody;
		try {
			requestBody = ChunkedRequestBodyCreator.forJsonPayload(objectMapper.writeValueAsBytes(setupComponentVerificationCardKeystoresPayload));
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(
					String.format(
							"Failed to serialize setupComponentVerificationCardKeystoresPayload. [electionEventId: %s, verificationCardSetId: %s]",
							electionEventId, verificationCardSetId), e);
		}

		final Response<ResponseBody> response;
		try {
			response = getVoteVerificationClient().uploadSetupComponentVerificationCardKeystores(electionEventId, verificationCardSetId, requestBody)
					.execute();
		} catch (final IOException e) {
			throw new UncheckedIOException("Failed to communicate with vote verification.", e);
		}

		checkState(response.isSuccessful(),
				"Request for uploading setup component verification card keystores payload failed. [electionEventId: %s, verificationCardSetId: %s]",
				electionEventId, verificationCardSetId);
	}

	/**
	 * Gets the vote verification Retrofit client
	 */
	private VoteVerificationClient getVoteVerificationClient() {
		final PrivateKey privateKey = keystoreService.getPrivateKey();
		final Retrofit restAdapter = RestClientService.getInstance()
				.getRestClientWithInterceptorAndJacksonConverter(voteVerificationUrl, privateKey, "SECURE_DATA_MANAGER");
		return restAdapter.create(VoteVerificationClient.class);
	}
}
