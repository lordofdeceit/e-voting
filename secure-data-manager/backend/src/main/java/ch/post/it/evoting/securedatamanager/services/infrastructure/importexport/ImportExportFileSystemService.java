/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.importexport;

import static ch.post.it.evoting.securedatamanager.services.infrastructure.importexport.WhiteList.MAX_DEPTH;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

/**
 * This class import and export the SDM files needed to other SDM instances.
 * <p/>
 * The import will load all elections present on the given path, while the export will only export a single election.
 * <p/>
 * The needed files are defined by the {@link WhiteList} class.
 */
@Service
public class ImportExportFileSystemService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ImportExportFileSystemService.class);
	private static final CopyOption[] COPY_OPTIONS = { LinkOption.NOFOLLOW_LINKS, StandardCopyOption.REPLACE_EXISTING,
			StandardCopyOption.COPY_ATTRIBUTES };

	private final PathResolver pathResolver;

	public ImportExportFileSystemService(final PathResolver pathResolver) {
		this.pathResolver = pathResolver;
	}

	/**
	 * Import the SDM file system according the whitelist.
	 *
	 * @param usbDirectory to import.
	 */
	public void importFileSystem(final Path usbDirectory) {
		checkNotNull(usbDirectory);
		checkArgument(Files.isDirectory(usbDirectory), "usbDirectory is not a directory. [%s]", usbDirectory);

		final Path localSdmDirectory = pathResolver.resolveSdmPath();

		checkArgument(Files.isDirectory(localSdmDirectory), "localSdmDirectory is not a directory. [%s]", localSdmDirectory);

		WhiteList.getImportList().stream()
				.flatMap(pattern -> getEligibleFiles(usbDirectory, pattern).stream())
				.forEach(file -> copyFileIfNotExist().accept(usbDirectory.resolve(file), localSdmDirectory.resolve(file)));
	}

	private static List<Path> getEligibleFiles(final Path directory, final Pattern pattern) {
		checkNotNull(directory);
		checkNotNull(pattern);

		LOGGER.info("Import files. [pattern: {}]", pattern);

		try (final Stream<Path> paths = Files.find(directory, MAX_DEPTH, (path, basicFileAttributes) ->
				pattern.matcher(FilenameUtils.separatorsToUnix(directory.relativize(path).toString())).matches())) {

			return paths.map(directory::relativize).toList();

		} catch (IOException e) {
			throw new UncheckedIOException("Cannot retrieve the list of file to import.", e);
		}
	}

	/**
	 * Export the SDM file system according the whitelist.
	 *
	 * @param electionEventId to export.
	 * @param usbDirectory    where export.
	 */
	public void exportFileSystem(final String electionEventId, final Path usbDirectory) {
		checkNotNull(electionEventId);
		checkNotNull(usbDirectory);
		checkArgument(Files.isDirectory(usbDirectory), "usbDirectory is not a directory. [%s]", usbDirectory);

		final Path localSdmDirectory = pathResolver.resolveSdmPath();

		checkArgument(Files.isDirectory(localSdmDirectory), "localSdmDirectory is not a directory. [%s]", localSdmDirectory);

		WhiteList.getExportList(electionEventId).stream()
				.flatMap(pattern -> getEligibleFiles(localSdmDirectory, pattern).stream())
				.forEach(file -> copyFile().accept(localSdmDirectory.resolve(file), usbDirectory.resolve(file)));
	}

	private static BiConsumer<Path, Path> copyFileIfNotExist() {
		return (Path source, Path target) -> {
			if (Files.notExists(target)) {
				copyFile().accept(source, target);
			} else {
				LOGGER.debug("Do not copy file, the file already exists and override is disabled. [source:{}, target:{}]",
						Files.exists(source), Files.exists(target));
			}
		};
	}

	private static BiConsumer<Path, Path> copyFile() {
		return (Path source, Path target) -> {
			try {
				if (Files.isSymbolicLink(source)) {
					throw new IllegalStateException(format(
							"There is a symbolic link in the SDM database. Aborting copy. [symbolic-link:%s]", source));
				}
				Files.createDirectories(target.getParent());
				Files.copy(source, target, COPY_OPTIONS);
			} catch (IOException e) {
				throw new UncheckedIOException(e);
			}
		};
	}
}
