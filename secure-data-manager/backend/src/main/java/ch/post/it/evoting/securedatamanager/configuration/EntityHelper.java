/*
 * (c) Copyright 2022 Swiss Post Ltd
 */
package ch.post.it.evoting.securedatamanager.configuration;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.BallotConfigService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballotbox.BallotBoxRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

/**
 * Service to operate with ballot boxes.
 */
@Service
@ConditionalOnProperty("role.isConfig")
public class EntityHelper {

	private final BallotConfigService ballotConfigService;
	private final BallotBoxRepository ballotBoxRepository;
	private final VotingCardSetRepository votingCardSetRepository;

	public EntityHelper(
			final BallotConfigService ballotConfigService,
			final BallotBoxRepository ballotBoxRepository,
			final VotingCardSetRepository votingCardSetRepository) {
		this.ballotConfigService = ballotConfigService;
		this.ballotBoxRepository = ballotBoxRepository;
		this.votingCardSetRepository = votingCardSetRepository;
	}

	public Ballot getBallotFromVotingCardSet(final String electionEventId, final String votingCardSetId) {
		final String ballotBoxId = votingCardSetRepository.getBallotBoxId(votingCardSetId);
		final String ballotId = ballotBoxRepository.getBallotId(ballotBoxId);
		return ballotConfigService.getBallot(electionEventId, ballotId);
	}

}
