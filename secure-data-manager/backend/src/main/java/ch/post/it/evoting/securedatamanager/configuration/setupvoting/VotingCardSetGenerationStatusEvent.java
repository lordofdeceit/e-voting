/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

public class VotingCardSetGenerationStatusEvent {

	private final String electionEventId;
	private final String votingCardSetId;
	private final GenerationType generationType;

	public VotingCardSetGenerationStatusEvent(final String electionEventId, final String votingCardSetId, final GenerationType generationType) {
		this.electionEventId = validateUUID(electionEventId);
		this.votingCardSetId = validateUUID(votingCardSetId);
		this.generationType = checkNotNull(generationType);
	}

	public String getElectionEventId() {
		return electionEventId;
	}

	public String getVotingCardSetId() {
		return votingCardSetId;
	}

	public GenerationType getType() {
		return generationType;
	}

	public static class Builder {
		private String electionEventId;
		private String votingCardSetId;
		private GenerationType generationType;

		public Builder setElectionEventId(final String electionEventId) {
			this.electionEventId = electionEventId;
			return this;
		}

		public Builder setVotingCardSetId(final String votingCardSetId) {
			this.votingCardSetId = votingCardSetId;
			return this;
		}

		public Builder setGenerationType(final GenerationType generationType) {
			this.generationType = generationType;
			return this;
		}

		public VotingCardSetGenerationStatusEvent build() {
			return new VotingCardSetGenerationStatusEvent(electionEventId, votingCardSetId, generationType);
		}
	}
}

