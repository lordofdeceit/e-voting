/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static java.nio.file.Files.delete;
import static java.nio.file.Files.newDirectoryStream;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.DirectoryStream.Filter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import javax.json.JsonObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.InvalidStatusTransitionException;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.Status;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.PayloadStorageException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.SetupComponentVerificationDataPayloadFileSystemRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.service.ConfigurationEntityStatusService;

/**
 * This is an application service that manages voting card sets.
 */
@Service
public class VotingCardSetDownloadService extends BaseVotingCardSetService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetDownloadService.class);

	private final IdleStatusService idleStatusService;
	private final EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesService;
	private final ConfigurationEntityStatusService configurationEntityStatusService;
	private final SetupComponentVerificationDataPayloadFileSystemRepository setupComponentVerificationDataPayloadFileSystemRepository;

	public VotingCardSetDownloadService(
			final IdleStatusService idleStatusService,
			final EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesService,
			final ConfigurationEntityStatusService configurationEntityStatusService,
			final SetupComponentVerificationDataPayloadFileSystemRepository setupComponentVerificationDataPayloadFileSystemRepository) {
		this.idleStatusService = idleStatusService;
		this.configurationEntityStatusService = configurationEntityStatusService;
		this.encryptedLongReturnCodeSharesService = encryptedLongReturnCodeSharesService;
		this.setupComponentVerificationDataPayloadFileSystemRepository = setupComponentVerificationDataPayloadFileSystemRepository;
	}

	/**
	 * Download the computed values for a votingCardSet
	 *
	 * @throws InvalidStatusTransitionException if the original status does not allow the download
	 */
	public void download(final String votingCardSetId, final String electionEventId)
			throws ResourceNotFoundException, InvalidStatusTransitionException, IOException {

		LOGGER.info("Downloading the computed values. [electionEventId: {}, votingCardSetId: {}]", electionEventId, votingCardSetId);

		if (!idleStatusService.getIdLock(votingCardSetId)) {
			return;
		}

		try {
			final Status fromStatus = Status.COMPUTED;
			final Status toStatus = Status.VCS_DOWNLOADED;

			checkVotingCardSetStatusTransition(electionEventId, votingCardSetId, fromStatus, toStatus);

			final JsonObject votingCardSetJson = votingCardSetRepository.getVotingCardSetJson(electionEventId, votingCardSetId);
			final String verificationCardSetId = votingCardSetJson.getString(JsonConstants.VERIFICATION_CARD_SET_ID);

			deleteNodeContributions(electionEventId, verificationCardSetId);

			final int chunkCount;
			try {
				chunkCount = setupComponentVerificationDataPayloadFileSystemRepository.getCount(electionEventId, verificationCardSetId);
			} catch (final PayloadStorageException e) {
				throw new IllegalStateException("Failed to get the chunk count.", e);
			}

			for (int chunkId = 0; chunkId < chunkCount; chunkId++) {
				List<ControlComponentCodeSharesPayload> contributions = encryptedLongReturnCodeSharesService.downloadGenEncLongCodeShares(electionEventId,
						verificationCardSetId, chunkId);
				writeNodeContributions(electionEventId, verificationCardSetId, chunkId, contributions);
			}

			configurationEntityStatusService.update(toStatus.name(), votingCardSetId, votingCardSetRepository);

		} finally {
			idleStatusService.freeIdLock(votingCardSetId);
		}

	}

	private static boolean isNodeContributions(final Path file) {
		final String name = file.getFileName().toString();
		return name.startsWith(Constants.CONFIG_FILE_CONTROL_COMPONENT_CODE_SHARES_PAYLOAD) && name.endsWith(Constants.JSON);
	}

	private void deleteNodeContributions(final String electionEventId, final String verificationCardSetId) throws IOException {
		final Path folder = pathResolver.resolve(Constants.CONFIG_FILES_BASE_DIR).resolve(electionEventId).resolve(Constants.CONFIG_DIR_NAME_ONLINE)
				.resolve(Constants.CONFIG_DIR_NAME_VOTEVERIFICATION).resolve(verificationCardSetId);
		final Filter<Path> filter = VotingCardSetDownloadService::isNodeContributions;
		try (final DirectoryStream<Path> files = newDirectoryStream(folder, filter)) {
			for (final Path file : files) {
				delete(file);
			}
		}
	}

	private void writeNodeContributions(final String electionEventId, final String verificationCardSetId, final int chunkId,
			final List<ControlComponentCodeSharesPayload> contributions)
			throws IOException {

		final String fileName = Constants.CONFIG_FILE_CONTROL_COMPONENT_CODE_SHARES_PAYLOAD + "." + chunkId + Constants.JSON;
		final Path path = pathResolver.resolve(Constants.CONFIG_FILES_BASE_DIR).resolve(electionEventId).resolve(Constants.CONFIG_DIR_NAME_ONLINE)
				.resolve(Constants.CONFIG_DIR_NAME_VOTEVERIFICATION).resolve(verificationCardSetId).resolve(fileName);

		byte[] bytes = DomainObjectMapper.getNewInstance().writeValueAsBytes(contributions);
		Files.write(path, bytes);
	}
}
