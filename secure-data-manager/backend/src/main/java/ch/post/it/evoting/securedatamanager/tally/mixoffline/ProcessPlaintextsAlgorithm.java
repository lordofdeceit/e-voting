/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.mixoffline;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamal;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientMessage;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.securedatamanager.tally.DecodeVotingOptionsAlgorithm;

/**
 * Implements the ProcessPlaintexts algorithm.
 */
@Service
public final class ProcessPlaintextsAlgorithm {

	private final ElGamal elGamal;
	private final FactorizeService factorizeService;
	private final DecodeWriteInsAlgorithm decodeWriteInsAlgorithm;
	private final DecodeVotingOptionsAlgorithm decodeVotingOptionsAlgorithm;

	public ProcessPlaintextsAlgorithm(
			final ElGamal elGamal,
			final FactorizeService factorizeService,
			final DecodeWriteInsAlgorithm decodeWriteInsAlgorithm,
			final DecodeVotingOptionsAlgorithm decodeVotingOptionsAlgorithm) {
		this.elGamal = elGamal;
		this.factorizeService = factorizeService;
		this.decodeWriteInsAlgorithm = decodeWriteInsAlgorithm;
		this.decodeVotingOptionsAlgorithm = decodeVotingOptionsAlgorithm;
	}

	/**
	 * Factorizes each plaintext vote to retrieve the voter's encoded voting options and decodes them to the actual voting options.
	 *
	 * @param context the context data. Must be non-null.
	 * @param input   the input data. Must be non-null.
	 * @return (L < sub > votes < / sub >, L < sub > decodedVotes < / sub >) - the list of all selected encoded voting options and the list of all
	 * selected decoded voting options as a {@link ProcessPlaintextsAlgorithmOutput}.
	 * @throws NullPointerException     if any of the inputs is null.
	 * @throws IllegalArgumentException if
	 *                                  <ul>
	 *                                      <li>the number of selectable voting options is strictly smaller than 1.</li>
	 *                                      <li>the number of selectable voting options is strictly greater than 120.</li>
	 *                                      <li>the number of allowed write-ins + 1 is smaller or equal to 0.</li>
	 *                                      <li>the number of allowed write-ins + 1 is strictly greater to the number of elements in the decrypted votes.</li>
	 *                                      <li>the number of allowed write-ins is not equal to the number of the elements in the write-in voting options.</li>
	 *                                      <li>the plaintexts votes group or the primes mapping table group or the write-in voting options group
	 *                                      are not equal to the context group.</li>
	 *                                  </ul>
	 */
	@SuppressWarnings("java:S117")
	public ProcessPlaintextsAlgorithmOutput processPlaintexts(final ProcessPlaintextsContext context,
			final ProcessPlaintextsInput input) {
		checkNotNull(context);
		checkNotNull(input);

		// Cross-group check
		checkArgument(context.encryptionGroup().equals(input.getPlaintextVotes().getGroup()),
				"The group of the plaintext votes must be equal to the context group.");

		// Context
		final GqGroup group = context.encryptionGroup();
		final PrimesMappingTable pTable = context.primesMappingTable();
		final GroupVector<PrimeGqElement, GqGroup> p_tilde = pTable.getPTable().stream()
				.map(PrimesMappingTableEntry::encodedVotingOption)
				.collect(GroupVector.toGroupVector());

		// Input
		final GroupVector<ElGamalMultiRecipientMessage, GqGroup> m = input.getPlaintextVotes();
		final GroupVector<PrimeGqElement, GqGroup> p_w_tilde = input.getWriteInVotingOptions();
		final int psi = input.getNumberOfSelectableVotingOptions();
		final int delta_hat = input.getNumberOfAllowedWriteInsPlusOne();
		checkArgument(psi >= 1, "The number of selectable voting options must be greater or equal to 1. [psi: %s]", psi);
		checkArgument(psi <= 120, "The number of selectable voting options must be smaller or equal to 120. [psi: %s]", psi);
		checkArgument(delta_hat > 0, "The number of allowed write-ins + 1 must be strictly greater than 0. [delta_hat: %s]", delta_hat);
		checkArgument(p_w_tilde.size() == delta_hat - 1,
				"The size of the write-in voting options must be equal to delta_hat minus 1. [delta_hat: %s, size: %s]",
				delta_hat, p_w_tilde.size());
		checkArgument(p_w_tilde.isEmpty() || p_w_tilde.getGroup().equals(group),
				"The group of write-in voting options must be equal to the context group.");

		// Requires
		final int l = m.getElementSize();
		final int N_c_hat = m.size();
		checkArgument(delta_hat <= l,
				"The number of allowed write-ins + 1 must be smaller or equal to the number of elements in the decrypted votes. [delta_hat: %s, l: %s]",
				delta_hat, l);
		checkArgument(N_c_hat >= 2, "There must be at least two mixed votes. [N_c_hat: %s]", N_c_hat);

		// Operation
		final ElGamalMultiRecipientMessage one_vector = elGamal.ones(group, delta_hat);

		record Selected(GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions, List<String> decodedVotingOptions,
						List<String> decodedWriteInVotes) {
		}

		final List<Selected> selectedVotingOptionsList = m.stream()
				.filter(m_i -> !m_i.equals(one_vector))
				.map(m_i -> {
					final GqElement phi_i_0 = m_i.get(0);
					final GroupVector<PrimeGqElement, GqGroup> p_k_hat = factorizeService.factorize(phi_i_0, p_tilde, psi);
					final List<String> v_k_hat = decodeVotingOptionsAlgorithm.decodeVotingOptions(p_k_hat, pTable);

					final GroupVector<GqElement, GqGroup> w_k = m_i.getElements().subVector(1, l);
					final List<String> s_k_hat = decodeWriteInsAlgorithm.decodeWriteIns(new DecodeWriteInsAlgorithmInput.Builder()
							.setWriteInVotingOptions(p_w_tilde)
							.setSelectedEncodedVotingOptions(p_k_hat)
							.setEncodedWriteIns(w_k)
							.build());

					return new Selected(p_k_hat, v_k_hat, s_k_hat);
				}).toList();

		final GroupVector<GroupVector<PrimeGqElement, GqGroup>, GqGroup> L_votes = selectedVotingOptionsList.stream()
				.map(Selected::encodedVotingOptions)
				.collect(GroupVector.toGroupVector());
		final List<List<String>> L_decodedVotes = selectedVotingOptionsList.stream()
				.map(Selected::decodedVotingOptions)
				.toList();
		final List<List<String>> L_writeIns = selectedVotingOptionsList.stream()
				.map(Selected::decodedWriteInVotes)
				.toList();

		return new ProcessPlaintextsAlgorithmOutput(L_votes, L_decodedVotes, L_writeIns);
	}
}
