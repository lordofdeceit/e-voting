/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

/**
 * Allows performing operations with the start voting key . The start voting key is persisted/retrieved to/from the file system of the SDM, in its
 * workspace.
 */
@Service
public class StartVotingKeyFileRepository {
	private static final Logger LOGGER = LoggerFactory.getLogger(StartVotingKeyFileRepository.class);

	final PathResolver pathResolver;

	public StartVotingKeyFileRepository(final PathResolver pathResolver) {
		this.pathResolver = pathResolver;
	}

	/**
	 * Saves a start voting key for the given {@code electionEventId}, {@code verificationCardSetId}, {@code verificationCardId}.
	 *
	 * @param electionEventId       the election event id. Must be non-null and a valid UUID.
	 * @param verificationCardSetId the verification card set id. Must be non-null and a valid UUID.
	 * @param verificationCardId    the verification card id. Must be non-null and a valid UUID.
	 * @param value                 the value of start voting key to save. Must be non-null.
	 * @throws FailedValidationException if any of {@code electionEventId}, {@code verificationCardSetId}, {@code verificationCardId} are invalid.
	 * @throws NullPointerException      if any of {@code electionEventId}, {@code verificationCardSetId}, {@code verificationCardId}, {@code value}
	 *                                   is null.
	 */
	public void save(final String electionEventId, final String verificationCardSetId, final String verificationCardId, final String value) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		validateUUID(verificationCardId);
		checkNotNull(value);

		final Path startVotingKey = getFilePath(electionEventId, verificationCardSetId, verificationCardId);

		try {
			Files.writeString(startVotingKey, value);
		} catch (final IOException e) {
			throw new UncheckedIOException(String.format(
					"An error occurred while saving the start voting key. [electionEventId %s, verificationCardSetId %s, verificationCardId %s]",
					electionEventId, verificationCardSetId, verificationCardId), e);
		}
	}

	/**
	 * Checks if a start voting key exist for the given {@code electionEventId}, {@code verificationCardSetId}, {@code verificationCardId}.
	 *
	 * @param electionEventId       the election event id. Must be non-null and a valid UUID.
	 * @param verificationCardSetId the verification card set id. Must be non-null and a valid UUID.
	 * @param verificationCardId    the verification card id. Must be non-null and a valid UUID.
	 * @return {@code true} if the start voting key is present, {@code false} otherwise.
	 * @throws FailedValidationException if any of {@code electionEventId}, {@code verificationCardSetId}, {@code verificationCardId} are invalid.
	 * @throws NullPointerException      if any of {@code electionEventId}, {@code verificationCardSetId}, {@code verificationCardId} is null.
	 */
	public boolean existsById(final String electionEventId, final String verificationCardSetId, final String verificationCardId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		validateUUID(verificationCardId);

		final Path startVotingKey = getFilePath(electionEventId, verificationCardSetId, verificationCardId);

		return Files.exists(startVotingKey);
	}

	/**
	 * Finds the start voting key by the given {@code electionEventId}, {@code verificationCardSetId}, {@code verificationCardId}.
	 *
	 * @param electionEventId       the election event id. Must be non-null and a valid UUID.
	 * @param verificationCardSetId the verification card set id. Must be non-null and a valid UUID.
	 * @param verificationCardId    the verification card id. Must be non-null and a valid UUID.
	 * @return the start voting key for this {@code electionEventId}, {@code verificationCardSetId}, {@code verificationCardId}.
	 * @throws FailedValidationException if any of {@code electionEventId}, {@code verificationCardSetId}, {@code verificationCardId} are invalid.
	 * @throws NullPointerException      if any of {@code electionEventId}, {@code verificationCardSetId}, {@code verificationCardId} is null.
	 */
	public Optional<String> findById(final String electionEventId, final String verificationCardSetId, final String verificationCardId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		validateUUID(verificationCardId);

		final Path startVotingKey = getFilePath(electionEventId, verificationCardSetId, verificationCardId);

		if (!Files.exists(startVotingKey)) {
			LOGGER.debug("Requested start voting key does not exist. [electionEventId {}, verificationCardSetId {}, verificationCardId {}]",
					electionEventId, verificationCardSetId, verificationCardId);
			return Optional.empty();
		}

		try {
			return Optional.of(Files.readString(startVotingKey));
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format(
							"An error occurred while retrieving the start voting key. [path: %s, electionEventId %s, verificationCardSetId %s, verificationCardId %s]",
							startVotingKey, electionEventId, verificationCardSetId, verificationCardId), e);
		}
	}

	private Path getFilePath(final String electionEventId, final String verificationCardSetId, final String verificationCardId) {
		return pathResolver
				.resolveOfflinePath(electionEventId)
				.resolve(Constants.CONFIG_START_VOTING_KEYS_DIRECTORY)
				.resolve(verificationCardSetId)
				.resolve(verificationCardId + Constants.KEY);
	}
}
