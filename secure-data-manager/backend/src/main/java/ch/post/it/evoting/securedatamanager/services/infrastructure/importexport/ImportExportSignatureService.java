/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.importexport;

import static java.lang.String.format;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

import org.bouncycastle.cms.CMSException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptolib.api.exceptions.GeneralCryptoLibException;
import ch.post.it.evoting.cryptolib.api.stores.StoresServiceAPI;
import ch.post.it.evoting.cryptolib.api.stores.bean.KeyStoreType;
import ch.post.it.evoting.cryptolib.cmssigner.CMSSigner;
import ch.post.it.evoting.cryptolib.stores.service.StoresService;
import ch.post.it.evoting.securedatamanager.services.application.service.SignaturesVerifierService;

@Service
public class ImportExportSignatureService {

	private final StoresServiceAPI storesService;
	private final SignaturesVerifierService signaturesVerifierService;
	private final Path keyStoreOnlinePath;

	public ImportExportSignatureService(final SignaturesVerifierService signaturesVerifierService,
			@Value("${import.export.zip.keyStore.online.path}")
			final String keyStoreOnlinePath) {
		this.storesService = new StoresService();
		this.signaturesVerifierService = signaturesVerifierService;
		this.keyStoreOnlinePath = Path.of(keyStoreOnlinePath);
	}

	/**
	 * Signs the give file with the CMS PKCS7 standard. The signature file is generated aside the original file and with the same name plus .p7
	 * extension
	 */
	public void signFile(final Path fileToSign, final char[] keystorePassword) {
		try {
			final KeyStore keyStore = getOnlineKeyStore(keystorePassword);
			final Enumeration<String> keyAliases = keyStore.aliases();
			final String keyAlias = keyAliases.nextElement();
			if (keyAliases.hasMoreElements()) {
				throw new IllegalArgumentException("There should be exactly one private key in the keystore");
			}
			final PrivateKey signingKey = (PrivateKey) keyStore.getKey(keyAlias, keystorePassword);
			Arrays.fill(keystorePassword, '0');
			final List<java.security.cert.Certificate> chainAsList = new ArrayList<>(Arrays.asList(keyStore.getCertificateChain(keyAlias)));
			final java.security.cert.Certificate signerCert = chainAsList.remove(0);

			final Path signedFilePath = fileToSign.resolveSibling(fileToSign.getFileName() + CMSSigner.SIGNATURE_FILE_EXTENSION);
			CMSSigner.sign(fileToSign.toFile(), signedFilePath.toFile(), signerCert, chainAsList, signingKey);
		} catch (IOException | CMSException | GeneralCryptoLibException | KeyStoreException | NoSuchAlgorithmException |
				 UnrecoverableKeyException e) {
			throw new IllegalStateException(format("Signature of the file fails. [file: %s]", fileToSign), e);
		}
	}

	private KeyStore getOnlineKeyStore(final char[] password) throws IOException, GeneralCryptoLibException {
		try (final InputStream in = new FileInputStream(keyStoreOnlinePath.toFile())) {
			return storesService.loadKeyStore(KeyStoreType.PKCS12, in, password);
		}
	}

	/**
	 * Verify the given file. Require a signature file named like the file to verify with .p7 extension.
	 */
	public void verifyFile(final Path fileToVerify) {
		Path signatureFile = fileToVerify.getParent().resolve(fileToVerify.getFileName() + ".p7");
		try {
			signaturesVerifierService.verifyPkcs7(fileToVerify, signatureFile);
		} catch (IOException | CMSException | GeneralCryptoLibException | CertificateException e) {
			throw new IllegalStateException(format("Verification of signature fail. [file: %s, signature: %s]", fileToVerify, signatureFile), e);
		}
	}
}
