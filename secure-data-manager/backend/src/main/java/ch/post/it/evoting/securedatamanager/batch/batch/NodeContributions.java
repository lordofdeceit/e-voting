/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.batch.batch;

import java.util.List;

import ch.post.it.evoting.cryptoprimitives.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationDataPayload;

public class NodeContributions {

	private List<ControlComponentCodeSharesPayload> nodeContributionResponse;

	private SetupComponentVerificationDataPayload nodeContributionRequest;

	public NodeContributions(final List<ControlComponentCodeSharesPayload> nodeContributionResponse,
			final SetupComponentVerificationDataPayload nodeContributionRequest) {
		this.nodeContributionResponse = nodeContributionResponse;
		this.nodeContributionRequest = nodeContributionRequest;
	}

	public List<ControlComponentCodeSharesPayload> getNodeContributionResponse() {
		return nodeContributionResponse;
	}

	public void setNodeContributionResponse(final List<ControlComponentCodeSharesPayload> nodeContributionResponse) {
		this.nodeContributionResponse = nodeContributionResponse;
	}

	public SetupComponentVerificationDataPayload getNodeContributionRequest() {
		return nodeContributionRequest;
	}

	public void setNodeContributionRequest(final SetupComponentVerificationDataPayload nodeContributionRequest) {
		this.nodeContributionRequest = nodeContributionRequest;
	}

}
