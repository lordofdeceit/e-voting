/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.VerificationCardSecretKey;

/**
 * Allows performing operations with the verification card secret key. The verification card secret key is persisted/retrieved to/from
 * the file system of the SDM, in its workspace.
 */
@Service
public class VerificationCardSecretKeyService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VerificationCardSecretKeyService.class);

	private final VerificationCardSecretKeyFileRepository verificationCardSecretKeyFileRepository;

	public VerificationCardSecretKeyService(
			final VerificationCardSecretKeyFileRepository verificationCardSecretKeyFileRepository) {
		this.verificationCardSecretKeyFileRepository = verificationCardSecretKeyFileRepository;
	}

	/**
	 * Saves the verification card secret key.
	 *
	 * @param verificationCardSecretKey the verification card secret key to save.
	 * @throws FailedValidationException if any of the ids are invalid.
	 * @throws NullPointerException      if any of {@code electionEventId}, {@code verificationCardSetId} or {@code value} is null.
	 * @throws IllegalStateException     if the verification card secret key cannot be saved.
	 */
	public void save(final VerificationCardSecretKey verificationCardSecretKey) {
		checkNotNull(verificationCardSecretKey);

		verificationCardSecretKeyFileRepository.save(verificationCardSecretKey);

		final String electionEventId = verificationCardSecretKey.electionEventId();
		final String verificationCardSetId = verificationCardSecretKey.verificationCardSetId();
		final String verificationCardId = verificationCardSecretKey.verificationCardId();

		LOGGER.info("Saved verification card secret key. [electionEventId: {}, verificationCardSetId: {}, verificationCardId: {}]",
				electionEventId, verificationCardSetId, verificationCardId);
	}

	/**
	 * Checks if the verification card secret key is present.
	 *
	 * @param electionEventId       the election event id to check. Must be non-null and a valid UUID.
	 * @param verificationCardSetId the verification card set id. Must be non-null and a valid UUID.
	 * @param verificationCardId    the verification card id. Must be non-null and a valid UUID.
	 * @return {@code true} if the verification card secret key is present, {@code false} otherwise.
	 * @throws NullPointerException      if any input is null.
	 * @throws FailedValidationException if any input is not a valid UUID.
	 */
	public boolean exist(final String electionEventId, final String verificationCardSetId, final String verificationCardId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		validateUUID(verificationCardId);

		return verificationCardSecretKeyFileRepository.existsById(electionEventId, verificationCardSetId, verificationCardId);
	}

	/**
	 * Loads the verification card secret key by the given ids.
	 *
	 * @param electionEventId       the payload's election event id. Must be non-null and a valid UUID.
	 * @param verificationCardSetId the payload's verification card set id. Must be non-null and a valid UUID.
	 * @param verificationCardId    the payload's verification card id. Must be non-null and a valid UUID.
	 * @return the verification card secret key for the given ids.
	 * @throws NullPointerException      if any input is null.
	 * @throws FailedValidationException if any input is not a valid UUID.
	 */
	public VerificationCardSecretKey load(final String electionEventId, final String verificationCardSetId, final String verificationCardId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		validateUUID(verificationCardId);

		final VerificationCardSecretKey verificationCardSecretKey =
				verificationCardSecretKeyFileRepository.findById(electionEventId, verificationCardSetId, verificationCardId)
						.orElseThrow(() -> new IllegalStateException(String.format(
								"Requested verification card secret key is not present. [electionEventId: %s, verificationCardSetId: %s, verificationCardId: %s]",
								electionEventId, verificationCardSetId, verificationCardId)));

		LOGGER.info("Loaded verification card secret key. [electionEventId: {}, verificationCardSetId: {}, verificationCardId: {}]",
				electionEventId, verificationCardSetId, verificationCardId);

		return verificationCardSecretKey;
	}
}
