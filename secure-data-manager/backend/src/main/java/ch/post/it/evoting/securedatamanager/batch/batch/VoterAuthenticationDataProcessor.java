/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.batch.batch;

import static ch.post.it.evoting.securedatamanager.config.commons.config.logevents.ConfigGeneratorLogEvents.GENCREDAT_ERROR_DERIVING_CREDENTIAL_ID;
import static ch.post.it.evoting.securedatamanager.config.commons.config.logevents.ConfigGeneratorLogEvents.GENCREDAT_SUCCESS_CREDENTIAL_ID_DERIVED;
import static ch.post.it.evoting.securedatamanager.config.commons.config.logevents.ConfigGeneratorLogEvents.GENSVPK_ERROR_DERIVING_KEYSTORE_SYMMETRIC_ENCRYPTION_KEY;
import static ch.post.it.evoting.securedatamanager.config.commons.config.logevents.ConfigGeneratorLogEvents.GENSVPK_ERROR_GENERATING_VCID;
import static ch.post.it.evoting.securedatamanager.config.commons.config.logevents.ConfigGeneratorLogEvents.GENSVPK_SUCCESS_KEYSTORE_SYMMETRIC_ENCRYPTION_KEY_DERIVED;
import static ch.post.it.evoting.securedatamanager.config.commons.config.logevents.ConfigGeneratorLogEvents.GENSVPK_SUCCESS_VCIDS_GENERATED;
import static java.util.Arrays.fill;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.common.annotations.VisibleForTesting;

import ch.post.it.evoting.cryptolib.api.derivation.CryptoAPIDerivedKey;
import ch.post.it.evoting.cryptolib.api.derivation.CryptoAPIPBKDFDeriver;
import ch.post.it.evoting.cryptolib.api.exceptions.GeneralCryptoLibException;
import ch.post.it.evoting.cryptolib.api.primitives.PrimitivesServiceAPI;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.domain.election.helpers.ReplacementsHolder;
import ch.post.it.evoting.securedatamanager.batch.batch.exceptions.GenerateCredentialIdException;
import ch.post.it.evoting.securedatamanager.batch.batch.exceptions.GenerateSVKVotingCardIdPassKeystoreException;
import ch.post.it.evoting.securedatamanager.batch.batch.exceptions.GenerateVotingcardIdException;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.commons.domain.VerificationCardIdValues;
import ch.post.it.evoting.securedatamanager.config.commons.config.exceptions.CreateVotingCardSetException;
import ch.post.it.evoting.securedatamanager.config.commons.config.model.authentication.ExtendedAuthInformation;
import ch.post.it.evoting.securedatamanager.config.commons.config.model.authentication.StartVotingKey;
import ch.post.it.evoting.securedatamanager.config.engine.actions.ExtendedAuthenticationService;
import ch.post.it.evoting.securedatamanager.config.engine.commands.voters.JobExecutionObjectContext;
import ch.post.it.evoting.securedatamanager.config.engine.commands.voters.VotersParametersHolder;
import ch.post.it.evoting.securedatamanager.config.engine.commands.voters.datapacks.beans.VotingCardCredentialDataPack;
import ch.post.it.evoting.securedatamanager.config.engine.commands.voters.datapacks.generators.VotingCardCredentialDataPackGenerator;
import ch.post.it.evoting.securedatamanager.configuration.StartVotingKeyService;

/**
 * Executes the batch part of the Voter authentication data process.
 */
@Service
@JobScope
public class VoterAuthenticationDataProcessor implements ItemProcessor<VerificationCardIdValues, GeneratedVotingCardOutput> {

	public static final String ERROR_MESSAGE_LOG = "{}. [electionEventId: {}, votingCardSetId: {}, voting card size: {}]";
	private static final Logger LOGGER = LoggerFactory.getLogger(VoterAuthenticationDataProcessor.class);
	private final Random random;
	private final VotingCardGenerationJobExecutionContext jobExecutionContext;
	private final String verificationCardSetId;
	private final String votingCardSetId;
	private final int numberOfVotingCards;
	private final CryptoAPIPBKDFDeriver pbkdfDeriver;
	private final VotersParametersHolder holder;
	private final ExtendedAuthenticationService extendedAuthenticationService;
	private final StartVotingKeyService startVotingKeyService;
	private final VotingCardCredentialDataPackGenerator votingCardCredentialDataPackGenerator;

	public VoterAuthenticationDataProcessor(
			@Qualifier("extendedAuthenticationServiceWithJobScope")
			final ExtendedAuthenticationService extendedAuthenticationService,
			final StartVotingKeyService startVotingKeyService,
			final VotingCardCredentialDataPackGenerator votingCardCredentialDataPackGenerator,
			final PrimitivesServiceAPI primitivesService,
			@Qualifier("jobExecutionObjectContext")
			final JobExecutionObjectContext objectContext,
			@Value("#{jobExecution}")
			final JobExecution jobExecution,
			final Random random) {
		this.extendedAuthenticationService = extendedAuthenticationService;
		this.startVotingKeyService = startVotingKeyService;
		this.votingCardCredentialDataPackGenerator = votingCardCredentialDataPackGenerator;
		this.pbkdfDeriver = primitivesService.getPBKDFDeriver();
		this.jobExecutionContext = new VotingCardGenerationJobExecutionContext(jobExecution.getExecutionContext());
		this.random = random;
		this.verificationCardSetId = jobExecutionContext.getVerificationCardSetId();
		this.votingCardSetId = jobExecutionContext.getVotingCardSetId();
		this.numberOfVotingCards = jobExecutionContext.getNumberOfVotingCards();
		this.holder = objectContext.get(jobExecutionContext.getJobInstanceId(), VotersParametersHolder.class);
	}

	@VisibleForTesting
	static String retrieveBallotCastingKey(final String verificationCardSetId, final String verificationCardId, final Path basePath) {

		LOGGER.info("Retrieving the ballot casting key for the base path {}, verificationCardSetId {} and verificationCardId {}.", basePath,
				verificationCardSetId, verificationCardId);

		final Path ballotCastingKeyPath = basePath.resolve(Constants.CONFIG_DIR_NAME_OFFLINE).resolve(Constants.CONFIG_BALLOT_CASTING_KEYS_DIRECTORY)
				.resolve(verificationCardSetId).resolve(verificationCardId + Constants.KEY);

		try {
			return Files.readString(ballotCastingKeyPath);
		} catch (final IOException e) {
			throw new IllegalStateException(
					String.format("Error retrieving the ballot casting key for the base path %s, verificationCardSetId %s and verificationCardId %s.",
							basePath, verificationCardSetId, verificationCardId), e);
		}
	}

	@Override
	public GeneratedVotingCardOutput process(final VerificationCardIdValues verificationCardIdValues) throws Exception {

		final String verificationCardId = verificationCardIdValues.getVerificationCardId();
		final String electionEventId = jobExecutionContext.getElectionEventId();
		final byte[] saltCredentialId = Base64.getDecoder().decode(jobExecutionContext.getSaltCredentialId());
		final byte[] saltKeystoreSymmetricEncryptionKey = Base64.getDecoder().decode(jobExecutionContext.getSaltKeystoreSymmetricEncryptionKey());

		LOGGER.debug("Generating voting and verification card... [electionEventId {}, verificationCardId {}]", electionEventId, verificationCardId);

		char[] keystoreSymmetricEncryptionKey = null;
		try {

			final String votingCardId = generateVotingCardId(electionEventId);
			final String ballotId = jobExecutionContext.getBallotId();
			final String ballotBoxId = jobExecutionContext.getBallotBoxId();
			final String startVotingKey = startVotingKeyService.load(electionEventId, verificationCardSetId, verificationCardId);
			final String credentialId = generateCredentialId(pbkdfDeriver, electionEventId, saltCredentialId, startVotingKey);

			keystoreSymmetricEncryptionKey = generateKeystoreSymmetricEncryptionKey(pbkdfDeriver, electionEventId, saltKeystoreSymmetricEncryptionKey,
					startVotingKey);

			final VotingCardCredentialDataPack voterCredentialDataPack = generateVotersCredentialDataPack(keystoreSymmetricEncryptionKey,
					credentialId);

			final String ballotCastingKey = retrieveBallotCastingKey(verificationCardSetId, verificationCardId, holder.getAbsoluteBasePath());

			final ExtendedAuthInformation extendedAuthInformation = getExtendedAuthInformation(electionEventId, startVotingKey);

			return GeneratedVotingCardOutput.success(votingCardId, votingCardSetId, ballotId, ballotBoxId, credentialId, electionEventId,
					verificationCardId, verificationCardSetId, startVotingKey, ballotCastingKey, voterCredentialDataPack, extendedAuthInformation);

		} finally {
			if (keystoreSymmetricEncryptionKey != null) {
				fill(keystoreSymmetricEncryptionKey, ' ');
			}
		}
	}

	private ExtendedAuthInformation getExtendedAuthInformation(final String electionEventId, final String startVotingKey) {
		// generates the authentication key and derives the ID
		return extendedAuthenticationService.create(StartVotingKey.ofValue(startVotingKey), electionEventId);
	}

	private VotingCardCredentialDataPack generateVotersCredentialDataPack(final char[] keystoreSymmetricEncryptionKey, final String credentialId) {
		final VotingCardCredentialDataPack voterCredentialDataPack;

		// create replacementHolder with eeid and credential ID
		final ReplacementsHolder replacementsHolder = new ReplacementsHolder(holder.getVotingCardCredentialInputDataPack().getEeid(), credentialId);

		try {
			voterCredentialDataPack = votingCardCredentialDataPackGenerator
					.generate(holder.getVotingCardCredentialInputDataPack(), replacementsHolder, keystoreSymmetricEncryptionKey, credentialId,
							holder.getVotingCardSetID(),
							holder.getCreateVotingCardSetCertificateProperties().getCredentialAuthCertificateProperties(),
							holder.getCredentialCACert(), holder.getElectionCACert());

		} catch (final GeneralCryptoLibException e) {
			throw new CreateVotingCardSetException("An error occurred while generating the voters credential data pack: " + e.getMessage(), e);
		}
		return voterCredentialDataPack;
	}

	private String generateVotingCardId(final String electionEventId) {
		final String votingCardId;

		try {
			votingCardId = random.genRandomBase16String(Constants.BASE16_ID_LENGTH).toLowerCase();
			LOGGER.debug("{}. [electionEventId: {}, votingCardSetId: {}, votingCardId: {}]", GENSVPK_SUCCESS_VCIDS_GENERATED.getInfo(),
					electionEventId, votingCardSetId, votingCardId);
		} catch (final Exception e) {
			LOGGER.error("{}. [electionEventId: {}, votingCardSetId: {}]", GENSVPK_ERROR_GENERATING_VCID.getInfo(), electionEventId, votingCardSetId);
			throw new GenerateVotingcardIdException(GENSVPK_ERROR_GENERATING_VCID.getInfo(), e);
		}

		return votingCardId;
	}

	private String generateCredentialId(final CryptoAPIPBKDFDeriver pbkdfDeriver, final String electionEventId, final byte[] salt,
			final String startVotingKey) {
		final String credentialId;
		try {
			credentialId = String.valueOf(getDerivedBytesInHEX(pbkdfDeriver, salt, startVotingKey));
		} catch (final Exception e) {
			LOGGER.error(ERROR_MESSAGE_LOG, GENCREDAT_ERROR_DERIVING_CREDENTIAL_ID.getInfo(),
					electionEventId, votingCardSetId, numberOfVotingCards);
			throw new GenerateCredentialIdException(GENCREDAT_ERROR_DERIVING_CREDENTIAL_ID.getInfo(), e);
		}
		LOGGER.debug("{}. [electionEventId: {}, votingCardSetId: {}, credentialId: {}]", GENCREDAT_SUCCESS_CREDENTIAL_ID_DERIVED.getInfo(),
				electionEventId, votingCardSetId, credentialId);

		return credentialId;
	}

	private char[] generateKeystoreSymmetricEncryptionKey(final CryptoAPIPBKDFDeriver pbkdfDeriver, final String electionEventId, final byte[] salt,
			final String startVotingKey) {
		final char[] keystoreSymmetricEncryptionKey;

		try {
			keystoreSymmetricEncryptionKey = getDerivedBytesInHEX(pbkdfDeriver, salt, startVotingKey);
		} catch (final Exception e) {
			// error deriving the keystore symmetric encryption key KSKey
			LOGGER.error(ERROR_MESSAGE_LOG,
					GENSVPK_ERROR_DERIVING_KEYSTORE_SYMMETRIC_ENCRYPTION_KEY.getInfo(), electionEventId, votingCardSetId, numberOfVotingCards);
			throw new GenerateSVKVotingCardIdPassKeystoreException(GENSVPK_ERROR_DERIVING_KEYSTORE_SYMMETRIC_ENCRYPTION_KEY.getInfo(), e);
		}

		// success - Keystore symmetric encryption key KSKey successfully derived
		LOGGER.debug(ERROR_MESSAGE_LOG,
				GENSVPK_SUCCESS_KEYSTORE_SYMMETRIC_ENCRYPTION_KEY_DERIVED.getInfo(), electionEventId, votingCardSetId, numberOfVotingCards);

		return keystoreSymmetricEncryptionKey;
	}

	private char[] getDerivedBytesInHEX(final CryptoAPIPBKDFDeriver derived, final byte[] salt, final String inputString)
			throws GeneralCryptoLibException {
		final CryptoAPIDerivedKey cryptoAPIDerivedKey = derived.deriveKey(inputString.toCharArray(), salt);
		final byte[] encoded = cryptoAPIDerivedKey.getEncoded();
		return Hex.encodeHex(encoded);
	}

}
