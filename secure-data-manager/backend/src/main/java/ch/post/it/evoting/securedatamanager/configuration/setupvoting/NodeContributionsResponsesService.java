/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;

/**
 * Service loading the chunk-wise contributions of all control component nodes. For performance reasons, the GenVerDat algorithm splits the entire
 * verification card set into smaller pieces (a process called chunking). In this service, we combine the chunks into a single data structure for the
 * entire verification card set.
 */
@Service
public class NodeContributionsResponsesService {

	private static final Logger LOGGER = LoggerFactory.getLogger(NodeContributionsResponsesService.class);

	private final NodeContributionsResponsesFileRepository nodeContributionsResponsesFileRepository;

	public NodeContributionsResponsesService(final NodeContributionsResponsesFileRepository nodeContributionsResponsesFileRepository) {
		this.nodeContributionsResponsesFileRepository = nodeContributionsResponsesFileRepository;
	}

	/**
	 * Loads all node contributions responses for the given {@code electionEventId} and {@code verificationCardSetId}. These node contributions
	 * responses are chunked in a list of node contributions.
	 *
	 * @param electionEventId       the node contributions responses' election event id.
	 * @param verificationCardSetId the node contributions responses' verification card set id.
	 * @return all node contributions responses {@code electionEventId} and {@code verificationCardSetId}.
	 * @throws FailedValidationException if {@code electionEventId} or {@code verificationCardSetId} is invalid.
	 */
	public List<List<ControlComponentCodeSharesPayload>> load(final String electionEventId,
			final String verificationCardSetId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);

		LOGGER.info("Loading all the node contributions. [electionEventId: {}, verificationCardSetId: {}]", electionEventId, verificationCardSetId);

		return nodeContributionsResponsesFileRepository.findAllOrderByChunkId(electionEventId, verificationCardSetId);
	}

}
