/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api.setup;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.securedatamanager.configuration.setupvoting.SetupComponentEvotingPrintService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/sdm-backend/operation")
@Api(value = "SDM Operations REST API for Config phase")
@ConditionalOnProperty("role.isConfig")
public class OperationsConfigController {

	private final SetupComponentEvotingPrintService setupComponentEvotingPrintService;

	public OperationsConfigController(final SetupComponentEvotingPrintService setupComponentEvotingPrintService) {
		this.setupComponentEvotingPrintService = setupComponentEvotingPrintService;
	}

	/**
	 * Generates the print file.
	 *
	 * @param electionEventId the election event id.
	 */
	@PostMapping(value = "/electionevent/{electionEventId}/print-files/generate")
	@ApiOperation(value = "Generate print files", notes = "Service to generate the print files.")
	public ResponseEntity<Void> generatePrintFiles(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electionEventId) {

		validateUUID(electionEventId);

		setupComponentEvotingPrintService.generate(electionEventId);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
