/*
 * (c) Copyright 2022 Swiss Post Ltd
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.configuration.VoterReturnCodesPayload;

/**
 * Allows to generate and persist {@link VoterReturnCodesPayload}.
 */
@Service
public class VoterReturnCodesPayloadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VoterReturnCodesPayloadService.class);

	private final VoterReturnCodesPayloadFileRepository voterReturnCodesPayloadFileRepository;

	public VoterReturnCodesPayloadService(final VoterReturnCodesPayloadFileRepository voterReturnCodesPayloadFileRepository) {
		this.voterReturnCodesPayloadFileRepository = voterReturnCodesPayloadFileRepository;
	}

	/**
	 * Persists a {@link VoterReturnCodesPayload}.
	 *
	 * @param payload         the payload to be saved. Must be non-null.
	 * @param votingCardSetId the voting card set id. Must be non-null and a valid UUID.
	 * @throws NullPointerException      if the {@code payload} or {@code votingCardSetId} is null.
	 * @throws FailedValidationException if the {@code votingCardSetId} format is not valid.
	 */
	public void save(final VoterReturnCodesPayload payload, final String votingCardSetId) {
		checkNotNull(payload);
		validateUUID(votingCardSetId);

		voterReturnCodesPayloadFileRepository.save(payload, votingCardSetId);
		LOGGER.info("Voter return codes payload successfully saved. [electionEventId: {}, votingCardSetId: {}]", payload.electionEventId(),
				votingCardSetId);
	}

	/**
	 * Loads the {@link VoterReturnCodesPayload} for the given the election event and voting card set.
	 *
	 * @param electionEventId, the election event id. Must be non-null and a valid UUID.
	 * @param votingCardSetId, the voting card set id. Must be non-null and a valid UUID.
	 * @return a {@link VoterReturnCodesPayload}.
	 * @throws FailedValidationException if {@code electionEventId} or {@code votingCardSetId} is invalid.
	 * @throws IllegalStateException     if the requested voter return codes payload is not present.
	 */
	public VoterReturnCodesPayload load(final String electionEventId, final String votingCardSetId) {
		validateUUID(electionEventId);
		validateUUID(votingCardSetId);

		return voterReturnCodesPayloadFileRepository.findByElectionEventIdAndVotingCardSetId(
						electionEventId, votingCardSetId)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Requested voter return codes payload is not present. [electionEventId: %s, votingCardSetId: %s]",
								electionEventId, votingCardSetId)));
	}

}
