/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.batch.batch.writers;

import java.nio.file.Path;

import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.LineAggregator;
import org.springframework.core.io.FileSystemResource;

import ch.post.it.evoting.securedatamanager.batch.batch.GeneratedVotingCardOutput;

public class StandardPrintingDataWriter extends FlatFileItemWriter<GeneratedVotingCardOutput> {

	public StandardPrintingDataWriter(final Path path) {

		setLineAggregator(lineAggregator());
		setTransactional(false);
		setAppendAllowed(false);
		setShouldDeleteIfExists(true);
		setResource(new FileSystemResource(path.toString()));
	}

	private LineAggregator<GeneratedVotingCardOutput> lineAggregator() {

		return item -> {
			final String verificationCardSetId = item.getVerificationCardSetId();
			final String votingCardId = item.getVotingCardId();
			final String verificationCardId = item.getVerificationCardId();
			final String electionEventId = item.getElectionEventId();
			final String ballotId = item.getBallotId();
			final String ballotCastingKey = item.getBallotCastingKey();
			final String startVotingKey = item.getStartVotingKey();

			return String.format("%s;%s;%s;%s;%s;%s;%s", verificationCardSetId, votingCardId, verificationCardId, electionEventId, ballotCastingKey,
					ballotId, startVotingKey);
		};
	}
}
