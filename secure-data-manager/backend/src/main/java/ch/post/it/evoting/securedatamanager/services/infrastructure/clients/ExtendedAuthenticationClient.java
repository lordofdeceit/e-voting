/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.clients;

import ch.post.it.evoting.domain.configuration.setupvoting.ExtendedAuthenticationDataPayload;
import ch.post.it.evoting.securedatamanager.commons.Constants;

import jakarta.validation.constraints.NotNull;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ExtendedAuthenticationClient {

	@POST("extendedauthentication/tenant/{tenantId}/electionevent/{electionEventId}/adminboard/{adminBoardId}")
	Call<ResponseBody> saveExtendedAuthenticationData(
			@Path(Constants.TENANT_ID)
					String tenantId,
			@Path(Constants.ELECTION_EVENT_ID)
					String electionEventId,
			@Path(Constants.ADMIN_BOARD_ID)
					String adminBoardId, @NotNull
			@Body
	ExtendedAuthenticationDataPayload extendedAuthenticationDataPayload);
}
