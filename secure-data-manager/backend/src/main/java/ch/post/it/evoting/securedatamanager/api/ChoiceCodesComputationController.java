/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.securedatamanager.services.application.service.EncryptedLongReturnCodeSharesService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Controller for checking the status of the choice codes computation.
 */
@RestController
@RequestMapping("/sdm-backend/choicecodes")
@Api(value = "Computed values REST API")
public class ChoiceCodesComputationController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChoiceCodesComputationController.class);

	private final EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesService;

	@Value("${voting.portal.enabled}")
	private boolean isVotingPortalEnabled;

	public ChoiceCodesComputationController(final EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesService) {
		this.encryptedLongReturnCodeSharesService = encryptedLongReturnCodeSharesService;
	}


	/**
	 * Check if computed choice codes are ready.
	 *
	 * @throws IOException if something fails uploading voting card sets or electoral authorities.
	 */
	@PostMapping(value = "/electionevent/{electionEventId}/status", produces = "application/json")
	@ResponseStatus(value = HttpStatus.OK)
	@ApiOperation(value = "Check if computed choice codes are ready and update its status", notes = "Service to check the status of computed choice codes.")
	public void updateChoiceCodesComputationStatus(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electionEventId) throws IOException {

		if (!isVotingPortalEnabled) {
			LOGGER.warn("The application is configured to not have connectivity to Voter Portal, check if this is the expected behavior");
			return;
		}

		validateUUID(electionEventId);

		encryptedLongReturnCodeSharesService.updateVotingCardSetsComputationStatus(electionEventId);
	}
}
