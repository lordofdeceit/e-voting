/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

public enum GenerationType {

	EXECUTION_TASK("EXECUTION_TASK"),
	SPRING_BATCH("SPRING_BATCH");

	private final String type;

	GenerationType(final String type) {
		this.type = type;
	}
}
