/*
 * (c) Copyright 2022 Swiss Post Ltd
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants.EMPTY_OBJECT;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import java.util.List;

import javax.json.JsonObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupMatrix;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.securedatamanager.SetupKeyPairService;
import ch.post.it.evoting.securedatamanager.config.commons.utils.JsonUtils;
import ch.post.it.evoting.securedatamanager.configuration.EncryptionParametersConfigService;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.application.service.ElectionEventService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballotbox.BallotBoxRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

@Service
@ConditionalOnProperty("role.isConfig")
public class VotingCardSetDataGenerationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetDataGenerationService.class);

	private final GenCMTableAlgorithm genCMTableAlgorithm;
	private final ElectionEventService electionEventService;
	private final BallotConfigService ballotConfigService;
	private final BallotBoxRepository ballotBoxRepository;
	private final SetupKeyPairService setupKeyPairService;
	private final VotingCardSetRepository votingCardSetRepository;
	private final EncryptionParametersConfigService encryptionParametersConfigService;
	private final CombineEncLongCodeSharesAlgorithm combineEncLongCodeSharesAlgorithm;
	private final EncryptedNodeLongReturnCodeSharesService encryptedNodeLongReturnCodeSharesService;

	public VotingCardSetDataGenerationService(
			final GenCMTableAlgorithm genCMTableAlgorithm,
			final ElectionEventService electionEventService,
			final BallotConfigService ballotConfigService,
			final BallotBoxRepository ballotBoxRepository,
			final SetupKeyPairService setupKeyPairService,
			final VotingCardSetRepository votingCardSetRepository,
			final EncryptionParametersConfigService encryptionParametersConfigService,
			final CombineEncLongCodeSharesAlgorithm combineEncLongCodeSharesAlgorithm,
			final EncryptedNodeLongReturnCodeSharesService encryptedNodeLongReturnCodeSharesService) {
		this.genCMTableAlgorithm = genCMTableAlgorithm;
		this.electionEventService = electionEventService;
		this.ballotConfigService = ballotConfigService;
		this.ballotBoxRepository = ballotBoxRepository;
		this.setupKeyPairService = setupKeyPairService;
		this.votingCardSetRepository = votingCardSetRepository;
		this.encryptionParametersConfigService = encryptionParametersConfigService;
		this.combineEncLongCodeSharesAlgorithm = combineEncLongCodeSharesAlgorithm;
		this.encryptedNodeLongReturnCodeSharesService = encryptedNodeLongReturnCodeSharesService;
	}

	/**
	 * Calls the CombineEncLongCodeShares and GenCMTable algorithms.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @param votingCardSetId the voting card set id. Must be non-null and a valid UUID.
	 * @return a {@link ReturnCodesGenerationOutput} containing outputs of the CombineEncLongCodeShares and GenCMTable algorithms.
	 * @throws NullPointerException      if {@code electionEventId} or {@code votingCardSetId} is null.
	 * @throws FailedValidationException if {@code electionEventId} or {@code votingCardSetId} is invalid.
	 */
	public ReturnCodesGenerationOutput generate(final String electionEventId, final String votingCardSetId) {
		validateUUID(electionEventId);
		validateUUID(votingCardSetId);
		checkArgument(electionEventService.exists(electionEventId), "The given election event ID does not exist. [electionEventId: %s]",
				electionEventId);
		checkArgument(votingCardSetRepository.exists(electionEventId, votingCardSetId),
				"The given voting card set ID does not belong to the given election event ID. [votingCardSetId: %s, electionEventId: %s]",
				votingCardSetId, electionEventId);

		final GqGroup encryptionGroup = encryptionParametersConfigService.loadEncryptionGroup(electionEventId);
		final String verificationCardSetId = getVerificationCardSetId(electionEventId, votingCardSetId);
		final String ballotId = getBallotId(votingCardSetId);
		final int numberOfVotingOptions = getNumberOfVotingOptions(electionEventId, ballotId);
		final int numberOfEligibleVoters = getNumberOfEligibleVoters(electionEventId, votingCardSetId);

		// Load the node contributions
		final EncryptedNodeLongReturnCodeShares encryptedNodeLongReturnCodeShares = encryptedNodeLongReturnCodeSharesService.load(electionEventId,
				verificationCardSetId);
		LOGGER.info("Encrypted long return code shares successfully loaded. [electionEventId: {}, votingCardSetId: {}, verificationCardSetId: {}]",
				electionEventId, votingCardSetId, verificationCardSetId);

		// Load Setup secret key
		final ElGamalMultiRecipientPrivateKey setupSecretKey = setupKeyPairService.load(electionEventId).getPrivateKey();

		// Prepare and call CombineEncLongCodeShares algorithm
		final CombineEncLongCodeSharesContext combineEncLongCodeSharesContext = new CombineEncLongCodeSharesContext.Builder()
				.setEncryptionGroup(encryptionGroup)
				.setElectionEventId(electionEventId)
				.setVerificationCardSetId(verificationCardSetId)
				.setSetupSecretKey(setupSecretKey)
				.setNumberOfVotingOptions(numberOfVotingOptions)
				.setNumberOfEligibleVoters(numberOfEligibleVoters)
				.build();
		final CombineEncLongCodeSharesInput combineEncLongCodeSharesInput = prepareCombineEncLongCodeSharesInput(
				encryptedNodeLongReturnCodeShares);
		final CombineEncLongCodeSharesOutput combineEncLongCodeSharesOutput = combineEncLongCodeSharesAlgorithm.combineEncLongCodeShares(
				combineEncLongCodeSharesContext, combineEncLongCodeSharesInput);
		LOGGER.info("Encrypted long return code shares successfully combined. [electionEventId: {}, votingCardSetId: {}, verificationCardSetId: {}]",
				electionEventId, votingCardSetId, verificationCardSetId);

		// Prepare and call genCMTable algorithm
		final GenCMTableContext genCMTableContext = new GenCMTableContext.Builder()
				.setEncryptionGroup(encryptionGroup)
				.setElectionEventId(electionEventId)
				.setBallotId(ballotId)
				.setVerificationCardSetId(verificationCardSetId)
				.setSetupSecretKey(setupSecretKey)
				.build();
		final GenCMTableInput genCMTableInput = new GenCMTableInput.Builder()
				.setVerificationCardIds(encryptedNodeLongReturnCodeShares.getVerificationCardIds())
				.setEncryptedPreChoiceReturnCodes(combineEncLongCodeSharesOutput.getEncryptedPreChoiceReturnCodesVector())
				.setPreVoteCastReturnCodes(GroupVector.from(combineEncLongCodeSharesOutput.getPreVoteCastReturnCodesVector()))
				.build();
		final GenCMTableOutput genCMTableOutput = genCMTableAlgorithm.genCMTable(genCMTableContext, genCMTableInput);
		LOGGER.info("Return codes mapping table successfully generated. [electionEventId: {}, votingCardSetId: {}, verificationCardSetId: {}]",
				electionEventId, votingCardSetId, verificationCardSetId);

		return new ReturnCodesGenerationOutput.Builder()
				.setElectionEventId(electionEventId)
				.setVerificationCardSetId(verificationCardSetId)
				.setVerificationCardIds(encryptedNodeLongReturnCodeShares.getVerificationCardIds())
				.setShortVoteCastReturnCodes(genCMTableOutput.getShortVoteCastReturnCodes())
				.setLongVoteCastReturnCodesAllowList(combineEncLongCodeSharesOutput.getLongVoteCastReturnCodesAllowList())
				.setReturnCodesMappingTable(genCMTableOutput.getReturnCodesMappingTable())
				.setShortChoiceReturnCodes(genCMTableOutput.getShortChoiceReturnCodes())
				.build();
	}

	private String getVerificationCardSetId(final String electionEventId, final String votingCardSetId) {
		final String votingCardSetAsJson = votingCardSetRepository.find(votingCardSetId);
		checkState(isNotEmpty(votingCardSetAsJson) && !EMPTY_OBJECT.equals(votingCardSetAsJson),
				"No voting card set found. [electionEventId: %s, votingCardSetId: %s]", electionEventId, votingCardSetId);

		final JsonObject votingCardSet = JsonUtils.getJsonObject(votingCardSetAsJson);
		return votingCardSet.getString(JsonConstants.VERIFICATION_CARD_SET_ID);
	}

	private String getBallotId(final String votingCardSetId) {
		final String ballotBoxId = votingCardSetRepository.getBallotBoxId(votingCardSetId);
		checkState(ballotBoxId != null, "No ballot box found for voting card set. [votingCardSetId: %s]", votingCardSetId);
		return ballotBoxRepository.getBallotId(ballotBoxId);
	}

	private int getNumberOfVotingOptions(final String electionEventId, final String ballotId) {
		final Ballot ballot = ballotConfigService.getBallot(electionEventId, ballotId);
		return new CombinedCorrectnessInformation(ballot).getTotalNumberOfVotingOptions();
	}

	private int getNumberOfEligibleVoters(final String electionEventId, final String votingCardSetId) {
		try {
			return votingCardSetRepository.getNumberOfVotingCards(electionEventId, votingCardSetId);
		} catch (final ResourceNotFoundException e) {
			throw new IllegalStateException(
					String.format("No voting card set found. [electionEventId: %s, votingCardSetId: %s]", electionEventId, votingCardSetId));
		}
	}

	private CombineEncLongCodeSharesInput prepareCombineEncLongCodeSharesInput(
			final EncryptedNodeLongReturnCodeShares encryptedNodeLongReturnCodeShares) {

		// Prepare the input matrix from the node return codes values
		final List<EncryptedSingleNodeLongReturnCodeShares> nodeReturnCodesValues = encryptedNodeLongReturnCodeShares.getNodeReturnCodesValues();

		final List<List<ElGamalMultiRecipientCiphertext>> partialChoiceReturnCodesColumns = nodeReturnCodesValues.stream()
				.map(EncryptedSingleNodeLongReturnCodeShares::getExponentiatedEncryptedPartialChoiceReturnCodes).toList();
		final GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> partialChoiceReturnCodesMatrix = GroupMatrix.fromColumns(
				partialChoiceReturnCodesColumns);

		final List<List<ElGamalMultiRecipientCiphertext>> confirmationKeysColumns = nodeReturnCodesValues.stream()
				.map(EncryptedSingleNodeLongReturnCodeShares::getExponentiatedEncryptedConfirmationKeys).toList();
		final GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> confirmationKeysMatrix = GroupMatrix.fromColumns(confirmationKeysColumns);

		return new CombineEncLongCodeSharesInput.Builder()
				.setExponentiatedEncryptedChoiceReturnCodesMatrix(partialChoiceReturnCodesMatrix)
				.setExponentiatedEncryptedConfirmationKeysMatrix(confirmationKeysMatrix)
				.setVerificationCardIds(encryptedNodeLongReturnCodeShares.getVerificationCardIds())
				.build();
	}

}
