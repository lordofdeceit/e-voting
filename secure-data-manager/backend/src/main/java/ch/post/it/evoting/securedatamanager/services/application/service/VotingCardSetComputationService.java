/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import java.security.SignatureException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.domain.configuration.setupvoting.ComputingStatus;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.InvalidStatusTransitionException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.PayloadStorageException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.SetupComponentVerificationDataPayloadFileSystemRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.service.ConfigurationEntityStatusService;

/**
 * This is an application service that deals with the computation of voting card data.
 */
@Service
public class VotingCardSetComputationService extends BaseVotingCardSetService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetComputationService.class);

	private final IdleStatusService idleStatusService;
	private final ConfigurationEntityStatusService configurationEntityStatusService;
	private final EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesService;
	private final SetupComponentVerificationDataPayloadFileSystemRepository setupComponentVerificationDataPayloadFileSystemRepository;

	public VotingCardSetComputationService(final IdleStatusService idleStatusService,
			final ConfigurationEntityStatusService configurationEntityStatusService,
			final EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesService,
			final SetupComponentVerificationDataPayloadFileSystemRepository setupComponentVerificationDataPayloadFileSystemRepository) {
		this.idleStatusService = idleStatusService;
		this.configurationEntityStatusService = configurationEntityStatusService;
		this.encryptedLongReturnCodeSharesService = encryptedLongReturnCodeSharesService;
		this.setupComponentVerificationDataPayloadFileSystemRepository = setupComponentVerificationDataPayloadFileSystemRepository;
	}

	/**
	 * Compute a voting card set.
	 *
	 * @param votingCardSetId the identifier of the voting card set
	 * @param electionEventId the identifier of the election event
	 * @throws InvalidStatusTransitionException if the original status does not allow computing
	 * @throws JsonProcessingException
	 * @throws PayloadStorageException          if the payload could not be store
	 * @throws SignatureException               if the payload signature could not be verified
	 */
	public void compute(final String votingCardSetId, final String electionEventId)
			throws  InvalidStatusTransitionException, PayloadStorageException, SignatureException {

		if (!idleStatusService.getIdLock(votingCardSetId)) {
			return;
		}

		LOGGER.info("Starting computation of voting card set {}...", votingCardSetId);

		try {

			validateUUID(votingCardSetId);
			validateUUID(electionEventId);

			final ComputingStatus toStatus = ComputingStatus.COMPUTING;

			final String verificationCardSetId = votingCardSetRepository.getVerificationCardSetId(votingCardSetId);

			final int chunkCount = setupComponentVerificationDataPayloadFileSystemRepository.getCount(electionEventId, verificationCardSetId);
			for (int chunkId = 0; chunkId < chunkCount; chunkId++) {
				// Retrieve the payload.
				final SetupComponentVerificationDataPayload payload = setupComponentVerificationDataPayloadFileSystemRepository.retrieve(
						electionEventId, verificationCardSetId, chunkId);

				// Send chunk for processing.
				encryptedLongReturnCodeSharesService.computeGenEncLongCodeShares(payload);
			}

			// All chunks have been sent, update status.
			configurationEntityStatusService.update(toStatus.name(), votingCardSetId, votingCardSetRepository);
			LOGGER.info("Computation of voting card set {} started", votingCardSetId);

		} finally {
			idleStatusService.freeIdLock(votingCardSetId);
		}
	}
}
