/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkState;

import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.returncodes.ControlComponentCodeShare;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationData;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.ChannelSecurityContextData;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.PayloadStorageException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.SetupComponentVerificationDataPayloadFileSystemRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

/**
 * Allows to combine the node contributions responses.
 */
@Service
@ConditionalOnProperty("role.isConfig")
public class EncryptedNodeLongReturnCodeSharesService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EncryptedNodeLongReturnCodeSharesService.class);

	private final NodeContributionsResponsesService nodeContributionsResponsesService;
	private final SignatureKeystore<Alias> signatureKeystoreService;

	private final VotingCardSetRepository votingCardSetRepository;

	private final SetupComponentVerificationDataPayloadFileSystemRepository setupComponentVerificationDataPayloadFileSystemRepository;

	public EncryptedNodeLongReturnCodeSharesService(
			final NodeContributionsResponsesService nodeContributionsResponsesService,
			@Qualifier("keystoreServiceSdmConfig")
			final SignatureKeystore<Alias> signatureKeystoreService,
			final VotingCardSetRepository votingCardSetRepository,
			final SetupComponentVerificationDataPayloadFileSystemRepository setupComponentVerificationDataPayloadFileSystemRepository) {
		this.nodeContributionsResponsesService = nodeContributionsResponsesService;
		this.signatureKeystoreService = signatureKeystoreService;
		this.votingCardSetRepository = votingCardSetRepository;
		this.setupComponentVerificationDataPayloadFileSystemRepository = setupComponentVerificationDataPayloadFileSystemRepository;
	}

	/**
	 * Loads all node contributions responses into an {@link EncryptedNodeLongReturnCodeShares} for the given {@code electionEventId} and
	 * {@code verificationCardSetId}.
	 *
	 * @param electionEventId       the node contributions responses' election event id. Must be non-null and a valid UUID.
	 * @param verificationCardSetId the node contributions responses' verification card set id. Must be non-null and a valid UUID.
	 * @return an {@link EncryptedNodeLongReturnCodeShares}
	 * @throws FailedValidationException if {@code electionEventId} or {@code verificationCardSetId} is invalid.
	 */
	public EncryptedNodeLongReturnCodeShares load(final String electionEventId, final String verificationCardSetId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);

		LOGGER.info("Loading the node contributions. [electionEventId: {}, verificationCardSetId: {}]", electionEventId, verificationCardSetId);

		final List<List<ControlComponentCodeSharesPayload>> nodeContributionsResponses =
				nodeContributionsResponsesService.load(electionEventId, verificationCardSetId);

		verifyConsistency(electionEventId, verificationCardSetId, nodeContributionsResponses);

		checkState(nodeContributionsResponses.parallelStream().flatMap(Collection::stream).allMatch(this::verifySignature),
				"All return code generation response payloads must have a valid signature. [electionEventId: {}, verificationCardSetId: {}]",
				electionEventId, verificationCardSetId);

		final List<EncryptedSingleNodeLongReturnCodeShares> encryptedSingleNodeLongReturnCodesGenerationValues = NODE_IDS.stream()
				.parallel()
				.map(nodeId -> {
					final List<String> verificationCardIds = new ArrayList<>();
					final List<ElGamalMultiRecipientCiphertext> partialChoiceReturnCodes = new ArrayList<>();
					final List<ElGamalMultiRecipientCiphertext> confirmationKeys = new ArrayList<>();

					nodeContributionsResponses.stream()
							.flatMap(Collection::stream)
							.filter(controlComponentCodeSharesPayload -> controlComponentCodeSharesPayload.getNodeId() == nodeId)
							.map(ControlComponentCodeSharesPayload::getControlComponentCodeShares)
							.flatMap(Collection::stream)
							.forEach(controlComponentCodeShare -> {
								verificationCardIds.add(controlComponentCodeShare.verificationCardId());
								partialChoiceReturnCodes.add(controlComponentCodeShare.exponentiatedEncryptedPartialChoiceReturnCodes());
								confirmationKeys.add(controlComponentCodeShare.exponentiatedEncryptedConfirmationKey());
							});

					return new EncryptedSingleNodeLongReturnCodeShares.Builder()
							.setNodeId(nodeId)
							.setVerificationCardIds(verificationCardIds)
							.setExponentiatedEncryptedPartialChoiceReturnCodes(partialChoiceReturnCodes)
							.setExponentiatedEncryptedConfirmationKeys(confirmationKeys)
							.build();
				}).toList();

		try {
			final String votingCardSetId = votingCardSetRepository.getVotingCardSetId(verificationCardSetId);
			final int numberOfVotingCards = votingCardSetRepository.getNumberOfVotingCards(electionEventId, votingCardSetId);
			encryptedSingleNodeLongReturnCodesGenerationValues.forEach(e -> checkState(e.getVerificationCardIds().size() == numberOfVotingCards,
					"The number of verification card ids does not match the expected number of voting cards for this verification card set ID. "
							+ "[electionEventId: %s, verificationCardSetId: %s, actualVerificationCardSetIdsCount: %s, expectedVerificationCardSetIdsCount: %s]",
					electionEventId, verificationCardSetId, e.getVerificationCardIds().size(), numberOfVotingCards));
		} catch (final ResourceNotFoundException e) {
			throw new IllegalStateException(String.format(
					"Cannot get the voting card set id. [electionEventId: %s, verificationCardSetId: %s]",
					electionEventId, verificationCardSetId), e);
		}

		LOGGER.info("Node contributions successfully loaded. [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
				verificationCardSetId);

		return new EncryptedNodeLongReturnCodeShares.Builder()
				.setElectionEventId(electionEventId)
				.setVerificationCardSetId(verificationCardSetId)
				.setVerificationCardIds(encryptedSingleNodeLongReturnCodesGenerationValues.get(0).getVerificationCardIds())
				.setNodeReturnCodesValues(encryptedSingleNodeLongReturnCodesGenerationValues)
				.build();
	}

	/**
	 * Verifies:
	 * <ul>
	 *     <li>there is at least one node contribution response.</li>
	 *     <li>the election event id and verification card set id of all the ControlComponentCodeSharesPayloads are correct.</li>
	 *     <li>there is no node id or chunk id missing.</li>
	 *     <li>the chunk count is the same for the SetupComponentVerificationDataPayloads and ControlComponentCodeSharesPayloads.</li>
	 *     <li>the chunks' consistency using {@link EncryptedNodeLongReturnCodeSharesService#verifyConsistencyChunk}.</li>
	 * </ul>
	 */
	private void verifyConsistency(final String electionEventId, final String verificationCardSetId,
			final List<List<ControlComponentCodeSharesPayload>> nodeContributionsResponses) {

		checkState(!nodeContributionsResponses.isEmpty(), "No node contributions responses. [electionEventId: %s, verificationCardSetId: %s]",
				electionEventId, verificationCardSetId);

		checkState(IntStream.range(0, nodeContributionsResponses.size()).parallel()
						.allMatch(i -> nodeContributionsResponses.get(i).size() == NODE_IDS.size() && nodeContributionsResponses.get(i).stream()
								.allMatch(nodeContributionResponse -> nodeContributionResponse.getChunkId() == i)),
				"The chunk ID sequence is interrupted or incomplete. [electionEventId: %s, verificationCardSetId: %s]", electionEventId,
				verificationCardSetId);

		checkState(nodeContributionsResponses.parallelStream().flatMap(Collection::stream)
						.allMatch(payload -> electionEventId.equals(payload.getElectionEventId()) && verificationCardSetId.equals(
								payload.getVerificationCardSetId())),
				"All return code generation response payloads must be related to the correct election event id and "
						+ "verification card set id. [electionEventId: %s, verificationCardSetId: %s]", electionEventId, verificationCardSetId);

		final int chunkCount;
		try {
			chunkCount = setupComponentVerificationDataPayloadFileSystemRepository.getCount(electionEventId, verificationCardSetId);
		} catch (final PayloadStorageException e) {
			throw new IllegalStateException(String.format(
					"No chunk count found for the SetupComponentVerificationDataPayload. [electionEventId: %s, verificationCardSetId: %s]",
					electionEventId, verificationCardSetId), e);
		}

		checkState(chunkCount == nodeContributionsResponses.size(),
				"The SetupComponentVerificationDataPayloads and ControlComponentCodeSharesPayloads have different chunk counts. "
						+ "[electionEventId: %s, verificationCardSetId: %s]", electionEventId, verificationCardSetId);

		IntStream.range(0, chunkCount)
				.parallel()
				.forEach(chunkId -> verifyConsistencyChunk(electionEventId, verificationCardSetId, nodeContributionsResponses, chunkId));
	}

	/**
	 * Verifies:
	 * <ul>
	 *     <li>the ControlComponentCodeSharesPayloads have the same encryption group as the SetupComponentVerificationDataPayloads.</li>
	 *     <li>the verification card ids are unique among the ControlComponentCodeSharesPayload chunks.</li>
	 *     <li>the ControlComponentCodeSharesPayloads' verification card ids have the same content and order across all nodes.</li>
	 *     <li>the verification card ids of the SetupComponentVerificationDataPayload's chunks have the same content and order than the
	 *     verification card ids of the ControlComponentCodeSharesPayload's chunks.</li>
	 * </ul>
	 */
	private void verifyConsistencyChunk(final String electionEventId, final String verificationCardSetId,
			final List<List<ControlComponentCodeSharesPayload>> nodeContributionsResponses, final int chunkId) {

		final SetupComponentVerificationDataPayload setupComponentVerificationDataPayload;
		try {
			setupComponentVerificationDataPayload = setupComponentVerificationDataPayloadFileSystemRepository.retrieve(electionEventId,
					verificationCardSetId, chunkId);
		} catch (final PayloadStorageException e) {
			throw new IllegalStateException(
					String.format("No SetupComponentVerificationDataPayload found. [electionEventId: %s, verificationCardSetId: %s, chunkId: %s]",
							electionEventId, verificationCardSetId, chunkId), e);
		}

		final List<String> setupComponentVerificationCardIds = setupComponentVerificationDataPayload.getSetupComponentVerificationData()
				.stream()
				.map(SetupComponentVerificationData::verificationCardId)
				.toList();

		nodeContributionsResponses.get(chunkId)
				.stream()
				.parallel()
				.forEach(nodePayload -> {
					final int nodeId = nodePayload.getNodeId();

					checkState(setupComponentVerificationDataPayload.getEncryptionGroup().equals(nodePayload.getEncryptionGroup()),
							"The ControlComponentCodeSharesPayload does not have the same encryption group as the "
									+ "SetupComponentVerificationDataPayload. [electionEventId: %s, verificationCardSetId: %s, chunkId: %s, nodeId: %s]",
							electionEventId, verificationCardSetId, chunkId, nodeId);

					final List<String> controlComponentVerificationCardIds = nodePayload.getControlComponentCodeShares().stream()
							.map(ControlComponentCodeShare::verificationCardId)
							.toList();
					// The ControlComponentCodeShare payload ensures no verificationCardId duplicates.

					checkState(setupComponentVerificationCardIds.equals(controlComponentVerificationCardIds),
							"The ControlComponentCodeSharesPayload does not have the same verification card ids as the "
									+ "SetupComponentVerificationDataPayload. [electionEventId: %s, verificationCardSetId: %s, chunkId: %s, nodeId: %s]",
							electionEventId, verificationCardSetId, chunkId, nodeId);
				});
	}

	private boolean verifySignature(final ControlComponentCodeSharesPayload payload) {
		final int nodeId = payload.getNodeId();
		final String electionEventId = payload.getElectionEventId();
		final String verificationCardSetId = payload.getVerificationCardSetId();
		final int chunkId = payload.getChunkId();

		final CryptoPrimitivesSignature signature = payload.getSignature();

		checkState(signature != null,
				"The signature of the control component code shares payload is null. [nodeId: %s, electionEventId: %s, verificationCardSetId: %s, chunkId: %s]",
				nodeId, electionEventId, verificationCardSetId, chunkId);

		try {
			return signatureKeystoreService.verifySignature(Alias.getControlComponentByNodeId(nodeId), payload,
					ChannelSecurityContextData.controlComponentCodeShares(nodeId, electionEventId, verificationCardSetId),
					signature.signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(String.format(
					"Cannot verify the signature of the control component code shares payload. [nodeId: %s, electionEventId: %s, verificationCardSetId: %s, chunkId: %s]",
					nodeId, electionEventId, verificationCardSetId, chunkId), e);
		}
	}

}
