/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.importexport;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.nio.file.attribute.AclEntryFlag.DIRECTORY_INHERIT;
import static java.nio.file.attribute.AclEntryFlag.FILE_INHERIT;
import static java.nio.file.attribute.AclEntryPermission.APPEND_DATA;
import static java.nio.file.attribute.AclEntryPermission.DELETE;
import static java.nio.file.attribute.AclEntryPermission.EXECUTE;
import static java.nio.file.attribute.AclEntryPermission.READ_ACL;
import static java.nio.file.attribute.AclEntryPermission.READ_ATTRIBUTES;
import static java.nio.file.attribute.AclEntryPermission.READ_DATA;
import static java.nio.file.attribute.AclEntryPermission.READ_NAMED_ATTRS;
import static java.nio.file.attribute.AclEntryPermission.SYNCHRONIZE;
import static java.nio.file.attribute.AclEntryPermission.WRITE_ATTRIBUTES;
import static java.nio.file.attribute.AclEntryPermission.WRITE_DATA;
import static java.nio.file.attribute.AclEntryPermission.WRITE_NAMED_ATTRS;
import static java.nio.file.attribute.AclEntryType.ALLOW;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.attribute.AclEntry;
import java.nio.file.attribute.AclFileAttributeView;
import java.nio.file.attribute.PosixFilePermissions;
import java.nio.file.attribute.UserPrincipal;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import net.lingala.zip4j.io.inputstream.ZipInputStream;
import net.lingala.zip4j.io.outputstream.ZipOutputStream;
import net.lingala.zip4j.model.LocalFileHeader;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.model.enums.AesKeyStrength;
import net.lingala.zip4j.model.enums.CompressionMethod;
import net.lingala.zip4j.model.enums.EncryptionMethod;

/**
 * Zip and unzip files.
 */
@Service
public class CompressionService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CompressionService.class);

	/**
	 * Zip the content of a given directory and return the result as a byte array.
	 *
	 * @param directoryToZip the path to the directory's content which must be zipped. Must be non-null.
	 * @param password       an optional password to encrypt the ZIP using the AES256 algorithm.
	 * @return a byte[] containing the generated zip file
	 */
	public byte[] zipDirectory(final Path directoryToZip, char[] password) throws IOException {
		checkNotNull(directoryToZip);
		checkArgument(Files.isDirectory(directoryToZip), "directoryToZip is not a directory. [%s]", directoryToZip);

		secureDirectory(directoryToZip);

		final ZipParameters zipParameters = new ZipParameters();
		if (password != null && password.length > 0) {
			zipParameters.setEncryptFiles(true);
			zipParameters.setEncryptionMethod(EncryptionMethod.AES);
			zipParameters.setAesKeyStrength(AesKeyStrength.KEY_STRENGTH_256);
			zipParameters.setCompressionMethod(CompressionMethod.DEFLATE);
		}

		try (final ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			try (final ZipOutputStream zos = new ZipOutputStream(baos, password)) {
				try (final Stream<Path> paths = Files.walk(directoryToZip)) {
					for (final Path path : paths.toList()) {
						if (!Files.isDirectory(path)) {
							final String zipEntryName = directoryToZip.relativize(path).toString();
							final byte[] buffer = new byte[1024];
							try (FileInputStream fis = new FileInputStream(path.toFile())) {
								zipParameters.setFileNameInZip(zipEntryName);
								zos.putNextEntry(zipParameters);
								int length;
								while ((length = fis.read(buffer)) != -1) {
									zos.write(buffer, 0, length);
								}
								zos.closeEntry();
							}
						}
					}
				}
			}
			return baos.toByteArray();
		}
	}

	/**
	 * Unzip a given zip file as byte array to a given destinationDirectory.
	 *
	 * @param zipFile              the byte[] of the zip file. Must be non-null.
	 * @param password             an optional password to decrypt the zip file.
	 * @param destinationDirectory a directory which exists and is empty
	 */
	public void unzipToDirectory(final byte[] zipFile, char[] password, final Path destinationDirectory) throws IOException {
		checkNotNull(zipFile);
		checkNotNull(destinationDirectory);
		checkArgument(Files.isDirectory(destinationDirectory), "destination is not an existing directory. [destinationDirectory : %s]",
				destinationDirectory);
		checkArgument(isDirEmpty(destinationDirectory), "destination directory is not empty. [destinationDirectory : %s]", destinationDirectory);

		secureDirectory(destinationDirectory);

		try (final ZipInputStream zis = new ZipInputStream(new ByteArrayInputStream(zipFile), password)) {
			LocalFileHeader entry;
			while ((entry = zis.getNextEntry()) != null) {
				final Path fileLocation = destinationDirectory.resolve(entry.getFileName());
				if (!Files.exists(fileLocation.getParent())) {
					Files.createDirectories(fileLocation.getParent());
				}

				final String canonicalDestinationDirPath = destinationDirectory.toFile().getCanonicalPath();
				final String canonicalFileLocation = fileLocation.toFile().getCanonicalPath();

				if (canonicalFileLocation.startsWith(canonicalDestinationDirPath + File.separator)) {
					final byte[] buffer = new byte[1024];
					try (final FileOutputStream fos = new FileOutputStream(fileLocation.toFile())) {
						int len;
						while ((len = zis.read(buffer)) != -1) {
							fos.write(buffer, 0, len);
						}
					}
				} else {
					LOGGER.warn("The zip file contains an unexpected file. [canonicalFileLocation:{}]", canonicalFileLocation);
				}
			}
		}
	}

	private static boolean isDirEmpty(final Path directory) throws IOException {
		try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(directory)) {
			return !dirStream.iterator().hasNext();
		}
	}

	private void secureDirectory(final Path path) throws IOException {
		final String POSIX_FILE_ATTRIBUTE_VIEW = "posix";
		final String ACL_FILE_ATTRIBUTE_VIEW = "acl";
		final String POSIX_USER_ONLY_PERMISSION = "rwx------";

		final Set<String> supportedFileAttributeViews = path.getFileSystem().supportedFileAttributeViews();
		if (supportedFileAttributeViews.contains(POSIX_FILE_ATTRIBUTE_VIEW)) {
			LOGGER.debug("File system supports POSIX, setting permission");
			Files.setPosixFilePermissions(path, PosixFilePermissions.fromString(POSIX_USER_ONLY_PERMISSION));
		} else if (supportedFileAttributeViews.contains(ACL_FILE_ATTRIBUTE_VIEW)) {
			LOGGER.debug("File system supports ACL, setting permission");
			final UserPrincipal fileOwner = Files.getOwner(path);

			final AclFileAttributeView view = Files.getFileAttributeView(path, AclFileAttributeView.class, LinkOption.NOFOLLOW_LINKS);

			final AclEntry entry = AclEntry.newBuilder()
					.setType(ALLOW)
					.setPrincipal(fileOwner)
					.setFlags(DIRECTORY_INHERIT,
							FILE_INHERIT)
					.setPermissions(WRITE_NAMED_ATTRS,
							WRITE_ATTRIBUTES,
							DELETE,
							WRITE_DATA,
							READ_ACL,
							APPEND_DATA,
							READ_ATTRIBUTES,
							READ_DATA,
							EXECUTE,
							SYNCHRONIZE,
							READ_NAMED_ATTRS)
					.build();

			final List<AclEntry> acl = List.of(entry);
			view.setAcl(acl);
		}
	}
}
