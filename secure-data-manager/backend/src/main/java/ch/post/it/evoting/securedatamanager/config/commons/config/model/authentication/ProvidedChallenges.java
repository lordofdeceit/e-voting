/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.config.commons.config.model.authentication;

import java.util.List;

public record ProvidedChallenges(String alias, List<String> challenges) {

}
