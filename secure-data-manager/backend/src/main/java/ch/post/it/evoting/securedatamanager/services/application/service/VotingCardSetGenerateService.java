/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.securedatamanager.configuration.setupvoting.ReturnCodesPayloadsGeneratedService;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.domain.model.generator.DataGeneratorResponse;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.InvalidStatusTransitionException;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.Status;
import ch.post.it.evoting.securedatamanager.services.domain.service.VotingCardSetDataGeneratorService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.service.ConfigurationEntityStatusService;

/**
 * This is an application service that manages voting card sets.
 */
@Service
@ConditionalOnProperty("role.isConfig")
public class VotingCardSetGenerateService extends BaseVotingCardSetService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetGenerateService.class);

	private final IdleStatusService idleStatusService;
	private final ConfigurationEntityStatusService configurationEntityStatusService;
	private final VotingCardSetDataGeneratorService votingCardSetDataGeneratorService;
	private final VotingCardSetGenerateBallotService votingCardSetGenerateBallotService;
	private final ReturnCodesPayloadsGeneratedService returnCodesPayloadsGeneratedService;

	public VotingCardSetGenerateService(final IdleStatusService idleStatusService,
			final ConfigurationEntityStatusService configurationEntityStatusService,
			final VotingCardSetDataGeneratorService votingCardSetDataGeneratorService,
			final VotingCardSetGenerateBallotService votingCardSetGenerateBallotService,
			final ReturnCodesPayloadsGeneratedService returnCodesPayloadsGeneratedService) {
		this.idleStatusService = idleStatusService;
		this.configurationEntityStatusService = configurationEntityStatusService;
		this.votingCardSetDataGeneratorService = votingCardSetDataGeneratorService;
		this.votingCardSetGenerateBallotService = votingCardSetGenerateBallotService;
		this.returnCodesPayloadsGeneratedService = returnCodesPayloadsGeneratedService;
	}

	/**
	 * Generates the voting card set data based on the given votingCardSetId. The generation contains 3 steps: generate the ballot box data, generate
	 * the ballot file and finally the voting card set data.
	 *
	 * @param electionEventId The id of the election event.
	 * @param votingCardSetId The id of the voting card set for which the data is generated.
	 * @return a DataGeneratorResponse containing information about the result of the generation.
	 * @throws InvalidStatusTransitionException if the original status does not allow the generation
	 */
	public DataGeneratorResponse generate(final String electionEventId, final String votingCardSetId)
			throws ResourceNotFoundException, InvalidStatusTransitionException {

		LOGGER.info("Generating the voting card set. [electionEventId: {}, votingCardSetId: {}]", electionEventId, votingCardSetId);

		// Check if generation already started
		if (!idleStatusService.getIdLock(votingCardSetId)) {
			return new DataGeneratorResponse();
		}

		try {

			validateUUID(electionEventId);
			validateUUID(votingCardSetId);

			checkVotingCardSetStatusTransition(electionEventId, votingCardSetId, Status.VCS_DOWNLOADED, Status.GENERATED);

			// Generate the needed data: ballot and ballot box
			final DataGeneratorResponse result = votingCardSetGenerateBallotService.generate(electionEventId, votingCardSetId);
			if (!result.isSuccessful()) {
				return result;
			}

			// Generate the Return Codes payloads
			returnCodesPayloadsGeneratedService.generate(electionEventId, votingCardSetId);

			// Generate voting card set data (via spring batch)
			final DataGeneratorResponse response = votingCardSetDataGeneratorService.generate(electionEventId, votingCardSetId);

			// Update voting card set status to GENERATING
			configurationEntityStatusService.update(Status.GENERATING.name(), votingCardSetId, votingCardSetRepository);

			return response;

		} finally {
			idleStatusService.freeIdLock(votingCardSetId);
		}
	}
}
