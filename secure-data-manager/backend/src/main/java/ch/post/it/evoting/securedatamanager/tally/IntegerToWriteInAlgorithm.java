/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally;

import static ch.post.it.evoting.domain.election.WriteInAlphabet.WRITE_IN_ALPHABET;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.LinkedList;

import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.math.ZqElement;

/**
 * Implements the IntegerToWriteIn algorithm.
 */
@Service
public class IntegerToWriteInAlgorithm {

	/**
	 * Maps a {@link ZqElement} to a write-in string.
	 *
	 * @param encoding x, the encoding as a {@link ZqElement}. Must be non-null and different from zero.
	 * @return the write-in string.
	 * @throws NullPointerException     if any parameter is null.
	 * @throws IllegalArgumentException if the encoding's value is zero.
	 */
	@SuppressWarnings("java:S117")
	public String integerToWriteIn(final ZqElement encoding) {

		// Input.
		BigInteger x = checkNotNull(encoding).getValue();
		checkArgument(!x.equals(BigInteger.ZERO), "The encoding x value must not be zero.");

		final BigInteger a = BigInteger.valueOf(WRITE_IN_ALPHABET.size());

		// Operation.
		final LinkedList<String> s = new LinkedList<>();
		while (x.compareTo(BigInteger.ZERO) > 0) {
			final BigInteger b = x.mod(a);
			final String c = WRITE_IN_ALPHABET.get(b.intValueExact());
			s.add(0, c);
			x = x.subtract(b).divide(a);
		}

		return String.join("", s);
	}
}
