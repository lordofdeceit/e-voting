/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkState;

import java.nio.file.Files;
import java.nio.file.Path;
import java.security.SignatureException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.ChannelSecurityContextData;
import ch.post.it.evoting.domain.XsdConstants;
import ch.post.it.evoting.domain.hashable.HashableCantonConfigFactory;
import ch.post.it.evoting.domain.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.securedatamanager.XmlFileRepository;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@Repository
@ConditionalOnProperty("role.isConfig")
public class CantonConfigConfigFileRepository extends XmlFileRepository<Configuration> {
	private static final Logger LOGGER = LoggerFactory.getLogger(CantonConfigConfigFileRepository.class);

	private static final String CANTON_CONFIG_XML = Constants.CONFIG_FILE_NAME_CONFIGURATION_ANONYMIZED;
	private final PathResolver pathResolver;
	private final SignatureKeystore<Alias> signatureKeystoreServiceSdmConfig;

	public CantonConfigConfigFileRepository(final PathResolver pathResolver,
			@Qualifier("keystoreServiceSdmConfig")
			final SignatureKeystore<Alias> signatureKeystoreServiceSdmConfig) {
		this.pathResolver = pathResolver;
		this.signatureKeystoreServiceSdmConfig = signatureKeystoreServiceSdmConfig;
	}

	/**
	 * Loads the canton config for the given election event id and validates it against the related XSD. The canton config is
	 * located in the {@value CANTON_CONFIG_XML} file and the related XSD in {@value XsdConstants#CANTON_CONFIG_XSD}.
	 * <p>
	 * If the canton config file or the related XSD does not exist this method returns an empty Optional.
	 * <p>
	 * This method also validates the signature of the loaded file.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @return the contest configuration as an {@link Optional}.
	 * @throws NullPointerException      if the election event id is null.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 * @throws IllegalStateException     if the signature is invalid, or it could not be verified.
	 */
	public Optional<Configuration> load(final String electionEventId) {

		validateUUID(electionEventId);

		LOGGER.debug("Loading canton config file. [electionEventId: {}]", electionEventId);

		final Path xmlFilePath = pathResolver.resolveInputPath(electionEventId).resolve(CANTON_CONFIG_XML);

		if (!Files.exists(xmlFilePath)) {
			LOGGER.debug("The requested file does not exist. [electionEventId: {}, xmlFilePath: {}]", electionEventId, xmlFilePath);
			return Optional.empty();
		}

		final Configuration configuration = read(xmlFilePath, XsdConstants.CANTON_CONFIG_XSD, Configuration.class);

		checkState(isSignatureValid(configuration, electionEventId));

		LOGGER.debug("File successfully loaded. [file: {}]", CANTON_CONFIG_XML);

		return Optional.of(configuration);
	}

	private boolean isSignatureValid(final Configuration configuration, final String electionEventId) {
		final byte[] signature = configuration.getSignature();
		final Hashable hashable = HashableCantonConfigFactory.fromConfiguration(configuration);
		final Hashable additionalContextData = ChannelSecurityContextData.cantonConfig();
		try {
			return signatureKeystoreServiceSdmConfig.verifySignature(Alias.CANTON, hashable, additionalContextData, signature);
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Unable to verify canton config signature. [electionEventId: %s]", electionEventId), e);
		}
	}

}
