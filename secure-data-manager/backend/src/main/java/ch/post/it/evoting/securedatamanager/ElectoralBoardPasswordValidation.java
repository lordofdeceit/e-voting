/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.nio.CharBuffer;

/**
 * Validation of an electoral board password.
 */
public class ElectoralBoardPasswordValidation {

	private static final int EB_PASSWORD_LENGTH = 12;

	private ElectoralBoardPasswordValidation() {
		// Intentionally left blank.
	}

	/**
	 * Validates the password of the electoral board member.
	 *
	 * @param password the EB password. Must be non-null.
	 * @throws NullPointerException     if the password is null.
	 * @throws IllegalArgumentException if
	 *                                  <ul>
	 *                                      <li>the size of the password is strictly smaller than 12.</li>
	 *                                      <li>the password does not contain at least one upper case letter.</li>
	 *                                      <li>the password does not contain at least one lower case letter.</li>
	 *                                      <li>the password does not contain at least one special character.</li>
	 *                                      <li>the password does not contain at least one digit.</li>
	 *                                  </ul>
	 */
	public static void validate(final char[] password) {
		checkNotNull(password);
		checkArgument(password.length >= EB_PASSWORD_LENGTH, "The password must be at least of size %s.", EB_PASSWORD_LENGTH);
		checkArgument(CharBuffer.wrap(password).chars().anyMatch(c -> Character.isAlphabetic(c) && Character.isUpperCase(c)),
				"The password must contain at least one upper case letter.");
		checkArgument(CharBuffer.wrap(password).chars().anyMatch(c -> Character.isAlphabetic(c) && Character.isLowerCase(c)),
				"The password must contain at least one lower case letter.");
		checkArgument(CharBuffer.wrap(password).chars().anyMatch(c -> String.valueOf((char) c).matches("[^A-Za-z0-9]")),
				"The password must contain at least one special character.");
		checkArgument(CharBuffer.wrap(password).chars().anyMatch(Character::isDigit), "The password must contain at least one digit.");
	}
}
