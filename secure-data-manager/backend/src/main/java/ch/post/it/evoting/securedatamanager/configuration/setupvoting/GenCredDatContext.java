/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Objects;

import ch.post.it.evoting.cryptoprimitives.domain.validations.Validations;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;

public final class GenCredDatContext {
	private final String electionEventId;
	private final String verificationCardSetId;
	private final ElGamalMultiRecipientPublicKey electionPublicKey;
	private final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey;
	private final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions;
	private final List<String> actualVotingOptions;
	private final List<String> correctnessInformationSelections;
	private final List<String> correctnessInformationVotingOptions;
	private final GqGroup encryptionGroup;

	private GenCredDatContext(
			final String electionEventId,
			final String verificationCardSetId,
			final ElGamalMultiRecipientPublicKey electionPublicKey,
			final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey,
			final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions,
			final List<String> actualVotingOptions,
			final List<String> correctnessInformationSelections,
			final List<String> correctnessInformationVotingOptions,
			final GqGroup encryptionGroup) {
		this.electionEventId = electionEventId;
		this.verificationCardSetId = verificationCardSetId;
		this.electionPublicKey = electionPublicKey;
		this.choiceReturnCodesEncryptionPublicKey = choiceReturnCodesEncryptionPublicKey;
		this.encodedVotingOptions = encodedVotingOptions;
		this.actualVotingOptions = actualVotingOptions;
		this.correctnessInformationSelections = correctnessInformationSelections;
		this.correctnessInformationVotingOptions = correctnessInformationVotingOptions;
		this.encryptionGroup = encryptionGroup;
	}

	public String electionEventId() {
		return electionEventId;
	}

	public String verificationCardSetId() {
		return verificationCardSetId;
	}

	public ElGamalMultiRecipientPublicKey electionPublicKey() {
		return electionPublicKey;
	}

	public ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey() {
		return choiceReturnCodesEncryptionPublicKey;
	}

	public GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions() {
		return encodedVotingOptions;
	}

	public List<String> actualVotingOptions() {
		return actualVotingOptions;
	}

	public List<String> correctnessInformationSelections() {
		return correctnessInformationSelections;
	}

	public List<String> correctnessInformationVotingOptions() {
		return correctnessInformationVotingOptions;
	}

	public GqGroup encryptionGroup() {
		return encryptionGroup;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final GenCredDatContext that = (GenCredDatContext) o;
		return Objects.equals(electionEventId, that.electionEventId) && Objects.equals(verificationCardSetId,
				that.verificationCardSetId) && Objects.equals(electionPublicKey, that.electionPublicKey) && Objects.equals(
				choiceReturnCodesEncryptionPublicKey, that.choiceReturnCodesEncryptionPublicKey) && Objects.equals(encodedVotingOptions,
				that.encodedVotingOptions) && Objects.equals(actualVotingOptions, that.actualVotingOptions) && Objects.equals(
				correctnessInformationSelections, that.correctnessInformationSelections) && Objects.equals(correctnessInformationVotingOptions,
				that.correctnessInformationVotingOptions) && Objects.equals(encryptionGroup, that.encryptionGroup);
	}

	@Override
	public int hashCode() {
		return Objects.hash(electionEventId, verificationCardSetId, electionPublicKey, choiceReturnCodesEncryptionPublicKey, encodedVotingOptions,
				actualVotingOptions, correctnessInformationSelections, correctnessInformationVotingOptions, encryptionGroup);
	}

	@Override
	public String toString() {
		return "GenCredDatContext{" +
				"electionEventId='" + electionEventId + '\'' +
				", verificationCardSetId='" + verificationCardSetId + '\'' +
				", electionPublicKey=" + electionPublicKey +
				", choiceReturnCodesEncryptionPublicKey=" + choiceReturnCodesEncryptionPublicKey +
				", encodedVotingOptions=" + encodedVotingOptions +
				", actualVotingOptions=" + actualVotingOptions +
				", correctnessInformationSelections=" + correctnessInformationSelections +
				", correctnessInformationVotingOptions=" + correctnessInformationVotingOptions +
				", encryptionGroup=" + encryptionGroup +
				'}';
	}

	public static class Builder {

		private String electionEventId;
		private String verificationCardSetId;
		private ElGamalMultiRecipientPublicKey electionPublicKey;
		private ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey;
		private GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions;
		private List<String> actualVotingOptions;
		private List<String> correctnessInformationSelections;
		private List<String> correctnessInformationVotingOptions;

		public GenCredDatContext build() {

			validateUUID(electionEventId);
			validateUUID(verificationCardSetId);
			checkNotNull(electionPublicKey);
			checkNotNull(choiceReturnCodesEncryptionPublicKey);
			checkNotNull(encodedVotingOptions);
			checkNotNull(actualVotingOptions);
			checkNotNull(correctnessInformationSelections);
			checkNotNull(correctnessInformationVotingOptions);

			final GqGroup gqGroup = electionPublicKey.getGroup();

			checkArgument(choiceReturnCodesEncryptionPublicKey.getGroup().equals(gqGroup),
					"The choiceReturnCodesEncryptionPublicKey and electionPublicKey must have the same group.");
			checkArgument(encodedVotingOptions.getGroup().equals(gqGroup),
					"Vectors encodedVotingOptions elements and electionPublicKey must have the same group.");

			checkArgument(!encodedVotingOptions.isEmpty(), "The encodedVotingOptions must not be empty.");
			checkArgument(!actualVotingOptions.isEmpty(), "The actualVotingOptions must not be empty.");

			checkArgument(encodedVotingOptions.stream().allMatch(Objects::nonNull),
					"The encoded voting options must contains only non-null elements.");
			checkArgument(actualVotingOptions.stream().allMatch(Objects::nonNull),
					"The actual voting options must contains only non-null elements.");

			checkArgument(encodedVotingOptions.size() == actualVotingOptions.size(),
					"Vectors encodedVotingOptions and actualVotingOptions must have the same size.");

			checkArgument(actualVotingOptions.stream().allMatch(e -> e.length() > 0),
					"The elements of the actualVotingOptions must contain at least one character.");

			checkArgument(correctnessInformationSelections.stream().allMatch(Objects::nonNull),
					"The correctness information selections must contains only non-null elements.");
			checkArgument(correctnessInformationVotingOptions.stream().allMatch(Objects::nonNull),
					"The correctness information voting options must contains only non-null elements.");

			correctnessInformationSelections.forEach(Validations::validateUUID);
			correctnessInformationVotingOptions.forEach(Validations::validateUUID);

			return new GenCredDatContext(electionEventId, verificationCardSetId, electionPublicKey, choiceReturnCodesEncryptionPublicKey,
					encodedVotingOptions, actualVotingOptions, correctnessInformationSelections, correctnessInformationVotingOptions, gqGroup);
		}

		public Builder setElectionEventId(final String electionEventId) {
			this.electionEventId = electionEventId;
			return this;
		}

		public Builder setVerificationCardSetId(final String verificationCardSetId) {
			this.verificationCardSetId = verificationCardSetId;
			return this;
		}

		public Builder setElectionPublicKey(final ElGamalMultiRecipientPublicKey electionPublicKey) {
			this.electionPublicKey = electionPublicKey;
			return this;
		}

		public Builder setChoiceReturnCodesEncryptionPublicKey(final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey) {
			this.choiceReturnCodesEncryptionPublicKey = choiceReturnCodesEncryptionPublicKey;
			return this;
		}

		public Builder setEncodedVotingOptions(final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions) {
			this.encodedVotingOptions = encodedVotingOptions;
			return this;
		}

		public Builder setActualVotingOptions(final List<String> actualVotingOptions) {
			this.actualVotingOptions = List.copyOf(actualVotingOptions);
			return this;
		}

		public Builder setCorrectnessInformationSelections(final List<String> correctnessInformationSelections) {
			this.correctnessInformationSelections = List.copyOf(correctnessInformationSelections);
			return this;
		}

		public Builder setCorrectnessInformationVotingOptions(final List<String> correctnessInformationVotingOptions) {
			this.correctnessInformationVotingOptions = List.copyOf(correctnessInformationVotingOptions);
			return this;
		}

	}

}
