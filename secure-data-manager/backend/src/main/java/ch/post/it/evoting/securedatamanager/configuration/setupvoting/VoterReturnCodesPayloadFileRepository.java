/*
 * (c) Copyright 2022 Swiss Post Ltd
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.configuration.VoterReturnCodesPayload;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@Repository
public class VoterReturnCodesPayloadFileRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(VoterReturnCodesPayloadFileRepository.class);

	private static final String FILE_NAME = "voterReturnCodesPayload" + Constants.JSON;

	private final ObjectMapper objectMapper;
	private final PathResolver payloadResolver;

	public VoterReturnCodesPayloadFileRepository(
			final ObjectMapper objectMapper,
			final PathResolver payloadResolver) {
		this.objectMapper = objectMapper;
		this.payloadResolver = payloadResolver;
	}

	/**
	 * Saves the voter return codes payload to the filesystem for the given election event and voting card set.
	 *
	 * @return the path of the saved file.
	 * @throws NullPointerException     if any of the inputs is null.
	 * @throws IllegalArgumentException if any of the inputs is not valid.
	 * @see PathResolver to get the resolved file Path.
	 */
	public Path save(final VoterReturnCodesPayload payload, final String votingCardSetId) {
		checkNotNull(payload);

		final String electionEventId = validateUUID(payload.electionEventId());
		validateUUID(votingCardSetId);

		final Path payloadPath = payloadPath(electionEventId, votingCardSetId);

		try {
			final byte[] payloadBytes = objectMapper.writeValueAsBytes(payload);
			final Path filePath = Files.write(payloadPath, payloadBytes);

			LOGGER.debug("Successfully persisted voter return codes payload. [electionEventId: {}, votingCardSetId: {}]", electionEventId,
					votingCardSetId);

			return filePath;
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Unable to write the voter return codes payload file. [electionEventId: %s, votingCardSetId: %s]", electionEventId,
							votingCardSetId), e);
		}
	}

	/**
	 * Retrieves from the file system a voter return codes payload by election event and voting card set ids.
	 *
	 * @param electionEventId the payload's election event id.
	 * @param votingCardSetId the payload's voting card set id.
	 * @return the voter return codes payload with the given ids or {@link Optional#empty} if not found.
	 * @throws FailedValidationException if {@code electionEventId} or {@code votingCardSetId} is invalid.
	 * @throws UncheckedIOException      if the deserialization of the payload fails.
	 */
	public Optional<VoterReturnCodesPayload> findByElectionEventIdAndVotingCardSetId(final String electionEventId, final String votingCardSetId) {
		validateUUID(electionEventId);
		validateUUID(votingCardSetId);

		final Path payloadPath = payloadPath(electionEventId, votingCardSetId);

		if (!Files.exists(payloadPath)) {
			LOGGER.warn("Requested voter return codes payload does not exist. [electionEventId: {}, votingCardSetId: {}]",
					electionEventId, votingCardSetId);
			return Optional.empty();
		}

		try {
			return Optional.of(objectMapper.readValue(payloadPath.toFile(), VoterReturnCodesPayload.class));
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Failed to deserialize voter return codes payload. [electionEventId: %s, votingCardSetId: %s]",
							electionEventId, votingCardSetId), e);
		}
	}

	private Path payloadPath(final String electionEventId, final String votingCardSetId) {
		final Path printingPath = payloadResolver.resolvePrintingPath(electionEventId);
		return printingPath.resolve(votingCardSetId).resolve(FILE_NAME);
	}
}
