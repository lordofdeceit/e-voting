/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.config.commons.config.logevents;

@SuppressWarnings("squid:S1192") // Ignore 'String literals should not be duplicated' Sonar's rule for this enum definition.
public enum ConfigGeneratorLogEvents {

	GENEECA_SUCCESS_CA_CERTIFICATE_GENERATED("GENERATOR", "GENEECA", "000", "CA certificate successfully generated and stored"),
	GENEECA_ERROR_GENERATING_CA_CERTIFICATE("GENERATOR", "GENEECA", "512", "Error generating the CA certificate"),
	GENEECA_ERROR_STORING_KEYSTORE("GENERATOR", "GENEECA", "513", "error creating keystore"),
	GENEECA_SUCCESS_STORING_KEYSTORE("GENERATOR", "GENEECA", "000", "successfully created and stored keystore"),
	GENEECA_ERROR_GENERATING_KEYPAIR_CA_CERTIFICATE("GENERATOR", "GENEECA", "515", "Error generating the key pair for the CA Certificate"),
	GENEECA_SUCCESS_KEYPAIR_GENERATED_STORED("GENERATOR", "GENEECA", "000", "Key Pairs successfully generated and stored"),

	GENABK_ERROR_GENERATING_KEYPAIR_AB("GENERATOR", "GENABK", "516", "error generating the key pair of the AB"),
	GENABK_SUCCESS_KEYPAIR_GENERATED_STORED("GENERATOR", "GENABK", "000", "key pair successfully generated and stored"),
	GENABK_SUCCESS_AB_CERTIFICATE_GENERATED("GENERATOR", "GENABK", "000", "AB certificate correctly generated"),
	GENABK_ERROR_GENERATING_AB_CERTIFICATE("GENERATOR", "GENABK", "517", "error generating the AB certificate"),

	GENSVPK_ERROR_GENERATING_VCID("GENERATOR", "GENSVPK", "525", "error - error generating the Voting Card Id"),
	GENSVPK_ERROR_DERIVING_KEYSTORE_SYMMETRIC_ENCRYPTION_KEY("GENERATOR", "GENSVPK", "526",
			"error - error deriving the keystore symmetric encryption key"),
	GENSVPK_SUCCESS_VCIDS_GENERATED("GENERATOR", "GENSVPK", "000", "success - Voting Card IDs successfuly generated"),
	GENSVPK_SUCCESS_KEYSTORE_SYMMETRIC_ENCRYPTION_KEY_DERIVED("GENERATOR", "GENSVPK", "000",
			"success - Keystore symmetric encryption key successfully derived"),

	GENCREDAT_ERROR_DERIVING_CREDENTIAL_ID("GENERATOR", "GENCREDAT", "527", "error - error deriving the Credential ID"),
	GENCREDAT_SUCCESS_CREDENTIAL_ID_DERIVED("GENERATOR", "GENCREDAT", "000", "success - Credential ID successfully derived"),
	GENCREDAT_ERROR_GENERATING_CREDENTIAL_ID_AUTHENTICATION_KEYPAIR("GENERATOR", "GENCREDAT", "529",
			"error - error generating the credentialID authentication key pair"),
	GENCREDAT_SUCCESS_CREDENTIAL_ID_AUTHENTICATION_KEYPAIR_GENERATED("GENERATOR", "GENCREDAT", "000",
			"success - credentialID authentication key pair successfully generated"),
	GENCREDAT_ERROR_GENERATING_CREDENTIAL_ID_AUTHENTICATION_CERTIFICATE("GENERATOR", "GENCREDAT", "531",
			"error - error generating the credentialID authentication certificate"),
	GENCREDAT_SUCCESS_CREDENTIAL_ID_AUTHENTICATION_CERTIFICATE_GENERATED("GENERATOR", "GENCREDAT", "000",
			"success - credentialID authentication certificate correctly generated"),
	GENCREDAT_ERROR_GENERATING_KEYSTORE("GENERATOR", "GENCREDAT", "532", "error - error creating keystore"),
	GENCREDAT_SUCCESS_KEYSTORE_GENERATED("GENERATOR", "GENCREDAT", "000", "success - successfully  created and stored keystore"),

	GENBB_SUCCESS_CREATED_AND_STORED("GENERATOR", "GENBB", "000", "Ballot box successfully created and stored"),
	GENBB_SUCCESS_CERTIFICATE_GENERATED("GENERATOR", "GENBB", "000", "Ballot box certificate correctly generated"),
	GENBB_ERROR_GENERATING_CERTIFICATE("GENERATOR", "GENBB", "555", "error - error generating ballot box certificate");

	private final String layer;

	private final String action;

	private final String outcome;

	private final String info;

	ConfigGeneratorLogEvents(final String layer, final String action, final String outcome, final String info) {
		this.layer = layer;
		this.action = action;
		this.outcome = outcome;
		this.info = info;
	}

	public String getAction() {
		return action;
	}

	public String getOutcome() {
		return outcome;
	}

	public String getInfo() {
		return info;
	}

	public String getLayer() {
		return layer;
	}
}
