/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.task;

public enum ExecutionTaskType {
	RETURN_CODES_PAYLOADS
}
