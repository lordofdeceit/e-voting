/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static java.nio.file.Files.newDirectoryStream;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.commons.PathResolver;
import ch.post.it.evoting.securedatamanager.config.commons.utils.JsonUtils;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.LongVoteCastReturnCodesAllowListUploadService;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.SetupComponentCMTablePayloadUploadService;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.Status;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.SynchronizeStatus;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.electionevent.ElectionEventRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetUploadRepository;

/**
 * Service which will upload the information related to the voting card set
 */
@Service
public class VotingCardSetUploadService {
	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetUploadService.class);

	private static final String NULL_ELECTION_EVENT_ID = "";

	private final PathResolver pathResolver;
	private final SignatureService signatureService;
	private final ElectionEventRepository electionEventRepository;
	private final VotingCardSetRepository votingCardSetRepository;
	private final VotingCardSetUploadRepository votingCardSetUploadRepository;
	private final ExtendedAuthenticationUploadService extendedAuthenticationUploadService;

	private final SetupComponentCMTablePayloadUploadService setupComponentCMTablePayloadUploadService;
	private final LongVoteCastReturnCodesAllowListUploadService longVoteCastReturnCodesAllowListUploadService;

	public VotingCardSetUploadService(
			final PathResolver pathResolver,
			final SignatureService signatureService,
			final ElectionEventRepository electionEventRepository,
			final VotingCardSetRepository votingCardSetRepository,
			final VotingCardSetUploadRepository votingCardSetUploadRepository,
			final ExtendedAuthenticationUploadService extendedAuthenticationUploadService,
			final SetupComponentCMTablePayloadUploadService setupComponentCMTablePayloadUploadService,
			final LongVoteCastReturnCodesAllowListUploadService longVoteCastReturnCodesAllowListUploadService) {
		this.pathResolver = pathResolver;
		this.signatureService = signatureService;
		this.electionEventRepository = electionEventRepository;
		this.votingCardSetRepository = votingCardSetRepository;
		this.votingCardSetUploadRepository = votingCardSetUploadRepository;
		this.extendedAuthenticationUploadService = extendedAuthenticationUploadService;
		this.setupComponentCMTablePayloadUploadService = setupComponentCMTablePayloadUploadService;
		this.longVoteCastReturnCodesAllowListUploadService = longVoteCastReturnCodesAllowListUploadService;
	}

	/**
	 * Uploads to the voter portal:
	 * <ul>
	 *     <li>the available ballots and ballot texts</li>
	 *     <li>the return codes mapping tables</li>
	 * </ul>
	 */
	public void uploadSynchronizableVotingCardSets(final String electionEvent) {

		final Map<String, Object> votingCardSetsParams = new HashMap<>();
		addSigned(votingCardSetsParams);
		addPendingToSynchronize(votingCardSetsParams);
		addElectionEventIdIfNotNull(electionEvent, votingCardSetsParams);

		final String votingCardSetDocuments = votingCardSetRepository.list(votingCardSetsParams);

		final JsonArray votingCardSets = JsonUtils.getJsonObject(votingCardSetDocuments).getJsonArray(JsonConstants.RESULT);

		for (int i = 0; i < votingCardSets.size(); i++) {

			final JsonObject votingCardSetInArray = votingCardSets.getJsonObject(i);
			final String electionEventId = votingCardSetInArray.getJsonObject(JsonConstants.ELECTION_EVENT).getString(JsonConstants.ID);

			final JsonObject eEvent = JsonUtils.getJsonObject(electionEventRepository.find(electionEventId));
			final JsonObject adminBoard = eEvent.getJsonObject(JsonConstants.ADMINISTRATION_AUTHORITY);

			final String adminBoardId = adminBoard.getString(JsonConstants.ID);

			final String votingCardSetId = votingCardSetInArray.getString(JsonConstants.ID);
			final String verificationCardSetId = votingCardSetInArray.getString(JsonConstants.VERIFICATION_CARD_SET_ID);

			final Path voterMaterialPath = pathResolver.resolve(Constants.CONFIG_FILES_BASE_DIR).resolve(electionEventId)
					.resolve(Constants.CONFIG_DIR_NAME_ONLINE).resolve(Constants.CONFIG_DIR_NAME_VOTERMATERIAL).resolve(votingCardSetId);

			final Path extendedAuthenticationPath = pathResolver.resolve(Constants.CONFIG_FILES_BASE_DIR).resolve(electionEventId)
					.resolve(Constants.CONFIG_DIR_NAME_ONLINE).resolve(Constants.CONFIG_FILE_EXTENDED_AUTHENTICATION_DATA).resolve(votingCardSetId);

			final JsonObjectBuilder builder = Json.createObjectBuilder();
			try {
				LOGGER.info("Uploading voter material configuration");
				uploadVoterMaterialContents(voterMaterialPath, electionEventId, votingCardSetId, adminBoardId);

				LOGGER.info("Uploading extended authentication");
				extendedAuthenticationUploadService.uploadExtendedAuthenticationFiles(extendedAuthenticationPath, electionEventId, adminBoardId);

				LOGGER.info("Uploading return codes mapping table");
				setupComponentCMTablePayloadUploadService.uploadReturnCodesMappingTable(electionEventId, verificationCardSetId);

				LOGGER.info("Uploading long vote cast return codes allow list");
				longVoteCastReturnCodesAllowListUploadService.upload(electionEventId, verificationCardSetId);

				builder.add(JsonConstants.ID, votingCardSetId);
				builder.add(JsonConstants.SYNCHRONIZED, SynchronizeStatus.SYNCHRONIZED.getIsSynchronized().toString());
				builder.add(JsonConstants.DETAILS, SynchronizeStatus.SYNCHRONIZED.getStatus());
				LOGGER.info("The voting card and verification card sets where successfully uploaded");
			} catch (final IOException e) {
				LOGGER.error("An error occurred while uploading the signed voting card set and verification card set", e);
				builder.add(JsonConstants.ID, votingCardSetId);
				builder.add(JsonConstants.DETAILS, SynchronizeStatus.FAILED.getStatus());
			}
			LOGGER.info("Changing the state of the voting card set");
			votingCardSetRepository.update(builder.build().toString());
		}
	}

	private void addElectionEventIdIfNotNull(final String electionEvent, final Map<String, Object> votingCardSetsParams) {
		if (!NULL_ELECTION_EVENT_ID.equals(electionEvent)) {
			votingCardSetsParams.put(JsonConstants.ELECTION_EVENT_DOT_ID, electionEvent);
		}
	}

	private void addPendingToSynchronize(final Map<String, Object> votingCardSetsParams) {
		votingCardSetsParams.put(JsonConstants.SYNCHRONIZED, SynchronizeStatus.PENDING.getIsSynchronized().toString());
	}

	private void addSigned(final Map<String, Object> votingCardSetsParams) {
		votingCardSetsParams.put(JsonConstants.STATUS, Status.SIGNED.name());
	}

	private void uploadVoterMaterialContents(final Path voterMaterialPath, final String electionEventId, final String votingCardSetId,
			final String adminBoardId) throws IOException {
		try (final DirectoryStream<Path> files = newDirectoryStream(voterMaterialPath, Constants.CSV_GLOB)) {
			for (final Path file : files) {
				final String name = file.getFileName().toString();
				if (name.startsWith(Constants.CONFIG_FILE_NAME_VOTER_INFORMATION)) {
					LOGGER.info("Uploading voter information file {} and its signature", name);
					uploadVoterInformationFromCSV(electionEventId, votingCardSetId, adminBoardId, file);
				} else if (name.startsWith(Constants.CONFIG_FILE_NAME_CREDENTIAL_DATA)) {
					LOGGER.info("Uploading credential data file {} and its signature", name);
					uploadCredentialDataFromCSV(electionEventId, votingCardSetId, adminBoardId, file);
				}
			}
		}
	}

	private void uploadVoterInformationFromCSV(final String electionEventId, final String votingCardSetId, final String adminBoardId,
			final Path filePath) throws IOException {

		try (final InputStream stream = signatureService.newCSVAndSignatureInputStream(filePath)) {

			votingCardSetUploadRepository.uploadVoterInformation(electionEventId, votingCardSetId, adminBoardId, stream);

		}
	}

	private void uploadCredentialDataFromCSV(final String electionEventId, final String votingCardSetId, final String adminBoardId,
			final Path filePath) throws IOException {

		try (final InputStream stream = signatureService.newCSVAndSignatureInputStream(filePath)) {

			votingCardSetUploadRepository.uploadCredentialData(electionEventId, votingCardSetId, adminBoardId, stream);

		}
	}
}
