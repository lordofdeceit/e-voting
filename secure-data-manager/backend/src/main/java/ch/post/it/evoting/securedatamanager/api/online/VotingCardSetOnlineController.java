/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api.online;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import java.io.IOException;
import java.security.SignatureException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.application.service.VotingCardSetComputationService;
import ch.post.it.evoting.securedatamanager.services.application.service.VotingCardSetDownloadService;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.InvalidStatusTransitionException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.PayloadStorageException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * The voting card set end-point for the online SDM.
 */
@RestController
@RequestMapping("/sdm-backend/online/votingcardsets")
@Api(value = "Voting card set REST API")
public class VotingCardSetOnlineController {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetOnlineController.class);

	private static final String VOTING_CARD_SET_URL_PATH = "/electionevent/{electionEventId}/votingcardset/{votingCardSetId}";

	private final VotingCardSetDownloadService votingCardSetDownloadService;
	private final VotingCardSetComputationService votingCardSetComputationService;

	public VotingCardSetOnlineController(final VotingCardSetDownloadService votingCardSetDownloadService,
			final VotingCardSetComputationService votingCardSetComputationService) {
		this.votingCardSetDownloadService = votingCardSetDownloadService;
		this.votingCardSetComputationService = votingCardSetComputationService;
	}

	/**
	 * Changes the status of a list of voting card sets by performing the compute operation. The HTTP call uses a PUT request to the voting card set
	 * endpoint. If the requested status cannot be transitioned to computing from the current one, the call will fail.
	 *
	 * @param electionEventId the election event id.
	 * @param votingCardSetId the voting card set id.
	 * @return a list of ids of the created voting card sets.
	 */
	@PutMapping(value = VOTING_CARD_SET_URL_PATH + "/compute", produces = "application/json")
	@ApiOperation(value = "Change the status of a voting card set to computing",
			notes = "Change the status of a voting card set to computing, performing the compute operation")
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Not Found") })
	public ResponseEntity<Object> setVotingCardSetStatusComputing(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electionEventId,
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String votingCardSetId)
			throws PayloadStorageException, SignatureException {

		validateUUID(electionEventId);
		validateUUID(votingCardSetId);

		ResponseEntity<Object> response;
		try {
			votingCardSetComputationService.compute(votingCardSetId, electionEventId);
			response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (final InvalidStatusTransitionException e) {
			LOGGER.info("Error trying to set voting card set status to computing. [electionEventId: {}, votingCardSetId: {}]", electionEventId,
					votingCardSetId, e);
			response = ResponseEntity.badRequest().build();
		}
		return response;
	}

	/**
	 * Changes the status of a list of voting card sets by performing the download operation. The HTTP call uses a PUT request to the voting card set
	 * endpoint. If the requested status cannot be transitioned to computing from the current one, the call will fail.
	 *
	 * @param electionEventId the election event id.
	 * @param votingCardSetId the voting card set id.
	 * @return a list of ids of the created voting card sets.
	 */
	@PutMapping(value = VOTING_CARD_SET_URL_PATH + "/download", produces = "application/json")
	@ApiOperation(value = "Change the status of a voting card set to downloaded",
			notes = "Change the status of a voting card set to downloaded, performing the download operation")
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Not Found") })
	public ResponseEntity<Object> setVotingCardSetStatusDownloaded(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electionEventId,
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String votingCardSetId) throws IOException, ResourceNotFoundException {

		validateUUID(electionEventId);
		validateUUID(votingCardSetId);

		ResponseEntity<Object> response;
		try {
			votingCardSetDownloadService.download(votingCardSetId, electionEventId);
			response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (final InvalidStatusTransitionException e) {
			LOGGER.info("Error trying to set voting card set status to downloaded. [electionEventId: {}, votingCardSetId: {}]", electionEventId,
					votingCardSetId, e);
			response = ResponseEntity.badRequest().build();
		}
		return response;
	}
}
