/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api.setup;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.cryptolib.api.exceptions.GeneralCryptoLibException;
import ch.post.it.evoting.securedatamanager.configuration.ElectoralBoardConfigService;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.domain.model.electoralauthority.ElectoralAuthoritySignInputData;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * The REST endpoint for accessing electoral authority data in Config phase.
 */
@RestController
@RequestMapping("/sdm-backend/electoralauthorities")
@Api(value = "Electoral authorities REST API")
@ConditionalOnProperty("role.isConfig")
public class ElectoralAuthorityConfigController {
	private static final Logger LOGGER = LoggerFactory.getLogger(ElectoralAuthorityConfigController.class);
	private final ElectoralBoardConfigService electoralBoardConfigService;

	public ElectoralAuthorityConfigController(final ElectoralBoardConfigService electoralBoardConfigService) {
		this.electoralBoardConfigService = electoralBoardConfigService;
	}

	/**
	 * Execute the constitute action: compute EB keypair from the EB passwords and persist the hashes of the EB passwords.
	 *
	 * @param electionEventId         the election event id.
	 * @param electoralAuthorityId    the electoral authority id.
	 * @param electoralBoardPasswords the list of members' password.
	 */
	@PostMapping(value = "/electionevent/{electionEventId}/electoralauthority/{electoralAuthorityId}/constitute")
	@ApiOperation(value = "Constitute Service", notes = "Service to compute a key pair and save hashes of passwords.")
	public ResponseEntity<Void> constitute(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electionEventId,
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electoralAuthorityId,
			@ApiParam(value = "electoralBoardPasswords", required = true)
			@RequestBody
			final List<char[]> electoralBoardPasswords) {

		validateUUID(electionEventId);
		validateUUID(electoralAuthorityId);
		checkNotNull(electoralBoardPasswords);
		checkArgument(electoralBoardPasswords.size() >= 2);

		LOGGER.info("Received request to constitute electoral board. [electionEventId: {}, electoralAuthorityId: {}]", electionEventId,
				electoralAuthorityId);

		if (electoralBoardConfigService.constitute(electionEventId, electoralAuthorityId, electoralBoardPasswords)) {
			return new ResponseEntity<>(HttpStatus.OK);
		}

		return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
	}

	/**
	 * Change the state of the electoral authority from constituted to signed for a given election event and electoral authority id.
	 *
	 * @param electionEventId      the election event id.
	 * @param electoralAuthorityId the electoral authority id.
	 * @return HTTP status code 200 - If the electoral authority is successfully signed. HTTP status code 404 - If the resource is not found. HTTP
	 * status code 412 - If the electoral authority is already signed.
	 */
	@PutMapping(value = "/electionevent/{electionEventId}/electoralauthority/{electoralAuthorityId}")
	@ApiOperation(value = "Sign electoral authority", notes = "Service to change the state of the electoral authority from constituted to signed for a given election event and electoral authority id..")
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Not Found"), @ApiResponse(code = 412, message = "Precondition Failed") })
	public ResponseEntity<Void> signElectoralAuthority(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electionEventId,
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electoralAuthorityId,
			@ApiParam(value = "ElectoralAuthoritySignInputData", required = true)
			@RequestBody
			final ElectoralAuthoritySignInputData inputData) {

		validateUUID(electionEventId);
		validateUUID(electoralAuthorityId);

		try {

			if (electoralBoardConfigService.sign(electionEventId, electoralAuthorityId, inputData.getPrivateKeyPEM())) {
				return new ResponseEntity<>(HttpStatus.OK);
			} else {
				LOGGER.error("An error occurred while fetching the given electoral authority to sign");
				return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
			}
		} catch (final ResourceNotFoundException e) {
			LOGGER.error("An error occurred while fetching the given electoral authority to sign", e);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (final GeneralCryptoLibException | IOException e) {
			LOGGER.error("An error occurred while signing the given electoral authority", e);
			return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
		}
	}
}
