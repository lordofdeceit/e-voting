/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.mixoffline;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import com.google.common.collect.Streams;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientMessage;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableBigInteger;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableByteArray;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.mixnet.Mixnet;
import ch.post.it.evoting.cryptoprimitives.mixnet.VerifiableShuffle;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.DecryptionProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.VerifiableDecryptions;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;
import ch.post.it.evoting.securedatamanager.services.application.service.BallotBoxService;

/**
 * Implements the MixDecOffline algorithm.
 */
@Service
public class MixDecOfflineAlgorithm {

	private final Hash hash;
	private final Mixnet mixnet;
	private final BallotBoxService ballotBoxService;
	private final ZeroKnowledgeProof zeroKnowledgeProof;

	public MixDecOfflineAlgorithm(
			final Hash hash,
			final Mixnet mixnet,
			final BallotBoxService ballotBoxService,
			final ZeroKnowledgeProof zeroKnowledgeProof) {
		this.hash = hash;
		this.mixnet = mixnet;
		this.ballotBoxService = ballotBoxService;
		this.zeroKnowledgeProof = zeroKnowledgeProof;
	}

	/**
	 * Shuffles (and re-encrypts) the votes and performs the final decryption.
	 *
	 * @param context the context data
	 * @param input   the input data
	 * @throws NullPointerException     if the context or the input is null.
	 * @throws IllegalArgumentException if
	 *                                  <ul>
	 *                                      <li>there are less than 2 votes.</li>
	 *                                      <li>the partially encrypted votes do not have exactly number of allowed write-ins + 1 elements</li>
	 *                                      <li>the partially encrypted votes have more elements than the electoral board keys</li>
	 *                                      <li>there is less than 2 electoral board members.</li>
	 *                                  </ul>
	 */
	@SuppressWarnings("java:S117")
	public MixDecOfflineOutput mixDecOffline(final MixDecOfflineContext context, final MixDecOfflineInput input) {
		checkNotNull(context);
		checkNotNull(input);

		// Cross-group checks
		checkArgument(context.getEncryptionGroup().equals(input.partiallyDecryptedVotes().getGroup()), "Context and input must have the same group.");

		final List<char[]> passwords = input.electoralBoardMembersPasswords().stream().map(char[]::clone).toList();

		// Context
		final GqGroup group = context.getEncryptionGroup();
		final BigInteger q = group.getQ();
		final String ee = context.getElectionEventId();
		final String bb = context.getBallotBoxId();
		final int delta_hat = context.getNumberOfAllowedWriteInsPlusOne();

		// Input
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> c_dec_4 = input.partiallyDecryptedVotes();
		final List<HashableByteArray> PW = passwords.stream()
				.map(PW_i -> HashableByteArray.from(StandardCharsets.UTF_8.encode(CharBuffer.wrap(PW_i)).array()))
				.toList();

		// Requires
		final int N_c_hat = c_dec_4.size();
		final int l = c_dec_4.getElementSize();
		final int k = PW.size();
		checkArgument(N_c_hat >= 2, "There must be at least 2 partially decrypted votes. [N_c_hat: %s]", N_c_hat);
		checkArgument(l == delta_hat,
				"The number of elements in the partially decrypted votes must correspond to the number of allowed write-ins + 1. [l: %s, delta_hat: %s]",
				l, delta_hat);
		checkArgument(k >= 2, "There must be at least 2 electoral board members. [k: %s]", k);
		// Corresponds to bb ∉ L_bb,Tally.
		checkArgument(!ballotBoxService.isDecrypted(bb), "The ballot box has already been decrypted. [ballotBoxId: %s]", bb);

		// Operation
		final List<ZqElement> EB_sk_elements = new ArrayList<>();
		for (int i = 0; i < delta_hat; i++) {
			final ZqElement EB_sk_i = hash.recursiveHashToZq(q,
					Streams.concat(Stream.of(HashableString.from("ElectoralBoardSecretKey"), HashableString.from(ee),
							HashableBigInteger.from(BigInteger.valueOf(i))), PW.stream()).collect(HashableList.toHashableList()));
			EB_sk_elements.add(EB_sk_i);
			// EB_pk is computed during the generation of the key pair (EB_pk, EB_sk).
		}
		final ElGamalMultiRecipientPrivateKey EB_sk = new ElGamalMultiRecipientPrivateKey(GroupVector.from(EB_sk_elements));
		final ElGamalMultiRecipientKeyPair EB_pk_EB_sk = ElGamalMultiRecipientKeyPair.from(EB_sk, group.getGenerator());
		final ElGamalMultiRecipientPublicKey EB_pk = EB_pk_EB_sk.getPublicKey();

		final List<String> i_aux = List.of(ee, bb, "MixDecOffline");
		final VerifiableShuffle c_mix_5_pi_mix_5 = mixnet.genVerifiableShuffle(c_dec_4, EB_pk);
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> c_mix_5 = c_mix_5_pi_mix_5.shuffledCiphertexts();
		final VerifiableDecryptions c_dec_5_pi_dec_5 = zeroKnowledgeProof.genVerifiableDecryptions(c_mix_5, EB_pk_EB_sk, i_aux);
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> c_dec_5 = c_dec_5_pi_dec_5.getCiphertexts();
		final GroupVector<DecryptionProof, ZqGroup> pi_dec_5 = c_dec_5_pi_dec_5.getDecryptionProofs();

		final GroupVector<ElGamalMultiRecipientMessage, GqGroup> m =
				IntStream.range(0, N_c_hat).mapToObj(i -> {
					final ElGamalMultiRecipientCiphertext c_dec_5_i = c_dec_5.get(i);
					return new ElGamalMultiRecipientMessage(c_dec_5_i.getPhis());
				}).collect(GroupVector.toGroupVector());

		// Corresponds to L_bb,Tally = L_bb,Tally ∪ bb.
		ballotBoxService.setDecrypted(bb);

		// Wipe the passwords after usage
		PW.forEach(PW_i -> Arrays.fill(PW_i.toHashableForm(), (byte) 0));
		passwords.forEach(PW_i -> Arrays.fill(PW_i, '0'));

		return new MixDecOfflineOutput(c_mix_5_pi_mix_5, m, pi_dec_5);
	}
}
