/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.mixoffline;

import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_ACTUAL_VOTING_OPTION_LENGTH;
import static ch.post.it.evoting.cryptoprimitives.utils.Validations.allEqual;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

import com.google.common.base.Preconditions;

import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;

/**
 * Regroups the outputs of the ProcessPlaintexts algorithm.
 */
public record ProcessPlaintextsAlgorithmOutput(GroupVector<GroupVector<PrimeGqElement, GqGroup>, GqGroup> L_votes,
											   List<List<String>> L_decodedVotes, List<List<String>> L_writeIns) {

	/**
	 * @param L_votes        the list of all selected encoded voting options. Must be non-null.
	 * @param L_decodedVotes the list of all selected decoded voting options. Must be non-null.
	 * @param L_writeIns     the list of all selected write-in voting options.
	 * @throws NullPointerException     if any of the lists is null.
	 * @throws IllegalArgumentException if
	 *                                  <ul>
	 *                                      <li>the lists don't have the same size.</li>
	 *                                      <li>in each list, the elements don't have the same size.</li>
	 *                                      <li>the size of the elements of the lists are not equal.</li>
	 *                                  </ul>
	 */
	@SuppressWarnings("java:S117")
	public ProcessPlaintextsAlgorithmOutput(final GroupVector<GroupVector<PrimeGqElement, GqGroup>, GqGroup> L_votes,
			final List<List<String>> L_decodedVotes, final List<List<String>> L_writeIns) {
		this.L_votes = checkNotNull(L_votes);

		List<List<String>> L_decodedVotesCopy = List.copyOf(checkNotNull(L_decodedVotes));

		L_decodedVotesCopy.forEach(Preconditions::checkNotNull);
		L_decodedVotesCopy = L_decodedVotesCopy.stream()
				.map(List::copyOf)
				.toList();

		L_decodedVotesCopy.forEach(decodedVotes -> decodedVotes.forEach(Preconditions::checkNotNull));
		this.L_decodedVotes = L_decodedVotesCopy;

		List<List<String>> L_writeInsCopy = List.copyOf(checkNotNull(L_writeIns));

		L_writeInsCopy.forEach(Preconditions::checkNotNull);
		L_writeInsCopy = L_writeInsCopy.stream()
				.map(List::copyOf)
				.toList();

		L_writeInsCopy.forEach(decodedVotes -> decodedVotes.forEach(Preconditions::checkNotNull));
		this.L_writeIns = L_writeInsCopy;

		checkArgument(this.L_votes.size() == this.L_decodedVotes.size(),
				"The size of the list of all selected encoded voting options and the size of the list of all selected decoded voting options must be equal. [L_votes_size: %s, L_decodedVotes_size: %s]",
				this.L_votes.size(), this.L_decodedVotes.size());

		checkArgument(this.L_votes.size() == this.L_writeIns.size(),
				"The size of the list of all selected encodedVotingOptions voting options and the size of the list of all selected decoded write-in votes must be equal. [L_votes_size: %s, L_writeIns_size: %s]",
				this.L_votes.size(), this.L_writeIns.size());

		// The same size for the elements of the L_votes is guaranteed by the GroupVector object.
		checkArgument(allEqual(this.L_decodedVotes.stream(), List::size), "All selected decoded voting options must have the same size.");

		if (!this.L_votes.isEmpty()) {
			final int encodedVotesSize = this.L_votes.getElementSize();
			final int decodedVotesSize = this.L_decodedVotes.get(0).size();
			checkArgument(encodedVotesSize == decodedVotesSize,
					"The size of the elements of the list of all selected encoded voting options must be equal to the size of the list of all selected decoded voting options. [encodedVotesSize: %s, decodedVotesSize: %s]",
					encodedVotesSize, decodedVotesSize);
		}

		final Predicate<String> isNotBlank = element -> !element.isBlank();
		final Predicate<String> isSmallerThanMaxLength = element -> element.length() <= MAXIMUM_ACTUAL_VOTING_OPTION_LENGTH;
		checkArgument(this.L_decodedVotes.stream()
						.flatMap(Collection::stream)
						.allMatch(isNotBlank.and(isSmallerThanMaxLength)),
				"The selected decoded voting options must be non-blank strings and their length must not exceed %s.",
				MAXIMUM_ACTUAL_VOTING_OPTION_LENGTH);

	}
}
