/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.commons.Constants.CONFIG_FILE_NAME_VERIFICATION_CARD_SECRET_KEY;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.VerificationCardSecretKey;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

/**
 * Allows performing operations with the verification card secret key. The verification card secret key is persisted/retrieved to/from the file system
 * of the SDM, in its workspace.
 */
@Repository
public class VerificationCardSecretKeyFileRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(VerificationCardSecretKeyFileRepository.class);
	final PathResolver pathResolver;
	private final ObjectMapper objectMapper;

	public VerificationCardSecretKeyFileRepository(final ObjectMapper objectMapper, final PathResolver pathResolver) {
		this.objectMapper = objectMapper;
		this.pathResolver = pathResolver;
	}

	/**
	 * Saves the verification card secret key for the given {@code electionEventId}, {@code verificationCardSetId} and {@code verificationCardId}.
	 *
	 * @param verificationCardSecretKey the verification card secret key to save.
	 * @throws NullPointerException if {@code verificationCardSecretKey} is null.
	 * @throws UncheckedIOException if the serialization of the verification card secret key fails.
	 */
	public Path save(final VerificationCardSecretKey verificationCardSecretKey) {
		checkNotNull(verificationCardSecretKey);

		final String electionEventId = verificationCardSecretKey.electionEventId();
		final String verificationCardSetId = verificationCardSecretKey.verificationCardSetId();
		final String verificationCardId = verificationCardSecretKey.verificationCardId();

		final Path payloadPath = getPayloadPath(electionEventId, verificationCardSetId, verificationCardId);
		try {
			final byte[] payloadBytes = objectMapper.writeValueAsBytes(verificationCardSecretKey);

			final Path writePath = Files.write(payloadPath, payloadBytes);
			LOGGER.debug(
					"Successfully persisted verification card secret key. [electionEventId: {}, verificationCardSetId: {}, verificationCardId: {}, path: {}]",
					electionEventId, verificationCardSetId, verificationCardId, payloadPath);

			return writePath;
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format(
							"Failed to serialize verification card secret key. [electionEventId: %s, verificationCardSetId: %s, verificationCardId: %s, path: %s]",
							electionEventId, verificationCardSetId, verificationCardId, payloadPath), e);
		}
	}

	/**
	 * Checks if the verification card secret key file exists for the given {@code electionEventId} and {@code verificationCardSetId}.
	 *
	 * @param electionEventId       the election event id to check. Must be non-null and a valid UUID.
	 * @param verificationCardSetId the verification card set id. Must be non-null and a valid UUID.
	 * @param verificationCardId    the verification card id. Must be non-null and a valid UUID.
	 * @return {@code true} if the verification card secret key file exists, {@code false} otherwise.
	 * @throws NullPointerException      if any input is null.
	 * @throws FailedValidationException if any input is not a valid UUID.
	 */
	public boolean existsById(final String electionEventId, final String verificationCardSetId, final String verificationCardId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		validateUUID(verificationCardId);

		final Path payloadPath = getPayloadPath(electionEventId, verificationCardSetId, verificationCardId);
		LOGGER.debug(
				"Checking verification card secret key file existence. [electionEventId: {}, verificationCardSetId: {}, verificationCardId: {}, path: {}]",
				electionEventId, verificationCardSetId, verificationCardId, payloadPath);

		return Files.exists(payloadPath);
	}

	/**
	 * Retrieves from the file system a verification card secret key by election event id and verification card set id.
	 *
	 * @param electionEventId       the election event id. Must be non-null and a valid UUID.
	 * @param verificationCardSetId the verification card set id. Must be non-null and a valid UUID.
	 * @param verificationCardId    the verification card id. Must be non-null and a valid UUID.
	 * @return the verification card secret key with the given id or {@link Optional#empty} if none found.
	 * @throws NullPointerException      if any input is null.
	 * @throws FailedValidationException if any input is not a valid UUID.
	 * @throws UncheckedIOException      if the deserialization of the verification card secret key fails.
	 */
	public Optional<VerificationCardSecretKey> findById(final String electionEventId, final String verificationCardSetId,
			final String verificationCardId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		validateUUID(verificationCardId);

		final Path payloadPath = getPayloadPath(electionEventId, verificationCardSetId, verificationCardId);
		if (!Files.exists(payloadPath)) {
			LOGGER.debug(
					"Requested verification card secret key does not exist. [electionEventId: {}, verificationCardSetId: {}, verificationCardId: {}, path: {}]",
					electionEventId, verificationCardSetId, verificationCardId, payloadPath);
			return Optional.empty();
		}

		try {
			return Optional.of(objectMapper.readValue(payloadPath.toFile(), VerificationCardSecretKey.class));
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format(
							"Failed to deserialize verification card secret key. [electionEventId: %s, verificationCardSetId: %s, verificationCardId: %s, path: %s]",
							electionEventId, verificationCardSetId, verificationCardId, payloadPath), e);
		}
	}

	private Path getPayloadPath(final String electionEventId, final String verificationCardSetId, final String verificationCardId) {
		final Path verificationCardSetPath = pathResolver.resolveOfflinePath(electionEventId)
				.resolve(Constants.CONFIG_VERIFICATION_CARD_SECRET_KEYS_DIRECTORY).resolve(verificationCardSetId);
		return verificationCardSetPath.resolve(String.format(CONFIG_FILE_NAME_VERIFICATION_CARD_SECRET_KEY, verificationCardId));
	}
}
