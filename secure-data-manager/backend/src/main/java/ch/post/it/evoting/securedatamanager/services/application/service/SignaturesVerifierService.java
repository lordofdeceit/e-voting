/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import java.io.IOException;
import java.nio.file.Path;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

import org.bouncycastle.cms.CMSException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptolib.api.exceptions.GeneralCryptoLibException;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.commons.PathResolver;
import ch.post.it.evoting.securedatamanager.config.commons.utils.SignatureVerifier;

/**
 * This implementation reads the prime numbers and encryption parameters' associated files and validates their signature.
 */
@Service
public class SignaturesVerifierService {

	@Autowired
	private SignatureVerifier signatureVerifier;

	@Autowired
	private PathResolver pathResolver;

	/**
	 * Verify a file and its chain. P7
	 *
	 * @return The trusted chain
	 */
	public Certificate[] verifyPkcs7(final Path filePath, final Path signaturePath)
			throws IOException, CMSException, GeneralCryptoLibException, CertificateException {
		final Path trustedCAPath = pathResolver.resolve(Constants.CONFIG_FILES_BASE_DIR, Constants.CONFIG_FILE_NAME_TRUSTED_CA_PEM);

		return signatureVerifier.verifyPkcs7(filePath, signaturePath, trustedCAPath);

	}
}
