/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.electoralauthority;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.PrivateKey;

import javax.json.JsonObject;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.domain.election.validation.ValidationResult;
import ch.post.it.evoting.securedatamanager.services.application.service.KeyStoreService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.RestClientService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.clients.VoteVerificationClient;

import retrofit2.Retrofit;

/**
 * Implements the repository for electoral authority uploadDataInContext.
 */
@Repository
public class ElectoralAuthorityUploadRepository {
	private static final Logger LOGGER = LoggerFactory.getLogger(ElectoralAuthorityUploadRepository.class);

	private static final String VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE = "The voting-portal connection is not enabled.";
	private static final String ADMIN_BOARD_ID_PARAM = "adminBoardId";
	private static final String ENDPOINT_CHECK_IF_EMPTY = "/electioneventdata/tenant/{tenantId}/electionevent/{electionEventId}/status";
	private static final String ELECTION_EVENT_ID_PARAM = "electionEventId";
	private static final String TENANT_ID_PARAM = "tenantId";
	private static final String UPLOAD_ELECTION_EVENT_DATA_URL = "/electioneventdata/tenant/{tenantId}/electionevent/{electionEventId}/adminboard/{adminBoardId}";

	private final KeyStoreService keystoreService;

	@Value("${voting.portal.enabled}")
	private boolean isVotingPortalEnabled;

	@Value("${AU_URL}")
	private String authenticationURL;

	@Value("${VV_URL}")
	private String voteVerificationUrl;

	@Value("${tenantID}")
	private String tenantId;

	public ElectoralAuthorityUploadRepository(final KeyStoreService keystoreService) {
		this.keystoreService = keystoreService;
	}

	/**
	 * Uploads the authentication context data.
	 *
	 * @param electionEventId the election event identifier.
	 * @param adminBoardId    the admin board id.
	 * @param json            the json object containing the authentication context data. @return True if the information is successfully uploaded.
	 *                        Otherwise, false.
	 */
	public boolean uploadAuthenticationContextData(final String electionEventId, final String adminBoardId, final JsonObject json) {

		checkState(isVotingPortalEnabled, VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		return uploadContextData(electionEventId, adminBoardId, json, authenticationURL);
	}

	private boolean uploadContextData(final String electionEventId, final String adminBoardId, final JsonObject json, final String url) {
		final WebTarget target = ClientBuilder.newClient().target(url);
		final Response response = target.path(UPLOAD_ELECTION_EVENT_DATA_URL).resolveTemplate(TENANT_ID_PARAM, tenantId)
				.resolveTemplate(ELECTION_EVENT_ID_PARAM, electionEventId).resolveTemplate(ADMIN_BOARD_ID_PARAM, adminBoardId).request()
				.post(Entity.entity(json.toString(), MediaType.APPLICATION_JSON_TYPE));

		return response.getStatus() == Response.Status.OK.getStatusCode();
	}

	/**
	 * Checks if there is information for the specific election event ID in the authentication (AU) context.
	 *
	 * @param electionEventId - identifier of the election event
	 * @return true if there is information, false otherwise.
	 */
	public boolean checkEmptyElectionEventDataInAU(final String electionEventId) {

		checkState(isVotingPortalEnabled, VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		return checkResult(electionEventId, authenticationURL);
	}

	private boolean checkResult(final String electionEvent, final String url) {
		boolean result = false;
		final WebTarget target = ClientBuilder.newClient().target(url + ENDPOINT_CHECK_IF_EMPTY);

		final Response response = target.resolveTemplate(TENANT_ID_PARAM, tenantId).resolveTemplate(ELECTION_EVENT_ID_PARAM, electionEvent)
				.request(MediaType.APPLICATION_JSON).get();

		final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();

		if (response.getStatus() == Response.Status.OK.getStatusCode()) {
			final String json = response.readEntity(String.class);
			try {
				result = objectMapper.readValue(json, ValidationResult.class).isResult();
			} catch (final IOException e) {
				LOGGER.error("Error checking if a ballot box is empty", e);
			}
		}
		return result;
	}

	/**
	 * Uploads the election event context payload to the vote verification.
	 *
	 * @param electionEventContextPayload the election event context payload to upload. Must be non-null.
	 * @return true if the upload was successful, false otherwise.
	 * @throws NullPointerException if the election event context payload is null.
	 */
	public boolean uploadElectionEventContextData(final ElectionEventContextPayload electionEventContextPayload) {
		checkNotNull(electionEventContextPayload);

		checkState(isVotingPortalEnabled, VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		final String electionEventId = electionEventContextPayload.getElectionEventContext().electionEventId();
		final retrofit2.Response<Void> response;
		try {
			response = getVoteVerificationClient().uploadElectionEventContext(electionEventId, electionEventContextPayload).execute();
		} catch (final IOException e) {
			throw new UncheckedIOException("Failed to communicate with vote verification.", e);
		}

		return response.isSuccessful();
	}

	/**
	 * Uploads the setup component public keys payload to the vote verification.
	 *
	 * @param setupComponentPublicKeysPayload the setup component public keys payload to upload. Must be non-null.
	 * @return true if the upload was successful, false otherwise.
	 * @throws NullPointerException if the setup component public keys payload is null.
	 */
	public boolean uploadSetupComponentPublicKeysData(final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload) {
		checkNotNull(setupComponentPublicKeysPayload);

		checkState(isVotingPortalEnabled, VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		final String electionEventId = setupComponentPublicKeysPayload.getElectionEventId();
		final retrofit2.Response<Void> response;
		try {
			response = getVoteVerificationClient().uploadSetupComponentPublicKeys(electionEventId, setupComponentPublicKeysPayload).execute();
		} catch (IOException e) {
			throw new UncheckedIOException("Failed to communicate with vote verification.", e);
		}

		return response.isSuccessful();
	}

	/**
	 * Gets the vote verification Retrofit client
	 */
	private VoteVerificationClient getVoteVerificationClient() {
		final PrivateKey privateKey = keystoreService.getPrivateKey();
		final Retrofit restAdapter = RestClientService.getInstance()
				.getRestClientWithInterceptorAndJacksonConverter(voteVerificationUrl, privateKey, "SECURE_DATA_MANAGER");
		return restAdapter.create(VoteVerificationClient.class);
	}
}
