/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.commons;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.util.encoders.DecoderException;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;

import com.google.common.annotations.VisibleForTesting;

/**
 * Validator for the RSA Public and Private Keys in PEM format.
 */
public class PemValidations {

	private PemValidations() {
		// Intentionally left blank.
	}

	/**
	 * Validate a RSA public or Private Keys in PEM format.
	 *
	 * @param toValidate the PEM to validate
	 * @throws IllegalArgumentException if the PEM is invalid or the keys are not RSA Public or Private Keys
	 */
	public static void validatePEM(final String toValidate) {
		checkNotNull(toValidate);
		checkArgument(StringUtils.isNotBlank(toValidate), "Certificate PEM parameter is required.");

		final PemObject pemObject = getPemObject(toValidate);
		final PemType pemType = PemType.of(pemObject.getType());

		if (!pemType.validate(pemObject)) {
			throw new IllegalArgumentException(String.format("The given PEM content is not valid. [string: %s]", toValidate));
		}
	}

	private static PemObject getPemObject(final String toValidate) {
		try (final Reader keyReader = new StringReader(toValidate);
				final PemReader pemReader = new PemReader(keyReader)) {

			final PemObject returnObject = pemReader.readPemObject();

			checkNotNull(returnObject, String.format("The given PEM has no content. [string: %s]", toValidate));

			return returnObject;
		} catch (final DecoderException ex) {
			throw new IllegalArgumentException(String.format("The given PEM content is not a valid Base64 encoded. [string: %s]", toValidate));
		} catch (final IOException ex) {
			throw new IllegalArgumentException(String.format("The given PEM content could not be parsed. [string: %s]", toValidate));
		}
	}

	@VisibleForTesting
	enum PemType {

		RSA_PUBLIC_KEY("PUBLIC KEY", PemType::validateRSAPublicKey),
		RSA_PRIVATE_KEY("RSA PRIVATE KEY", PemType::validateRSAPrivateKey);

		private final String name;
		private final Function<PemObject, Boolean> validator;

		PemType(final String name, final Function<PemObject, Boolean> validator) {
			this.name = name;
			this.validator = validator;
		}

		boolean validate(final PemObject pemObject) {
			return validator.apply(pemObject);
		}

		private static boolean validateRSAPublicKey(final PemObject pemObject) {
			final KeyFactory factory;
			try {
				factory = KeyFactory.getInstance("RSA");
			} catch (final NoSuchAlgorithmException ex) {
				throw new IllegalArgumentException("The RSA cryptographic algorithm is requested but is not available in the environment.");
			}

			try {
				final byte[] content = pemObject.getContent();
				final X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(content);

				factory.generatePublic(pubKeySpec);

				return true;
			} catch (final InvalidKeySpecException ex) {
				return false;
			}
		}

		private static boolean validateRSAPrivateKey(final PemObject pemObject) {
			final KeyFactory factory;
			try {
				factory = KeyFactory.getInstance("RSA");
			} catch (final NoSuchAlgorithmException ex) {
				throw new IllegalArgumentException("The RSA cryptographic algorithm is requested but is not available in the environment.");
			}

			try {
				final byte[] content = pemObject.getContent();
				final PKCS8EncodedKeySpec privKeySpec = new PKCS8EncodedKeySpec(content);

				factory.generatePrivate(privKeySpec);

				return true;
			} catch (final InvalidKeySpecException ex) {
				return false;
			}
		}

		public static PemType of(final String type) {
			for (final PemType pemType : values()) {
				if (type.equals(pemType.name)) {
					return pemType;
				}
			}
			throw new IllegalArgumentException(String.format("The given value is not an handled PEM type. [string: %s]", type));
		}
	}

}
