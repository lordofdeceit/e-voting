/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.configuration.SetupComponentCMTablePayload;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@Repository
public class SetupComponentCMTablePayloadFileRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(SetupComponentCMTablePayloadFileRepository.class);

	private static final String FILE_NAME = "setupComponentCMtablePayload" + Constants.JSON;

	private final ObjectMapper objectMapper;
	private final PathResolver payloadResolver;

	public SetupComponentCMTablePayloadFileRepository(
			final ObjectMapper objectMapper,
			final PathResolver payloadResolver) {
		this.objectMapper = objectMapper;
		this.payloadResolver = payloadResolver;
	}

	/**
	 * Saves the setup component CMTable payload to the filesystem for the given election event and verification card set.
	 *
	 * @return the path of the saved file.
	 * @throws NullPointerException     if any of the inputs is null.
	 * @throws IllegalArgumentException if any of the inputs is not valid.
	 * @see PathResolver to get the resolved file Path.
	 */
	public Path save(final SetupComponentCMTablePayload payload) {
		checkNotNull(payload);

		final String electionEventId = validateUUID(payload.getElectionEventId());
		final String verificationCardSetId = validateUUID(payload.getVerificationCardSetId());

		final Path payloadPath = payloadPath(electionEventId, verificationCardSetId);

		try {
			final byte[] payloadBytes = objectMapper.writeValueAsBytes(payload);
			final Path filePath = Files.write(payloadPath, payloadBytes);

			LOGGER.debug("Successfully persisted setup component CMTable payload. [electionEventId: {}, verificationCardSetId: {}]",
					electionEventId, verificationCardSetId);

			return filePath;
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Unable to write the setup component CMTable payload file. [electionEventId: %s, verificationCardSetId: %s]",
							electionEventId, verificationCardSetId), e);
		}
	}

	/**
	 * Retrieves from the file system a setup component CMTable payload by election event and verification card set ids.
	 *
	 * @param electionEventId       the payload's election event id.
	 * @param verificationCardSetId the payload's verification card set id.
	 * @return the setup component CMTable payload with the given ids or {@link Optional#empty} if not found.
	 * @throws FailedValidationException if {@code electionEventId} or {@code verificationCardSetId} is invalid.
	 * @throws UncheckedIOException      if the deserialization of the payload fails.
	 */
	public Optional<SetupComponentCMTablePayload> findByElectionEventIdAndVerificationCardSetId(final String electionEventId,
			final String verificationCardSetId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);

		final Path payloadPath = payloadPath(electionEventId, verificationCardSetId);

		if (!Files.exists(payloadPath)) {
			LOGGER.warn("Requested setup component CMTable payload does not exist. [electionEventId: {}, verificationCardSetId: {}]",
					electionEventId, verificationCardSetId);
			return Optional.empty();
		}

		try {
			return Optional.of(objectMapper.readValue(payloadPath.toFile(), SetupComponentCMTablePayload.class));
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Failed to deserialize setup component CMTable payload. [electionEventId: %s, verificationCardSetId: %s]",
							electionEventId, verificationCardSetId), e);
		}
	}

	/**
	 * Retrieves from the file system all setup component CMTable payloads by election event id.
	 *
	 * @param electionEventId the payload's election event id.
	 * @return the list setup component CMTable payloads for the given election event.
	 * @throws FailedValidationException if {@code electionEventId} is invalid.
	 * @throws UncheckedIOException      if the deserialization of the payload fails.
	 */
	public List<SetupComponentCMTablePayload> findAllByElectionEventId(final String electionEventId) {
		validateUUID(electionEventId);

		final Path voteVerificationPath = payloadResolver.resolveVoteVerificationPath(electionEventId);

		if (!Files.exists(voteVerificationPath)) {
			LOGGER.debug("Request vote verification path does not exist. [electionEventId: {}, path: {}]", electionEventId, voteVerificationPath);
			return Collections.emptyList();
		}

		try (final Stream<Path> paths = Files.walk(voteVerificationPath, 2)) {
			return paths.filter(p -> Files.isDirectory(p) && !voteVerificationPath.equals(p))
					.map(verificationCardSetFolderPath -> verificationCardSetFolderPath.resolve(FILE_NAME))
					.filter(Files::exists)
					.map(payloadPath -> {
						try {
							return objectMapper.readValue(payloadPath.toFile(), SetupComponentCMTablePayload.class);
						} catch (final IOException e) {
							throw new UncheckedIOException(String.format(
									"Failed to deserialize setup component CMTable payload. [electionEventId: %s, verificationCardSetId: %s]",
									electionEventId, payloadPath.getParent().getFileName()), e);
						}
					}).toList();
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Failed to walk vote verification directory. [electionEventId: %s, path: %s]", electionEventId,
							voteVerificationPath), e);
		}

	}

	private Path payloadPath(final String electionEventId, final String verificationCardSetId) {
		final Path verificationCardSetPath = payloadResolver.resolveVerificationCardSetPath(electionEventId, verificationCardSetId);
		return verificationCardSetPath.resolve(FILE_NAME);
	}
}
