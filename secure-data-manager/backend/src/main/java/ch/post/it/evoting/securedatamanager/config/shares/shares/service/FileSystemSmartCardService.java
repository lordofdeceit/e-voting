/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.config.shares.shares.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.post.it.evoting.cryptolib.api.secretsharing.Share;
import ch.post.it.evoting.securedatamanager.config.shares.shares.EncryptedShare;
import ch.post.it.evoting.securedatamanager.config.shares.shares.exception.SharesException;
import ch.post.it.evoting.securedatamanager.config.shares.shares.exception.SmartcardException;

/**
 * Implementation of the Smart Cards Services using the file system to write and read the Shares. The reason for the existence of this class is only
 * for providing a way to do and automatic e2e
 */
class FileSystemSmartCardService implements SmartCardService {



	Map<String, Integer> labelToIndexMap = new ConcurrentHashMap<>();
	final String SMARTCARDS_PROFILE_AUTOMATIC="e2e-automatic";

	private static final Logger LOGGER = LoggerFactory.getLogger(FileSystemSmartCardService.class);

	private static final Path SMART_CARD_FOLDER = Paths.get(System.getProperty("user.home") + "/sdm/smart-cards");
	private static final String SMARTCARDS_PROFILE = System.getenv("SMARTCARDS_PROFILE");

	private static final Path SMART_CARD_FILE_PATH = Paths.get(SMART_CARD_FOLDER.toString(), "/smart-card.b64");

	@Override
	public void write(final Share share, final String label, final Integer indexOfShare, final String oldPinPuk, final String newPinPuk, final PrivateKey signingPrivateKey)
			throws SmartcardException {

		Path path;
		try {
			if(SMARTCARDS_PROFILE_AUTOMATIC.equalsIgnoreCase(SMARTCARDS_PROFILE)) {
				path = SMART_CARD_FOLDER.resolve("smart-card.b64.ab"+indexOfShare);
				labelToIndexMap.put(label,indexOfShare);
			} else {
				path = SMART_CARD_FILE_PATH;
			}

			final EncryptedShare encryptedShare = new EncryptedShare(share, signingPrivateKey);
			Files.createDirectories(SMART_CARD_FOLDER);
			LOGGER.info("Saving smartcard to: {}", path.toAbsolutePath());
			Files.write(path, Arrays.asList(Base64.getEncoder().encodeToString(encryptedShare.getEncryptedShareBytes()),
					Base64.getEncoder().encodeToString(encryptedShare.getEncryptedShareSignature()),
					Base64.getEncoder().encodeToString(encryptedShare.getSecretKeyBytes()), label));
		} catch (final IOException e) {
			throw new SmartcardException(e.getMessage(), e);
		}
	}

	@Override
	public Share read(final String pin, final PublicKey signatureVerificationPublicKey) throws SmartcardException {

		try {
			final List<String> lines = readFile(SMART_CARD_FILE_PATH);

			final EncryptedShare es = new EncryptedShare(Base64.getDecoder().decode(lines.get(0)), Base64.getDecoder().decode(lines.get(1)),
					signatureVerificationPublicKey);
			return es.decrypt(Base64.getDecoder().decode(lines.get(2)));
		} catch (final SharesException | IOException e) {
			throw new SmartcardException(e.getMessage(), e);
		}
	}

	/**
	 * Checks the status of the inserted smartcard.
	 *
	 * @return true if the smartcard status is satisfactory, false otherwise
	 */
	@Override
	public boolean isSmartcardOk() {

		return Files.exists(SMART_CARD_FILE_PATH);
	}

	/**
	 * Read the smartcard label
	 *
	 * @return the label written to the smartcard
	 * @throws SmartcardException if an {@link IOException} occurs.
	 */
	@Override
	public String readSmartcardLabel(String label) throws SmartcardException {

		Path path;
		if(SMARTCARDS_PROFILE_AUTOMATIC.equalsIgnoreCase(SMARTCARDS_PROFILE)) {
			Integer indexOfShare = labelToIndexMap.get(label);
			path = SMART_CARD_FOLDER.resolve("smart-card.b64.ab"+indexOfShare);
		} else {
			path = SMART_CARD_FILE_PATH;
		}
		try {
			final List<String> lines = readFile(path);
			return lines.get(3);
		} catch (final IOException e) {
			throw new SmartcardException(e.getMessage(), e);
		}
	}

	private List<String> readFile(Path smartCardFilePath) throws IOException {
		return Files.readAllLines(smartCardFilePath);
	}

}
