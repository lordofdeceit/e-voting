/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.IntStream;

import com.google.common.collect.MoreCollectors;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionAttributes;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.cryptoprimitives.domain.election.Question;
import ch.post.it.evoting.domain.xmlns.evotingconfig.AuthorizationObjectType;
import ch.post.it.evoting.domain.xmlns.evotingconfig.AuthorizationType;
import ch.post.it.evoting.domain.xmlns.evotingconfig.BallotType;
import ch.post.it.evoting.domain.xmlns.evotingconfig.CandidatePositionType;
import ch.post.it.evoting.domain.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.domain.xmlns.evotingconfig.DomainOfInfluenceType;
import ch.post.it.evoting.domain.xmlns.evotingconfig.ElectionInformationType;
import ch.post.it.evoting.domain.xmlns.evotingconfig.VariantBallotType;
import ch.post.it.evoting.domain.xmlns.evotingconfig.VoteInformationType;
import ch.post.it.evoting.domain.xmlns.evotingprint.AnswerType;
import ch.post.it.evoting.domain.xmlns.evotingprint.CandidateListType;
import ch.post.it.evoting.domain.xmlns.evotingprint.CandidateType;
import ch.post.it.evoting.domain.xmlns.evotingprint.ContestType;
import ch.post.it.evoting.domain.xmlns.evotingprint.ElectionType;
import ch.post.it.evoting.domain.xmlns.evotingprint.ListType;
import ch.post.it.evoting.domain.xmlns.evotingprint.QuestionType;
import ch.post.it.evoting.domain.xmlns.evotingprint.VoteType;
import ch.post.it.evoting.domain.xmlns.evotingprint.VotingCardList;
import ch.post.it.evoting.domain.xmlns.evotingprint.VotingCardType;

/**
 * Maps to {@link VotingCardList}.
 */
public class VotingCardListMapper {

	private VotingCardListMapper() {
		// static usage only.
	}

	/**
	 * Returns the voting card list to be outputed in evoting-print.
	 *
	 * @param configuration         the configuration of the event.
	 * @param voterPrintingDataMap  the list of voter printing data.
	 * @param primesMappingTableMap the map of verificationCardSetId and primes mapping table.
	 * @param ballotMap             the map of verificationCardSetId and ballot.
	 * @return the root object VotingCardList of the evoting-print file.
	 * @throws NullPointerException if any input is null.
	 */
	public static VotingCardList toVotingCardList(final Configuration configuration, final Map<String, VoterPrintingData> voterPrintingDataMap,
			final Map<String, PrimesMappingTable> primesMappingTableMap, final Map<String, Ballot> ballotMap) {

		checkNotNull(configuration);
		checkNotNull(voterPrintingDataMap);
		checkNotNull(primesMappingTableMap);
		checkNotNull(ballotMap);

		// Prepare reused list
		final List<AuthorizationType> authorizationTypeList = configuration.getAuthorizations().getAuthorization();
		final List<ch.post.it.evoting.domain.xmlns.evotingconfig.VoteType> voteTypeList = configuration.getContest()
				.getVoteInformation().stream().parallel()
				.map(VoteInformationType::getVote)
				.toList();
		final List<ElectionInformationType> electionInformationTypeList = configuration.getContest().getElectionInformation();

		final List<VotingCardType> votingCardTypesList = configuration.getRegister().getVoter().stream().parallel()
				.map(voterType -> {
					final List<String> domainOfInfluenceList = authorizationTypeList.stream().parallel()
							.filter(authorizationType -> authorizationType.getAuthorizationIdentification().equals(voterType.getAuthorization()))
							.map(AuthorizationType::getAuthorizationObject)
							.flatMap(Collection::stream)
							.map(AuthorizationObjectType::getDomainOfInfluence)
							.map(DomainOfInfluenceType::getId)
							.toList();

					final String voterIdentification = voterType.getVoterIdentification();
					final VoterPrintingData voterPrintingData = voterPrintingDataMap.get(voterIdentification);
					final PrimesMappingTable primesMappingTable = primesMappingTableMap.get(voterPrintingData.verificationCardSetId());
					final Ballot ballot = ballotMap.get(voterPrintingData.verificationCardSetId());

					final VotingCardType votingCard = new VotingCardType();
					votingCard.setVoterIdentification(voterIdentification);
					votingCard.setVotingCardId(voterPrintingData.votingCardId());
					votingCard.setStartVotingKey(voterPrintingData.startVotingKey());
					votingCard.setBallotCastingKey(voterPrintingData.ballotCastingKey());
					votingCard.setVoteCastCode(voterPrintingData.voteCastCode());

					// Votes
					if (!voteTypeList.isEmpty()) {
						votingCard.setVote(toVotes(voteTypeList, domainOfInfluenceList, primesMappingTable,
								voterPrintingData.encodedVotingOptionToChoiceCodeMap()));
					}

					// Elections
					if (!electionInformationTypeList.isEmpty()) {
						votingCard.setElection(toElections(electionInformationTypeList, domainOfInfluenceList, primesMappingTable,
								voterPrintingData.encodedVotingOptionToChoiceCodeMap(), ballot));
					}

					return votingCard;
				})
				.toList();

		final ContestType contest = new ContestType();
		contest.setContestIdentification(configuration.getContest().getContestIdentification());
		contest.setVotingCard(votingCardTypesList);

		final VotingCardList votingCardList = new VotingCardList();
		votingCardList.setContest(contest);

		return votingCardList;
	}

	private static List<VoteType> toVotes(final List<ch.post.it.evoting.domain.xmlns.evotingconfig.VoteType> voteTypeList,
			final List<String> domainOfInfluenceList, final PrimesMappingTable primesMappingTable,
			final Map<Integer, String> encodedVotingOptionToChoiceCodeMap) {
		return voteTypeList.stream().parallel()
				.filter(configurationVoteType -> domainOfInfluenceList.contains(configurationVoteType.getDomainOfInfluence()))
				.map(configurationVoteType -> {
					final VoteType voteType = new VoteType();
					voteType.setVoteIdentification(configurationVoteType.getVoteIdentification());

					// StandardBallot
					voteType.getQuestion()
							.addAll(toStandardBallotQuestions(configurationVoteType, primesMappingTable, encodedVotingOptionToChoiceCodeMap));

					// VariantBallot -> StandardQuestion && VariantBallot -> TieBreakQuestion
					final List<VariantBallotType> variantBallotTypeList = configurationVoteType.getBallot().stream().parallel()
							.map(BallotType::getVariantBallot)
							.filter(Objects::nonNull)
							.toList();

					voteType.getQuestion()
							.addAll(toVariantBallotStandardQuestions(variantBallotTypeList, primesMappingTable, encodedVotingOptionToChoiceCodeMap));
					voteType.getQuestion()
							.addAll(toVariantBallotTieBreakQuestions(variantBallotTypeList, primesMappingTable, encodedVotingOptionToChoiceCodeMap));

					return voteType;
				})
				.toList();
	}

	private static List<QuestionType> toStandardBallotQuestions(final ch.post.it.evoting.domain.xmlns.evotingconfig.VoteType configurationVoteType,
			final PrimesMappingTable primesMappingTable,
			final Map<Integer, String> encodedVotingOptionToChoiceCodeMap) {
		return configurationVoteType.getBallot().stream().parallel()
				.map(BallotType::getStandardBallot)
				.filter(Objects::nonNull)
				.map(standardBallotType -> {
					final List<AnswerType> answers = standardBallotType.getAnswer().stream()
							.map(standardAnswerType -> {
								final String choiceCode = getChoiceCodeFromActualVotingOption(standardAnswerType.getAnswerIdentification(),
										primesMappingTable, encodedVotingOptionToChoiceCodeMap)
										.orElseThrow(() -> new IllegalStateException("Standard ballot answer choice code must not be null"));

								return new AnswerType()
										.withAnswerIdentification(standardAnswerType.getAnswerIdentification())
										.withChoiceCode(choiceCode);
							})
							.toList();

					return new QuestionType()
							.withQuestionIdentification(standardBallotType.getQuestionIdentification())
							.withAnswer(answers);
				})
				.toList();
	}

	private static List<QuestionType> toVariantBallotStandardQuestions(final List<VariantBallotType> variantBallotTypeList,
			final PrimesMappingTable primesMappingTable, final Map<Integer, String> encodedVotingOptionToChoiceCodeMap) {
		return variantBallotTypeList.stream()
				.map(VariantBallotType::getStandardQuestion)
				.flatMap(Collection::stream)
				.map(standardQuestionType -> {
					final List<AnswerType> answers = standardQuestionType.getAnswer().stream()
							.map(standardAnswerType -> {
								final String choiceCode = getChoiceCodeFromActualVotingOption(standardAnswerType.getAnswerIdentification(),
										primesMappingTable, encodedVotingOptionToChoiceCodeMap)
										.orElseThrow(() -> new IllegalStateException("Variant ballot standard answer choice code must not be null"));

								return new AnswerType()
										.withAnswerIdentification(standardAnswerType.getAnswerIdentification())
										.withChoiceCode(choiceCode);
							})
							.toList();

					return new QuestionType()
							.withQuestionIdentification(standardQuestionType.getQuestionIdentification())
							.withAnswer(answers);
				})
				.toList();
	}

	private static List<QuestionType> toVariantBallotTieBreakQuestions(final List<VariantBallotType> variantBallotTypeList,
			final PrimesMappingTable primesMappingTable, final Map<Integer, String> encodedVotingOptionToChoiceCodeMap) {
		return variantBallotTypeList.stream()
				.map(VariantBallotType::getTieBreakQuestion)
				.flatMap(Collection::stream)
				.map(tieBreakQuestionType -> {
					final List<AnswerType> answers = tieBreakQuestionType.getAnswer().stream()
							.map(tiebreakAnswerType -> {
								final String choiceCode = getChoiceCodeFromActualVotingOption(tiebreakAnswerType.getAnswerIdentification(),
										primesMappingTable, encodedVotingOptionToChoiceCodeMap)
										.orElseThrow(() -> new IllegalStateException("Variant ballot tiebreak answer choice code must not be null"));

								return new AnswerType()
										.withAnswerIdentification(tiebreakAnswerType.getAnswerIdentification())
										.withChoiceCode(choiceCode);
							})
							.toList();

					return new QuestionType()
							.withQuestionIdentification(tieBreakQuestionType.getQuestionIdentification())
							.withAnswer(answers);
				})
				.toList();
	}

	private static List<ElectionType> toElections(final List<ElectionInformationType> electionInformationTypeList,
			final List<String> domainOfInfluenceList, final PrimesMappingTable primesMappingTable,
			final Map<Integer, String> encodedVotingOptionToChoiceCodeMap, final Ballot ballot) {

		return electionInformationTypeList.stream().parallel()
				.filter(configurationElectionInformationType -> domainOfInfluenceList.contains(
						configurationElectionInformationType.getElection().getDomainOfInfluence()))
				.map(configurationElectionInformationType -> {
					final ElectionType electionType = new ElectionType();
					electionType.setElectionIdentification(configurationElectionInformationType.getElection().getElectionIdentification());

					// Candidates
					// Only for elections without defined lists
					if (configurationElectionInformationType.getList().stream()
							.allMatch(ch.post.it.evoting.domain.xmlns.evotingconfig.ListType::isListEmpty)) {
						electionType.getCandidate()
								.addAll(toCandidates(configurationElectionInformationType, primesMappingTable, encodedVotingOptionToChoiceCodeMap));
					}

					// Lists
					electionType.getList()
							.addAll(toLists(configurationElectionInformationType, primesMappingTable, encodedVotingOptionToChoiceCodeMap));

					// Write-ins
					if (configurationElectionInformationType.getElection().isWriteInsAllowed()) {
						electionType.getWriteInsChoiceCode()
								.addAll(toWriteInsChoiceCodes(configurationElectionInformationType.getElection().getElectionIdentification(),
										primesMappingTable, encodedVotingOptionToChoiceCodeMap, ballot));
					}

					return electionType;
				})
				.toList();
	}

	private static List<CandidateType> toCandidates(final ElectionInformationType configurationElectionInformationType,
			final PrimesMappingTable primesMappingTable, final Map<Integer, String> encodedVotingOptionToChoiceCodeMap) {
		return configurationElectionInformationType.getCandidate().stream()
				.map(configurationCandidateType -> {
					final List<String> choiceCodeList = getChoiceCodeListFromActualVotingOption(
							configurationCandidateType.getCandidateIdentification(), primesMappingTable, encodedVotingOptionToChoiceCodeMap);

					checkState(!choiceCodeList.isEmpty(), "Candidate choice code list must not be empty");

					return new CandidateType()
							.withCandidateIdentification(configurationCandidateType.getCandidateIdentification())
							.withChoiceCode(choiceCodeList);
				})
				.filter(candidateType -> !candidateType.getChoiceCode().isEmpty())
				.toList();
	}

	private static List<ListType> toLists(final ElectionInformationType configurationElectionInformationType,
			final PrimesMappingTable primesMappingTable, final Map<Integer, String> encodedVotingOptionToChoiceCodeMap) {
		return configurationElectionInformationType.getList().stream()
				.map(configListType -> {
					final String choiceCode = getChoiceCodeFromActualVotingOption(configListType.getListIdentification(), primesMappingTable,
							encodedVotingOptionToChoiceCodeMap)
							.orElse(null);

					final List<CandidateListType> candidateListTypes;
					if (!configListType.isListEmpty()) {
						candidateListTypes = toCandidateLists(configListType.getCandidatePosition(), primesMappingTable,
								encodedVotingOptionToChoiceCodeMap);
					} else {
						candidateListTypes = toCandidateEmptyLists(configListType.getCandidatePosition(), primesMappingTable,
								encodedVotingOptionToChoiceCodeMap);
					}

					return new ListType()
							.withListIdentification(configListType.getListIdentification())
							.withChoiceCode(choiceCode)
							.withCandidate(candidateListTypes);
				})
				.toList();
	}

	private static List<String> toWriteInsChoiceCodes(final String electionIdentification, final PrimesMappingTable primesMappingTable,
			final Map<Integer, String> encodedVotingOptionToChoiceCodeMap, final Ballot ballot) {

		final String alias = ballot.contests().stream()
				.filter(contest -> contest.alias().equals(electionIdentification))
				.map(contest -> contest.questions().stream()
						.filter(Question::isWriteIn)
						.map(Question::writeInAttribute)
						.findFirst()
						.map(writeInAttribute -> contest.attributes().stream()
								.filter(electionAttributes -> electionAttributes.getId().equals(writeInAttribute))
								.map(ElectionAttributes::getAlias)
								.collect(MoreCollectors.onlyElement()))
						.orElse(null))
				.collect(MoreCollectors.onlyElement());

		if (alias != null) {
			return getChoiceCodeListFromActualVotingOption(alias, primesMappingTable, encodedVotingOptionToChoiceCodeMap);
		} else {
			return List.of();
		}
	}

	private static List<CandidateListType> toCandidateLists(final List<CandidatePositionType> candidatePositionTypeList,
			final PrimesMappingTable primesMappingTable, final Map<Integer, String> encodedVotingOptionToChoiceCodeMap) {
		return candidatePositionTypeList.stream()
				.map(candidatePositionType -> {
					final List<String> choiceCodeList = getChoiceCodeListFromActualVotingOption(
							candidatePositionType.getCandidateListIdentification(), primesMappingTable, encodedVotingOptionToChoiceCodeMap);

					checkState(!choiceCodeList.isEmpty(), "CandidateList choice code list must not be empty");

					return new CandidateListType()
							.withCandidateListIdentification(candidatePositionType.getCandidateListIdentification())
							.withChoiceCode(choiceCodeList);
				})
				.toList();
	}

	private static List<CandidateListType> toCandidateEmptyLists(final List<CandidatePositionType> candidatePositionTypeList,
			final PrimesMappingTable primesMappingTable, final Map<Integer, String> encodedVotingOptionToChoiceCodeMap) {

		final List<String> choiceCodesList = getChoiceCodeListFromActualVotingOption(
				candidatePositionTypeList.get(0).getCandidateListIdentification(), primesMappingTable, encodedVotingOptionToChoiceCodeMap);

		return IntStream.range(0, candidatePositionTypeList.size())
				.mapToObj(i -> new CandidateListType()
						.withCandidateListIdentification(candidatePositionTypeList.get(i).getCandidateListIdentification())
						.withChoiceCode(choiceCodesList.get(i))
				)
				.toList();
	}

	private static Optional<String> getChoiceCodeFromActualVotingOption(final String actualVotingOption, final PrimesMappingTable primesMappingTable,
			final Map<Integer, String> encodedVotingOptionToChoiceCodeMap) {
		final List<String> choiceCodes = primesMappingTable.getPTable()
				.stream()
				.filter(primesMappingTableEntry -> primesMappingTableEntry.actualVotingOption().equals(actualVotingOption))
				.map(PrimesMappingTableEntry::encodedVotingOption)
				.map(encodedVotingOption -> encodedVotingOptionToChoiceCodeMap.get(encodedVotingOption.getValueAsInt()))
				.toList();

		if (choiceCodes.size() > 1) {
			throw new IllegalStateException(
					String.format("Unexpected number of choice code. [expectedNumber: 1, actualNumber: %s, actualVotingOption: %s]",
							choiceCodes.size(), actualVotingOption));
		}

		if (choiceCodes.isEmpty()) {
			return Optional.empty();
		}

		return Optional.of(choiceCodes.get(0));
	}

	private static List<String> getChoiceCodeListFromActualVotingOption(final String actualVotingOption,
			final PrimesMappingTable primesMappingTable, final Map<Integer, String> encodedVotingOptionToChoiceCodeMap) {

		return primesMappingTable.getPTable()
				.stream()
				.filter(primesMappingTableEntry -> primesMappingTableEntry.actualVotingOption().equals(actualVotingOption))
				.map(PrimesMappingTableEntry::encodedVotingOption)
				.map(encodedVotingOption -> encodedVotingOptionToChoiceCodeMap.get(encodedVotingOption.getValueAsInt()))
				.toList();
	}

}
