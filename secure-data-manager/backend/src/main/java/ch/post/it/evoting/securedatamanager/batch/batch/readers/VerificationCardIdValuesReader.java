/*
 * (c) Copyright 2022 Swiss Post Ltd
 */
package ch.post.it.evoting.securedatamanager.batch.batch.readers;

import java.util.concurrent.BlockingQueue;

import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.item.ItemReader;
import org.springframework.stereotype.Component;

import ch.post.it.evoting.securedatamanager.commons.domain.VerificationCardIdValues;

@Component
@JobScope
public class VerificationCardIdValuesReader implements ItemReader<VerificationCardIdValues> {

	private final BlockingQueue<VerificationCardIdValues> verificationCardIdValuesQueue;

	public VerificationCardIdValuesReader(final BlockingQueue<VerificationCardIdValues> verificationCardIdValuesQueue) {
		this.verificationCardIdValuesQueue = verificationCardIdValuesQueue;
	}

	@Override
	public VerificationCardIdValues read() throws InterruptedException {
		final VerificationCardIdValues verificationCardIdValues = verificationCardIdValuesQueue.take();
		if (!verificationCardIdValues.isPoisonPill()) {
			return verificationCardIdValues;
		} else {
			// Add again the poison pill to the queue to ensure all remaining
			// threads will receive it
			verificationCardIdValuesQueue.add(VerificationCardIdValues.poisonPill());
			return null;
		}
	}

}
