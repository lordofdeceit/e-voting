/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.config.commons.config.model.authentication;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

/**
 * Challenge information that may be used for the extended authentication phase
 */
public record ExtendedAuthChallenge(AuthenticationDerivedElement derivedChallenges, Optional<String> alias, byte[] salt) {

	public static ExtendedAuthChallenge of(final AuthenticationDerivedElement derivedChallenges, final Optional<String> alias, final byte[] salt) {
		return new ExtendedAuthChallenge(derivedChallenges, alias, salt);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ExtendedAuthChallenge that = (ExtendedAuthChallenge) o;
		return Objects.equals(derivedChallenges, that.derivedChallenges) && Objects.equals(alias, that.alias)
				&& Arrays.equals(salt, that.salt);
	}

	@Override
	public int hashCode() {
		int result = Objects.hash(derivedChallenges, alias);
		result = 31 * result + Arrays.hashCode(salt);
		return result;
	}

	@Override
	public String toString() {
		return "ExtendedAuthChallenge{" +
				"derivedChallenges=" + derivedChallenges +
				", alias=" + alias +
				", salt=" + Arrays.toString(salt) +
				'}';
	}
}
