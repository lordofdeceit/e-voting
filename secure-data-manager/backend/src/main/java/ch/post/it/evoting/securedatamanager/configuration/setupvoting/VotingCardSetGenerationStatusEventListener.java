/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.github.benmanes.caffeine.cache.Cache;

import ch.post.it.evoting.securedatamanager.services.domain.model.status.Status;
import ch.post.it.evoting.securedatamanager.services.infrastructure.service.ConfigurationEntityStatusService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

@Component
public class VotingCardSetGenerationStatusEventListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetGenerationStatusEventListener.class);
	private final VotingCardSetRepository votingCardSetRepository;
	private final Cache<String, GenerationStatus> generationStatusCache;
	private final PrintingDataGenerationService printingDataGenerationService;
	private final ConfigurationEntityStatusService configurationEntityStatusService;

	public VotingCardSetGenerationStatusEventListener(
			final VotingCardSetRepository votingCardSetRepository,
			final Cache<String, GenerationStatus> generationStatusCache,
			final PrintingDataGenerationService printingDataGenerationService,
			final ConfigurationEntityStatusService configurationEntityStatusService) {
		this.generationStatusCache = generationStatusCache;
		this.votingCardSetRepository = votingCardSetRepository;
		this.printingDataGenerationService = printingDataGenerationService;
		this.configurationEntityStatusService = configurationEntityStatusService;
	}

	@EventListener()
	public void listen(final VotingCardSetGenerationStatusEvent event) {
		final String electionEventId = event.getElectionEventId();
		final String votingCardSetId = event.getVotingCardSetId();
		final GenerationType processingEventType = event.getType();

		LOGGER.debug("Received spring generation event. [votingCardSetId: {}, generationStatusType: {}]", votingCardSetId, processingEventType);

		GenerationStatus status = generationStatusCache.getIfPresent(votingCardSetId);
		if (status != null) {
			status.updateStatus(event);
		} else {
			status = addStatus(event);
		}

		if (status.isAllCompleted()) {
			printingDataGenerationService.generate(electionEventId, votingCardSetId);
			configurationEntityStatusService.update(Status.GENERATED.name(), votingCardSetId, votingCardSetRepository);
			LOGGER.info("Voting card set is generated. [votingCardSetId: {}]", votingCardSetId);
		}

	}

	private GenerationStatus addStatus(final VotingCardSetGenerationStatusEvent event) {
		final GenerationStatus newStatus = new GenerationStatus();
		newStatus.updateStatus(event);
		generationStatusCache.put(event.getVotingCardSetId(), newStatus);
		return newStatus;
	}

}

