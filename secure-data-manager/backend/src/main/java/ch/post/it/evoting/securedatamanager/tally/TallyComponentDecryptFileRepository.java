/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.commons.Constants.TALLY_COMPONENT_DECRYPT_XML;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.nio.file.Files;
import java.nio.file.Path;
import java.security.SignatureException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.ChannelSecurityContextData;
import ch.post.it.evoting.domain.XsdConstants;
import ch.post.it.evoting.domain.hashable.HashableContestResultsFactory;
import ch.post.it.evoting.domain.xmlns.evotingdecrypt.Results;
import ch.post.it.evoting.securedatamanager.XmlFileRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@Repository
@ConditionalOnProperty("role.isTally")
public class TallyComponentDecryptFileRepository extends XmlFileRepository<Results> {
	private static final Logger LOGGER = LoggerFactory.getLogger(TallyComponentDecryptFileRepository.class);

	private final PathResolver pathResolver;
	private final SignatureKeystore<Alias> signatureKeystoreServiceSdmTally;

	public TallyComponentDecryptFileRepository(
			final PathResolver pathResolver,
			@Qualifier("keystoreServiceSdmTally")
			final SignatureKeystore<Alias> signatureKeystoreServiceSdmTally) {
		this.pathResolver = pathResolver;
		this.signatureKeystoreServiceSdmTally = signatureKeystoreServiceSdmTally;
	}

	/**
	 * Saves the given results in the {@value ch.post.it.evoting.securedatamanager.commons.Constants#TALLY_COMPONENT_DECRYPT_XML} file while
	 * validating it against the related {@value XsdConstants#TALLY_COMPONENT_DECRYPT_XSD}.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @param results         the results. Must be non-null.
	 * @throws NullPointerException      if any input is null.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 */
	public void save(final String electionEventId, final Results results) {
		validateUUID(electionEventId);
		checkNotNull(results);

		LOGGER.debug("Signing tally component decrypt... [electionEventId: {}]", electionEventId);

		final byte[] signature = getSignature(results, electionEventId);
		results.setSignature(signature);

		LOGGER.debug("Tally component decrypt successfully signed. [electionEventId: {}]", electionEventId);

		final String extendedFilename = getExtendedFilename(results.getContestIdentification());

		LOGGER.debug("Saving file... [path: {}, electionEventId: {}]", extendedFilename, electionEventId);

		final Path xmlFilePath = pathResolver.resolveOutputPath(electionEventId).resolve(extendedFilename);

		final Path writePath = write(results, XsdConstants.TALLY_COMPONENT_DECRYPT_XSD, xmlFilePath);

		LOGGER.debug("File successfully saved. [electionEventId: {}, path: {}]", electionEventId, writePath);
	}

	private byte[] getSignature(final Results results, final String electionEventId) {
		final Hashable hashable = HashableContestResultsFactory.fromResults(results);
		final Hashable additionalContextData = ChannelSecurityContextData.tallyComponentDecrypt();

		try {
			return signatureKeystoreServiceSdmTally.generateSignature(hashable, additionalContextData);
		} catch (final SignatureException e) {
			throw new IllegalStateException(String.format("Could not sign contest results. [electionEventId: %s]", electionEventId));
		}
	}

	/**
	 * Loads the tally component decrypt for the given election event id and validates it against the related XSD. The tally component decrypt is
	 * located in the {@value ch.post.it.evoting.securedatamanager.commons.Constants#TALLY_COMPONENT_DECRYPT_XML} file and the related XSD in
	 * {@value XsdConstants#TALLY_COMPONENT_DECRYPT_XSD}.
	 * <p>
	 * If the contest configuration file or the related XSD does not exist this method returns an empty Optional.
	 * <p>
	 * This method also validates the signature of the loaded file.
	 *
	 * @param electionEventId       the election event id. Must be non-null and a valid UUID.
	 * @param contestIdentification the contest identification. Must be non-null and non-blank.
	 * @return the tally component decrypt as an {@link Optional}.
	 * @throws NullPointerException      if the election event id of the contest identification is null.
	 * @throws IllegalArgumentException  if the contest identification is blank.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 * @throws IllegalStateException     if the signature is invalid, or it could not be verified.
	 */
	public Optional<Results> load(final String electionEventId, final String contestIdentification) {

		validateUUID(electionEventId);
		checkNotNull(contestIdentification);
		checkArgument(!contestIdentification.isBlank(), "The contest identification cannot be blank.");

		final String extendedFilename = getExtendedFilename(contestIdentification);

		LOGGER.debug("Loading file... [extendedFilename: {}, electionEventId: {}]", extendedFilename, electionEventId);

		final Path xmlFilePath = pathResolver.resolveOutputPath(electionEventId).resolve(extendedFilename);

		if (!Files.exists(xmlFilePath)) {
			LOGGER.debug("The requested file does not exist. [path: {}, electionEventId: {}]", xmlFilePath, electionEventId);
			return Optional.empty();
		}

		final Results results = read(xmlFilePath, XsdConstants.TALLY_COMPONENT_DECRYPT_XSD, Results.class);

		checkState(isSignatureValid(results, electionEventId));

		LOGGER.debug("File successfully loaded [path: {}, electionEventId: {}].", extendedFilename, electionEventId);

		return Optional.of(results);
	}

	private String getExtendedFilename(final String contestIdentification) {
		return String.format(TALLY_COMPONENT_DECRYPT_XML, contestIdentification);
	}

	private boolean isSignatureValid(final Results results, final String electionEventId) {
		final byte[] signature = results.getSignature();
		final Hashable hashable = HashableContestResultsFactory.fromResults(results);
		final Hashable additionalContextData = ChannelSecurityContextData.tallyComponentDecrypt();
		try {
			return signatureKeystoreServiceSdmTally.verifySignature(Alias.SDM_TALLY, hashable, additionalContextData, signature);
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Unable to verify tally component decrypt signature. [electionEventId: %s]", electionEventId), e);
		}
	}

}
