/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import java.io.IOException;
import java.security.PrivateKey;
import java.util.Base64;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.support.ApplicationContextFactory;
import org.springframework.batch.core.configuration.support.GenericApplicationContextFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.integration.util.CallerBlocksPolicy;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

import ch.post.it.evoting.cryptolib.api.asymmetric.AsymmetricServiceAPI;
import ch.post.it.evoting.cryptolib.api.certificates.CertificatesServiceAPI;
import ch.post.it.evoting.cryptolib.api.exceptions.GeneralCryptoLibException;
import ch.post.it.evoting.cryptolib.api.extendedkeystore.KeyStoreService;
import ch.post.it.evoting.cryptolib.api.primitives.PrimitivesServiceAPI;
import ch.post.it.evoting.cryptolib.api.securerandom.CryptoAPIRandomString;
import ch.post.it.evoting.cryptolib.api.services.ServiceFactory;
import ch.post.it.evoting.cryptolib.api.stores.StoresServiceAPI;
import ch.post.it.evoting.cryptolib.api.symmetric.SymmetricServiceAPI;
import ch.post.it.evoting.cryptolib.asymmetric.service.AsymmetricServiceFactoryHelper;
import ch.post.it.evoting.cryptolib.certificates.factory.X509CertificateGenerator;
import ch.post.it.evoting.cryptolib.certificates.factory.builders.CertificateDataBuilder;
import ch.post.it.evoting.cryptolib.certificates.service.CertificatesServiceFactoryHelper;
import ch.post.it.evoting.cryptolib.extendedkeystore.service.ExtendedKeyStoreServiceFactoryHelper;
import ch.post.it.evoting.cryptolib.keystore.KeyStoreReader;
import ch.post.it.evoting.cryptolib.primitives.service.PrimitivesServiceFactoryHelper;
import ch.post.it.evoting.cryptolib.stores.service.PollingStoresServiceFactory;
import ch.post.it.evoting.cryptolib.symmetric.service.SymmetricServiceFactoryHelper;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamal;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.mixnet.Mixnet;
import ch.post.it.evoting.cryptoprimitives.mixnet.MixnetFactory;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystoreFactory;
import ch.post.it.evoting.cryptoprimitives.symmetric.Symmetric;
import ch.post.it.evoting.cryptoprimitives.symmetric.SymmetricFactory;
import ch.post.it.evoting.cryptoprimitives.utils.KeyDerivation;
import ch.post.it.evoting.cryptoprimitives.utils.KeyDerivationFactory;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProofFactory;
import ch.post.it.evoting.securedatamanager.config.engine.commands.progress.ProgressManager;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.GenerationStatus;
import ch.post.it.evoting.securedatamanager.services.application.config.SmartCardConfig;
import ch.post.it.evoting.securedatamanager.services.infrastructure.DatabaseManager;
import ch.post.it.evoting.securedatamanager.services.infrastructure.DatabaseManagerFactory;
import ch.post.it.evoting.securedatamanager.services.infrastructure.RestClientService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.clients.MessageBrokerOrchestratorClient;

import retrofit2.Retrofit;

@Configuration
public class SecureDataManagerConfig {
	private static final Logger LOGGER = LoggerFactory.getLogger(SecureDataManagerConfig.class);

	@Value("${spring.profiles.active:}")
	private String activeProfiles;

	@PostConstruct
	private void postConstruct() {
		LOGGER.info("Spring active profiles : {}", activeProfiles);
	}

	@Bean
	@Profile("standard")
	public ApplicationContextFactory configStandardContext() {
		return new GenericApplicationContextFactory(ConfigJobConfigStandard.class);
	}

	@Bean
	@Profile("challenge")
	public ApplicationContextFactory configChallengeContext() {
		return new GenericApplicationContextFactory(ConfigJobConfigChallenge.class);
	}

	@Bean
	public ServiceFactory<AsymmetricServiceAPI> asymmetricServiceAPIServiceFactory(final GenericObjectPoolConfig genericObjectPoolConfig) {
		return AsymmetricServiceFactoryHelper.getFactoryOfThreadSafeServices(genericObjectPoolConfig);
	}

	@Bean
	public AsymmetricServiceAPI asymmetricServiceAPI(final ServiceFactory<AsymmetricServiceAPI> asymmetricServiceAPIServiceFactory)
			throws GeneralCryptoLibException {
		return asymmetricServiceAPIServiceFactory.create();
	}

	@Bean
	public ServiceFactory<KeyStoreService> extendedKeyStoreServiceAPIServiceFactory(final GenericObjectPoolConfig genericObjectPoolConfig) {
		return ExtendedKeyStoreServiceFactoryHelper.getFactoryOfThreadSafeServices(genericObjectPoolConfig);
	}

	@Bean
	public KeyStoreService extendedKeyStoreServiceAPI(final ServiceFactory<KeyStoreService> extendedKeyStoreServiceAPIServiceFactory)
			throws GeneralCryptoLibException {
		return extendedKeyStoreServiceAPIServiceFactory.create();
	}

	@Bean
	public GenericObjectPoolConfig genericObjectPoolConfig(final Environment env) {

		final GenericObjectPoolConfig genericObjectPoolConfig = new GenericObjectPoolConfig();
		genericObjectPoolConfig.setMaxTotal(Integer.parseInt(env.getProperty("services.cryptolib.pool.size")));
		genericObjectPoolConfig.setMaxIdle(Integer.parseInt(env.getProperty("services.cryptolib.timeout")));

		return genericObjectPoolConfig;
	}

	@Bean(initMethod = "createDatabase")
	public DatabaseManager databaseManager(final DatabaseManagerFactory databaseManagerFactory,
			@Value("${database.name}")
			final String databaseName) {
		return databaseManagerFactory.newDatabaseManager(databaseName);
	}

	@Bean
	public StoresServiceAPI storesService() {
		return new PollingStoresServiceFactory().create();
	}

	@Bean
	public ObjectMapper objectMapper() {
		return DomainObjectMapper.getNewInstance();
	}

	@Bean
	public ServiceFactory<CertificatesServiceAPI> certificatesServiceAPIServiceFactory(final GenericObjectPoolConfig genericObjectPoolConfig) {
		return CertificatesServiceFactoryHelper.getFactoryOfThreadSafeServices(genericObjectPoolConfig);
	}

	@Bean
	public CertificatesServiceAPI certificatesServiceAPI(final ServiceFactory<CertificatesServiceAPI> certificatesServiceAPIServiceFactory)
			throws GeneralCryptoLibException {
		return certificatesServiceAPIServiceFactory.create();
	}

	@Bean
	public CryptoAPIRandomString cryptoAPIRandomString(final PrimitivesServiceAPI primitivesServiceAPI) {
		return primitivesServiceAPI.get32CharAlphabetCryptoRandomString();
	}

	@Bean
	public CertificateDataBuilder certificateDataBuilder() {
		return new CertificateDataBuilder();
	}

	@Bean
	public X509CertificateGenerator certificatesGenerator(final CertificatesServiceAPI certificatesService,
			final CertificateDataBuilder certificateDataBuilder) {
		return new X509CertificateGenerator(certificatesService, certificateDataBuilder);
	}

	@Bean
	public KeyStoreReader keyStoreReader() {
		return new KeyStoreReader();
	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean
	public ProgressManager votersProgressManager() {
		return new ProgressManager();
	}

	@Bean
	public Base64.Encoder encoder() {
		return Base64.getEncoder();
	}

	@Bean
	@Qualifier("urlSafeEncoder")
	public Base64.Encoder urlSafeEncoder() {
		return Base64.getUrlEncoder();
	}

	@Bean
	public ObjectReader readerForDeserialization() {
		final ObjectMapper mapper = mapperForDeserialization();
		return mapper.reader();
	}

	private ObjectMapper mapperForDeserialization() {
		final ObjectMapper mapper = new ObjectMapper();
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		mapper.disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES);
		mapper.findAndRegisterModules();
		return mapper;
	}

	@Bean
	@ConditionalOnProperty(name = "smartcards.profile", havingValue = "e2e-automatic" )
	public SmartCardConfig getSmartCardConfigE2EAutomatic() {
		return SmartCardConfig.FILE;
	}

	@Bean
	@ConditionalOnProperty(name = "smartcards.profile", havingValue = "e2e-manual" )
	public SmartCardConfig getSmartCardConfigE2EManual() {
		return SmartCardConfig.FILE;
	}

	@Bean
	@ConditionalOnProperty(name = "smartcards.profile", havingValue = "default")
	public SmartCardConfig getSmartCardConfigDefault() {
		return SmartCardConfig.SMART_CARD;
	}

	@Bean
	public ServiceFactory<PrimitivesServiceAPI> primitivesServiceAPIServiceFactory(final GenericObjectPoolConfig genericObjectPoolConfig) {
		return PrimitivesServiceFactoryHelper.getFactoryOfThreadSafeServices(genericObjectPoolConfig);
	}

	@Bean
	public ServiceFactory<SymmetricServiceAPI> symmetricServiceAPIServiceFactory(final GenericObjectPoolConfig genericObjectPoolConfig) {
		return SymmetricServiceFactoryHelper.getFactoryOfThreadSafeServices(genericObjectPoolConfig);
	}

	@Bean
	public PrimitivesServiceAPI primitivesServiceAPI(final ServiceFactory<PrimitivesServiceAPI> primitivesServiceAPIServiceFactory)
			throws GeneralCryptoLibException {
		return primitivesServiceAPIServiceFactory.create();
	}

	@Bean
	public SymmetricServiceAPI symmetricServiceAPI(final ServiceFactory<SymmetricServiceAPI> symmetricServiceAPIServiceFactory)
			throws GeneralCryptoLibException {
		return symmetricServiceAPIServiceFactory.create();
	}

	@Bean
	public Random random() {
		return RandomFactory.createRandom();
	}

	@Bean
	public Mixnet mixnet() {
		return MixnetFactory.createMixnet();
	}

	@Bean
	public ZeroKnowledgeProof zeroKnowledgeProof() {
		return ZeroKnowledgeProofFactory.createZeroKnowledgeProof();
	}

	@Bean
	public Hash cryptoPrimitivesHash() {
		return HashFactory.createHash();
	}

	@Bean
	ElGamal elGamal() {
		return ElGamalFactory.createElGamal();
	}

	@Bean
	ch.post.it.evoting.cryptoprimitives.math.Base64 base64() {
		return BaseEncodingFactory.createBase64();
	}

	@Bean
	KeyDerivation keyDerivation() {
		return KeyDerivationFactory.createKeyDerivation();
	}

	@Bean
	Symmetric symmetric() {
		return SymmetricFactory.createSymmetric();
	}

	@Bean
	ExecutorService executorService() {
		final CallerBlocksPolicy policy = new CallerBlocksPolicy(1000000000);
		final int numberOfThreads = 20;
		return new ThreadPoolExecutor(numberOfThreads, numberOfThreads, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(), policy);
	}

	@Bean(name = "keystoreServiceSdmConfig")
	@ConditionalOnProperty("role.isConfig")
	SignatureKeystore<Alias> keystoreServiceSdmConfig(final KeystoreRepository repository) throws IOException {
		LOGGER.info("Create a sdm config keystore service.");

		final Alias sdmConfig = Alias.SDM_CONFIG;
		return SignatureKeystoreFactory.createSignatureKeystore(repository.getConfigKeyStore(), "PKCS12", repository.getConfigKeystorePassword(),
				keystore -> KeyStoreValidator.validateKeyStore(keystore, sdmConfig), sdmConfig);
	}

	@Bean(name = "keystoreServiceSdmTally")
	@ConditionalOnProperty("role.isTally")
	SignatureKeystore<Alias> keystoreServiceSdmTally(final KeystoreRepository repository) throws IOException {
		LOGGER.info("Create a sdm tally keystore service.");

		final Alias sdmTally = Alias.SDM_TALLY;
		return SignatureKeystoreFactory.createSignatureKeystore(repository.getTallyKeyStore(), "PKCS12", repository.getTallyKeystorePassword(),
				keystore -> KeyStoreValidator.validateKeyStore(keystore, sdmTally), sdmTally);
	}

	@Bean
	MessageBrokerOrchestratorClient getMessageBrokerOrchestratorClient(
			final ch.post.it.evoting.securedatamanager.services.application.service.KeyStoreService sdmKeyStoreService,
			@Value("${MO_URL}")
			final String orchestratorUrl) {
		final PrivateKey privateKey = sdmKeyStoreService.getPrivateKey();
		final Retrofit restAdapter = RestClientService.getInstance()
				.getRestClientWithInterceptorAndJacksonConverter(orchestratorUrl, privateKey, "SECURE_DATA_MANAGER");
		return restAdapter.create(MessageBrokerOrchestratorClient.class);
	}

	@Bean
	public Cache<String, GenerationStatus> createGenerationStatusCache() {
		return Caffeine.newBuilder()
				.build();
	}

	@Bean
	Argon2PasswordEncoder argon2PasswordEncoder(
			@Value("${argon2.saltLength}")
			final int saltLength,
			@Value("${argon2.hashLength}")
			final int hashLength,
			@Value("${argon2.parallelism}")
			final int parallelism,
			@Value("${argon2.memory}")
			final int memory,
			@Value("${argon2.iterations}")
			final int iterations) {
		return new Argon2PasswordEncoder(saltLength, hashLength, parallelism, memory, iterations);
	}
}
