/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

@JsonDeserialize(using = VerificationCardSecretKeyDeserializer.class)
public record VerificationCardSecretKey(String electionEventId,
										String verificationCardSetId,
										String verificationCardId,
										ElGamalMultiRecipientPrivateKey privateKey,
										GqGroup gqGroup) {
	public VerificationCardSecretKey {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		validateUUID(verificationCardId);
		checkNotNull(privateKey);
		checkNotNull(gqGroup);
		checkArgument(privateKey.getGroup().hasSameOrderAs(gqGroup), "The private key group must be of same order as the gqGroup.");
	}
}
