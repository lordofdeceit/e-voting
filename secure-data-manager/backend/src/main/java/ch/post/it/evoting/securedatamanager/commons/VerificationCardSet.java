/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.commons;

/**
 * Verification card set containing the election event identifier, the ballot box identifier, the voting card set identifier, the verification card
 * set identifier and the administration board identifier
 */
public record VerificationCardSet(String electionEventId, String ballotBoxId, String votingCardSetId, String verificationCardSetId,
								  String adminBoardId) {

}
