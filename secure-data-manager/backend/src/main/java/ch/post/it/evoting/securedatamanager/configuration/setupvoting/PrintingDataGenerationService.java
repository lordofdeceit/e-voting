/*
 * (c) Copyright 2022 Swiss Post Ltd
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkState;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.domain.configuration.ChoiceReturnCodeToEncodedVotingOptionEntry;
import ch.post.it.evoting.domain.configuration.VoterReturnCodes;
import ch.post.it.evoting.domain.configuration.VoterReturnCodesPayload;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.PathResolver;

@Service
public class PrintingDataGenerationService {

	// Partial printing line structure: (verificationCardSetId, votingCardId, (verificationCardId), electionEventId), (ballotCastingKey), (ballotId, [svk2, alias | startVotingKey])
	private static final String PARTIAL_PRINTING_LINE_REGEXP = "([0-9a-f]{32};[0-9a-f]{32};([0-9a-f]{32});[0-9a-f]{32});(\\d{9});(.*)";
	private static final Pattern PARTIAL_PRINTING_LINE_PATTERN = Pattern.compile(PARTIAL_PRINTING_LINE_REGEXP);

	private static final Logger LOGGER = LoggerFactory.getLogger(PrintingDataGenerationService.class);
	private static final String PARTIAL_PRINTING_DATA_FILE = Constants.PARTIAL_PRINTING_DATA + Constants.CSV;
	private static final String PRINTING_DATA_FILE = Constants.PRINTING_DATA + Constants.CSV;

	private final PathResolver pathResolver;
	private final VoterReturnCodesPayloadService voterReturnCodesPayloadService;

	public PrintingDataGenerationService(
			final PathResolver pathResolver,
			final VoterReturnCodesPayloadService voterReturnCodesPayloadService) {
		this.pathResolver = pathResolver;
		this.voterReturnCodesPayloadService = voterReturnCodesPayloadService;
	}

	/**
	 * Generate the printing data file.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @param votingCardSetId the voting card set id. Must be non-null and a valid UUID.
	 * @return a {@link ReturnCodesGenerationOutput} containing outputs of the CombineEncLongCodeShares and GenCMTable algorithms.
	 * @throws NullPointerException      if {@code electionEventId} or {@code votingCardSetId} is null.
	 * @throws FailedValidationException if {@code electionEventId} or {@code votingCardSetId} is invalid.
	 */
	public void generate(final String electionEventId, final String votingCardSetId) {
		validateUUID(electionEventId);
		validateUUID(votingCardSetId);

		final VoterReturnCodesPayload voterReturnCodesPayload = voterReturnCodesPayloadService.load(electionEventId, votingCardSetId);

		final Path printingFolderPath = getPrintingVotingCardSetPath(electionEventId, votingCardSetId);

		final Path partialPrintingDataPath = printingFolderPath.resolve(PARTIAL_PRINTING_DATA_FILE);
		final List<String> printingDataLines;
		try (final BufferedReader bufferedReader = Files.newBufferedReader(partialPrintingDataPath)) {
			printingDataLines = bufferedReader.lines().map(line -> {
				final Matcher m = PARTIAL_PRINTING_LINE_PATTERN.matcher(line);

				checkState(m.find(),
						"Partial printing data line does not match expected format. [electionEventId: %s, votingCardSetId: %s, line: %s]",
						electionEventId, votingCardSetId, line);

				final String lineStart = m.group(1);
				final String verificationCardId = m.group(2);
				final String ballotCastingKey = m.group(3);
				final String lineEnd = m.group(4);

				final VoterReturnCodes currentVoterReturnCodes = voterReturnCodesPayload.voterReturnCodes().stream()
						.filter(voterReturnCodes -> verificationCardId.equals(voterReturnCodes.verificationCardId()))
						.findFirst()
						.orElseThrow(() -> new IllegalStateException(
								String.format("VoterReturnCodes must not be null. [electionEventId: %s, votingCardSetId: %s, verificationCardId: %s]",
										electionEventId, votingCardSetId, verificationCardId)));

				return prepareLine(lineStart, ballotCastingKey, lineEnd, currentVoterReturnCodes);
			}).toList();
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Unable to read the partial printing data file. [electionEventId: %s, votingCardSetId: %s, path: %s]",
							electionEventId, votingCardSetId, partialPrintingDataPath), e);
		}

		final Path printingDataPath = printingFolderPath.resolve(PRINTING_DATA_FILE);
		try {
			Files.write(printingDataPath, (Iterable<String>) printingDataLines::iterator);
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Unable to write the printing data file. [electionEventId: %s, votingCardSetId: %s, path: %s]",
							electionEventId, votingCardSetId, printingDataPath), e);
		}

		LOGGER.info("Printing data file successfully written. [electionEventId: {}, votingCardSetId: {}]", electionEventId, votingCardSetId);
	}

	private String prepareLine(final String lineStart, final String ballotCastingKey, final String lineEnd, final VoterReturnCodes voterReturnCodes) {
		final String choiceReturnCodesToEncodedVotingOptions;
		choiceReturnCodesToEncodedVotingOptions = writeChoiceReturnCodesToEncodedVotingOptions(
				voterReturnCodes.choiceReturnCodesToEncodedVotingOptions());

		return String.format("%s;%s;%s;%s;%s", lineStart, choiceReturnCodesToEncodedVotingOptions, ballotCastingKey,
				voterReturnCodes.voteCastReturnCode(), lineEnd);
	}

	private Path getPrintingVotingCardSetPath(final String electionEventId, final String votingCardSetId) {
		final Path printingPath = pathResolver.resolvePrintingPath(electionEventId);
		return printingPath.resolve(votingCardSetId);
	}

	private String writeChoiceReturnCodesToEncodedVotingOptions(
			final GroupVector<ChoiceReturnCodeToEncodedVotingOptionEntry, GqGroup> choiceReturnCodesToEncodedVotingOptions) {
		final StringBuilder str = new StringBuilder();
		str.append("{");
		choiceReturnCodesToEncodedVotingOptions.forEach(entry -> {
			str.append("\"");
			str.append(entry.choiceReturnCode());
			str.append("\":");
			str.append(entry.encodedVotingOption().getValueAsInt());
			str.append(",");
		});
		str.deleteCharAt(str.lastIndexOf(","));
		str.append("}");

		return str.toString();
	}

}
