/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.IntStream;

import javax.json.JsonArray;
import javax.json.JsonObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.MoreCollectors;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationData;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SignedPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement.PrimeGqElementFactory;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.ChannelSecurityContextData;
import ch.post.it.evoting.domain.configuration.SetupComponentTallyDataPayload;
import ch.post.it.evoting.securedatamanager.SetupComponentTallyDataPayloadService;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.commons.VerificationCardSet;
import ch.post.it.evoting.securedatamanager.config.commons.utils.JsonUtils;
import ch.post.it.evoting.securedatamanager.configuration.EncryptionParametersConfigService;
import ch.post.it.evoting.securedatamanager.configuration.PrimesMappingTableService;
import ch.post.it.evoting.securedatamanager.configuration.StartVotingKeyService;
import ch.post.it.evoting.securedatamanager.configuration.VerificationCardSecretKeyService;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.BallotConfigService;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.GenVerDatAlgorithm;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.GenVerDatOutput;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.VerificationCardSecretKey;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.InvalidStatusTransitionException;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.Status;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballotbox.BallotBoxRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.PayloadStorageException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.SetupComponentVerificationDataPayloadFileSystemRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.service.ConfigurationEntityStatusService;

/**
 * Service that deals with the pre-computation of voting card sets.
 * <p>
 * The service invokes the GenVerDat algorithm described in the cryptographic protocol.
 */
@Service
@ConditionalOnProperty("role.isConfig")
public class VotingCardSetPrecomputationService extends BaseVotingCardSetService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetPrecomputationService.class);

	private final BallotConfigService ballotConfigService;
	private final GenVerDatAlgorithm genVerDatAlgorithm;
	private final IdleStatusService idleStatusService;
	private final ElectionEventService electionEventService;
	private final BallotBoxRepository ballotBoxRepository;
	private final EncryptionParametersConfigService encryptionParametersConfigService;
	private final ConfigurationEntityStatusService configurationEntityStatusService;
	private final SetupComponentVerificationDataPayloadFileSystemRepository setupComponentVerificationDataPayloadFileSystemRepository;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final StartVotingKeyService startVotingKeyService;
	private final PrimesMappingTableService primesMappingTableService;
	private final SetupComponentTallyDataPayloadService setupComponentTallyDataPayloadService;
	private final VerificationCardSecretKeyService verificationCardSecretKeyService;

	@Value("${choiceCodeGenerationChunkSize:100}")
	private int chunkSize;

	private ElGamalMultiRecipientPublicKey setupPublicKey;

	@Autowired
	public VotingCardSetPrecomputationService(
			final BallotConfigService ballotConfigService,
			final GenVerDatAlgorithm genVerDatAlgorithm,
			final IdleStatusService idleStatusService,
			final ElectionEventService electionEventService,
			final BallotBoxRepository ballotBoxRepository,
			final EncryptionParametersConfigService encryptionParametersConfigService,
			final ConfigurationEntityStatusService configurationEntityStatusService,
			final SetupComponentVerificationDataPayloadFileSystemRepository setupComponentVerificationDataPayloadFileSystemRepository,
			@Qualifier("keystoreServiceSdmConfig")
			final SignatureKeystore<Alias> signatureKeystoreService,
			final StartVotingKeyService startVotingKeyService,
			final PrimesMappingTableService primesMappingTableService, final VerificationCardSecretKeyService verificationCardSecretKeyService,
			final SetupComponentTallyDataPayloadService setupComponentTallyDataPayloadService) {

		this.ballotConfigService = ballotConfigService;
		this.genVerDatAlgorithm = genVerDatAlgorithm;
		this.idleStatusService = idleStatusService;
		this.electionEventService = electionEventService;
		this.ballotBoxRepository = ballotBoxRepository;
		this.encryptionParametersConfigService = encryptionParametersConfigService;
		this.configurationEntityStatusService = configurationEntityStatusService;
		this.setupComponentVerificationDataPayloadFileSystemRepository = setupComponentVerificationDataPayloadFileSystemRepository;
		this.signatureKeystoreService = signatureKeystoreService;
		this.startVotingKeyService = startVotingKeyService;
		this.primesMappingTableService = primesMappingTableService;
		this.verificationCardSecretKeyService = verificationCardSecretKeyService;
		this.setupComponentTallyDataPayloadService = setupComponentTallyDataPayloadService;
	}

	/**
	 * Pre-compute a voting card set.
	 *
	 * @throws InvalidStatusTransitionException If the original status does not allow pre-computing
	 * @throws PayloadStorageException          If an error occurs while storing the payload.
	 */
	public void precompute(final String votingCardSetId, final String electionEventId, final String adminBoardId)
			throws ResourceNotFoundException, InvalidStatusTransitionException, PayloadStorageException {

		validateUUID(votingCardSetId);
		validateUUID(electionEventId);

		if (!idleStatusService.getIdLock(votingCardSetId)) {
			return;
		}

		// Construct the matching verification card set context.
		final String verificationCardSetId = votingCardSetRepository.getVerificationCardSetId(votingCardSetId);
		final String ballotBoxId = votingCardSetRepository.getBallotBoxId(votingCardSetId);

		final VerificationCardSet precomputeContext = new VerificationCardSet(electionEventId, ballotBoxId, votingCardSetId, verificationCardSetId,
				adminBoardId);

		try {
			performPrecompute(precomputeContext);
		} finally {
			idleStatusService.freeIdLock(votingCardSetId);
		}
	}

	private void performPrecompute(final VerificationCardSet precomputeContext)
			throws ResourceNotFoundException, InvalidStatusTransitionException, PayloadStorageException {
		checkArgument(electionEventService.exists(precomputeContext.electionEventId()),
				"The election event ID of the given context does not exist. [electionEventId: %s]", precomputeContext.electionEventId());

		final String electionEventId = precomputeContext.electionEventId();
		final String verificationCardSetId = precomputeContext.verificationCardSetId();
		final String votingCardSetId = precomputeContext.votingCardSetId();

		LOGGER.info("Starting pre-computation of voting card set {}...", votingCardSetId);

		final Status fromStatus = Status.LOCKED;
		final Status toStatus = Status.PRECOMPUTED;

		checkVotingCardSetStatusTransition(electionEventId, votingCardSetId, fromStatus, toStatus);

		final int numberOfVotingCardsToGenerate = votingCardSetRepository.getNumberOfVotingCards(electionEventId, votingCardSetId);

		LOGGER.info("Generating {} voting cards for votingCardSetId {}...", numberOfVotingCardsToGenerate, votingCardSetId);

		// Get the setup public key.
		setupPublicKey = getSetupPublicKey(precomputeContext);
		final GqGroup electionGqGroup = setupPublicKey.getGroup();

		setupComponentVerificationDataPayloadFileSystemRepository.remove(electionEventId, verificationCardSetId);

		final Ballot ballot = getBallot(precomputeContext);

		// Retrieve the prime numbers representing the voting options.
		final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions = getEncodedVotingOptionsForBallot(ballot, electionGqGroup);

		// Retrieve the actual voting options.
		final List<String> actualVotingOptions = ballot.getActualVotingOptions();

		// Build payloads to request the return code generation (choice return codes and vote cast return code) from the control components.

		final List<GenVerDatOutput> genVerDatOutputs = new ArrayList<>();
		// Build full-sized chunks (i.e. with `chunkSize` elements)
		final int fullChunkCount = numberOfVotingCardsToGenerate / chunkSize;
		for (int i = 0; i < fullChunkCount; i++) {
			// Generate verification data for the chunk.
			genVerDatOutputs.add(generateRequestPayload(precomputeContext, encodedVotingOptions, actualVotingOptions, i, chunkSize));
		}

		// Build an eventual last chunk with the remaining elements.
		final int lastChunkSize = numberOfVotingCardsToGenerate % chunkSize;
		if (lastChunkSize > 0) {
			genVerDatOutputs.add(generateRequestPayload(precomputeContext, encodedVotingOptions, actualVotingOptions, fullChunkCount, lastChunkSize));
		}

		final String ballotBoxDefaultTitle = getBallotBoxDefaultTitle(precomputeContext);

		persistSetupComponentTallyDataPayload(genVerDatOutputs, electionEventId, verificationCardSetId, ballotBoxDefaultTitle, electionGqGroup);
		persistPrimesMappingTable(genVerDatOutputs, electionEventId, verificationCardSetId);

		LOGGER.info("Generation of {} voting cards for votingCardSetId {} successful.", numberOfVotingCardsToGenerate, votingCardSetId);

		// Update the voting card set status to 'pre-computed'.
		configurationEntityStatusService.update(toStatus.name(), votingCardSetId, votingCardSetRepository);
	}

	/**
	 * Generates the verification data with genVerData and creates the SetupComponentVerificationDataPayload for the request.
	 */
	private GenVerDatOutput generateRequestPayload(final VerificationCardSet precomputeContext,
			final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions,
			final List<String> actualVotingOptions, final int chunkId, final int chunkSize)
			throws PayloadStorageException {

		final String verificationCardSetId = precomputeContext.verificationCardSetId();

		// Generate verification data. Since we chunk the payloads, we are not directly working with the number of eligible voters (N_E),
		// but rather with the chunk size as input of the algorithm.
		LOGGER.debug("Generating verification data for verificationCardSet {} and chunk {}...", verificationCardSetId, chunkId);
		final GenVerDatOutput genVerDatOutput = genVerDatAlgorithm.genVerDat(chunkSize, encodedVotingOptions, actualVotingOptions, precomputeContext,
				setupPublicKey);

		// Persist casting keys, start voting keys, and verification card key pairs.
		persistBallotCastingKeys(precomputeContext, genVerDatOutput);
		persistStartVotingKeys(precomputeContext, genVerDatOutput);
		persistVerificationCardSecretKeys(precomputeContext, genVerDatOutput);

		LOGGER.debug("Generation of verification data for verificationCardSet {} and chunk {} successful. Creating payload...", verificationCardSetId,
				chunkId);

		// Create the payload.
		final SetupComponentVerificationDataPayload payload = createRequestPayload(precomputeContext, chunkId, genVerDatOutput);

		// Store the payload.
		setupComponentVerificationDataPayloadFileSystemRepository.store(payload);

		return genVerDatOutput;
	}

	private SetupComponentVerificationDataPayload createRequestPayload(final VerificationCardSet precomputeContext, final int chunkId,
			final GenVerDatOutput genVerDatOutput) {

		// Create payload.
		final List<SetupComponentVerificationData> returnCodeGenerationInputList = new ArrayList<>();
		for (int j = 0; j < genVerDatOutput.size(); j++) {
			final String verificationCardId = genVerDatOutput.getVerificationCardIds().get(j);
			final ElGamalMultiRecipientCiphertext encryptedHashedConfirmationKey = genVerDatOutput.getEncryptedHashedConfirmationKeys().get(j);
			final ElGamalMultiRecipientCiphertext encryptedHashedPartialChoiceReturnCodes = genVerDatOutput.getEncryptedHashedPartialChoiceReturnCodes()
					.get(j);
			final ElGamalMultiRecipientPublicKey verificationCardPublicKey = genVerDatOutput.getVerificationCardKeyPairs().get(j).getPublicKey();

			returnCodeGenerationInputList.add(
					new SetupComponentVerificationData(verificationCardId, encryptedHashedConfirmationKey, encryptedHashedPartialChoiceReturnCodes,
							verificationCardPublicKey));
		}
		final String electionEventId = precomputeContext.electionEventId();
		final String verificationCardSetId = precomputeContext.verificationCardSetId();
		final CombinedCorrectnessInformation combinedCorrectnessInformation = new CombinedCorrectnessInformation(getBallot(precomputeContext));

		final SetupComponentVerificationDataPayload payload = new SetupComponentVerificationDataPayload(electionEventId,
				verificationCardSetId, genVerDatOutput.getPartialChoiceReturnCodesAllowList(), chunkId, genVerDatOutput.getGroup(),
				returnCodeGenerationInputList, combinedCorrectnessInformation);

		// Sign the payload.
		LOGGER.debug("Signing payload... [electionEventId: {}, verificationCardSet: {}, chunkId {}]", electionEventId, verificationCardSetId,
				chunkId);

		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentVerificationData(electionEventId, verificationCardSetId);

		payload.setSignature(getPayloadSignature(payload, additionalContextData));

		LOGGER.debug("Payload successfully created and signed. [electionEventId: {}, verificationCardSet: {}, chunkId {}]", electionEventId,
				verificationCardSetId, chunkId);

		return payload;
	}

	/**
	 * Signs a setup component verification data payload.
	 *
	 * @param payload the payload to sign.
	 */
	private CryptoPrimitivesSignature getPayloadSignature(final SignedPayload payload, final Hashable additionalContextData) {

		final byte[] signature;
		try {
			signature = signatureKeystoreService.generateSignature(payload, additionalContextData);
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Failed to generate payload signature. [name: %s]", payload.getClass().getName()));
		}

		return new CryptoPrimitivesSignature(signature);
	}

	private void persistSetupComponentTallyDataPayload(final List<GenVerDatOutput> genVerDatOutputs, final String electionEventId,
			final String verificationCardSetId, final String ballotBoxDefaultTitle, final GqGroup encryptionGroup) {

		final SetupComponentTallyDataPayload setupComponentTallyDataPayload =
				new SetupComponentTallyDataPayload(electionEventId,
						verificationCardSetId,
						ballotBoxDefaultTitle,
						encryptionGroup,
						genVerDatOutputs.stream()
								.map(GenVerDatOutput::getVerificationCardIds)
								.flatMap(Collection::stream)
								.toList(),
						genVerDatOutputs.stream()
								.map(GenVerDatOutput::getVerificationCardKeyPairs)
								.flatMap(Collection::stream)
								.map(ElGamalMultiRecipientKeyPair::getPublicKey)
								.collect(GroupVector.toGroupVector())
				);

		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentTallyData(electionEventId, verificationCardSetId);
		setupComponentTallyDataPayload.setSignature(getPayloadSignature(setupComponentTallyDataPayload, additionalContextData));
		LOGGER.info("Successfully signed setup component tally data payload [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
				verificationCardSetId);

		setupComponentTallyDataPayloadService.save(setupComponentTallyDataPayload);
	}

	private void persistPrimesMappingTable(final List<GenVerDatOutput> genVerDatOutputs, final String electionEventId,
			final String verificationCardSetId) {

		// The pTable is the same across all chunks.
		final PrimesMappingTable primesMappingTable = genVerDatOutputs.get(0).getPTable();

		primesMappingTableService.save(electionEventId, verificationCardSetId, primesMappingTable);
		LOGGER.info("Successfully saved the primes mapping table. [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
				verificationCardSetId);
	}

	@VisibleForTesting
	void persistVerificationCardSecretKeys(final VerificationCardSet precomputeContext, final GenVerDatOutput genVerDatOutput) {
		final String electionEventId = precomputeContext.electionEventId();
		final String verificationCardSetId = precomputeContext.verificationCardSetId();

		final File verificationCardSecretKeysFile = pathResolver.resolve(Constants.CONFIG_FILES_BASE_DIR).resolve(electionEventId)
				.resolve(Constants.CONFIG_DIR_NAME_OFFLINE).resolve(Constants.CONFIG_VERIFICATION_CARD_SECRET_KEYS_DIRECTORY)
				.resolve(verificationCardSetId).toFile();

		checkState(verificationCardSecretKeysFile.exists() || verificationCardSecretKeysFile.mkdirs(),
				"An error occurred while creating the directory for saving the verification card secret key. [electionEventId: %s, verificationCardSetId: %s]",
				electionEventId, verificationCardSetId);

		IntStream.range(0, genVerDatOutput.size()).forEach(i -> {
			final String verificationCardId = genVerDatOutput.getVerificationCardIds().get(i);
			final ElGamalMultiRecipientKeyPair keyPair = genVerDatOutput.getVerificationCardKeyPairs().get(i);
			final VerificationCardSecretKey verificationCardSecretKey = new VerificationCardSecretKey(electionEventId, verificationCardSetId,
					verificationCardId, keyPair.getPrivateKey(), keyPair.getGroup());

			verificationCardSecretKeyService.save(verificationCardSecretKey);
		});

	}

	@VisibleForTesting
	void persistBallotCastingKeys(final VerificationCardSet precomputeContext, final GenVerDatOutput genVerDatOutput) {
		final String electionEventId = precomputeContext.electionEventId();
		final String verificationCardSetId = precomputeContext.verificationCardSetId();

		LOGGER.info("Persisting the ballot casting keys for for the electionEventId {} and verificationCardSetId {}.", electionEventId,
				verificationCardSetId);

		final Path ballotCastingKeysDirPath = pathResolver.resolve(Constants.CONFIG_FILES_BASE_DIR).resolve(electionEventId)
				.resolve(Constants.CONFIG_DIR_NAME_OFFLINE).resolve(Constants.CONFIG_BALLOT_CASTING_KEYS_DIRECTORY)
				.resolve(verificationCardSetId);

		final File ballotCastingKeyPairsFile = ballotCastingKeysDirPath.toFile();

		if (!ballotCastingKeyPairsFile.exists() && !ballotCastingKeyPairsFile.mkdirs()) {
			throw new IllegalStateException(String.format(
					"An error occurred while creating the directory for saving the ballot casting keys for electionEventId %s and verificationCardSetId %s",
					electionEventId, verificationCardSetId));
		}

		for (int i = 0; i < genVerDatOutput.size(); i++) {
			final String verificationCardId = genVerDatOutput.getVerificationCardIds().get(i);
			final String BCK = genVerDatOutput.getBallotCastingKeys().get(i);
			try {
				Files.writeString(ballotCastingKeysDirPath.resolve(verificationCardId + Constants.KEY), BCK);
			} catch (final IOException e) {
				throw new IllegalStateException(String.format(
						"An error occurred while saving ballot casting key for electionEventId %s, verificationCardSetId %s and verificationCardId %s.",
						electionEventId, verificationCardSetId, verificationCardId), e);
			}
		}
	}

	@VisibleForTesting
	void persistStartVotingKeys(final VerificationCardSet precomputeContext, final GenVerDatOutput genVerDatOutput) {
		final String electionEventId = precomputeContext.electionEventId();
		final String verificationCardSetId = precomputeContext.verificationCardSetId();

		LOGGER.info("Persisting the start voting keys for for the electionEventId {} and verificationCardSetId {}.",
				electionEventId, verificationCardSetId);

		final Path startVotingKeysDirPath = pathResolver.resolve(Constants.CONFIG_FILES_BASE_DIR).resolve(electionEventId)
				.resolve(Constants.CONFIG_DIR_NAME_OFFLINE).resolve(Constants.CONFIG_START_VOTING_KEYS_DIRECTORY)
				.resolve(verificationCardSetId);

		final File startVotingKeyPairsFile = startVotingKeysDirPath.toFile();

		if (!startVotingKeyPairsFile.exists() && !startVotingKeyPairsFile.mkdirs()) {
			throw new IllegalStateException(String.format(
					"An error occurred while creating the directory for saving the start voting keys for electionEventId %s and verificationCardSetId %s",
					electionEventId, verificationCardSetId));
		}

		for (int i = 0; i < genVerDatOutput.size(); i++) {
			final String verificationCardId = genVerDatOutput.getVerificationCardIds().get(i);
			final String startVotingKey = genVerDatOutput.getStartVotingKeys().get(i);

			startVotingKeyService.save(electionEventId, verificationCardSetId, verificationCardId,
					startVotingKey);
		}
	}

	private Ballot getBallot(final VerificationCardSet precomputeContext) {
		final String ballotId = ballotBoxRepository.getBallotId(precomputeContext.ballotBoxId());
		return ballotConfigService.getBallot(precomputeContext.electionEventId(), ballotId);
	}

	private String getBallotBoxDefaultTitle(final VerificationCardSet precomputeContext) {
		final String electionEventId = precomputeContext.electionEventId();
		final String ballotBoxId = precomputeContext.ballotBoxId();

		final JsonArray ballotBoxesJsonArray = JsonUtils.getJsonObject(ballotBoxRepository.listByElectionEvent(electionEventId))
				.getJsonArray(JsonConstants.RESULT);

		return ballotBoxesJsonArray.stream()
				.map(JsonObject.class::cast)
				.filter(ballotBoxJsonObject -> ballotBoxId.equals(ballotBoxJsonObject.getString(JsonConstants.ID)))
				.map(ballotBoxJsonObject -> ballotBoxJsonObject.getString(JsonConstants.DEFAULT_TITLE))
				.collect(MoreCollectors.onlyElement());
	}

	/**
	 * Gets the ballot's encoded voting options (prime numbers) as {@link PrimeGqElement}s.
	 *
	 * @param ballot the ballot.
	 * @param group  the election event group.
	 * @return a stream of encrypted representations
	 */
	private GroupVector<PrimeGqElement, GqGroup> getEncodedVotingOptionsForBallot(final Ballot ballot, final GqGroup group) {
		return ballot.getEncodedVotingOptions().stream()
				.map(value -> PrimeGqElementFactory.fromValue(value, group))
				.collect(GroupVector.toGroupVector());
	}

	/**
	 * Read the setup secret key from the file system and derive the setup public key from it. To reconstruct the secret key, the encryption
	 * parameters are first read from the file system.
	 *
	 * @return the setup public key.
	 */
	private ElGamalMultiRecipientPublicKey getSetupPublicKey(final VerificationCardSet precomputeContext) {
		final GqGroup gqGroup = encryptionParametersConfigService.loadEncryptionGroup(precomputeContext.electionEventId());

		try {
			// Read the setup secret key from file system.
			final ElGamalMultiRecipientPrivateKey setupSecretKey = objectMapper.reader().withAttribute("group", gqGroup).readValue(
					pathResolver.resolve(Constants.CONFIG_FILES_BASE_DIR, precomputeContext.electionEventId(), Constants.CONFIG_DIR_NAME_OFFLINE,
							Constants.SETUP_SECRET_KEY_FILE_NAME).toFile(), ElGamalMultiRecipientPrivateKey.class);

			// Create key pair from the setup secret key and retrieve corresponding setup public key.
			return ElGamalMultiRecipientKeyPair.from(setupSecretKey, gqGroup.getGenerator()).getPublicKey();
		} catch (final IOException e) {
			throw new UncheckedIOException("Failed to deserialize setup secret key.", e);
		}
	}

}

