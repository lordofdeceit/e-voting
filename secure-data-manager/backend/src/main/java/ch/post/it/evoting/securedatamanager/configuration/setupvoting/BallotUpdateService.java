/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.StringReader;
import java.io.UncheckedIOException;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.MoreCollectors;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.domain.election.Contest;
import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionAttributes;
import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionOption;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.EncryptionParametersPayload;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.domain.xmlns.evotingconfig.BallotType;
import ch.post.it.evoting.domain.xmlns.evotingconfig.CandidatePositionType;
import ch.post.it.evoting.domain.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.domain.xmlns.evotingconfig.ElectionInformationType;
import ch.post.it.evoting.domain.xmlns.evotingconfig.ListType;
import ch.post.it.evoting.domain.xmlns.evotingconfig.StandardAnswerType;
import ch.post.it.evoting.domain.xmlns.evotingconfig.StandardBallotType;
import ch.post.it.evoting.domain.xmlns.evotingconfig.StandardQuestionType;
import ch.post.it.evoting.domain.xmlns.evotingconfig.TieBreakQuestionType;
import ch.post.it.evoting.domain.xmlns.evotingconfig.TiebreakAnswerType;
import ch.post.it.evoting.domain.xmlns.evotingconfig.VariantBallotType;
import ch.post.it.evoting.domain.xmlns.evotingconfig.VoteInformationType;
import ch.post.it.evoting.domain.xmlns.evotingconfig.VoteType;
import ch.post.it.evoting.securedatamanager.EncryptionParametersPayloadService;

/**
 * Service for updating information of the ballots for a given election event.
 */
@Service
@ConditionalOnProperty("role.isConfig")
public class BallotUpdateService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BallotUpdateService.class);

	private final ObjectMapper objectMapper;
	private final CantonConfigConfigService cantonConfigConfigService;

	private final EncryptionParametersPayloadService encryptionParametersPayloadService;
	private final Map<String, SmallPrimesTuple> electionEventRepresentationMap = new ConcurrentHashMap<>();
	private final Map<String, String> optionIdRepresentationMap = new ConcurrentHashMap<>();

	public BallotUpdateService(final EncryptionParametersPayloadService encryptionParametersPayloadService,
			final ObjectMapper objectMapper, final CantonConfigConfigService cantonConfigConfigService) {
		this.encryptionParametersPayloadService = encryptionParametersPayloadService;
		this.objectMapper = objectMapper;
		this.cantonConfigConfigService = cantonConfigConfigService;
	}

	/**
	 * Update the options' representation value of the ballot.
	 *
	 * @param electionEventId The electionEventId
	 * @param ballot          The ballot to be updated.
	 * @return Return a JsonObject with the updated ballot.
	 */
	public JsonObject updateOptionsRepresentation(final String electionEventId, final JsonObject ballot) {
		validateUUID(electionEventId);
		checkNotNull(ballot);

		final SmallPrimesTuple smallPrimesTuple = getSmallPrimesTuple(electionEventId);

		final JsonObject updateBallot = updateBallotOptions(ballot, smallPrimesTuple);
		LOGGER.info("Update ballot options' representation. [electionEventId: {}, ballotId: {}]", electionEventId, ballot.getString("id"));

		return updateBallot;
	}

	/**
	 * Update the blank voting options of the ballot to match their identifier defined in the configuration.
	 *
	 * @param electionEventId  the election event id. Must be non-null and a valid UUID.
	 * @param ballotJsonObject the ballot. Must be non-null.
	 * @return the updated ballot.
	 * @throws NullPointerException      if any input is null.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 */
	public JsonObject updateBlankVotingOptionsAlias(final String electionEventId, final JsonObject ballotJsonObject) {
		validateUUID(electionEventId);
		checkNotNull(ballotJsonObject);

		final Configuration configuration = cantonConfigConfigService.load(electionEventId);
		final Ballot ballot = toBallot(ballotJsonObject);

		ballot.contests().forEach(contest -> {
			final String template = contest.template();

			contest.attributes().stream()
					.filter(attribute -> attribute.getAlias().startsWith("BLANK_"))
					.forEach(attribute -> {

						final String attributeId = attribute.getId();

						checkState(attribute.getRelated().size() == 1,
								"A blank voting option must have a single related field entry. [attributedId: %s]", attributeId);

						final String relatedAttributeId = attribute.getRelated().get(0);

						final String aliasOfRelatedAttribute = contest.attributes().stream()
								.filter(relatedAttribute -> relatedAttribute.getId().equals(relatedAttributeId))
								.map(ElectionAttributes::getAlias)
								.collect(MoreCollectors.onlyElement());

						attribute.setAlias(
								switch (template) {
									case Contest.OPTIONS_TEMPLATE ->
											getUpdatedAliasForOptionsContestBlankAttributeAlias(aliasOfRelatedAttribute, configuration);
									case Contest.LISTS_AND_CANDIDATES_TEMPLATE ->
											getUpdatedAliasForListsAndCandidatesContestBlankAttributeAlias(aliasOfRelatedAttribute, contest,
													configuration);
									default -> throw new IllegalArgumentException(
											String.format("Contests with template \"%s\" are not supported. [contestId: %s]", template,
													contest.id()));
								});
					});

		});

		return toJsonObject(ballot);
	}

	private String getUpdatedAliasForListsAndCandidatesContestBlankAttributeAlias(final String aliasOfRelatedAttribute, final Contest contest,
			final Configuration configuration) {

		final String contestAlias = contest.alias();
		final ElectionInformationType electionInformationMatch = configuration.getContest().getElectionInformation().stream()
				.filter(electionInformation -> electionInformation.getElection().getElectionIdentification().equals(contestAlias))
				.collect(MoreCollectors.onlyElement());

		final ListType emptyList = electionInformationMatch.getList().stream()
				.filter(ListType::isListEmpty)
				.collect(MoreCollectors.onlyElement());

		return switch (aliasOfRelatedAttribute) {
			case "lists" -> emptyList.getListIdentification();
			case "candidates" -> emptyList.getCandidatePosition().stream()
					.filter(candidatePositionType -> candidatePositionType.getPositionOnList().equals(BigInteger.ONE))
					.map(CandidatePositionType::getCandidateListIdentification)
					.collect(MoreCollectors.onlyElement());
			default -> throw new IllegalStateException(
					String.format("Unsupported related alias for empty list. [relatedAlias: %s]", aliasOfRelatedAttribute));
		};
	}

	private String getUpdatedAliasForOptionsContestBlankAttributeAlias(final String aliasOfRelatedAttribute, final Configuration configuration) {

		final String emptyAnswerType = "EMPTY";

		final List<BallotType> ballotTypeList = configuration.getContest().getVoteInformation().stream().parallel()
				.map(VoteInformationType::getVote)
				.map(VoteType::getBallot)
				.flatMap(Collection::stream)
				.toList();

		return Stream.concat(
				Stream.concat(
						// Standard Question
						ballotTypeList.stream().parallel()
								.map(BallotType::getStandardBallot)
								.filter(Objects::nonNull)
								.filter(standardBallotType -> standardBallotType.getQuestionIdentification().equals(aliasOfRelatedAttribute))
								.map(StandardBallotType::getAnswer)
								.flatMap(Collection::stream)
								.filter(standardAnswerType -> emptyAnswerType.equals(standardAnswerType.getStandardAnswerType()))
								.map(StandardAnswerType::getAnswerIdentification),

						// Variant Question
						ballotTypeList.stream().parallel()
								.map(BallotType::getVariantBallot)
								.filter(Objects::nonNull)
								.map(VariantBallotType::getStandardQuestion)
								.flatMap(Collection::stream)
								.filter(standardQuestionType -> standardQuestionType.getQuestionIdentification().equals(aliasOfRelatedAttribute))
								.map(StandardQuestionType::getAnswer)
								.flatMap(Collection::stream)
								.filter(standardAnswerType -> emptyAnswerType.equals(standardAnswerType.getStandardAnswerType()))
								.map(StandardAnswerType::getAnswerIdentification)
				),

				// Tie-break Question
				ballotTypeList.stream().parallel()
						.map(BallotType::getVariantBallot)
						.filter(Objects::nonNull)
						.map(VariantBallotType::getTieBreakQuestion)
						.flatMap(Collection::stream)
						.filter(tieBreakQuestionType -> tieBreakQuestionType.getQuestionIdentification().equals(aliasOfRelatedAttribute))
						.map(TieBreakQuestionType::getAnswer)
						.flatMap(Collection::stream)
						.filter(tiebreakAnswerType -> tiebreakAnswerType.getStandardQuestionReference() == null)
						.map(TiebreakAnswerType::getAnswerIdentification)

		).collect(MoreCollectors.onlyElement());
	}

	private SmallPrimesTuple getSmallPrimesTuple(final String electionEventId) {
		return electionEventRepresentationMap
				.computeIfAbsent(electionEventId, k -> {
					final EncryptionParametersPayload encryptionParametersPayload = encryptionParametersPayloadService.load(electionEventId);
					final List<PrimeGqElement> primeGqElementList = encryptionParametersPayload.getSmallPrimes().stream().toList();
					final DequeBasedSynchronizedStack<PrimeGqElement> smallPrimesDeque = new DequeBasedSynchronizedStack<>();
					smallPrimesDeque.addAll(primeGqElementList);
					return new SmallPrimesTuple(smallPrimesDeque, primeGqElementList.size());
				});
	}

	private JsonObject updateBallotOptions(final JsonObject ballotObject, final SmallPrimesTuple smallPrimesTuple) {

		final Ballot ballot = toBallot(ballotObject);

		final int orderedElectionOptionsSize = ballot.getOrderedElectionOptions().size();
		checkArgument(orderedElectionOptionsSize <= smallPrimesTuple.originalSize,
				"The number of ballot options must be smaller than or equal to the available primes. [#options:%s, #primes:%s]",
				orderedElectionOptionsSize, smallPrimesTuple.originalSize);

		ballot.contests().stream()
				.map(Contest::options)
				.flatMap(Collection::stream)
				.forEach(electionOption -> electionOption.setRepresentation(
						getRepresentationValue(electionOption, smallPrimesTuple.smallPrimesDeque)));

		return toJsonObject(ballot);
	}

	private String getRepresentationValue(final ElectionOption option, final DequeBasedSynchronizedStack<PrimeGqElement> representationIds) {
		return optionIdRepresentationMap.computeIfAbsent(option.getId(), k -> {
			final PrimeGqElement primeGqElement = representationIds.pop();
			return primeGqElement.getValue().toString();
		});
	}

	private JsonObject toJsonObject(final Ballot ballot) {
		final JsonObject updatedBallot;
		try {
			final String ballotJson = objectMapper.writeValueAsString(ballot);

			final JsonReader jsonReader = Json.createReader(new StringReader(ballotJson));
			updatedBallot = jsonReader.readObject();
			jsonReader.close();

		} catch (final IOException e) {
			throw new UncheckedIOException("Cannot deserialize the Ballot object to a ballot JsonObject.", e);
		}
		return updatedBallot;
	}

	private Ballot toBallot(final JsonObject ballotObject) {
		final Ballot ballot;
		try {
			ballot = objectMapper.readValue(ballotObject.toString(), Ballot.class);
		} catch (final IOException e) {
			throw new UncheckedIOException("Cannot deserialize the ballot json string to a valid Ballot object.", e);
		}
		return ballot;
	}

	record SmallPrimesTuple(DequeBasedSynchronizedStack<PrimeGqElement> smallPrimesDeque, int originalSize) {
	}

	private static class DequeBasedSynchronizedStack<T> {

		// Internal Deque which gets decorated for synchronization.
		private final Deque<T> dequeStore;

		public DequeBasedSynchronizedStack() {
			this.dequeStore = new LinkedList<>();
		}

		public synchronized T pop() {
			return this.dequeStore.pop();
		}

		public synchronized boolean addAll(final Collection<T> collection) {
			return this.dequeStore.addAll(collection);
		}

		public synchronized int size() {
			return this.dequeStore.size();
		}
	}

}
