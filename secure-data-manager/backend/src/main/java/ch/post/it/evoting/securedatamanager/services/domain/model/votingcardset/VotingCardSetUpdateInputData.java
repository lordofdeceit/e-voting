/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.domain.model.votingcardset;

/**
 * Request body for updating a voting card set.
 */
public record VotingCardSetUpdateInputData(String privateKeyPEM,
										   String adminBoardId) {
}
