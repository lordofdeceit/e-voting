/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.config.shares.shares.domain;

import java.security.PublicKey;

/**
 * @param authoritiesPublicKey the public key corresponding to the private key with which the shares are signed.
 * @param boardPublicKey       the board's public key, that will be used to extract parameters that will help the reconstruction of the private key.
 */
public record ReadSharesOperationContext(PublicKey authoritiesPublicKey, PublicKey boardPublicKey) {

}
