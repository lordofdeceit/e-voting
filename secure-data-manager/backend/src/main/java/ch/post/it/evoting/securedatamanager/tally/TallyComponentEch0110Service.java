/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.google.common.base.Preconditions;

import ch.ech.xmlns.ech_0110._4.Delivery;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.xmlns.evotingconfig.AuthorizationType;
import ch.post.it.evoting.domain.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.domain.xmlns.evotingdecrypt.Results;

@Service
@ConditionalOnProperty("role.isTally")
public class TallyComponentEch0110Service {

	private static final Logger LOGGER = LoggerFactory.getLogger(TallyComponentEch0110Service.class);

	private final TallyComponentDecryptService tallyComponentDecryptService;
	private final CantonConfigTallyService cantonConfigTallyService;
	private final TallyComponentEch0110FileRepository tallyComponentEch0110FileRepository;

	public TallyComponentEch0110Service(
			final TallyComponentDecryptService tallyComponentDecryptService,
			final CantonConfigTallyService cantonConfigTallyService,
			final TallyComponentEch0110FileRepository tallyComponentEch0110FileRepository) {
		this.tallyComponentDecryptService = tallyComponentDecryptService;
		this.cantonConfigTallyService = cantonConfigTallyService;
		this.tallyComponentEch0110FileRepository = tallyComponentEch0110FileRepository;
	}

	/**
	 * Generates and persists the delivery tally file.
	 *
	 * @param electionEventId    the election event id. Must be non-null and a valid UUID.
	 * @param authorizationTypes the configuration authorizations to be included in the delivery tally file. Must be non-null and not contain any null
	 *                           entry.
	 * @throws NullPointerException      if any input is null or if {@code authorizationTypes} contains a null entry.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 */
	public void generate(final String electionEventId, final Set<AuthorizationType> authorizationTypes) {
		validateUUID(electionEventId);
		checkNotNull(authorizationTypes);
		authorizationTypes.stream().parallel().forEach(Preconditions::checkNotNull);
		final List<AuthorizationType> authorizations = List.copyOf(authorizationTypes);

		LOGGER.debug("Generating tally component eCH-0110 file... [electionEventId: {}]", electionEventId);

		final Configuration configuration = cantonConfigTallyService.load(electionEventId);
		final Results results = tallyComponentDecryptService.load(electionEventId, configuration.getContest().getContestIdentification());

		final Delivery delivery = DeliveryMapper.INSTANCE.map(electionEventId, configuration, results, authorizations);

		tallyComponentEch0110FileRepository.save(delivery, electionEventId);

		LOGGER.info("Tally component eCH-0110 file successfully generated. [electionEventId: {}]", electionEventId);
	}

}
