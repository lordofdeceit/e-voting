/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidParameterException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import ch.post.it.evoting.securedatamanager.ControlComponentPublicKeysService;
import ch.post.it.evoting.securedatamanager.services.application.exception.CCKeysNotExistException;
import ch.post.it.evoting.securedatamanager.services.application.service.ElectionEventService;
import ch.post.it.evoting.securedatamanager.services.domain.model.operation.OperationResult;
import ch.post.it.evoting.securedatamanager.services.domain.model.operation.OperationsData;
import ch.post.it.evoting.securedatamanager.services.domain.model.operation.OperationsOutputCode;
import ch.post.it.evoting.securedatamanager.services.infrastructure.importexport.ImportExportService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/sdm-backend/operation")
@Api(value = "SDM Operations REST API")
public class OperationsController {

	private static final Logger LOGGER = LoggerFactory.getLogger(OperationsController.class);
	private static final String REQUEST_CAN_NOT_BE_PERFORMED = "The request can not be performed for the current resource";
	private final ElectionEventService electionEventService;
	private final ControlComponentPublicKeysService controlComponentPublicKeysService;
	private final ImportExportService importExportService;

	public OperationsController(
			final ElectionEventService electionEventService,
			final ControlComponentPublicKeysService controlComponentPublicKeysService,
			final ImportExportService importExportService) {
		this.electionEventService = electionEventService;
		this.controlComponentPublicKeysService = controlComponentPublicKeysService;
		this.importExportService = importExportService;
	}

	@PostMapping(value = "/export/{electionEventId}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> exportOperation(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electionEventId,
			@RequestBody
			final OperationsData request) {
		try {
			validateUUID(electionEventId);
			byte[] data = importExportService.exportSdmData(electionEventId, request.getPassword());
			return ResponseEntity.ok()
					.contentType(MediaType.APPLICATION_OCTET_STREAM)
					.header("content-Disposition", "inline; filename=" + String.format("export-%s.sdm", getEeAlias(electionEventId)))
					.body(data);

		} catch (final InvalidParameterException e) {
			LOGGER.error(e.getMessage(), e);
			return ResponseEntity.notFound().build();
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			return ResponseEntity.internalServerError().build();
		}
	}

	private String getEeAlias(final String electionEventId) throws CCKeysNotExistException {

		// Check if Control Components keys exists
		if (!controlComponentPublicKeysService.exist(electionEventId)) {
			throw new CCKeysNotExistException(
					String.format("The Control Components keys do not exist for this election event. [electionEventId: %s]", electionEventId));
		}

		final String electionEventAlias = electionEventService.getElectionEventAlias(electionEventId);
		if (StringUtils.isBlank(electionEventAlias)) {
			throw
					new InvalidParameterException("Invalid Election Event Id: " + electionEventId);
		}

		return electionEventAlias;
	}

	@PostMapping(value = "/import")
	@ApiOperation(value = "Import operation service")
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Not Found"), @ApiResponse(code = 403, message = "Forbidden"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	public ResponseEntity<OperationResult> importOperation(
			@ApiParam(value = "file", required = true)
			@RequestParam("file")
			final MultipartFile zip) {

		try (final InputStream in = zip.getInputStream()) {
			byte[] bytes = in.readAllBytes();
			if (bytes == null || bytes.length < 1) {
				final OperationResult opRes = new OperationResult();
				opRes.setError(OperationsOutputCode.MISSING_PARAMETER.value());
				return ResponseEntity.badRequest().body(opRes);
			}
			importExportService.importSdmData(bytes);

		} catch (final IOException e) {
			final OperationsOutputCode code = OperationsOutputCode.ERROR_IO_OPERATIONS;
			return handleException(e, code.value());
		} catch (final InvalidParameterException e) {
			final OperationsOutputCode code = OperationsOutputCode.MISSING_PARAMETER;
			return handleInvalidParamException(e, code.value());
		} catch (final Exception e) {
			final OperationsOutputCode code = OperationsOutputCode.GENERAL_ERROR;
			return handleException(e, code.value());
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	private ResponseEntity<OperationResult> handleException(final Exception e, final int errorCode) {
		LOGGER.error("{}{}", REQUEST_CAN_NOT_BE_PERFORMED, errorCode, e);
		final OperationResult output = new OperationResult();
		output.setError(errorCode);
		output.setException(e.getClass().getName());
		output.setMessage(e.getMessage());
		return new ResponseEntity<>(output, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	private ResponseEntity<OperationResult> handleInvalidParamException(final Exception e, final int errorCode) {
		LOGGER.error("{}{}", REQUEST_CAN_NOT_BE_PERFORMED, errorCode, e);
		final OperationResult output = new OperationResult();
		output.setError(errorCode);
		output.setException(e.getClass().getName());
		output.setMessage(e.getMessage());
		return new ResponseEntity<>(output, HttpStatus.NOT_FOUND);
	}
}
