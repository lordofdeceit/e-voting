/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.cryptoprimitives.math.GroupVector.toGroupVector;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.security.SignatureException;
import java.util.Collection;
import java.util.List;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.google.common.collect.MoreCollectors;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionEventContext;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.cryptoprimitives.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.ChannelSecurityContextData;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.configuration.SetupComponentTallyDataPayload;
import ch.post.it.evoting.domain.configuration.SetupComponentVerificationCardKeystoresPayload;
import ch.post.it.evoting.domain.configuration.VerificationCardKeystore;
import ch.post.it.evoting.securedatamanager.SetupComponentTallyDataPayloadService;
import ch.post.it.evoting.securedatamanager.configuration.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.configuration.EntityHelper;
import ch.post.it.evoting.securedatamanager.configuration.StartVotingKeyService;
import ch.post.it.evoting.securedatamanager.configuration.VerificationCardSecretKeyService;
import ch.post.it.evoting.securedatamanager.services.application.service.ElectionEventService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

/**
 * Creates and persists the setup component verification card keystores payload in the SDM file system.
 */
@Service
@ConditionalOnProperty("role.isConfig")
public class SetupComponentVerificationCardKeystoresPayloadGenerationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SetupComponentVerificationCardKeystoresPayloadGenerationService.class);

	private final EntityHelper entityHelper;
	private final GenCredDatAlgorithm genCredDatAlgorithm;
	private final ElectionEventService electionEventService;
	private final StartVotingKeyService startVotingKeyService;
	private final VotingCardSetRepository votingCardSetRepository;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final VerificationCardSecretKeyService verificationCardSecretKeyService;
	private final ElectionEventContextPayloadService electionEventContextPayloadService;
	private final SetupComponentTallyDataPayloadService setupComponentTallyDataPayloadService;
	private final SetupComponentVerificationCardKeystoresPayloadService setupComponentVerificationCardKeystoresPayloadService;

	public SetupComponentVerificationCardKeystoresPayloadGenerationService(
			final EntityHelper entityHelper,
			final GenCredDatAlgorithm genCredDatAlgorithm,
			final ElectionEventService electionEventService,
			final StartVotingKeyService startVotingKeyService,
			final VotingCardSetRepository votingCardSetRepository,
			@Qualifier("keystoreServiceSdmConfig")
			final SignatureKeystore<Alias> signatureKeystoreService,
			final VerificationCardSecretKeyService verificationCardSecretKeyService,
			final ElectionEventContextPayloadService electionEventContextPayloadService,
			final SetupComponentTallyDataPayloadService setupComponentTallyDataPayloadService,
			final SetupComponentVerificationCardKeystoresPayloadService setupComponentVerificationCardKeystoresPayloadService) {
		this.entityHelper = entityHelper;
		this.genCredDatAlgorithm = genCredDatAlgorithm;
		this.electionEventService = electionEventService;
		this.startVotingKeyService = startVotingKeyService;
		this.votingCardSetRepository = votingCardSetRepository;
		this.signatureKeystoreService = signatureKeystoreService;
		this.verificationCardSecretKeyService = verificationCardSecretKeyService;
		this.electionEventContextPayloadService = electionEventContextPayloadService;
		this.setupComponentTallyDataPayloadService = setupComponentTallyDataPayloadService;
		this.setupComponentVerificationCardKeystoresPayloadService = setupComponentVerificationCardKeystoresPayloadService;
	}

	/**
	 * Creates and saves the list of setup component verification card keystores payloads.
	 *
	 * @param electionEventId                      the election event id. Must be non-null ana a valid UUID.
	 * @param choiceReturnCodesEncryptionPublicKey the choice return codes encryption public key. Must be non-null.
	 * @param electionPublicKey                    the election public key. Must be non-null.
	 * @return the list of created setup component verification card keystores payloads. The returned list is unmodifiable.
	 * @throws NullPointerException      if any input is null.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 * @throws IllegalArgumentException  if the choice return codes encryption public key and the election public key do not have the same group.
	 */
	public List<SetupComponentVerificationCardKeystoresPayload> generate(final String electionEventId,
			final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey, final ElGamalMultiRecipientPublicKey electionPublicKey) {

		validateUUID(electionEventId);
		checkNotNull(choiceReturnCodesEncryptionPublicKey);
		checkNotNull(electionPublicKey);
		checkArgument(electionEventService.exists(electionEventId));
		checkArgument(choiceReturnCodesEncryptionPublicKey.getGroup().equals(electionPublicKey.getGroup()),
				"The choice return codes encryption public key and the election public key must have the same group");

		LOGGER.info("Generating setup component verification card keystores payloads... [electionEventId: {}]", electionEventId);

		final List<String> votingCardSetIds = votingCardSetRepository.findAllVotingCardSetIds(electionEventId);
		final ElectionEventContextPayload electionEventContextPayload = loadElectionEventContextPayload(electionEventId);
		final ElectionEventContext electionEventContext = electionEventContextPayload.getElectionEventContext();

		final List<SetupComponentVerificationCardKeystoresPayload> setupComponentVerificationCardKeystoresPayloads = votingCardSetIds.stream()
				.parallel()
				.map(votingCardSetId -> {
					final String verificationCardSetId = votingCardSetRepository.getVerificationCardSetId(votingCardSetId);

					final VerificationCardSetContext verificationCardSetContext = electionEventContext.verificationCardSetContexts().stream()
							.parallel()
							.filter(vcsContext -> vcsContext.verificationCardSetId().equals(verificationCardSetId))
							.collect(MoreCollectors.onlyElement());

					final GroupVector<PrimesMappingTableEntry, GqGroup> pTable = verificationCardSetContext.primesMappingTable().getPTable();

					final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions = pTable.stream()
							.map(PrimesMappingTableEntry::encodedVotingOption)
							.collect(toGroupVector());

					final List<String> actualVotingOptions = pTable.stream()
							.map(PrimesMappingTableEntry::actualVotingOption)
							.toList();

					final SetupComponentTallyDataPayload setupComponentTallyDataPayload = loadSetupComponentTallyDataPayload(electionEventId,
							verificationCardSetId);
					final List<String> verificationCardIds = setupComponentTallyDataPayload.getVerificationCardIds();
					final List<VerificationCardSecretKey> verificationCardSecretKeysList = verificationCardIds.stream().parallel()
							.map(verificationCardId -> verificationCardSecretKeyService.load(electionEventId, verificationCardSetId,
									verificationCardId))
							.toList();

					final GroupVector<ZqElement, ZqGroup> verificationCardSecretKeys = verificationCardSecretKeysList.stream()
							.map(payload -> payload.privateKey().stream().toList())
							.flatMap(Collection::stream)
							.collect(toGroupVector());

					final List<String> startVotingKeys = verificationCardIds.stream().parallel()
							.map(verificationCardId -> startVotingKeyService.load(electionEventId, verificationCardSetId, verificationCardId))
							.toList();

					final Ballot ballot = entityHelper.getBallotFromVotingCardSet(electionEventId, votingCardSetId);
					final CombinedCorrectnessInformation combinedCorrectnessInformation = new CombinedCorrectnessInformation(ballot);

					final GenCredDatContext genCredDatContext = new GenCredDatContext.Builder()
							.setElectionEventId(electionEventId)
							.setVerificationCardSetId(verificationCardSetId)
							.setElectionPublicKey(electionPublicKey)
							.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
							.setEncodedVotingOptions(encodedVotingOptions)
							.setActualVotingOptions(actualVotingOptions)
							.setCorrectnessInformationSelections(getCorrectnessInformationSelections(combinedCorrectnessInformation))
							.setCorrectnessInformationVotingOptions(getCorrectnessInformationVotingOptions(combinedCorrectnessInformation))
							.build();

					final GenCredDatInput genCredDatInput = new GenCredDatInput(verificationCardIds, verificationCardSecretKeys, startVotingKeys);

					LOGGER.debug("Performing GenCredDat algorithm... [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
							verificationCardSetId);

					final GenCredDatOutput genCredDatOutput = genCredDatAlgorithm.genCredDat(genCredDatContext, genCredDatInput);

					LOGGER.debug("GenCredDat algorithm successfully performed. [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
							verificationCardSetId);

					return createSetupComponentVerificationCardKeystoresPayload(electionEventId, verificationCardSetId, verificationCardIds,
							genCredDatOutput);
				}).toList();

		setupComponentVerificationCardKeystoresPayloads.stream().parallel().forEach(setupComponentVerificationCardKeystoresPayloadService::save);

		LOGGER.info("Generated and saved setup component verification card keystores payloads. [electionEventId: {}]", electionEventId);

		return setupComponentVerificationCardKeystoresPayloads;
	}

	private SetupComponentVerificationCardKeystoresPayload createSetupComponentVerificationCardKeystoresPayload(final String electionEventId,
			final String verificationCardSetId, final List<String> verificationCardIds, final GenCredDatOutput genCredDatOutput) {
		final List<VerificationCardKeystore> verificationCardKeystores = IntStream.range(0, verificationCardIds.size()).parallel()
				.mapToObj(i -> new VerificationCardKeystore(verificationCardIds.get(i), genCredDatOutput.verificationCardKeystores().get(i)))
				.toList();

		final SetupComponentVerificationCardKeystoresPayload setupComponentVerificationCardKeystoresPayload = new SetupComponentVerificationCardKeystoresPayload(
				electionEventId, verificationCardSetId, verificationCardKeystores);

		final Hashable hashable = ChannelSecurityContextData.setupComponentVerificationCardKeystores(electionEventId, verificationCardSetId);

		final byte[] signature;
		try {
			signature = signatureKeystoreService.generateSignature(setupComponentVerificationCardKeystoresPayload, hashable);
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format(
							"Could not sign setup component verification card keystores payload. [electionEventId: %s, , verificationCardSetId: %s]",
							electionEventId, verificationCardSetId));
		}

		final CryptoPrimitivesSignature setupComponentVerificationCardKeystoresPayloadSignature = new CryptoPrimitivesSignature(signature);
		setupComponentVerificationCardKeystoresPayload.setSignature(setupComponentVerificationCardKeystoresPayloadSignature);

		return setupComponentVerificationCardKeystoresPayload;
	}

	private ElectionEventContextPayload loadElectionEventContextPayload(final String electionEventId) {
		final ElectionEventContextPayload electionEventContextPayload = electionEventContextPayloadService.load(electionEventId);

		final CryptoPrimitivesSignature signature = electionEventContextPayload.getSignature();

		checkState(signature != null, "The signature of the election event context payload is null. [electionEventId: %s]", electionEventId);

		final Hashable additionalContextData = ChannelSecurityContextData.electionEventContext(electionEventId);

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreService.verifySignature(Alias.SDM_CONFIG, electionEventContextPayload,
					additionalContextData, electionEventContextPayload.getSignature().signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not verify the signature of the election event context payload. [electionEventId: %s]", electionEventId));
		}

		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(ElectionEventContextPayload.class, String.format("[electionEventId: %s]", electionEventId));
		}

		return electionEventContextPayload;
	}

	private SetupComponentTallyDataPayload loadSetupComponentTallyDataPayload(final String electionEventId, final String verificationCardSetId) {
		final SetupComponentTallyDataPayload setupComponentTallyDataPayload = setupComponentTallyDataPayloadService.load(electionEventId,
				verificationCardSetId);

		final CryptoPrimitivesSignature signature = setupComponentTallyDataPayload.getSignature();

		checkState(signature != null,
				"The signature of the setup component tally data payload is null. [electionEventId: %s, verificationCardSetId: %s]",
				electionEventId, verificationCardSetId);

		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentTallyData(electionEventId, verificationCardSetId);

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreService.verifySignature(Alias.SDM_CONFIG, setupComponentTallyDataPayload,
					additionalContextData, setupComponentTallyDataPayload.getSignature().signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format(
							"Could not verify the signature of the setup component tally data payload. [electionEventId: %s, verificationCardSetId: %s]",
							electionEventId, verificationCardSetId));
		}

		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(SetupComponentTallyDataPayload.class,
					String.format("[electionEventId: %s, verificationCardSetId: %s]", electionEventId, verificationCardSetId));
		}

		return setupComponentTallyDataPayload;
	}

	private List<String> getCorrectnessInformationSelections(final CombinedCorrectnessInformation combinedCorrectnessInformation) {
		return IntStream.range(0, combinedCorrectnessInformation.getTotalNumberOfSelections())
				.boxed()
				.map(combinedCorrectnessInformation::getCorrectnessIdForSelectionIndex)
				.toList();
	}

	private List<String> getCorrectnessInformationVotingOptions(final CombinedCorrectnessInformation combinedCorrectnessInformation) {
		return IntStream.range(0, combinedCorrectnessInformation.getTotalNumberOfVotingOptions())
				.boxed()
				.map(combinedCorrectnessInformation::getCorrectnessIdForVotingOptionIndex)
				.toList();
	}
}
