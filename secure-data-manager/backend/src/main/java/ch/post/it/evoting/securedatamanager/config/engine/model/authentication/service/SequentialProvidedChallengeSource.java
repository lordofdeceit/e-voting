/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.config.engine.model.authentication.service;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import ch.post.it.evoting.securedatamanager.config.commons.config.exceptions.SequentialProvidedChallengeSourceException;
import ch.post.it.evoting.securedatamanager.config.commons.config.model.authentication.ProvidedChallenges;

public class SequentialProvidedChallengeSource {

	private static final int PROVIDED_CHALLENGE_FILE_ALIAS_INDEX = 0;

	private static final Logger LOGGER = LoggerFactory.getLogger(SequentialProvidedChallengeSource.class);

	private final MappingIterator<List<String>> providedChallengeIterator;

	public SequentialProvidedChallengeSource(final Path providedChallengePath) {
		try {
			final CsvSchema csvSchema = CsvSchema.builder()
					.setColumnSeparator(';')
					.setLineSeparator(String.valueOf(CsvSchema.DEFAULT_LINEFEED))
					.build();

			final CsvMapper mapper = new CsvMapper();
			providedChallengeIterator = mapper
					.enable(CsvParser.Feature.WRAP_AS_ARRAY)
					.enable(CsvParser.Feature.IGNORE_TRAILING_UNMAPPABLE)
					.readerForListOf(String.class)
					.with(csvSchema)
					.readValues(new FileReader(providedChallengePath.toString()));
		} catch (final IOException e) {
			LOGGER.error("Could not find provided challenge input data file: {}", providedChallengePath);
			throw new SequentialProvidedChallengeSourceException(
					String.format("Could not find provided challenge input data file: %s", providedChallengePath), e);
		}
	}

	public ProvidedChallenges next() {
		final List<String> nextLine;
		final String alias;
		final List<String> challenges;

		synchronized (providedChallengeIterator) {
			if (providedChallengeIterator.hasNext()) {
				nextLine = providedChallengeIterator.next();
				alias = nextLine.get(PROVIDED_CHALLENGE_FILE_ALIAS_INDEX);
				challenges = nextLine.subList(PROVIDED_CHALLENGE_FILE_ALIAS_INDEX + 1, nextLine.size());
				return new ProvidedChallenges(alias, challenges);
			}
		}
		return null;
	}
}
