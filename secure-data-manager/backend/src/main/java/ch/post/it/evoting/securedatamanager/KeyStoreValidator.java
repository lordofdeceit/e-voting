/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.Arrays;
import java.util.function.BooleanSupplier;
import java.util.stream.Stream;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;

/**
 * Validates that the keystore contains the private key of this authority and the certificates of the other authorities.
 */
public final class KeyStoreValidator {

	private KeyStoreValidator() {
		// utility class
	}

	public static boolean validateKeyStore(KeyStore keyStore, Alias signingAlias) {
		BooleanSupplier signerCertificatesPresent = () -> Arrays.stream(Alias.values())
				.allMatch(alias -> {
					try {
						return keyStore.getCertificate(alias.get()) != null;
					} catch (KeyStoreException e) {
						return false;
					}
				});

		BooleanSupplier signingKeyPresent = () -> Stream.of(signingAlias)
				.allMatch(alias -> {
					try {
						return keyStore.getKey(signingAlias.get(), "".toCharArray()) != null;
					} catch (KeyStoreException | NoSuchAlgorithmException | UnrecoverableKeyException e) {
						return false;
					}
				});

		return signerCertificatesPresent.getAsBoolean() && signingKeyPresent.getAsBoolean();
	}
}
