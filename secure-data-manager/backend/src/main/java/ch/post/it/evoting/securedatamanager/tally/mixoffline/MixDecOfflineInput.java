/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.mixoffline;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.securedatamanager.ElectoralBoardPasswordValidation;

/**
 * Contains the inputs of the MixDecOffline algorithm.
 *
 * @param partiallyDecryptedVotes        c<sub>dec,4</sub>, the partially decrypted votes. Non-null.
 * @param electoralBoardMembersPasswords (PW<sub>0</sub>, ..., PW<sub>k-1</sub>), the passwords of the k electoral board members. Non-null.
 */
public record MixDecOfflineInput(GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> partiallyDecryptedVotes, List<char[]> electoralBoardMembersPasswords) {

	public MixDecOfflineInput {
		checkNotNull(partiallyDecryptedVotes);
		checkNotNull(electoralBoardMembersPasswords);

		electoralBoardMembersPasswords.forEach(ElectoralBoardPasswordValidation::validate);
	}
}
