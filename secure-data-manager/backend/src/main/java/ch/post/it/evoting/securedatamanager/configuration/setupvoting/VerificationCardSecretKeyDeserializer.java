/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.EncryptionGroupUtils;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

public class VerificationCardSecretKeyDeserializer extends JsonDeserializer<VerificationCardSecretKey> {

	@Override
	public VerificationCardSecretKey deserialize(final JsonParser parser, final DeserializationContext deserializationContext)
			throws IOException {

		final ObjectMapper mapper = DomainObjectMapper.getNewInstance();

		final JsonNode node = mapper.readTree(parser);
		final String electionEventId = mapper.readValue(node.get("electionEventId").toString(), String.class);
		final String verificationCardSetId = mapper.readValue(node.get("verificationCardSetId").toString(), String.class);
		final String verificationCardId = mapper.readValue(node.get("verificationCardId").toString(), String.class);
		final JsonNode encryptionGroupNode = node.get("gqGroup");
		final GqGroup encryptionGroup = EncryptionGroupUtils.getEncryptionGroup(mapper, encryptionGroupNode);

		final ElGamalMultiRecipientPrivateKey privateKey = mapper.reader()
				.withAttribute("group", encryptionGroup)
				.readValue(node.get("privateKey"), ElGamalMultiRecipientPrivateKey.class);

		return new VerificationCardSecretKey(electionEventId, verificationCardSetId, verificationCardId, privateKey, encryptionGroup);
	}
}
