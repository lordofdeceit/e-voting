/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.configuration.SetupComponentVerificationCardKeystoresPayload;
import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.commons.PathResolver;
import ch.post.it.evoting.securedatamanager.config.commons.utils.JsonUtils;
import ch.post.it.evoting.securedatamanager.configuration.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.configuration.SetupComponentPublicKeysPayloadService;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.SetupComponentVerificationCardKeystoresPayloadService;
import ch.post.it.evoting.securedatamanager.services.application.exception.DatabaseException;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.Status;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.SynchronizeStatus;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.electionevent.ElectionEventRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.electoralauthority.ElectoralAuthorityRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.electoralauthority.ElectoralAuthorityUploadRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

/**
 * Service which uploads files to voter portal after creating the electoral authorities
 */
@Service
public class ElectoralAuthoritiesUploadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ElectoralAuthoritiesUploadService.class);

	private static final String CONSTANT_AUTHENTICATION_CONTEXT_DATA = "authenticationContextData";
	private static final String CONSTANT_AUTHENTICATION_VOTER_DATA = "authenticationVoterData";

	private final ElectoralAuthorityRepository electoralAuthorityRepository;
	private final ElectoralAuthorityUploadRepository electoralAuthorityUploadRepository;
	private final PathResolver pathResolver;
	private final ElectionEventRepository electionEventRepository;
	private final ElectionEventContextPayloadService electionEventContextPayloadService;
	private final SetupComponentPublicKeysPayloadService setupComponentPublicKeysPayloadService;
	private final VotingCardSetRepository votingCardSetRepository;
	private final SetupComponentVerificationCardKeystoresPayloadService setupComponentVerificationCardKeystoresPayloadService;

	public ElectoralAuthoritiesUploadService(final ElectoralAuthorityRepository electoralAuthorityRepository,
			final ElectoralAuthorityUploadRepository electoralAuthorityUploadRepository,
			final PathResolver pathResolver,
			final ElectionEventRepository electionEventRepository,
			final ElectionEventContextPayloadService electionEventContextPayloadService,
			final SetupComponentPublicKeysPayloadService setupComponentPublicKeysPayloadService,
			final VotingCardSetRepository votingCardSetRepository,
			final SetupComponentVerificationCardKeystoresPayloadService setupComponentVerificationCardKeystoresPayloadService) {
		this.electoralAuthorityRepository = electoralAuthorityRepository;
		this.electoralAuthorityUploadRepository = electoralAuthorityUploadRepository;
		this.pathResolver = pathResolver;
		this.electionEventRepository = electionEventRepository;
		this.electionEventContextPayloadService = electionEventContextPayloadService;
		this.setupComponentPublicKeysPayloadService = setupComponentPublicKeysPayloadService;
		this.votingCardSetRepository = votingCardSetRepository;
		this.setupComponentVerificationCardKeystoresPayloadService = setupComponentVerificationCardKeystoresPayloadService;
	}

	/**
	 * Uploads the available electoral authorities texts to the voter portal.
	 */
	public void uploadSynchronizableElectoralAuthorities(final String electionEvent) throws IOException {

		final Map<String, Object> params = new HashMap<>();
		params.put(JsonConstants.STATUS, Status.SIGNED.name());
		params.put(JsonConstants.SYNCHRONIZED, SynchronizeStatus.PENDING.getIsSynchronized().toString());

		if (StringUtils.isNotEmpty(electionEvent)) {
			params.put(JsonConstants.ELECTION_EVENT_DOT_ID, electionEvent);
		}

		final String documents = electoralAuthorityRepository.list(params);
		final JsonArray electoralAuthorities = JsonUtils.getJsonObject(documents).getJsonArray(JsonConstants.RESULT);

		for (int i = 0; i < electoralAuthorities.size(); i++) {

			final JsonObject electoralAuthoritiesInArray = electoralAuthorities.getJsonObject(i);
			final String electoralAuthorityId = electoralAuthoritiesInArray.getString(JsonConstants.ID);
			final String electionEventId = electoralAuthoritiesInArray.getJsonObject(JsonConstants.ELECTION_EVENT).getString(JsonConstants.ID);
			final JsonObject eEvent = JsonUtils.getJsonObject(electionEventRepository.find(electionEventId));
			final JsonObject adminBoard = eEvent.getJsonObject(JsonConstants.ADMINISTRATION_AUTHORITY);
			final String adminBoardId = adminBoard.getString(JsonConstants.ID);

			LOGGER.info("Uploading the signed authentication context configuration. [electionEventId: {}, electoralAuthorityId: {}]", electionEventId,
					electoralAuthorityId);
			final boolean authResult = uploadAuthenticationContextData(electionEventId, adminBoardId);

			final JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
			if (authResult) {
				LOGGER.info("Changing the status of the electoral authority. [electionEventId: {}, electoralAuthorityId: {}]", electionEventId,
						electoralAuthorityId);
				jsonObjectBuilder.add(JsonConstants.ID, electoralAuthorityId);
				jsonObjectBuilder.add(JsonConstants.SYNCHRONIZED, SynchronizeStatus.SYNCHRONIZED.getIsSynchronized().toString());
				jsonObjectBuilder.add(JsonConstants.DETAILS, SynchronizeStatus.SYNCHRONIZED.getStatus());
				LOGGER.info("The electoral authority was uploaded successfully. [electionEventId: {}, electoralAuthorityId: {}]", electionEventId,
						electoralAuthorityId);
			} else {
				LOGGER.error("An error occurred while uploading the signed electoral authority. [electionEventId: {}, electoralAuthorityId: {}]",
						electionEventId, electoralAuthorityId);
				jsonObjectBuilder.add(JsonConstants.ID, electoralAuthorityId);
				jsonObjectBuilder.add(JsonConstants.DETAILS, SynchronizeStatus.FAILED.getStatus());

			}

			try {
				electoralAuthorityRepository.update(jsonObjectBuilder.build().toString());
			} catch (final DatabaseException ex) {
				LOGGER.error(String.format(
						"An error occurred while updating the signed electoral authority. [electionEventId: %s, electoralAuthorityId: %s]",
						electionEventId, electoralAuthorityId), ex);
			}

			uploadElectionEventContextData(electionEventId);
			uploadSetupComponentPublicKeysData(electionEventId);
			uploadSetupComponentVerificationCardKeystoresPayloads(electionEventId);
		}

	}

	/**
	 * Uploads the setup component verification card keystores payloads.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 * @throws NullPointerException      if the election event id is null.
	 */
	private void uploadSetupComponentVerificationCardKeystoresPayloads(final String electionEventId) {
		validateUUID(electionEventId);

		LOGGER.info("Uploading setup component verification card keystores payloads... [electionEventId: {}]", electionEventId);

		final Map<String, Object> votingCardSetsCriteria = new HashMap<>();
		votingCardSetsCriteria.put(JsonConstants.STATUS, Status.SIGNED.name());
		votingCardSetsCriteria.put(JsonConstants.ELECTION_EVENT_DOT_ID, electionEventId);

		final String votingCardSetsEntities = votingCardSetRepository.list(votingCardSetsCriteria);
		final JsonArray votingCardSets = JsonUtils.getJsonObject(votingCardSetsEntities).getJsonArray(JsonConstants.RESULT);

		final List<SetupComponentVerificationCardKeystoresPayload> setupComponentVerificationCardKeystoresPayloads = IntStream.range(0,
						votingCardSets.size())
				.mapToObj(votingCardSets::getJsonObject)
				.map(votingCardSet -> votingCardSet.getString(JsonConstants.VERIFICATION_CARD_SET_ID))
				.map(verificationCardId -> setupComponentVerificationCardKeystoresPayloadService.load(electionEventId, verificationCardId))
				.toList();

		setupComponentVerificationCardKeystoresPayloadService.upload(setupComponentVerificationCardKeystoresPayloads);

		LOGGER.info("Successfully uploaded setup component verification card keystores payloads. [electionEventId: {}]", electionEventId);
	}

	/**
	 * Uploads the election event context payload to the vote verification.
	 *
	 * @param electionEventId the election event id.
	 * @throws IllegalStateException            if an error occurred while verifying the signature of the election event context payload or the
	 *                                          request for uploading the election event context failed.
	 * @throws InvalidPayloadSignatureException if the signature of the election event context payload is invalid.
	 */
	private void uploadElectionEventContextData(final String electionEventId) {

		LOGGER.info("Uploading election event context payload... [electionEventId: {}]", electionEventId);

		final ElectionEventContextPayload electionEventContextPayload = electionEventContextPayloadService.load(electionEventId);

		final boolean isUploaded = electoralAuthorityUploadRepository.uploadElectionEventContextData(electionEventContextPayload);
		checkState(isUploaded, "Request for uploading election event context failed. [electionEventId: %s]", electionEventId);

		LOGGER.info("Successfully uploaded election event context payload. [electionEventId: {}]", electionEventId);
	}

	/**
	 * Uploads the setup component public keys payload to the vote verification.
	 *
	 * @param electionEventId the election event id.
	 * @throws IllegalStateException            if an error occurred while verifying the signature of the setup component public keys payload or the
	 *                                          request for uploading the setup component public keys failed.
	 * @throws InvalidPayloadSignatureException if the signature of the setup component public keys payload is invalid.
	 */
	private void uploadSetupComponentPublicKeysData(final String electionEventId) {

		LOGGER.info("Uploading setup component public keys payload... [electionEventId: {}]", electionEventId);

		final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload = setupComponentPublicKeysPayloadService.load(electionEventId);

		final boolean isUploaded = electoralAuthorityUploadRepository.uploadSetupComponentPublicKeysData(setupComponentPublicKeysPayload);
		checkState(isUploaded, "Request for uploading setup component public keys failed. [electionEventId: %s]", electionEventId);

		LOGGER.info("Successfully uploaded setup component public keys payload. [electionEventId: {}]", electionEventId);
	}

	private boolean uploadAuthenticationContextData(final String electionEventId, final String adminBoardId) throws IOException {

		if (electoralAuthorityUploadRepository.checkEmptyElectionEventDataInAU(electionEventId)) {
			final Path authenticationPath = pathResolver.resolve(Constants.CONFIG_FILES_BASE_DIR, electionEventId, Constants.CONFIG_DIR_NAME_ONLINE,
					Constants.CONFIG_DIR_NAME_AUTHENTICATION);

			final Path authenticationContextPath = pathResolver.resolve(authenticationPath.toString(),
					Constants.CONFIG_FILE_NAME_SIGNED_AUTH_CONTEXT_DATA);
			final Path authenticationVoterDataPath = pathResolver.resolve(authenticationPath.toString(),
					Constants.CONFIG_FILE_NAME_SIGNED_AUTH_VOTER_DATA);
			final JsonObject authenticationContexData = JsonUtils
					.getJsonObject(new String(Files.readAllBytes(authenticationContextPath), StandardCharsets.UTF_8));
			final JsonObject authenticationVoterData = JsonUtils
					.getJsonObject(new String(Files.readAllBytes(authenticationVoterDataPath), StandardCharsets.UTF_8));
			final JsonObject jsonInput = Json.createObjectBuilder().add(CONSTANT_AUTHENTICATION_CONTEXT_DATA, authenticationContexData.toString())
					.add(CONSTANT_AUTHENTICATION_VOTER_DATA, authenticationVoterData.toString()).build();

			return electoralAuthorityUploadRepository.uploadAuthenticationContextData(electionEventId, adminBoardId, jsonInput);
		}

		return true;
	}

}
