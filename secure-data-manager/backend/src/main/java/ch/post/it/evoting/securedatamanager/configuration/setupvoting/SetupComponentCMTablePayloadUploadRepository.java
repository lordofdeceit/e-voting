/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.PrivateKey;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.configuration.SetupComponentCMTablePayload;
import ch.post.it.evoting.securedatamanager.commons.ChunkedRequestBodyCreator;
import ch.post.it.evoting.securedatamanager.services.application.service.KeyStoreService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.RestClientService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.clients.VoteVerificationClient;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;

@Repository
public class SetupComponentCMTablePayloadUploadRepository {
	private static final Logger LOGGER = LoggerFactory.getLogger(SetupComponentCMTablePayloadUploadRepository.class);

	private final KeyStoreService keystoreService;

	private final ObjectMapper objectMapper;

	@Value("${voting.portal.enabled}")
	private boolean isVotingPortalEnabled;

	@Value("${VV_URL}")
	private String voteVerificationUrl;

	public SetupComponentCMTablePayloadUploadRepository(final KeyStoreService keystoreService, final ObjectMapper objectMapper) {
		this.keystoreService = keystoreService;
		this.objectMapper = objectMapper;
	}

	/**
	 * Uploads the setup component CMTable payload to the vote verification.
	 *
	 * @param electionEventId              the election event id. Must be non-null and a valid UUID.
	 * @param verificationCardSetId        the verification card set id. Must be non-null and a valid UUID.
	 * @param setupComponentCMTablePayload the {@link SetupComponentCMTablePayload} to upload. Must be non-null.
	 * @throws NullPointerException      if any of the inputs is null.
	 * @throws FailedValidationException if {@code electionEventId} or {@code verificationCardSetId} is invalid.
	 */
	public void uploadReturnCodesMappingTable(final String electionEventId, final String verificationCardSetId,
			final SetupComponentCMTablePayload setupComponentCMTablePayload) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkNotNull(setupComponentCMTablePayload);

		checkState(isVotingPortalEnabled, "The voting-portal connection is not enabled.");

		final byte[] setupComponentCMTablePayloadBytes;
		try {
			setupComponentCMTablePayloadBytes = objectMapper.writeValueAsBytes(setupComponentCMTablePayload);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(
					String.format("Failed to serialize SetupComponentCMTablePayload. [electionEventId: %s, verificationCardSetId: %s]",
							electionEventId, verificationCardSetId), e);
		}
		final RequestBody requestBody = ChunkedRequestBodyCreator.forJsonPayload(setupComponentCMTablePayloadBytes);

		final retrofit2.Response<ResponseBody> response;
		try {
			response = getVoteVerificationClient().saveReturnCodeMappingTable(electionEventId, verificationCardSetId, requestBody)
					.execute();
		} catch (final IOException e) {
			throw new UncheckedIOException("Failed to communicate with vote verification.", e);
		}

		if (!response.isSuccessful()) {
			throw new IllegalStateException(
					String.format(
							"Request for uploading setup component CMTable payloads failed. [electionEventId: %s, verificationCardSetId: %s]",
							electionEventId, verificationCardSetId));
		}

		LOGGER.info("Successfully uploaded setup component CMTable payloads. [electionEventId: {}, verificationCardSetId: {}}]", electionEventId,
				verificationCardSetId);
	}

	/**
	 * Gets the vote verification Retrofit client
	 */
	private VoteVerificationClient getVoteVerificationClient() {
		final PrivateKey privateKey = keystoreService.getPrivateKey();
		final Retrofit restAdapter = RestClientService.getInstance()
				.getRestClientWithInterceptorAndJacksonConverter(voteVerificationUrl, privateKey, "SECURE_DATA_MANAGER");
		return restAdapter.create(VoteVerificationClient.class);
	}
}
