/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.integerToString;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamal;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientMessage;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.cryptoprimitives.math.Base64;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupMatrix;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;

/**
 * Implements the CombineEncLongCodeShares algorithm.
 */
@Service
public class CombineEncLongCodeSharesAlgorithm {

	private final Hash hash;
	private final ElGamal elGamal;
	private final Base64 base64;

	public CombineEncLongCodeSharesAlgorithm(
			final Hash hash,
			final ElGamal elGamal,
			final Base64 base64) {
		this.hash = hash;
		this.elGamal = elGamal;
		this.base64 = base64;
	}

	/**
	 * Combines the control components’ encrypted long return code shares.
	 *
	 * @param context the {@link CombineEncLongCodeSharesContext}. Must be non-null.
	 * @param input   the {@link CombineEncLongCodeSharesInput} containing all needed inputs. Must be non-null.
	 * @return the combined control components’ encrypted long return code shares in a {@link CombineEncLongCodeSharesOutput}.
	 * @throws NullPointerException if the context or input is null.
	 */
	@SuppressWarnings("java:S117")
	public CombineEncLongCodeSharesOutput combineEncLongCodeShares(final CombineEncLongCodeSharesContext context,
			final CombineEncLongCodeSharesInput input) {
		checkNotNull(context);
		checkNotNull(input);

		// Context.
		final GqGroup group = context.getEncryptionGroup();
		final String ee = context.getElectionEventId();
		final String vcs = context.getVerificationCardSetId();
		final ElGamalMultiRecipientPrivateKey sk_setup = context.getSetupSecretKey();
		final int n = context.getNumberOfVotingOptions();
		final int N_E = context.getNumberOfEligibleVoters();

		// Input.
		final GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> C_expPCC = input.getExponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix();
		final GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> C_expCK = input.getExponentiatedEncryptedHashedConfirmationKeysMatrix();
		final List<String> vc = input.getVerificationCardIds();

		// Cross-checks.
		checkArgument(group.equals(C_expPCC.getGroup()), "The context and input must have the same encryption group.");
		checkArgument(C_expPCC.get(0, 0).size() == n,
				"The size of each ciphertext in C_expPCC must be equal to the number of voting options.");
		checkArgument(C_expPCC.numRows() == N_E,
				"The number of rows of the matrices C_expPCC and C_expCK must be equal to the number of eligible voters.");

		// Operation.
		record VoterCombinedCodes(ElGamalMultiRecipientCiphertext c_pC_id, GqElement pVCC_id, String hlVCC_id) {
		}

		final List<VoterCombinedCodes> voterCombinedCodes = IntStream.range(0, N_E)
				.parallel()
				.mapToObj(id -> {
					ElGamalMultiRecipientCiphertext c_pC_id = elGamal.neutralElement(n, group);
					final String vc_id = vc.get(id);

					final List<GqElement> lVCC_id = new ArrayList<>();
					final List<HashableString> hlVCC_id = new ArrayList<>();

					// The specification uses 1 indexing, but we are bound to 0 indexing
					for (int j = 0; j < 4; j++) {
						c_pC_id = c_pC_id.getCiphertextProduct(C_expPCC.get(id, j));

						final ElGamalMultiRecipientCiphertext C_expCK_j_id = C_expCK.get(id, j);

						final GqElement lVCC_id_j = getMessage(C_expCK_j_id, sk_setup);

						final HashableList i_aux_1 = HashableList.of(HashableString.from("CreateLVCCShare"), HashableString.from(ee),
								HashableString.from(vcs), HashableString.from(vc_id), HashableString.from(integerToString(j + 1)));

						final HashableString hlVCC_id_j = HashableString.from(base64.base64Encode(hash.recursiveHash(i_aux_1, lVCC_id_j)));

						lVCC_id.add(lVCC_id_j);
						hlVCC_id.add(hlVCC_id_j);
					}

					// pVCC_id ← ∏ lVCC_id_j
					final GqElement pVCC_id = lVCC_id.stream().reduce(group.getIdentity(), GqElement::multiply);

					final HashableList i_aux_2 = HashableList.of(HashableString.from("VerifyLVCCHash"), HashableString.from(ee),
							HashableString.from(vcs),
							HashableString.from(vc_id));

					final String hhlVCC_id = base64.base64Encode(hash.recursiveHash(i_aux_2, hlVCC_id.get(0), hlVCC_id.get(1),
							hlVCC_id.get(2), hlVCC_id.get(3)));

					// L_lVCC ← L_lVCC ∪ (hhlVCC_id)
					return new VoterCombinedCodes(c_pC_id, pVCC_id, hhlVCC_id);
				})
				.toList();

		return new CombineEncLongCodeSharesOutput.Builder()
				.setEncryptedPreChoiceReturnCodesVector(
						voterCombinedCodes.stream().map(VoterCombinedCodes::c_pC_id).collect(GroupVector.toGroupVector()))
				.setPreVoteCastReturnCodesVector(voterCombinedCodes.stream().map(VoterCombinedCodes::pVCC_id).collect(GroupVector.toGroupVector()))
				.setLongVoteCastReturnCodesAllowList(voterCombinedCodes.stream().map(VoterCombinedCodes::hlVCC_id).toList())
				.build();
	}

	private GqElement getMessage(final ElGamalMultiRecipientCiphertext ciphertext, final ElGamalMultiRecipientPrivateKey secretKey) {
		final ElGamalMultiRecipientMessage message = elGamal.getMessage(ciphertext, secretKey);

		checkArgument(message.getElements().size() == 1, "The message must have only one element.");

		return message.getElements().get(0);
	}

}
