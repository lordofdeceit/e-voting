/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;

/**
 * Allows saving, retrieving and finding start voting keys
 */
@Service
public class StartVotingKeyService {
	private static final Logger LOGGER = LoggerFactory.getLogger(StartVotingKeyService.class);

	private final StartVotingKeyFileRepository startVotingKeyFileRepository;

	public StartVotingKeyService(final StartVotingKeyFileRepository startVotingKeyFileRepository) {
		this.startVotingKeyFileRepository = startVotingKeyFileRepository;
	}

	/**
	 * Saves a start voting key for the given {@code electionEventId}, {@code verificationCardSetId}, {@code verificationCardId}.
	 *
	 * @param electionEventId       the election event id.
	 * @param verificationCardSetId the verification card set id.
	 * @param verificationCardId    the verification card id.
	 * @param value                 the value of start voting key to save.
	 * @throws FailedValidationException if any of {@code electionEventId}, {@code verificationCardSetId}, {@code verificationCardId} are invalid.
	 * @throws NullPointerException      if any of {@code electionEventId}, {@code verificationCardSetId}, {@code verificationCardId}, {@code value}
	 *                                   is null.
	 */
	public void save(final String electionEventId, final String verificationCardSetId, final String verificationCardId, final String value) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		validateUUID(verificationCardId);
		checkNotNull(value);

		startVotingKeyFileRepository.save(electionEventId, verificationCardSetId, verificationCardId, value);
		LOGGER.info("Saved start voting key. [electionEventId: {}, verificationCardSetId: {}, verificationCardId: {}]", electionEventId,
				verificationCardSetId, verificationCardId);
	}

	/**
	 * Checks if a start voting key exist for the given {@code electionEventId}, {@code verificationCardSetId}, {@code verificationCardId}.
	 *
	 * @param electionEventId       the election event id.
	 * @param verificationCardSetId the verification card set id.
	 * @param verificationCardId    the verification card id.
	 * @return {@code true} if the start voting key is present, {@code false} otherwise.
	 * @throws FailedValidationException if any of {@code electionEventId}, {@code verificationCardSetId}, {@code verificationCardId} are invalid.
	 * @throws NullPointerException      if any of {@code electionEventId}, {@code verificationCardSetId}, {@code verificationCardId} is null.
	 */
	public boolean exist(final String electionEventId, final String verificationCardSetId, final String verificationCardId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		validateUUID(verificationCardId);

		return startVotingKeyFileRepository.existsById(electionEventId, verificationCardSetId, verificationCardId);
	}

	/**
	 * Loads the start voting key for the given {@code electionEventId}, {@code verificationCardSetId}, {@code verificationCardId}. The result of this
	 * method is cached.
	 *
	 * @param electionEventId       the election event id.
	 * @param verificationCardSetId the verification card set id.
	 * @param verificationCardId    the verification card id.
	 * @return the start voting key for {@code electionEventId}, {@code verificationCardSetId}, {@code verificationCardId}.
	 * @throws FailedValidationException if any of {@code electionEventId}, {@code verificationCardSetId}, {@code verificationCardId} are invalid.
	 * @throws NullPointerException      if any of {@code electionEventId}, {@code verificationCardSetId}, {@code verificationCardId} is null.
	 * @throws IllegalStateException     if the requested start voting key is not present.
	 */
	@Cacheable("startVotingKeys")
	public String load(final String electionEventId, final String verificationCardSetId, final String verificationCardId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		validateUUID(verificationCardId);

		final String value = startVotingKeyFileRepository.findById(electionEventId, verificationCardSetId, verificationCardId)
				.orElseThrow(() -> new IllegalStateException(
						String.format(
								"Requested start voting key is not present. [electionEventId: %s, verificationCardSetId: %s, verificationCardId: %s]",
								electionEventId, verificationCardSetId, verificationCardId)));

		LOGGER.info("Loaded start voting key. [electionEventId: {}, verificationCardSetId: {}, verificationCardId: {}]",
				electionEventId, verificationCardSetId, verificationCardId);

		return value;
	}
}
