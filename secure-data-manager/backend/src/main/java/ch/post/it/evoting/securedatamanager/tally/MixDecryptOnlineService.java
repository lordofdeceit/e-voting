/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.domain.tally.ControlComponentBallotBoxPayload;
import ch.post.it.evoting.domain.tally.MixDecryptOnlinePayload;
import ch.post.it.evoting.securedatamanager.services.application.service.BallotBoxService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.clients.MessageBrokerOrchestratorClient;

import retrofit2.Response;

@Service
public class MixDecryptOnlineService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MixDecryptOnlineService.class);
	private static final String COMMUNICATION_FAILURE_MESSAGE = "Failed to communicate with orchestrator.";
	private static final String VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE = "The voting-portal connection is not enabled.";
	private final BallotBoxService ballotBoxService;
	private final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient;
	private final ControlComponentShufflePayloadFileRepository controlComponentShufflePayloadFileRepository;
	private final ControlComponentBallotBoxPayloadFileRepository controlComponentBallotBoxPayloadFileRepository;
	private final boolean isVotingPortalEnabled;

	public MixDecryptOnlineService(
			final BallotBoxService ballotBoxService,
			final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient,
			final ControlComponentShufflePayloadFileRepository controlComponentShufflePayloadFileRepository,
			final ControlComponentBallotBoxPayloadFileRepository controlComponentBallotBoxPayloadFileRepository,
			@Value("${voting.portal.enabled}")
			final boolean isVotingPortalEnabled) {
		this.ballotBoxService = ballotBoxService;
		this.messageBrokerOrchestratorClient = messageBrokerOrchestratorClient;
		this.controlComponentShufflePayloadFileRepository = controlComponentShufflePayloadFileRepository;
		this.controlComponentBallotBoxPayloadFileRepository = controlComponentBallotBoxPayloadFileRepository;
		this.isVotingPortalEnabled = isVotingPortalEnabled;
	}

	/**
	 * Requests the control-components to mix the ballot box with given id {@code ballotBoxId}.
	 *
	 * @param electionEventId the election event id of the ballot box.
	 * @param ballotBoxId     the id of the ballot box to mix.
	 * @throws UncheckedIOException if the communication with the orchestrator fails.
	 */
	public void startOnlineMixing(final String electionEventId, final String ballotBoxId) {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		checkState(isVotingPortalEnabled, VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		final Response<Void> response;
		try {
			response = messageBrokerOrchestratorClient.createMixDecryptOnline(electionEventId, ballotBoxId).execute();
		} catch (final IOException e) {
			throw new UncheckedIOException(COMMUNICATION_FAILURE_MESSAGE, e);
		}

		if (response.isSuccessful()) {
			ballotBoxService.updateBallotBoxStatus(ballotBoxId, BallotBoxStatus.MIXING);
			LOGGER.info("Ballot box status updated. [ballotBoxId: {}, status: {}", ballotBoxId, BallotBoxStatus.MIXING);
		}
	}

	/**
	 * Gets the status of the ballot box with id {@code ballotBoxId} from the control-components.
	 *
	 * @param electionEventId the election event id of the ballot box.
	 * @param ballotBoxId     the id of the ballot box to get the status.
	 * @return the status of the ballot box.
	 * @throws UncheckedIOException  if the communication with the orchestrator fails.
	 * @throws IllegalStateException if the response from the orchestrator was unsuccessful.
	 */
	public BallotBoxStatus getOnlineStatus(final String electionEventId, final String ballotBoxId) {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		checkState(isVotingPortalEnabled, VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		final Response<BallotBoxStatus> response;
		try {
			response = messageBrokerOrchestratorClient.checkMixDecryptOnlineStatus(electionEventId, ballotBoxId).execute();
		} catch (final IOException e) {
			throw new UncheckedIOException(COMMUNICATION_FAILURE_MESSAGE, e);
		}

		checkState(response.isSuccessful() && response.body() != null, "Get online status unsuccessful. [ballotBoxId: %s]", ballotBoxId);

		return response.body();
	}

	/**
	 * Downloads and persists the outputs generated when mixing the requested ballot box on the online control component nodes.
	 *
	 * @param electionEventId the election event id.
	 * @param ballotBoxId     the ballot box id.
	 * @throws IllegalArgumentException         if the ballot box is not {@link BallotBoxStatus#MIXED}.
	 * @throws UncheckedIOException             if the communication with the orchestrator fails.
	 * @throws IllegalStateException            if the response from the orchestrator was unsuccessful.
	 * @throws InvalidPayloadSignatureException if the signature of the response's content is invalid.
	 */
	public void downloadOnlineMixnetPayloads(final String electionEventId, final String ballotBoxId) {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		checkState(isVotingPortalEnabled, VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		LOGGER.info("Requesting payloads for ballot box... [ballotBoxId: {}]", ballotBoxId);

		// Check that the full ballot box has been mixed.
		if (!ballotBoxService.hasStatus(ballotBoxId, BallotBoxStatus.MIXED)) {
			throw new IllegalArgumentException(String.format("Ballot box is not mixed [ballotBoxId : %s]", ballotBoxId));
		}

		final Response<MixDecryptOnlinePayload> response;
		try {
			response = messageBrokerOrchestratorClient.downloadMixDecryptOnline(electionEventId, ballotBoxId).execute();
		} catch (final IOException e) {
			throw new UncheckedIOException(COMMUNICATION_FAILURE_MESSAGE, e);
		}

		final String ballotId = ballotBoxService.getBallotId(ballotBoxId);
		checkState(response.isSuccessful() && response.body() != null, "Download unsuccessful. [ballotBoxId: {}]", ballotBoxId);

		final List<ControlComponentBallotBoxPayload> controlComponentBallotBoxPayloads = response.body().controlComponentBallotBoxPayloads();
		final List<ControlComponentShufflePayload> shufflePayloads = response.body().controlComponentShufflePayloads();

		// Save mixnet payloads.
		controlComponentBallotBoxPayloadFileRepository.saveAll(ballotId, controlComponentBallotBoxPayloads);
		LOGGER.info("Confirmed encrypted votes payloads successfully stored. [electionEventId:{}, ballotId:{}, ballotBoxId:{}]", electionEventId,
				ballotId, ballotBoxId);

		shufflePayloads.forEach(payload -> controlComponentShufflePayloadFileRepository.savePayload(ballotId, payload));
		LOGGER.info("Control component shuffle payloads successfully stored. [electionEventId:{}, ballotId:{}, ballotBoxId:{}]", electionEventId,
				ballotId, ballotBoxId);

		ballotBoxService.updateBallotBoxStatus(ballotBoxId, BallotBoxStatus.DOWNLOADED);
		LOGGER.info("Ballot box status updated. [electionEventId: {}, ballotId: {}, ballotBoxId: {}, status: {}]", BallotBoxStatus.DOWNLOADED,
				electionEventId, ballotId, ballotBoxId);
	}

}
