/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static com.google.common.base.Preconditions.checkNotNull;

public class GenerationStatus {

	private boolean executionTask;
	private boolean springBatch;

	public GenerationStatus() {
		executionTask = false;
		springBatch = false;
	}

	public void executionTaskCompleted() {
		executionTask = true;
	}

	public void springBatchCompleted() {
		springBatch = true;
	}

	public boolean isAllCompleted() {
		return executionTask && springBatch;
	}

	public void updateStatus(final VotingCardSetGenerationStatusEvent event) {
		checkNotNull(event);
		if (GenerationType.SPRING_BATCH.equals(event.getType())) {
			this.springBatchCompleted();
		} else {
			this.executionTaskCompleted();
		}
	}

}
