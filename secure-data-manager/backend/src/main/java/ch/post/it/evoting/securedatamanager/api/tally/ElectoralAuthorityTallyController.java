/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api.tally;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.securedatamanager.tally.ElectoralBoardTallyService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * The REST endpoint for accessing electoral authority data in Tally phase.
 */
@RestController
@RequestMapping("/sdm-backend/electoralauthorities")
@Api(value = "Electoral authorities REST API")
@ConditionalOnProperty("role.isTally")
public class ElectoralAuthorityTallyController {
	private final ElectoralBoardTallyService electoralBoardTallyService;

	public ElectoralAuthorityTallyController(final ElectoralBoardTallyService electoralBoardTallyService) {
		this.electoralBoardTallyService = electoralBoardTallyService;
	}

	/**
	 * Validates an electoral board member's password against its persisted hash.
	 *
	 * @param electionEventId        the election event id.
	 * @param electoralAuthorityId   the electoral authority id.
	 * @param memberIndex            the index of the member in the member list.
	 * @param electoralBoardPassword the member's password.
	 */
	@PostMapping(value = "/electionevent/{electionEventId}/electoralauthority/{electoralAuthorityId}/index/{memberIndex}/validate")
	@ApiOperation(value = "Validate electoral board password", notes = "Service to validate the electoral board password against its hash.")
	public ResponseEntity<Void> validatePassword(
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electionEventId,
			@ApiParam(value = "String", required = true)
			@PathVariable
			final String electoralAuthorityId,
			@ApiParam(value = "memberIndex", required = true)
			@PathVariable
			final int memberIndex,
			@ApiParam(value = "electoralBoardPassword", required = true)
			@RequestBody
			final char[] electoralBoardPassword) {

		validateUUID(electionEventId);
		validateUUID(electoralAuthorityId);
		checkNotNull(electoralBoardPassword);

		if (electoralBoardTallyService.verifyElectoralBoardMemberPassword(electionEventId, memberIndex, electoralBoardPassword)) {
			return new ResponseEntity<>(HttpStatus.OK);
		}

		return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
	}
}
