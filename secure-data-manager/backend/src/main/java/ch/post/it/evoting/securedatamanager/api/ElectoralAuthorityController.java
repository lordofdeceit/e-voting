/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.api;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.securedatamanager.services.infrastructure.electoralauthority.ElectoralAuthorityRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * The REST endpoint for accessing electoral authority data.
 */
@RestController
@RequestMapping("/sdm-backend/electoralauthorities")
@Api(value = "Electoral authorities REST API")
public class ElectoralAuthorityController {
	private final ElectoralAuthorityRepository electoralAuthorityRepository;

	public ElectoralAuthorityController(final ElectoralAuthorityRepository electoralAuthorityRepository) {
		this.electoralAuthorityRepository = electoralAuthorityRepository;
	}

	/**
	 * Returns a list of electoral authorities identified by an election event identifier.
	 *
	 * @param electionEventId the election event id.
	 * @return a list of electoral authorities belong to an election event.
	 */
	@GetMapping(value = "/electionevent/{electionEventId}", produces = "application/json")
	@ResponseBody
	@ApiOperation(value = "List electoral authorities", response = String.class, notes = "Service to retrieve the list "
			+ "of electoral authorities for a given election event.")
	public String getElectoralAuthorities4ElectionEventId(
			@PathVariable
			final String electionEventId) {

		validateUUID(electionEventId);

		return electoralAuthorityRepository.listByElectionEvent(electionEventId);
	}
}
