/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.ech.xmlns.ech_0222._1.Delivery;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.domain.xmlns.evotingdecrypt.Results;

@Service
@ConditionalOnProperty("role.isTally")
public class TallyComponentEch0222Service {

	private static final Logger LOGGER = LoggerFactory.getLogger(TallyComponentEch0222Service.class);

	private final TallyComponentDecryptService tallyComponentDecryptService;
	private final CantonConfigTallyService cantonConfigTallyService;
	private final TallyComponentEch0222FileRepository tallyComponentEch0222FileRepository;

	public TallyComponentEch0222Service(
			final TallyComponentDecryptService tallyComponentDecryptService,
			final CantonConfigTallyService cantonConfigTallyService,
			final TallyComponentEch0222FileRepository tallyComponentEch0222FileRepository) {
		this.tallyComponentDecryptService = tallyComponentDecryptService;
		this.cantonConfigTallyService = cantonConfigTallyService;
		this.tallyComponentEch0222FileRepository = tallyComponentEch0222FileRepository;
	}

	/**
	 * Generates and persists the eCH-0222 tally file.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @throws NullPointerException      if the election event id is null.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 */
	public void generate(final String electionEventId) {
		validateUUID(electionEventId);

		LOGGER.debug("Generating tally component eCH-0222 file... [electionEventId: {}]", electionEventId);

		final Configuration configuration = cantonConfigTallyService.load(electionEventId);
		final Results results = tallyComponentDecryptService.load(electionEventId, configuration.getContest().getContestIdentification());

		final Delivery delivery = RawDataDeliveryMapper.createECH0222(electionEventId, configuration, results);

		tallyComponentEch0222FileRepository.save(delivery, electionEventId);

		LOGGER.info("Tally component eCH-0222 file successfully generated. [electionEventId: {}]", electionEventId);
	}
}
