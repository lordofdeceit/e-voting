/*
 * (c) Copyright 2022 Swiss Post Ltd
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.commons.Constants.NULL_ELECTION_EVENT_ID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.configuration.SetupComponentCMTablePayload;

/**
 * Service which uploads setup component CMTable payload files to the voter portal.
 */
@Service
public class SetupComponentCMTablePayloadUploadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SetupComponentCMTablePayloadUploadService.class);

	private final SetupComponentCMTablePayloadService setupComponentCMTablePayloadService;
	private final SetupComponentCMTablePayloadUploadRepository setupComponentCMTablePayloadUploadRepository;

	public SetupComponentCMTablePayloadUploadService(
			final SetupComponentCMTablePayloadService setupComponentCMTablePayloadService,
			final SetupComponentCMTablePayloadUploadRepository setupComponentCMTablePayloadUploadRepository
	) {
		this.setupComponentCMTablePayloadService = setupComponentCMTablePayloadService;
		this.setupComponentCMTablePayloadUploadRepository = setupComponentCMTablePayloadUploadRepository;
	}

	/**
	 * Uploads the available return codes mapping tables to the voter portal.
	 *
	 * @param electionEventId, the election event id. Must be non-null and a valid UUID.
	 * @throws FailedValidationException if {@code electionEventId} is invalid.
	 */
	public void uploadReturnCodesMappingTable(final String electionEventId, final String verificationCardSetId) {
		if (NULL_ELECTION_EVENT_ID.equals(electionEventId)) {
			LOGGER.info("No election event available");
			return;
		}

		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);

		LOGGER.info("Uploading setup component CMTable payload... [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
				verificationCardSetId);

		final SetupComponentCMTablePayload setupComponentCMTablePayload = setupComponentCMTablePayloadService.load(electionEventId,
				verificationCardSetId);

		setupComponentCMTablePayloadUploadRepository.uploadReturnCodesMappingTable(electionEventId, verificationCardSetId,
				setupComponentCMTablePayload);
		LOGGER.info("Successfully uploaded setup component CMTable payloads. [electionEventId: {}]", electionEventId);
	}

}
