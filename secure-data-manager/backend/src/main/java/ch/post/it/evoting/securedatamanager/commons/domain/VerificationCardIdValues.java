/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.commons.domain;

public class VerificationCardIdValues {

	private String verificationCardId;

	private boolean poisonPill;

	private VerificationCardIdValues() {
		poisonPill = true;
	}

	public VerificationCardIdValues(final String verificationCardId) {
		this.verificationCardId = verificationCardId;
	}

	public static VerificationCardIdValues poisonPill() {
		return new VerificationCardIdValues();
	}

	public String getVerificationCardId() {
		return verificationCardId;
	}

	public void setVerificationCardId(final String verificationCardId) {
		this.verificationCardId = verificationCardId;
	}

	public boolean isPoisonPill() {
		return poisonPill;
	}

}
