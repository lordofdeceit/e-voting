/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.google.common.base.Preconditions;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.SchnorrProof;
import ch.post.it.evoting.securedatamanager.configuration.setuptally.SetupTallyEBAlgorithm;
import ch.post.it.evoting.securedatamanager.configuration.setuptally.SetupTallyEBContext;
import ch.post.it.evoting.securedatamanager.configuration.setuptally.SetupTallyEBInput;
import ch.post.it.evoting.securedatamanager.configuration.setuptally.SetupTallyEBOutput;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.BallotConfigService;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.GenVerCardSetKeysAlgorithm;
import ch.post.it.evoting.securedatamanager.configuration.setupvoting.SetupComponentVerificationCardKeystoresPayloadGenerationService;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.application.service.ElectionEventService;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballotbox.BallotBoxRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

@Service
@ConditionalOnProperty("role.isConfig")
public class ElectoralBoardConstitutionService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ElectoralBoardConstitutionService.class);
	private final ControlComponentPublicKeysConfigService controlComponentPublicKeysConfigService;
	private final GenVerCardSetKeysAlgorithm genVerCardSetKeysAlgorithm;
	private final SetupTallyEBAlgorithm setupTallyEBAlgorithm;
	private final ElectoralBoardPersistenceService electoralBoardPersistenceService;
	private final SetupComponentVerificationCardKeystoresPayloadGenerationService setupComponentVerificationCardKeystoresPayloadGenerationService;
	private final ElectionEventService electionEventService;
	private final VotingCardSetRepository votingCardSetRepository;
	private final BallotBoxRepository ballotBoxRepository;
	private final BallotConfigService ballotConfigService;
	private final PrimesMappingTableService primesMappingTableService;
	private final EncryptionParametersConfigService encryptionParametersConfigService;

	public ElectoralBoardConstitutionService(
			final ControlComponentPublicKeysConfigService controlComponentPublicKeysConfigService,
			final GenVerCardSetKeysAlgorithm genVerCardSetKeysAlgorithm,
			final SetupTallyEBAlgorithm setupTallyEBAlgorithm,
			final ElectoralBoardPersistenceService electoralBoardPersistenceService,
			final SetupComponentVerificationCardKeystoresPayloadGenerationService setupComponentVerificationCardKeystoresPayloadGenerationService,
			final ElectionEventService electionEventService,
			final VotingCardSetRepository votingCardSetRepository,
			final BallotBoxRepository ballotBoxRepository,
			final BallotConfigService ballotConfigService,
			final PrimesMappingTableService primesMappingTableService,
			final EncryptionParametersConfigService encryptionParametersConfigService) {
		this.controlComponentPublicKeysConfigService = controlComponentPublicKeysConfigService;
		this.genVerCardSetKeysAlgorithm = genVerCardSetKeysAlgorithm;
		this.setupTallyEBAlgorithm = setupTallyEBAlgorithm;
		this.electoralBoardPersistenceService = electoralBoardPersistenceService;
		this.setupComponentVerificationCardKeystoresPayloadGenerationService = setupComponentVerificationCardKeystoresPayloadGenerationService;
		this.electionEventService = electionEventService;
		this.votingCardSetRepository = votingCardSetRepository;
		this.ballotBoxRepository = ballotBoxRepository;
		this.ballotConfigService = ballotConfigService;
		this.primesMappingTableService = primesMappingTableService;
		this.encryptionParametersConfigService = encryptionParametersConfigService;
	}

	/**
	 * Constitutes the electoral board.
	 *
	 * @param electionEventId                the election event id. Must be non-null and a valid UUID.
	 * @param electoralBoardMembersPasswords the passwords of the electoral board members. Must be non-null and a valid EBPassword.
	 * @param electoralBoardMembersHashes    the hashes of the electoral board members' passwords. Must be non-null.
	 * @throws FailedValidationException if the election event id is invalid.
	 * @throws NullPointerException      if any of the inputs is null.
	 * @throws IllegalStateException     if any hash is empty.
	 */
	public void constitute(final String electionEventId, final List<char[]> electoralBoardMembersPasswords,
			final List<byte[]> electoralBoardMembersHashes) {
		validateUUID(electionEventId);
		checkNotNull(electoralBoardMembersPasswords);
		checkArgument(electionEventService.exists(electionEventId));
		electoralBoardMembersPasswords.forEach(Preconditions::checkNotNull);
		final List<char[]> passwords = electoralBoardMembersPasswords.stream().parallel().map(char[]::clone).toList();
		passwords.stream().parallel().forEach(Preconditions::checkNotNull);
		checkArgument(passwords.size() >= 2);

		// Wipe the passwords after usage
		electoralBoardMembersPasswords.stream().parallel().forEach(pw -> Arrays.fill(pw, '0'));

		checkNotNull(electoralBoardMembersHashes);
		electoralBoardMembersHashes.stream().parallel().forEach(Preconditions::checkNotNull);
		final List<byte[]> passwordHashes = checkNotNull(electoralBoardMembersHashes).stream().parallel().map(byte[]::clone).toList();
		passwordHashes.stream().parallel().forEach(Preconditions::checkNotNull);
		passwordHashes.stream().parallel().forEach(hash -> checkArgument(hash.length > 0));

		checkArgument(passwords.size() == passwordHashes.size());

		LOGGER.debug("Loading control component public keys... [electionEventId: {}]", electionEventId);

		final List<ControlComponentPublicKeys> controlComponentPublicKeys = controlComponentPublicKeysConfigService.loadOrderByNodeId(
				electionEventId);

		LOGGER.debug("Loaded control component public keys. [electionEventId: {}]", electionEventId);

		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccrjChoiceReturnCodesEncryptionPublicKeys = controlComponentPublicKeys.stream()
				.map(ControlComponentPublicKeys::ccrjChoiceReturnCodesEncryptionPublicKey)
				.collect(GroupVector.toGroupVector());

		final GroupVector<GroupVector<SchnorrProof, ZqGroup>, ZqGroup> ccrjSchnorrProofs = controlComponentPublicKeys.stream()
				.map(ControlComponentPublicKeys::ccrjSchnorrProofs)
				.collect(GroupVector.toGroupVector());

		final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey =
				genVerCardSetKeysAlgorithm.genVerCardSetKeys(electionEventId, ccrjChoiceReturnCodesEncryptionPublicKeys, ccrjSchnorrProofs);

		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmElectionPublicKeys = controlComponentPublicKeys.stream()
				.map(ControlComponentPublicKeys::ccmjElectionPublicKey)
				.collect(GroupVector.toGroupVector());

		final GroupVector<GroupVector<SchnorrProof, ZqGroup>, ZqGroup> ccmjSchnorrProofs = controlComponentPublicKeys.stream()
				.map(ControlComponentPublicKeys::ccmjSchnorrProofs)
				.collect(GroupVector.toGroupVector());

		LOGGER.debug("Building Verification Card Set Contexts... [electionEventId: {}]", electionEventId);

		final List<VerificationCardSetContext> verificationCardSetContexts = buildVerificationCardSetContexts(electionEventId);

		LOGGER.debug("Built Verification Card Set Contexts. [electionEventId: {}]", electionEventId);

		final int maximumNumberOfWriteIns = verificationCardSetContexts.stream().parallel()
				.mapToInt(VerificationCardSetContext::numberOfWriteInFields)
				.max()
				.orElseThrow(IllegalArgumentException::new);

		final GqGroup encryptionGroup = encryptionParametersConfigService.loadEncryptionGroup(electionEventId);
		final SetupTallyEBContext setupTallyEBContext = new SetupTallyEBContext(encryptionGroup, electionEventId);
		final SetupTallyEBInput setupTallyEBInput = new SetupTallyEBInput(ccmElectionPublicKeys, ccmjSchnorrProofs, maximumNumberOfWriteIns,
				passwords);

		LOGGER.debug("Performing Setup Tally EB algorithm... [electionEventId: {}]", electionEventId);

		final SetupTallyEBOutput setupTallyEBOutput = setupTallyEBAlgorithm.setupTallyEB(setupTallyEBContext, setupTallyEBInput);

		LOGGER.debug("Setup Tally EB algorithm successfully performed. [electionEventId: {}]", electionEventId);

		final ElGamalMultiRecipientPublicKey electionPublicKey = setupTallyEBOutput.getElectionPublicKey();
		final ElGamalMultiRecipientPublicKey electoralBoardPublicKey = setupTallyEBOutput.getElectoralBoardPublicKey();
		final GroupVector<SchnorrProof, ZqGroup> electoralBoardSchnorrProofs = setupTallyEBOutput.getElectoralBoardSchnorrProofs();

		LOGGER.debug("Persisting Electoral Board... [electionEventId: {}]", electionEventId);

		electoralBoardPersistenceService.persist(electionEventId, controlComponentPublicKeys, choiceReturnCodesEncryptionPublicKey, electionPublicKey,
				electoralBoardPublicKey, electoralBoardSchnorrProofs, passwordHashes, verificationCardSetContexts);

		LOGGER.info("Electoral board successfully constituted and persisted. [electionEventId: {}]", electionEventId);

		setupComponentVerificationCardKeystoresPayloadGenerationService.generate(electionEventId, choiceReturnCodesEncryptionPublicKey,
				electionPublicKey);

		LOGGER.info("Successfully generated and persisted setup component verification card keystores payloads. [electionEventId: {}]",
				electionEventId);
	}

	/**
	 * Builds the list of {@link VerificationCardSetContext} for the given election event id.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @return the list of {@link VerificationCardSetContext}.
	 * @throws NullPointerException      if the input is null.
	 * @throws FailedValidationException if the input is not a valid UUID.
	 */
	private List<VerificationCardSetContext> buildVerificationCardSetContexts(final String electionEventId) {
		validateUUID(electionEventId);

		final List<String> votingCardSetIds = votingCardSetRepository.findAllVotingCardSetIds(electionEventId);

		return votingCardSetIds.stream().parallel()
				.map(votingCardSetId -> {
					final String verificationCardSetId = votingCardSetRepository.getVerificationCardSetId(votingCardSetId);
					final String ballotBoxId = votingCardSetRepository.getBallotBoxId(votingCardSetId);
					final String ballotId = ballotBoxRepository.getBallotId(ballotBoxId);
					final Ballot ballot = ballotConfigService.getBallot(electionEventId, ballotId);

					final boolean testBallotBox = ballotBoxRepository.isTestBallotBox(ballotBoxId);
					final int numberOfWriteInFields = new CombinedCorrectnessInformation(ballot).getTotalNumberOfWriteInOptions();
					final int gracePeriod = ballotBoxRepository.getGracePeriod(ballotBoxId);
					final PrimesMappingTable primesMappingTable = primesMappingTableService.load(electionEventId, verificationCardSetId);

					final int numberOfVotingCards;
					try {
						numberOfVotingCards = votingCardSetRepository.getNumberOfVotingCards(electionEventId, votingCardSetId);
					} catch (final ResourceNotFoundException e) {
						throw new IllegalStateException(
								String.format("Number of voting cards not found. [electionEventId: %s, votingCardSetId: %s]", electionEventId,
										votingCardSetId));
					}

					return new VerificationCardSetContext(verificationCardSetId, ballotBoxId, testBallotBox, numberOfWriteInFields,
							numberOfVotingCards, gracePeriod, primesMappingTable);
				})
				.toList();
	}

}
