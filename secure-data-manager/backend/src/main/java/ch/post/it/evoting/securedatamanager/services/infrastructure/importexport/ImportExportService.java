/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.infrastructure.importexport;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;

import ch.post.it.evoting.securedatamanager.commons.Constants;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;

@Service
public class ImportExportService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImportExportService.class);

    private final ImportExportFileSystemService filesystemService;
    private final ImportExportOrientDbService orientDbService;
    private final CompressionService compressionService;
    private final char[] importExportZipPassword;

    public ImportExportService(final CompressionService compressionService,
            final ImportExportOrientDbService orientDbService,
            final ImportExportFileSystemService filesystemService,
            @Value("${import.export.zip.password}")
            final char[] importExportZipPassword) {
        this.compressionService = compressionService;
        this.orientDbService = orientDbService;
        this.filesystemService = filesystemService;
        this.importExportZipPassword = importExportZipPassword;
    }

    public void importSdmData(final byte[] zipContent) {
        checkNotNull(zipContent);
        final Path unzipDirectory = unzip(zipContent);
        final Path sdmDirectory = unzipDirectory.resolve(Constants.SDM_DIR_NAME);

        importOrientDb(unzipDirectory);
        filesystemService.importFileSystem(sdmDirectory);

        deleteDirectory(unzipDirectory);
    }

    private static void deleteDirectory(Path directory) {
        try {
            FileSystemUtils.deleteRecursively(directory);
        } catch (IOException e) {
            LOGGER.warn("Fail to remove directory. [directory: {}]", directory);
        }
    }

    private Path unzip(final byte[] zipContent) {
        try {
            final Path unzipDirectory = createTemporaryDirectory();
            compressionService.unzipToDirectory(zipContent, importExportZipPassword, unzipDirectory);
            return unzipDirectory;
        } catch (IOException e) {
            throw new UncheckedIOException("Cannot unzip the file.", e);
        }
    }

    private void importOrientDb(final Path directory) {
        try {
            final Path dbDump = directory.resolve(Constants.DBDUMP_FILE_NAME);
            orientDbService.importOrientDb(dbDump);
        } catch (IOException e) {
            throw new UncheckedIOException("Cannot import orient DB.", e);
        }
    }

    public byte[] exportSdmData(final String electionEventId, final char[] keystorePassword) {
        try {
            validateUUID(electionEventId);
            checkNotNull(keystorePassword);

            final Path zipDirectory = createTemporaryDirectory();
            final Path filesystem = zipDirectory.resolve(Constants.SDM_DIR_NAME);

            Files.createDirectories(filesystem);

            exportOrientDb(zipDirectory, electionEventId, keystorePassword);
            filesystemService.exportFileSystem(electionEventId, filesystem);
            byte[] zipPayload = compressionService.zipDirectory(zipDirectory, importExportZipPassword);

            deleteDirectory(zipDirectory);

            return zipPayload;

        } catch (IOException e) {
            throw new UncheckedIOException("Error during export.", e);
        }
    }

    private static Path createTemporaryDirectory() {
        try {
            return Files.createTempDirectory(UUID.randomUUID().toString()).toAbsolutePath();
        } catch (IOException e) {
            throw new UncheckedIOException("Unable to create temporary directory", e);
        }
    }

    private void exportOrientDb(final Path directory, final String electionEventId, final char[] keystorePassword) {
        try {
            final Path dbDump = directory.resolve(Constants.DBDUMP_FILE_NAME);
            orientDbService.exportOrientDb(dbDump, electionEventId, keystorePassword);
        } catch (IOException | ResourceNotFoundException e) {
            throw new IllegalStateException("Cannot export orient DB.", e);
        }
    }
}
