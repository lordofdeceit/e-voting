/*
 * (c) Copyright 2022 Swiss Post Ltd
 */
package ch.post.it.evoting.securedatamanager.batch.batch;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

import java.security.SignatureException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.ChannelSecurityContextData;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.securedatamanager.commons.domain.VerificationCardIdValues;

/**
 * The nodeContributions refers to the contributions of the control components generated in GenEncLongCodeShares.
 */
@Service
@JobScope
@ConditionalOnProperty("role.isConfig")
public class NodeContributionsProcessor implements ItemProcessor<NodeContributions, List<VerificationCardIdValues>> {

	private final SignatureKeystore<Alias> signatureKeystoreService;

	public NodeContributionsProcessor(
			@Qualifier("keystoreServiceSdmConfig")
			final SignatureKeystore<Alias> signatureKeystoreService) {

		this.signatureKeystoreService = signatureKeystoreService;
	}

	@Override
	public List<VerificationCardIdValues> process(final NodeContributions nodeContributions) throws Exception {
		final List<ControlComponentCodeSharesPayload> nodeContributionResponse = nodeContributions.getNodeContributionResponse();

		verifySignatures(nodeContributionResponse);
		verifyIdsConsistency(nodeContributionResponse);

		return buildResponse(nodeContributionResponse);
	}

	private void verifyIdsConsistency(final List<ControlComponentCodeSharesPayload> nodeContributions) {
		checkArgument(!nodeContributions.isEmpty(), "The node contributions list can not be empty.");

		final String electionEventId = nodeContributions.get(0).getElectionEventId();
		final String verificationCardSetId = nodeContributions.get(0).getVerificationCardSetId();
		final int chunkId = nodeContributions.get(0).getChunkId();

		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);

		checkArgument(nodeContributions.stream().map(ControlComponentCodeSharesPayload::getElectionEventId)
				.allMatch(eeid -> eeid.equals(electionEventId)), "All node contributions must be have the same election event id.");
		checkArgument(
				nodeContributions.stream().map(ControlComponentCodeSharesPayload::getVerificationCardSetId)
						.allMatch(vcsid -> vcsid.equals(verificationCardSetId)),
				"All node contributions must be have the same verification card set id.");
		checkArgument(nodeContributions.stream().map(ControlComponentCodeSharesPayload::getChunkId)
				.allMatch(cid -> cid.equals(chunkId)), "All node contributions must be have the same chunk id.");
	}

	private void verifySignatures(final List<ControlComponentCodeSharesPayload> nodeContributions) {
		nodeContributions.forEach(returnCodeNoteContribution -> {
			final ControlComponentCodeSharesPayload responsePayload = returnCodeNoteContribution;

			final CryptoPrimitivesSignature signature = responsePayload.getSignature();

			final int nodeId = responsePayload.getNodeId();
			final String electionEventId = responsePayload.getElectionEventId();
			final String verificationCardSetId = responsePayload.getVerificationCardSetId();

			checkState(signature != null,
					"The signature of the control component code shares payload is null. [nodeId: %s, electionEventId: %s, verificationCardSetId: %s]",
					nodeId, electionEventId, verificationCardSetId);

			final Hashable additionalContextData = ChannelSecurityContextData.controlComponentCodeShares(nodeId, electionEventId,
					verificationCardSetId);

			final boolean isSignatureValid;
			try {
				isSignatureValid = signatureKeystoreService.verifySignature(Alias.getControlComponentByNodeId(nodeId), responsePayload,
						additionalContextData, signature.signatureContents());
			} catch (final SignatureException e) {
				throw new IllegalStateException(
						String.format("Unable to verify signature [nodeId: %s, electionEventId: %s, verificationCardSetId: %s]",
								nodeId, electionEventId, verificationCardSetId), e);
			}

			if (!isSignatureValid) {
				throw new InvalidPayloadSignatureException(ControlComponentCodeSharesPayload.class,
						String.format("[nodeId: %s, electionEventId: %s, verificationCardSetId: %s]", nodeId, electionEventId,
								verificationCardSetId));
			}
		});
	}

	private List<VerificationCardIdValues> buildResponse(
			final List<ControlComponentCodeSharesPayload> nodeContributionResponse) {

		final List<VerificationCardIdValues> response = new ArrayList<>();

		final int chunkSize = nodeContributionResponse.get(0).getControlComponentCodeShares().size();
		for (int i = 0; i < chunkSize; i++) {
			// The verification card id is the same for all nodes.
			final String verificationCardId = nodeContributionResponse.get(0).getControlComponentCodeShares().get(i)
					.verificationCardId();

			response.add(new VerificationCardIdValues(verificationCardId));
		}

		return response;
	}
}
