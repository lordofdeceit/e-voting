/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;

/**
 * Implements the DecodeVotingOptions algorithm.
 */
@Service
public class DecodeVotingOptionsAlgorithm {

	/**
	 * Returns the list of decoded voting options.
	 *
	 * @param encodedVotingOptions (p&#771;<sub>0</sub>, ..., p&#771;<sub>m-1</sub>)</li>, the encoded voting options. Must be non-null and all
	 *                             encoded voting options must be distinct.
	 * @param primesMappingTable   pTable, the primes mapping table of size n. Must be non-null. {@link PrimesMappingTableEntry} constructor validates actualVotingOption.
	 * @return (v<sub>0</sub>, ..., v<sub>m-1</sub>), the list of decoded voting options.
	 * @throws NullPointerException     if any input is null.
	 * @throws IllegalArgumentException if
	 *                                  <ul>
	 *                                      <li>the inputs don't have the same group.</li>
	 *                                      <li>m is bigger or equal to n.</li>
	 *                                  </ul>
	 */
	public List<String> decodeVotingOptions(final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions,
			final PrimesMappingTable primesMappingTable) {
		checkNotNull(encodedVotingOptions);
		checkNotNull(primesMappingTable);

		checkArgument(primesMappingTable.getPTable().getGroup().equals(encodedVotingOptions.getGroup()),
				"The groups of the primes mapping table and the encoded voting options must be equal.");

		final int m = encodedVotingOptions.size();
		final int n = primesMappingTable.size(); // Guaranteed to be strictly positive by the Object itself.

		// Require
		checkArgument(m < n, "The size of the encoded voting options must be smaller than the size of the primes mapping table. [m: %s, n: %s]", m,
				n);
		checkArgument(encodedVotingOptions.stream().distinct().count() == encodedVotingOptions.size(),
				"The encoded voting options must all be distinct.");

		// Operation & Output
		return encodedVotingOptions.stream()
				.map(primesMappingTable::getPrimesMappingTableEntry)
				.map(PrimesMappingTableEntry::actualVotingOption)
				.toList();
	}

}
