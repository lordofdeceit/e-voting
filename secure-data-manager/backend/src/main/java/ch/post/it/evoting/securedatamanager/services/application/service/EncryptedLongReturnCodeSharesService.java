/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.services.application.service;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import ch.post.it.evoting.cryptoprimitives.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.domain.configuration.setupvoting.ComputingStatus;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.PayloadStorageException;
import ch.post.it.evoting.securedatamanager.services.infrastructure.cc.SetupComponentVerificationDataPayloadFileSystemRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.clients.MessageBrokerOrchestratorClient;
import ch.post.it.evoting.securedatamanager.services.infrastructure.votingcardset.VotingCardSetRepository;

import retrofit2.Response;

@Service
public class EncryptedLongReturnCodeSharesService {

	private static final String VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE = "The voting-portal connection is not enabled.";
	private final VotingCardSetRepository votingCardSetRepository;
	private final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient;
	private final SetupComponentVerificationDataPayloadFileSystemRepository setupComponentVerificationDataPayloadFileSystemRepository;
	private final boolean isVotingPortalEnabled;

	public EncryptedLongReturnCodeSharesService(
			final VotingCardSetRepository votingCardSetRepository,
			final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient,
			final SetupComponentVerificationDataPayloadFileSystemRepository setupComponentVerificationDataPayloadFileSystemRepository,
			@Value("${voting.portal.enabled}")
			final boolean isVotingPortalEnabled) {
		this.votingCardSetRepository = votingCardSetRepository;
		this.messageBrokerOrchestratorClient = messageBrokerOrchestratorClient;
		this.setupComponentVerificationDataPayloadFileSystemRepository = setupComponentVerificationDataPayloadFileSystemRepository;
		this.isVotingPortalEnabled = isVotingPortalEnabled;
	}

	/**
	 * Send the Setup Component verification data to the control components to generate the encrypted long Return Code Shares (encrypted long Choice
	 * Return Code shares and encrypted long Vote Cast Return Code shares).
	 */
	public void computeGenEncLongCodeShares(final SetupComponentVerificationDataPayload setupComponentVerificationDataPayload) {

		checkNotNull(setupComponentVerificationDataPayload);
		checkState(isVotingPortalEnabled, VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		final String electionEventId = setupComponentVerificationDataPayload.getElectionEventId();
		final String verificationCardSetId = setupComponentVerificationDataPayload.getVerificationCardSetId();
		final int chunkId = setupComponentVerificationDataPayload.getChunkId();

		final Response<Void> response;
		try {
			response = messageBrokerOrchestratorClient.computeGenEncLongCodeShares(electionEventId, verificationCardSetId, chunkId,
					setupComponentVerificationDataPayload).execute();
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}

		checkState(response.isSuccessful(), "Compute unsuccessful. [electionEventId: {}, verificationCardSetId: {}, chunkId: {}]", electionEventId,
				verificationCardSetId, chunkId);
	}

	/**
	 * Check if the control components finished the generation of the encrypted long Return Code Shares and update the status of the voting card set
	 * they belong to. The SDM checks if the number of generated encrypted long Return Code Shares (number of chunks) in the voting server corresponds
	 * to the number of Setup Component verification data (number of chunks) in the SDM.
	 */
	public void updateVotingCardSetsComputationStatus(final String electionEventId) throws IOException {

		validateUUID(electionEventId);
		checkState(isVotingPortalEnabled, VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		final Map<String, Object> votingCardSetsParams = new HashMap<>();
		votingCardSetsParams.put(JsonConstants.STATUS, ComputingStatus.COMPUTING.name());
		votingCardSetsParams.put(JsonConstants.ELECTION_EVENT_DOT_ID, electionEventId);

		final String votingCardSetJSON = votingCardSetRepository.list(votingCardSetsParams);
		final JsonArray votingCardSets = new JsonParser().parse(votingCardSetJSON).getAsJsonObject().get(JsonConstants.RESULT).getAsJsonArray();

		for (int i = 0; i < votingCardSets.size(); i++) {
			final JsonObject votingCardSetInArray = votingCardSets.get(i).getAsJsonObject();
			final String verificationCardSetId = votingCardSetInArray.get(JsonConstants.VERIFICATION_CARD_SET_ID).getAsString();

			final int chunkCount;
			try {
				chunkCount = setupComponentVerificationDataPayloadFileSystemRepository.getCount(electionEventId, verificationCardSetId);
			} catch (final PayloadStorageException e) {
				throw new IllegalStateException(e);
			}

			final Response<ComputingStatus> computingStatusResponse = messageBrokerOrchestratorClient.checkGenEncLongCodeSharesStatus(electionEventId,
					verificationCardSetId, chunkCount).execute();

			checkState(computingStatusResponse.isSuccessful() && computingStatusResponse.body() != null,
					"Get status unsuccessful. [electionEventId: %s, verificationCardSetId: %s, chunkCount: %s]", electionEventId,
					verificationCardSetId, chunkCount);

			final ComputingStatus computingStatus = computingStatusResponse.body();

			votingCardSetInArray.addProperty(JsonConstants.STATUS, computingStatus.toString());
			votingCardSetRepository.update(votingCardSetInArray.toString());
		}
	}

	/**
	 * Download the control components' encrypted long Choice Return codes and the encrypted long Vote Cast Return code shares.
	 */
	public List<ControlComponentCodeSharesPayload> downloadGenEncLongCodeShares(final String electionEventId, final String verificationCardSetId,
			final int chunkId) throws IOException {

		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkArgument(chunkId >= 0);
		checkState(isVotingPortalEnabled, VOTING_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		final Response<List<ControlComponentCodeSharesPayload>> response = messageBrokerOrchestratorClient.downloadGenEncLongCodeShares(
				electionEventId, verificationCardSetId, chunkId).execute();

		checkState(response.isSuccessful() && response.body() != null,
				"Download unsuccessful. [electionEventId: %s, verificationCardSetId: %s, chunkId: %s]", electionEventId, verificationCardSetId,
				chunkId);

		return response.body();
	}
}
