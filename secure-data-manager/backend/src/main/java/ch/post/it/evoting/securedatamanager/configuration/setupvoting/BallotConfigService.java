/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.configuration.setupvoting;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.PrivateKey;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptolib.api.exceptions.GeneralCryptoLibException;
import ch.post.it.evoting.cryptolib.certificates.utils.PemUtils;
import ch.post.it.evoting.cryptolib.commons.serialization.JsonSignatureService;
import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.securedatamanager.config.commons.utils.JsonUtils;
import ch.post.it.evoting.securedatamanager.services.application.exception.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.services.domain.model.status.SynchronizeStatus;
import ch.post.it.evoting.securedatamanager.services.infrastructure.JsonConstants;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballot.BallotRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.ballottext.BallotTextRepository;
import ch.post.it.evoting.securedatamanager.services.infrastructure.service.ConfigurationEntityStatusService;

/**
 * Service for operates with ballots.
 */
@Service
@ConditionalOnProperty("role.isConfig")
public class BallotConfigService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BallotConfigService.class);

	private static final String BALLOT_ID_FIELD = "ballot.id";

	private final ConfigurationEntityStatusService statusService;
	private final BallotRepository ballotRepository;
	private final BallotTextRepository ballotTextRepository;
	private final ObjectMapper objectMapper;
	private final BallotUpdateService ballotUpdateService;

	public BallotConfigService(final ConfigurationEntityStatusService statusService, final BallotRepository ballotRepository,
			final BallotTextRepository ballotTextRepository, final ObjectMapper objectMapper,
			final BallotUpdateService ballotUpdateService) {
		this.statusService = statusService;
		this.ballotRepository = ballotRepository;
		this.ballotTextRepository = ballotTextRepository;
		this.objectMapper = objectMapper;
		this.ballotUpdateService = ballotUpdateService;
	}

	/**
	 * Sign the given ballot and related ballot texts, and change the state of the ballot from locked to SIGNED for a given election event and ballot
	 * id.
	 *
	 * @param electionEventId the election event id.
	 * @param ballotId        the ballot id.
	 * @param privateKeyPEM   the administration board private key in PEM format.
	 * @throws ResourceNotFoundException if the ballot is not found.
	 * @throws GeneralCryptoLibException if the private key cannot be read.
	 */
	public void sign(final String electionEventId, final String ballotId, final String privateKeyPEM)
			throws ResourceNotFoundException, GeneralCryptoLibException {

		final PrivateKey privateKey = PemUtils.privateKeyFromPem(privateKeyPEM);

		final JsonObject ballot = getValidBallot(electionEventId, ballotId);
		final JsonObject modifiedBallot = removeBallotMetaData(ballot);

		JsonObject updatedBallot = ballotUpdateService.updateOptionsRepresentation(electionEventId, modifiedBallot);
		updatedBallot = ballotUpdateService.updateBlankVotingOptionsAlias(electionEventId, updatedBallot);
		ballotRepository.updateBallotContests(ballotId, updatedBallot.getJsonArray(JsonConstants.BALLOT_CONTESTS).toString());
		LOGGER.info("Updated ballot options representation. [ballotId:{}]", ballotId);

		final String signedBallot = JsonSignatureService.sign(privateKey, updatedBallot.toString());
		LOGGER.info("Signed ballot. [ballotId:{}]", ballotId);

		ballotRepository.updateSignedBallot(ballotId, signedBallot);
		LOGGER.info("Updated ballot with signed ballot in repository. [ballotId:{}]", ballotId);

		final JsonArray ballotTexts = getBallotTexts(ballotId);
		LOGGER.info("Loaded ballot texts. [ballotId:{}]", ballotId);

		for (int i = 0; i < ballotTexts.size(); i++) {

			final JsonObject ballotText = ballotTexts.getJsonObject(i);
			final String ballotTextId = ballotText.getString(JsonConstants.ID);
			final JsonObject modifiedBallotText = removeBallotTextMetaData(ballotText);
			final String signedBallotText = JsonSignatureService.sign(privateKey, modifiedBallotText.toString());
			ballotTextRepository.updateSignedBallotText(ballotTextId, signedBallotText);
		}
		LOGGER.info("Signed ballot texts. [ballotId:{}]", ballotId);

		statusService.updateWithSynchronizedStatus(BallotBoxStatus.SIGNED.name(), ballotId, ballotRepository, SynchronizeStatus.PENDING);
		LOGGER.info("Updated ballot status. [ballotId:{}, status:{}]", ballotId, BallotBoxStatus.SIGNED);

		LOGGER.info("The ballot was successfully signed");
	}

	private JsonObject removeBallotTextMetaData(final JsonObject ballotText) {
		return removeField(JsonConstants.SIGNED_OBJECT, ballotText);
	}

	private JsonObject removeBallotMetaData(final JsonObject ballot) {

		JsonObject modifiedBallot = removeField(JsonConstants.STATUS, ballot);
		modifiedBallot = removeField(JsonConstants.DETAILS, modifiedBallot);
		modifiedBallot = removeField(JsonConstants.SYNCHRONIZED, modifiedBallot);
		return removeField(JsonConstants.SIGNED_OBJECT, modifiedBallot);
	}

	private JsonObject removeField(final String field, final JsonObject obj) {

		final JsonObjectBuilder builder = Json.createObjectBuilder();

		for (final Map.Entry<String, JsonValue> e : obj.entrySet()) {
			final String key = e.getKey();
			final JsonValue value = e.getValue();
			if (!key.equals(field)) {
				builder.add(key, value);
			}

		}
		return builder.build();
	}

	private JsonArray getBallotTexts(final String ballotId) {
		final Map<String, Object> ballotTextParams = new HashMap<>();
		ballotTextParams.put(BALLOT_ID_FIELD, ballotId);
		return JsonUtils.getJsonObject(ballotTextRepository.list(ballotTextParams)).getJsonArray(JsonConstants.RESULT);
	}

	private JsonObject getValidBallot(final String electionEventId, final String ballotId) throws ResourceNotFoundException {

		final Optional<JsonObject> possibleBallot = getPossibleValidBallot(electionEventId, ballotId);

		if (possibleBallot.isEmpty()) {
			throw new ResourceNotFoundException("Ballot not found");
		}

		return possibleBallot.get();
	}

	/**
	 * Pre: there is just one matching element
	 *
	 * @return single {@link BallotBoxStatus#LOCKED} ballot in json object format
	 */
	private Optional<JsonObject> getPossibleValidBallot(final String electionEventId, final String ballotId) {

		Optional<JsonObject> ballot = Optional.empty();
		final Map<String, Object> attributeValueMap = new HashMap<>();

		attributeValueMap.put(JsonConstants.ELECTION_EVENT_DOT_ID, electionEventId);
		attributeValueMap.put(JsonConstants.ID, ballotId);
		attributeValueMap.put(JsonConstants.STATUS, BallotBoxStatus.LOCKED.name());
		final String ballotResultListAsJson = ballotRepository.list(attributeValueMap);

		if (StringUtils.isEmpty(ballotResultListAsJson)) {
			return ballot;
		} else {
			final JsonArray ballotResultList = JsonUtils.getJsonObject(ballotResultListAsJson).getJsonArray(JsonConstants.RESULT);

			if (ballotResultList != null && !ballotResultList.isEmpty()) {
				ballot = Optional.of(ballotResultList.getJsonObject(0));
			} else {
				return ballot;
			}
		}
		return ballot;
	}

	public Ballot getBallot(final String electionEventId, final String ballotId) {
		final Map<String, Object> attributeValueMap = new HashMap<>();
		attributeValueMap.put(JsonConstants.ELECTION_EVENT_DOT_ID, electionEventId);
		attributeValueMap.put(JsonConstants.ID, ballotId);
		final String ballotAsJson = ballotRepository.find(attributeValueMap);
		try {
			return objectMapper.readValue(ballotAsJson, Ballot.class);
		} catch (final IOException e) {
			throw new UncheckedIOException("Cannot deserialize the ballot box json string to a valid Ballot object.", e);
		}
	}
}
