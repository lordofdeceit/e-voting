/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.config.commands.encryptionparametersandprimes;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;

/**
 * The output of the GetElectionEventEncryptionParameters algorithm.
 *
 * @param encryptionGroup the {@link GqGroup} with group modulus <i>p</i>, group cardinality <i>q</i>, and group generator <i>g</i>. Must be
 *                        non-null.
 * @param smallPrimes     the vector of small prime group elements. Must be non-null. The elements must belong to the encryption group.
 */
public record GetElectionEventEncryptionParametersOutput(GqGroup encryptionGroup, GroupVector<PrimeGqElement, GqGroup> smallPrimes) {

	public GetElectionEventEncryptionParametersOutput {
		checkNotNull(encryptionGroup);
		checkNotNull(smallPrimes);

		final int omega = smallPrimes.size();
		checkArgument(omega > 0, "There must be at least 1 small prime.");
		checkArgument(encryptionGroup.equals(smallPrimes.getGroup()), "The encryption group must be the same as the small primes' group.");
	}
}
