
Step 1: Generate encryption parameters and prime group members command
 java -jar config-cryptographic-parameters-tool-{VERSION}.jar -genEncryptionParametersAndPrimes -seed_path <path to seed file> [-out <output folder>]

  -seed_path <path to seed file>             The input path of the seed data file for
                                             encryption parameters generation.
  -out (optional)                            Output folder where the files will be
                                             generated. Default = output/

 Example:
  java -jar config-cryptographic-parameters-tool-0.15.3.1.jar -genEncryptionParametersAndPrimes -seed_path seed.txt -out my_output/
