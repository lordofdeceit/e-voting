/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.config.integrationtests.keystore;

import java.io.InputStream;
import java.nio.file.Path;
import java.security.KeyStore;

import javax.annotation.PostConstruct;

import ch.post.it.evoting.cryptolib.api.extendedkeystore.KeyStoreService;
import ch.post.it.evoting.cryptolib.extendedkeystore.cryptoapi.CryptoAPIExtendedKeyStore;

public class KeyStoreServiceForTesting implements KeyStoreService {

	@PostConstruct
	public void initPrivateKey() {
	}

	@Override
	public CryptoAPIExtendedKeyStore createKeyStore() {
		return null;
	}

	@Override
	public CryptoAPIExtendedKeyStore loadKeyStore(InputStream in, KeyStore.PasswordProtection password) {
		return null;
	}

	@Override
	public CryptoAPIExtendedKeyStore loadKeyStore(InputStream in, char[] password) {
		return null;
	}

	@Override
	public CryptoAPIExtendedKeyStore loadKeyStore(Path path, char[] password) {
		return null;
	}

	@Override
	public CryptoAPIExtendedKeyStore loadKeyStoreFromJSON(InputStream in, KeyStore.PasswordProtection password) {
		return null;
	}

	@Override
	public String formatKeyStoreToJSON(InputStream in) {
		return null;
	}
}
