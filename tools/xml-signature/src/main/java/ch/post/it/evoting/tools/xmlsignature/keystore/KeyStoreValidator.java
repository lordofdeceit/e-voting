/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.tools.xmlsignature.keystore;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.Arrays;
import java.util.function.BooleanSupplier;
import java.util.stream.Stream;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;

/**
 * Validates that the keystore contains the private key of this authority and the certificates of the other authorities.
 */
public final class KeyStoreValidator {

	private KeyStoreValidator() {
		// utility class
	}

	/**
	 * Check if the given keystore corresponds to the direct-trust requirements. All public keys must be present and if required to sign, the
	 * related private key.
	 * @param keyStore the keystore to check.
	 * @param signingAlias if not null, ensure that the corresponding private key is present.
	 * @return if keystore is valid.
	 */
	public static boolean validateKeyStore(final KeyStore keyStore, final Alias signingAlias) {
		final BooleanSupplier areSignerCertificatesPresent = () -> Arrays.stream(Alias.values())
				.allMatch(alias -> {
					try {
						return keyStore.getCertificate(alias.get()) != null;
					} catch (final KeyStoreException e) {
						return false;
					}
				});

		final BooleanSupplier isSigningKeyPresent = () -> Stream.of(signingAlias)
				.allMatch(alias -> {
					try {
						return keyStore.getKey(signingAlias.get(), "".toCharArray()) != null;
					} catch (final KeyStoreException | NoSuchAlgorithmException | UnrecoverableKeyException e) {
						return false;
					}
				});

		return areSignerCertificatesPresent.getAsBoolean() && (signingAlias == null || isSigningKeyPresent.getAsBoolean());
	}
}
