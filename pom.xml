<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://maven.apache.org/POM/4.0.0" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">

	<modelVersion>4.0.0</modelVersion>

	<groupId>ch.post.it.evoting</groupId>
	<artifactId>evoting</artifactId>
	<version>1.2.2.0</version>
	<packaging>pom</packaging>
	<name>evoting</name>

	<modules>
		<module>evoting-dependencies</module>
		<module>cryptolib</module>
		<module>domain</module>
		<module>command-messaging</module>
		<module>voting-server</module>
		<module>control-components</module>
		<module>secure-data-manager</module>
		<module>cryptolib-js</module>
		<module>voting-client-js</module>
		<module>voter-portal</module>
		<module>tools/config-cryptographic-parameters-tool</module>
		<module>tools/xml-signature</module>
	</modules>

	<properties>
		<jacoco.version>0.8.8</jacoco.version>

		<maven.compiler.source>17</maven.compiler.source>
		<maven.compiler.target>17</maven.compiler.target>
		<maven.compiler.release>17</maven.compiler.release>

		<maven-deploy-plugin.version>3.0.0-M2</maven-deploy-plugin.version>

		<maven-enforcer-plugin.version>3.0.0</maven-enforcer-plugin.version>
		<maven-enforcer-plugin.requiredJavaVersion>[17.0.5]</maven-enforcer-plugin.requiredJavaVersion>
		<maven-enforcer-plugin.requiredMavenVersion>3.8.6</maven-enforcer-plugin.requiredMavenVersion>
		<maven-javadoc-plugin.version>3.3.2</maven-javadoc-plugin.version>
		<maven-release-plugin.version>3.0.0-M5</maven-release-plugin.version>
		<spring-boot-maven-plugin.version>2.7.0</spring-boot-maven-plugin.version>

		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.build.outputTimestamp>2023-01-12T11:26:19+02:00</project.build.outputTimestamp>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

		<!-- Sonar -->
		<sonar.scm.disabled>true</sonar.scm.disabled>
		<sonar.skipDesign>true</sonar.skipDesign>
		<sonar.coverage.jacoco.xmlReportPaths>${project.basedir}/target/site/jacoco/jacoco.xml</sonar.coverage.jacoco.xmlReportPaths>
		<sonar.junit.reportPaths>${project.basedir}/target/surefire-reports/</sonar.junit.reportPaths>
		<sonar.javascript.lcov.reportPaths>${project.basedir}/coverage/lcov.info</sonar.javascript.lcov.reportPaths>
		<sonar.coverage.exclusions>**/test/**/*,**/tests/**/*</sonar.coverage.exclusions>
		<sonar.java.source>${maven.compiler.release}</sonar.java.source>
		<sonar.java.target>${maven.compiler.release}</sonar.java.target>
		<sonar.issue.ignore.multicriteria>e1</sonar.issue.ignore.multicriteria>
		<sonar.issue.ignore.multicriteria.e1.ruleKey>css:S4667</sonar.issue.ignore.multicriteria.e1.ruleKey>
		<sonar.issue.ignore.multicriteria.e1.resourceKey>**/*.scss</sonar.issue.ignore.multicriteria.e1.resourceKey>
		<skip.it>false</skip.it>
	</properties>

	<build>
		<plugins>
			<plugin>
				<artifactId>maven-deploy-plugin</artifactId>
				<version>${maven-deploy-plugin.version}</version>
			</plugin>
			<plugin>
				<artifactId>maven-release-plugin</artifactId>
				<version>${maven-release-plugin.version}</version>
				<inherited>true</inherited>
				<configuration>
					<preparationGoals>clean install</preparationGoals>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>${maven-javadoc-plugin.version}</version>
				<configuration>
					<doclint>none</doclint>
					<failOnError>false</failOnError>
					<notimestamp>true</notimestamp>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-enforcer-plugin</artifactId>
				<version>${maven-enforcer-plugin.version}</version>
				<executions>
					<execution>
						<goals>
							<goal>enforce</goal>
						</goals>
						<configuration>
							<rules>
								<requireJavaVersion>
									<version>${maven-enforcer-plugin.requiredJavaVersion}</version>
								</requireJavaVersion>
								<requireMavenVersion>
									<version>${maven-enforcer-plugin.requiredMavenVersion}</version>
								</requireMavenVersion>
							</rules>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<version>${jacoco.version}</version>
				<executions>
					<execution>
						<id>prepare-agent</id>
						<goals>
							<goal>prepare-agent</goal>
						</goals>
					</execution>
					<execution>
						<id>report</id>
						<goals>
							<goal>report</goal>
						</goals>
						<phase>prepare-package</phase>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<version>${spring-boot-maven-plugin.version}</version>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-failsafe-plugin</artifactId>
				<configuration>
					<skip>${skip.it}</skip>
				</configuration>
			</plugin>

		</plugins>
	</build>

	<profiles>
		<profile>
			<id>with-final-packaging</id>
			<activation>
				<activeByDefault>false</activeByDefault>
			</activation>
			<modules>
			</modules>
		</profile>
	</profiles>
</project>
