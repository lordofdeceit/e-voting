/*
 * (c) Original Developers indicated in attribution.txt, 2022. All Rights Reserved.
 */
package ch.post.it.evoting.cryptolib.api.symmetric;

import javax.crypto.SecretKey;

import ch.post.it.evoting.cryptolib.api.derivation.CryptoAPIDerivedKey;
import ch.post.it.evoting.cryptolib.api.exceptions.GeneralCryptoLibException;

/**
 * An API for using cryptographic symmetric activities.
 */
public interface SymmetricServiceAPI {

	/**
	 * Generates a {@link javax.crypto.SecretKey} that can be used for encryption.
	 *
	 * <p>The generated {@link javax.crypto.SecretKey} will be configured according to the properties
	 * of the service.
	 *
	 * @return a SecretKey that can be used for encryption.
	 */
	SecretKey getSecretKeyForEncryption();

	/**
	 * Encrypts the given plain text using the given {@link javax.crypto.SecretKey} and an internally generated random Initialization Vector (IV) of
	 * size given by the symmetric algorithm.
	 *
	 * @param key  the {@link javax.crypto.SecretKey} to use.
	 * @param data the plain text to be encrypted.
	 * @return the generated IV and the cipher text.
	 * @throws GeneralCryptoLibException if the key or the data is null or empty or if the encryption process fails.
	 */
	byte[] encrypt(final SecretKey key, final byte[] data) throws GeneralCryptoLibException;

	/**
	 * Decrypts the given data which contains the IV and the cipher text using the given {@link javax.crypto.SecretKey}.
	 *
	 * @param key  the {@link javax.crypto.SecretKey} to use.
	 * @param data the IV and the cipher text.
	 * @return the plain text.
	 * @throws GeneralCryptoLibException if secret key algorithm is not equals to symmetric encryption algorithm.
	 * @throws GeneralCryptoLibException if the key or the data is null or empty or if the decryption process fails.
	 */
	byte[] decrypt(final SecretKey key, final byte[] data) throws GeneralCryptoLibException;

	/**
	 * Returns the {@link SecretKey} ready to use for encrypt/decrypt, from a byte[].
	 *
	 * @param keyBytes the encoded key as a byte array.
	 * @return the {@link SecretKey} for encrypt/decrypt.
	 * @throws GeneralCryptoLibException if the key is null or empty.
	 */
	SecretKey getSecretKeyForEncryptionFromBytes(byte[] keyBytes) throws GeneralCryptoLibException;

	/**
	 * Returns the {@link SecretKey} ready to use for encrypt/decrypt, from a {@link CryptoAPIDerivedKey}.
	 *
	 * @param key the {@link CryptoAPIDerivedKey}.
	 * @return the {@link SecretKey} for encrypt/decrypt.
	 * @throws GeneralCryptoLibException if the key is null or empty.
	 */
	SecretKey getSecretKeyForEncryptionFromDerivedKey(CryptoAPIDerivedKey key) throws GeneralCryptoLibException;
}
