/*
 * (c) Original Developers indicated in attribution.txt, 2022. All Rights Reserved.
 */
package ch.post.it.evoting.cryptolib.mathematical.polynomials;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;

/**
 * This type represents an interpolation point of the polynomial, i.e., a pair of the form (x, P(x)) for a given x.
 */
public record Point(BigInteger x, BigInteger y) {

	/**
	 * A Point defined by the coordinates x and y.
	 *
	 * @param x the x coordinate
	 * @param y the y coordinate
	 */
	public Point {
		checkNotNull(x, "The x-coordinate cannot be null.");
		checkNotNull(y, "The y-coordinate cannot be null.");
	}
}
