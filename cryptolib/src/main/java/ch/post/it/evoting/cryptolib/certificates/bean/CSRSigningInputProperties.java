/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.cryptolib.certificates.bean;

import java.time.ZonedDateTime;

public record CSRSigningInputProperties(ZonedDateTime notBefore, ZonedDateTime notAfter, CertificateParameters.Type type) {

}
