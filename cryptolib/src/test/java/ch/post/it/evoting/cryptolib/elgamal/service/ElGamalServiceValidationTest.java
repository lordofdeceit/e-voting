/*
 * (c) Original Developers indicated in attribution.txt, 2022. All Rights Reserved.
 */
package ch.post.it.evoting.cryptolib.elgamal.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import ch.post.it.evoting.cryptolib.api.exceptions.GeneralCryptoLibException;
import ch.post.it.evoting.cryptolib.elgamal.bean.ElGamalPrivateKey;
import ch.post.it.evoting.cryptolib.primitives.securerandom.constants.SecureRandomConstants;
import ch.post.it.evoting.cryptolib.test.tools.utils.CommonTestDataGenerator;

class ElGamalServiceValidationTest {

	private static String whiteSpaceString;

	@BeforeAll
	static void setUp() {
		whiteSpaceString = CommonTestDataGenerator
				.getWhiteSpaceString(CommonTestDataGenerator.getInt(1, SecureRandomConstants.MAXIMUM_GENERATED_STRING_LENGTH));

	}

	static Stream<Arguments> deserializeElGamalPrivateKey() {

		final String privateKeyWithNullZpSubgroup = "{\"privateKey\":{\"zpSubgroup\":null,\"exponents\":[\"BA==\",\"BQ==\"]}}";
		final String privateKeyWithNullElementList = "{\"privateKey\":{\"zpSubgroup\":{\"p\":\"Fw==\",\"q\":\"Cw==\",\"g\":\"Ag==\"},\"exponents\":null}}";
		final String privateKeyWithEmptyElementList = "{\"privateKey\":{\"zpSubgroup\":{\"p\":\"Fw==\",\"q\":\"Cw==\",\"g\":\"Ag==\"},\"exponents\":[]}}";
		final String privateKeyWithNullInElementList =
				"{\"privateKey\":{\"zpSubgroup\":{\"p\":\"Fw==\",\"q\":\"Cw==\",\"g\":\"Ag==\"}," + "\"exponents\":[\"BA==\",null]}}";

		return Stream.of(arguments(null, ElGamalPrivateKey.class.getSimpleName() + " JSON string is null."),
				arguments("", ElGamalPrivateKey.class.getSimpleName() + " JSON string is blank."),
				arguments(whiteSpaceString, ElGamalPrivateKey.class.getSimpleName() + " JSON string is blank."),
				arguments(privateKeyWithNullZpSubgroup, "Zp subgroup is null."),
				arguments(privateKeyWithNullElementList, "List of ElGamal private key exponents is null."),
				arguments(privateKeyWithEmptyElementList, "List of ElGamal private key exponents is empty."),
				arguments(privateKeyWithNullInElementList, "List of ElGamal private key exponents contains one or more null elements."));
	}

	@ParameterizedTest
	@MethodSource("deserializeElGamalPrivateKey")
	void testElGamalPrivateKeyDeserializationValidation(final String jsonStr, final String errorMsg) {
		final GeneralCryptoLibException exception = assertThrows(GeneralCryptoLibException.class, () -> ElGamalPrivateKey.fromJson(jsonStr));
		assertEquals(errorMsg, exception.getMessage());
	}
}
