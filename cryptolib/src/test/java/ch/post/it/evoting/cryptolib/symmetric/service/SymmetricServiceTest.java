/*
 * (c) Original Developers indicated in attribution.txt, 2022. All Rights Reserved.
 */
package ch.post.it.evoting.cryptolib.symmetric.service;

import javax.crypto.SecretKey;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptolib.api.exceptions.GeneralCryptoLibException;
import ch.post.it.evoting.cryptolib.primitives.primes.bean.TestCryptoDerivedKey;
import ch.post.it.evoting.cryptolib.primitives.primes.utils.PrimitivesTestDataGenerator;
import ch.post.it.evoting.cryptolib.primitives.securerandom.constants.SecureRandomConstants;
import ch.post.it.evoting.cryptolib.test.tools.utils.CommonTestDataGenerator;

/**
 * Tests of the symmetric service API.
 */
class SymmetricServiceTest {

	private static final int LARGE_DATA_LENGTH = 10000000;

	private static SymmetricService symmetricServiceForDefaultPolicy;

	private static SecretKey secretKeyForEncryption;

	private static int dataByteLength;

	private static byte[] data;

	@BeforeAll
	public static void setUp() throws GeneralCryptoLibException {

		symmetricServiceForDefaultPolicy = new SymmetricService();

		secretKeyForEncryption = symmetricServiceForDefaultPolicy.getSecretKeyForEncryption();

		dataByteLength = CommonTestDataGenerator.getInt(1, SecureRandomConstants.MAXIMUM_GENERATED_BYTE_ARRAY_LENGTH);

		data = PrimitivesTestDataGenerator.getByteArray(dataByteLength);
	}

	@Test
	void testWhenSymmetricallyEncryptAndDecryptThenOk() throws GeneralCryptoLibException {

		final byte[] encryptedData = symmetricServiceForDefaultPolicy.encrypt(secretKeyForEncryption, data);

		final byte[] decryptedData = symmetricServiceForDefaultPolicy.decrypt(secretKeyForEncryption, encryptedData);

		Assertions.assertArrayEquals(decryptedData, data);
	}

	@Test
	void testWhenSymmetricallyEncryptAndDecryptWithLargeDataThenOk() throws GeneralCryptoLibException {

		final byte[] data = PrimitivesTestDataGenerator.getByteArray(LARGE_DATA_LENGTH);

		final byte[] encryptedData = symmetricServiceForDefaultPolicy.encrypt(secretKeyForEncryption, data);

		final byte[] decryptedData = symmetricServiceForDefaultPolicy.decrypt(secretKeyForEncryption, encryptedData);

		Assertions.assertArrayEquals(decryptedData, data);
	}

	@Test
	void testWhenGetSecretKeyForEncryptionFromDerivedKeyThenOk() throws GeneralCryptoLibException {

		final TestCryptoDerivedKey derivedKeyForEncryption = new TestCryptoDerivedKey(secretKeyForEncryption.getEncoded());

		final SecretKey secretKey = symmetricServiceForDefaultPolicy.getSecretKeyForEncryptionFromDerivedKey(derivedKeyForEncryption);

		Assertions.assertArrayEquals(secretKey.getEncoded(), secretKeyForEncryption.getEncoded());
	}

	@Test
	void testWhenGetSecretKeyForEncryptionFromBytesThenOk() throws GeneralCryptoLibException {

		final SecretKey secretKey = symmetricServiceForDefaultPolicy.getSecretKeyForEncryptionFromBytes(secretKeyForEncryption.getEncoded());

		Assertions.assertArrayEquals(secretKey.getEncoded(), secretKeyForEncryption.getEncoded());
	}
}
