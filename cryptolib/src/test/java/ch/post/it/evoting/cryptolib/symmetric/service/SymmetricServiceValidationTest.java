/*
 * (c) Original Developers indicated in attribution.txt, 2022. All Rights Reserved.
 */
package ch.post.it.evoting.cryptolib.symmetric.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.nio.charset.StandardCharsets;
import java.util.stream.Stream;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import ch.post.it.evoting.cryptolib.api.derivation.CryptoAPIDerivedKey;
import ch.post.it.evoting.cryptolib.api.exceptions.GeneralCryptoLibException;
import ch.post.it.evoting.cryptolib.primitives.primes.bean.TestCryptoDerivedKey;
import ch.post.it.evoting.cryptolib.primitives.primes.utils.PrimitivesTestDataGenerator;
import ch.post.it.evoting.cryptolib.primitives.securerandom.constants.SecureRandomConstants;
import ch.post.it.evoting.cryptolib.symmetric.key.configuration.SymmetricKeyPolicy;
import ch.post.it.evoting.cryptolib.symmetric.key.configuration.SymmetricKeyPolicyFromProperties;
import ch.post.it.evoting.cryptolib.test.tools.bean.TestSecretKey;
import ch.post.it.evoting.cryptolib.test.tools.utils.CommonTestDataGenerator;

class SymmetricServiceValidationTest {

	private static SymmetricService symmetricServiceForDefaultPolicy;
	private static SecretKey secretKeyForEncryption;
	private static byte[] data;
	private static byte[] emptyByteArray;
	private static TestSecretKey nullContentSecretKey;
	private static TestSecretKey emptyContentSecretKey;
	private static TestCryptoDerivedKey nullContentDerivedKey;
	private static TestCryptoDerivedKey emptyContentDerivedKey;

	@BeforeAll
	public static void setUp() throws GeneralCryptoLibException {

		symmetricServiceForDefaultPolicy = new SymmetricService();

		secretKeyForEncryption = symmetricServiceForDefaultPolicy.getSecretKeyForEncryption();

		final int dataByteLength = CommonTestDataGenerator.getInt(1, SecureRandomConstants.MAXIMUM_GENERATED_BYTE_ARRAY_LENGTH);

		data = PrimitivesTestDataGenerator.getByteArray(dataByteLength);

		emptyByteArray = new byte[0];

		nullContentSecretKey = new TestSecretKey(null);
		emptyContentSecretKey = new TestSecretKey(emptyByteArray);

		nullContentDerivedKey = new TestCryptoDerivedKey(null);
		emptyContentDerivedKey = new TestCryptoDerivedKey(emptyByteArray);
	}

	static Stream<Arguments> symmetricallyEncrypt() {

		return Stream.of(arguments(null, data, "Secret key is null."), arguments(nullContentSecretKey, data, "Secret key content is null."),
				arguments(emptyContentSecretKey, data, "Secret key content is empty."), arguments(secretKeyForEncryption, null, "Data is null."),
				arguments(secretKeyForEncryption, emptyByteArray, "Data is empty."));
	}

	static Stream<Arguments> symmetricallyDecrypt() throws GeneralCryptoLibException {

		final byte[] encryptedData = symmetricServiceForDefaultPolicy.encrypt(secretKeyForEncryption, data);

		return Stream.of(arguments(null, encryptedData, "Secret key is null."),
				arguments(nullContentSecretKey, encryptedData, "Secret key content is null."),
				arguments(emptyContentSecretKey, encryptedData, "Secret key content is empty."),
				arguments(secretKeyForEncryption, null, "Encrypted data is null."),
				arguments(secretKeyForEncryption, emptyByteArray, "Encrypted data is empty."));
	}

	static Stream<Arguments> getSecretKeyForEncryptionFromDerivedKey() {

		return Stream.of(arguments(null, "Derived key is null."), arguments(nullContentDerivedKey, "Derived key content is null."),
				arguments(emptyContentDerivedKey, "Derived key content is empty."));
	}

	static Stream<Arguments> getSecretKeyForEncryptionFromBytes() {

		return Stream.of(arguments(null, "Secret key is null."), arguments(emptyByteArray, "Secret key is empty."));
	}

	@ParameterizedTest
	@MethodSource("symmetricallyEncrypt")
	void testSymmetricEncryptionValidation(final SecretKey key, final byte[] data, final String errorMsg) {
		final GeneralCryptoLibException exception = assertThrows(GeneralCryptoLibException.class,
				() -> symmetricServiceForDefaultPolicy.encrypt(key, data));
		assertEquals(errorMsg, exception.getMessage());
	}

	@ParameterizedTest
	@MethodSource("symmetricallyDecrypt")
	void testSymmetricDecryptionValidation(final SecretKey key, final byte[] data, final String errorMsg) {
		final GeneralCryptoLibException exception = assertThrows(GeneralCryptoLibException.class,
				() -> symmetricServiceForDefaultPolicy.decrypt(key, data));
		assertEquals(errorMsg, exception.getMessage());
	}

	@ParameterizedTest
	@MethodSource("getSecretKeyForEncryptionFromDerivedKey")
	void testSecretKeyForEncryptionFromDerivedKeyValidation(final CryptoAPIDerivedKey key, final String errorMsg) {
		final GeneralCryptoLibException exception = assertThrows(GeneralCryptoLibException.class,
				() -> symmetricServiceForDefaultPolicy.getSecretKeyForEncryptionFromDerivedKey(key));
		assertEquals(errorMsg, exception.getMessage());
	}

	@ParameterizedTest
	@MethodSource("getSecretKeyForEncryptionFromBytes")
	void testSecretKeyForEncryptionFromBytesValidation(final byte[] keyBytes, final String errorMsg) {
		final GeneralCryptoLibException exception = assertThrows(GeneralCryptoLibException.class,
				() -> symmetricServiceForDefaultPolicy.getSecretKeyForEncryptionFromBytes(keyBytes));
		assertEquals(errorMsg, exception.getMessage());
	}

	@Test
	void encryptWithUnsupportedKeyLengthTest() {
		final SymmetricKeyPolicy symmetricKeyPolicy = new SymmetricKeyPolicyFromProperties();
		final int bitsLength = symmetricKeyPolicy.getSecretKeyAlgorithmAndSpec().getKeyLength();
		final byte[] key = new byte[bitsLength / 4];
		final SecretKey secretKeyCreated = new SecretKeySpec(key, "AES");

		final String errorMsg =
				"The specified key's length must be equal to symmetric key length in the policy: " + bitsLength + "; Found " + key.length * 8;
		final GeneralCryptoLibException exception = assertThrows(GeneralCryptoLibException.class,
				() -> symmetricServiceForDefaultPolicy.encrypt(secretKeyCreated, "testMessage".getBytes(StandardCharsets.UTF_8)));
		assertEquals(errorMsg, exception.getMessage());
	}
}
