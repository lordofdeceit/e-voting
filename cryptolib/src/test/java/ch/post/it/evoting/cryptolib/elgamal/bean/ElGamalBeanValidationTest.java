/*
 * (c) Original Developers indicated in attribution.txt, 2022. All Rights Reserved.
 */
package ch.post.it.evoting.cryptolib.elgamal.bean;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import ch.post.it.evoting.cryptolib.api.exceptions.GeneralCryptoLibException;
import ch.post.it.evoting.cryptolib.mathematical.groups.impl.Exponent;
import ch.post.it.evoting.cryptolib.mathematical.groups.impl.ZpGroupElement;
import ch.post.it.evoting.cryptolib.mathematical.groups.impl.ZpSubgroup;
import ch.post.it.evoting.cryptolib.test.tools.configuration.GroupLoader;

class ElGamalBeanValidationTest {

	private static ZpSubgroup zpSubgroup;
	private static List<Exponent> exponentList;
	private static List<ZpGroupElement> elementList;
	private static List<Exponent> emptyExponentList;
	private static List<ZpGroupElement> elementListWithNullValue;

	@BeforeAll
	static void setUp() throws GeneralCryptoLibException {

		final GroupLoader zpGroupLoader = new GroupLoader();
		zpSubgroup = new ZpSubgroup(zpGroupLoader.getG(), zpGroupLoader.getP(), zpGroupLoader.getQ());

		exponentList = new ArrayList<>();
		exponentList.add(new Exponent(zpSubgroup.getQ(), BigInteger.TEN));

		elementList = new ArrayList<>();
		elementList.add(new ZpGroupElement(BigInteger.ONE, zpSubgroup));

		emptyExponentList = new ArrayList<>();

		elementListWithNullValue = new ArrayList<>(elementList);
		elementListWithNullValue.add(null);
	}

	static Stream<Arguments> createElGamalEncryptionParameters() {

		final BigInteger p = zpSubgroup.getP();
		final BigInteger q = zpSubgroup.getQ();
		final BigInteger g = zpSubgroup.getG();

		return Stream.of(arguments(null, q, g, "Zp subgroup p parameter is null."), arguments(p, null, g, "Zp subgroup q parameter is null."),
				arguments(p, q, null, "Zp subgroup generator is null."),
				arguments(p, BigInteger.ZERO, g, "Zp subgroup q parameter must be greater than or equal to : 1; Found 0"), arguments(p, p, g,
						"Zp subgroup q parameter must be less than or equal to Zp subgroup p parameter minus 1: " + p.subtract(BigInteger.ONE)
								+ "; Found " + p),
				arguments(p, q, BigInteger.valueOf(1), "Zp subgroup generator must be greater than or equal to : 2; Found 1"), arguments(p, q, p,
						"Zp subgroup generator must be less than or equal to Zp subgroup p parameter minus 1: " + p.subtract(BigInteger.ONE)
								+ "; Found " + p));
	}

	static Stream<Arguments> createElGamalPrivateKey() {

		final List<Exponent> exponentsWithNullValue = new ArrayList<>(exponentList);
		exponentsWithNullValue.add(null);

		return Stream.of(arguments(null, zpSubgroup, "List of ElGamal private key exponents is null."),
				arguments(emptyExponentList, zpSubgroup, "List of ElGamal private key exponents is empty."),
				arguments(exponentsWithNullValue, zpSubgroup, "List of ElGamal private key exponents contains one or more null elements."),
				arguments(exponentList, null, "Zp subgroup is null."));
	}

	@ParameterizedTest
	@MethodSource("createElGamalPrivateKey")
	void testElGamalPrivateKeyCreationValidation(final List<Exponent> exponents, final ZpSubgroup zpSubgroup, final String errorMsg) {
		final GeneralCryptoLibException exception = assertThrows(GeneralCryptoLibException.class, () -> new ElGamalPrivateKey(exponents, zpSubgroup));
		assertEquals(errorMsg, exception.getMessage());
	}
}
