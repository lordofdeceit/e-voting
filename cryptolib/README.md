# Java cryptographic library

The *cryptolib* library contains legacy implementations of various cryptographic algorithms. We continuously replace the *cryptolib* library's contents with the open-source [crypto-primitives Java library](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives).

## Usage

One can use the cryptolib as a library in other components.

## Development

Check the build instructions in the readme of the repository 'evoting' for compiling the components.
