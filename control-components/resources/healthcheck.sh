#!/usr/bin/env bash
if [  $(unset JAVA_TOOL_OPTIONS && jcmd -l | grep control | awk '{print $1}') -eq 1 ] ; then
  echo "Healthcheck: OK, Java process running"
else
  echo "Healthcheck: NOK: Java main process (pid 1) not found"
  exit 1
fi
