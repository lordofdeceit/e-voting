/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.tally.mixonline;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.security.SecureRandom;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.mixnet.MixnetFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProofFactory;

/**
 * Tests of MixDecOnlineAlgorithm.
 */
@DisplayName("mixDecOnline with")
class MixDecOnlineAlgorithmTest {

	private static final int UUID_LENGTH = 32;
	private static final SecureRandom RANDOM = new SecureRandom();
	private static final Random RANDOM_SERVICE = RandomFactory.createRandom();
	private static final GqGroup GQ_GROUP = GroupTestData.getLargeGqGroup();

	private BallotBoxService ballotBoxService;
	private int n;
	private int l;
	private int delta;
	private int mu;
	private int nodeId;
	private MixDecOnlineAlgorithm mixDecOnlineAlgorithm;
	private MixDecOnlineContext mixDecOnlineContext;
	private MixDecOnlineInput mixDecOnlineInput;

	@BeforeEach
	void setup() {
		n = RANDOM.nextInt(4) + 2;
		l = RANDOM.nextInt(5) + 1;
		delta = l + RANDOM.nextInt(5);
		mu = delta + RANDOM.nextInt(5);
		nodeId = RANDOM.nextInt(4) + 1;
		ballotBoxService = mock(BallotBoxService.class);
		mixDecOnlineAlgorithm = new MixDecOnlineAlgorithm(ballotBoxService, ElGamalFactory.createElGamal(), MixnetFactory.createMixnet(), nodeId,
				ZeroKnowledgeProofFactory.createZeroKnowledgeProof());
		mixDecOnlineContext = generateMixDecryptContext(l);
		mixDecOnlineInput = generateMixDecryptInput(n, l, mu, delta, nodeId);
	}

	@Test
	@DisplayName("null arguments throws a NullPointerException")
	void mixDecOnlineWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class, () -> mixDecOnlineAlgorithm.mixDecOnline(null, mixDecOnlineInput));
		assertThrows(NullPointerException.class, () -> mixDecOnlineAlgorithm.mixDecOnline(mixDecOnlineContext, null));
	}

	@Test
	@DisplayName("context with different group than input throws an IllegalArgumentException")
	void mixDecOnlineWithDifferentGroupThrows() {
		final GqGroup otherGroup = GroupTestData.getDifferentGqGroup(GQ_GROUP);
		final MixDecOnlineContext otherGroupContext = new MixDecOnlineContext.Builder()
				.setEncryptionGroup(otherGroup)
				.setElectionEventId(mixDecOnlineContext.getElectionEventId())
				.setBallotBoxId(mixDecOnlineContext.getBallotBoxId())
				.setNumberOfAllowedWriteInsPlusOne(mixDecOnlineContext.getNumberOfAllowedWriteInsPlusOne())
				.build();
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> mixDecOnlineAlgorithm.mixDecOnline(otherGroupContext, mixDecOnlineInput));
		assertEquals("The context and input must have the same encryption group", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("less than 2 partially decrypted votes throws an IllegalArgumentException")
	void mixDecOnlineWithLessThanTwoPartiallyEncryptedVotesThrows() {
		final MixDecOnlineInput badMixDecOnlineInput = generateMixDecryptInput(1, l, delta, mu, nodeId);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> mixDecOnlineAlgorithm.mixDecOnline(mixDecOnlineContext, badMixDecOnlineInput));
		assertEquals(String.format("There must be at least 2 partially decrypted votes. [N_c_hat: %s]",
				badMixDecOnlineInput.getPartiallyDecryptedVotes().size()), Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("the partially decrypted votes having an element size different from delta_hat throws an IllegalArgumentException")
	void mixDecryptOnlineWithPartiallyDecryptedVotesBadPhiSizeThrows() {
		final MixDecOnlineInput badMixDecOnlineInput = generateMixDecryptInput(n, l + 1, mu, delta, nodeId);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> mixDecOnlineAlgorithm.mixDecOnline(mixDecOnlineContext, badMixDecOnlineInput));
		assertEquals(String.format(
						"The number of element of each partially decrypted vote must be the allowed number of write-ins + 1. [l: %s, delta_hat: %s]",
						badMixDecOnlineInput.getPartiallyDecryptedVotes().getElementSize(), mixDecOnlineContext.getNumberOfAllowedWriteInsPlusOne()),
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("the partially decrypted votes having an element size greater than delta throws an IllegalArgumentException")
	void mixDecryptOnlineWithPartiallyDecryptedVotesElementSizeGreaterThanDeltaThrows() {
		mixDecOnlineContext = generateMixDecryptContext(delta + 1);
		final MixDecOnlineInput badMixDecOnlineInput = generateMixDecryptInput(n, delta + 1, mu, delta, nodeId);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> mixDecOnlineAlgorithm.mixDecOnline(mixDecOnlineContext, badMixDecOnlineInput));
		assertEquals(
				String.format("Each partially decrypted vote must not have more elements than the electoral board public key. [l: %s, delta: %s]",
						badMixDecOnlineInput.getPartiallyDecryptedVotes().getElementSize(), badMixDecOnlineInput.getElectoralBoardPublicKey().size()),
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("mu smaller than delta throws an IllegalArgumentException")
	void mixDecryptOnlineWithMuSmallerThanDeltaThrows() {
		final MixDecOnlineInput badMixDecOnlineInput = generateMixDecryptInput(n, l, mu, mu + 1, nodeId);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> mixDecOnlineAlgorithm.mixDecOnline(mixDecOnlineContext, badMixDecOnlineInput));
		assertEquals(String.format("The electoral board public key must not have more elements than the CCM election keys. [delta: %s, mu: %s]",
						badMixDecOnlineInput.getElectoralBoardPublicKey().size(), badMixDecOnlineInput.getCcmElectionPublicKeys().getElementSize()),
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("CCMj public key not corresponding to CCMj secret key throws an IllegalArgumentException")
	void mixDecOnlineWithBadCcmjKeyPairThrows() {
		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(GQ_GROUP);
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> partiallyDecryptedVotes = elGamalGenerator.genRandomCiphertextVector(n, l);
		final ElGamalMultiRecipientPrivateKey ccmjElectionSecretKey = elGamalGenerator.genRandomPrivateKey(mu);
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmElectionPublicKeys = Stream.generate(
						() -> elGamalGenerator.genRandomPublicKey(mu))
				.limit(4)
				.collect(GroupVector.toGroupVector());
		final ElGamalMultiRecipientPublicKey electoralBoardPublicKey = elGamalGenerator.genRandomPublicKey(delta);

		final MixDecOnlineInput badMixDecOnlineInput = new MixDecOnlineInput.Builder()
				.setPartiallyDecryptedVotes(partiallyDecryptedVotes)
				.setCcmElectionPublicKeys(ccmElectionPublicKeys)
				.setElectoralBoardPublicKey(electoralBoardPublicKey)
				.setCcmjElectionSecretKey(ccmjElectionSecretKey)
				.build();

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> mixDecOnlineAlgorithm.mixDecOnline(mixDecOnlineContext, badMixDecOnlineInput));
		assertEquals("The public key of the reconstituted CCM_j election public key pair does not correspond to the given CCM_j election public key.",
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("valid input does not throw")
	void mixDecryptOnlineWithValidInput() {
		final String ballotBoxId = mixDecOnlineContext.getBallotBoxId();
		when(ballotBoxService.isMixed(ballotBoxId)).thenReturn(false, true);
		mixDecOnlineAlgorithm.mixDecOnline(mixDecOnlineContext, mixDecOnlineInput);
		verify(ballotBoxService).setMixed(ballotBoxId);
		assertTrue(ballotBoxService.isMixed(ballotBoxId));
	}

	private MixDecOnlineContext generateMixDecryptContext(final int numberAllowedWriteInsPlusOne) {
		final String electionEventId = RANDOM_SERVICE.genRandomBase16String(UUID_LENGTH).toLowerCase();
		final String ballotBoxId = RANDOM_SERVICE.genRandomBase16String(UUID_LENGTH).toLowerCase();
		return new MixDecOnlineContext.Builder()
				.setEncryptionGroup(GQ_GROUP)
				.setElectionEventId(electionEventId)
				.setBallotBoxId(ballotBoxId)
				.setNumberOfAllowedWriteInsPlusOne(numberAllowedWriteInsPlusOne)
				.build();
	}

	/**
	 * Generates a MixDecryptInput object with a given number of partially decrypted votes.
	 *
	 * @param n the desired number of partially decrypted votes.
	 * @return a {@link MixDecOnlineInput}
	 */
	private MixDecOnlineInput generateMixDecryptInput(final int n, final int l, final int mu, final int delta, final int nodeId) {
		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(GQ_GROUP);
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> partiallyDecryptedVotes = elGamalGenerator.genRandomCiphertextVector(n, l);
		final ElGamalMultiRecipientPrivateKey ccmjElectionSecretKey = elGamalGenerator.genRandomPrivateKey(mu);
		final List<ElGamalMultiRecipientPublicKey> ccmElectionPublicKeys = Stream.generate(
				() -> elGamalGenerator.genRandomPublicKey(mu)).limit(4).collect(Collectors.toList());
		ccmElectionPublicKeys.set(nodeId - 1, ElGamalMultiRecipientKeyPair.from(ccmjElectionSecretKey, GQ_GROUP.getGenerator()).getPublicKey());
		final ElGamalMultiRecipientPublicKey electoralBoardPublicKey = elGamalGenerator.genRandomPublicKey(delta);

		return new MixDecOnlineInput.Builder()
				.setPartiallyDecryptedVotes(partiallyDecryptedVotes)
				.setCcmElectionPublicKeys(GroupVector.from(ccmElectionPublicKeys))
				.setElectoralBoardPublicKey(electoralBoardPublicKey)
				.setCcmjElectionSecretKey(ccmjElectionSecretKey)
				.build();
	}
}