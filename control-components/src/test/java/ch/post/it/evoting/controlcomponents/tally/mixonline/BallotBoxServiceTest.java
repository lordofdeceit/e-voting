/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.tally.mixonline;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.security.SecureRandom;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import ch.post.it.evoting.controlcomponents.ElectionEventEntity;
import ch.post.it.evoting.controlcomponents.ElectionEventService;
import ch.post.it.evoting.controlcomponents.TestDatabaseCleanUpService;
import ch.post.it.evoting.controlcomponents.VerificationCardSetEntity;
import ch.post.it.evoting.controlcomponents.VerificationCardSetService;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.domain.SecurityLevelTestingOnly;
import ch.post.it.evoting.domain.SerializationUtils;

@SpringBootTest
@SecurityLevelTestingOnly
@ActiveProfiles("test")
@DisplayName("BallotBoxService")
class BallotBoxServiceTest {
	private static final Random RANDOM_SERVICE = RandomFactory.createRandom();
	private static final SecureRandom RANDOM = new SecureRandom();
	private static final int ID_LENGTH = 32;
	private static final String BAD_ID = "Bad Id";

	private static PrimesMappingTable primesMappingTable;

	@Autowired
	private BallotBoxService ballotBoxService;

	@Autowired
	private ElectionEventService electionEventService;

	@Autowired
	private VerificationCardSetService verificationCardSetService;

	// This test does not need keystore functionality
	@MockBean
	private SignatureKeystore<Alias> signatureKeystoreService;

	private String ballotBoxId;
	private String electionEventId;
	private String verificationCardSetId;
	private GqGroup encryptionGroup;
	private boolean testBallotBox;

	@BeforeAll
	static void setupAll() {
		final GqGroup encryptionGroup = SerializationUtils.getGqGroup();
		final GroupVector<PrimeGqElement, GqGroup> smallPrimeGroupMembers = PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers(
				encryptionGroup, 1);
		primesMappingTable = PrimesMappingTable.from(List.of(new PrimesMappingTableEntry("actualVotingOption", smallPrimeGroupMembers.get(0))));
	}

	@BeforeEach
	void setup() {
		ballotBoxId = RANDOM_SERVICE.genRandomBase16String(ID_LENGTH).toLowerCase();
		electionEventId = RANDOM_SERVICE.genRandomBase16String(ID_LENGTH).toLowerCase();
		verificationCardSetId = RANDOM_SERVICE.genRandomBase16String(ID_LENGTH).toLowerCase();
		encryptionGroup = GroupTestData.getGqGroup();
		testBallotBox = RANDOM.nextBoolean();
	}

	@AfterAll
	static void cleanUpAll(
			@Autowired
			final TestDatabaseCleanUpService testDatabaseCleanUpService) {

		testDatabaseCleanUpService.cleanUp();
	}

	@Test
	void testSaveWithBadArgumentThrows() {
		assertThrows(NullPointerException.class,
				() -> ballotBoxService.save(null, verificationCardSetId, testBallotBox, 1, 10, 900, primesMappingTable));
		assertThrows(NullPointerException.class, () -> ballotBoxService.save(ballotBoxId, null, testBallotBox, 1, 10, 900, primesMappingTable));
		assertThrows(NullPointerException.class, () -> ballotBoxService.save(ballotBoxId, null, testBallotBox, 1, 10, 900, null));
		assertThrows(IllegalArgumentException.class,
				() -> ballotBoxService.save(ballotBoxId, verificationCardSetId, testBallotBox, -1, 10, 900, primesMappingTable));
		assertThrows(IllegalArgumentException.class,
				() -> ballotBoxService.save(ballotBoxId, verificationCardSetId, testBallotBox, 1, -1, 900, primesMappingTable));
		assertThrows(IllegalArgumentException.class,
				() -> ballotBoxService.save(ballotBoxId, verificationCardSetId, testBallotBox, 1, 10, -1, primesMappingTable));
	}

	@Test
	void testExistsWithBadArgumentThrows() {
		assertThrows(NullPointerException.class, () -> ballotBoxService.existsForElectionEventId(null, electionEventId));
		assertThrows(NullPointerException.class, () -> ballotBoxService.existsForElectionEventId(ballotBoxId, null));
		assertThrows(FailedValidationException.class, () -> ballotBoxService.existsForElectionEventId(BAD_ID, electionEventId));
		assertThrows(FailedValidationException.class, () -> ballotBoxService.existsForElectionEventId(ballotBoxId, BAD_ID));
	}

	@Test
	void testExistsWithValidArgumentDoesNotThrow() {
		final boolean existsBefore = ballotBoxService.existsForElectionEventId(ballotBoxId, electionEventId);
		assertFalse(existsBefore);
		setUpElection();
		final boolean existsAfter = ballotBoxService.existsForElectionEventId(ballotBoxId, electionEventId);
		assertTrue(existsAfter);
	}

	@Test
	void testGetBallotBoxWithBadArgumentThrows() {
		assertThrows(NullPointerException.class, () -> ballotBoxService.getBallotBox((String) null));
		assertThrows(FailedValidationException.class, () -> ballotBoxService.getBallotBox(BAD_ID));
	}

	@Test
	void testGetBallotBoxForUnsavedBallotBoxThrows() {
		assertThrows(IllegalStateException.class, () -> ballotBoxService.getBallotBox(ballotBoxId));
	}

	@Test
	void testGetBallotBoxWithValidArgumentDoesNotThrow() {
		setUpElection();
		final BallotBoxEntity ballotBoxEntity = ballotBoxService.getBallotBox(ballotBoxId);
		assertEquals(ballotBoxId, ballotBoxEntity.getBallotBoxId());
		assertEquals(testBallotBox, ballotBoxEntity.isTestBallotBox());
	}

	@Test
	void testIsMixedWithBadArgumentThrows() {
		assertThrows(NullPointerException.class, () -> ballotBoxService.isMixed(null));
	}

	@Test
	void testIsMixedWithValidArgumentDoesNotThrow() {
		setUpElection();
		assertFalse(ballotBoxService.isMixed(ballotBoxId));
		ballotBoxService.setMixed(ballotBoxId);
		assertTrue(ballotBoxService.isMixed(ballotBoxId));
	}

	@Test
	void testSetMixedSetsMixedStateCorrectly() {
		setUpElection();
		final BallotBoxEntity ballotBoxEntity = ballotBoxService.setMixed(ballotBoxId);
		assertTrue(ballotBoxEntity.isMixed());
	}

	private void setUpElection() {
		// Save election event.
		final ElectionEventEntity savedElectionEventEntity = electionEventService.save(electionEventId, encryptionGroup);
		// Save verification card set.
		final VerificationCardSetEntity verificationCardSetEntity = new VerificationCardSetEntity(verificationCardSetId,
				savedElectionEventEntity, new CombinedCorrectnessInformation(Collections.emptyList()));
		verificationCardSetService.save(verificationCardSetEntity);

		ballotBoxService.save(ballotBoxId, verificationCardSetId, testBallotBox, 1, 10, 900, primesMappingTable);
	}
}
