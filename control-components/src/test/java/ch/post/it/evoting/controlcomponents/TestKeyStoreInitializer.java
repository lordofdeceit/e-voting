/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;

public class TestKeyStoreInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

	public static final Path KEYSTORE_DIRECTORY_PATH = Path.of("target", "direct-trust");
	public static final Path KEYSTORE_FILENAME_PATH = KEYSTORE_DIRECTORY_PATH.resolve("signing_keystore_test.p12");
	public static final Path KEYSTORE_PASSWORD_FILENAME_PATH = KEYSTORE_DIRECTORY_PATH.resolve("signing_pw_test.txt");

	@Override
	public void initialize(final ConfigurableApplicationContext applicationContext) {
		try {
			if (Files.notExists(KEYSTORE_FILENAME_PATH) || Files.notExists(KEYSTORE_PASSWORD_FILENAME_PATH)) {
				FileUtils.deleteDirectory(KEYSTORE_DIRECTORY_PATH.toFile());
			}
			Files.createDirectories(KEYSTORE_DIRECTORY_PATH);

			final String keystoreLocation = KEYSTORE_FILENAME_PATH.toString();
			final String keystorePasswordLocation = KEYSTORE_PASSWORD_FILENAME_PATH.toString();

			KeystoreFilesCreator.create(keystoreLocation, keystorePasswordLocation, Alias.CONTROL_COMPONENT_1.get());

			final Map<String, String> properties = new HashMap<>();
			properties.put("direct.trust.keystore.location", keystoreLocation);
			properties.put("direct.trust.keystore.password.location", keystorePasswordLocation);
			TestPropertyValues.of(properties).applyTo(applicationContext);

		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}
}
