/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.voting.confirmvote;

import static ch.post.it.evoting.controlcomponents.voting.confirmvote.CreateLVCCShareAlgorithm.MAX_CONFIRMATION_ATTEMPTS;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.controlcomponents.VerificationCardStateService;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.math.Base64;
import ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.GqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.utils.KeyDerivation;
import ch.post.it.evoting.cryptoprimitives.utils.KeyDerivationFactory;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;

/**
 * Tests of CreateLVCCShareAlgorithm.
 */
@DisplayName("CreateLVCCShareService")
class CreateLVCCShareAlgorithmTest extends TestGroupSetup {

	private static final int NODE_ID = 1;
	private static final int l_ID = 32;
	private static final Hash hash = spy(Hash.class);
	private static final Base64 base64 = BaseEncodingFactory.createBase64();
	private static final ZeroKnowledgeProof zeroKnowledgeProof = spy(ZeroKnowledgeProof.class);
	private static final KeyDerivation keyDerivation = spy(KeyDerivationFactory.createKeyDerivation());
	private static final VerificationCardStateService verificationCardStateService = mock(VerificationCardStateService.class);

	private static CreateLVCCShareAlgorithm createLVCCShareAlgorithm;

	@BeforeAll
	static void setUpAll() {
		createLVCCShareAlgorithm = new CreateLVCCShareAlgorithm(keyDerivation, hash, base64, zeroKnowledgeProof,
				verificationCardStateService);
	}

	@Nested
	@DisplayName("calling createLVCCShare with")
	class CreateLVCCTest {

		private GqElement confirmationKey;
		private ZqElement ccrjReturnCodesGenerationSecretKey;
		private String verificationCardId;
		private LVCCContext context;
		private CreateLVCCShareInput input;

		@BeforeEach
		void setUp() {
			confirmationKey = gqGroupGenerator.genMember();
			ccrjReturnCodesGenerationSecretKey = zqGroupGenerator.genRandomZqElementMember();

			final String electionEventId = RandomFactory.createRandom().genRandomBase16String(l_ID).toLowerCase();
			final String verificationCardSetId = RandomFactory.createRandom().genRandomBase16String(l_ID).toLowerCase();
			verificationCardId = RandomFactory.createRandom().genRandomBase16String(l_ID).toLowerCase();

			context = new LVCCContext(gqGroup, NODE_ID, electionEventId, verificationCardSetId);
			input = new CreateLVCCShareInput(confirmationKey, ccrjReturnCodesGenerationSecretKey, verificationCardId);
		}

		@Test
		@DisplayName("valid parameters does not throw")
		void validParameters() {
			when(verificationCardStateService.isSentVote(verificationCardId)).thenReturn(true);
			when(verificationCardStateService.isNotConfirmedVote(verificationCardId)).thenReturn(true);

			final GqGroup gqGroup = GroupTestData.getLargeGqGroup();
			final ZqGroup zqGroup = ZqGroup.sameOrderAs(gqGroup);
			final GqGroupGenerator gqGroupGenerator = new GqGroupGenerator(gqGroup);
			final ZqGroupGenerator zqGroupGenerator = new ZqGroupGenerator(zqGroup);

			confirmationKey = gqGroupGenerator.genMember();
			ccrjReturnCodesGenerationSecretKey = zqGroupGenerator.genRandomZqElementMember();

			final CreateLVCCShareInput createLVCCShareInput = new CreateLVCCShareInput(confirmationKey, ccrjReturnCodesGenerationSecretKey,
					input.verificationCardId());
			final LVCCContext lvccContext = new LVCCContext(gqGroup, context.nodeId(), context.electionEventId(), context.verificationCardSetId());

			doReturn(new byte[] { 0x4 }).when(hash).recursiveHash(any());
			doReturn(gqGroupGenerator.genMember()).when(hash).hashAndSquare(any(), any());

			final ExponentiationProof exponentiationProof = new ExponentiationProof(zqGroupGenerator.genRandomZqElementMember(),
					zqGroupGenerator.genRandomZqElementMember());
			doReturn(exponentiationProof).when(zeroKnowledgeProof).genExponentiationProof(any(), any(), any(), any());

			final CreateLVCCShareOutput output = createLVCCShareAlgorithm.createLVCCShare(lvccContext, createLVCCShareInput);

			assertEquals(0, output.getConfirmationAttempts());
			assertEquals(gqGroup, output.getHashedSquaredConfirmationKey().getGroup());
			assertEquals(gqGroup, output.getLongVoteCastReturnCodeShare().getGroup());
			assertEquals(gqGroup, output.getVoterVoteCastReturnCodeGenerationPublicKey().getGroup());
			assertTrue(gqGroup.hasSameOrderAs(output.getExponentiationProof().getGroup()));
		}

		@Test
		@DisplayName("any null parameter throws NullPointerException")
		void nullParameters() {
			assertAll(
					() -> assertThrows(NullPointerException.class,
							() -> createLVCCShareAlgorithm.createLVCCShare(context, null)),
					() -> assertThrows(NullPointerException.class,
							() -> createLVCCShareAlgorithm.createLVCCShare(null, input))
			);
		}

		@Test
		@DisplayName("confirmation key and secret key having different group order throws IllegalArgumentException")
		void diffGroupKeys() {
			final GqElement otherGroupConfirmationKey = otherGqGroupGenerator.genMember();

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
					() -> new CreateLVCCShareInput(otherGroupConfirmationKey, ccrjReturnCodesGenerationSecretKey, verificationCardId));
			assertEquals("The confirmation key must have the same order as the CCRj Return Codes Generation secret key.", exception.getMessage());
		}

		@Test
		@DisplayName("long choice return codes not computed throw IllegalArgumentException")
		void notPreviouslyComputedLCC() {
			when(verificationCardStateService.isSentVote(verificationCardId)).thenReturn(false);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
					() -> createLVCCShareAlgorithm.createLVCCShare(context, input));
			final String message = String.format(
					"The CCR_j cannot create the LVCC Share since it did not compute the long Choice Return Code shares for the verification card. [vc_id: %s]",
					verificationCardId);
			assertEquals(message, exception.getMessage());
		}

		@Test
		@DisplayName("max confirmation attempts exceeded throws IllegalArgumentException")
		void exceededAttempts() {
			when(verificationCardStateService.isSentVote(verificationCardId)).thenReturn(true);
			when(verificationCardStateService.isNotConfirmedVote(verificationCardId)).thenReturn(true);
			when(verificationCardStateService.getConfirmationAttempts(verificationCardId)).thenReturn(MAX_CONFIRMATION_ATTEMPTS);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
					() -> createLVCCShareAlgorithm.createLVCCShare(context, input));
			assertEquals(String.format("Max confirmation attempts of %s exceeded.", MAX_CONFIRMATION_ATTEMPTS), exception.getMessage());
		}

	}

}
