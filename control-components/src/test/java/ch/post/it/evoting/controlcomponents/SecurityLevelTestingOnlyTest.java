/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.securitylevel.SecurityLevel;
import ch.post.it.evoting.domain.SecurityLevelTestingOnly;

class SecurityLevelTestingOnlyTest {

	@Test
	@SecurityLevelTestingOnly
	void testingOnlySecurityLevel() {
		assertEquals("TESTING_ONLY", SecurityLevel.getSystemSecurityLevel().toString());
	}

	@Test
	void defaultSecurityLevel() {
		assertEquals("EXTENDED", SecurityLevel.getSystemSecurityLevel().toString());
	}

}
