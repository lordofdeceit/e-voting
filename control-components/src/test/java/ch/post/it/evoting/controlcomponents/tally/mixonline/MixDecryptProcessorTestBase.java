/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.tally.mixonline;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.amqp.core.Message;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Streams;

import ch.post.it.evoting.controlcomponents.ElectionContextService;
import ch.post.it.evoting.controlcomponents.ElectionEventEntity;
import ch.post.it.evoting.controlcomponents.ElectionEventService;
import ch.post.it.evoting.controlcomponents.SetupComponentPublicKeysService;
import ch.post.it.evoting.controlcomponents.TestDatabaseCleanUpService;
import ch.post.it.evoting.controlcomponents.VerificationCardSetEntity;
import ch.post.it.evoting.controlcomponents.VerificationCardSetService;
import ch.post.it.evoting.controlcomponents.voting.VotingIntegrationTestBase;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionEventContext;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.cryptoprimitives.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamal;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.SchnorrProof;

public class MixDecryptProcessorTestBase extends VotingIntegrationTestBase {

	private static final Map<Integer, MixDecryptResponse> responseMessages = new HashMap<>();
	private static final Random RANDOM_SERVICE = RandomFactory.createRandom();
	private static final ElGamal elGamal = ElGamalFactory.createElGamal();
	protected static GqGroup gqGroup;
	protected static String ballotBoxIdToMix;
	private static ElGamalGenerator elGamalGenerator;
	private static ElectionEventContext electionEventContext;
	private static SetupComponentPublicKeys setupComponentPublicKeys;

	private static List<ElGamalMultiRecipientKeyPair> ccmjElectionKeyPairs;

	static {
		setUpElection();
	}

	@BeforeAll
	static void setUpAll(
			@Autowired
			final ElectionEventService electionEventService,
			@Autowired
			final ElectionContextService electionContextService,
			@Autowired
			final SetupComponentPublicKeysService setupComponentPublicKeysService,
			@Autowired
			final VerificationCardSetService verificationCardSetService) {

		// Save election event.
		final ElectionEventEntity electionEventEntity = electionEventService.save(electionEventContext.electionEventId(), gqGroup);

		// Save verification card sets.
		final List<VerificationCardSetContext> verificationCardSetContexts = electionEventContext.verificationCardSetContexts();
		verificationCardSetContexts.forEach(verificationCardSetContext -> {
			final String verificationCardSetId = verificationCardSetContext.verificationCardSetId();
			final VerificationCardSetEntity verificationCardSetEntity = new VerificationCardSetEntity(verificationCardSetId, electionEventEntity,
					new CombinedCorrectnessInformation(Collections.emptyList()));
			verificationCardSetService.save(verificationCardSetEntity);
		});

		// Save election context
		electionContextService.save(electionEventContext);
		setupComponentPublicKeysService.save(electionEventId, setupComponentPublicKeys);

		ballotBoxIdToMix = verificationCardSetContexts.get(0).ballotBoxId();
	}

	@AfterAll
	static void cleanUp(
			@Autowired
			final TestDatabaseCleanUpService testDatabaseCleanUpService) {

		testDatabaseCleanUpService.cleanUp();
	}

	private static void setUpElection() {
		gqGroup = GroupTestData.getLargeGqGroup();
		elGamalGenerator = new ElGamalGenerator(gqGroup);
		final int l = 2;

		ccmjElectionKeyPairs = IntStream.rangeClosed(1, 4)
				.mapToObj(nodeId -> ElGamalMultiRecipientKeyPair.genKeyPair(gqGroup, l, RANDOM_SERVICE))
				.toList();

		electionEventContext = createElectionEventContext(electionEventId, 5, l);
		setupComponentPublicKeys = createSetupComponentPublicKeys(l);
	}

	private static ElectionEventContext createElectionEventContext(final String electionEventId, final int verificationCardSetContextNbr,
			final int l) {

		final GroupVector<PrimeGqElement, GqGroup> smallPrimeGroupMembers = PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers(
				gqGroup, 1);
		final PrimesMappingTable primesMappingTable = PrimesMappingTable.from(
				List.of(new PrimesMappingTableEntry("actualVotingOption", smallPrimeGroupMembers.get(0))));

		final List<VerificationCardSetContext> verificationCardSetContexts = Stream.generate(() -> {
					final String verificationCardSetId = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase();
					final String ballotBoxId = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase();
					return new VerificationCardSetContext(verificationCardSetId, ballotBoxId, true, 1, 10, 900, primesMappingTable);
				})
				.limit(verificationCardSetContextNbr)
				.toList();

		final LocalDateTime startTime = LocalDateTime.now();
		final LocalDateTime finishTime = startTime.plusWeeks(1);

		return new ElectionEventContext(electionEventId, verificationCardSetContexts, startTime, finishTime);
	}

	private static SetupComponentPublicKeys createSetupComponentPublicKeys(final int l) {

		final List<ControlComponentPublicKeys> combinedControlComponentPublicKeys = new ArrayList<>();

		IntStream.rangeClosed(1, 4)
				.forEach(nodeId -> combinedControlComponentPublicKeys.add(generateCombinedControlComponentPublicKeys(nodeId, l)));

		final ElGamalMultiRecipientPublicKey electoralBoardPublicKey = elGamalGenerator.genRandomPublicKey(l);
		final GroupVector<SchnorrProof, ZqGroup> electoralBoardSchnorrProofs = generateSchnorrProofs(l);

		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccrChoiceReturnCodePublicKeys = combinedControlComponentPublicKeys.stream()
				.map(ControlComponentPublicKeys::ccrjChoiceReturnCodesEncryptionPublicKey)
				.collect(GroupVector.toGroupVector());

		final ElGamalMultiRecipientPublicKey choiceReturnCodesPublicKey = elGamal.combinePublicKeys(ccrChoiceReturnCodePublicKeys);

		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmEBElectionPublicKeys = Streams.concat(
				combinedControlComponentPublicKeys.stream()
						.map(ControlComponentPublicKeys::ccmjElectionPublicKey),
				Stream.of(electoralBoardPublicKey)).collect(GroupVector.toGroupVector());

		final ElGamalMultiRecipientPublicKey electionPublicKey = elGamal.combinePublicKeys(ccmEBElectionPublicKeys);

		return new SetupComponentPublicKeys(combinedControlComponentPublicKeys, electoralBoardPublicKey, electoralBoardSchnorrProofs, electionPublicKey, choiceReturnCodesPublicKey);
	}

	private static ControlComponentPublicKeys generateCombinedControlComponentPublicKeys(final int nodeId, final int l) {
		final GroupVector<SchnorrProof, ZqGroup> schnorrProofs = generateSchnorrProofs(l);
		final ElGamalMultiRecipientPublicKey ccrjChoiceReturnCodesPublicKey = elGamalGenerator.genRandomPublicKey(l);

		return new ControlComponentPublicKeys(nodeId, ccrjChoiceReturnCodesPublicKey, schnorrProofs,
				ccmjElectionKeyPairs.get(nodeId - 1).getPublicKey(), schnorrProofs);
	}

	private static GroupVector<SchnorrProof, ZqGroup> generateSchnorrProofs(final int l) {
		final ZqGroup zqGroup = ZqGroup.sameOrderAs(gqGroup);
		final ZqElement e = ZqElement.create(2, zqGroup);
		final ZqElement z = ZqElement.create(2, zqGroup);
		final SchnorrProof schnorrProof = new SchnorrProof(e, z);
		return Collections.nCopies(l, schnorrProof).stream().collect(GroupVector.toGroupVector());
	}

	protected void addResponseMessage(final int nodeId, final String electionEventId, final String ballotBoxId, final Message responseMessage) {
		responseMessages.put(nodeId, new MixDecryptResponse(electionEventId, ballotBoxId, responseMessage));
	}

	protected MixDecryptResponse getResponseMessage(final int nodeId) {
		return responseMessages.get(nodeId);
	}

	protected static ElGamalMultiRecipientKeyPair getCcmjKeyPair(final int nodeId) {
		return ccmjElectionKeyPairs.get(nodeId - 1);
	}

	protected static class MixDecryptResponse {

		private final String electionEventId;
		private final String ballotBoxId;
		private final Message message;

		MixDecryptResponse(final String electionEventId, final String ballotBoxId, final Message message) {
			this.electionEventId = electionEventId;
			this.ballotBoxId = ballotBoxId;
			this.message = message;
		}

		public String getElectionEventId() {
			return electionEventId;
		}

		public String getBallotBoxId() {
			return ballotBoxId;
		}

		public Message getMessage() {
			return message;
		}
	}

}
