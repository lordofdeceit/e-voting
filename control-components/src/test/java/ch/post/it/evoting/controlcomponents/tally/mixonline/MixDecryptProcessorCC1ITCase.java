/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.tally.mixonline;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.after;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.security.SignatureException;
import java.util.Collections;
import java.util.UUID;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.ClassOrderer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestClassOrder;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.annotation.DirtiesContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponents.CcmjElectionKeysService;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.tally.MixDecryptOnlineRequestPayload;

@TestClassOrder(ClassOrderer.OrderAnnotation.class)
@Order(1)
@SpringBootTest(properties = { "nodeID=1" })
@DisplayName("MixDecryptProcessor on node 1 consuming")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
class MixDecryptProcessorCC1ITCase extends MixDecryptProcessorTestBase {

	private static final int NODE_ID_1 = 1;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	private ObjectMapper objectMapper;

	@SpyBean
	private MixDecryptProcessor mixDecryptProcessor;

	@SpyBean
	private MixDecryptService mixDecryptService;

	@SpyBean
	private VerifyMixDecOnlineAlgorithm verifyMixDecOnlineAlgorithm;

	@SpyBean
	private SignatureKeystore<Alias> signatureKeystoreService;

	@BeforeAll
	static void setUpAll(
			@Autowired
			final CcmjElectionKeysService ccmjElectionKeysService) {

		// Each node needs to save its ccmj election key pair.
		ccmjElectionKeysService.save(electionEventId, getCcmjKeyPair(NODE_ID_1));
	}

	@Test
	@DisplayName("a mixDecryptOnlineRequestPayload correctly mixes")
	void request() throws IOException, SignatureException {
		// Request payload.
		final MixDecryptOnlineRequestPayload mixDecryptOnlineRequestPayload = new MixDecryptOnlineRequestPayload(electionEventId, ballotBoxIdToMix,
				NODE_ID_1, Collections.emptyList());

		final byte[] request = objectMapper.writeValueAsBytes(mixDecryptOnlineRequestPayload);

		final MessageProperties messageProperties = new MessageProperties();
		final String correlationId = UUID.randomUUID().toString();
		messageProperties.setCorrelationId(correlationId);

		// Sends a request to the processor.
		final Message message = new Message(request, messageProperties);
		rabbitTemplate.send(RABBITMQ_EXCHANGE, MIX_DEC_ONLINE_REQUEST_QUEUE_1, message);

		// Collects the response of the processor.
		final Message responseMessage = rabbitTemplate.receive(MIX_DEC_ONLINE_RESPONSE_QUEUE_1, 5000);

		assertNotNull(responseMessage);
		assertEquals(correlationId, responseMessage.getMessageProperties().getCorrelationId());

		verify(mixDecryptProcessor, after(5000).times(1)).onMessage(any());
		verify(mixDecryptService).performMixDecrypt(any(), any(), any());
		verify(verifyMixDecOnlineAlgorithm, never()).verifyMixDecOnline(any(), any());
		verify(signatureKeystoreService, times(2)).generateSignature(any(), any());

		// Saves the response so the next nodes can use it.
		addResponseMessage(NODE_ID_1, electionEventId, ballotBoxIdToMix, responseMessage);
	}

}