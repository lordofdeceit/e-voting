/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.voting.sendvote;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.controlcomponents.ElectionEventEntity;
import ch.post.it.evoting.controlcomponents.VerificationCardSetEntity;
import ch.post.it.evoting.controlcomponents.VerificationCardSetService;
import ch.post.it.evoting.controlcomponents.VerificationCardStateService;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.election.CorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.cryptoprimitives.math.Base64;
import ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.GqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.utils.KeyDerivation;
import ch.post.it.evoting.cryptoprimitives.utils.KeyDerivationFactory;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;

/**
 * Tests of CreateLCCShareAlgorithm.
 */
@DisplayName("CreateLCCShareAlgorithm")
class CreateLCCShareAlgorithmTest extends TestGroupSetup {

	private static final int NODE_ID = 1;
	private static final int PSI = 5;
	private static final int l_ID = 32;
	private static final List<BigInteger> LIST_OF_WRITE_IN_OPTIONS = Collections.emptyList();
	private static final String CORRECTNESS_ID = "1";
	private static final Hash hash = spy(HashFactory.createHash());
	private static final ZeroKnowledgeProof zeroKnowledgeProof = spy(ZeroKnowledgeProof.class);
	private static final Base64 base64 = BaseEncodingFactory.createBase64();
	private static final VerificationCardSetService verificationCardSetServiceMock = mock(VerificationCardSetService.class);
	private static final VerificationCardStateService verificationCardStateServiceMock = mock(VerificationCardStateService.class);
	private static final KeyDerivation keyDerivation = spy(KeyDerivationFactory.createKeyDerivation());
	private static CreateLCCShareAlgorithm createLCCShareAlgorithm;

	@BeforeAll
	static void setUpAll() {
		createLCCShareAlgorithm = new CreateLCCShareAlgorithm(keyDerivation, hash, zeroKnowledgeProof, base64, verificationCardSetServiceMock,
				verificationCardStateServiceMock);
	}

	@Nested
	@DisplayName("calling createLCCShare with")
	class CreateLCCShareTest {

		private String verificationCardId;
		private CreateLCCShareContext context;
		private CreateLCCShareInput input;

		@BeforeEach
		void setUp() {
			boolean allDistinct;
			GroupVector<GqElement, GqGroup> partialChoiceReturnCodes;
			do {
				partialChoiceReturnCodes = gqGroupGenerator.genRandomGqElementVector(PSI);
				allDistinct = partialChoiceReturnCodes.stream()
						.allMatch(ConcurrentHashMap.newKeySet()::add);
			}
			while (!allDistinct);

			final String electionEventId = RandomFactory.createRandom().genRandomBase16String(l_ID).toLowerCase();
			final String verificationCardSetId = RandomFactory.createRandom().genRandomBase16String(l_ID).toLowerCase();
			verificationCardId = RandomFactory.createRandom().genRandomBase16String(l_ID).toLowerCase();

			final GqGroup gqGroup = GroupTestData.getLargeGqGroup();
			final ZqGroup zqGroup = ZqGroup.sameOrderAs(gqGroup);
			final GqGroupGenerator gqGroupGenerator = new GqGroupGenerator(gqGroup);
			final ZqGroupGenerator zqGroupGenerator = new ZqGroupGenerator(zqGroup);
			final ZqElement ccrjReturnCodesGenerationSecretKey = zqGroupGenerator.genRandomZqElementMember();

			doReturn(new byte[] { 0x4 }).when(hash).recursiveHash(any());
			final List<String> allowList = partialChoiceReturnCodes.stream()
					.map(pCC_id_i -> hash.hashAndSquare(pCC_id_i.getValue(), gqGroup))
					.map(hpCC_id_i -> hash.recursiveHash(hpCC_id_i, HashableString.from(verificationCardId), HashableString.from(electionEventId)))
					.map(base64::base64Encode)
					.toList();

			boolean otherAllDistinct;
			GroupVector<GqElement, GqGroup> otherPartialChoiceReturnCodes;
			do {
				otherPartialChoiceReturnCodes = gqGroupGenerator.genRandomGqElementVector(PSI);
				otherAllDistinct = otherPartialChoiceReturnCodes.stream()
						.allMatch(ConcurrentHashMap.newKeySet()::add);
			}
			while (!otherAllDistinct);

			context = new CreateLCCShareContext(gqGroup, NODE_ID, electionEventId, verificationCardSetId, PSI, allowList);
			input = new CreateLCCShareInput(otherPartialChoiceReturnCodes, ccrjReturnCodesGenerationSecretKey, verificationCardId);

			final ExponentiationProof exponentiationProof = new ExponentiationProof(zqGroupGenerator.genRandomZqElementMember(),
					zqGroupGenerator.genRandomZqElementMember());
			doReturn(exponentiationProof).when(zeroKnowledgeProof).genExponentiationProof(any(), any(), any(), any());

			reset(verificationCardSetServiceMock);
		}

		@Test
		@DisplayName("valid parameters does not throw")
		void validParameters() {
			final String verificationCardSetId = context.verificationCardSetId();
			final CombinedCorrectnessInformation combinedCorrectnessInformation = new CombinedCorrectnessInformation(
					Collections.singletonList(new CorrectnessInformation(CORRECTNESS_ID, PSI, PSI, LIST_OF_WRITE_IN_OPTIONS)));
			final VerificationCardSetEntity verificationCardSetEntity = new VerificationCardSetEntity(verificationCardSetId,
					new ElectionEventEntity(), combinedCorrectnessInformation);
			when(verificationCardSetServiceMock.getVerificationCardSet(verificationCardSetId)).thenReturn(verificationCardSetEntity);

			when(verificationCardStateServiceMock.isPartiallyDecrypted(verificationCardId)).thenReturn(true);
			when(verificationCardStateServiceMock.isNotSentVote(verificationCardId)).thenReturn(true);

			final CreateLCCShareOutput output = createLCCShareAlgorithm.createLCCShare(context, input);

			assertEquals(PSI, output.getHashedPartialChoiceReturnCodes().size());
			assertEquals(PSI, output.getLongChoiceReturnCodeShare().size());
			assertTrue(context.encryptionGroup().hasSameOrderAs(output.getExponentiationProof().getGroup()));
			assertTrue(context.encryptionGroup().hasSameOrderAs(output.getVoterChoiceReturnCodeGenerationPublicKey().getGroup()));
		}

		@Test
		@DisplayName("any null parameter throws NullPointerException")
		void nullParameters() {
			assertAll(
					() -> assertThrows(NullPointerException.class, () -> createLCCShareAlgorithm.createLCCShare(null, input)),
					() -> assertThrows(NullPointerException.class, () -> createLCCShareAlgorithm.createLCCShare(context, null))
			);
		}

		@Test
		@DisplayName("codes and keys having different group order throws IllegalArgumentException")
		void diffGroupCodesKeys() {
			final ZqElement otherGroupSecretKey = otherZqGroupGenerator.genRandomZqElementMember();
			final GroupVector<GqElement, GqGroup> partialChoiceReturnCodes = input.partialChoiceReturnCodes();

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
					() -> new CreateLCCShareInput(partialChoiceReturnCodes, otherGroupSecretKey, verificationCardId));
			assertEquals("The partial choice return codes and return codes generation secret key must have the same group order.",
					exception.getMessage());
		}

		@Test
		@DisplayName("partial codes not all distinct throws IllegalArgumentException")
		void notDistinctCodes() {
			final GroupVector<GqElement, GqGroup> notDistinctCodes = GroupVector.from(input.partialChoiceReturnCodes())
					.append(input.partialChoiceReturnCodes().get(0));
			final ZqElement ccrjReturnCodesGenerationSecretKey = input.ccrjReturnCodesGenerationSecretKey();

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
					() -> new CreateLCCShareInput(notDistinctCodes, ccrjReturnCodesGenerationSecretKey, verificationCardId));
			assertEquals("All pCC must be distinct.", exception.getMessage());
		}

		@Test
		@DisplayName("not yet partially decrypted codes throws IllegalArgumentException")
		void didNotYetPartiallyDecrypt() {
			final String verificationCardSetId = context.verificationCardSetId();
			final CombinedCorrectnessInformation combinedCorrectnessInformation = new CombinedCorrectnessInformation(
					Collections.singletonList(new CorrectnessInformation("1", PSI, PSI, LIST_OF_WRITE_IN_OPTIONS)));
			final VerificationCardSetEntity verificationCardSetEntity = new VerificationCardSetEntity(verificationCardSetId,
					new ElectionEventEntity(), combinedCorrectnessInformation);
			when(verificationCardSetServiceMock.getVerificationCardSet(verificationCardSetId)).thenReturn(verificationCardSetEntity);

			when(verificationCardStateServiceMock.isPartiallyDecrypted(verificationCardId)).thenReturn(false);
			when(verificationCardStateServiceMock.isNotSentVote(verificationCardId)).thenReturn(true);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
					() -> createLCCShareAlgorithm.createLCCShare(context, input));
			assertEquals(
					String.format("The partial Choice Return Codes have not yet been partially decrypted. [vc_id: %s].",
							verificationCardId), exception.getMessage());
		}

		@Test
		@DisplayName("long choice return codes share already generated throws IllegalArgumentException")
		void alreadyGeneratedLongChoiceReturnCodesShare() {
			final String verificationCardSetId = context.verificationCardSetId();
			final CombinedCorrectnessInformation combinedCorrectnessInformation = new CombinedCorrectnessInformation(
					Collections.singletonList(new CorrectnessInformation("1", PSI, PSI, LIST_OF_WRITE_IN_OPTIONS)));
			final VerificationCardSetEntity verificationCardSetEntity = new VerificationCardSetEntity(verificationCardSetId,
					new ElectionEventEntity(), combinedCorrectnessInformation);
			when(verificationCardSetServiceMock.getVerificationCardSet(verificationCardSetId)).thenReturn(verificationCardSetEntity);

			when(verificationCardStateServiceMock.isPartiallyDecrypted(verificationCardId)).thenReturn(true);
			when(verificationCardStateServiceMock.isNotSentVote(verificationCardId)).thenReturn(false);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
					() -> createLCCShareAlgorithm.createLCCShare(context, input));
			assertEquals(
					String.format("The CCR_j already generated the long Choice Return Code share in a previous attempt. [vc_id: %s].",
							verificationCardId), exception.getMessage());
		}

		@Test
		@DisplayName("partial choice return code not in allow list throws IllegalArgumentException")
		void pccNotInAllowList() {
			final String verificationCardSetId = context.verificationCardSetId();
			final List<String> allowList = Collections.singletonList(base64.base64Encode("asdasd".getBytes(StandardCharsets.UTF_8)));
			final CombinedCorrectnessInformation combinedCorrectnessInformation = new CombinedCorrectnessInformation(
					Collections.singletonList(new CorrectnessInformation(CORRECTNESS_ID, PSI, PSI, LIST_OF_WRITE_IN_OPTIONS)));
			final VerificationCardSetEntity verificationCardSetEntity = new VerificationCardSetEntity(verificationCardSetId,
					new ElectionEventEntity(), combinedCorrectnessInformation);
			when(verificationCardSetServiceMock.getVerificationCardSet(verificationCardSetId)).thenReturn(verificationCardSetEntity);

			when(verificationCardStateServiceMock.isPartiallyDecrypted(verificationCardId)).thenReturn(true);
			when(verificationCardStateServiceMock.isNotSentVote(verificationCardId)).thenReturn(true);

			final CreateLCCShareContext otherContext = new CreateLCCShareContext(context.encryptionGroup(), context.nodeId(),
					context.electionEventId(), context.verificationCardSetId(), context.numberOfSelectableVotingOptions(), allowList);
			final IllegalStateException exception = assertThrows(IllegalStateException.class,
					() -> createLCCShareAlgorithm.createLCCShare(otherContext, input));
			assertEquals("The partial Choice Return Codes allow list does not contain the partial Choice Return Code.", exception.getMessage());
		}
	}

}
