/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.configuration.setupvoting;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.security.SecureRandom;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import com.google.common.base.Throwables;

import ch.post.it.evoting.controlcomponents.VerificationCardService;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.utils.KeyDerivation;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;

/**
 * Tests of GenEncLongCodeSharesAlgorithm.
 */
@DisplayName("A GenEncLongCodeSharesAlgorithm")
class GenEncLongCodeSharesAlgorithmTest extends TestGroupSetup {

	private static final Random random = RandomFactory.createRandom();
	private static final int NUM_KEY_ELEMENTS = 5;
	private static final int CONFIRMATION_KEY_SIZE = 1;

	private static final String ELECTION_EVENT_ID = random.genRandomBase16String(32).toLowerCase();
	private static final String VERIFICATION_CARD_SET_ID = random.genRandomBase16String(32).toLowerCase();
	private static final List<String> VERIFICATION_CARD_IDS = List.of(
			random.genRandomBase16String(32).toLowerCase(),
			random.genRandomBase16String(32).toLowerCase(),
			random.genRandomBase16String(32).toLowerCase());

	private static final int NODE_ID = 1;

	private static final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
	private static final ElGamalGenerator elGamalGeneratorOther = new ElGamalGenerator(GroupTestData.getDifferentGqGroup(gqGroup));

	private static int ciphertextSize;
	private static ZqElement returnCodesGenerationSecretKey;
	private static List<String> verificationCardIDs;
	private static GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> verificationCardPublicKeys;
	private static GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedHashedPartialChoiceReturnCodes;
	private static GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedHashedConfirmationKeys;

	private final ZeroKnowledgeProof zeroKnowledgeProof = mock(ZeroKnowledgeProof.class);
	private final KeyDerivation keyDerivation = mock(KeyDerivation.class);
	private final VerificationCardService verificationCardService = mock(VerificationCardService.class);
	private final ElGamalMultiRecipientKeyPair ccmKeyPair = ElGamalMultiRecipientKeyPair.genKeyPair(gqGroup, NUM_KEY_ELEMENTS, random);

	private GenEncLongCodeSharesAlgorithm genEncLongCodeSharesAlgorithm;
	private GenEncLongCodeSharesContext context;
	private GenEncLongCodeSharesInput input;

	@BeforeAll
	static void setupAll() {
		ciphertextSize = new SecureRandom().nextInt(5) + 1;
	}

	@BeforeEach
	void setup() {
		genEncLongCodeSharesAlgorithm = new GenEncLongCodeSharesAlgorithm(keyDerivation, zeroKnowledgeProof, verificationCardService);

		returnCodesGenerationSecretKey = zqGroupGenerator.genRandomZqElementMember();
		verificationCardIDs = VERIFICATION_CARD_IDS;
		verificationCardPublicKeys = GroupVector.of(ccmKeyPair.getPublicKey(), ccmKeyPair.getPublicKey(), ccmKeyPair.getPublicKey());
		encryptedHashedPartialChoiceReturnCodes = elGamalGenerator.genRandomCiphertextVector(3, ciphertextSize);
		encryptedHashedConfirmationKeys = elGamalGenerator.genRandomCiphertextVector(3, CONFIRMATION_KEY_SIZE);

		context = new GenEncLongCodeSharesContext.Builder()
				.setElectionEventId(ELECTION_EVENT_ID)
				.setVerificationCardSetId(VERIFICATION_CARD_SET_ID)
				.setEncryptionGroup(gqGroup)
				.setNodeId(NODE_ID)
				.setNumberOfVotingOptions(ciphertextSize)
				.build();

		input = new GenEncLongCodeSharesInput.Builder()
				.returnCodesGenerationSecretKey(returnCodesGenerationSecretKey)
				.verificationCardIds(verificationCardIDs)
				.verificationCardPublicKeys(verificationCardPublicKeys)
				.encryptedHashedPartialChoiceReturnCodes(encryptedHashedPartialChoiceReturnCodes)
				.encryptedHashedConfirmationKeys(encryptedHashedConfirmationKeys)
				.build();

		when(verificationCardService.exists(any())).thenReturn(false);
		when(keyDerivation.KDFToZq(any(), any(), any())).thenReturn(zqGroupGenerator.genRandomZqElementMember());
		when(zeroKnowledgeProof.genExponentiationProof(any(), any(), any(), any()))
				.thenReturn(new ExponentiationProof(zqGroupGenerator.genRandomZqElementMember(), zqGroupGenerator.genRandomZqElementMember()));
	}

	@Test
	@DisplayName("valid parameters does not throw")
	void validParamDoesNotThrow() {
		assertDoesNotThrow(() -> genEncLongCodeSharesAlgorithm.genEncLongCodeShares(context, input));

		verify(verificationCardService, times(3)).exists(any());
		verify(verificationCardService, times(1)).saveAll(any());
	}

	@Test
	@DisplayName("null parameter throws NullPointerException")
	void nullParamThrows() {
		assertThrows(NullPointerException.class, () -> genEncLongCodeSharesAlgorithm.genEncLongCodeShares(null, null));
		assertThrows(NullPointerException.class, () -> genEncLongCodeSharesAlgorithm.genEncLongCodeShares(context, null));
		assertThrows(NullPointerException.class, () -> genEncLongCodeSharesAlgorithm.genEncLongCodeShares(null, input));
	}

	@Test
	@DisplayName("parameters already been generated voting cards IllegalArgumentException")
	void alreadyGeneratedVotingCardThrow() {
		when(verificationCardService.exists(any())).thenReturn(false, true, false);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> genEncLongCodeSharesAlgorithm.genEncLongCodeShares(context, input));
		assertEquals("Voting cards have already been generated.", Throwables.getRootCause(exception).getMessage());

	}

	@Nested
	@DisplayName("using a GenEncLongCodeSharesContext built with")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class GenEncLongCodeSharesContextTest {

		@Test
		@DisplayName("null election event id throws NullPointerException")
		void nullElectionEventId() {
			final GenEncLongCodeSharesContext.Builder builder = new GenEncLongCodeSharesContext.Builder()
					.setElectionEventId(null)
					.setVerificationCardSetId(VERIFICATION_CARD_SET_ID)
					.setEncryptionGroup(gqGroup)
					.setNodeId(NODE_ID)
					.setNumberOfVotingOptions(ciphertextSize);

			assertThrows(NullPointerException.class, builder::build);
		}

		@Test
		@DisplayName("invalid election event id throws FailedValidationException")
		void invalidElectionEventId() {
			GenEncLongCodeSharesContext.Builder builder = new GenEncLongCodeSharesContext.Builder()
					.setElectionEventId("")
					.setVerificationCardSetId(VERIFICATION_CARD_SET_ID)
					.setEncryptionGroup(gqGroup)
					.setNodeId(NODE_ID)
					.setNumberOfVotingOptions(ciphertextSize);

			assertThrows(FailedValidationException.class, builder::build);

			builder = builder.setElectionEventId("0b88257ec32142b");
			assertThrows(FailedValidationException.class, builder::build);
		}

		@Test
		@DisplayName("null verification card set id throws NullPointerException")
		void nullVerificationCardSetId() {
			final GenEncLongCodeSharesContext.Builder builder = new GenEncLongCodeSharesContext.Builder()
					.setElectionEventId(ELECTION_EVENT_ID)
					.setVerificationCardSetId(null)
					.setEncryptionGroup(gqGroup)
					.setNodeId(NODE_ID)
					.setNumberOfVotingOptions(ciphertextSize);

			assertThrows(NullPointerException.class, builder::build);
		}

		@Test
		@DisplayName("invalid verification card set id throws FailedValidationException")
		void invalidVerificationCardSetId() {
			GenEncLongCodeSharesContext.Builder builder = new GenEncLongCodeSharesContext.Builder()
					.setElectionEventId(ELECTION_EVENT_ID)
					.setVerificationCardSetId("")
					.setEncryptionGroup(gqGroup)
					.setNodeId(NODE_ID)
					.setNumberOfVotingOptions(ciphertextSize);

			assertThrows(FailedValidationException.class, builder::build);

			builder = builder.setVerificationCardSetId("f1bd7b195d6f38");
			assertThrows(FailedValidationException.class, builder::build);
		}

		@Test
		@DisplayName("null encryption group throws NullPointerException")
		void nullEncryptionGroup() {
			final GenEncLongCodeSharesContext.Builder builder = new GenEncLongCodeSharesContext.Builder()
					.setElectionEventId(ELECTION_EVENT_ID)
					.setVerificationCardSetId(VERIFICATION_CARD_SET_ID)
					.setEncryptionGroup(null)
					.setNodeId(NODE_ID)
					.setNumberOfVotingOptions(ciphertextSize);

			assertThrows(NullPointerException.class, builder::build);
		}

		@Test
		@DisplayName("invalid node id throws IllegalArgumentException")
		void invalidNodeId() {
			final int invalidNodeId = 0;
			final GenEncLongCodeSharesContext.Builder builder = new GenEncLongCodeSharesContext.Builder()
					.setElectionEventId(ELECTION_EVENT_ID)
					.setVerificationCardSetId(VERIFICATION_CARD_SET_ID)
					.setEncryptionGroup(gqGroup)
					.setNodeId(invalidNodeId)
					.setNumberOfVotingOptions(ciphertextSize);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String errorMessage = String.format("The node id must be part of the known node ids. [nodeId: %s]", invalidNodeId);
			assertEquals(errorMessage, Throwables.getRootCause(exception).getMessage());
		}

		@Test
		@DisplayName("invalid number of voting options throws IllegalArgumentException")
		void invalidNumberOfVotingOptions() {
			final GenEncLongCodeSharesContext.Builder builder = new GenEncLongCodeSharesContext.Builder()
					.setElectionEventId(ELECTION_EVENT_ID)
					.setVerificationCardSetId(VERIFICATION_CARD_SET_ID)
					.setEncryptionGroup(gqGroup)
					.setNodeId(NODE_ID)
					.setNumberOfVotingOptions(0);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);
			assertEquals("The number of voting options must be strictly positive.", Throwables.getRootCause(exception).getMessage());
		}

		@Test
		@DisplayName("valid parameters gives expected context")
		void validParameters() {
			final GenEncLongCodeSharesContext.Builder builder = new GenEncLongCodeSharesContext.Builder()
					.setElectionEventId(ELECTION_EVENT_ID)
					.setVerificationCardSetId(VERIFICATION_CARD_SET_ID)
					.setEncryptionGroup(gqGroup)
					.setNodeId(NODE_ID)
					.setNumberOfVotingOptions(ciphertextSize);

			assertDoesNotThrow(builder::build);
		}
	}

	@Nested
	@DisplayName("using a GenEncLongCodeSharesInput built with")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class GenEncLongCodeSharesInputTest {

		@Test
		@DisplayName("any null parameter throws NullPointerException")
		void anyNullParameter() {
			final GenEncLongCodeSharesInput.Builder builderWithNullReturnCodesGenerationSecretKey = new GenEncLongCodeSharesInput.Builder()
					.verificationCardIds(verificationCardIDs)
					.verificationCardPublicKeys(verificationCardPublicKeys)
					.encryptedHashedPartialChoiceReturnCodes(encryptedHashedPartialChoiceReturnCodes)
					.encryptedHashedConfirmationKeys(encryptedHashedConfirmationKeys);

			final GenEncLongCodeSharesInput.Builder builderWithNullVerificationCardIds = new GenEncLongCodeSharesInput.Builder()
					.returnCodesGenerationSecretKey(returnCodesGenerationSecretKey)
					.verificationCardPublicKeys(verificationCardPublicKeys)
					.encryptedHashedPartialChoiceReturnCodes(encryptedHashedPartialChoiceReturnCodes)
					.encryptedHashedConfirmationKeys(encryptedHashedConfirmationKeys);

			final GenEncLongCodeSharesInput.Builder builderWithNullVerificationCardPublicKeys = new GenEncLongCodeSharesInput.Builder()
					.returnCodesGenerationSecretKey(returnCodesGenerationSecretKey)
					.verificationCardIds(verificationCardIDs)
					.encryptedHashedPartialChoiceReturnCodes(encryptedHashedPartialChoiceReturnCodes)
					.encryptedHashedConfirmationKeys(encryptedHashedConfirmationKeys);

			final GenEncLongCodeSharesInput.Builder builderWithNullEncryptedHashedPartialChoiceReturnCodes = new GenEncLongCodeSharesInput.Builder()
					.returnCodesGenerationSecretKey(returnCodesGenerationSecretKey)
					.verificationCardIds(verificationCardIDs)
					.verificationCardPublicKeys(verificationCardPublicKeys)
					.encryptedHashedConfirmationKeys(encryptedHashedConfirmationKeys);

			final GenEncLongCodeSharesInput.Builder builderWithNullEncryptedHashedConfirmationKeys = new GenEncLongCodeSharesInput.Builder()
					.returnCodesGenerationSecretKey(returnCodesGenerationSecretKey)
					.verificationCardIds(verificationCardIDs)
					.verificationCardPublicKeys(verificationCardPublicKeys)
					.encryptedHashedPartialChoiceReturnCodes(encryptedHashedPartialChoiceReturnCodes);

			assertAll(
					() -> assertThrows(NullPointerException.class, builderWithNullReturnCodesGenerationSecretKey::build),
					() -> assertThrows(NullPointerException.class, builderWithNullVerificationCardIds::build),
					() -> assertThrows(NullPointerException.class, builderWithNullVerificationCardPublicKeys::build),
					() -> assertThrows(NullPointerException.class, builderWithNullEncryptedHashedPartialChoiceReturnCodes::build),
					() -> assertThrows(NullPointerException.class, builderWithNullEncryptedHashedConfirmationKeys::build)
			);
		}

		@Test
		@DisplayName("failing size checks throws IllegalArgumentException")
		void failingSizeChecks() {
			final int N_E = verificationCardIDs.size();
			final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> verificationCardPublicKeys_plus1 =
					GroupVector.of(ccmKeyPair.getPublicKey(), ccmKeyPair.getPublicKey(), ccmKeyPair.getPublicKey(), ccmKeyPair.getPublicKey());

			GenEncLongCodeSharesInput.Builder builder =
					new GenEncLongCodeSharesInput.Builder()
							.returnCodesGenerationSecretKey(returnCodesGenerationSecretKey)
							.verificationCardIds(verificationCardIDs)
							.verificationCardPublicKeys(verificationCardPublicKeys_plus1)
							.encryptedHashedPartialChoiceReturnCodes(encryptedHashedPartialChoiceReturnCodes)
							.encryptedHashedConfirmationKeys(encryptedHashedConfirmationKeys);

			IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);

			String message = String.format("The vector verification Card Public Keys is of incorrect size [size: expected: %S, actual: %S]",
					N_E, verificationCardPublicKeys_plus1.size());
			assertEquals(message, Throwables.getRootCause(exception).getMessage());

			final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedHashedPartialChoiceReturnCodes_plus1 =
					elGamalGenerator.genRandomCiphertextVector(4, 1);

			builder = builder.verificationCardPublicKeys(verificationCardPublicKeys)
					.encryptedHashedPartialChoiceReturnCodes(encryptedHashedPartialChoiceReturnCodes_plus1);

			exception = assertThrows(IllegalArgumentException.class, builder::build);

			message = String.format("The vector encrypted, hashed partial Choice Return Codes is of incorrect size [size: expected: %S, actual: %S]",
					verificationCardIDs.size(), verificationCardPublicKeys_plus1.size());
			assertEquals(message, Throwables.getRootCause(exception).getMessage());

			final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedHashedConfirmationKeys_plus1 =
					elGamalGenerator.genRandomCiphertextVector(4, 1);

			builder = builder.encryptedHashedPartialChoiceReturnCodes(encryptedHashedPartialChoiceReturnCodes)
					.encryptedHashedConfirmationKeys(encryptedHashedConfirmationKeys_plus1);

			exception = assertThrows(IllegalArgumentException.class, builder::build);

			message = String.format("The vector encrypted, hashed Confirmation Keys is of incorrect size [size: expected: %S, actual: %S]",
					N_E, encryptedHashedConfirmationKeys_plus1.size());
			assertEquals(message, Throwables.getRootCause(exception).getMessage());
		}

		@Test
		@DisplayName("invalid size confirmation key throws IllegalArgumentException")
		void invalidSizeConfirmationKey() {
			final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> tooManyElementsConfirmationKey = elGamalGenerator.genRandomCiphertextVector(3,
					2);

			final GenEncLongCodeSharesInput.Builder builder =
					new GenEncLongCodeSharesInput.Builder()
							.returnCodesGenerationSecretKey(returnCodesGenerationSecretKey)
							.verificationCardIds(verificationCardIDs)
							.verificationCardPublicKeys(verificationCardPublicKeys)
							.encryptedHashedPartialChoiceReturnCodes(encryptedHashedPartialChoiceReturnCodes)
							.encryptedHashedConfirmationKeys(tooManyElementsConfirmationKey);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String message = String.format("The encrypted hashed Confirmation keys must be of size 1. [actual phi: %s]", 2);
			assertEquals(message, Throwables.getRootCause(exception).getMessage());
		}

		@Test
		@DisplayName("different group orders throws IllegalArgumentException")
		void differentGroupOrders() {
			final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedHashedConfirmationKeys_grp2 =
					elGamalGeneratorOther.genRandomCiphertextVector(3, 1);

			final GenEncLongCodeSharesInput.Builder builder =
					new GenEncLongCodeSharesInput.Builder()
							.returnCodesGenerationSecretKey(returnCodesGenerationSecretKey)
							.verificationCardIds(verificationCardIDs)
							.verificationCardPublicKeys(verificationCardPublicKeys)
							.encryptedHashedPartialChoiceReturnCodes(encryptedHashedPartialChoiceReturnCodes)
							.encryptedHashedConfirmationKeys(encryptedHashedConfirmationKeys_grp2);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String message = "The Vector of exponentiated, encrypted, hashed partial Choice Return Codes and the Vector of exponentiated, "
					+ "encrypted, hashed Confirmation Keys do not have the same group order.";
			assertEquals(message, Throwables.getRootCause(exception).getMessage());
		}

		@Test
		@DisplayName("different groups throws IllegalArgumentException")
		void differentGroups() {
			final ElGamalMultiRecipientKeyPair ccmKeyPairOther = ElGamalMultiRecipientKeyPair.genKeyPair(otherGqGroup, NUM_KEY_ELEMENTS, random);
			final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> verificationCardPublicKeysOther = GroupVector.of(
					ccmKeyPairOther.getPublicKey(),
					ccmKeyPairOther.getPublicKey(), ccmKeyPairOther.getPublicKey());

			final GenEncLongCodeSharesInput.Builder builder =
					new GenEncLongCodeSharesInput.Builder()
							.returnCodesGenerationSecretKey(returnCodesGenerationSecretKey)
							.verificationCardIds(verificationCardIDs)
							.verificationCardPublicKeys(verificationCardPublicKeysOther)
							.encryptedHashedPartialChoiceReturnCodes(encryptedHashedPartialChoiceReturnCodes)
							.encryptedHashedConfirmationKeys(encryptedHashedConfirmationKeys);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String message = "The exponentiated, encrypted, hashed partial Choice Return Codes and the verification card public keys must have the same group.";
			assertEquals(message, Throwables.getRootCause(exception).getMessage());
		}

		@Test
		@DisplayName("non unique verification card ids throws IllegalArgumentException")
		void nonUniqueVerificationCardIds() {
			final List<String> verificationCardIdsWithDuplicates = List.of(VERIFICATION_CARD_IDS.get(0), VERIFICATION_CARD_IDS.get(1),
					VERIFICATION_CARD_IDS.get(0));

			final GenEncLongCodeSharesInput.Builder builder = new GenEncLongCodeSharesInput.Builder();
			builder.verificationCardIds(verificationCardIdsWithDuplicates)
					.verificationCardPublicKeys(verificationCardPublicKeys)
					.encryptedHashedPartialChoiceReturnCodes(encryptedHashedPartialChoiceReturnCodes)
					.encryptedHashedConfirmationKeys(encryptedHashedConfirmationKeys)
					.returnCodesGenerationSecretKey(returnCodesGenerationSecretKey);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);
			assertEquals("The list of verification card ids contains duplicated values.", Throwables.getRootCause(exception).getMessage());
		}

		@Test
		@DisplayName("valid parameters gives expected input")
		void validParameters() {
			final GenEncLongCodeSharesInput.Builder builder =
					new GenEncLongCodeSharesInput.Builder()
							.returnCodesGenerationSecretKey(returnCodesGenerationSecretKey)
							.verificationCardIds(verificationCardIDs)
							.verificationCardPublicKeys(verificationCardPublicKeys)
							.encryptedHashedPartialChoiceReturnCodes(encryptedHashedPartialChoiceReturnCodes)
							.encryptedHashedConfirmationKeys(encryptedHashedConfirmationKeys);

			assertDoesNotThrow(builder::build);
		}
	}

	@Nested
	@DisplayName("outputting a GenEncLongCodeSharesOutput built with")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class GenEncLongCodeSharesOutputTest {

		private final int SIZE_10 = 10;
		private final GqGroup generator = GroupTestData.getDifferentGqGroup(gqGroup);
		private final GqGroup generator2 = GroupTestData.getDifferentGqGroup(gqGroup);

		private final List<GqElement> K_j_id = Stream.generate(generator::getGenerator).limit(SIZE_10).toList();
		private final List<GqElement> Kc_j_id = Stream.generate(generator2::getGenerator).limit(SIZE_10).toList();
		private final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> c_expPCC_j_id = elGamalGenerator.genRandomCiphertextVector(10,
				ciphertextSize);
		private final List<ExponentiationProof> pi_expPCC_j_id =
				Stream.generate(
								() -> new ExponentiationProof(zqGroupGenerator.genRandomZqElementMember(), zqGroupGenerator.genRandomZqElementMember()))
						.limit(SIZE_10)
						.collect(Collectors.toList());

		private final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> c_expCK_j_id = elGamalGenerator.genRandomCiphertextVector(10,
				ciphertextSize);
		private final List<ExponentiationProof> pi_expCK_j_id =
				Stream.generate(
								() -> new ExponentiationProof(zqGroupGenerator.genRandomZqElementMember(), zqGroupGenerator.genRandomZqElementMember()))
						.limit(SIZE_10)
						.collect(Collectors.toList());

		@Test
		@DisplayName("valid parameters gives expected output")
		void validParameters() {
			final GenEncLongCodeSharesOutput output =
					new GenEncLongCodeSharesOutput.Builder()
							.setVoterChoiceReturnCodeGenerationPublicKeys(K_j_id)
							.setVoterVoteCastReturnCodeGenerationPublicKeys(Kc_j_id)
							.setExponentiatedEncryptedHashedPartialChoiceReturnCodes(c_expPCC_j_id)
							.setProofsCorrectExponentiationPartialChoiceReturnCodes(pi_expPCC_j_id)
							.setExponentiatedEncryptedHashedConfirmationKeys(c_expCK_j_id)
							.setProofsCorrectExponentiationConfirmationKeys(pi_expCK_j_id)
							.build();

			final GqGroup confirmationKeysGroup = output.getExponentiatedEncryptedHashedConfirmationKeys().getGroup();
			final GqGroup partialChoiceReturnCodesGroup = output.getExponentiatedEncryptedHashedPartialChoiceReturnCodes().getGroup();

			assertTrue(confirmationKeysGroup.hasSameOrderAs(partialChoiceReturnCodesGroup));
		}

		@Test
		@DisplayName("any null parameter throws NullPointerException")
		void anyNullParameter() {
			GenEncLongCodeSharesOutput.Builder builder = new GenEncLongCodeSharesOutput.Builder()
					.setVoterChoiceReturnCodeGenerationPublicKeys(K_j_id)
					.setVoterVoteCastReturnCodeGenerationPublicKeys(Kc_j_id)
					.setExponentiatedEncryptedHashedPartialChoiceReturnCodes(c_expPCC_j_id)
					.setProofsCorrectExponentiationPartialChoiceReturnCodes(pi_expPCC_j_id)
					.setExponentiatedEncryptedHashedConfirmationKeys(c_expCK_j_id)
					.setProofsCorrectExponentiationConfirmationKeys(pi_expCK_j_id);

			builder = builder.setVoterChoiceReturnCodeGenerationPublicKeys(null);
			Exception exception = assertThrows(NullPointerException.class, builder::build);

			assertEquals("The Vector of Voter Choice Return Code Generation public keys is null.", Throwables.getRootCause(exception).getMessage());

			builder = builder.setVoterChoiceReturnCodeGenerationPublicKeys(K_j_id)
					.setVoterVoteCastReturnCodeGenerationPublicKeys(null);
			exception = assertThrows(NullPointerException.class, builder::build);

			assertEquals("The Vector of Voter Vote Cast Return Code Generation public keys is null.",
					Throwables.getRootCause(exception).getMessage());

			builder = builder.setVoterVoteCastReturnCodeGenerationPublicKeys(Kc_j_id)
					.setExponentiatedEncryptedHashedPartialChoiceReturnCodes(null);
			exception = assertThrows(NullPointerException.class, builder::build);

			assertEquals("The Vector of exponentiated, encrypted, hashed partial Choice Return Codes is null.",
					Throwables.getRootCause(exception).getMessage());

			builder = builder.setExponentiatedEncryptedHashedPartialChoiceReturnCodes(c_expPCC_j_id)
					.setProofsCorrectExponentiationPartialChoiceReturnCodes(null);
			exception = assertThrows(NullPointerException.class, builder::build);

			assertEquals("The Proofs of correct exponentiation of the partial Choice Return Codes is null.",
					Throwables.getRootCause(exception).getMessage());

			builder = builder.setProofsCorrectExponentiationPartialChoiceReturnCodes(pi_expPCC_j_id)
					.setExponentiatedEncryptedHashedConfirmationKeys(null);
			exception = assertThrows(NullPointerException.class, builder::build);

			assertEquals("The Vector of exponentiated, encrypted, hashed Confirmation Keys is null.",
					Throwables.getRootCause(exception).getMessage());

			builder = builder.setExponentiatedEncryptedHashedConfirmationKeys(c_expCK_j_id)
					.setProofsCorrectExponentiationConfirmationKeys(null);
			exception = assertThrows(NullPointerException.class, builder::build);

			assertEquals("The Proofs of correct exponentiation of the Confirmation Keys is null.", Throwables.getRootCause(exception).getMessage());
		}

		@Test
		@DisplayName("empty vectors throws IllegalArgumentException")
		void emptyVectors() {
			final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> emptyVector = GroupVector.of();

			final List<GqElement> emptyGqElementList = Collections.emptyList();
			GenEncLongCodeSharesOutput.Builder builder = new GenEncLongCodeSharesOutput.Builder()
					.setVoterChoiceReturnCodeGenerationPublicKeys(K_j_id)
					.setVoterVoteCastReturnCodeGenerationPublicKeys(Kc_j_id)
					.setExponentiatedEncryptedHashedPartialChoiceReturnCodes(c_expPCC_j_id)
					.setProofsCorrectExponentiationPartialChoiceReturnCodes(pi_expPCC_j_id)
					.setExponentiatedEncryptedHashedConfirmationKeys(c_expCK_j_id)
					.setProofsCorrectExponentiationConfirmationKeys(pi_expCK_j_id);

			builder = builder.setVoterChoiceReturnCodeGenerationPublicKeys(emptyGqElementList);
			Exception exception = assertThrows(IllegalArgumentException.class, builder::build);

			assertEquals("The Vector of Voter Choice Return Code Generation public keys must have more than zero elements.",
					Throwables.getRootCause(exception).getMessage());

			builder = builder.setVoterChoiceReturnCodeGenerationPublicKeys(K_j_id).setVoterVoteCastReturnCodeGenerationPublicKeys(emptyGqElementList);
			exception = assertThrows(IllegalArgumentException.class, builder::build);

			assertEquals("The Vector of Voter Vote Cast Return Code Generation public keys must have more than zero elements.",
					Throwables.getRootCause(exception).getMessage());

			builder = builder.setVoterVoteCastReturnCodeGenerationPublicKeys(Kc_j_id)
					.setExponentiatedEncryptedHashedPartialChoiceReturnCodes(emptyVector);
			exception = assertThrows(IllegalArgumentException.class, builder::build);

			assertEquals("The Vector of exponentiated, encrypted, hashed partial Choice Return Codes must have more than zero elements.",
					Throwables.getRootCause(exception).getMessage());

			final List<ExponentiationProof> emptyExponentiationProofList = Collections.emptyList();

			builder = builder.setExponentiatedEncryptedHashedPartialChoiceReturnCodes(c_expPCC_j_id)
					.setProofsCorrectExponentiationPartialChoiceReturnCodes(emptyExponentiationProofList);
			exception = assertThrows(IllegalArgumentException.class, builder::build);

			assertEquals("The Proofs of correct exponentiation of the partial Choice Return Codes must have more than zero elements.",
					Throwables.getRootCause(exception).getMessage());

			builder = builder.setProofsCorrectExponentiationPartialChoiceReturnCodes(pi_expPCC_j_id)
					.setExponentiatedEncryptedHashedConfirmationKeys(emptyVector);
			exception = assertThrows(IllegalArgumentException.class, builder::build);

			assertEquals("The Vector of exponentiated, encrypted, hashed Confirmation Keys must have more than zero elements.",
					Throwables.getRootCause(exception).getMessage());

			builder = builder.setExponentiatedEncryptedHashedConfirmationKeys(c_expCK_j_id)
					.setProofsCorrectExponentiationConfirmationKeys(emptyExponentiationProofList);
			exception = assertThrows(IllegalArgumentException.class, builder::build);

			assertEquals("The Proofs of correct exponentiation of the Confirmation Keys must have more than zero elements.",
					Throwables.getRootCause(exception).getMessage());
		}

		@Test
		@DisplayName("inconsistentVectorsSize throws IllegalArgumentException")
		void inconsistentVectorsSize() {
			final List<GqElement> Kc_j_id_1 = Stream.generate(generator2::getGenerator).limit(SIZE_10 + 1).collect(Collectors.toList());
			final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> c_expPCC_j_id_1 = elGamalGenerator.genRandomCiphertextVector(SIZE_10 + 1,
					ciphertextSize);
			final List<ExponentiationProof> pi_expPCC_j_id_1 =
					Stream.generate(
									() -> new ExponentiationProof(zqGroupGenerator.genRandomZqElementMember(), zqGroupGenerator.genRandomZqElementMember()))
							.limit(SIZE_10 + 1)
							.collect(Collectors.toList());

			final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> c_expCK_j_id_1 = elGamalGenerator.genRandomCiphertextVector(SIZE_10 + 1,
					ciphertextSize);
			final List<ExponentiationProof> pi_expCK_j_id_1 =
					Stream.generate(
									() -> new ExponentiationProof(zqGroupGenerator.genRandomZqElementMember(), zqGroupGenerator.genRandomZqElementMember()))
							.limit(SIZE_10 + 1)
							.collect(Collectors.toList());

			final int N_E = K_j_id.size();
			GenEncLongCodeSharesOutput.Builder builder = new GenEncLongCodeSharesOutput.Builder()
					.setVoterChoiceReturnCodeGenerationPublicKeys(K_j_id)
					.setVoterVoteCastReturnCodeGenerationPublicKeys(Kc_j_id)
					.setExponentiatedEncryptedHashedPartialChoiceReturnCodes(c_expPCC_j_id)
					.setProofsCorrectExponentiationPartialChoiceReturnCodes(pi_expPCC_j_id)
					.setExponentiatedEncryptedHashedConfirmationKeys(c_expCK_j_id)
					.setProofsCorrectExponentiationConfirmationKeys(pi_expCK_j_id);

			builder = builder.setVoterVoteCastReturnCodeGenerationPublicKeys(Kc_j_id_1);
			Exception exception = assertThrows(IllegalArgumentException.class, builder::build);

			assertEquals(String.format("The Vector of Voter Vote Cast Return Code Generation public keys is of incorrect size "
									+ "[size: expected: %s, actual: %s].",
							N_E, Kc_j_id_1.size()),
					Throwables.getRootCause(exception).getMessage());

			builder = builder.setVoterVoteCastReturnCodeGenerationPublicKeys(Kc_j_id)
					.setExponentiatedEncryptedHashedPartialChoiceReturnCodes(c_expPCC_j_id_1);
			exception = assertThrows(IllegalArgumentException.class, builder::build);

			assertEquals(String.format("The Vector of exponentiated, encrypted, hashed partial Choice Return Codes is of incorrect size "
							+ "[size: expected: %s, actual: %s].", N_E, c_expPCC_j_id_1.size()),
					Throwables.getRootCause(exception).getMessage());

			builder = builder.setExponentiatedEncryptedHashedPartialChoiceReturnCodes(c_expPCC_j_id)
					.setProofsCorrectExponentiationPartialChoiceReturnCodes(pi_expPCC_j_id_1);
			exception = assertThrows(IllegalArgumentException.class, builder::build);

			assertEquals(String.format("The Proofs of correct exponentiation of the partial Choice Return Codes is of incorrect size "
							+ "[size: expected: %s, actual: %s].", N_E, c_expPCC_j_id_1.size()),
					Throwables.getRootCause(exception).getMessage());

			builder = builder.setProofsCorrectExponentiationPartialChoiceReturnCodes(pi_expPCC_j_id)
					.setExponentiatedEncryptedHashedConfirmationKeys(c_expCK_j_id_1);
			exception = assertThrows(IllegalArgumentException.class, builder::build);

			assertEquals(String.format("The Vector of exponentiated, encrypted, hashed Confirmation Keys is of incorrect size "
							+ "[size: expected: %s, actual: %s].", N_E, c_expPCC_j_id_1.size()),
					Throwables.getRootCause(exception).getMessage());

			builder = builder.setExponentiatedEncryptedHashedConfirmationKeys(c_expCK_j_id)
					.setProofsCorrectExponentiationConfirmationKeys(pi_expCK_j_id_1);
			exception = assertThrows(IllegalArgumentException.class, builder::build);

			assertEquals(String.format("The Proofs of correct exponentiation of the Confirmation Keys is of incorrect size "
							+ "[size: expected: %s, actual: %s].", N_E, c_expPCC_j_id_1.size()),
					Throwables.getRootCause(exception).getMessage());
		}

		@Test
		@DisplayName("different group orders throws IllegalArgumentException")
		void differentGroupOrders() {
			final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> c_expPCC_j_id_q1 = elGamalGeneratorOther.genRandomCiphertextVector(SIZE_10,
					ciphertextSize);
			final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> c_expCK_j_id_q1 = elGamalGeneratorOther.genRandomCiphertextVector(SIZE_10,
					ciphertextSize);

			GenEncLongCodeSharesOutput.Builder builder = new GenEncLongCodeSharesOutput.Builder()
					.setVoterChoiceReturnCodeGenerationPublicKeys(K_j_id)
					.setVoterVoteCastReturnCodeGenerationPublicKeys(Kc_j_id)
					.setExponentiatedEncryptedHashedPartialChoiceReturnCodes(c_expPCC_j_id)
					.setProofsCorrectExponentiationPartialChoiceReturnCodes(pi_expPCC_j_id)
					.setExponentiatedEncryptedHashedConfirmationKeys(c_expCK_j_id)
					.setProofsCorrectExponentiationConfirmationKeys(pi_expCK_j_id);

			builder = builder.setExponentiatedEncryptedHashedPartialChoiceReturnCodes(c_expPCC_j_id_q1);
			Exception exception = assertThrows(IllegalArgumentException.class, builder::build);

			assertEquals("The Vector of exponentiated, encrypted, hashed partial Choice Return Codes and the Vector of exponentiated, encrypted, "
							+ "hashed Confirmation Keys do not have the same group order.",
					Throwables.getRootCause(exception).getMessage());

			builder = builder.setExponentiatedEncryptedHashedPartialChoiceReturnCodes(c_expPCC_j_id)
					.setExponentiatedEncryptedHashedConfirmationKeys(c_expCK_j_id_q1);
			exception = assertThrows(IllegalArgumentException.class, builder::build);

			assertEquals("The Vector of exponentiated, encrypted, hashed partial Choice Return Codes and the Vector of exponentiated, encrypted, "
							+ "hashed Confirmation Keys do not have the same group order.",
					Throwables.getRootCause(exception).getMessage());
		}
	}
}
