/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.tally.mixonline;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.security.SecureRandom;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;

@DisplayName("Constructing a MixDecryptInput object with")
class MixDecOnlineInputTest {

	private static final SecureRandom RANDOM = new SecureRandom();

	private ElGamalGenerator elGamalGenerator;
	private GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> partiallyDecryptedVotes;
	private GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmElectionPublicKeys;
	private ElGamalMultiRecipientPublicKey electoralBoardPublicKey;
	private ElGamalMultiRecipientPrivateKey ccmjElectionSecretKey;
	private MixDecOnlineInput.Builder mixDecOnlineInputBuilder;

	@BeforeEach
	void setup() {
		final GqGroup gqGroup = GroupTestData.getGqGroup();
		elGamalGenerator = new ElGamalGenerator(gqGroup);
		final int n = RANDOM.nextInt(20) + 1;
		final int l = RANDOM.nextInt(10) + 1;
		final int mu = RANDOM.nextInt(15) + 1;
		final int delta = RANDOM.nextInt(12) + 1;
		partiallyDecryptedVotes = elGamalGenerator.genRandomCiphertextVector(n, l);
		ccmElectionPublicKeys = Stream.generate(() -> elGamalGenerator.genRandomPublicKey(mu)).limit(4).collect(GroupVector.toGroupVector());
		electoralBoardPublicKey = elGamalGenerator.genRandomPublicKey(delta);
		ccmjElectionSecretKey = elGamalGenerator.genRandomPrivateKey(mu);
		mixDecOnlineInputBuilder = new MixDecOnlineInput.Builder()
				.setPartiallyDecryptedVotes(partiallyDecryptedVotes)
				.setCcmElectionPublicKeys(ccmElectionPublicKeys)
				.setElectoralBoardPublicKey(electoralBoardPublicKey)
				.setCcmjElectionSecretKey(ccmjElectionSecretKey);
	}

	@Test
	@DisplayName("null arguments throws a NullPointerException")
	void constructWithNullArgumentsThrows() {
		final MixDecOnlineInput.Builder nullPartiallyDecryptedVotes = mixDecOnlineInputBuilder.setPartiallyDecryptedVotes(null);
		assertThrows(NullPointerException.class, nullPartiallyDecryptedVotes::build);

		final MixDecOnlineInput.Builder nullCcmElectionPublicKeys = mixDecOnlineInputBuilder.setCcmElectionPublicKeys(null);
		assertThrows(NullPointerException.class, nullCcmElectionPublicKeys::build);

		final MixDecOnlineInput.Builder nullElectoralBoardPublicKey = mixDecOnlineInputBuilder.setElectoralBoardPublicKey(null);
		assertThrows(NullPointerException.class, nullElectoralBoardPublicKey::build);

		final MixDecOnlineInput.Builder nullCcmjElectionSecretKey = mixDecOnlineInputBuilder.setCcmjElectionSecretKey(null);
		assertThrows(NullPointerException.class, nullCcmjElectionSecretKey::build);
	}

	@Test
	@DisplayName("CCM election public keys of size different 4 throws an IllegalArgumentException")
	void constructWithCcmElectionPublicKeysBadSizeThrows() {
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> tooFewCCMElectionPublicKeys = Stream.generate(
				() -> elGamalGenerator.genRandomPublicKey(ccmElectionPublicKeys.getElementSize())).limit(3).collect(GroupVector.toGroupVector());
		final MixDecOnlineInput.Builder builderWithTooFewCCMElectionPublicKeys = mixDecOnlineInputBuilder.setCcmElectionPublicKeys(
				tooFewCCMElectionPublicKeys);
		final IllegalArgumentException tooFewException = assertThrows(IllegalArgumentException.class,
				builderWithTooFewCCMElectionPublicKeys::build);
		assertEquals("There must be exactly 4 CCM election public keys.", Throwables.getRootCause(tooFewException).getMessage());

		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> tooManyCCMElectionPublicKeys = Stream.generate(
				() -> elGamalGenerator.genRandomPublicKey(ccmElectionPublicKeys.getElementSize())).limit(5).collect(GroupVector.toGroupVector());
		final MixDecOnlineInput.Builder builderWithTooManyCCMElectionPublicKeys = mixDecOnlineInputBuilder.setCcmElectionPublicKeys(
				tooManyCCMElectionPublicKeys);
		final IllegalArgumentException tooManyException = assertThrows(IllegalArgumentException.class,
				builderWithTooManyCCMElectionPublicKeys::build);
		assertEquals("There must be exactly 4 CCM election public keys.", Throwables.getRootCause(tooManyException).getMessage());
	}

	@Test
	@DisplayName("the CCM election public keys having a different group order than the CCMj election secret key throws an IllegalArgumentException")
	void constructWithCcmElectionPublicKeysDifferentGroupOrderThanCcmjElectionSecretKeyThrows() {
		final GqGroup differentGqGroup = GroupTestData.getDifferentGqGroup(ccmElectionPublicKeys.getGroup());
		final ElGamalGenerator differentElGamalGenerator = new ElGamalGenerator(differentGqGroup);
		final ElGamalMultiRecipientPrivateKey differentCcmjElectionPrivateKey = differentElGamalGenerator.genRandomPrivateKey(
				ccmjElectionSecretKey.size());
		final MixDecOnlineInput.Builder builderWithDifferentCcmjElectionPrivateKey = mixDecOnlineInputBuilder.setCcmjElectionSecretKey(
				differentCcmjElectionPrivateKey);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builderWithDifferentCcmjElectionPrivateKey::build);
		assertEquals("The CCM election public keys must have the same group order as the CCM_j election secret key.",
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("the CCM election public keys having a different group than the electoral board public key throws an IllegalArgumentException")
	void constructWithPublicKeysDifferentGroupsThrows() {
		final GqGroup differentGqGroup = GroupTestData.getDifferentGqGroup(ccmElectionPublicKeys.getGroup());
		final ElGamalGenerator differentElGamalGenerator = new ElGamalGenerator(differentGqGroup);
		final ElGamalMultiRecipientPublicKey differentElectoralBoardPublicKey = differentElGamalGenerator.genRandomPublicKey(
				electoralBoardPublicKey.size());
		final MixDecOnlineInput.Builder builderWithDifferentElectoralBoardPublicKey = mixDecOnlineInputBuilder.setElectoralBoardPublicKey(
				differentElectoralBoardPublicKey);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builderWithDifferentElectoralBoardPublicKey::build);
		assertEquals("The CCM election public keys must have the same group as the electoral board public key.",
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("the partially decrypted votes having a different group than the public keys throws an IllegalArgumentException")
	void constructWithPartiallyDecryptedVotesDifferentGroupThanPublicKeysThrows() {
		final GqGroup differentGqGroup = GroupTestData.getDifferentGqGroup(partiallyDecryptedVotes.getGroup());
		final ElGamalGenerator differentElGamalGenerator = new ElGamalGenerator(differentGqGroup);
		final int n = partiallyDecryptedVotes.size();
		final int l = partiallyDecryptedVotes.getElementSize();
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> differentPartiallyDecryptedVotes = differentElGamalGenerator.genRandomCiphertextVector(
				n, l);
		final MixDecOnlineInput.Builder builderWithDifferentPartiallyDecryptedVotes = mixDecOnlineInputBuilder.setPartiallyDecryptedVotes(
				differentPartiallyDecryptedVotes);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builderWithDifferentPartiallyDecryptedVotes::build);
		assertEquals("The partially decrypted votes must have the same group as the public keys.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("CCM election public keys having a different size than the CCMj election secret key")
	void constructWithCcmElectionPublicKeysDifferentSizeThanCcmjElectionPrivateKey() {
		final ElGamalMultiRecipientPrivateKey differentCcmjElectionSecretKey = elGamalGenerator.genRandomPrivateKey(ccmjElectionSecretKey.size() + 1);
		final MixDecOnlineInput.Builder builderWithDifferentCcmjElectionSecretKey = mixDecOnlineInputBuilder.setCcmjElectionSecretKey(
				differentCcmjElectionSecretKey);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builderWithDifferentCcmjElectionSecretKey::build);
		assertEquals("The CCM election public keys must have the same size as the CCM_j election secret key.", exception.getMessage());
	}

	@Test
	@DisplayName("valid inputs does not throw")
	void constructWithValidInputs() {
		assertDoesNotThrow(
				() -> mixDecOnlineInputBuilder.build());
	}
}