/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.controlcomponents.configuration.setupvoting.PCCAllowListChunkRepository;
import ch.post.it.evoting.controlcomponents.tally.mixonline.BallotBoxRepository;
import ch.post.it.evoting.controlcomponents.voting.confirmvote.LongVoteCastReturnCodesShareRepository;

@Service
public class TestDatabaseCleanUpService {

	@Autowired
	private ElectionContextRepository electionContextRepository;

	@Autowired
	private SetupComponentPublicKeysRepository setupComponentPublicKeysRepository;

	@Autowired
	private CcrjReturnCodesKeysRepository ccrjReturnCodesKeysRepository;

	@Autowired
	private CcmjElectionKeysRepository ccmjElectionKeysRepository;

	@Autowired
	private VerificationCardRepository verificationCardRepository;

	@Autowired
	private BallotBoxRepository ballotBoxRepository;

	@Autowired
	private VerificationCardSetRepository verificationCardSetRepository;

	@Autowired
	private PCCAllowListChunkRepository pccAllowListChunkRepository;

	@Autowired
	private EncryptedVerifiableVoteRepository encryptedVerifiableVoteRepository;

	@Autowired
	private ElectionEventRepository electionEventRepository;

	@Autowired
	private LongVoteCastReturnCodesShareRepository longVoteCastReturnCodesShareRepository;

	public void cleanUp() {
		longVoteCastReturnCodesShareRepository.deleteAll();
		encryptedVerifiableVoteRepository.deleteAll();
		verificationCardRepository.deleteAll();
		ballotBoxRepository.deleteAll();
		pccAllowListChunkRepository.deleteAll();
		verificationCardSetRepository.deleteAll();
		ccrjReturnCodesKeysRepository.deleteAll();
		ccmjElectionKeysRepository.deleteAll();
		setupComponentPublicKeysRepository.deleteAll();
		electionContextRepository.deleteAll();
		electionEventRepository.deleteAll();
	}

}
