/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;

import ch.post.it.evoting.controlcomponents.tally.mixonline.BallotBoxService;
import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionEventContext;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.domain.SecurityLevelTestingOnly;

@SecurityLevelTestingOnly
@DisplayName("ElectionContextServiceTest")
class ElectionContextServiceTest {

	private static final ObjectMapper objectMapper = mock(ObjectMapper.class);
	private static final ElectionContextRepository electionContextRepository = mock(ElectionContextRepository.class);
	private static final ElectionEventService electionEventService = mock(ElectionEventService.class);
	private static final BallotBoxService ballotBoxService = mock(BallotBoxService.class);

	private static ElectionContextService electionContextService;
	private static ElectionEventContext electionEventContext;
	private static String electionEventId;

	@BeforeAll
	static void setUpAll() throws IOException {
		final ObjectMapper mapper = DomainObjectMapper.getNewInstance();
		final URL electionContextPayloadUrl = ElectionContextServiceTest.class.getResource(
				"/configuration/electioncontext/election-event-context-payload.json");
		final GqGroup encryptionParameters;
		final ElectionEventContextPayload electionEventContextPayload = mapper.readValue(electionContextPayloadUrl,
				ElectionEventContextPayload.class);
		electionEventContext = electionEventContextPayload.getElectionEventContext();
		encryptionParameters = electionEventContextPayload.getEncryptionGroup();
		electionEventId = electionEventContext.electionEventId();
		final ElectionEventEntity electionEventEntity = new ElectionEventEntity(electionEventId, encryptionParameters);

		final ElectionContextEntity electionContextEntity = new ElectionContextEntity.Builder()
				.setElectionEventEntity(electionEventEntity)
				.setStartTime(electionEventContext.startTime())
				.setFinishTime(electionEventContext.finishTime()).build();

		when(electionContextRepository.save(any())).thenReturn(new ElectionContextEntity());
		when(electionContextRepository.findByElectionEventId(electionEventContext.electionEventId())).thenReturn(
				Optional.ofNullable(electionContextEntity));
		when(electionEventService.getElectionEventEntity(electionEventId)).thenReturn(electionEventEntity);
		when(ballotBoxService.saveFromContexts(any())).thenReturn(Collections.emptyList());
		when(electionEventService.getEncryptionGroup(any())).thenReturn(encryptionParameters);

		electionContextService = new ElectionContextService(ballotBoxService, electionEventService, electionContextRepository);
	}

	@Test
	@DisplayName("saving with valid ElectionEventContext does not throw")
	void savingValidElectionEventContextDoesNotThrow() {
		electionContextService.save(electionEventContext);
		verify(electionContextRepository, times(1)).save(any());
	}

	@Test
	@DisplayName("loading non existing ElectionEventContext throws IllegalStateException")
	void loadNonExistingThrows() {
		final String nonExistingElectionId = "e77dbe3c70874ea584c490a0c6ac0ca4";
		final IllegalStateException exceptionCcm = assertThrows(IllegalStateException.class,
				() -> electionContextService.getElectionContextEntity(nonExistingElectionId));
		assertEquals(String.format("Election context entity not found. [electionEventId: %s]", nonExistingElectionId),
				Throwables.getRootCause(exceptionCcm).getMessage());
	}
}
