/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;

/**
 * When the KeyStoreValidator will be migrated to crypto primitives, refactor this test to use in memory keystore/password instead of files.
 */
class KeyStoreValidatorTest {

	@TempDir
	static Path tempKeystorePath;

	@Test
	void validKeystore() throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
		// given
		final String keystoreLocation = tempKeystorePath.resolve("signing_keystore_test.p12").toString();
		final String keystorePasswordLocation = tempKeystorePath.resolve("signing_pw_test.txt").toString();
		final Alias alias = Alias.CONTROL_COMPONENT_1;
		KeystoreFilesCreator.create(keystoreLocation, keystorePasswordLocation, alias.get());
		final KeyStore keyStore = KeyStore.getInstance("PKCS12");
		keyStore.load(Files.newInputStream(Paths.get(keystoreLocation)),
				new String(Files.readAllBytes(Paths.get(keystorePasswordLocation)), StandardCharsets.UTF_8).toCharArray());

		// when
		boolean isValid = KeyStoreValidator.validateKeyStore(keyStore, alias);

		// then
		assertThat(isValid).isTrue();
	}

	@Test
	void invalidKeystoreWithMissingCertificate() throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
		// given
		final String keystoreLocation = tempKeystorePath.resolve("signing_keystore_test.p12").toString();
		final String keystorePasswordLocation = tempKeystorePath.resolve("signing_pw_test.txt").toString();
		final Alias alias = Alias.CONTROL_COMPONENT_1;
		KeystoreFilesCreator.create(keystoreLocation, keystorePasswordLocation, alias.get());
		final KeyStore keyStore = KeyStore.getInstance("PKCS12");
		keyStore.load(Files.newInputStream(Paths.get(keystoreLocation)),
				new String(Files.readAllBytes(Paths.get(keystorePasswordLocation)), StandardCharsets.UTF_8).toCharArray());
		keyStore.deleteEntry(Alias.VOTING_SERVER.get());

		// when
		boolean isValid = KeyStoreValidator.validateKeyStore(keyStore, alias);

		// then
		assertThat(isValid).isFalse();
	}

	@Test
	void invalidKeystoreWithMissingPrivateKey() throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
		// given
		final String keystoreLocation = tempKeystorePath.resolve("signing_keystore_test.p12").toString();
		final String keystorePasswordLocation = tempKeystorePath.resolve("signing_pw_test.txt").toString();
		final Alias alias = Alias.CONTROL_COMPONENT_1;
		KeystoreFilesCreator.create(keystoreLocation, keystorePasswordLocation, alias.get());
		final KeyStore keyStore = KeyStore.getInstance("PKCS12");
		keyStore.load(Files.newInputStream(Paths.get(keystoreLocation)),
				new String(Files.readAllBytes(Paths.get(keystorePasswordLocation)), StandardCharsets.UTF_8).toCharArray());

		Certificate certificate = keyStore.getCertificate(alias.get());
		keyStore.deleteEntry(alias.get());
		keyStore.setCertificateEntry(alias.get(), certificate);

		// when
		boolean isValid = KeyStoreValidator.validateKeyStore(keyStore, alias);

		// then
		assertThat(isValid).isFalse();
	}

	@Test
	void unloadedKeystore() throws KeyStoreException {
		// given
		final Alias alias = Alias.CONTROL_COMPONENT_1;
		final KeyStore keyStore = KeyStore.getInstance("PKCS12");

		// when
		boolean isValid = KeyStoreValidator.validateKeyStore(keyStore, alias);

		// then
		assertThat(isValid).isFalse();
	}

	@Test
	void invalidKeystoreWithProtectedPrivateKey()
			throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException, UnrecoverableKeyException {
		// given
		final String keystoreLocation = tempKeystorePath.resolve("signing_keystore_test.p12").toString();
		final String keystorePasswordLocation = tempKeystorePath.resolve("signing_pw_test.txt").toString();
		final Alias alias = Alias.CONTROL_COMPONENT_1;
		KeystoreFilesCreator.create(keystoreLocation, keystorePasswordLocation, alias.get());
		final KeyStore keyStore = KeyStore.getInstance("PKCS12");
		keyStore.load(Files.newInputStream(Paths.get(keystoreLocation)),
				new String(Files.readAllBytes(Paths.get(keystorePasswordLocation)), StandardCharsets.UTF_8).toCharArray());

		Certificate certificate = keyStore.getCertificate(alias.get());
		Key key = keyStore.getKey(alias.get(), "".toCharArray());
		keyStore.deleteEntry(alias.get());
		keyStore.setKeyEntry(alias.get(), key, "pw".toCharArray(), new Certificate[] { certificate });

		// when
		boolean isValid = KeyStoreValidator.validateKeyStore(keyStore, alias);

		// then
		assertThat(isValid).isFalse();
	}

	@Test
	void emptyKeystore() throws KeyStoreException, CertificateException, IOException, NoSuchAlgorithmException {
		// given
		final Alias alias = Alias.CONTROL_COMPONENT_1;
		final KeyStore keyStore = KeyStore.getInstance("PKCS12");
		keyStore.load(null, "".toCharArray());

		// when
		boolean isValid = KeyStoreValidator.validateKeyStore(keyStore, alias);

		// then
		assertThat(isValid).isFalse();
	}
}