/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PCCAllowListChunkService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PCCAllowListChunkService.class);

	private final PCCAllowListChunkRepository pccAllowListChunkRepository;

	public PCCAllowListChunkService(final PCCAllowListChunkRepository pccAllowListChunkRepository) {
		this.pccAllowListChunkRepository = pccAllowListChunkRepository;
	}

	@Transactional
	public PCCAllowListChunkEntity save(final PCCAllowListChunkEntity pccAllowListChunkEntity) {
		checkNotNull(pccAllowListChunkEntity);

		final PCCAllowListChunkEntity savedEntity = pccAllowListChunkRepository.save(pccAllowListChunkEntity);
		final int chunkId = savedEntity.getChunkId();
		LOGGER.debug("Saved pcc allow list chunk entity. [chunkId: {}]", chunkId);

		return savedEntity;
	}

	/**
	 * Gets the pcc allow lists for the given verification card set id.
	 *
	 * @param verificationCardSetId the verification card set id. Must be a valid UUID.
	 * @return the pcc allow lists.
	 */
	@Transactional
	public List<PCCAllowListChunkEntity> getPCCAllowLists(final String verificationCardSetId) {
		validateUUID(verificationCardSetId);

		final List<PCCAllowListChunkEntity> pccAllowLists = pccAllowListChunkRepository.findAllByVerificationCardSetId(verificationCardSetId);
		checkState(!pccAllowLists.isEmpty(), "No pcc allow list chunk found. [verificationCardSetId: %s]", verificationCardSetId);
		LOGGER.debug("Loaded pcc allow list chunks. [verificationCardSetId: {}, size: {}]", verificationCardSetId, pccAllowLists.size());

		return pccAllowLists;
	}

}
