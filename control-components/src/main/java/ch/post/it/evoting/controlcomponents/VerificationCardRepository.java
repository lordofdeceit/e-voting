/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.MANDATORY)
public interface VerificationCardRepository extends CrudRepository<VerificationCardEntity, Long> {

	Optional<VerificationCardEntity> findByVerificationCardId(final String verificationCardId);

	boolean existsByVerificationCardId(final String verificationCardId);

	int countAllByVerificationCardSetEntity_VerificationCardSetIdAndVerificationCardSetEntity_ElectionEventEntity_ElectionEventId(
			final String verificationCardSetId, final String electionEventId);
}
