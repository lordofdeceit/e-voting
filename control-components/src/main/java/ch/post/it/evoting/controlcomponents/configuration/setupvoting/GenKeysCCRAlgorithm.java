/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.integerToString;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.SchnorrProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;

/**
 * Implements the GenKeysCCR<sub>j</sub> algorithm.
 */
@Service
public class GenKeysCCRAlgorithm {

	private static final int PHI = MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS;

	private final int nodeId;
	private final Random random;
	private final ZeroKnowledgeProof zeroKnowledgeProof;

	public GenKeysCCRAlgorithm(
			@Value("${nodeID}")
			final int nodeId,
			final Random random,
			final ZeroKnowledgeProof zeroKnowledgeProof) {
		this.nodeId = nodeId;
		this.random = random;
		this.zeroKnowledgeProof = zeroKnowledgeProof;
	}

	/**
	 * Generates the CCR<sub>j</sub> Choice Return Codes encryption key pair and the CCR<sub>j</sub> Return Codes Generation secret key.
	 *
	 * @param encryptionGroup the GqGroup in which to generate the keys.
	 * @param electionEventId the election event id for which to generate the keys.
	 * @return the CCR<sub>j</sub> Choice Return Codes encryption key pair and the CCR<sub>j</sub> Return Codes Generation secret key as a
	 * {@link GenKeysCCROutput}.
	 * @throws NullPointerException      if the provided GqGroup is null.
	 * @throws FailedValidationException if {@code electionEventId} is invalid.
	 */
	@SuppressWarnings("java:S117")
	public GenKeysCCROutput genKeysCCR(final GqGroup encryptionGroup, final String electionEventId) {
		checkNotNull(encryptionGroup);
		validateUUID(electionEventId);

		// Variables.
		final BigInteger q = encryptionGroup.getQ();
		final String ee = electionEventId;
		final int j = nodeId;

		// Operation.
		final ElGamalMultiRecipientKeyPair keyPair = ElGamalMultiRecipientKeyPair.genKeyPair(encryptionGroup, PHI, random);
		final ElGamalMultiRecipientPublicKey pk_CCR_j = keyPair.getPublicKey();
		final ElGamalMultiRecipientPrivateKey sk_CCR_j = keyPair.getPrivateKey();

		final List<String> i_aux = List.of(ee, "GenKeysCCR", integerToString(j));
		final GroupVector<SchnorrProof, ZqGroup> pi_pkCCR_j = IntStream.range(0, PHI)
				.mapToObj(i -> zeroKnowledgeProof.genSchnorrProof(sk_CCR_j.get(i), pk_CCR_j.get(i), i_aux))
				.collect(GroupVector.toGroupVector());

		final ZqElement k_prime_j = ZqElement.create(random.genRandomInteger(q), ZqGroup.sameOrderAs(encryptionGroup));

		// Output.
		return new GenKeysCCROutput(keyPair, k_prime_j, pi_pkCCR_j);
	}

	/**
	 * Holds the output of the GenKeysCCR algorithm.
	 */
	public record GenKeysCCROutput(ElGamalMultiRecipientKeyPair ccrjChoiceReturnCodesEncryptionKeyPair,
								   ZqElement ccrjReturnCodesGenerationSecretKey,
								   GroupVector<SchnorrProof, ZqGroup> ccrjSchnorrProofs) {

		public GenKeysCCROutput {
			checkNotNull(ccrjChoiceReturnCodesEncryptionKeyPair);
			checkNotNull(ccrjReturnCodesGenerationSecretKey);
			checkNotNull(ccrjSchnorrProofs);

			// Size check.
			checkArgument(ccrjChoiceReturnCodesEncryptionKeyPair.size() == PHI,
					"The ccrj Choice Return Codes encryption key pair must be of size phi. [phi: %s]", PHI);
			checkArgument(ccrjSchnorrProofs.size() == PHI, "There must be phi Schnorr proofs. [phi: %s]", PHI);

			// Cross group check.
			checkArgument(ccrjChoiceReturnCodesEncryptionKeyPair.getGroup().hasSameOrderAs(ccrjReturnCodesGenerationSecretKey.getGroup()),
					"The ccrj Return Codes generation secret key must have the same order than the ccr Choice Return Codes encryption key pair.");
			checkArgument(ccrjChoiceReturnCodesEncryptionKeyPair.getGroup().hasSameOrderAs(ccrjSchnorrProofs.getGroup()),
					"The Schnorr proofs must have the same group order as the ccr Choice Return Codes encryption key pair.");

		}

	}

}
