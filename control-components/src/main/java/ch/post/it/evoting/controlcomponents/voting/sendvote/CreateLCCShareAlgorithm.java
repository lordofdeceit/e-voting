/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.voting.sendvote;

import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.integerToByteArray;
import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.integerToString;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import ch.post.it.evoting.controlcomponents.VerificationCardSetEntity;
import ch.post.it.evoting.controlcomponents.VerificationCardSetService;
import ch.post.it.evoting.controlcomponents.VerificationCardStateService;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.cryptoprimitives.math.Base64;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.utils.KeyDerivation;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;

/**
 * Implements the CreateLCCShare<sub>j</sub> algorithm.
 */
@Service
public class CreateLCCShareAlgorithm {

	private final KeyDerivation keyDerivation;
	private final Hash hash;
	private final ZeroKnowledgeProof zeroKnowledgeProof;
	private final Base64 base64;
	private final VerificationCardSetService verificationCardSetService;
	private final VerificationCardStateService verificationCardStateService;

	public CreateLCCShareAlgorithm(
			final KeyDerivation keyDerivation,
			final Hash hash,
			final ZeroKnowledgeProof zeroKnowledgeProof,
			final Base64 base64,
			final VerificationCardSetService verificationCardSetService,
			final VerificationCardStateService verificationCardStateService) {
		this.keyDerivation = keyDerivation;
		this.hash = hash;
		this.zeroKnowledgeProof = zeroKnowledgeProof;
		this.base64 = base64;
		this.verificationCardSetService = verificationCardSetService;
		this.verificationCardStateService = verificationCardStateService;
	}

	/**
	 * Generates the long Choice Return Codes shares and proves correct exponentiation.
	 * <p>
	 * By contract the context ids are verified prior to calling this method.
	 * </p>
	 *
	 * @param context the {@link CreateLCCShareContext}. Not null.
	 * @param input   the {@link CreateLCCShareInput}. Not null.
	 * @return the hashed partial Choice Return Codes, the long Choice Return Code share, the Voter Choice Return Code Generation public key and the
	 * exponentiation proof encapsulated in a {@link CreateLCCShareOutput}.
	 * @throws NullPointerException     if any input parameter is null.
	 * @throws IllegalStateException    if the partial Choice Return Codes allow list does not contain all partial Choice Return Codes.
	 * @throws IllegalArgumentException if
	 *                                  <ul>
	 *                                      <li>The context and input do not have the same group.</li>
	 *                                      <li>The number of partial choice return codes is different from psi.</li>
	 *                                      <li>The verification card is not in L<sub>decPCC,j</sub>.</li>
	 *                                      <li>The verification card is in L<sub>sentVotes,j</sub></li>
	 *                                  </ul>
	 */
	@SuppressWarnings("java:S117")
	public CreateLCCShareOutput createLCCShare(final CreateLCCShareContext context, final CreateLCCShareInput input) {
		checkNotNull(context);
		checkNotNull(input);

		// Context.
		final GqGroup encryptionGroup = context.encryptionGroup();
		final BigInteger q = encryptionGroup.getQ();
		final GqElement g = encryptionGroup.getGenerator();
		final int j = context.nodeId();
		final String ee = context.electionEventId();
		final String vcs = context.verificationCardSetId();
		final int psi = context.numberOfSelectableVotingOptions();
		final List<String> L_pCC = context.pCCAllowList();

		// Input.
		final GroupVector<GqElement, GqGroup> pCC_id = input.partialChoiceReturnCodes();
		final ZqElement k_prime_j = input.ccrjReturnCodesGenerationSecretKey();
		final String vc_id = input.verificationCardId();
		final VerificationCardSetEntity verificationCardSet = verificationCardSetService.getVerificationCardSet(vcs);
		final CombinedCorrectnessInformation combinedCorrectnessInformation = verificationCardSet.getCombinedCorrectnessInformation();

		// Cross-checks.
		checkArgument(encryptionGroup.equals(pCC_id.getGroup()), "The context and input must have the same group.");
		checkArgument(pCC_id.size() == psi, String.format("The number of partial choice return codes must be equal to psi. [psi: %s]", psi));

		// Require.
		// all pCC distinct ensured by CreateLCCShareInput.
		// Corresponds to vc_id ∈ L_decPCC,j.
		checkArgument(verificationCardStateService.isPartiallyDecrypted(vc_id),
				"The partial Choice Return Codes have not yet been partially decrypted. [vc_id: %s].", vc_id);
		// Corresponds to vc_id ∉ L_sentVotes,j.
		checkArgument(verificationCardStateService.isNotSentVote(vc_id),
				"The CCR_j already generated the long Choice Return Code share in a previous attempt. [vc_id: %s].", vc_id);

		// Operation.
		final byte[] PRK = integerToByteArray(k_prime_j.getValue());

		final List<String> info = List.of("VoterChoiceReturnCodeGeneration", ee, vcs, vc_id);

		final ZqElement k_j_id = keyDerivation.KDFToZq(PRK, info, q);

		final GqElement K_j_id = g.exponentiate(k_j_id);

		final List<GqElement> hpCC_id_elements = new ArrayList<>();
		final List<GqElement> lCC_j_id_elements = new ArrayList<>();
		for (int i = 0; i < psi; i++) {
			final GqElement pCC_id_i = pCC_id.get(i);
			final GqElement hpCC_id_i = hash.hashAndSquare(pCC_id_i.getValue(), pCC_id_i.getGroup());

			final String ci = combinedCorrectnessInformation.getCorrectnessIdForSelectionIndex(i);

			final byte[] lpCC_id_i = hash.recursiveHash(hpCC_id_i, HashableString.from(vc_id), HashableString.from(ee), HashableString.from(ci));

			if (!L_pCC.contains(base64.base64Encode(lpCC_id_i))) {
				throw new IllegalStateException("The partial Choice Return Codes allow list does not contain the partial Choice Return Code.");
			} else {
				final GqElement lCC_j_id_i = hpCC_id_i.exponentiate(k_j_id);
				lCC_j_id_elements.add(lCC_j_id_i);
			}

			hpCC_id_elements.add(hpCC_id_i);
		}
		final GroupVector<GqElement, GqGroup> hpCC_id = GroupVector.from(hpCC_id_elements);

		final GroupVector<GqElement, GqGroup> lCC_j_id = GroupVector.from(lCC_j_id_elements);

		final List<String> i_aux = Arrays.asList(ee, vc_id, "CreateLCCShare", integerToString(j));

		final GroupVector<GqElement, GqGroup> g_hpCC_id = hpCC_id.prepend(g);
		final GroupVector<GqElement, GqGroup> K_j_id_lCC_j_id = lCC_j_id.prepend(K_j_id);
		final ExponentiationProof pi_expLCC_j_id = zeroKnowledgeProof.genExponentiationProof(g_hpCC_id, k_j_id, K_j_id_lCC_j_id, i_aux);

		// Corresponds to L_sentVotes,j ← L_sentVotes,j ∪ vc_id
		verificationCardStateService.setSentVote(vc_id);

		return new CreateLCCShareOutput(hpCC_id, lCC_j_id, K_j_id, pi_expLCC_j_id);
	}
}
