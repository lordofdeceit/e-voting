/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.tally.mixonline;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

/**
 * Regroups the context values needed by the VerifyMixDecOnline algorithm.
 *
 * <ul>
 * <li>encryptionGroup, the encryption group. Not null.</li>
 * <li>j, the node id. In range [2, 4].</li>
 * <li>ee, the election event id. Not null and valid UUID.</li>
 * <li>bb, the ballot box id. Not null and a valid UUID.</li>
 * <li>delta_hat, the number of allowed write-ins plus one. Strictly positive.</li>
 * </ul>
 */
public record VerifyMixDecOnlineContext(GqGroup encryptionGroup, int nodeId, String electionEventId, String ballotBoxId,
										int numberOfAllowedWriteInsPlusOne) {

	public VerifyMixDecOnlineContext {
		checkNotNull(encryptionGroup);
		checkArgument(NODE_IDS.contains(nodeId), "The node id must be part of the known node ids. [nodeId: %]", nodeId);
		checkArgument(nodeId != 1, "The first node must omit the VerifyMixDecOnline algorithm.");
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);
		checkArgument(numberOfAllowedWriteInsPlusOne > 0, "The number of allowed write-ins + 1 must be strictly positive.");
	}
}
