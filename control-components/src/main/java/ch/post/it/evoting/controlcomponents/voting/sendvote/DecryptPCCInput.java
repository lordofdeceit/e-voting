/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.voting.sendvote;

import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.cryptoprimitives.utils.Validations.allEqual;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.function.Function;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;

/**
 * Regroups the input values needed for the DecryptPCC<sub>j</sub> algorithm.
 *
 * <ul>
 * <li>vc<sub>id</sub>>, the verification card id. Not null and a valid UUID.</li>
 * <li>d<sub>j</sub>, CCR<sub>j</sub>'s exponentiated gamma elements. Not null.</li>
 * <li>(d<sub>j_hat_1</sub>, d<sub>j_hat_2</sub>, d<sub>j_hat_3</sub>), the other CCR's exponentiated gamma elements. Not null.</li>
 * <li>(pi<sub>decPCC, j_hat_1</sub>, pi<sub>decPCC, j_hat_2</sub>, pi<sub>decPCC, j_hat_3</sub>), the other CCR's exponentiation proofs. Not null.</li>
 * <li>(pk<sub>CCR_j_hat_1</sub>, pk<sub>CCR_j_hat_2</sub>, pk<sub>CCR_j_hat_3</sub>), the other CCR's Choice Return Codes encryption keys. Not null.</li>
 * <li>E1, the encrypted vote. Not null.</li>
 * <li>E1_tilde, the exponentiated encrypted vote. Not null.</li>
 * <li>E2, the encrypted partial Choice Return Codes. Not null.</li>
 * </ul>
 */
public class DecryptPCCInput {

	private final String verificationCardId;
	private final GroupVector<GqElement, GqGroup> exponentiatedGammaElements;
	private final GroupVector<GroupVector<GqElement, GqGroup>, GqGroup> otherCcrExponentiatedGammaElements;
	private final GroupVector<GroupVector<ExponentiationProof, ZqGroup>, ZqGroup> otherCcrExponentiationProofs;
	private final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> otherCcrChoiceReturnCodesEncryptionKeys;
	private final ElGamalMultiRecipientCiphertext encryptedVote;
	private final ElGamalMultiRecipientCiphertext exponentiatedEncryptedVote;
	private final ElGamalMultiRecipientCiphertext encryptedPartialChoiceReturnCodes;

	private DecryptPCCInput(final String verificationCardId,
			final GroupVector<GqElement, GqGroup> exponentiatedGammaElements,
			final GroupVector<GroupVector<GqElement, GqGroup>, GqGroup> otherCcrExponentiatedGammaElements,
			final GroupVector<GroupVector<ExponentiationProof, ZqGroup>, ZqGroup> otherCcrExponentiationProofs,
			final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> otherCcrChoiceReturnCodesEncryptionKeys,
			final ElGamalMultiRecipientCiphertext encryptedVote,
			final ElGamalMultiRecipientCiphertext exponentiatedEncryptedVote,
			final ElGamalMultiRecipientCiphertext encryptedPartialChoiceReturnCodes) {
		this.verificationCardId = verificationCardId;
		this.exponentiatedGammaElements = exponentiatedGammaElements;
		this.otherCcrExponentiatedGammaElements = otherCcrExponentiatedGammaElements;
		this.otherCcrExponentiationProofs = otherCcrExponentiationProofs;
		this.otherCcrChoiceReturnCodesEncryptionKeys = otherCcrChoiceReturnCodesEncryptionKeys;
		this.encryptedVote = encryptedVote;
		this.exponentiatedEncryptedVote = exponentiatedEncryptedVote;
		this.encryptedPartialChoiceReturnCodes = encryptedPartialChoiceReturnCodes;
	}

	String getVerificationCardId() {
		return verificationCardId;
	}

	GroupVector<GqElement, GqGroup> getExponentiatedGammaElements() {
		return exponentiatedGammaElements;
	}

	GroupVector<GroupVector<GqElement, GqGroup>, GqGroup> getOtherCcrExponentiatedGammaElements() {
		return otherCcrExponentiatedGammaElements;
	}

	GroupVector<GroupVector<ExponentiationProof, ZqGroup>, ZqGroup> getOtherCcrExponentiationProofs() {
		return otherCcrExponentiationProofs;
	}

	GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> getOtherCcrChoiceReturnCodesEncryptionKeys() {
		return otherCcrChoiceReturnCodesEncryptionKeys;
	}

	ElGamalMultiRecipientCiphertext getEncryptedVote() {
		return encryptedVote;
	}

	ElGamalMultiRecipientCiphertext getExponentiatedEncryptedVote() {
		return exponentiatedEncryptedVote;
	}

	ElGamalMultiRecipientCiphertext getEncryptedPartialChoiceReturnCodes() {
		return encryptedPartialChoiceReturnCodes;
	}

	/**
	 * Builder performing input validations and cross-validations before constructing a {@link DecryptPCCInput}.
	 */
	public static class Builder {

		private String verificationCardId;
		private GroupVector<GqElement, GqGroup> exponentiatedGammaElements;
		private GroupVector<GroupVector<GqElement, GqGroup>, GqGroup> otherCcrExponentiatedGammaElements;
		private GroupVector<GroupVector<ExponentiationProof, ZqGroup>, ZqGroup> otherCcrExponentiationProofs;
		private GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> otherCcrChoiceReturnCodesEncryptionKeys;
		private ElGamalMultiRecipientCiphertext encryptedVote;
		private ElGamalMultiRecipientCiphertext exponentiatedEncryptedVote;
		private ElGamalMultiRecipientCiphertext encryptedPartialChoiceReturnCodes;

		public Builder setVerificationCardId(final String verificationCardId) {
			this.verificationCardId = verificationCardId;
			return this;
		}

		public Builder setExponentiatedGammaElements(final GroupVector<GqElement, GqGroup> exponentiatedGammaElements) {
			this.exponentiatedGammaElements = exponentiatedGammaElements;
			return this;
		}

		public Builder setOtherCcrExponentiatedGammaElements(
				final GroupVector<GroupVector<GqElement, GqGroup>, GqGroup> otherCcrExponentiatedGammaElements) {
			this.otherCcrExponentiatedGammaElements = otherCcrExponentiatedGammaElements;
			return this;
		}

		public Builder setOtherCcrExponentiationProofs(
				final GroupVector<GroupVector<ExponentiationProof, ZqGroup>, ZqGroup> otherCcrExponentiationProofs) {
			this.otherCcrExponentiationProofs = otherCcrExponentiationProofs;
			return this;
		}

		public Builder setOtherCcrChoiceReturnCodesEncryptionKeys(
				final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> otherCcrChoiceReturnCodesEncryptionKeys) {
			this.otherCcrChoiceReturnCodesEncryptionKeys = otherCcrChoiceReturnCodesEncryptionKeys;
			return this;
		}

		public Builder setEncryptedVote(final ElGamalMultiRecipientCiphertext encryptedVote) {
			this.encryptedVote = encryptedVote;
			return this;
		}

		public Builder setExponentiatedEncryptedVote(final ElGamalMultiRecipientCiphertext exponentiatedEncryptedVote) {
			this.exponentiatedEncryptedVote = exponentiatedEncryptedVote;
			return this;
		}

		public Builder setEncryptedPartialChoiceReturnCodes(final ElGamalMultiRecipientCiphertext encryptedPartialChoiceReturnCodes) {
			this.encryptedPartialChoiceReturnCodes = encryptedPartialChoiceReturnCodes;
			return this;
		}

		/**
		 * Creates the DecryptPCCInput object.
		 *
		 * @throws NullPointerException      if any of the fields are null.
		 * @throws FailedValidationException if the verification card id is not a valid UUID.
		 * @throws IllegalArgumentException  if
		 *                                   <ul>
		 *                                       <li>at least one numberOfSelectableVotingOptions is different.</li>
		 *                                       <li>numberOfSelectableVotingOptions is not in the range [1, 120].</li>
		 *                                       <li>the other CCR's lists are not of size 3.</li>
		 *                                       <li>numberOfAllowedWriteInsPlusOne is not strictly positive.</li>
		 *                                       <li>at least one group is different.</li>
		 *                                       <li>numberOfAllowedWriteInsPlusOne is strictly greater than numberOfSelectableVotingOptions.</li>
		 *                                       <li>numberOfSelectableVotingOptions is strictly greater than phi.</li>
		 *                                   </ul>
		 */
		public DecryptPCCInput build() {
			validateUUID(verificationCardId);
			checkNotNull(exponentiatedGammaElements);
			checkNotNull(otherCcrExponentiatedGammaElements);
			checkNotNull(otherCcrExponentiationProofs);
			checkNotNull(otherCcrChoiceReturnCodesEncryptionKeys);
			checkNotNull(encryptedVote);
			checkNotNull(exponentiatedEncryptedVote);
			checkNotNull(encryptedPartialChoiceReturnCodes);

			// Check sizes
			final List<Integer> sizes = List.of(exponentiatedGammaElements.size(), otherCcrExponentiatedGammaElements.get(0).size(),
					otherCcrExponentiationProofs.get(0).size(), encryptedPartialChoiceReturnCodes.size());
			checkArgument(allEqual(sizes.stream(), Function.identity()),
					"The exponentiated gamma elements, the other CCR's exponentiated gamma elements, the other CCR's exponentiation proofs "
							+ "and the encrypted partial Choice Return Codes must have the same size.");
			checkArgument(!exponentiatedGammaElements.isEmpty(), "numberOfSelectableVotingOptions should be at least 1.");
			checkArgument(exponentiatedGammaElements.size() <= MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS,
					"numberOfSelectableVotingOptions should be at most %s.", MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS);

			checkArgument(otherCcrExponentiatedGammaElements.size() == 3,
					"There must be exactly 3 vectors of other CCR's exponentiated gamma elements.");
			checkArgument(otherCcrExponentiatedGammaElements.allEqual(GroupVector::size),
					"All other CCR's exponentiated gamma elements must have the same size.");

			checkArgument(otherCcrExponentiationProofs.size() == 3, "There must be exactly 3 vectors of other CCR's exponentiation proofs.");
			checkArgument(otherCcrExponentiationProofs.allEqual(GroupVector::size),
					"All other CCR's exponentiation proof vectors must have the same size.");

			checkArgument(otherCcrChoiceReturnCodesEncryptionKeys.size() == 3,
					"There must be exactly 3 vectors of other CCR's Choice Return Codes encryption keys.");
			checkArgument(otherCcrChoiceReturnCodesEncryptionKeys.allEqual(ElGamalMultiRecipientPublicKey::size),
					"All other CCR's Choice Return Codes encryption keys must have the same size.");

			checkArgument(encryptedVote.size() > 0, "The size of the encrypted vote must be at least one.");

			checkArgument(exponentiatedEncryptedVote.size() == 1, "The exponentiated encrypted votes must have exactly one phi element.");

			// Cross-group checks
			final List<GqGroup> gqGroups = List.of(
					exponentiatedGammaElements.getGroup(), otherCcrExponentiatedGammaElements.getGroup(),
					otherCcrChoiceReturnCodesEncryptionKeys.getGroup(), encryptedVote.getGroup(), exponentiatedEncryptedVote.getGroup(),
					encryptedPartialChoiceReturnCodes.getGroup());
			checkArgument(allEqual(gqGroups.stream(), Function.identity()), "All input GqGroups must be the same.");
			checkArgument(otherCcrExponentiationProofs.getGroup().hasSameOrderAs(encryptedVote.getGroup()),
					"The other CCR's exponentiation proofs' group must have the same order as the encrypted vote's group.");

			// Requires
			final int delta_hat = encryptedVote.size();
			final int psi = exponentiatedGammaElements.size();
			checkArgument(delta_hat <= psi + 1,
					"numberOfAllowedWriteInsPlusOne must be smaller or equal to numberOfSelectableVotingOptions + 1. [numberOfAllowedWriteInsPlusOne: %s, numberOfSelectableVotingOptions + 1: %s]",
					delta_hat, psi + 1);

			final int phi = otherCcrChoiceReturnCodesEncryptionKeys.getElementSize();
			checkArgument(psi <= phi,
					"numberOfSelectableVotingOptions must be smaller or equal to phi. [numberOfSelectableVotingOptions: %s, phi: %s]", psi, phi);

			return new DecryptPCCInput(verificationCardId, exponentiatedGammaElements, otherCcrExponentiatedGammaElements,
					otherCcrExponentiationProofs, otherCcrChoiceReturnCodesEncryptionKeys, encryptedVote, exponentiatedEncryptedVote,
					encryptedPartialChoiceReturnCodes);
		}
	}
}