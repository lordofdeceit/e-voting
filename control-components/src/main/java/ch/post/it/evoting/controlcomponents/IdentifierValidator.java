/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import org.springframework.stereotype.Service;

import ch.post.it.evoting.domain.ContextIds;

@Service
public class IdentifierValidator {

	private final ElectionEventService electionEventService;
	private final VerificationCardSetService verificationCardSetService;
	private final VerificationCardService verificationCardService;

	public IdentifierValidator(final ElectionEventService electionEventService, final VerificationCardSetService verificationCardSetService,
			final VerificationCardService verificationCardService) {
		this.electionEventService = electionEventService;
		this.verificationCardSetService = verificationCardSetService;
		this.verificationCardService = verificationCardService;
	}

	/**
	 * Validates that the given context ids are stored in the database
	 *
	 * @param contextIds
	 */
	public void validateContextIds(final ContextIds contextIds) {
		checkNotNull(contextIds);

		final String electionEventId = contextIds.electionEventId();
		checkArgument(electionEventService.exists(electionEventId), "ElectionEventId does not exist. [electionEventId: %s]",
				electionEventId);

		final String verificationCardSetId = contextIds.verificationCardSetId();
		checkArgument(verificationCardSetService.exists(verificationCardSetId), "VerificationCardSetId does not exist. [verificationCardSetId: %s]",
				verificationCardSetId);

		final String verificationCardId = contextIds.verificationCardId();
		checkArgument(verificationCardService.exists(verificationCardId), "VerificationCardId does not exist. [verificationCardId: %s]",
				verificationCardId);
	}
}
