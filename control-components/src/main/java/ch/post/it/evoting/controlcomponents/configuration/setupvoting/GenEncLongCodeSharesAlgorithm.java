/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.math.GroupVector.toGroupVector;
import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.integerToByteArray;
import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.integerToString;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import com.google.common.collect.Streams;

import ch.post.it.evoting.controlcomponents.VerificationCard;
import ch.post.it.evoting.controlcomponents.VerificationCardService;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.utils.KeyDerivation;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;

@Service
public class GenEncLongCodeSharesAlgorithm {

	private final KeyDerivation keyDerivation;
	private final ZeroKnowledgeProof zeroKnowledgeProof;
	private final VerificationCardService verificationCardService;

	public GenEncLongCodeSharesAlgorithm(
			final KeyDerivation keyDerivation,
			final ZeroKnowledgeProof zeroKnowledgeProof,
			final VerificationCardService verificationCardService) {
		this.keyDerivation = keyDerivation;
		this.zeroKnowledgeProof = zeroKnowledgeProof;
		this.verificationCardService = verificationCardService;
	}

	/**
	 * Generates the encrypted CCR_j long return code shares.
	 *
	 * @param context the {@link GenEncLongCodeSharesContext} containing necessary ids, keys and group. Non-null.
	 * @param input   the {@link GenEncLongCodeSharesInput} input, contains the verification card id, the encrypted hashed partial choice return codes
	 *                and the encrypted hashed confirmation key of a specific voter. Non-null.
	 * @return output the {@link GenEncLongCodeSharesOutput}, contains the output for each verification card.
	 * @throws NullPointerException     if any of the context or input are null.
	 * @throws IllegalArgumentException if any of the voting cards has already been generated.
	 */
	@SuppressWarnings("java:S117")
	public GenEncLongCodeSharesOutput genEncLongCodeShares(final GenEncLongCodeSharesContext context, final GenEncLongCodeSharesInput input) {
		checkNotNull(context);
		checkNotNull(input);

		// Context.
		final BigInteger q = context.getEncryptionGroup().getQ();
		final GqElement g = context.getEncryptionGroup().getGenerator();
		final int j = context.getNodeId();
		final String ee = context.getElectionEventId();
		final String vcs = context.getVerificationCardSetId();

		// Input.
		final ZqElement k_prime_j = input.getReturnCodesGenerationSecretKey();
		final List<String> vc = input.getVerificationCardIds();
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> c_pCC = input.getEncryptedHashedPartialChoiceReturnCodes();
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> c_ck = input.getEncryptedHashedConfirmationKeys();

		// Cross-checks.
		checkArgument(context.getEncryptionGroup().equals(input.getVerificationCardPublicKeys().getGroup()),
				"The context and input must have the same group.");

		final int n = context.getNumberOfVotingOptions();
		checkArgument(n == c_pCC.getElementSize(),
				"The encrypted hashed partial choice return codes must have n elements. [expected: %s, actual: %s]", n, c_pCC.getElementSize());

		record EncLongCodeShare(GqElement K_j_id, GqElement Kc_j_id, ElGamalMultiRecipientCiphertext c_expPCC_j_id,
								ExponentiationProof pi_expPCC_j_id,
								ElGamalMultiRecipientCiphertext c_expCK_j_id,
								ExponentiationProof pi_expCK_j_id,
								VerificationCard l_genVC_j_id) {
		}

		final int N_E = vc.size();

		// Require
		vc.forEach(vc_id -> checkArgument(!verificationCardService.exists(vc_id), "Voting cards have already been generated."));

		// Operation.
		final byte[] PRK = integerToByteArray(k_prime_j.getValue());

		final List<EncLongCodeShare> encLongCodeShares = IntStream.range(0, N_E).parallel()
				.mapToObj(id -> {
					final String vc_id = vc.get(id);

					final List<String> info = List.of("VoterChoiceReturnCodeGeneration", ee, vcs, vc_id);

					final ZqElement k_j_id = keyDerivation.KDFToZq(PRK, info, q);

					final GqElement K_j_id = g.exponentiate(k_j_id);

					final List<String> info_CK = List.of("VoterVoteCastReturnCodeGeneration", ee, vcs, vc_id);

					final ZqElement kc_j_id = keyDerivation.KDFToZq(PRK, info_CK, q);

					final GqElement Kc_j_id = g.exponentiate(kc_j_id);

					final ElGamalMultiRecipientCiphertext c_pCC_id = c_pCC.get(id);
					final ElGamalMultiRecipientCiphertext c_expPCC_j_id = c_pCC_id.getCiphertextExponentiation(k_j_id);

					final List<String> i_aux = Arrays.asList(ee, vc_id, "GenEncLongCodeShares", integerToString(j));

					final GroupVector<GqElement, GqGroup> g_c_pCC_id = Streams.concat(Stream.of(g), c_pCC_id.stream()).collect(toGroupVector());
					GroupVector<GqElement, GqGroup> k_j_id_c_expPCC_j_id = Streams.concat(Stream.of(K_j_id), c_expPCC_j_id.stream())
							.collect(toGroupVector());
					final ExponentiationProof pi_expPCC_j_id = zeroKnowledgeProof.genExponentiationProof(g_c_pCC_id, k_j_id, k_j_id_c_expPCC_j_id,
							i_aux);

					final ElGamalMultiRecipientCiphertext c_ck_id = c_ck.get(id);
					final ElGamalMultiRecipientCiphertext c_expCK_j_id = c_ck_id.getCiphertextExponentiation(kc_j_id);

					final GroupVector<GqElement, GqGroup> g_c_ck_id = Streams.concat(Stream.of(g), c_ck_id.stream()).collect(toGroupVector());
					final GroupVector<GqElement, GqGroup> Kc_j_id_c_expCK_j_id = Streams.concat(Stream.of(Kc_j_id), c_expCK_j_id.stream())
							.collect(toGroupVector());
					final ExponentiationProof pi_expCK_j_id = zeroKnowledgeProof.genExponentiationProof(g_c_ck_id, kc_j_id, Kc_j_id_c_expCK_j_id,
							i_aux);

					// Create verification card for vc_id.
					final ElGamalMultiRecipientPublicKey verificationCardPublicKey = input.getVerificationCardPublicKeys().get(id);
					final VerificationCard verificationCard = new VerificationCard(vc_id, vcs, verificationCardPublicKey);

					return new EncLongCodeShare(K_j_id, Kc_j_id, c_expPCC_j_id, pi_expPCC_j_id, c_expCK_j_id, pi_expCK_j_id, verificationCard);
				})
				.toList();

		// Save all verification cards. Equivalent to performing L_genVC,j ← L_genVC,j ∪ vc_id for all id.
		verificationCardService.saveAll(encLongCodeShares.stream().map(EncLongCodeShare::l_genVC_j_id).toList());

		return new GenEncLongCodeSharesOutput.Builder()
				.setVoterChoiceReturnCodeGenerationPublicKeys(encLongCodeShares.stream().map(EncLongCodeShare::K_j_id).toList())
				.setVoterVoteCastReturnCodeGenerationPublicKeys(encLongCodeShares.stream().map(EncLongCodeShare::Kc_j_id).toList())
				.setExponentiatedEncryptedHashedPartialChoiceReturnCodes(encLongCodeShares.stream()
						.map(EncLongCodeShare::c_expPCC_j_id).collect(toGroupVector()))
				.setProofsCorrectExponentiationPartialChoiceReturnCodes(encLongCodeShares.stream().map(EncLongCodeShare::pi_expPCC_j_id).toList())
				.setExponentiatedEncryptedHashedConfirmationKeys(encLongCodeShares.stream()
						.map(EncLongCodeShare::c_expCK_j_id).collect(toGroupVector()))
				.setProofsCorrectExponentiationConfirmationKeys(encLongCodeShares.stream().map(EncLongCodeShare::pi_expCK_j_id).toList())
				.build();
	}

}
