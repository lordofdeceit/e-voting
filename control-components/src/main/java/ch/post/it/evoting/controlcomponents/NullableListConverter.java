/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents;

import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import javax.persistence.AttributeConverter;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SuppressWarnings("java:S1168") // The desired behavior of this converter is exactly to handle null values in the database.
@Component
public class NullableListConverter implements AttributeConverter<List<String>, byte[]> {

	final ObjectMapper objectMapper;

	public NullableListConverter(final ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	@Override
	public byte[] convertToDatabaseColumn(final List<String> list) {
		if (list == null) {
			return null;
		}

		try {
			return objectMapper.writeValueAsBytes(list);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(e);
		}
	}

	@Override
	public List<String> convertToEntityAttribute(final byte[] list) {
		if (list == null) {
			return null;
		}

		try {
			return Arrays.asList(objectMapper.readValue(new String(list, StandardCharsets.UTF_8), String[].class));
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(e);
		}
	}
}
