/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.controlcomponents.configuration.setupvoting.PCCAllowListChunkEntity;
import ch.post.it.evoting.controlcomponents.configuration.setupvoting.PCCAllowListChunkService;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;

@Service
public class VerificationCardSetService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VerificationCardSetService.class);

	private final ElectionEventService electionEventService;
	private final PCCAllowListChunkService pccAllowListChunkService;
	private final VerificationCardSetRepository verificationCardSetRepository;

	public VerificationCardSetService(
			final ElectionEventService electionEventService,
			final PCCAllowListChunkService pccAllowListChunkService,
			final VerificationCardSetRepository verificationCardSetRepository) {
		this.electionEventService = electionEventService;
		this.pccAllowListChunkService = pccAllowListChunkService;
		this.verificationCardSetRepository = verificationCardSetRepository;
	}

	@Transactional
	public VerificationCardSetEntity save(final VerificationCardSetEntity verificationCardSetEntity) {
		checkNotNull(verificationCardSetEntity);

		return verificationCardSetRepository.save(verificationCardSetEntity);
	}

	@Transactional
	public boolean exists(final String verificationCardSetId) {
		validateUUID(verificationCardSetId);

		return verificationCardSetRepository.existsByVerificationCardSetId(verificationCardSetId);
	}

	@Transactional
	public int numberOfVotingCardSets(final String electionEventId) {
		validateUUID(electionEventId);

		return verificationCardSetRepository.countAllByElectionEventEntity_ElectionEventId(electionEventId);
	}

	@Transactional
	public VerificationCardSetEntity getVerificationCardSet(final String verificationCardSetId) {
		validateUUID(verificationCardSetId);

		return verificationCardSetRepository.findByVerificationCardSetId(verificationCardSetId)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Verification card set not found. [verificationCardSetId: %s]", verificationCardSetId)));
	}

	@Transactional
	public VerificationCardSetEntity getOrCreateVerificationCardSet(final String electionEventId, final String verificationCardSetId,
			final CombinedCorrectnessInformation combinedCorrectnessInformation) {

		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkNotNull(combinedCorrectnessInformation);

		if (exists(verificationCardSetId)) {
			LOGGER.debug("Found existing verification card set. [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
					verificationCardSetId);
			return getVerificationCardSet(verificationCardSetId);
		} else {
			// Create a new verification card set.
			final ElectionEventEntity electionEventEntity = electionEventService.getElectionEventEntity(electionEventId);
			final VerificationCardSetEntity newVerificationCardSetEntity = new VerificationCardSetEntity(verificationCardSetId, electionEventEntity,
					combinedCorrectnessInformation);
			final VerificationCardSetEntity savedVerificationCardSetEntity = save(newVerificationCardSetEntity);
			LOGGER.debug("New verification card set saved. [electionEventId: {}, verificationCardSetId: {}]", electionEventId, verificationCardSetId);

			return savedVerificationCardSetEntity;
		}
	}

	/**
	 * Gets the partial Choice Return Codes allow list for the given verification card set id.
	 * <p>
	 * WARNING: This will not return the complete allow list if called before all chunks have been processed and saved.
	 *
	 * @param verificationCardSetId the verification card set id. Must be a valid UUID.
	 * @return the partial Choice Return Codes allow list.
	 */
	@Transactional
	public List<String> getPartialChoiceReturnCodesAllowList(final String verificationCardSetId) {
		validateUUID(verificationCardSetId);

		final List<PCCAllowListChunkEntity> chunksByVerificationCardId = pccAllowListChunkService.getPCCAllowLists(verificationCardSetId);

		return chunksByVerificationCardId.stream()
				.map(PCCAllowListChunkEntity::getPartialChoiceReturnCodesAllowList)
				.flatMap(Collection::stream)
				.toList();
	}

	/**
	 * Sets the given long vote cast return codes allow list into the verification card set corresponding to the given verification card set id.
	 *
	 * @param verificationCardSetId            the verification card set id. Must be non-null and a valid UUID.
	 * @param longVoteCastReturnCodesAllowList the long vote cast return codes allow list. Must be non-null.
	 */
	@Transactional
	public void setLongVoteCastReturnCodesAllowList(final String verificationCardSetId, final List<String> longVoteCastReturnCodesAllowList) {
		LOGGER.info("Updating verification card set with long vote cast return codes allow list... [verificationCardSetId: {}]",
				verificationCardSetId);

		validateUUID(verificationCardSetId);
		final List<String> longVoteCastReturnCodesAllowListImmutableCopy = List.copyOf(checkNotNull(longVoteCastReturnCodesAllowList));

		final VerificationCardSetEntity verificationCardSetEntity =
				verificationCardSetRepository.findByVerificationCardSetId(verificationCardSetId)
						.orElseThrow(() -> new IllegalStateException(
								String.format("Could not find any matching verification card set [verificationCardSetId: %s]",
										verificationCardSetId)));

		verificationCardSetEntity.setLongVoteCastReturnCodesAllowList(longVoteCastReturnCodesAllowListImmutableCopy);

		LOGGER.info("Successfully updated verification card set with long vote cast return codes allow list. [verificationCardSetId: {}]",
				verificationCardSetId);
	}
}
