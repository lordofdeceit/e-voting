/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.configuration.electioncontext;

import static ch.post.it.evoting.controlcomponents.ControlComponentsApplicationBootstrap.RABBITMQ_EXCHANGE;
import static ch.post.it.evoting.domain.SharedQueue.ELECTION_CONTEXT_REQUEST_PATTERN;
import static ch.post.it.evoting.domain.SharedQueue.ELECTION_CONTEXT_RESPONSE_PATTERN;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.SignatureException;
import java.time.LocalDateTime;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.controlcomponents.ElectionContextService;
import ch.post.it.evoting.controlcomponents.ExactlyOnceCommand;
import ch.post.it.evoting.controlcomponents.ExactlyOnceCommandExecutor;
import ch.post.it.evoting.controlcomponents.Messages;
import ch.post.it.evoting.controlcomponents.VerificationCardService;
import ch.post.it.evoting.controlcomponents.VerificationCardSetService;
import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionEventContext;
import ch.post.it.evoting.cryptoprimitives.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.ChannelSecurityContextData;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.configuration.ElectionContextResponsePayload;

/**
 * Consumes the messages asking for the Election Event Context.
 */
@Service
public class ElectionContextProcessor {

	public static final Logger LOGGER = LoggerFactory.getLogger(ElectionContextProcessor.class);

	private final ObjectMapper objectMapper;
	private final RabbitTemplate rabbitTemplate;
	private final ElectionContextService electionContextService;
	private final VerificationCardSetService verificationCardSetService;
	private final VerificationCardService verificationCardService;
	private final ExactlyOnceCommandExecutor exactlyOnceCommandExecutor;
	private final SignatureKeystore<Alias> signatureKeystoreService;

	private String responseQueue;

	@Value("${nodeID}")
	private int nodeId;

	public ElectionContextProcessor(final ObjectMapper objectMapper,
			final RabbitTemplate rabbitTemplate,
			final ElectionContextService electionContextService,
			final VerificationCardSetService verificationCardSetService,
			final VerificationCardService verificationCardService,
			final ExactlyOnceCommandExecutor exactlyOnceCommandExecutor,
			final SignatureKeystore<Alias> signatureKeystoreService) {

		this.objectMapper = objectMapper;
		this.rabbitTemplate = rabbitTemplate;
		this.electionContextService = electionContextService;
		this.verificationCardSetService = verificationCardSetService;
		this.verificationCardService = verificationCardService;
		this.exactlyOnceCommandExecutor = exactlyOnceCommandExecutor;
		this.signatureKeystoreService = signatureKeystoreService;
	}

	@PostConstruct
	public void initQueue() {
		responseQueue = String.format("%s%s", ELECTION_CONTEXT_RESPONSE_PATTERN, nodeId);
	}

	@RabbitListener(queues = ELECTION_CONTEXT_REQUEST_PATTERN + "${nodeID}", autoStartup = "false")
	public void onMessage(final Message message) throws IOException {
		final String correlationId = message.getMessageProperties().getCorrelationId();
		checkNotNull(correlationId, "Correlation Id must not be null.");

		final byte[] messageBytes = message.getBody();
		final ElectionEventContextPayload electionEventContextPayload = objectMapper.readValue(messageBytes, ElectionEventContextPayload.class);
		verifyPayloadSignature(electionEventContextPayload);

		final ElectionEventContext electionEventContext = electionEventContextPayload.getElectionEventContext();
		verifyElectionEventContext(electionEventContext);

		final String contextId = electionEventContext.electionEventId();
		LOGGER.info("Received election event context request. [contextId: {}, correlationId: {}]", contextId, correlationId);

		final ExactlyOnceCommand exactlyOnceCommand = new ExactlyOnceCommand.Builder()
				.setCorrelationId(correlationId)
				.setContextId(contextId)
				.setContext(Context.CONFIGURATION_ELECTION_CONTEXT.toString())
				.setTask(() -> createElectionContextResponse(electionEventContext)).setRequestContent(messageBytes)
				.build();
		final byte[] payloadBytes = exactlyOnceCommandExecutor.process(exactlyOnceCommand);

		final Message responseMessage = Messages.createMessage(correlationId, payloadBytes);

		rabbitTemplate.send(RABBITMQ_EXCHANGE, responseQueue, responseMessage);
		LOGGER.info("Election event context response sent. [contextId: {}]", contextId);
	}

	private void verifyPayloadSignature(final ElectionEventContextPayload electionEventContextPayload) {
		final String electionEventId = electionEventContextPayload.getElectionEventContext().electionEventId();

		final CryptoPrimitivesSignature signature = electionEventContextPayload.getSignature();

		checkState(signature != null, "The signature of the election event context payload is null. [electionEventId: %s]", electionEventId);

		final Hashable additionalContextData = ChannelSecurityContextData.electionEventContext(electionEventId);

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreService.verifySignature(Alias.SDM_CONFIG, electionEventContextPayload,
					additionalContextData, signature.signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not verify the signature of the election event context payload. [electionEventId: %s]", electionEventId));
		}

		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(ElectionEventContextPayload.class,
					String.format("[electionEventId: %s]", electionEventId));
		}
	}

	private void verifyElectionEventContext(final ElectionEventContext electionEventContext) {
		final String electionEventId = electionEventContext.electionEventId();
		final List<VerificationCardSetContext> verificationCardSetContexts = electionEventContext.verificationCardSetContexts();

		// The foreign key relation in the database ensures every verificationCardSetId in BALLOT_BOX has a matching entry in VERIFICATION_CARD_SET.
		// Checking the number of verificationCardSetIds is needed.
		final int numberOfVerificationCardSetIds = verificationCardSetContexts.size();
		final int savedNumberOfVerificationCardSetIds = verificationCardSetService.numberOfVotingCardSets(electionEventId);
		checkArgument(numberOfVerificationCardSetIds == savedNumberOfVerificationCardSetIds,
				"The number of verification card set IDs should be equal to the one saved for the given election event ID. "
						+ "[electionEventId: %s, numberOfVerificationCardSetIds: %s savedNumberOfVerificationCardSetIds: %s]", electionEventId,
				numberOfVerificationCardSetIds, savedNumberOfVerificationCardSetIds);

		verificationCardSetContexts.forEach(context -> {
			final String verificationCardSetId = context.verificationCardSetId();
			final int numberOfVerificationCardIds = context.numberOfVotingCards();
			final int savedNumberOfVerificationCardIds = verificationCardService.numberOfVotingCards(electionEventId, verificationCardSetId);
			checkArgument(numberOfVerificationCardIds == savedNumberOfVerificationCardIds,
					"The number of verification card IDs should be equal to the one saved for the given election event and verification card set ID. "
							+ "[electionEventId: %s, verificationCardSetId: %s, numberOfVerificationCardIds: %s, savedNumberOfVerificationCardIds: %s]",
					electionEventId, verificationCardSetId, numberOfVerificationCardIds, savedNumberOfVerificationCardIds);
		});

		checkArgument(electionEventContext.finishTime().isAfter(LocalDateTime.now()),
				"The election event period should not be finished yet. [electionEventId: %s]", electionEventId);
	}

	private byte[] createElectionContextResponse(final ElectionEventContext electionEventContext) {
		final String electionEventId = electionEventContext.electionEventId();

		electionContextService.save(electionEventContext);

		LOGGER.info("Saved election event context. [electionEventId: {}]", electionEventId);

		final ElectionContextResponsePayload electionContextResponsePayload = new ElectionContextResponsePayload(nodeId, electionEventId);

		try {
			return objectMapper.writeValueAsBytes(electionContextResponsePayload);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(
					String.format("Could not serialize election event context response payload. [electionEventId: %s]", electionEventId), e);
		}
	}

}
