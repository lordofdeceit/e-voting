/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.voting.confirmvote;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateBase64Encoded;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ch.post.it.evoting.cryptoprimitives.domain.validations.Validations;

/**
 * Regroups the inputs needed by the VerifyLVCCHash<sub>j</sub> algorithm.
 *
 * <ul>
 * <li>L<sub>lVCC</sub>, the long Vote Case Return Codes allow list. Not null and not empty.</li>
 * <li>hlVCC<sub>id,j</sub>, the CCRj's hashed long Vote Cast Return Code share. Not null.</li>
 * <li>(hlVCC<sub>id, j_hat_1</sub>, hlVCC<sub>id, j_hat_2</sub>, hlVCC<sub>id, j_hat_3</sub>), the other CCRj's hashed long Vote Cast Return Code shares. Not null.</li>
 * <li>vc_id, the verification card id. Not null and a valid UUID.</li>
 * </ul>
 */
public class VerifyLVCCHashInput {

	private final List<String> longVoteCastReturnCodesAllowList;
	private final String ccrjHashedLongVoteCastReturnCode;
	private final List<String> otherCCRsHashedLongVoteCastReturnCodes;
	private final String verificationCardId;

	private VerifyLVCCHashInput(
			final List<String> longVoteCastReturnCodesAllowList,
			final String ccrjHashedLongVoteCastReturnCode,
			final List<String> otherCCRsHashedLongVoteCastReturnCodes,
			final String verificationCardId) {
		this.longVoteCastReturnCodesAllowList = longVoteCastReturnCodesAllowList;
		this.ccrjHashedLongVoteCastReturnCode = ccrjHashedLongVoteCastReturnCode;
		this.otherCCRsHashedLongVoteCastReturnCodes = otherCCRsHashedLongVoteCastReturnCodes;
		this.verificationCardId = verificationCardId;
	}

	List<String> getLongVoteCastReturnCodesAllowList() {
		return List.copyOf(longVoteCastReturnCodesAllowList);
	}

	String getCcrjHashedLongVoteCastReturnCode() {
		return ccrjHashedLongVoteCastReturnCode;
	}

	List<String> getOtherCCRsHashedLongVoteCastReturnCodes() {
		return List.copyOf(otherCCRsHashedLongVoteCastReturnCodes);
	}

	String getVerificationCardId() {
		return verificationCardId;
	}

	public static class Builder {
		private List<String> longVoteCastReturnCodesAllowList;
		private String ccrjHashedLongVoteCastReturnCode;
		private List<String> otherCCRsHashedLongVoteCastReturnCodes;
		private String verificationCardId;

		public Builder setLongVoteCastReturnCodesAllowList(final List<String> longVoteCastReturnCodesAllowList) {
			this.longVoteCastReturnCodesAllowList = List.copyOf(checkNotNull(longVoteCastReturnCodesAllowList));
			return this;
		}

		public Builder setCcrjHashedLongVoteCastReturnCode(final String ccrjHashedLongVoteCastReturnCode) {
			this.ccrjHashedLongVoteCastReturnCode = ccrjHashedLongVoteCastReturnCode;
			return this;
		}

		public Builder setOtherCCRsHashedLongVoteCastReturnCodes(final List<String> otherCCRsHashedLongVoteCastReturnCodes) {
			this.otherCCRsHashedLongVoteCastReturnCodes = List.copyOf(checkNotNull(otherCCRsHashedLongVoteCastReturnCodes));
			return this;
		}

		public Builder setVerificationCardId(final String verificationCardId) {
			this.verificationCardId = verificationCardId;
			return this;
		}

		public VerifyLVCCHashInput build() {
			checkNotNull(longVoteCastReturnCodesAllowList);
			checkNotNull(ccrjHashedLongVoteCastReturnCode);
			checkNotNull(otherCCRsHashedLongVoteCastReturnCodes);
			validateUUID(verificationCardId);

			// Size checks.
			checkArgument(!this.longVoteCastReturnCodesAllowList.isEmpty(), "The Long Vote Cast Return codes allow list cannot be empty.");
			checkArgument(this.otherCCRsHashedLongVoteCastReturnCodes.size() == NODE_IDS.size() - 1,
					"There number of other CCRs hashed long Vote Cast Return Codes must be equal to the number of known node ids - 1.");

			// Cross-size checks.
			checkArgument(longVoteCastReturnCodesAllowList.stream().map(String::length).collect(Collectors.toSet()).size() == 1,
					"The length of all long Vote Cast Return Codes allow list entries must be equal.");
			checkArgument(otherCCRsHashedLongVoteCastReturnCodes.stream().map(String::length).collect(Collectors.toSet()).size() == 1,
					"The length of all the other CCRj hashed long Vote Cast Return Code shares must be equal.");
			checkArgument(Stream.of(longVoteCastReturnCodesAllowList.get(0).length(), ccrjHashedLongVoteCastReturnCode.length(),
							otherCCRsHashedLongVoteCastReturnCodes.get(0).length()).collect(Collectors.toSet()).size() == 1,
					"The length of each long Vote Cast Return Codes must be equal to the length of each long Vote Cast Return Code share.");

			// Base64 checks.
			this.longVoteCastReturnCodesAllowList.forEach(Validations::validateBase64Encoded);
			validateBase64Encoded(this.ccrjHashedLongVoteCastReturnCode);
			this.otherCCRsHashedLongVoteCastReturnCodes.forEach(Validations::validateBase64Encoded);

			return new VerifyLVCCHashInput(longVoteCastReturnCodesAllowList, ccrjHashedLongVoteCastReturnCode, otherCCRsHashedLongVoteCastReturnCodes,
					verificationCardId);
		}
	}

}
