/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.configuration.setupvoting;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface PCCAllowListChunkRepository extends CrudRepository<PCCAllowListChunkEntity, Long> {

	@Query("select e from PCCAllowListChunkEntity e where e.verificationCardSetEntity.verificationCardSetId = ?1")
	List<PCCAllowListChunkEntity> findAllByVerificationCardSetId(final String verificationCardSetId);

}
