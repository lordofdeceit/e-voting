/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.configuration.setupvoting;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

import com.google.common.base.Preconditions;

import ch.post.it.evoting.controlcomponents.AllowListConverter;
import ch.post.it.evoting.controlcomponents.VerificationCardSetEntity;

@Entity
@Table(name = "PCC_ALLOW_LIST_CHUNK")
public class PCCAllowListChunkEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PCC_ALLOW_LIST_CHUNK_SEQ_GENERATOR")
	@SequenceGenerator(sequenceName = "PCC_ALLOW_LIST_CHUNK_SEQ", allocationSize = 1, name = "PCC_ALLOW_LIST_CHUNK_SEQ_GENERATOR")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "VERIFICATION_CARD_SET_FK_ID", referencedColumnName = "ID")
	private VerificationCardSetEntity verificationCardSetEntity;

	@Convert(converter = AllowListConverter.class)
	private List<String> partialChoiceReturnCodesAllowList;

	private int chunkId;

	@Version
	private Integer changeControlId;

	public PCCAllowListChunkEntity() {
	}

	public PCCAllowListChunkEntity(final VerificationCardSetEntity verificationCardSetEntity, final List<String> partialChoiceReturnCodesAllowList,
			final int chunkId) {
		this.verificationCardSetEntity = checkNotNull(verificationCardSetEntity);
		this.partialChoiceReturnCodesAllowList = checkNotNull(partialChoiceReturnCodesAllowList).stream()
				.map(Preconditions::checkNotNull)
				.toList();

		checkArgument(chunkId >= 0);
		this.chunkId = chunkId;
	}

	public VerificationCardSetEntity getVerificationCardSetEntity() {
		return verificationCardSetEntity;
	}

	public List<String> getPartialChoiceReturnCodesAllowList() {
		return List.copyOf(partialChoiceReturnCodesAllowList);
	}

	public int getChunkId() {
		return chunkId;
	}

}
