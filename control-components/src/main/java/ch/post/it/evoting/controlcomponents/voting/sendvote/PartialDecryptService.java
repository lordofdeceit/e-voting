/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.voting.sendvote;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.time.LocalDateTime;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.common.annotations.VisibleForTesting;

import ch.post.it.evoting.controlcomponents.CcrjReturnCodesKeysService;
import ch.post.it.evoting.controlcomponents.ElectionContextEntity;
import ch.post.it.evoting.controlcomponents.ElectionContextService;
import ch.post.it.evoting.controlcomponents.ElectionEventService;
import ch.post.it.evoting.controlcomponents.IdentifierValidator;
import ch.post.it.evoting.controlcomponents.SetupComponentPublicKeysService;
import ch.post.it.evoting.controlcomponents.VerificationCardService;
import ch.post.it.evoting.controlcomponents.VerificationCardSetEntity;
import ch.post.it.evoting.controlcomponents.tally.mixonline.BallotBoxEntity;
import ch.post.it.evoting.controlcomponents.tally.mixonline.BallotBoxService;
import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.domain.ContextIds;
import ch.post.it.evoting.domain.EncryptedVerifiableVote;
import ch.post.it.evoting.domain.voting.sendvote.PartiallyDecryptedEncryptedPCC;

/**
 * Verifies the encrypted vote's zero-knowledge proofs and partially decrypts the partial Choice Return Codes.
 */
@Service
public class PartialDecryptService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PartialDecryptService.class);

	private final VerifyBallotCCRAlgorithm verifyBallotCCRAlgorithm;
	private final VerificationCardService verificationCardService;
	private final IdentifierValidator identifierValidator;
	private final PartialDecryptPCCAlgorithm partialDecryptPCCAlgorithm;
	private final ElectionContextService electionContextService;
	private final ElectionEventService electionEventService;
	private final SetupComponentPublicKeysService setupComponentPublicKeysService;
	private final CcrjReturnCodesKeysService ccrjReturnCodesKeysService;
	private final BallotBoxService ballotBoxService;

	@Value("${nodeID}")
	private int nodeId;

	public PartialDecryptService(
			final VerifyBallotCCRAlgorithm verifyBallotCCRAlgorithm,
			final VerificationCardService verificationCardService,
			final IdentifierValidator identifierValidator,
			final PartialDecryptPCCAlgorithm partialDecryptPCCAlgorithm,
			final ElectionContextService electionContextService,
			final ElectionEventService electionEventService,
			final SetupComponentPublicKeysService setupComponentPublicKeysService,
			final CcrjReturnCodesKeysService ccrjReturnCodesKeysService,
			final BallotBoxService ballotBoxService) {
		this.verifyBallotCCRAlgorithm = verifyBallotCCRAlgorithm;
		this.verificationCardService = verificationCardService;
		this.identifierValidator = identifierValidator;
		this.partialDecryptPCCAlgorithm = partialDecryptPCCAlgorithm;
		this.electionContextService = electionContextService;
		this.electionEventService = electionEventService;
		this.setupComponentPublicKeysService = setupComponentPublicKeysService;
		this.ccrjReturnCodesKeysService = ccrjReturnCodesKeysService;
		this.ballotBoxService = ballotBoxService;
	}

	/**
	 * Verifies the {@link EncryptedVerifiableVote} in the VerifyBallotCCR_j algorithm and then partially decrypts the encrypted partial Choice Return
	 * Codes with the CCR_j Choice Return Codes encryption secret key in the PartialDecryptPCC_j algorithm.
	 *
	 * @param encryptedVerifiableVote the object containing the encrypted vote and the corresponding zero-knowledge proofs.
	 * @return the partially decrypted encrypted Partial Choice Return Codes as a {@link PartiallyDecryptedEncryptedPCC}.
	 */
	PartiallyDecryptedEncryptedPCC performPartialDecrypt(final EncryptedVerifiableVote encryptedVerifiableVote) {

		checkNotNull(encryptedVerifiableVote);

		final ContextIds contextIds = encryptedVerifiableVote.contextIds();

		identifierValidator.validateContextIds(contextIds);

		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();

		final VerificationCardSetEntity verificationCardSetEntity = verificationCardService.getVerificationCardEntity(verificationCardId)
				.getVerificationCardSetEntity();
		final BallotBoxEntity ballotBox = ballotBoxService.getBallotBox(verificationCardSetEntity);
		final GqGroup encryptionGroup = electionEventService.getEncryptionGroup(electionEventId);

		validateVoteIsAllowed(electionEventId, verificationCardId, LocalDateTime::now, ballotBox);

		LOGGER.debug("Starting partial decryption of partial Choice Return Codes. [contextIds: {}]", contextIds);

		// Verify the encrypted vote's zero-knowledge proofs.
		final int numberOfSelectableVotingOptions = verificationCardSetEntity.getCombinedCorrectnessInformation().getTotalNumberOfSelections();
		final int numberOfAllowedWriteInsPlusOne = ballotBox.getNumberOfWriteInsPlusOne();
		final PrimesMappingTable primesMappingTable = ballotBoxService.getBallotBoxPrimesMappingTable(verificationCardSetEntity);
		final VerifyBallotCCRContext verifyBallotCCRContext = new VerifyBallotCCRContext(encryptionGroup, nodeId, electionEventId,
				verificationCardSetId, numberOfSelectableVotingOptions, numberOfAllowedWriteInsPlusOne, primesMappingTable);

		final GqElement verificationCardPublicKey = verificationCardService.getVerificationCard(verificationCardId).verificationCardPublicKey()
				.get(0);

		final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey = setupComponentPublicKeysService.getChoiceReturnCodesEncryptionPublicKey(
				electionEventId);

		final ElGamalMultiRecipientPublicKey electionPublicKey = setupComponentPublicKeysService.getElectionPublicKey(electionEventId);

		final VerifyBallotCCRInput input = new VerifyBallotCCRInput.Builder()
				.setVerificationCardId(verificationCardId)
				.setEncryptedVote(encryptedVerifiableVote.encryptedVote())
				.setExponentiatedEncryptedVote(encryptedVerifiableVote.exponentiatedEncryptedVote())
				.setEncryptedPartialChoiceReturnCodes(encryptedVerifiableVote.encryptedPartialChoiceReturnCodes())
				.setVerificationCardPublicKey(verificationCardPublicKey)
				.setElectionPublicKey(electionPublicKey)
				.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
				.setExponentiationProof(encryptedVerifiableVote.exponentiationProof())
				.setPlaintextEqualityProof(encryptedVerifiableVote.plaintextEqualityProof())
				.build();

		if (!verifyBallotCCRAlgorithm.verifyBallotCCR(verifyBallotCCRContext, input)) {
			LOGGER.error("The client's encrypted vote zero-knowledge proofs are invalid. [contextIds: {}]", contextIds);
			throw new IllegalStateException("The client's encrypted vote zero-knowledge proofs are invalid.");
		}

		LOGGER.debug("The client's encrypted vote zero-knowledge proofs are valid. [contextIds: {}]", contextIds);

		final ElGamalMultiRecipientKeyPair ccrjChoiceReturnCodesEncryptionKeyPair = ccrjReturnCodesKeysService
				.getCcrjReturnCodesKeys(electionEventId).ccrjChoiceReturnCodesEncryptionKeyPair();

		// Perform partial decryption of the encrypted partial Choice Return codes.
		final DecryptPCCContext decryptPCCContext = new DecryptPCCContext.Builder()
				.setEncryptionGroup(encryptionGroup)
				.setNodeId(nodeId)
				.setElectionEventId(electionEventId)
				.setVerificationCardSetId(verificationCardSetId)
				.setNumberOfSelectableVotingOptions(numberOfSelectableVotingOptions)
				.setNumberOfAllowedWriteInsPlusOne(numberOfAllowedWriteInsPlusOne)
				.build();

		final PartialDecryptPCCInput partialDecryptPCCInput = new PartialDecryptPCCInput.Builder()
				.setVerificationCardId(verificationCardId)
				.setEncryptedVote(encryptedVerifiableVote.encryptedVote())
				.setExponentiatedEncryptedVote(encryptedVerifiableVote.exponentiatedEncryptedVote())
				.setEncryptedPartialChoiceReturnCodes(encryptedVerifiableVote.encryptedPartialChoiceReturnCodes())
				.setCcrjChoiceReturnCodesEncryptionPublicKey(ccrjChoiceReturnCodesEncryptionKeyPair.getPublicKey())
				.setCcrjChoiceReturnCodesEncryptionSecretKey(ccrjChoiceReturnCodesEncryptionKeyPair.getPrivateKey())
				.build();
		final PartialDecryptPCCOutput partialDecryptPCCOutput = partialDecryptPCCAlgorithm.partialDecryptPCC(decryptPCCContext,
				partialDecryptPCCInput);

		LOGGER.info("Successfully partially decrypted the encrypted partial Choice Return Codes. [contextIds: {}]", contextIds);

		final GroupVector<GqElement, GqGroup> exponentiatedGammas = partialDecryptPCCOutput.exponentiatedGammas();
		final GroupVector<ExponentiationProof, ZqGroup> exponentiationProofs = partialDecryptPCCOutput.exponentiationProofs();

		return new PartiallyDecryptedEncryptedPCC(contextIds, nodeId, exponentiatedGammas, exponentiationProofs);
	}

	@VisibleForTesting
	void validateVoteIsAllowed(final String electionEventId, final String verificationCardId, final Supplier<LocalDateTime> now,
			final BallotBoxEntity ballotBox) {

		final ElectionContextEntity electionContextEntity = electionContextService.getElectionContextEntity(electionEventId);

		final LocalDateTime electionStartTime = electionContextEntity.getStartTime();
		final LocalDateTime electionEndTime = electionContextEntity.getFinishTime();
		final LocalDateTime currentTime = now.get();

		final boolean afterStartTime = currentTime.isAfter(electionStartTime) || currentTime.isEqual(electionStartTime);
		final boolean beforeEndTime = currentTime.isBefore(electionEndTime.plusSeconds(ballotBox.getGracePeriod())) || currentTime
				.isEqual(electionEndTime.plusSeconds(ballotBox.getGracePeriod()));

		checkState(afterStartTime && beforeEndTime,
				"Impossible to vote before or after the dedicated time. [electionEventId: %s, ballotBoxId: %s, verificationCardId: %s, "
						+ "startTime: %s, finishTime: %s, gracePeriod: %s]",
				electionEventId, ballotBox.getBallotBoxId(), verificationCardId, electionStartTime, electionEndTime, ballotBox.getGracePeriod());
		checkState(!ballotBox.isMixed(),
				"Impossible to vote in an already mixed ballot box. [electionEventId: %s, ballotBoxId: %s, verificationCardId: %s]",
				electionEventId, ballotBox.getBallotBoxId(), verificationCardId);
	}
}
