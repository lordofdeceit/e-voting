/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.voting.sendvote;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.cryptoprimitives.domain.VotingOptionsConstants.MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.domain.election.PrimesMappingTable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

/**
 * Regroups the context values needed by the VerifyBallotCCR<sub>j</sub> algorithm.
 *
 * <ul>
 * <li>encryptionGroup, the {@code GqGroup} with modulus p, cardinality q and generator g. Not null.</li>
 * <li>j, the CCR's index.</li>
 * <li>ee, the election event id. Not null and a valid UUID.</li>
 * <li>vcs, the verification card set id. Not null and a valid UUID.</li>
 * <li>psi, the number of selectable voting options. In range [1, 120].</li>
 * <li>delta_hat, the number of allowed write-ins plus one. Strictly positive.</li>
 * <li>pTable, the primes mapping table. Not null.</li>
 * </ul>
 */
public record VerifyBallotCCRContext(GqGroup encryptionGroup, int nodeId, String electionEventId, String verificationCardSetId,
									 int numberOfSelectableVotingOptions, int numberOfAllowedWriteInsPlusOne, PrimesMappingTable primesMappingTable) {

	public VerifyBallotCCRContext {
		checkNotNull(encryptionGroup);
		checkArgument(NODE_IDS.contains(nodeId), "The node id must be part of the known node ids. [nodeId: %s]", nodeId);
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkArgument(numberOfSelectableVotingOptions >= 1 && numberOfSelectableVotingOptions <= MAXIMUM_NUMBER_OF_SELECTABLE_VOTING_OPTIONS,
				"The number of selectable voting options is out of range.");
		checkArgument(numberOfAllowedWriteInsPlusOne > 0, "The number of allowed write-ins + 1 must be strictly positive.");
		checkNotNull(primesMappingTable);

		checkArgument(encryptionGroup.equals(primesMappingTable.getPTable().getGroup()), "All context must have the same encryption group.");
	}

}
