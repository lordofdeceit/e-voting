/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.extendedauthentication.services.infrastructure.csv;

public record FailedAuthenticationVotingCardItem(String credentialId,
												 String votingCardId) {
}
