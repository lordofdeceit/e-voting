/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.extendedauthentication.domain.model;

/**
 * Class for storing encrypted start voting key.
 */
public record EncryptedSVK(String encryptedSVK) {
}
