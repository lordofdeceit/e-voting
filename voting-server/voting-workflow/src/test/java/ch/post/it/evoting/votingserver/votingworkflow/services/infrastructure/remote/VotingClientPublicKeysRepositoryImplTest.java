/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.votingworkflow.services.infrastructure.remote;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.domain.SecurityLevelTestingOnly;
import ch.post.it.evoting.domain.election.EncryptionParameters;
import ch.post.it.evoting.domain.election.VotingClientPublicKeysData;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.commons.tracking.TrackIdInstance;

import retrofit2.Call;
import retrofit2.Response;

@SecurityLevelTestingOnly
@ExtendWith(MockitoExtension.class)
class VotingClientPublicKeysRepositoryImplTest {

	private final String TRACK_ID = "1";
	private final String TENANT_ID = "100";
	private final String ELECTION_EVENT_ID = "641741bb1de24df1825513e7e66c94f5";
	private final String VERIFICATION_CARD_SET_ID = "6770ace8b4f040e8adfb3753e058657a";
	private final String SETUP_COMPONENT_PUBLIC_KEYS_PATH = "setupkeys";

	@Mock
	private TrackIdInstance trackId;

	@Mock
	private VerificationClient verificationClient;

	@InjectMocks
	private VotingClientPublicKeysRepositoryImpl rut = new VotingClientPublicKeysRepositoryImpl(verificationClient);

	@BeforeEach
	void init() {
		when(trackId.getTrackId()).thenReturn(TRACK_ID);
	}

	@Test
	void testFindVotingClientPublicKeysSuccessful() throws Exception {
		final GqGroup gqGroup;
		gqGroup = new GqGroup(BigInteger.valueOf(23), BigInteger.valueOf(11), BigInteger.valueOf(2));

		final List<GqElement> gqElements = new ArrayList<>();
		gqElements.add(GqElement.GqElementFactory.fromSquareRoot(BigInteger.valueOf(7), gqGroup));
		gqElements.add(GqElement.GqElementFactory.fromSquareRoot(BigInteger.valueOf(5), gqGroup));
		final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
		final EncryptionParameters encryptionParameters = new EncryptionParameters("23", "11", "2");

		final VotingClientPublicKeysData expected = new VotingClientPublicKeysData();
		expected.setEncryptionParameters(encryptionParameters);
		final List<String> publicKey = new ElGamalMultiRecipientPublicKey(GroupVector.from(gqElements)).getKeyElements().stream().map(pk -> {
			try {
				return objectMapper.writeValueAsString(pk);
			} catch (final JsonProcessingException e) {
				return "";
			}
		}).collect(Collectors.toList());
		expected.setElectionPublicKey(publicKey);
		expected.setChoiceReturnCodesEncryptionPublicKey(publicKey);

		@SuppressWarnings("unchecked")
		final Call<VotingClientPublicKeysData> callMock = (Call<VotingClientPublicKeysData>) Mockito.mock(Call.class);
		when(callMock.execute()).thenReturn(Response.success(expected));

		when(verificationClient
				.getVotingClientPublicKeys(TRACK_ID, SETUP_COMPONENT_PUBLIC_KEYS_PATH, TENANT_ID, ELECTION_EVENT_ID))
				.thenReturn(callMock);

		final VotingClientPublicKeysData votingClientPublicKeys = rut.findByTenantElectionEventVerificationCardSetId(TENANT_ID, ELECTION_EVENT_ID,
				VERIFICATION_CARD_SET_ID);
		assertEquals(expected, votingClientPublicKeys);
	}

	@Test
	void testFindVotingClientPublicKeysUnsuccessful() throws ResourceNotFoundException {
		when(verificationClient
				.getVotingClientPublicKeys(TRACK_ID, SETUP_COMPONENT_PUBLIC_KEYS_PATH, TENANT_ID, ELECTION_EVENT_ID))
				.thenThrow(ResourceNotFoundException.class);

		assertThrows(ResourceNotFoundException.class,
				() -> rut.findByTenantElectionEventVerificationCardSetId(TENANT_ID, ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
	}
}
