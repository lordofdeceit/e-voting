/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.votingworkflow.services.domain.model.authentication;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

import javax.json.JsonObject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.domain.SecurityLevelTestingOnly;
import ch.post.it.evoting.domain.configuration.VerificationCardKeystore;
import ch.post.it.evoting.domain.election.EncryptionParameters;
import ch.post.it.evoting.domain.election.VotingClientPublicKeysData;
import ch.post.it.evoting.domain.election.validation.ValidationError;
import ch.post.it.evoting.domain.election.validation.ValidationErrorType;
import ch.post.it.evoting.domain.election.validation.ValidationResult;
import ch.post.it.evoting.votingserver.commons.beans.challenge.ChallengeInformation;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ApplicationException;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.votingworkflow.services.domain.model.ballot.BallotRepository;
import ch.post.it.evoting.votingserver.votingworkflow.services.domain.model.ballot.BallotTextRepository;
import ch.post.it.evoting.votingserver.votingworkflow.services.domain.model.ballotbox.BallotBoxInformationRepository;
import ch.post.it.evoting.votingserver.votingworkflow.services.domain.model.information.VoterInformation;
import ch.post.it.evoting.votingserver.votingworkflow.services.domain.model.state.VotingCardState;
import ch.post.it.evoting.votingserver.votingworkflow.services.domain.model.state.VotingCardStates;
import ch.post.it.evoting.votingserver.votingworkflow.services.domain.model.votingclientpublickeys.VotingClientPublicKeysRepository;
import ch.post.it.evoting.votingserver.votingworkflow.services.domain.service.VotingCardStateService;
import ch.post.it.evoting.votingserver.votingworkflow.services.infrastructure.remote.VerificationCardKeystoreRepository;

@SecurityLevelTestingOnly
@ExtendWith(MockitoExtension.class)
class AuthenticationTokenServiceTest {

	private static final String RESOURCES_FOLDER_NAME = "AuthenticationTokenServiceTest";
	private static final String RESOURCES_FILE_NAME = "ballot.json";

	private final String VERIFICATION_CARD_SET_ID = "1";
	private final String TENANT_ID = "100";
	private final String ELECTION_EVENT_ID = "1";
	private final String CREDENTIAL_ID = "1";
	private final String VOTING_CARD_ID = "1";
	private final String BALLOT_ID = "1";
	private final String BALLOT_BOX_ID = "1";
	private final String VERIFICATION_CARD_ID = "1";

	@InjectMocks
	private AuthenticationTokenService sut = new AuthenticationTokenService();

	@Mock
	private AuthenticationTokenRepository authenticationTokenRepository;

	@Mock
	private BallotRepository ballotRepository;

	@Mock
	private BallotBoxInformationRepository ballotBoxInformationRepository;

	@Mock
	private VerificationCardKeystoreRepository verificationCardKeystoreRepository;

	@Mock
	private BallotTextRepository ballotTextRepository;

	@Mock
	private VotingCardStateService votingCardStateService;

	@Mock
	private VotingClientPublicKeysRepository votingClientPublicKeysRepository;

	@Test
	void testValidateAuthenticationToken() throws IOException, ResourceNotFoundException, ApplicationException {
		final AuthenticationToken authenticationToken = new AuthenticationToken();
		authenticationToken.setId("1");

		final ValidationResult validationResultMock = new ValidationResult();
		validationResultMock.setResult(true);

		final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
		when(authenticationTokenRepository
				.validateAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID, objectMapper.writeValueAsString(authenticationToken)))
				.thenReturn(validationResultMock);
		sut.validateAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID, objectMapper.writeValueAsString(authenticationToken));

		Assertions.assertTrue(validationResultMock.isResult());
	}

	@Test
	void testGetAuthenticationTokenSuccessful() throws Exception {
		final ChallengeInformation challengeInformation = new ChallengeInformation();

		final VoterInformation voterInformationMock = new VoterInformation();
		voterInformationMock.setVotingCardId(VOTING_CARD_ID);
		voterInformationMock.setBallotId(BALLOT_ID);
		voterInformationMock.setBallotBoxId(BALLOT_BOX_ID);
		voterInformationMock.setVerificationCardId(VERIFICATION_CARD_ID);
		voterInformationMock.setVerificationCardSetId(VERIFICATION_CARD_SET_ID);

		final AuthenticationTokenMessage authTokenMessageMock = new AuthenticationTokenMessage();
		authTokenMessageMock.setAuthenticationToken(new AuthenticationToken(voterInformationMock, "base64authToken", "timestamp", "signature"));
		authTokenMessageMock.setValidationError(new ValidationError(ValidationErrorType.SUCCESS));

		final VotingCardState votingCardStateMock = new VotingCardState();
		votingCardStateMock.setState(VotingCardStates.NOT_SENT);

		final VerificationCardKeystore verificationCardKeystoreMock = new VerificationCardKeystore("0123456789abcdef0123456789abcdef",
				"dmVyaWZpY2F0aW9uQ2FyZEtleXN0b3Jl");

		final GqGroup gqGroup;
		gqGroup = new GqGroup(BigInteger.valueOf(11), BigInteger.valueOf(5), BigInteger.valueOf(3));

		final EncryptionParameters encryptionParameters = new EncryptionParameters("11", "5", "3");
		final List<GqElement> gqElements = Collections.singletonList(GqElement.GqElementFactory.fromValue(BigInteger.ONE, gqGroup));
		final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
		final List<String> publicKey = new ElGamalMultiRecipientPublicKey(GroupVector.from(gqElements)).getKeyElements().stream().map(pk -> {
			try {
				return objectMapper.writeValueAsString(pk);
			} catch (final JsonProcessingException e) {
				return "";
			}
		}).toList();

		final VotingClientPublicKeysData votingClientPublicKeysData = new VotingClientPublicKeysData();
		votingClientPublicKeysData.setEncryptionParameters(encryptionParameters);
		votingClientPublicKeysData.setElectionPublicKey(publicKey);
		votingClientPublicKeysData.setChoiceReturnCodesEncryptionPublicKey(publicKey);

		final String ballotString = Files.readString(
				Paths.get(AuthenticationTokenServiceTest.class.getClassLoader()
						.getResource(RESOURCES_FOLDER_NAME + File.separator + RESOURCES_FILE_NAME).toURI()));

		when(authenticationTokenRepository.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation))
				.thenReturn(authTokenMessageMock);
		when(votingCardStateService.getVotingCardState(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID)).thenReturn(votingCardStateMock);
		when(ballotRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID)).thenReturn(ballotString);
		when(ballotTextRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID))
				.thenReturn("{ \"ballotTexts\": [ \"text1\", \"text2\" ], \"ballotTextsSignature\": [ \"signature1\",  \"signature2\" ] }");
		when(ballotBoxInformationRepository.getBallotBoxInfoByTenantIdElectionEventIdBallotBoxId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_BOX_ID))
				.thenReturn("{}");
		when(verificationCardKeystoreRepository.findByElectionEventIdAndVerificationCardSetIdAndVerificationCardId(ELECTION_EVENT_ID,
				VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID))
				.thenReturn(verificationCardKeystoreMock);
		when(votingClientPublicKeysRepository.findByTenantElectionEventVerificationCardSetId(TENANT_ID, ELECTION_EVENT_ID,
				VERIFICATION_CARD_SET_ID)).thenReturn(
				votingClientPublicKeysData);

		final JsonObject authToken = sut.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation);
		Assertions.assertNotNull(authToken);
	}

	@Test
	void testGetAuthenticationToken_AuthTokenNotFound() throws ApplicationException, ResourceNotFoundException {
		final ChallengeInformation challengeInformation = new ChallengeInformation();

		when(authenticationTokenRepository.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation))
				.thenThrow(new ResourceNotFoundException("exception"));

		assertThrows(ResourceNotFoundException.class,
				() -> sut.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation));
	}

	@Test
	void testGetAuthenticationToken_VoteCardStateNotFound() throws ApplicationException, ResourceNotFoundException {
		final ChallengeInformation challengeInformation = new ChallengeInformation();

		final VoterInformation voterInformationMock = new VoterInformation();
		voterInformationMock.setVotingCardId(VOTING_CARD_ID);
		voterInformationMock.setBallotId(BALLOT_ID);
		voterInformationMock.setBallotBoxId(BALLOT_BOX_ID);
		voterInformationMock.setVerificationCardId(VERIFICATION_CARD_ID);
		voterInformationMock.setVerificationCardSetId(VERIFICATION_CARD_SET_ID);

		final AuthenticationTokenMessage authTokenMessageMock = new AuthenticationTokenMessage();
		authTokenMessageMock.setAuthenticationToken(new AuthenticationToken(voterInformationMock, "base64authToken", "timestamp", "signature"));
		authTokenMessageMock.setValidationError(new ValidationError(ValidationErrorType.SUCCESS));

		when(authenticationTokenRepository.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation))
				.thenReturn(authTokenMessageMock);
		when(votingCardStateService.getVotingCardState(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID))
				.thenThrow(new ApplicationException("exception"));

		assertThrows(ApplicationException.class, () -> sut.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation));
	}

	@Test
	void testGetAuthenticationToken_BallotNotFound() throws ApplicationException, ResourceNotFoundException {
		final ChallengeInformation challengeInformation = new ChallengeInformation();

		final VoterInformation voterInformationMock = new VoterInformation();
		voterInformationMock.setVotingCardId(VOTING_CARD_ID);
		voterInformationMock.setBallotId(BALLOT_ID);
		voterInformationMock.setBallotBoxId(BALLOT_BOX_ID);
		voterInformationMock.setVerificationCardId(VERIFICATION_CARD_ID);
		voterInformationMock.setVerificationCardSetId(VERIFICATION_CARD_SET_ID);

		final AuthenticationTokenMessage authTokenMessageMock = new AuthenticationTokenMessage();
		authTokenMessageMock.setAuthenticationToken(new AuthenticationToken(voterInformationMock, "base64authToken", "timestamp", "signature"));
		authTokenMessageMock.setValidationError(new ValidationError(ValidationErrorType.SUCCESS));

		final VotingCardState votingCardStateMock = new VotingCardState();
		votingCardStateMock.setState(VotingCardStates.NOT_SENT);

		when(authenticationTokenRepository.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation))
				.thenReturn(authTokenMessageMock);
		when(votingCardStateService.getVotingCardState(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID)).thenReturn(votingCardStateMock);
		when(ballotRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID))
				.thenThrow(new ResourceNotFoundException("exception"));

		assertThrows(ResourceNotFoundException.class,
				() -> sut.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation));
	}

	@Test
	void testGetAuthenticationToken_BallotTextNotFound() throws ApplicationException, ResourceNotFoundException {
		final ChallengeInformation challengeInformation = new ChallengeInformation();

		final VoterInformation voterInformationMock = new VoterInformation();
		voterInformationMock.setVotingCardId(VOTING_CARD_ID);
		voterInformationMock.setBallotId(BALLOT_ID);
		voterInformationMock.setBallotBoxId(BALLOT_BOX_ID);
		voterInformationMock.setVerificationCardId(VERIFICATION_CARD_ID);
		voterInformationMock.setVerificationCardSetId(VERIFICATION_CARD_SET_ID);

		final AuthenticationTokenMessage authTokenMessageMock = new AuthenticationTokenMessage();
		authTokenMessageMock.setAuthenticationToken(new AuthenticationToken(voterInformationMock, "base64authToken", "timestamp", "signature"));
		authTokenMessageMock.setValidationError(new ValidationError(ValidationErrorType.SUCCESS));

		final VotingCardState votingCardStateMock = new VotingCardState();
		votingCardStateMock.setState(VotingCardStates.NOT_SENT);

		when(authenticationTokenRepository.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation))
				.thenReturn(authTokenMessageMock);
		when(votingCardStateService.getVotingCardState(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID)).thenReturn(votingCardStateMock);
		when(ballotRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID)).thenReturn("{}");
		when(ballotTextRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID))
				.thenThrow(new ResourceNotFoundException("exception"));

		assertThrows(ResourceNotFoundException.class,
				() -> sut.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation));
	}

	@Test
	void testGetAuthenticationToken_BallotBoxInfoNotFound() throws ApplicationException, ResourceNotFoundException {
		final ChallengeInformation challengeInformation = new ChallengeInformation();

		final VoterInformation voterInformationMock = new VoterInformation();
		voterInformationMock.setVotingCardId(VOTING_CARD_ID);
		voterInformationMock.setBallotId(BALLOT_ID);
		voterInformationMock.setBallotBoxId(BALLOT_BOX_ID);
		voterInformationMock.setVerificationCardId(VERIFICATION_CARD_ID);
		voterInformationMock.setVerificationCardSetId(VERIFICATION_CARD_SET_ID);

		final AuthenticationTokenMessage authTokenMessageMock = new AuthenticationTokenMessage();
		authTokenMessageMock.setAuthenticationToken(new AuthenticationToken(voterInformationMock, "base64authToken", "timestamp", "signature"));
		authTokenMessageMock.setValidationError(new ValidationError(ValidationErrorType.SUCCESS));

		final VotingCardState votingCardStateMock = new VotingCardState();
		votingCardStateMock.setState(VotingCardStates.NOT_SENT);

		when(authenticationTokenRepository.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation))
				.thenReturn(authTokenMessageMock);
		when(votingCardStateService.getVotingCardState(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID)).thenReturn(votingCardStateMock);
		when(ballotRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID)).thenReturn("{}");
		when(ballotTextRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID))
				.thenReturn("{ \"ballotTexts\": [ \"text1\", \"text2\" ], \"ballotTextsSignature\": [ \"signature1\",  \"signature2\" ] }");
		when(ballotBoxInformationRepository.getBallotBoxInfoByTenantIdElectionEventIdBallotBoxId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_BOX_ID))
				.thenThrow(new ResourceNotFoundException("exception"));

		assertThrows(ResourceNotFoundException.class,
				() -> sut.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation));
	}

	@Test
	void testGetAuthenticationToken_VerificationNotFound() throws Exception {
		final ChallengeInformation challengeInformation = new ChallengeInformation();

		final VoterInformation voterInformationMock = new VoterInformation();
		voterInformationMock.setVotingCardId(VOTING_CARD_ID);
		voterInformationMock.setBallotId(BALLOT_ID);
		voterInformationMock.setBallotBoxId(BALLOT_BOX_ID);
		voterInformationMock.setVerificationCardId(VERIFICATION_CARD_ID);
		voterInformationMock.setVerificationCardSetId(VERIFICATION_CARD_SET_ID);

		final AuthenticationTokenMessage authTokenMessageMock = new AuthenticationTokenMessage();
		authTokenMessageMock.setAuthenticationToken(new AuthenticationToken(voterInformationMock, "base64authToken", "timestamp", "signature"));
		authTokenMessageMock.setValidationError(new ValidationError(ValidationErrorType.SUCCESS));

		final VotingCardState votingCardStateMock = new VotingCardState();
		votingCardStateMock.setState(VotingCardStates.NOT_SENT);

		final GqGroup gqGroup;
		gqGroup = new GqGroup(BigInteger.valueOf(11), BigInteger.valueOf(5), BigInteger.valueOf(3));
		final EncryptionParameters encryptionParameters = new EncryptionParameters("11", "5", "3");
		final List<GqElement> gqElements = Collections.singletonList(GqElement.GqElementFactory.fromValue(BigInteger.ONE, gqGroup));
		final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
		final List<String> publicKey = new ElGamalMultiRecipientPublicKey(GroupVector.from(gqElements)).getKeyElements().stream().map(pk -> {
			try {
				return objectMapper.writeValueAsString(pk);
			} catch (final JsonProcessingException e) {
				return "";
			}
		}).toList();
		final VotingClientPublicKeysData votingClientPublicKeysData = new VotingClientPublicKeysData();
		votingClientPublicKeysData.setEncryptionParameters(encryptionParameters);
		votingClientPublicKeysData.setElectionPublicKey(publicKey);
		votingClientPublicKeysData.setChoiceReturnCodesEncryptionPublicKey(publicKey);

		when(authenticationTokenRepository.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation))
				.thenReturn(authTokenMessageMock);
		when(votingCardStateService.getVotingCardState(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID)).thenReturn(votingCardStateMock);
		when(ballotRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID)).thenReturn("{}");
		when(ballotTextRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID))
				.thenReturn("{ \"ballotTexts\": [ \"text1\", \"text2\" ], \"ballotTextsSignature\": [ \"signature1\",  \"signature2\" ] }");
		when(ballotBoxInformationRepository.getBallotBoxInfoByTenantIdElectionEventIdBallotBoxId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_BOX_ID))
				.thenReturn("{}");
		when(votingClientPublicKeysRepository.findByTenantElectionEventVerificationCardSetId(TENANT_ID, ELECTION_EVENT_ID,
				VERIFICATION_CARD_SET_ID)).thenReturn(
				votingClientPublicKeysData);
		when(verificationCardKeystoreRepository.findByElectionEventIdAndVerificationCardSetIdAndVerificationCardId(ELECTION_EVENT_ID,
				VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_ID))
				.thenThrow(new ResourceNotFoundException("exception"));

		assertThrows(ResourceNotFoundException.class,
				() -> sut.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation));
	}

	@Test
	void testGetAuthenticationToken_VotingClientPublicKeysNotFound() throws ApplicationException, ResourceNotFoundException, IOException {
		final ChallengeInformation challengeInformation = new ChallengeInformation();

		final VoterInformation voterInformationMock = new VoterInformation();
		voterInformationMock.setVotingCardId(VOTING_CARD_ID);
		voterInformationMock.setBallotId(BALLOT_ID);
		voterInformationMock.setBallotBoxId(BALLOT_BOX_ID);
		voterInformationMock.setVerificationCardId(VERIFICATION_CARD_ID);
		voterInformationMock.setVerificationCardSetId(VERIFICATION_CARD_SET_ID);

		final AuthenticationTokenMessage authTokenMessageMock = new AuthenticationTokenMessage();
		authTokenMessageMock.setAuthenticationToken(new AuthenticationToken(voterInformationMock, "base64authToken", "timestamp", "signature"));
		authTokenMessageMock.setValidationError(new ValidationError(ValidationErrorType.SUCCESS));

		final VotingCardState votingCardStateMock = new VotingCardState();
		votingCardStateMock.setState(VotingCardStates.NOT_SENT);

		when(authenticationTokenRepository.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation))
				.thenReturn(authTokenMessageMock);
		when(votingCardStateService.getVotingCardState(TENANT_ID, ELECTION_EVENT_ID, VOTING_CARD_ID)).thenReturn(votingCardStateMock);
		when(ballotRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID)).thenReturn("{}");
		when(ballotTextRepository.findByTenantIdElectionEventIdBallotId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_ID))
				.thenReturn("{ \"ballotTexts\": [ \"text1\", \"text2\" ], \"ballotTextsSignature\": [ \"signature1\",  \"signature2\" ] }");
		when(ballotBoxInformationRepository.getBallotBoxInfoByTenantIdElectionEventIdBallotBoxId(TENANT_ID, ELECTION_EVENT_ID, BALLOT_BOX_ID))
				.thenReturn("{}");
		when(votingClientPublicKeysRepository.findByTenantElectionEventVerificationCardSetId(TENANT_ID, ELECTION_EVENT_ID,
				VERIFICATION_CARD_SET_ID)).thenThrow(
				new ResourceNotFoundException("exception"));

		assertThrows(ResourceNotFoundException.class,
				() -> sut.getAuthenticationToken(TENANT_ID, ELECTION_EVENT_ID, CREDENTIAL_ID, challengeInformation));
	}

}
