/*
 * (c) Copyright 2022 Swiss Post Ltd
 */
package ch.post.it.evoting.votingserver.votingworkflow.services.infrastructure.remote;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import java.io.IOException;
import java.io.UncheckedIOException;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.domain.configuration.VerificationCardKeystore;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RetrofitConsumer;
import ch.post.it.evoting.votingserver.commons.tracking.TrackIdInstance;

import okhttp3.ResponseBody;

/**
 * Implementation of the VerificationCardKeystoreRepository using a REST client.
 */
@Stateless
public class VerificationCardKeystoreRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(VerificationCardKeystoreRepository.class);

	private final VerificationClient verificationClient;

	@Inject
	private TrackIdInstance trackId;

	@Inject
	VerificationCardKeystoreRepository(final VerificationClient verificationClient) {
		this.verificationClient = verificationClient;
	}

	/**
	 * Finds the verification card keystore given an election event id, a verification card set id and a verification card id. This implementation is
	 * based on a rest client which calls to a web service restoperation.
	 *
	 * @param electionEventId       the identifier of the election event. Must be non-null and a valid UUID.
	 * @param verificationCardSetId the identifier of the verification card set. Must be non-null and a valid UUID.
	 * @param verificationCardId    the identifier of the verification card. Must be non-null and a valid UUID.
	 * @return the verification card keystore
	 * @throws ResourceNotFoundException if the verification card keystore does not exist for the given ids.
	 */
	public VerificationCardKeystore findByElectionEventIdAndVerificationCardSetIdAndVerificationCardId(final String electionEventId,
			final String verificationCardSetId, final String verificationCardId) throws ResourceNotFoundException {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		validateUUID(verificationCardId);

		try {
			final ResponseBody response = RetrofitConsumer.processResponse(
					verificationClient.findVerificationCardKeystore(trackId.getTrackId(), electionEventId, verificationCardSetId,
							verificationCardId));
			final VerificationCardKeystore verificationCardKeystore = DomainObjectMapper.getNewInstance()
					.readValue(response.byteStream(), VerificationCardKeystore.class);
			LOGGER.info("Verification card keystore found. [electionEventId: {}, verificationCardSetId: {}, verificationCardId: {}]", electionEventId,
					verificationCardSetId, verificationCardId);
			return verificationCardKeystore;
		} catch (final ResourceNotFoundException e) {
			LOGGER.error("Verification card keystore not found. [electionEventId: {}, verificationCardSetId: {}, verificationCardId: {}]",
					electionEventId,
					verificationCardSetId, verificationCardId);
			throw e;
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}
}
