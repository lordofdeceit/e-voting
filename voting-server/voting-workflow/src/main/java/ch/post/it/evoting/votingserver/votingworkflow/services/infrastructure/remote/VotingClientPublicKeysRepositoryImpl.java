/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.votingworkflow.services.infrastructure.remote;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.post.it.evoting.domain.election.VotingClientPublicKeysData;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RetrofitConsumer;
import ch.post.it.evoting.votingserver.commons.tracking.TrackIdInstance;
import ch.post.it.evoting.votingserver.commons.util.PropertiesFileReader;
import ch.post.it.evoting.votingserver.votingworkflow.services.domain.model.votingclientpublickeys.VotingClientPublicKeysRepository;

/**
 * Implementation of the VotingClientPublicKeysRepository using a REST client.
 */
@Stateless
public class VotingClientPublicKeysRepositoryImpl implements VotingClientPublicKeysRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingClientPublicKeysRepositoryImpl.class);

	/**
	 * The properties file reader.
	 */
	private static final PropertiesFileReader PROPERTIES = PropertiesFileReader.getInstance();
	private static final String SETUP_COMPONENT_PUBLIC_KEYS_PATH = PROPERTIES.getPropertyValue("SETUP_COMPONENT_PUBLIC_KEYS_PATH");

	private VerificationClient verificationClient;

	@Inject
	private TrackIdInstance trackId;

	@Inject
	VotingClientPublicKeysRepositoryImpl(final VerificationClient verificationClient) {
		this.verificationClient = verificationClient;
	}

	/**
	 * Finds the voting client public keys given an election even id. This implementation is based on a rest client which calls to a web service rest
	 * operation.
	 *
	 * @param tenantId              the identifier of the tenant.
	 * @param electionEventId       the identifier of the election event.
	 * @param verificationCardSetId the identifier of the verification card set.
	 * @return the voting client public keys given the election event id.
	 * @throws ResourceNotFoundException if the election event context does not exist for the given election event id.
	 */
	@Override
	public VotingClientPublicKeysData findByTenantElectionEventVerificationCardSetId(final String tenantId, final String electionEventId,
			final String verificationCardSetId) throws ResourceNotFoundException {
		checkNotNull(tenantId);
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);

		try {
			final VotingClientPublicKeysData votingClientPublicKeys = RetrofitConsumer.processResponse(
					verificationClient.getVotingClientPublicKeys(trackId.getTrackId(), SETUP_COMPONENT_PUBLIC_KEYS_PATH, tenantId, electionEventId));
			LOGGER.info("Voting client public keys found. [tenantId: {}, electionEventId: {}, verificationCardSetId: {}]", tenantId, electionEventId,
					verificationCardSetId);
			return votingClientPublicKeys;
		} catch (final ResourceNotFoundException e) {
			LOGGER.error("Voting client public keys not found. [tenantId: {}, electionEventId: {}]", tenantId, electionEventId);
			throw e;
		}
	}
}
