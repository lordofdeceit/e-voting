/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.votingworkflow.services.domain.model.authentication;

import java.io.IOException;
import java.util.List;
import java.util.stream.IntStream;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.domain.configuration.VerificationCardKeystore;
import ch.post.it.evoting.domain.election.VotingClientPublicKeysData;
import ch.post.it.evoting.domain.election.validation.ValidationResult;
import ch.post.it.evoting.votingserver.commons.beans.challenge.ChallengeInformation;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ApplicationException;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.DuplicateEntryException;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.EntryPersistenceException;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.commons.util.JsonUtils;
import ch.post.it.evoting.votingserver.votingworkflow.services.domain.model.ballot.BallotRepository;
import ch.post.it.evoting.votingserver.votingworkflow.services.domain.model.ballot.BallotTextRepository;
import ch.post.it.evoting.votingserver.votingworkflow.services.domain.model.ballotbox.BallotBoxInformationRepository;
import ch.post.it.evoting.votingserver.votingworkflow.services.domain.model.state.VotingCardState;
import ch.post.it.evoting.votingserver.votingworkflow.services.domain.model.state.VotingCardStates;
import ch.post.it.evoting.votingserver.votingworkflow.services.domain.model.votingclientpublickeys.VotingClientPublicKeysRepository;
import ch.post.it.evoting.votingserver.votingworkflow.services.domain.service.VotingCardStateService;
import ch.post.it.evoting.votingserver.votingworkflow.services.infrastructure.remote.VerificationCardKeystoreRepository;

/**
 * Service for generating the authentication token.
 */
@Stateless
public class AuthenticationTokenService {

	private static final String BALLOT = "ballot";
	private static final String BALLOT_BOX = "ballotBox";
	private static final String CI_SELECTIONS = "ciSelections";
	private static final String VALIDATION_ERROR = "validationError";
	private static final String CI_VOTING_OPTIONS = "ciVotingOptions";
	private static final String VOTING_CARD_STATE = "votingCardState";
	private static final String VERIFICATION_CARD = "verificationCard";
	private static final String AUTHENTICATION_TOKEN = "authenticationToken";
	private static final String ACTUAL_VOTING_OPTIONS = "actualVotingOptions";
	private static final String ENCODED_VOTING_OPTIONS = "encodedVotingOptions";
	private static final String BALLOT_TEXTS_SIGNATURE = "ballotTextsSignature";
	private static final String TOTAL_NUMBER_OF_WRITE_INS = "totalNumberOfWriteIns";
	private static final String VOTING_CLIENT_PUBLIC_KEYS = "votingClientPublicKeys";
	private static final String VERIFICATION_CARD_KEYSTORE = "verificationCardKeystore";

	private static final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();

	@EJB
	private BallotRepository ballotRepository;
	@EJB
	private BallotTextRepository ballotTextRepository;
	@EJB
	private VotingCardStateService votingCardStateService;
	@EJB
	private AuthenticationTokenRepository authenticationTokenRepository;
	@EJB
	private BallotBoxInformationRepository ballotBoxInformationRepository;
	@EJB
	private VotingClientPublicKeysRepository votingClientPublicKeysRepository;
	@EJB
	private VerificationCardKeystoreRepository verificationCardKeystoreRepository;

	/**
	 * Gets the authentication token which matches with the given parameters.
	 *
	 * @param tenantId             - the tenant identifier.
	 * @param electionEventId      - the election event identifier.
	 * @param credentialId         - the credential Identifier.
	 * @param challengeInformation - the challenge information including client challenge, server challenge, and server timestamp.
	 * @return json object represeting the authentication token.
	 */
	public JsonObject getAuthenticationToken(
			final String tenantId, final String electionEventId, final String credentialId, final ChallengeInformation challengeInformation)
			throws ApplicationException, ResourceNotFoundException, IOException, DuplicateEntryException, EntryPersistenceException {

		// build the authentication token
		final AuthenticationTokenMessage authTokenMessage = buildAuthenticationToken(tenantId, electionEventId, credentialId, challengeInformation);

		// assuming the authToken is always present even if it fails 'validation'
		// auth token
		final AuthenticationToken authToken = authTokenMessage.getAuthenticationToken();
		final JsonObject authTokenJsonObject = JsonUtils.getJsonObject(objectMapper.writeValueAsString(authToken));

		// get the voting card id from the voter information
		final String votingCardId = authToken.getVoterInformation().getVotingCardId();

		// recover the voting card state
		final VotingCardState votingCardState = votingCardStateService.getVotingCardState(tenantId, electionEventId, votingCardId);

		// initialize the voting card state
		votingCardStateService.initializeVotingCardState(votingCardState);
		final VotingCardStates votingCardStateValue = votingCardState.getState();
		// always include the validation result
		final JsonValue validationJson = JsonUtils.getJsonObject(objectMapper.writeValueAsString(authTokenMessage.getValidationError()));

		// create object response
		final JsonObjectBuilder authTokenObjectBuilder = Json.createObjectBuilder();
		authTokenObjectBuilder.add(AUTHENTICATION_TOKEN, authTokenJsonObject).add(VALIDATION_ERROR, validationJson)
				.add(VOTING_CARD_STATE, votingCardStateValue.name());

		// Always add the ballot information to the result so that the FE can show the ballot info
		addBallotAndVerificationDataToAuthToken(tenantId, electionEventId, authToken, authTokenObjectBuilder);

		return authTokenObjectBuilder.build();
	}

	public ValidationResult validateAuthenticationToken(final String tenantId, final String electionEventId, final String votingCardId,
			final String authenticationToken) throws IOException, ResourceNotFoundException, ApplicationException {

		return authenticationTokenRepository.validateAuthenticationToken(tenantId, electionEventId, votingCardId, authenticationToken);

	}

	// Generate authentication token.
	private AuthenticationTokenMessage buildAuthenticationToken(final String tenantId, final String electionEventId, final String credentialId,
			final ChallengeInformation challengeInformation) throws ResourceNotFoundException, ApplicationException {
		return authenticationTokenRepository.getAuthenticationToken(tenantId, electionEventId, credentialId, challengeInformation);
	}

	// Gets the verification card keystore json.
	private VerificationCardKeystore getVerificationCardKeystore(final String electionEventId, final String verificationCardSetId,
			final String verificationCardId) throws ResourceNotFoundException {
		return verificationCardKeystoreRepository.findByElectionEventIdAndVerificationCardSetIdAndVerificationCardId(
				electionEventId, verificationCardSetId, verificationCardId);
	}

	// Gets the ballot box json.
	private JsonObject getBallotBoxJson(final String tenantId, final String electionEventId, final String ballotBoxId)
			throws ResourceNotFoundException {
		final String ballotBox = ballotBoxInformationRepository
				.getBallotBoxInfoByTenantIdElectionEventIdBallotBoxId(tenantId, electionEventId, ballotBoxId);
		return JsonUtils.getJsonObject(ballotBox);
	}

	// Gets the ballot text json.
	private JsonValue getBallotTextSignature(final String tenantId, final String electionEventId, final String ballotId)
			throws ResourceNotFoundException {
		final String ballotAndSignatureJson = ballotTextRepository.findByTenantIdElectionEventIdBallotId(tenantId, electionEventId, ballotId);
		final JsonObject ballotAndSignatureJsonObject = JsonUtils.getJsonObject(ballotAndSignatureJson);
		return ballotAndSignatureJsonObject.getJsonArray(BALLOT_TEXTS_SIGNATURE);
	}

	// Gets the voting client public keys json.
	private JsonObject getVotingClientPublicKeys(final String tenantId, final String electionEventId, final String verificationCardSetId)
			throws ResourceNotFoundException, IOException {
		final VotingClientPublicKeysData votingClientPublicKeys = votingClientPublicKeysRepository.findByTenantElectionEventVerificationCardSetId(
				tenantId, electionEventId, verificationCardSetId);
		return JsonUtils.getJsonObject(objectMapper.writeValueAsString(votingClientPublicKeys));
	}

	/**
	 * Add the ballot and verification information to the authentication token.
	 *
	 * @param tenantId               the tenant
	 * @param electionEventId        the election
	 * @param authToken              the original authentication token
	 * @param authTokenObjectBuilder the resulting authentication object
	 */
	private void addBallotAndVerificationDataToAuthToken(final String tenantId, final String electionEventId, final AuthenticationToken authToken,
			final JsonObjectBuilder authTokenObjectBuilder) throws ResourceNotFoundException, IOException {

		final String verificationCardSetId = authToken.getVoterInformation().getVerificationCardSetId();

		// search ballot representation extracting the info from the token
		final String ballotId = authToken.getVoterInformation().getBallotId();
		final String ballotString = ballotRepository.findByTenantIdElectionEventIdBallotId(tenantId, electionEventId, ballotId);
		final Ballot ballot = objectMapper.readValue(ballotString, Ballot.class);

		final JsonValue ballotJson = JsonUtils.getJsonObject(ballotString);

		// search ballot texts extracting the info from the token
		final JsonValue ballotTextSignatureJson = getBallotTextSignature(tenantId, electionEventId, ballotId);

		// search ballot box representation extracting the info from the token
		final String ballotBoxId = authToken.getVoterInformation().getBallotBoxId();
		final JsonValue ballotBoxJson = getBallotBoxJson(tenantId, electionEventId, ballotBoxId);

		// search voting client public keys
		final JsonValue votingClientPublicKeys = getVotingClientPublicKeys(tenantId, electionEventId, verificationCardSetId);

		// Obtains the verification card keystore
		final String verificationCardId = authToken.getVoterInformation().getVerificationCardId();
		final VerificationCardKeystore verificationCardKeystore = getVerificationCardKeystore(electionEventId, verificationCardSetId,
				verificationCardId);
		final JsonArray encodedVotingOptionsJson = JsonUtils.getJsonArray(objectMapper.writeValueAsString(ballot.getEncodedVotingOptions()));
		final JsonArray actualVotingOptionsJson = JsonUtils.getJsonArray(objectMapper.writeValueAsString(ballot.getActualVotingOptions()));
		final CombinedCorrectnessInformation combinedCorrectnessInformation = new CombinedCorrectnessInformation(ballot);

		final JsonArray ciSelectionsJson = JsonUtils.getJsonArray(
				objectMapper.writeValueAsString(getCorrectnessInformationSelections(combinedCorrectnessInformation)));
		final JsonArray ciVotingOptionsJson = JsonUtils.getJsonArray(
				objectMapper.writeValueAsString(getCorrectnessInformationVotingOptions(combinedCorrectnessInformation)));
		final JsonObject verificationCardJson = Json.createObjectBuilder()
				.add("id", verificationCardId)
				.add(VERIFICATION_CARD_KEYSTORE, verificationCardKeystore.verificationCardKeystore())
				.add(ENCODED_VOTING_OPTIONS, encodedVotingOptionsJson)
				.add(ACTUAL_VOTING_OPTIONS, actualVotingOptionsJson)
				.add(CI_SELECTIONS, ciSelectionsJson)
				.add(CI_VOTING_OPTIONS, ciVotingOptionsJson)
				.add(TOTAL_NUMBER_OF_WRITE_INS, combinedCorrectnessInformation.getTotalNumberOfWriteInOptions())
				.build();

		authTokenObjectBuilder
				.add(BALLOT, ballotJson)
				.add(BALLOT_TEXTS_SIGNATURE, ballotTextSignatureJson)
				.add(BALLOT_BOX, ballotBoxJson)
				.add(VERIFICATION_CARD, verificationCardJson)
				.add(VOTING_CLIENT_PUBLIC_KEYS, votingClientPublicKeys);
	}

	private List<String> getCorrectnessInformationSelections(final CombinedCorrectnessInformation combinedCorrectnessInformation) {
		return IntStream.range(0, combinedCorrectnessInformation.getTotalNumberOfSelections())
				.boxed()
				.map(combinedCorrectnessInformation::getCorrectnessIdForSelectionIndex)
				.toList();
	}

	private List<String> getCorrectnessInformationVotingOptions(final CombinedCorrectnessInformation combinedCorrectnessInformation) {
		return IntStream.range(0, combinedCorrectnessInformation.getTotalNumberOfVotingOptions())
				.boxed()
				.map(combinedCorrectnessInformation::getCorrectnessIdForVotingOptionIndex)
				.toList();
	}

}
