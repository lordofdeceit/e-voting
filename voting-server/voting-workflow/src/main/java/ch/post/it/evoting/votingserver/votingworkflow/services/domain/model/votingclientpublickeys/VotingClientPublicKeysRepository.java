/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.votingworkflow.services.domain.model.votingclientpublickeys;

import java.io.IOException;

import ch.post.it.evoting.domain.election.VotingClientPublicKeysData;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;

/**
 * Interface for providing operations related with voting client public keys.
 */
public interface VotingClientPublicKeysRepository {

	/**
	 * Finds the voting client public keys given an election event id.
	 *
	 * @param tenantId              the identifier of the tenant.
	 * @param electionEventId       the identifier of the election event.
	 * @return the voting client public keys
	 * @throws ResourceNotFoundException if the election event context does not exist for the given election event id.
	 */
	VotingClientPublicKeysData findByTenantElectionEventVerificationCardSetId(final String tenantId, final String electionEventId,
			final String verificationCardSetId)
			throws ResourceNotFoundException, IOException;
}
