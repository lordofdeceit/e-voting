/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.messagebrokerorchestrator.tally;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.domain.tally.ControlComponentBallotBoxPayload;
import ch.post.it.evoting.domain.tally.MixDecryptOnlinePayload;

@Service
public class MixnetPayloadService {

	private final ObjectMapper objectMapper;
	private final ShufflePayloadRepository shufflePayloadRepository;
	private final ControlComponentBallotBoxPayloadRepository controlComponentBallotBoxPayloadRepository;

	public MixnetPayloadService(
			final ObjectMapper objectMapper,
			final ShufflePayloadRepository shufflePayloadRepository,
			final ControlComponentBallotBoxPayloadRepository controlComponentBallotBoxPayloadRepository) {
		this.objectMapper = objectMapper;
		this.shufflePayloadRepository = shufflePayloadRepository;
		this.controlComponentBallotBoxPayloadRepository = controlComponentBallotBoxPayloadRepository;
	}

	@Transactional
	public void saveControlComponentBallotBoxPayload(final ControlComponentBallotBoxPayload controlComponentBallotBoxPayload) {
		checkNotNull(controlComponentBallotBoxPayload);

		final String electionEventId = controlComponentBallotBoxPayload.getElectionEventId();
		final String ballotBoxId = controlComponentBallotBoxPayload.getBallotBoxId();
		final int nodeId = controlComponentBallotBoxPayload.getNodeId();

		final byte[] payload;
		try {
			payload = objectMapper.writeValueAsBytes(controlComponentBallotBoxPayload);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException("Failed to serialize control component ballot box payload.", e);
		}

		final ControlComponentBallotBoxPayloadEntity controlComponentBallotBoxPayloadEntity = new ControlComponentBallotBoxPayloadEntity(
				electionEventId, ballotBoxId, nodeId, payload);

		controlComponentBallotBoxPayloadRepository.save(controlComponentBallotBoxPayloadEntity);
	}

	@Transactional
	public void saveShufflePayload(final ControlComponentShufflePayload mixnetShufflePayload) {
		checkNotNull(mixnetShufflePayload);

		final String electionEventId = mixnetShufflePayload.getElectionEventId();
		final String ballotBoxId = mixnetShufflePayload.getBallotBoxId();
		final int nodeId = mixnetShufflePayload.getNodeId();

		try {
			final byte[] payload = objectMapper.writeValueAsBytes(mixnetShufflePayload);

			final ShufflePayloadEntity shufflePayloadEntity = new ShufflePayloadEntity(electionEventId, ballotBoxId, nodeId, payload);

			shufflePayloadRepository.save(shufflePayloadEntity);
		} catch (final JsonProcessingException ex) {
			throw new UncheckedIOException("Failed to process the mixing DTO", ex);
		}
	}

	@Transactional
	public int countMixDecryptOnlinePayloads(final String electionEventId, final String ballotBoxId) {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		return shufflePayloadRepository.countByElectionEventIdAndBallotBoxId(electionEventId, ballotBoxId);
	}

	@Transactional
	public Optional<MixDecryptOnlinePayload> getMixnetPayloads(final String electionEventId, final String ballotBoxId) {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		final List<ControlComponentBallotBoxPayloadEntity> controlComponentBallotBoxPayloadEntities =
				controlComponentBallotBoxPayloadRepository.findByElectionEventIdAndBallotBoxId(electionEventId, ballotBoxId).stream()
						.sorted(Comparator.comparingInt(ControlComponentBallotBoxPayloadEntity::getNodeId))
						.toList();

		if (controlComponentBallotBoxPayloadEntities.size() != 4) {
			return Optional.empty();
		}

		final List<ShufflePayloadEntity> shufflePayloadEntities =
				shufflePayloadRepository.findByElectionEventIdAndBallotBoxIdOrderByNodeId(electionEventId, ballotBoxId).stream()
						.toList();

		if (shufflePayloadEntities.size() != 4) {
			return Optional.empty();
		}

		return Optional.of(
				createMixDecryptOnlinePayload(electionEventId, ballotBoxId, controlComponentBallotBoxPayloadEntities, shufflePayloadEntities));
	}

	@Transactional
	public List<ControlComponentShufflePayload> getControlComponentShufflePayloadsOrderByNodeId(final String electionEventId, final String ballotBoxId) {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		return shufflePayloadRepository.findByElectionEventIdAndBallotBoxIdOrderByNodeId(electionEventId, ballotBoxId)
				.stream()
				.map(ShufflePayloadEntity::getShufflePayload)
				.map(bytes -> {
					try {
						return objectMapper.readValue(bytes, ControlComponentShufflePayload.class);
					} catch (final IOException e) {
						throw new UncheckedIOException("Couldn't deserialize a ControlComponentShufflePayload", e);
					}
				})
				.toList();
	}

	private MixDecryptOnlinePayload createMixDecryptOnlinePayload(final String electionEventId, final String ballotBoxId,
			final List<ControlComponentBallotBoxPayloadEntity> controlComponentBallotBoxPayloadEntities,
			final List<ShufflePayloadEntity> shufflePayloadEntities) {

		final List<ControlComponentBallotBoxPayload> controlComponentBallotBoxPayloads = controlComponentBallotBoxPayloadEntities.stream()
				.map(controlComponentBallotBoxPayloadEntity -> {
					try {
						return objectMapper.readValue(controlComponentBallotBoxPayloadEntity.getControlComponentBallotBoxPayload(),
								ControlComponentBallotBoxPayload.class);
					} catch (final IOException e) {
						throw new UncheckedIOException("Error deserializing confirmed encrypted votes payload.", e);
					}
				})
				.toList();

		final List<ControlComponentShufflePayload> controlComponentShufflePayloads = shufflePayloadEntities.stream()
				.map(shufflePayloadEntity -> {
					try {
						return objectMapper.readValue(shufflePayloadEntity.getShufflePayload(), ControlComponentShufflePayload.class);
					} catch (final IOException e) {
						throw new UncheckedIOException("Error deserialiazing ControlComponentShufflePayload", e);
					}
				})
				.toList();

		return new MixDecryptOnlinePayload(electionEventId, ballotBoxId, controlComponentBallotBoxPayloads, controlComponentShufflePayloads);

	}

}
