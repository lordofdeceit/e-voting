/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.messagebrokerorchestrator.voting.sendvote;

import static ch.post.it.evoting.domain.SharedQueue.PARTIAL_DECRYPT_PCC_REQUEST_PATTERN;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.VotingServerEncryptedVotePayload;
import ch.post.it.evoting.votingserver.messagebrokerorchestrator.voting.BroadcastCommand;
import ch.post.it.evoting.votingserver.messagebrokerorchestrator.voting.BroadcastCommandProducer;

@RestController
@RequestMapping("/api/v1/voting/sendvote")
public class ReturnCodesPartialDecryptContributionsController {

	private final ObjectMapper objectMapper;
	private final BroadcastCommandProducer broadcastCommandProducer;

	public ReturnCodesPartialDecryptContributionsController(final ObjectMapper objectMapper,
			final BroadcastCommandProducer broadcastCommandProducer) {
		this.objectMapper = objectMapper;
		this.broadcastCommandProducer = broadcastCommandProducer;
	}

	@PostMapping("/partialdecrypt/electionevent/{electionEventId}/verificationCardSetId/{verificationCardSetId}/verificationCardId/{verificationCardId}")
	public List<ControlComponentPartialDecryptPayload> getChoiceReturnCodesPartialDecryptContributions(
			@PathVariable
			final String electionEventId,
			@PathVariable
			final String verificationCardSetId,
			@PathVariable
			final String verificationCardId,
			@RequestBody
			final VotingServerEncryptedVotePayload votingServerEncryptedVotePayload) throws ExecutionException, InterruptedException, TimeoutException {

		final String contextId = String.join("-", Arrays.asList(electionEventId, verificationCardSetId, verificationCardId));

		final BroadcastCommand<ControlComponentPartialDecryptPayload> broadcastCommand = new BroadcastCommand.Builder<ControlComponentPartialDecryptPayload>()
				.contextId(contextId)
				.context(Context.VOTING_RETURN_CODES_PARTIAL_DECRYPT_PCC)
				.payload(votingServerEncryptedVotePayload)
				.pattern(PARTIAL_DECRYPT_PCC_REQUEST_PATTERN)
				.deserialization(this::deserializePayload)
				.build();

		return broadcastCommandProducer.sendMessagesAwaitingNotification(broadcastCommand);
	}

	private ControlComponentPartialDecryptPayload deserializePayload(final byte[] payload) {
		try {
			return objectMapper.readValue(payload, ControlComponentPartialDecryptPayload.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}
}
