/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.messagebrokerorchestrator.tally;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "CONTROL_COMPONENT_BALLOT_BOX_PAYLOAD")
class ControlComponentBallotBoxPayloadEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "CONTROL_COMPONENT_BALLOT_BOX_PAYLOAD_GENERATOR")
	@SequenceGenerator(name = "CONTROL_COMPONENT_BALLOT_BOX_PAYLOAD_GENERATOR", sequenceName = "CONTROL_COMPONENT_BALLOT_BOX_PAYLOAD_SEQ", allocationSize = 1)
	private Long id;

	private String electionEventId;

	private String ballotBoxId;

	private int nodeId;

	@Column(name = "PAYLOAD")
	private byte[] controlComponentBallotBoxPayload;

	@Version
	private Integer changeControlId;

	protected ControlComponentBallotBoxPayloadEntity() {
	}

	ControlComponentBallotBoxPayloadEntity(final String electionEventId, final String ballotBoxId, final int nodeId,
			final byte[] controlComponentBallotBoxPayload) {
		this.electionEventId = validateUUID(electionEventId);
		this.ballotBoxId = validateUUID(ballotBoxId);
		this.nodeId = nodeId;
		this.controlComponentBallotBoxPayload = checkNotNull(controlComponentBallotBoxPayload);
	}

	public String getElectionEventId() {
		return electionEventId;
	}

	public String getBallotBoxId() {
		return ballotBoxId;
	}

	public int getNodeId() {
		return nodeId;
	}

	public byte[] getControlComponentBallotBoxPayload() {
		return controlComponentBallotBoxPayload;
	}
}
