/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.messagebrokerorchestrator.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.domain.SharedQueue.ELECTION_CONTEXT_REQUEST_PATTERN;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.domain.configuration.ElectionContextResponsePayload;
import ch.post.it.evoting.votingserver.messagebrokerorchestrator.voting.BroadcastCommand;
import ch.post.it.evoting.votingserver.messagebrokerorchestrator.voting.BroadcastCommandProducer;

@RestController
@RequestMapping("api/v1/configuration/electioncontext")
public class ElectionEventContextController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ElectionEventContextController.class);

	private final ObjectMapper objectMapper;
	private final BroadcastCommandProducer broadcastCommandProducer;

	public ElectionEventContextController(final ObjectMapper objectMapper, final BroadcastCommandProducer broadcastCommandProducer) {
		this.broadcastCommandProducer = broadcastCommandProducer;
		this.objectMapper = objectMapper;
	}

	@PostMapping("/electionevent/{electionEventId}")
	public void uploadElectionEventContextPayload(
			@PathVariable
			final String electionEventId,
			@RequestBody
			final ElectionEventContextPayload electionEventContextPayload) throws ExecutionException, InterruptedException, TimeoutException {

		validateUUID(electionEventId);
		checkNotNull(electionEventContextPayload);

		final String correlationId = UUID.randomUUID().toString();

		LOGGER.info("Uploading election event context... [electionEventId: {}, correlationId: {}]", electionEventId, correlationId);

		final BroadcastCommand<ElectionContextResponsePayload> broadcastCommand = new BroadcastCommand.Builder<ElectionContextResponsePayload>()
				.contextId(electionEventId)
				.context(Context.CONFIGURATION_ELECTION_CONTEXT)
				.payload(electionEventContextPayload)
				.pattern(ELECTION_CONTEXT_REQUEST_PATTERN)
				.deserialization(this::deserializePayload)
				.build();

		broadcastCommandProducer.sendMessagesAwaitingNotification(broadcastCommand);

		LOGGER.info("Successfully uploaded election event context. [electionEventId: {}, correlationId: {}]", electionEventId, correlationId);
	}

	private ElectionContextResponsePayload deserializePayload(final byte[] payload) {
		try {
			return objectMapper.readValue(payload, ElectionContextResponsePayload.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}
}
