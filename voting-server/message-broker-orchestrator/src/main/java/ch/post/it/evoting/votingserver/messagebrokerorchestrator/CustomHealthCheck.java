/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.messagebrokerorchestrator;

import static ch.post.it.evoting.votingserver.commons.infrastructure.health.HealthCheck.HealthCheckResult;

import java.time.Instant;
import java.util.EnumMap;
import java.util.Map;

import org.springframework.boot.actuate.amqp.RabbitHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.Status;
import org.springframework.boot.actuate.jdbc.DataSourceHealthIndicator;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.votingserver.commons.infrastructure.health.HealthCheckValidationType;

@RestController
public class CustomHealthCheck {

	private final DataSourceHealthIndicator dataSourceHealthIndicator;
	private final RabbitHealthIndicator rabbitHealthIndicator;

	public CustomHealthCheck(
			@Lazy
			final DataSourceHealthIndicator dataSourceHealthIndicator,
			@Lazy
			final RabbitHealthIndicator rabbitHealthIndicator) {
		this.dataSourceHealthIndicator = dataSourceHealthIndicator;
		this.rabbitHealthIndicator = rabbitHealthIndicator;
	}

	@GetMapping("check")
	public ResponseEntity<Map<HealthCheckValidationType, HealthCheckResult>> health() {
		final Health databaseHealth = dataSourceHealthIndicator.health();
		final boolean databaseStatus = Status.UP.equals(databaseHealth.getStatus());

		final Health rabbitHealth = rabbitHealthIndicator.health();
		final boolean rabbitStatus = Status.UP.equals(rabbitHealth.getStatus());

		final Instant timestamp = Instant.now();

		final Map<HealthCheckValidationType, HealthCheckResult> results = new EnumMap<>(HealthCheckValidationType.class);
		results.put(HealthCheckValidationType.DATABASE, new HealthCheckResult(databaseStatus, databaseHealth.getDetails().toString(), timestamp));
		results.put(HealthCheckValidationType.MESSAGE_BROKER, new HealthCheckResult(rabbitStatus, rabbitHealth.getDetails().toString(), timestamp));

		if (databaseStatus && rabbitStatus) {
			return ResponseEntity.ok(results);
		} else {
			return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(results);
		}
	}
}
