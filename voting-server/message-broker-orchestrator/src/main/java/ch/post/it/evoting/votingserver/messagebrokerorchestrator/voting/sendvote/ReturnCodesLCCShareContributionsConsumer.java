/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.messagebrokerorchestrator.voting.sendvote;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.util.Arrays;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentLCCSharePayload;
import ch.post.it.evoting.domain.voting.sendvote.LongChoiceReturnCodesShare;
import ch.post.it.evoting.votingserver.messagebrokerorchestrator.aggregator.AggregatorService;
import ch.post.it.evoting.votingserver.messagebrokerorchestrator.voting.CommandFacade;

@Component
public class ReturnCodesLCCShareContributionsConsumer {

	private final CommandFacade commandFacade;
	private final ObjectMapper objectMapper;
	private final AggregatorService aggregatorService;

	public ReturnCodesLCCShareContributionsConsumer(final CommandFacade commandFacade,
			final ObjectMapper objectMapper,
			final AggregatorService aggregatorService) {
		this.commandFacade = commandFacade;
		this.objectMapper = objectMapper;
		this.aggregatorService = aggregatorService;
	}

	@RabbitListener(queues = "#{queueNameResolver.get(\"CREATE_LCC_SHARE_RESPONSE_PATTERN\")}")
	public void consumer(final Message message) throws IOException {

		final String correlationId = checkNotNull(message.getMessageProperties().getCorrelationId());

		final byte[] encodedResponse = message.getBody();
		final ControlComponentLCCSharePayload controlComponentLCCSharePayload = objectMapper.readValue(encodedResponse,
				ControlComponentLCCSharePayload.class);

		final LongChoiceReturnCodesShare longChoiceReturnCodesShare = controlComponentLCCSharePayload.getLongChoiceReturnCodesShare();
		final String contextId = String.join("-", Arrays.asList(longChoiceReturnCodesShare.electionEventId(),
				longChoiceReturnCodesShare.verificationCardSetId(),
				longChoiceReturnCodesShare.verificationCardId()));
		final int nodeId = longChoiceReturnCodesShare.nodeId();

		commandFacade.saveResponse(encodedResponse, correlationId, contextId, Context.VOTING_RETURN_CODES_CREATE_LCC_SHARE, nodeId);

		aggregatorService.notifyPartialResponseReceived(correlationId, contextId);
	}
}
