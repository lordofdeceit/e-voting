/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.messagebrokerorchestrator.voting.confirmvote;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.domain.SharedQueue.CREATE_LVCC_SHARE_HASH_REQUEST_PATTERN;
import static ch.post.it.evoting.domain.SharedQueue.VERIFY_LVCC_SHARE_HASH_REQUEST_PATTERN;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableBigInteger;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponenthlVCCPayload;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponentlVCCSharePayload;
import ch.post.it.evoting.domain.voting.confirmvote.VotingServerConfirmPayload;
import ch.post.it.evoting.votingserver.messagebrokerorchestrator.voting.BroadcastCommand;
import ch.post.it.evoting.votingserver.messagebrokerorchestrator.voting.BroadcastCommandProducer;

@RestController
public class LongVoteCastReturnCodesSharesController {

	private static final Logger LOGGER = LoggerFactory.getLogger(LongVoteCastReturnCodesSharesController.class);

	private final ObjectMapper objectMapper;
	private final BroadcastCommandProducer broadcastProducerService;
	private final Hash hash;

	public LongVoteCastReturnCodesSharesController(final ObjectMapper objectMapper, final BroadcastCommandProducer broadcastProducerService,
			Hash hash) {
		this.objectMapper = objectMapper;
		this.broadcastProducerService = broadcastProducerService;
		this.hash = hash;
	}

	private ControlComponentlVCCSharePayload deserializeControlComponentlVCCSharePayload(final byte[] payload) {
		try {
			return objectMapper.readValue(payload, ControlComponentlVCCSharePayload.class);
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	@PostMapping("/api/v1.1/voting/confirmvote/longvotecastreturncodeshare/electionevent/{electionEventId}/verificationCardSetId/{verificationCardSetId}/verificationCardId/{verificationCardId}")
	public ResponseEntity<List<ControlComponentlVCCSharePayload>> getLongVoteCastReturnCodesContributions(
			@PathVariable
			final String electionEventId,
			@PathVariable
			final String verificationCardSetId,
			@PathVariable
			final String verificationCardId,
			@RequestBody
			final VotingServerConfirmPayload votingServerConfirmPayload)
			throws ExecutionException, InterruptedException, TimeoutException {

		validateUUID(electionEventId);
		validateUUID(verificationCardId);
		checkNotNull(votingServerConfirmPayload);
		LOGGER.info("Received request to compute LVCC share [electionEventId : {}, verificationCardSetId : {}, verificationCardId : {}]",
				electionEventId, verificationCardSetId, verificationCardId);

		final BigInteger confirmationKeyValue = votingServerConfirmPayload.getConfirmationKey().element().getValue();
		final String confirmationKeyAsString = Base64.getEncoder().encodeToString(hash.recursiveHash(HashableBigInteger.from(confirmationKeyValue)));


		final String contextId = String.join("-", Arrays.asList(electionEventId, verificationCardSetId, verificationCardId, confirmationKeyAsString));

		final BroadcastCommand<ControlComponenthlVCCPayload> createLVCCCommand = new BroadcastCommand.Builder<ControlComponenthlVCCPayload>()
				.contextId(contextId)
				.context(Context.VOTING_RETURN_CODES_CREATE_LVCC_SHARE_HASH)
				.payload(votingServerConfirmPayload)
				.pattern(CREATE_LVCC_SHARE_HASH_REQUEST_PATTERN)
				.deserialization(this::deserializeHashPayload)
				.build();

		final List<ControlComponenthlVCCPayload> longReturnCodesShareHashPayloads =
				broadcastProducerService.sendMessagesAwaitingNotification(createLVCCCommand);

		LOGGER.info("Received Long Vote Cast Return Codes Share hashes [electionEventId : {}, verificationCardSetId : {}, verificationCardId : {}]",
				electionEventId, verificationCardSetId, verificationCardId);

		final BroadcastCommand<ControlComponentlVCCSharePayload> verifyLVCCCommand = new BroadcastCommand.Builder<ControlComponentlVCCSharePayload>()
				.contextId(contextId)
				.context(Context.VOTING_RETURN_CODES_VERIFY_LVCC_SHARE_HASH)
				.payload(longReturnCodesShareHashPayloads)
				.pattern(VERIFY_LVCC_SHARE_HASH_REQUEST_PATTERN)
				.deserialization(this::deserializeControlComponentlVCCSharePayload)
				.build();

		final List<ControlComponentlVCCSharePayload> controlComponentlVCCSharePayloads =
				broadcastProducerService.sendMessagesAwaitingNotification(verifyLVCCCommand);

		if (controlComponentlVCCSharePayloads.stream().allMatch(ControlComponentlVCCSharePayload::isVerified)) {
			return ResponseEntity.ok(controlComponentlVCCSharePayloads);
		} else {
			return ResponseEntity.badRequest().body(controlComponentlVCCSharePayloads);
		}
	}

	private ControlComponenthlVCCPayload deserializeHashPayload(final byte[] payload) {
		try {
			return objectMapper.readValue(payload, ControlComponenthlVCCPayload.class);
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}

}
