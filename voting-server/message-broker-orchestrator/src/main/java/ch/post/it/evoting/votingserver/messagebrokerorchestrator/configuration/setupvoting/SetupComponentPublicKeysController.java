/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.messagebrokerorchestrator.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.domain.SharedQueue.SETUP_COMPONENT_PUBLIC_KEYS_REQUEST_PATTERN;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.domain.configuration.SetupComponentPublicKeysResponsePayload;
import ch.post.it.evoting.votingserver.messagebrokerorchestrator.voting.BroadcastCommand;
import ch.post.it.evoting.votingserver.messagebrokerorchestrator.voting.BroadcastCommandProducer;

@RestController
@RequestMapping("/api/v1/configuration/setupkeys")
public class SetupComponentPublicKeysController {
	private static final Logger LOGGER = LoggerFactory.getLogger(SetupComponentPublicKeysController.class);

	private final ObjectMapper objectMapper;
	private final BroadcastCommandProducer broadcastCommandProducer;

	public SetupComponentPublicKeysController(final ObjectMapper objectMapper, final BroadcastCommandProducer broadcastCommandProducer) {
		this.objectMapper = objectMapper;
		this.broadcastCommandProducer = broadcastCommandProducer;
	}

	@PostMapping("/electionevent/{electionEventId}")
	public void uploadSetupComponentPublicKeys(
			@PathVariable
			final String electionEventId,
			@RequestBody
			final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload)
			throws ExecutionException, InterruptedException, TimeoutException {

		validateUUID(electionEventId);
		checkNotNull(setupComponentPublicKeysPayload);
		checkArgument(electionEventId.equals(setupComponentPublicKeysPayload.getElectionEventId()), "Election event id mismatch.");

		final String contextId = electionEventId;
		final String correlationId = UUID.randomUUID().toString();

		LOGGER.info("Uploading setup component public keys... [contextId: {}, correlationId: {}]", contextId, correlationId);

		final BroadcastCommand<SetupComponentPublicKeysResponsePayload> broadcastCommand = new BroadcastCommand.Builder<SetupComponentPublicKeysResponsePayload>()
				.contextId(contextId)
				.context(Context.CONFIGURATION_SETUP_COMPONENT_PUBLIC_KEYS)
				.payload(setupComponentPublicKeysPayload)
				.pattern(SETUP_COMPONENT_PUBLIC_KEYS_REQUEST_PATTERN)
				.deserialization(this::deserializePayload)
				.build();

		broadcastCommandProducer.sendMessagesAwaitingNotification(broadcastCommand);

		LOGGER.info("Successfully uploaded setup component public keys. [contextId: {}, correlationId: {}]", contextId, correlationId);
	}

	private SetupComponentPublicKeysResponsePayload deserializePayload(final byte[] payload) {
		try {
			return objectMapper.readValue(payload, SetupComponentPublicKeysResponsePayload.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}
}
