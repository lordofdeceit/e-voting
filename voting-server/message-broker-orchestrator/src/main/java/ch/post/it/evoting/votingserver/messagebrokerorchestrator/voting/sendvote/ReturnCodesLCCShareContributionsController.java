/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.messagebrokerorchestrator.voting.sendvote;

import static ch.post.it.evoting.domain.SharedQueue.CREATE_LCC_SHARE_REQUEST_PATTERN;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.domain.voting.sendvote.CombinedControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentLCCSharePayload;
import ch.post.it.evoting.votingserver.messagebrokerorchestrator.voting.BroadcastCommand;
import ch.post.it.evoting.votingserver.messagebrokerorchestrator.voting.BroadcastCommandProducer;

@RestController
@RequestMapping("/api/v1/voting/sendvote")
public class ReturnCodesLCCShareContributionsController {

	private final ObjectMapper objectMapper;
	private final BroadcastCommandProducer broadcastCommandProducer;

	public ReturnCodesLCCShareContributionsController(final ObjectMapper objectMapper, final BroadcastCommandProducer broadcastCommandProducer) {
		this.objectMapper = objectMapper;
		this.broadcastCommandProducer = broadcastCommandProducer;
	}

	@PostMapping("/lccshare/electionevent/{electionEventId}/verificationCardSetId/{verificationCardSetId}/verificationCardId/{verificationCardId}")
	public List<ControlComponentLCCSharePayload> getLongChoiceReturnCodesContributions(
			@PathVariable
			final String electionEventId,
			@PathVariable
			final String verificationCardSetId,
			@PathVariable
			final String verificationCardId,
			@RequestBody
			final CombinedControlComponentPartialDecryptPayload combinedControlComponentPartialDecryptPayload)
			throws ExecutionException, InterruptedException, TimeoutException {
		final String contextId = String.join("-", Arrays.asList(electionEventId, verificationCardSetId, verificationCardId));

		final BroadcastCommand<ControlComponentLCCSharePayload> broadcastCommand = new BroadcastCommand.Builder<ControlComponentLCCSharePayload>()
				.contextId(contextId)
				.context(Context.VOTING_RETURN_CODES_CREATE_LCC_SHARE)
				.payload(combinedControlComponentPartialDecryptPayload)
				.pattern(CREATE_LCC_SHARE_REQUEST_PATTERN)
				.deserialization(this::deserializePayload)
				.build();

		return broadcastCommandProducer.sendMessagesAwaitingNotification(broadcastCommand);
	}

	private ControlComponentLCCSharePayload deserializePayload(final byte[] payload) {
		try {
			return objectMapper.readValue(payload, ControlComponentLCCSharePayload.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}
}
