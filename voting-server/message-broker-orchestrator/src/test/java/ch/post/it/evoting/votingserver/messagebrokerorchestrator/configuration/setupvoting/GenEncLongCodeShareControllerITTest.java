/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.messagebrokerorchestrator.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.domain.SharedQueue.GEN_ENC_LONG_CODE_SHARES_REQUEST_PATTERN;
import static ch.post.it.evoting.domain.SharedQueue.GEN_ENC_LONG_CODE_SHARES_RESPONSE_PATTERN;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.domain.configuration.setupvoting.ComputingStatus;
import ch.post.it.evoting.votingserver.messagebrokerorchestrator.BroadcastIntegrationTestService;
import ch.post.it.evoting.votingserver.messagebrokerorchestrator.IntegrationTestSupport;

@DisplayName("GenEncLongCodeShareController integration test")
class GenEncLongCodeShareControllerITTest extends IntegrationTestSupport {

	@Autowired
	BroadcastIntegrationTestService broadcastIntegrationTestService;

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	private WebTestClient webTestClient;

	@AfterEach
	void cleanUp() {
		broadcastIntegrationTestService.cleanUpMessageBrokerOrchestrator();
	}

	@Test
	@DisplayName("Simulate GenEncLongCodeShare Compute Request ")
	void firstTimeCommand() throws IOException {

		final Context context = Context.CONFIGURATION_RETURN_CODES_GEN_ENC_LONG_CODE_SHARES;

		final CountDownLatch webClientCountDownLatch = new CountDownLatch(1);
		final Resource payloadsResource = new ClassPathResource(
				"configuration/setupvoting/enclongcodeshares/setupComponentVerificationDataPayload.0.json");

		final SetupComponentVerificationDataPayload requestPayload = objectMapper.readValue(payloadsResource.getFile(),
				SetupComponentVerificationDataPayload.class);

		final ExecutorService executorService = Executors.newFixedThreadPool(1, new CustomizableThreadFactory("http-pool-"));

		String electionEventId = requestPayload.getElectionEventId();
		String verificationCardSetId = requestPayload.getVerificationCardSetId();
		String chunkId = String.valueOf(requestPayload.getChunkId());

		//Send the HTTP request in a separate thread and wait for the results.
		executorService.execute(() -> {
			webTestClient.put()
					.uri(uriBuilder -> uriBuilder
							.path("/api/v1/configuration/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/chunk/{chunkId}/computegenenclongcodeshares")
							.build(electionEventId, verificationCardSetId, chunkId))
					.accept(MediaType.APPLICATION_JSON)
					.bodyValue(requestPayload)
					.exchange()
					.expectStatus().isCreated();

			webClientCountDownLatch.countDown();
		});

		final String contextId = String.join("-", electionEventId, verificationCardSetId, chunkId);

		broadcastIntegrationTestService.awaitBroadcastRequestsSaved(context, contextId, 30, SECONDS);

		List<ControlComponentCodeSharesPayload> controlComponentCodeSharesPayloads = NODE_IDS.stream()
				.map(nodeId -> {
					try {
						return objectMapper.readValue((new ClassPathResource(
										"configuration/setupvoting/enclongcodeshares/controlComponentCodeSharesPayloads.0.node." + nodeId
												+ ".json")).getFile(),
								ControlComponentCodeSharesPayload.class);
					} catch (IOException e) {
						throw new UncheckedIOException(e);
					}
				}).toList();

		controlComponentCodeSharesPayloads.forEach(payload ->
				broadcastIntegrationTestService.respondWithForGivenNodeId(GEN_ENC_LONG_CODE_SHARES_REQUEST_PATTERN,
						GEN_ENC_LONG_CODE_SHARES_RESPONSE_PATTERN, nodeId -> payload, payload.getNodeId()));

		String chunkCount = "1";
		await()
				.atMost(30, TimeUnit.SECONDS)
				.pollDelay(100, TimeUnit.MILLISECONDS)
				.until(() -> Objects.equals((webTestClient.get()
						.uri(uriBuilder -> uriBuilder
								.path("/api/v1/configuration/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/chunkcount/{chunkCount}/status")
								.build(electionEventId, verificationCardSetId, chunkCount))
						.accept(MediaType.APPLICATION_JSON)
						.exchange()
						.expectBody(ComputingStatus.class)
						.returnResult()
						.getResponseBody()
				), ComputingStatus.COMPUTED));

		List<ControlComponentCodeSharesPayload> downloadedControlComponentCodeSharesPayloads = webTestClient.get()
				.uri(uriBuilder -> uriBuilder
						.path("/api/v1/configuration/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/chunk/{chunkId}/download")
						.build(electionEventId, verificationCardSetId, chunkId))
				.accept(MediaType.APPLICATION_JSON)
				.exchange()
				.expectBodyList(ControlComponentCodeSharesPayload.class)
				.returnResult()
				.getResponseBody();

		Assertions.assertEquals(controlComponentCodeSharesPayloads, downloadedControlComponentCodeSharesPayloads);

	}
}
