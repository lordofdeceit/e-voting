/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.messagebrokerorchestrator.voting.sendvote;

import static ch.post.it.evoting.domain.SharedQueue.PARTIAL_DECRYPT_PCC_REQUEST_PATTERN;
import static ch.post.it.evoting.domain.SharedQueue.PARTIAL_DECRYPT_PCC_RESPONSE_PATTERN;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.commandmessaging.Context;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.VotingServerEncryptedVotePayload;
import ch.post.it.evoting.votingserver.messagebrokerorchestrator.BroadcastIntegrationTestService;
import ch.post.it.evoting.votingserver.messagebrokerorchestrator.IntegrationTestSupport;

@DisplayName("ReturnCodesPartialDecryptContributionsControllerIT end to end integration test")
class ReturnCodesPartialDecryptContributionsControllerIT extends IntegrationTestSupport {

	@Autowired
	BroadcastIntegrationTestService broadcastIntegrationTestService;

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	private WebTestClient webTestClient;

	@AfterEach
	void cleanUp() {
		broadcastIntegrationTestService.cleanUpMessageBrokerOrchestrator();
	}

	@Test
	@DisplayName("Process VotingServerEncryptedVotePayload, happy path")
	void firstTimeCommand() throws IOException, InterruptedException {

		final String contextId = String.join("-", Arrays.asList(electionEventId, verificationCardSetId, verificationCardId));
		final Context context = Context.VOTING_RETURN_CODES_PARTIAL_DECRYPT_PCC;

		final CountDownLatch webClientCountDownLatch = new CountDownLatch(1);

		final Resource payloadsResource = new ClassPathResource("voting/sendvote/voting-server-encrypted-vote-payload.json");

		final VotingServerEncryptedVotePayload requestPayload = objectMapper.readValue(payloadsResource.getFile(),
				VotingServerEncryptedVotePayload.class);

		final ExecutorService executorService = Executors.newFixedThreadPool(1, new CustomizableThreadFactory("http-pool-"));

		//Send the HTTP request in a separate thread and wait for the results.
		executorService.execute(() -> {
			webTestClient.post()
					.uri(uriBuilder -> uriBuilder
							.path("/api/v1/voting/sendvote/partialdecrypt/electionevent/")
							.pathSegment(electionEventId, "verificationCardSetId", verificationCardSetId, "verificationCardId", verificationCardId)
							.build(1L))
					.accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
					.bodyValue(requestPayload)
					.exchange()
					.expectStatus().isOk()
					.expectBody()
					.jsonPath("$..partiallyDecryptedEncryptedPCC").hasJsonPath()
					.jsonPath("$..nodeId").value(Matchers.hasSize(4));

			webClientCountDownLatch.countDown();
		});

		broadcastIntegrationTestService.awaitBroadcastRequestsSaved(context, contextId, 30, SECONDS);

		final Resource ccPayloadsResource = new ClassPathResource("voting/sendvote/control-component-partial-decrypt-payloads.json");
		final List<ControlComponentPartialDecryptPayload> responsePayloads = objectMapper
				.readValue(ccPayloadsResource.getFile(), new TypeReference<List<ControlComponentPartialDecryptPayload>>() {
				});

		broadcastIntegrationTestService
				.respondWith(PARTIAL_DECRYPT_PCC_REQUEST_PATTERN, PARTIAL_DECRYPT_PCC_RESPONSE_PATTERN, nodeId -> responsePayloads.get(nodeId - 1));

		assertTrue(webClientCountDownLatch.await(30, SECONDS));
	}

}
