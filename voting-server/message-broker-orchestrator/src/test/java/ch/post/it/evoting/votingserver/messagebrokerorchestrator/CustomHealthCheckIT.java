/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.messagebrokerorchestrator;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

@DisplayName("Checks custom health endpoint")
class CustomHealthCheckIT extends IntegrationTestSupport {

	@Autowired
	private WebTestClient webTestClient;

	@Test
	@DisplayName("happy path")
	void healthCheck() {
		webTestClient.get()
				.uri(uriBuilder -> uriBuilder
						.path("/check")
						.build())
				.accept(MediaType.APPLICATION_JSON)
				.exchange()
				.expectStatus()
				.isOk();
	}
}
