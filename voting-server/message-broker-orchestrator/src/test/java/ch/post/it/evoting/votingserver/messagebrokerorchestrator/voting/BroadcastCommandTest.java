/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.messagebrokerorchestrator.voting;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.StringContains.containsStringIgnoringCase;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.function.Function;

import org.junit.jupiter.api.Test;

import ch.post.it.evoting.commandmessaging.Context;

class BroadcastCommandTest {

	private final String contextId = "contextId";
	private final Object payload = new Object();
	private final String pattern = "queuePattern";

	private final Function<byte[], Object> deserialization = (bytes) -> new Object();

	@Test
	void testBuildWhenContextIdNotSet() {
		final BroadcastCommand.Builder<Object> builder = new BroadcastCommand.Builder<>()
				.context(Context.CONFIGURATION_ELECTION_CONTEXT)
				.payload(payload)
				.pattern(pattern)
				.deserialization(deserialization);

		final Exception ex = assertThrows(NullPointerException.class, builder::build);

		assertNotNull(ex);
		assertThat(ex.getMessage(), containsStringIgnoringCase("contextId"));
	}

	@Test
	void testBuildWhenContextNotSet() {
		final BroadcastCommand.Builder<Object> builder = new BroadcastCommand.Builder<>()
				.contextId(contextId)
				.payload(payload)
				.pattern(pattern)
				.deserialization(deserialization);

		final Exception ex = assertThrows(NullPointerException.class, builder::build);

		assertNotNull(ex);
		assertThat(ex.getMessage(), containsStringIgnoringCase("Context"));
	}

	@Test
	void testBuildWhenRequestPayloadNotSet() {
		final BroadcastCommand.Builder<Object> builder = new BroadcastCommand.Builder<>()
				.contextId(contextId)
				.context(Context.CONFIGURATION_ELECTION_CONTEXT)
				.pattern(pattern)
				.deserialization(deserialization);

		final Exception ex = assertThrows(NullPointerException.class, builder::build);

		assertNotNull(ex);
		assertThat(ex.getMessage(), containsStringIgnoringCase("Payload"));
	}

	@Test
	void testBuildWhenQueuePatternNotSet() {
		final BroadcastCommand.Builder<Object> builder = new BroadcastCommand.Builder<>()
				.contextId(contextId)
				.context(Context.CONFIGURATION_ELECTION_CONTEXT)
				.payload(payload)
				.deserialization(deserialization);

		final Exception ex = assertThrows(NullPointerException.class, builder::build);

		assertNotNull(ex);
		assertThat(ex.getMessage(), containsStringIgnoringCase("Pattern"));
	}

	@Test
	void testBuildWhenAllSet() {
		final BroadcastCommand.Builder<Object> builder = new BroadcastCommand.Builder<>()
				.contextId(contextId)
				.context(Context.CONFIGURATION_ELECTION_CONTEXT)
				.payload(payload)
				.pattern(pattern)
				.deserialization(deserialization);

		final BroadcastCommand<Object> broadcastCommand = assertDoesNotThrow(builder::build);

		assertThat("Task is null", broadcastCommand, is(not(nullValue())));
		assertThat("Task contextId is different", broadcastCommand.getContextId(), is(equalTo(contextId)));
		assertThat("Task context is different", broadcastCommand.getContext(), is(equalTo(Context.CONFIGURATION_ELECTION_CONTEXT)));
		assertThat("Task payload is different", broadcastCommand.getPayload(), is(equalTo(payload)));
		assertThat("Task pattern is different", broadcastCommand.getPattern(), is(equalTo(pattern)));
		assertThat("Task deserialization is different", broadcastCommand.getDeserialization(), is(equalTo(deserialization)));
	}
}
