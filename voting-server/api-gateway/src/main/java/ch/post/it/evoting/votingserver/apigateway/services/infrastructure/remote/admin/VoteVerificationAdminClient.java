/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.apigateway.services.infrastructure.remote.admin;

import javax.ws.rs.core.MediaType;

import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RestClientInterceptor;
import ch.post.it.evoting.votingserver.commons.ui.Constants;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * The Interface of vote verification client for admin.
 */
public interface VoteVerificationAdminClient {

	@POST("api/v1/configuration/returncodesmappingtable/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}")
	@Headers({ "Accept:" + MediaType.APPLICATION_JSON, "Transfer-Encoding:chunked" })
	Call<Void> saveReturnCodesMappingTable(
			@Path(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@Path(Constants.PARAMETER_VALUE_VERIFICATION_CARD_SET_ID)
			final String verificationCardSetId,
			@Body
			final RequestBody setupComponentCMTablePayload,
			@Header(RestClientInterceptor.HEADER_ORIGINATOR)
			final String originator,
			@Header(RestClientInterceptor.HEADER_SIGNATURE)
			final String signature,
			@Header(Constants.PARAMETER_X_FORWARDED_FOR)
			final String xForwardedFor,
			@Header(Constants.PARAMETER_X_REQUEST_ID)
			final String trackingId);

	@POST("api/v1/configuration/electioncontext/electionevent/{electionEventId}")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<Void> uploadElectionEventContext(
			@Path(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@Body
			final RequestBody electionEventContextPayload,
			@Header(RestClientInterceptor.HEADER_ORIGINATOR)
			final String originator,
			@Header(RestClientInterceptor.HEADER_SIGNATURE)
			final String signature,
			@Header(Constants.PARAMETER_X_FORWARDED_FOR)
			final String xForwardedFor,
			@Header(Constants.PARAMETER_X_REQUEST_ID)
			final String trackingId);

	@POST("api/v1/configuration/setupkeys/electionevent/{electionEventId}")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<Void> uploadSetupComponentPublicKeys(
			@Path(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@Body
			final RequestBody setupComponentPublicKeysPayload,
			@Header(RestClientInterceptor.HEADER_ORIGINATOR)
			final String originator,
			@Header(RestClientInterceptor.HEADER_SIGNATURE)
			final String signature,
			@Header(Constants.PARAMETER_X_FORWARDED_FOR)
			final String xForwardedFor,
			@Header(Constants.PARAMETER_X_REQUEST_ID)
			final String trackingId);

	@POST("api/v1/configuration/setupcomponentverificationcardkeystores/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}")
	@Headers({ "Accept:" + MediaType.APPLICATION_JSON, "Transfer-Encoding:chunked" })
	Call<Void> uploadSetupComponentVerificationCardKeystores(
			@Path(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@Path(Constants.PARAMETER_VALUE_VERIFICATION_CARD_SET_ID)
			final String verificationCardSetId,
			@Body
			final RequestBody setupComponentVerificationCardKeystoresPayload,
			@Header(RestClientInterceptor.HEADER_ORIGINATOR)
			final String originator,
			@Header(RestClientInterceptor.HEADER_SIGNATURE)
			final String signature,
			@Header(Constants.PARAMETER_X_FORWARDED_FOR)
			final String xForwardedFor,
			@Header(Constants.PARAMETER_X_REQUEST_ID)
			final String trackingId);
}
