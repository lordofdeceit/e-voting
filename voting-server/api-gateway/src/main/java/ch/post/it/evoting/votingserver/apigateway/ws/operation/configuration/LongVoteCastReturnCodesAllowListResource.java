/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.apigateway.ws.operation.configuration;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import java.io.InputStream;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.post.it.evoting.votingserver.apigateway.services.infrastructure.remote.controlcomponents.MessageBrokerOrchestratorClient;
import ch.post.it.evoting.votingserver.apigateway.ws.RestApplication;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.InputStreamTypedOutput;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RestClientInterceptor;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RetrofitConsumer;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RetrofitException;

import okhttp3.RequestBody;
import retrofit2.http.Body;

/**
 * * API to upload the long vote cast return codes allow list to the control components.
 */
@Stateless(name = "ag-LongVoteCastReturnCodesAllowListResource")
@Path("/mo")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class LongVoteCastReturnCodesAllowListResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(LongVoteCastReturnCodesAllowListResource.class);

	static final String LONG_VOTE_ALLOW_LIST_PATH = "api/v1/configuration/setupvoting/longvotecastreturncodesallowlist/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}";

	private final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient;

	@Inject
	public LongVoteCastReturnCodesAllowListResource(final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient) {
		this.messageBrokerOrchestratorClient = messageBrokerOrchestratorClient;
	}

	/**
	 * Uploads the long vote cast return codes allow list to the message broker orchestrator.
	 *
	 * @param electionEventId       the election event id
	 * @param verificationCardSetId the verification card set id
	 * @param originator            the originator
	 * @param signature             the signature
	 * @return the status of the request
	 */
	@POST
	@Path(LONG_VOTE_ALLOW_LIST_PATH)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response uploadLongVoteCastReturnCodesAllowList(
			@PathParam(RestApplication.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@PathParam(RestApplication.PARAMETER_VALUE_VERIFICATION_CARD_SET_ID)
			final String verificationCardSetId,
			@HeaderParam(RestClientInterceptor.HEADER_ORIGINATOR)
			final String originator,
			@HeaderParam(RestClientInterceptor.HEADER_SIGNATURE)
			final String signature,
			@NotNull
			@Body
			final InputStream longVoteCastReturnCodesAllowListRequestPayload,
			@Context
			final HttpServletRequest request) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);

		final RequestBody body = new InputStreamTypedOutput(MediaType.APPLICATION_JSON, longVoteCastReturnCodesAllowListRequestPayload);

		try {
			RetrofitConsumer.processResponse(
					messageBrokerOrchestratorClient.uploadLongVoteCastReturnCodesAllowList(electionEventId, verificationCardSetId, body));

			return Response.ok().build();
		} catch (final RetrofitException e) {
			LOGGER.error("Error trying to upload long vote cast return codes allow list.", e);

			return Response.status(e.getHttpCode()).build();
		}
	}
}
