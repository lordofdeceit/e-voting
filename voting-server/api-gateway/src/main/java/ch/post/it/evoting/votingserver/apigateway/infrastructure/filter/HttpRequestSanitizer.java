/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.apigateway.infrastructure.filter;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.stream.Stream;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.commons.io.IOUtils;

public class HttpRequestSanitizer extends HttpServletRequestWrapper {

	private static final String CONTENT_TYPE = "Content-Type";
	private static final String TEXT_CSV = "text/csv";
	private final HttpServletRequest request;

	/**
	 * Constructs a request object wrapping the given request.
	 *
	 * @param request request to wrap
	 * @throws IllegalArgumentException if the request is null
	 */
	public HttpRequestSanitizer(final HttpServletRequest request) {
		super(request);
		this.request = request;
	}

	@Override
	public String getHeader(final String name) {
		final String header = request.getHeader(name);
		HtmlSanitizer.checkSanitized(header);
		return header;
	}

	@Override
	public ServletInputStream getInputStream() throws IOException {

		final String contentType = request.getHeader(CONTENT_TYPE);
		final String input = IOUtils.toString(request.getInputStream(), StandardCharsets.UTF_8);

		if (contentType != null && contentType.contains(APPLICATION_JSON)) {
			JsonSanitizer.checkSanitized(input);

		} else if (contentType != null && contentType.contains(TEXT_CSV)) {
			CsvSanitizer.checkSanitized(input);

		} else {
			HtmlSanitizer.checkSanitized(input);
		}

		return new ServletInputStream() {
			private final ByteArrayInputStream bais = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));

			@Override
			public int read() {
				return bais.read();
			}
		};
	}

	@Override
	public String getQueryString() {
		final String query = request.getQueryString();

		if (query == null) {
			return null;
		}

		final String sanitizedQuery = Stream.of(query)
				.map(String::trim)
				.filter(s -> !s.isBlank() && !s.startsWith("="))
				.flatMap(s -> Stream.of(s.split("&")))
				.map(s -> s.split("=", -1))
				.filter(s -> s.length == 1 || s.length == 2)
				.map(s -> {
					final String key = URLDecoder.decode(s[0], StandardCharsets.UTF_8);
					final String value = s.length == 2 ? URLDecoder.decode(s[1], StandardCharsets.UTF_8) : "";

					HtmlSanitizer.checkSanitized(key);
					HtmlSanitizer.checkSanitized(value);

					return String.join("=", key, value);
				})
				.reduce((s1, s2) -> String.join("&", s1, s2))
				.orElse("");

		if (!sanitizedQuery.equals(query)) {
			final String loggedQuery = query.replaceAll("[\n\r\t]", "_");
			throw new IllegalArgumentException(String.format("Data does not pass sanitization. [input=%s, sanitize=%s]", loggedQuery, sanitizedQuery));
		}

		return sanitizedQuery;
	}

	@Override
	public String getParameter(final String name) {
		final String parameter = request.getParameter(name);
		HtmlSanitizer.checkSanitized(parameter);
		return parameter;
	}

	@Override
	public String[] getParameterValues(final String name) {
		final String[] parameters = request.getParameterValues(name);
		Stream.of(parameters).forEach(HtmlSanitizer::checkSanitized);
		return parameters;
	}

	@Override
	public Map<String, String[]> getParameterMap() {
		final Map<String, String[]> parameters = request.getParameterMap();
		parameters.forEach((key, value) -> {
			HtmlSanitizer.checkSanitized(key);
			Stream.of(value).forEach(HtmlSanitizer::checkSanitized);
		});
		return parameters;
	}
}
