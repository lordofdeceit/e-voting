/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.apigateway.ws.operation.admin;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import java.io.UncheckedIOException;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.domain.configuration.SetupComponentVerificationCardKeystoresPayload;
import ch.post.it.evoting.votingserver.apigateway.services.infrastructure.remote.admin.VoteVerificationAdminClient;
import ch.post.it.evoting.votingserver.apigateway.ws.proxy.XForwardedForFactory;
import ch.post.it.evoting.votingserver.apigateway.ws.proxy.XForwardedForFactoryImpl;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.ChunkedRequestBodyCreator;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RestClientInterceptor;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RetrofitConsumer;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RetrofitException;
import ch.post.it.evoting.votingserver.commons.tracking.TrackIdGenerator;

import okhttp3.RequestBody;

/**
 * Web service for handling verification card data resources.
 */
@Stateless(name = "ag-VerificationCardDataResource")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Path(VerificationCardDataResource.RESOURCE_PATH)
public class VerificationCardDataResource {

	static final String RESOURCE_PATH = "/vv/verificationcarddata";
	private static final String QUERY_PARAMETER_ELECTION_EVENT_ID = "electionEventId";
	private static final String QUERY_PARAMETER_VERIFICATION_CARD_SET_ID = "verificationCardSetId";
	private static final Logger LOGGER = LoggerFactory.getLogger(VerificationCardDataResource.class);

	private final TrackIdGenerator trackIdGenerator;
	private final VoteVerificationAdminClient voteVerificationAdminClient;
	private final ObjectMapper objectMapper;
	private final XForwardedForFactory xForwardedForFactory = XForwardedForFactoryImpl.getInstance();

	@Inject
	VerificationCardDataResource(final TrackIdGenerator trackIdGenerator,
			final VoteVerificationAdminClient voteVerificationAdminClient, final ObjectMapper objectMapper) {
		this.trackIdGenerator = trackIdGenerator;
		this.voteVerificationAdminClient = voteVerificationAdminClient;
		this.objectMapper = objectMapper;
	}

	@POST
	@Path("api/v1/configuration/setupcomponentverificationcardkeystores/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response uploadSetupComponentVerificationCardKeystores(
			@PathParam(QUERY_PARAMETER_ELECTION_EVENT_ID)
			final String electionEventId,
			@PathParam(QUERY_PARAMETER_VERIFICATION_CARD_SET_ID)
			final String verificationCardSetId,
			@NotNull
			final SetupComponentVerificationCardKeystoresPayload setupComponentVerificationCardKeystoresPayload,
			@HeaderParam(RestClientInterceptor.HEADER_ORIGINATOR)
			final String originator,
			@HeaderParam(RestClientInterceptor.HEADER_SIGNATURE)
			final String signature,
			@Context
			final HttpServletRequest request) {

		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);

		final String xForwardedFor = xForwardedForFactory.newXForwardedFor(request);
		final String trackingId = trackIdGenerator.generate();

		final RequestBody body;
		try {
			body = ChunkedRequestBodyCreator.forJsonPayload(objectMapper.writeValueAsBytes(setupComponentVerificationCardKeystoresPayload));
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(
					String.format(
							"Failed to serialize SetupComponentVerificationCardKeystoresPayload. [electionEventId: %s, verificationCardSetId: %s]",
							electionEventId, verificationCardSetId), e);
		}

		try {
			RetrofitConsumer.processResponse(
					voteVerificationAdminClient.uploadSetupComponentVerificationCardKeystores(electionEventId, verificationCardSetId, body,
							originator, signature, xForwardedFor, trackingId));

			return Response.ok().build();
		} catch (final RetrofitException e) {
			LOGGER.error(String.format(
					"Error trying request setup component verification card keystores. [electionEventId: %s, verificationCardSetId: %s]",
					electionEventId, verificationCardSetId), e);
			return Response.status(e.getHttpCode()).build();
		}
	}
}
