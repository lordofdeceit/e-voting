/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.apigateway.infrastructure.filter;

import java.text.Normalizer;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.safety.Safelist;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.IntrusionException;

final class HtmlSanitizer {

	private HtmlSanitizer() {
		// Utility class
	}

	/**
	 * Check if the given html input passes sanitization successfully.
	 * <p>
	 * Validation removes null chars and uniformize the unicode chars used. <br/> Then, it shrinks encoded string to its minimalist form to avoid
	 * encoding attack. <br/> Finally, it filters unsafe tags from the string. <br/>
	 * <p>
	 * If the sanitized string does not correspond to the input (meaning some content is unsafe) an exception is thrown.
	 *
	 * @param input to be evaluated.
	 * @throws IllegalArgumentException if the input does not pass sanitization successfully.
	 */
	public static void checkSanitized(final String input) {
		if (input == null || input.isBlank()) {
			return;
		}

		final String sanitize = sanitize(input);

		if (!input.equals(sanitize)) {
			final String loggedInput = input.replaceAll("[\n\r\t]", "_");
			throw new IllegalArgumentException(String.format("Data does not pass sanitization. [input=%s, sanitize=%s]", loggedInput, sanitize));
		}
	}

	/**
	 * Sanitize the input string to prevent injection and XSS attacks.
	 *
	 * @param input string to be evaluated.
	 * @return sanitized data.
	 */
	private static String sanitize(final String input) {
		return Optional.of(input)
				// Avoid null characters
				.map(s -> StringUtils.replace(s, "\0", ""))
				// Normalize
				.map(s -> Normalizer.normalize(s, Normalizer.Form.NFKC))
				// Use the ESAPI library to avoid encoded attacks.
				.map(HtmlSanitizer::canonicalize)
				// Clean out HTML
				.map(s -> Jsoup.clean(s, Safelist.none()))
				.orElseThrow(() -> new IllegalArgumentException("Sanitized data is null."));
	}

	private static String canonicalize(final String s) {
		try {
			return ESAPI.encoder().canonicalize(s);
		} catch (IntrusionException e) {
			throw new IllegalArgumentException("Data does not pass sanitization. See ESAPI exception.", e);
		}
	}
}
