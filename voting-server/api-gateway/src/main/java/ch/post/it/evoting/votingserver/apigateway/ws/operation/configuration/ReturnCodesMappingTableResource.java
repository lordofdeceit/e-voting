/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.apigateway.ws.operation.configuration;

import java.io.UncheckedIOException;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.domain.configuration.SetupComponentCMTablePayload;
import ch.post.it.evoting.votingserver.apigateway.services.infrastructure.remote.admin.VoteVerificationAdminClient;
import ch.post.it.evoting.votingserver.apigateway.ws.RestApplication;
import ch.post.it.evoting.votingserver.apigateway.ws.proxy.XForwardedForFactory;
import ch.post.it.evoting.votingserver.apigateway.ws.proxy.XForwardedForFactoryImpl;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.ChunkedRequestBodyCreator;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RestClientInterceptor;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RetrofitConsumer;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RetrofitException;
import ch.post.it.evoting.votingserver.commons.tracking.TrackIdGenerator;

import jakarta.validation.constraints.NotNull;
import okhttp3.RequestBody;

/**
 * Web service for handling return codes mapping table resources
 */
@Stateless(name = "ag-ReturnCodesMappingTableResource")
@Path("/vv")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class ReturnCodesMappingTableResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReturnCodesMappingTableResource.class);

	private final XForwardedForFactory xForwardedForFactory = XForwardedForFactoryImpl.getInstance();

	@Inject
	private TrackIdGenerator trackIdGenerator;

	@Inject
	private VoteVerificationAdminClient voteVerificationAdminClient;

	@Inject
	private ObjectMapper objectMapper;

	@POST
	@Path("api/v1/configuration/returncodesmappingtable/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveReturnCodesMappingTable(
			@PathParam(RestApplication.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@PathParam(RestApplication.PARAMETER_VALUE_VERIFICATION_CARD_SET_ID)
			final String verificationCardSetId,
			@NotNull
			final SetupComponentCMTablePayload setupComponentCMTablePayload,
			@HeaderParam(RestClientInterceptor.HEADER_ORIGINATOR)
			final String originator,
			@HeaderParam(RestClientInterceptor.HEADER_SIGNATURE)
			final String signature,
			@Context
			final HttpServletRequest request) {

		final String xForwardedFor = xForwardedForFactory.newXForwardedFor(request);
		final String trackingId = trackIdGenerator.generate();

		final byte[] setupComponentCMTablePayloadBytes;
		try {
			setupComponentCMTablePayloadBytes = objectMapper.writeValueAsBytes(setupComponentCMTablePayload);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(
					String.format("Failed to serialize SetupComponentCMTablePayload. [electionEventId: %s, verificationCardSetId: %s]",
							electionEventId, verificationCardSetId), e);
		}
		final RequestBody body = ChunkedRequestBodyCreator.forJsonPayload(setupComponentCMTablePayloadBytes);

		try {
			RetrofitConsumer.processResponse(
					voteVerificationAdminClient.saveReturnCodesMappingTable(electionEventId, verificationCardSetId, body, originator, signature,
							xForwardedFor, trackingId));
			return Response.ok().build();
		} catch (final RetrofitException e) {
			LOGGER.error("Error trying request return codes mapping table.", e);
			return Response.status(e.getHttpCode()).build();
		}
	}
}
