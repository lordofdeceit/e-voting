/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.apigateway.ws.operation.configuration;

import java.io.IOException;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import ch.post.it.evoting.cryptoprimitives.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.domain.configuration.setupvoting.ComputingStatus;
import ch.post.it.evoting.votingserver.apigateway.services.infrastructure.remote.controlcomponents.MessageBrokerOrchestratorClient;
import ch.post.it.evoting.votingserver.apigateway.ws.RestApplication;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RestClientInterceptor;

import retrofit2.Response;
import retrofit2.http.Body;

@Stateless(name = "ag-GenEncLongCodeSharesResource")
@Path("/mo")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class GenEncLongCodeSharesResource {

	@Inject
	private MessageBrokerOrchestratorClient messageBrokerOrchestratorClient;

	@PUT
	@Path("api/v1/configuration/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/chunk/{chunkId}/computegenenclongcodeshares")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response<Void> computeGenEncLongCodeShares(
			@PathParam(RestApplication.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@PathParam(RestApplication.PARAMETER_VALUE_VERIFICATION_CARD_SET_ID)
			final String verificationCardSetId,
			@PathParam(RestApplication.PARAMETER_VALUE_CHUNK_ID)
			final int chunkId,
			@HeaderParam(RestClientInterceptor.HEADER_ORIGINATOR)
			final String originator,
			@HeaderParam(RestClientInterceptor.HEADER_SIGNATURE)
			final String signature,
			@Body
			final SetupComponentVerificationDataPayload setupComponentVerificationDataPayload,
			@Context
			final HttpServletRequest request) throws IOException {

		return messageBrokerOrchestratorClient.computeGenEncLongCodeShares(electionEventId, verificationCardSetId,
				chunkId, setupComponentVerificationDataPayload).execute();

	}

	@GET
	@Path("api/v1/configuration/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/chunkcount/{chunkCount}/status")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ComputingStatus checkGenEncLongCodeSharesStatus(
			@PathParam(RestApplication.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@PathParam(RestApplication.PARAMETER_VALUE_VERIFICATION_CARD_SET_ID)
			final String verificationCardSetId,
			@PathParam(RestApplication.PARAMETER_VALUE_CHUNK_COUNT)
			final int chunkCount,
			@Context
			final HttpServletRequest request) throws IOException {

		return (messageBrokerOrchestratorClient.checkGenEncLongCodeSharesStatus(electionEventId, verificationCardSetId, chunkCount).execute()).body();

	}

	@GET
	@Path("api/v1/configuration/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/chunk/{chunkId}/download")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<ControlComponentCodeSharesPayload> downloadGenEncLongCodeShares(
			@PathParam(RestApplication.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@PathParam(RestApplication.PARAMETER_VALUE_VERIFICATION_CARD_SET_ID)
			final String verificationCardSetId,
			@PathParam(RestApplication.PARAMETER_VALUE_CHUNK_ID)
			final int chunkId,
			@Context
			final HttpServletRequest request) throws IOException {

		return (messageBrokerOrchestratorClient.downloadGenEncLongCodeShares(electionEventId, verificationCardSetId, chunkId).execute()).body();

	}
}
