/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.apigateway.ws.operation.tally;

import java.io.Reader;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.domain.tally.MixDecryptOnlinePayload;
import ch.post.it.evoting.votingserver.apigateway.services.infrastructure.remote.controlcomponents.MessageBrokerOrchestratorClient;
import ch.post.it.evoting.votingserver.apigateway.ws.RestApplication;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RestClientInterceptor;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RetrofitConsumer;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RetrofitException;

import retrofit2.http.Body;

/**
 * Web service to handle the mixing and decrypting in the control components.
 */
@Stateless(name = "ag-MixDecryptOnlineResource")
@Path(MixDecryptOnlineResource.RESOURCE_PATH)
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class MixDecryptOnlineResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(MixDecryptOnlineResource.class);

	@SuppressWarnings("java:S1075")
	static final String RESOURCE_PATH = "/mo/api/v1/tally/";
	private static final String BASE_PATH = "mixonline/electionevent/{electionEventId}/ballotbox/{ballotBoxId}";
	static final String MIXONLINE_MIX_PATH = BASE_PATH + "/mix";
	static final String MIXONLINE_STATUS_PATH = BASE_PATH + "/status";
	static final String MIXONLINE_DOWNLOAD_PATH = BASE_PATH + "/download";

	private static final String PATH_PARAMETER_ELECTION_EVENT_ID = "electionEventId";
	private static final String PATH_PARAMETER_BALLOT_BOX_ID = "ballotBoxId";

	private final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient;
	private static final ObjectMapper domainMapper = DomainObjectMapper.getNewInstance();

	@Inject
	public MixDecryptOnlineResource(final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient) {
		this.messageBrokerOrchestratorClient = messageBrokerOrchestratorClient;
	}

	/**
	 * Send a request to the mixing control component nodes for the creation of MixDecryptOnline.
	 *
	 * @param electionEventId the election event the ballot box belongs to
	 * @param ballotBoxId     the identifier of the ballot box
	 * @param originator      the originator
	 * @param signature       the signature
	 * @return the status of the request
	 */
	@PUT
	@Path(MIXONLINE_MIX_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createMixDecryptOnline(
			@PathParam(RestApplication.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@PathParam(PATH_PARAMETER_BALLOT_BOX_ID)
			final String ballotBoxId,
			@HeaderParam(RestClientInterceptor.HEADER_ORIGINATOR)
			final String originator,
			@HeaderParam(RestClientInterceptor.HEADER_SIGNATURE)
			final String signature,
			@Body
			final Reader bodyReader,
			@Context
			final HttpServletRequest request) {

		LOGGER.debug("processMixDecryptOnline(electionEventId: {}, ballotBoxId: {})", electionEventId, ballotBoxId);

		try {
			RetrofitConsumer.processResponse(messageBrokerOrchestratorClient.createMixDecryptOnline(electionEventId, ballotBoxId));

			return Response.status(Response.Status.CREATED).build();

		} catch (final RetrofitException rfE) {
			LOGGER.error("Error in process MixDecryptOnline.", rfE);

			return Response.status(rfE.getHttpCode()).build();
		}
	}

	/**
	 * Send a request to the mixing control component nodes for the creation of MixDecryptOnline.
	 *
	 * @param electionEventId the election event the ballot box belongs to
	 * @param ballotBoxId     the identifier of the ballot box
	 * @param originator      the originator
	 * @param signature       the signature
	 * @return the status of the creation of Mixnet
	 */
	@GET
	@Path(MIXONLINE_STATUS_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkMixDecryptOnlineStatus(
			@PathParam(PATH_PARAMETER_ELECTION_EVENT_ID)
			final String electionEventId,
			@PathParam(PATH_PARAMETER_BALLOT_BOX_ID)
			final String ballotBoxId,
			@HeaderParam(RestClientInterceptor.HEADER_ORIGINATOR)
			final String originator,
			@HeaderParam(RestClientInterceptor.HEADER_SIGNATURE)
			final String signature,
			@Context
			final HttpServletRequest request) {

		LOGGER.debug("getMixDecryptOnlineStatus(electionEventId: {}, ballotBoxId: {})", electionEventId, ballotBoxId);

		try {
			final BallotBoxStatus response = RetrofitConsumer.processResponse(
					messageBrokerOrchestratorClient.checkMixDecryptOnlineStatus(electionEventId, ballotBoxId));

			return Response.ok().entity(response).build();

		} catch (final RetrofitException rfE) {
			LOGGER.error("Error in MixDecryptOnline status.", rfE);
			return Response.status(rfE.getHttpCode()).build();
		}
	}

	/**
	 * Get the MixDecryptOnlinePayload from the mixing control component nodes.
	 *
	 * @param electionEventId the election event the ballot box belongs to
	 * @param ballotBoxId     the identifier of the ballot box
	 * @param originator      the originator
	 * @param signature       the signature
	 * @return the Mixnet payload
	 */
	@GET
	@Path(MIXONLINE_DOWNLOAD_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	public Response downloadMixDecryptOnline(
			@PathParam(PATH_PARAMETER_ELECTION_EVENT_ID)
			final String electionEventId,
			@PathParam(PATH_PARAMETER_BALLOT_BOX_ID)
			final String ballotBoxId,
			@HeaderParam(RestClientInterceptor.HEADER_ORIGINATOR)
			final String originator,
			@HeaderParam(RestClientInterceptor.HEADER_SIGNATURE)
			final String signature,
			@Context
			final HttpServletRequest request) {

		LOGGER.debug("getMixDecryptOnlinePayload(electionEventId: {}, ballotBoxId: {})", electionEventId, ballotBoxId);

		try {
			final MixDecryptOnlinePayload response = RetrofitConsumer.processResponse(
					messageBrokerOrchestratorClient.downloadMixDecryptOnline(electionEventId, ballotBoxId));

			final String responseJson = domainMapper.writeValueAsString(response);

			return Response.ok().entity(responseJson).build();

		} catch (final RetrofitException rfE) {
			LOGGER.error("Error in MixDecryptOnline download.", rfE);
			return Response.status(rfE.getHttpCode()).build();
		} catch (final JsonProcessingException ex) {
			LOGGER.error("Error in MixDecryptOnline download.", ex);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}
