/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.apigateway.services.infrastructure.remote.admin;

import ch.post.it.evoting.votingserver.commons.ui.Constants;

import jakarta.validation.constraints.NotNull;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * The Interface of election information client for admin.
 */
public interface ElectionInformationAdminClient {

	@POST("{pathBallotdata}/tenant/{tenantId}/electionevent/{electionEventId}/ballot/{ballotId}/adminboard/{adminBoardId}")
	Call<ResponseBody> saveBallotData(
			@Path(Constants.PARAMETER_PATH_BALLOTDATA)
			String pathBallotdata,
			@Path(Constants.PARAMETER_VALUE_TENANT_ID)
			String tenantId,
			@Path(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			String electionEventId,
			@Path(Constants.PARAMETER_VALUE_BALLOT_ID)
			String ballotId,
			@Path(Constants.PARAMETER_VALUE_ADMIN_BOARD_ID)
			String adminBoardId,
			@NotNull
			@Header(Constants.PARAMETER_X_FORWARDED_FOR)
			String xForwardedFor,
			@NotNull
			@Header(Constants.PARAMETER_X_REQUEST_ID)
			String trackingId,
			@NotNull
			@Body
			RequestBody ballotData);

	@POST("{pathBallotboxdata}/tenant/{tenantId}/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/adminboard/{adminBoardId}")
	Call<ResponseBody> addBallotBoxInformation(
			@Path(Constants.PARAMETER_PATH_BALLOTBOXDATA)
			String pathBallotboxdata,
			@Path(Constants.PARAMETER_VALUE_TENANT_ID)
			String tenantId,
			@Path(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			String electionEventId,
			@Path(Constants.PARAMETER_VALUE_BALLOT_BOX_ID)
			String ballotBoxId,
			@Path(Constants.PARAMETER_VALUE_ADMIN_BOARD_ID)
			String adminBoardId,
			@NotNull
			@Header(Constants.PARAMETER_X_FORWARDED_FOR)
			String xForwardedFor,
			@NotNull
			@Header(Constants.PARAMETER_X_REQUEST_ID)
			String trackingId,
			@NotNull
			@Body
			RequestBody ballotBoxData);

}
