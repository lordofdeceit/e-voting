/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.apigateway.services.infrastructure.remote.controlcomponents;

import java.util.List;

import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.databind.JsonNode;

import ch.post.it.evoting.cryptoprimitives.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.cryptoprimitives.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.domain.configuration.setupvoting.ComputingStatus;
import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.domain.tally.MixDecryptOnlinePayload;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RestClientInterceptor;
import ch.post.it.evoting.votingserver.commons.ui.Constants;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface MessageBrokerOrchestratorClient {

	@POST("api/v1/configuration/setupvoting/keygeneration/electionevent/{electionEventId}")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<JsonNode> generateCCKeys(
			@Path(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@Body
			final RequestBody controlComponentKeyGenerationRequestPayload,
			@Header(RestClientInterceptor.HEADER_ORIGINATOR)
			final String originator,
			@Header(RestClientInterceptor.HEADER_SIGNATURE)
			final String signature,
			@Header(Constants.PARAMETER_X_FORWARDED_FOR)
			final String xForwardedFor,
			@Header(Constants.PARAMETER_X_REQUEST_ID)
			final String trackingId);

	@POST("api/v1/configuration/setupvoting/longvotecastreturncodesallowlist/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<Void> uploadLongVoteCastReturnCodesAllowList(
			@Path(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@Path(Constants.PARAMETER_VALUE_VERIFICATION_CARD_SET_ID)
			final String verificationCardSetId,
			@Body
			final RequestBody longVoteCastReturnCodesAllowListPayload);

	@PUT("api/v1/tally/mixonline/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/mix")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<Void> createMixDecryptOnline(
			@Path(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@Path(Constants.PARAMETER_VALUE_BALLOT_BOX_ID)
			final String ballotBoxId);

	@GET("api/v1/tally/mixonline/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/status")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<BallotBoxStatus> checkMixDecryptOnlineStatus(
			@Path(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@Path(Constants.PARAMETER_VALUE_BALLOT_BOX_ID)
			final String ballotBoxId);

	@GET("api/v1/tally/mixonline/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/download")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<MixDecryptOnlinePayload> downloadMixDecryptOnline(
			@Path(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@Path(Constants.PARAMETER_VALUE_BALLOT_BOX_ID)
			final String ballotBoxId);

	@PUT("api/v1/configuration/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/chunk/{chunkId}/computegenenclongcodeshares")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<Void> computeGenEncLongCodeShares(
			@Path(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@Path(Constants.PARAMETER_VALUE_VERIFICATION_CARD_SET_ID)
			final String verificationCardSetId,
			@Path(Constants.PARAMETER_VALUE_CHUNK_ID)
			final Integer chunkId,
			@Body
			final SetupComponentVerificationDataPayload setupComponentVerificationDataPayload);

	@GET("api/v1/configuration/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/chunk/{chunkId}/download")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<List<ControlComponentCodeSharesPayload>> downloadGenEncLongCodeShares(
			@Path(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@Path(Constants.PARAMETER_VALUE_VERIFICATION_CARD_SET_ID)
			final String verificationCardSetId,
			@Path(Constants.PARAMETER_VALUE_CHUNK_ID)
			final Integer chunkId);

	@GET("api/v1/configuration/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/chunkcount/{chunkCount}/status")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<ComputingStatus> checkGenEncLongCodeSharesStatus(
			@Path(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@Path(Constants.PARAMETER_VALUE_VERIFICATION_CARD_SET_ID)
			final String verificationCardSetId,
			@Path(Constants.PARAMETER_VALUE_CHUNK_COUNT)
			final Integer chunkCount);

}
