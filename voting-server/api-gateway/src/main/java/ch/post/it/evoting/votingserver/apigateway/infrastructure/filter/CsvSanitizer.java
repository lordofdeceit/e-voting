/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.apigateway.infrastructure.filter;

import java.util.stream.Stream;

final class CsvSanitizer {

	private CsvSanitizer() {
		// Utility class
	}

	/**
	 * Check if the given 'CSV' string passes sanitization successfully.
	 * <p>
	 * The validation is an HTML validation of each line of the CSV. Therefore, it doesn't care about delimiters.
	 *
	 * @param input to be evaluated.
	 * @throws IllegalArgumentException if the input does not pass sanitization successfully.
	 */
	public static void checkSanitized(final String input) {
		if (input == null) {
			return;
		}

		Stream.of(input.split("\\r?\\n"))
				.map(String::trim)
				.map(s -> s.replace("\\", ""))
				.forEach(HtmlSanitizer::checkSanitized);
	}
}
