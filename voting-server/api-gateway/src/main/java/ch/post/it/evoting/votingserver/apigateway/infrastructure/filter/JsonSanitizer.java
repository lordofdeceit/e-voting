/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.apigateway.infrastructure.filter;

import java.text.Normalizer;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

final class JsonSanitizer {

	private JsonSanitizer() {
		// Utility class
	}

	/**
	 * Check if the given JSON input passes sanitization successfully.
	 * <p>
	 * Validation removes null chars and uniformize the unicode chars used. <br/>Then, it removes part of the JSON which is unsafe to be given to eval
	 * function (where malicious code could be).
	 * <p>
	 * If the sanitized string does not correspond to the input (meaning some content is unsafe) an exception is thrown.
	 *
	 * @param input to be evaluated.
	 * @throws IllegalArgumentException if the input does not pass sanitization successfully.
	 */
	public static void checkSanitized(final String input) {
		if (input == null || input.isBlank()) {
			return;
		}

		final String sanitize = sanitize(input);

		if (!input.equals(sanitize)) {
			final String loggedInput = input.replaceAll("[\n\r\t]", "_");
			throw new IllegalArgumentException(String.format("Data does not pass sanitization. [input=%s, sanitize=%s]", loggedInput, sanitize));
		}
	}

	/**
	 * Sanitize the input string with Google's Json lib.
	 *
	 * @param input string to be evaluated.
	 * @return sanitized data.
	 */
	private static String sanitize(final String input) {
		return Optional.of(input)
				// Avoid null characters
				.map(s -> StringUtils.replace(s, "\0", ""))
				// Normalize
				.map(s -> Normalizer.normalize(s, Normalizer.Form.NFKC))
				// Use the Google library to clean JSON.
				.map(com.google.json.JsonSanitizer::sanitize)
				.orElseThrow(() -> new IllegalArgumentException("Sanitized data is null."));
	}
}
