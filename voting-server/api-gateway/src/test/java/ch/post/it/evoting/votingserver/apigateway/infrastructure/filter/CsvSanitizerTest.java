/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.apigateway.infrastructure.filter;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.stream.Stream;

import org.assertj.core.api.ThrowableAssert.ThrowingCallable;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class CsvSanitizerTest {

	@ParameterizedTest(name = "{0}")
	@MethodSource("happyPathProvider")
	void happyPath(final String testLabel, final String input) {
		// given

		// when
		final ThrowingCallable validation = () -> CsvSanitizer.checkSanitized(input);

		// then
		assertThatCode(validation).doesNotThrowAnyException();
	}

	static Stream<Arguments> happyPathProvider() {
		return Stream.of(
				Arguments.of("Null", null),
				Arguments.of("Simple row with comma", "a1,a2,a3,a4"),
				Arguments.of("Simple row with semicolon", "a1;a2;a3;a4"),
				Arguments.of("Simple row with comma (eol)", "a1,a2,a3,a4\r\n"),
				Arguments.of("Simple row with semicolon (eol)", "a1;a2;a3;a4\r\n"),
				Arguments.of("Simple row with mix", "a1,a2;a3,a4\n"),
				Arguments.of("Two rows with different length", "a1,a2\nb1,b2,b3\n"),
				Arguments.of("Two empty rows", "\n"),
				Arguments.of("Multiline with mix", "a1, a2;a3,a4\r\nb1, b2,b3\nc1; c2,c3,c4,\nd1 \r\ne1 ;"),
				Arguments.of("Backslash mix", "a1,\\\"a2\\\\\",\\\\\\\"a3\\\\\\\"")
		);
	}

	@Test
	void ensureHtmlSanitizationIsCorrectlyCall() {
		// given
		final String invalidValue = "\u006F\u0302 \0 test %2526 <script>";

		// when
		final ThrowingCallable validation = () -> CsvSanitizer.checkSanitized(invalidValue);

		// then
		assertThatThrownBy(validation).isInstanceOf(IllegalArgumentException.class);
	}
}