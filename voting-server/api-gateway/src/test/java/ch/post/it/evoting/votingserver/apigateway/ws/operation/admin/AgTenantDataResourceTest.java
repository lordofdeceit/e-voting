/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.apigateway.ws.operation.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.message.internal.OutboundJaxrsResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import ch.post.it.evoting.votingserver.apigateway.services.infrastructure.remote.admin.AuthenticationAdminClient;
import ch.post.it.evoting.votingserver.commons.tracking.TrackIdGenerator;

import okhttp3.Request;
import okio.Timeout;
import retrofit2.Call;
import retrofit2.Callback;

@ExtendWith(MockitoExtension.class)
class AgTenantDataResourceTest {

	private static final String TRACK_ID = "trackId";
	private static final String TENANT_ID = "100";

	@Mock
	TrackIdGenerator trackIdGenerator;
	@Mock
	HttpServletRequest servletRequest;
	@Mock
	AuthenticationAdminClient authenticationAdminClient;

	@InjectMocks
	AgTenantDataResource agTenantDataResource;

	@Test
	void checkTenantActivation() {
		when(servletRequest.getHeader(anyString())).thenReturn("");
		when(servletRequest.getLocalAddr()).thenReturn("");
		when(trackIdGenerator.generate()).thenReturn(TRACK_ID);

		final MockedCall mockedCall = new MockedCall();

		when(authenticationAdminClient.checkTenantActivation(AgTenantDataResource.PATH_TENANT_DATA, TENANT_ID, ",", TRACK_ID)).thenReturn(mockedCall);

		final Response response = agTenantDataResource.checkTenantActivation(TENANT_ID, servletRequest);

		assertEquals(200, response.getStatus());

		final JsonArray expectedArray = new JsonArray();
		expectedArray.add(getJsonObject());

		assertEquals(expectedArray.toString(), ((OutboundJaxrsResponse) response).getContext().getEntity().toString());
	}

	private static JsonObject getJsonObject() {
		final JsonObject tenantConfigured = new JsonObject();
		tenantConfigured.addProperty(TENANT_ID, "configured");
		return tenantConfigured;
	}

	static class MockedCall implements Call<JsonObject> {
		@Override
		public retrofit2.Response<JsonObject> execute() throws IOException {
			return retrofit2.Response.success(getJsonObject());
		}

		@Override
		public void enqueue(final Callback<JsonObject> callback) {

		}

		@Override
		public boolean isExecuted() {
			return false;
		}

		@Override
		public void cancel() {

		}

		@Override
		public boolean isCanceled() {
			return false;
		}

		@Override
		public Call<JsonObject> clone() {
			return null;
		}

		@Override
		public Request request() {
			return null;
		}

		@Override
		public Timeout timeout() {
			return null;
		}
	}
}
