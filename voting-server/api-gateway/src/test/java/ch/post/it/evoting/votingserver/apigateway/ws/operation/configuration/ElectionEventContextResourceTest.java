/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.apigateway.ws.operation.configuration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;

import ch.post.it.evoting.votingserver.apigateway.services.infrastructure.remote.admin.VoteVerificationAdminClient;
import ch.post.it.evoting.votingserver.apigateway.ws.proxy.XForwardedForFactoryImpl;
import ch.post.it.evoting.votingserver.commons.tracking.TrackIdGenerator;

import retrofit2.Call;
import retrofit2.Response;
import uk.org.webcompere.systemstubs.environment.EnvironmentVariables;
import uk.org.webcompere.systemstubs.jupiter.SystemStub;
import uk.org.webcompere.systemstubs.jupiter.SystemStubsExtension;

@ExtendWith(SystemStubsExtension.class)
class ElectionEventContextResourceTest extends JerseyTest {

	@SystemStub
	private final EnvironmentVariables environmentVariables = new EnvironmentVariables().set("VERIFICATION_CONTEXT_URL", "localhost");

	@Mock
	Logger logger;

	@Mock
	TrackIdGenerator trackIdGenerator;

	@Mock
	HttpServletRequest servletRequest;

	@Mock
	VoteVerificationAdminClient voteVerificationAdminClient;

	@InjectMocks
	ElectionEventContextResource electionEventContextResource;

	@Test
	void saveElectionEventContextWithValidParameters() throws IOException {
		final String electionEventId = "e3e3c2fd8a16489291c5c24e7b74b26e";
		final String trackId = "trackId";
		final String X_FORWARDER_VALUE = "localhost,";

		@SuppressWarnings("unchecked")
		final Call<Void> call = (Call<Void>) Mockito.mock(Call.class);
		when(call.execute()).thenReturn(Response.success(null));

		when(servletRequest.getHeader(XForwardedForFactoryImpl.HEADER)).thenReturn("localhost");
		when(servletRequest.getRemoteAddr()).thenReturn("");
		when(servletRequest.getLocalAddr()).thenReturn("");
		when(trackIdGenerator.generate()).thenReturn(trackId);
		when(voteVerificationAdminClient
				.uploadElectionEventContext(eq(electionEventId), any(), any(), any(), eq(X_FORWARDER_VALUE), eq(trackId)))
				.thenReturn(call);

		final int status = target("/vv/api/v1/configuration/electioncontext/electionevent/{electionEventId}").resolveTemplate(
				"electionEventId", electionEventId).request().post(Entity.entity(new ArrayList<>(), MediaType.APPLICATION_JSON_TYPE)).getStatus();

		assertEquals(200, status);
	}

	@Override
	protected Application configure() {
		MockitoAnnotations.openMocks(this);

		AbstractBinder binder = new AbstractBinder() {
			@Override
			protected void configure() {
				bind(logger).to(Logger.class);
				bind(trackIdGenerator).to(TrackIdGenerator.class);
				bind(servletRequest).to(HttpServletRequest.class);
				bind(voteVerificationAdminClient).to(VoteVerificationAdminClient.class);
			}
		};
		forceSet(TestProperties.CONTAINER_PORT, "0");
		return new ResourceConfig().register(electionEventContextResource).register(binder);
	}
}
