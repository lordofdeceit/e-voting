/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.apigateway.infrastructure.filter;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.ThrowableAssert.ThrowingCallable;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class HtmlSanitizerTest {

	@ParameterizedTest(name = "{0}")
	@MethodSource("happyPathProvider")
	void happyPath(final String testLabel, final String input) {
		// given

		// when
		final ThrowingCallable validation = () -> HtmlSanitizer.checkSanitized(input);

		// then
		assertThatCode(validation).doesNotThrowAnyException();
	}

	static Stream<Arguments> happyPathProvider() {
		return Stream.of(
				Arguments.of("Null", null),
				Arguments.of("Empty", ""),
				Arguments.of("Blank", "  "),
				Arguments.of("Normalized unicode", "\u0302"),
				Arguments.of("Valid content", "This is a test content for the sanitizer. [123456789@$#^*]")
		);
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("exceptionProvider")
	void exception(final String testLabel, final String input, final String expectedMessage) {
		// given

		// when
		final ThrowingCallable validation = () -> HtmlSanitizer.checkSanitized(input);

		// then
		assertThatThrownBy(validation)
				.isInstanceOf(IllegalArgumentException.class)
				.hasMessage(expectedMessage);
	}

	static Stream<Arguments> exceptionProvider() {
		return Stream.of(
				Arguments.of("Unicode not normalized", "\u006F\u0302", "Data does not pass sanitization. [input=ô, sanitize=ô]"),
				Arguments.of("Remove null", "test\0test\0", "Data does not pass sanitization. [input=test\0test\0, sanitize=testtest]"),
				Arguments.of("Encoding attack 1", "%2526", "Data does not pass sanitization. See ESAPI exception."),
				Arguments.of("Encoding attack 2", "%26lt;", "Data does not pass sanitization. See ESAPI exception."),
				Arguments.of("Encoding attack 3", "%%316", "Data does not pass sanitization. See ESAPI exception."),
				Arguments.of("Encoding attack 4", "&%6ct;", "Data does not pass sanitization. See ESAPI exception."),
				Arguments.of("Encoding attack 5", "%253c", "Data does not pass sanitization. See ESAPI exception."),
				Arguments.of("Script injection attack 1", "test<script>", "Data does not pass sanitization. [input=test<script>, sanitize=test]")
		);
	}
}