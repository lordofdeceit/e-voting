/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.apigateway.ws.operation.configuration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.domain.configuration.SetupComponentCMTablePayload;
import ch.post.it.evoting.votingserver.apigateway.services.infrastructure.remote.admin.VoteVerificationAdminClient;
import ch.post.it.evoting.votingserver.apigateway.ws.proxy.XForwardedForFactoryImpl;
import ch.post.it.evoting.votingserver.commons.tracking.TrackIdGenerator;

import retrofit2.Call;
import retrofit2.Response;
import uk.org.webcompere.systemstubs.environment.EnvironmentVariables;
import uk.org.webcompere.systemstubs.jupiter.SystemStub;
import uk.org.webcompere.systemstubs.jupiter.SystemStubsExtension;

@ExtendWith(SystemStubsExtension.class)
class ReturnCodesMappingTableResourceTest extends JerseyTest {

	@SystemStub
	private final EnvironmentVariables environmentVariables = new EnvironmentVariables()
			.set("VERIFICATION_CONTEXT_URL", "localhost");

	@Mock
	Logger logger;

	@Mock
	TrackIdGenerator trackIdGenerator;

	@Mock
	HttpServletRequest servletRequest;

	@Mock
	VoteVerificationAdminClient voteVerificationAdminClient;

	@Mock
	ObjectMapper objectMapper;

	@InjectMocks
	ReturnCodesMappingTableResource returnCodesMappingTableResource;

	@Test
	void saveReturnCodesMappingTableWithValidParameters() throws IOException {
		final String electionEventId = "e3e3c2fd8a16489291c5c24e7b74b26e";
		final String verificationCardSetId = "e3e3c2fd8a16489291c5c24e7b74b26e";
		final String trackId = "trackId";
		final String X_FORWARDER_VALUE = "localhost,";

		final Call<Void> callMock = (Call<Void>) Mockito.mock(Call.class);
		when(callMock.execute()).thenReturn(Response.success(null));

		when(servletRequest.getHeader(XForwardedForFactoryImpl.HEADER)).thenReturn("localhost");
		when(servletRequest.getRemoteAddr()).thenReturn("");
		when(servletRequest.getLocalAddr()).thenReturn("");
		when(trackIdGenerator.generate()).thenReturn(trackId);
		when(voteVerificationAdminClient
				.saveReturnCodesMappingTable(eq(electionEventId), eq(verificationCardSetId), any(), any(), any(), eq(X_FORWARDER_VALUE), eq(trackId)))
				.thenReturn(callMock);
		when(objectMapper.writeValueAsBytes(any())).thenReturn(new byte[] { 1, 2 });

		final SetupComponentCMTablePayload request = new SetupComponentCMTablePayload.Builder()
				.setElectionEventId(electionEventId)
				.setVerificationCardSetId(verificationCardSetId)
				.setReturnCodesMappingTable(new TreeMap<>())
				.setSignature(new CryptoPrimitivesSignature(new byte[] { 1, 21 }))
				.build();

		final int status = target(
				"/vv/api/v1/configuration/returncodesmappingtable/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}")
				.resolveTemplate("electionEventId", electionEventId)
				.resolveTemplate("verificationCardSetId", verificationCardSetId)
				.request()
				.post(Entity.entity(request, MediaType.APPLICATION_JSON_TYPE))
				.getStatus();

		assertEquals(200, status);
	}

	@Override
	protected Application configure() {
		MockitoAnnotations.openMocks(this);

		final AbstractBinder binder = new AbstractBinder() {
			@Override
			protected void configure() {
				bind(logger).to(Logger.class);
				bind(trackIdGenerator).to(TrackIdGenerator.class);
				bind(servletRequest).to(HttpServletRequest.class);
				bind(voteVerificationAdminClient).to(VoteVerificationAdminClient.class);
				bind(objectMapper).to(ObjectMapper.class);
			}
		};
		forceSet(TestProperties.CONTAINER_PORT, "0");
		return new ResourceConfig().register(returnCodesMappingTableResource).register(binder);
	}
}
