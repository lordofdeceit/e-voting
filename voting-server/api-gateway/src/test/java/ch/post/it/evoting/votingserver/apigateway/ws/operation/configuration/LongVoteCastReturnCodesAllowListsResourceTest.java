/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.apigateway.ws.operation.configuration;

import static javax.ws.rs.core.Response.Status.OK;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;

import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.domain.configuration.setupvoting.SetupComponentLVCCAllowListPayload;
import ch.post.it.evoting.votingserver.apigateway.services.infrastructure.remote.controlcomponents.MessageBrokerOrchestratorClient;
import ch.post.it.evoting.votingserver.apigateway.ws.RestApplication;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RestClientInterceptor;
import ch.post.it.evoting.votingserver.commons.tracking.TrackIdGenerator;

import retrofit2.Call;
import retrofit2.Response;

@DisplayName("A longVoteCastReturnCodesAllowListResource")
class LongVoteCastReturnCodesAllowListResourceTest extends JerseyTest {
	@Mock
	Logger logger;

	@Mock
	private TrackIdGenerator trackIdGenerator;

	@Mock
	private HttpServletRequest servletRequest;

	@Mock
	private MessageBrokerOrchestratorClient messageBrokerOrchestratorClient;

	@InjectMocks
	private LongVoteCastReturnCodesAllowListResource longVoteCastReturnCodesAllowListResource;

	@Override
	protected Application configure() {
		try (final AutoCloseable ignored = MockitoAnnotations.openMocks(this)) {

			final AbstractBinder binder = new AbstractBinder() {
				@Override
				protected void configure() {
					bind(logger).to(Logger.class);
					bind(trackIdGenerator).to(TrackIdGenerator.class);
					bind(servletRequest).to(HttpServletRequest.class);
					bind(messageBrokerOrchestratorClient).to(MessageBrokerOrchestratorClient.class);
				}
			};

			forceSet(TestProperties.CONTAINER_PORT, "0");

			return new ResourceConfig().register(longVoteCastReturnCodesAllowListResource).register(binder);
		} catch (final Exception ex) {
			throw new RuntimeException("Problem creating configuration of JAX_RS Application", ex);
		}
	}

	private void commonMockPreparation() {
		when(servletRequest.getHeader(RestClientInterceptor.HEADER_ORIGINATOR)).thenReturn("originator");
		when(servletRequest.getHeader(RestClientInterceptor.HEADER_SIGNATURE)).thenReturn("signature");

		when(servletRequest.getRemoteAddr()).thenReturn("");
		when(servletRequest.getLocalAddr()).thenReturn("");
	}

	@Test
	void uploadLongVoteCastReturnCodesAllowList() throws IOException {
		final String path = "/mo/" + LongVoteCastReturnCodesAllowListResource.LONG_VOTE_ALLOW_LIST_PATH;
		final String electionEventId = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase();
		final String verificationCardSetId = RandomFactory.createRandom().genRandomBase16String(32).toLowerCase();

		final String lVCC1 = Base64.getEncoder().encodeToString(new byte[]{78});
		final String lVCC2 = Base64.getEncoder().encodeToString(new byte[]{49});
		final SetupComponentLVCCAllowListPayload request = new SetupComponentLVCCAllowListPayload(electionEventId, verificationCardSetId,
				Arrays.asList(lVCC1, lVCC2), new CryptoPrimitivesSignature(new byte[0]));

		@SuppressWarnings("unchecked")
		final Call<Void> callMock = (Call<Void>) Mockito.mock(Call.class);
		when(callMock.execute()).thenReturn(Response.success(null));

		commonMockPreparation();

		when(messageBrokerOrchestratorClient.uploadLongVoteCastReturnCodesAllowList(eq(electionEventId), eq(verificationCardSetId), any()))
				.thenReturn(callMock);

		final WebTarget webTarget = target(path)
				.resolveTemplate(RestApplication.PARAMETER_VALUE_ELECTION_EVENT_ID, electionEventId)
				.resolveTemplate(RestApplication.PARAMETER_VALUE_VERIFICATION_CARD_SET_ID, verificationCardSetId);

		try (final javax.ws.rs.core.Response response = webTarget.request().post(Entity.entity(request, MediaType.APPLICATION_JSON_TYPE))) {
			assertEquals(OK.getStatusCode(), response.getStatus());
		}
	}

}
