/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.apigateway.infrastructure.filter;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.stream.Stream;

import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class JsonSanitizerTest {

	@ParameterizedTest(name = "{0}")
	@MethodSource("happyPathProvider")
	void happyPath(final String testLabel, final String input) {
		// given

		// when
		final ThrowableAssert.ThrowingCallable validation = () -> JsonSanitizer.checkSanitized(input);

		// then
		assertThatCode(validation).doesNotThrowAnyException();
	}

	static Stream<Arguments> happyPathProvider() {
		return Stream.of(
				Arguments.of("Null", null),
				Arguments.of("Empty", ""),
				Arguments.of("Blank", "  "),
				Arguments.of("Valid content", "{\"a\":{\"b\":\"1\",\"c\":\"2\"},\"d\":0,\"e\":1.0}")
		);
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("exceptionProvider")
	void exception(final String testLabel, final String input, final String expectedMessage) {
		// given

		// when
		final ThrowableAssert.ThrowingCallable validation = () -> JsonSanitizer.checkSanitized(input);

		// then
		assertThatThrownBy(validation)
				.isInstanceOf(IllegalArgumentException.class)
				.hasMessage(expectedMessage);
	}

	static Stream<Arguments> exceptionProvider() {
		return Stream.of(
				Arguments.of("Missing quotes", "{a:{b:\"1\",c:\"2\"},d:0,e:1.0}",
						"Data does not pass sanitization. [input={a:{b:\"1\",c:\"2\"},d:0,e:1.0}, sanitize={\"a\":{\"b\":\"1\",\"c\":\"2\"},\"d\":0,\"e\":1.0}]"),
				Arguments.of("Missing curly brace", "{\"test\":\"user\"",
						"Data does not pass sanitization. [input={\"test\":\"user\", sanitize={\"test\":\"user\"}]"),
				Arguments.of("Invalid JSON", "(this is not a json)",
						"Data does not pass sanitization. [input=(this is not a json), sanitize=\"this\" ]"),
				Arguments.of("Script in JSON",
						"{\"name\":\"Alice\",\"description\":\"Likes pie & security holes.\"});alert(1);({\"name\":\"Alice\",\"description\":\"Likes XSS.\"}",
						"Data does not pass sanitization. [input={\"name\":\"Alice\",\"description\":\"Likes pie & security holes.\"});alert(1);({\"name\":\"Alice\",\"description\":\"Likes XSS.\"}, sanitize={\"name\":\"Alice\",\"description\":\"Likes pie & security holes.\"}]")
		);
	}
}