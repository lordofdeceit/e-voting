/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.apigateway.infrastructure.filter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.ThrowableAssert.ThrowingCallable;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Map;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class HttpRequestSanitizerTest {

	@Mock
	private HttpServletRequest request;
	@InjectMocks
	private HttpRequestSanitizer sanitizer;

	@Test
	void getHeaderWithValidValue() {
		// given
		final String key = "test-key";
		final String value = "test-value";
		when(request.getHeader(key)).thenReturn(value);

		// when
		final String header = sanitizer.getHeader(key);

		// then
		assertThat(header).isEqualTo(value);
		verify(request).getHeader(key);
	}

	@ParameterizedTest
	@MethodSource("invalidValuesProvider")
	void getHeaderWithInvalidValue(final String input) {
		// given
		final String key = "test-key";
		when(request.getHeader(key)).thenReturn(input);

		// when
		final ThrowingCallable validation = () -> sanitizer.getHeader(key);

		// then
		assertThatThrownBy(validation).isInstanceOf(IllegalArgumentException.class);
		verify(request).getHeader(key);
	}

	static Stream<Arguments> invalidValuesProvider() {
		return Stream.of("\u006F\u0302", "\0", "%2526", "<script>").map(Arguments::of);
	}

	@Test
	void getParameterWithValidValue() {
		// given
		final String key = "test-key";
		final String value = "test-value";
		when(request.getParameter(key)).thenReturn(value);

		// when
		final String parameter = sanitizer.getParameter(key);

		// then
		assertThat(parameter).isEqualTo(value);
		verify(request).getParameter(key);
	}

	@ParameterizedTest
	@MethodSource("invalidValuesProvider")
	void getParameterWithInvalidValue(final String input) {
		// given
		final String key = "test-key";
		when(request.getParameter(key)).thenReturn(input);

		// when
		final ThrowingCallable validation = () -> sanitizer.getParameter(key);

		// then
		assertThatThrownBy(validation).isInstanceOf(IllegalArgumentException.class);
		verify(request).getParameter(key);
	}

	@Test
	void getParameterValuesWithValidValue() {
		// given
		final String key = "test-key";
		final String[] values = { "test-value-1", "test-value-2", "test-value-3" };
		when(request.getParameterValues(key)).thenReturn(values);

		// when
		final String[] parameters = sanitizer.getParameterValues(key);

		// then
		assertThat(parameters).containsExactly(values);
	}

	@ParameterizedTest
	@MethodSource("invalidValuesProvider")
	void getParameterValuesWithInvalidValue(final String input) {
		// given
		final String key = "test-key";
		final String[] values = { "test-value-1", "test-value-2", input };
		when(request.getParameterValues(key)).thenReturn(values);

		// when
		final ThrowingCallable validation = () -> sanitizer.getParameterValues(key);

		// then
		assertThatThrownBy(validation).isInstanceOf(IllegalArgumentException.class);
		verify(request).getParameterValues(key);
	}

	@Test
	void getParameterMapWithValidValue() {
		// given
		final var expectedParameters = Map.of(
				"k1", new String[] { "a1", "a2" },
				"k2", new String[] { "b1", "b2", "b3" },
				"k3", new String[] { "c1" });
		when(request.getParameterMap()).thenReturn(expectedParameters);

		// when
		final var actualParameters = sanitizer.getParameterMap();

		// then
		assertThat(actualParameters).containsAllEntriesOf(expectedParameters);
	}

	@ParameterizedTest
	@MethodSource("invalidValuesProvider")
	void getParameterMapWithInvalidValue(final String input) {
		// given
		final var expectedParameters = Map.of(
				"k1", new String[] { "a1", "a2" },
				"k2", new String[] { "b1", "b2", "b3" },
				"k3", new String[] { "c1", input });
		when(request.getParameterMap()).thenReturn(expectedParameters);

		// when
		final ThrowingCallable validation = () -> sanitizer.getParameterMap();

		// then
		assertThatThrownBy(validation).isInstanceOf(IllegalArgumentException.class);
		verify(request).getParameterMap();
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("happyPathProvider")
	void getQueryStringWithValidValue(final String testLabel, final String input) {
		// given
		when(request.getQueryString()).thenReturn(input);

		// when
		final var actual = sanitizer.getQueryString();

		// then
		assertThat(actual).isEqualTo(input);
	}

	static Stream<Arguments> happyPathProvider() {
		return Stream.of(
				Arguments.of("Null", null),
				Arguments.of("No parameter", ""),
				Arguments.of("Only one parameter", "k1=v1"),
				Arguments.of("Null parameter", "k1="),
				Arguments.of("Several not null parameters", "k1=v1&k2=v2&k3=v3"),
				Arguments.of("Several with null parameters", "k1=&k2=v2&k3=v3"),
				Arguments.of("Same keys are not override", "k=v1&k=v2")
		);
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("exceptionProvider")
	void getQueryStringWithInvalidValue(final String testLabel, final String input, final String expectedMessage) {
		// given
		when(request.getQueryString()).thenReturn(input);

		// when
		final ThrowingCallable validation = () -> sanitizer.getQueryString();

		// then
		assertThatThrownBy(validation)
				.isInstanceOf(IllegalArgumentException.class)
				.hasMessage(expectedMessage);
		verify(request).getQueryString();
	}

	static Stream<Arguments> exceptionProvider() {
		return Stream.of(
				Arguments.of("Value with equal", "k=v=", "Data does not pass sanitization. [input=k=v=, sanitize=]"),
				Arguments.of("Key with equal", "k==v", "Data does not pass sanitization. [input=k==v, sanitize=]"),
				Arguments.of("Double equal", "k==", "Data does not pass sanitization. [input=k==, sanitize=]"),
				Arguments.of("No key", "=v", "Data does not pass sanitization. [input==v, sanitize=]")
		);
	}

	@ParameterizedTest
	@MethodSource("invalidValuesProvider")
	void ensureHtmlSanitizationIsCorrectlyCalled(final String input) {
		// given
		when(request.getQueryString()).thenReturn(input);

		// when
		final ThrowingCallable validation = () -> sanitizer.getQueryString();

		// then
		assertThatThrownBy(validation).isInstanceOf(IllegalArgumentException.class);
		verify(request).getQueryString();
	}
}
