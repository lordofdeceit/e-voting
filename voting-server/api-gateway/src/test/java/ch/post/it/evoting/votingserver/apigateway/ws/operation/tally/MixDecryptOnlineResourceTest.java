/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.apigateway.ws.operation.tally;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.domain.SecurityLevelTestingOnly;
import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.domain.tally.ControlComponentBallotBoxPayload;
import ch.post.it.evoting.domain.tally.MixDecryptOnlinePayload;
import ch.post.it.evoting.votingserver.apigateway.services.infrastructure.remote.controlcomponents.MessageBrokerOrchestratorClient;
import ch.post.it.evoting.votingserver.apigateway.ws.RestApplication;
import ch.post.it.evoting.votingserver.apigateway.ws.proxy.XForwardedForFactoryImpl;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RestClientInterceptor;

import retrofit2.Call;
import retrofit2.Response;

@SecurityLevelTestingOnly
@DisplayName("MixDecryptOnlineResource")
class MixDecryptOnlineResourceTest extends JerseyTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(MixDecryptOnlineResourceTest.class);
	private static final String MIX_DEC_ONLINE_PATH = "tally/mixonline";
	private static final String AUTH_TOKEN = "authToken";

	@Mock
	Logger logger;

	@Mock
	private HttpServletRequest servletRequest;
	@Mock
	private MessageBrokerOrchestratorClient messageBrokerOrchestratorClient;

	@InjectMocks
	private MixDecryptOnlineResource sut;

	private static ObjectMapper domainMapper;
	private static List<ControlComponentBallotBoxPayload> controlComponentBallotBoxPayloads;
	private static List<ControlComponentShufflePayload> responseControlComponentShufflePayloads;

	@BeforeEach
	void setup() throws IOException {
		assertEquals("TESTING_ONLY", System.getenv("SECURITY_LEVEL"));
		domainMapper = DomainObjectMapper.getNewInstance();

		// Responses for each node
		final URL controlComponentBallotBoxPayloadResource = getClass().getClassLoader()
				.getResource(MIX_DEC_ONLINE_PATH + "/control-component-ballot-box-payload.json");
		controlComponentBallotBoxPayloads = domainMapper.readValue(controlComponentBallotBoxPayloadResource, new TypeReference<>() {
		});

		final URL shufflePayloadsResource = getClass().getClassLoader().getResource(MIX_DEC_ONLINE_PATH + "/mixnet-shuffle-payloads.json");
		responseControlComponentShufflePayloads = domainMapper.readValue(shufflePayloadsResource, new TypeReference<>() {
		});
	}

	@Override
	protected Application configure() {
		try (final AutoCloseable closable = MockitoAnnotations.openMocks(this)) {

			final AbstractBinder binder = new AbstractBinder() {
				@Override
				protected void configure() {
					bind(logger).to(Logger.class);
					bind(servletRequest).to(HttpServletRequest.class);
					bind(messageBrokerOrchestratorClient).to(MessageBrokerOrchestratorClient.class);
				}
			};

			forceSet(TestProperties.CONTAINER_PORT, "0");

			return new ResourceConfig().register(sut).register(binder);
		} catch (final Exception ex) {
			logger.error("", ex);
			throw new RuntimeException("Problem creating configuration of JAX_RS Application", ex);
		}
	}

	private void commonMockPreparation() {
		when(servletRequest.getHeader(XForwardedForFactoryImpl.HEADER)).thenReturn("localhost");
		when(servletRequest.getHeader(RestApplication.PARAMETER_AUTHENTICATION_TOKEN)).thenReturn(AUTH_TOKEN);
		when(servletRequest.getHeader(RestClientInterceptor.HEADER_ORIGINATOR)).thenReturn("originator");
		when(servletRequest.getHeader(RestClientInterceptor.HEADER_SIGNATURE)).thenReturn("signature");

		when(servletRequest.getRemoteAddr()).thenReturn("");
		when(servletRequest.getLocalAddr()).thenReturn("");
	}

	@Test
	void createMixDecryptOnlineTest() throws IOException {
		final String path = MixDecryptOnlineResource.RESOURCE_PATH + MixDecryptOnlineResource.MIXONLINE_MIX_PATH;
		final String electionEventId = "e3e3c2fd8a16489291c5c24e7b74b26e";
		final String ballotBoxId = "e3e3c2fd8a16489291c5c2222222222e";
		final Call<Void> callMock = (Call<Void>) Mockito.mock(Call.class);

		commonMockPreparation();

		when(callMock.execute()).thenReturn(retrofit2.Response.success(null));
		when(messageBrokerOrchestratorClient.createMixDecryptOnline(electionEventId, ballotBoxId)).thenReturn(callMock);

		final WebTarget webTarget = target(path)
				.resolveTemplate("electionEventId", electionEventId)
				.resolveTemplate("ballotBoxId", ballotBoxId);
		final Invocation.Builder request = webTarget.request();

		LOGGER.debug("PATH: {}, electionEventId: {}, ballotBoxId: {}", path, electionEventId, ballotBoxId);
		LOGGER.debug("webTarget.uri: {}", webTarget.getUri());

		try (final javax.ws.rs.core.Response res = request.put(Entity.entity(new ArrayList<>(), MediaType.APPLICATION_JSON_TYPE))) {
			LOGGER.debug("Result status: {}", res.getStatusInfo());
			assertEquals(javax.ws.rs.core.Response.Status.CREATED.getStatusCode(), res.getStatus());
			assertFalse(res.hasEntity());
		} catch (final Exception ex) {
			LOGGER.error("Request PUT error: {}", ex.getMessage(), ex);
		}
	}

	@Test
	void checkMixDecryptOnlineStatusTest() throws IOException {
		final String path = MixDecryptOnlineResource.RESOURCE_PATH + MixDecryptOnlineResource.MIXONLINE_STATUS_PATH;
		final String electionEventId = "e3e3c2fd8a16489291c5c24e7b74b26e";
		final String ballotBoxId = "e3e3c2fd8a16489291c5c2222222222e";
		final Call<BallotBoxStatus> call = (Call<BallotBoxStatus>) Mockito.mock(Call.class);

		commonMockPreparation();

		when(call.execute()).thenReturn(retrofit2.Response.success(BallotBoxStatus.MIXED));
		when(messageBrokerOrchestratorClient.checkMixDecryptOnlineStatus(electionEventId, ballotBoxId)).thenReturn(call);

		final WebTarget webTarget = target(path)
				.resolveTemplate("electionEventId", electionEventId)
				.resolveTemplate("ballotBoxId", ballotBoxId);
		final Invocation.Builder request = webTarget.request();

		LOGGER.debug("PATH: {}, electionEventId: {}, ballotBoxId: {}", path, electionEventId, ballotBoxId);
		LOGGER.debug("webTarget.uri: {}", webTarget.getUri());

		try (final javax.ws.rs.core.Response response = request.get()) {
			LOGGER.debug("Result status: {}", response.getStatusInfo());
			assertEquals(javax.ws.rs.core.Response.Status.OK.getStatusCode(), response.getStatus());
			assertTrue(response.hasEntity());

			final BallotBoxStatus mixStatus = response.readEntity(BallotBoxStatus.class);
			LOGGER.debug("mixStatus: {}", mixStatus);
			assertEquals(BallotBoxStatus.MIXED, mixStatus);
		} catch (final Exception ex) {
			LOGGER.error("Request GET error: {}", ex.getMessage(), ex);
		}
	}

	@Test
	void downloadMixDecryptOnlineTest() throws IOException {
		final String path = MixDecryptOnlineResource.RESOURCE_PATH + MixDecryptOnlineResource.MIXONLINE_DOWNLOAD_PATH;
		final String electionEventId = "e3e3c2fd8a16489291c5c24e7b74b26e";
		final String ballotBoxId = "e3e3c2fd8a16489291c5c2222222222e";
		final Call<MixDecryptOnlinePayload> call = (Call<MixDecryptOnlinePayload>) Mockito.mock(Call.class);

		final MixDecryptOnlinePayload mixDecryptOnlinePayload = new MixDecryptOnlinePayload(electionEventId, ballotBoxId,
				controlComponentBallotBoxPayloads, responseControlComponentShufflePayloads);
		final String jsonResult = domainMapper.writeValueAsString(mixDecryptOnlinePayload);

		commonMockPreparation();

		when(call.execute()).thenReturn(Response.success(mixDecryptOnlinePayload));
		when(messageBrokerOrchestratorClient.downloadMixDecryptOnline(electionEventId, ballotBoxId)).thenReturn(call);

		final WebTarget webTarget = target(path)
				.resolveTemplate("electionEventId", electionEventId)
				.resolveTemplate("ballotBoxId", ballotBoxId);
		final Invocation.Builder request = webTarget.request();

		LOGGER.debug("PATH: {}, electionEventId: {}, ballotBoxId: {}", path, electionEventId, ballotBoxId);
		LOGGER.debug("webTarget.uri: {}", webTarget.getUri());

		try (final javax.ws.rs.core.Response response = request.get()) {
			LOGGER.debug("Result status: {}", response.getStatusInfo());
			assertEquals(javax.ws.rs.core.Response.Status.OK.getStatusCode(), response.getStatus());
			assertTrue(response.hasEntity());

			final String mixPayloadJson = response.readEntity(String.class);
			assertEquals(jsonResult, mixPayloadJson);
		} catch (final Exception ex) {
			LOGGER.error("Request GET error: {}", ex.getMessage(), ex);
		}
	}
}
