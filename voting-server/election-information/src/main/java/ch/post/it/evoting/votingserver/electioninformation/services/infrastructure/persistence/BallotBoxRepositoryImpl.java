/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.electioninformation.services.infrastructure.persistence;

import java.io.IOException;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.post.it.evoting.cryptolib.api.exceptions.CryptoLibException;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.commons.infrastructure.persistence.BaseRepositoryImpl;
import ch.post.it.evoting.votingserver.electioninformation.services.domain.model.ballotbox.BallotBox;
import ch.post.it.evoting.votingserver.electioninformation.services.domain.model.ballotbox.BallotBoxRepository;
import ch.post.it.evoting.votingserver.electioninformation.services.domain.model.votingcard.VotingCard;
import ch.post.it.evoting.votingserver.electioninformation.services.domain.model.votingcard.VotingCardWriter;

/**
 * Implementation of VoteRepository.
 */
@Stateless
public class BallotBoxRepositoryImpl extends BaseRepositoryImpl<BallotBox, Integer> implements BallotBoxRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(BallotBoxRepositoryImpl.class);

	private static final String PARAMETER_VOTING_CARD_ID = "votingCardId";

	private static final String PARAMETER_TENANT_ID = "tenantId";

	private static final String PARAMETER_ELECTION_EVENT_ID = "electionEventId";

	private static final String PARAMETER_BALLOT_BOX_ID = "ballotBoxId";

	private static final String PARAMETER_BALLOT_ID = "ballotId";

	private static final int FETCH_SIZE = 1000;

	/**
	 * Searches for a vote with the given tenant, election event and voting card id. This implementation uses database access by executing a SQL-query
	 * to select the data to be retrieved.
	 *
	 * @param tenantId        - the identifier of the tenant.
	 * @param electionEventId - the identifier of the election event.
	 * @param votingCardId    - the identifier of the voting card.
	 * @return a entity representing the vote stored in the ballot box.
	 * @throws ResourceNotFoundException if the vote is not found.
	 */
	@Override
	public BallotBox findByTenantIdElectionEventIdVotingCardId(final String tenantId, final String electionEventId, final String votingCardId)
			throws ResourceNotFoundException {
		final TypedQuery<BallotBox> query = entityManager.createQuery(
				"SELECT b FROM BallotBox b WHERE b.tenantId = :tenantId AND b.electionEventId = :electionEventId AND b.votingCardId = :votingCardId ORDER BY b.id DESC",
				BallotBox.class);
		query.setParameter(PARAMETER_TENANT_ID, tenantId);
		query.setParameter(PARAMETER_ELECTION_EVENT_ID, electionEventId);
		query.setParameter(PARAMETER_VOTING_CARD_ID, votingCardId);
		final List<BallotBox> listBallotBox = query.getResultList();
		if (!listBallotBox.isEmpty()) {
			return listBallotBox.get(0);
		}
		throw new ResourceNotFoundException("");
	}

	@Override
	public List<BallotBox> findByTenantIdElectionEventIdBallotBoxId(final String tenantId, final String electionEventId, final String ballotBoxId) {
		final TypedQuery<BallotBox> query = entityManager.createQuery(
				"SELECT b FROM BallotBox b WHERE b.tenantId = :tenantId AND b.electionEventId = :electionEventId AND b.ballotBoxId = :ballotBoxId",
				BallotBox.class);
		query.setParameter(PARAMETER_TENANT_ID, tenantId);
		query.setParameter(PARAMETER_ELECTION_EVENT_ID, electionEventId);
		query.setParameter(PARAMETER_BALLOT_BOX_ID, ballotBoxId);
		return query.getResultList();
	}

	/**
	 * @see BallotBoxRepository#findByTenantIdElectionEventIdVotingCardIdBallotBoxIdBallotId(String, String, String, String, String)
	 */
	@Override
	public List<BallotBox> findByTenantIdElectionEventIdVotingCardIdBallotBoxIdBallotId(final String tenantId, final String electionEventId,
			final String votingCardId, final String ballotBoxId, final String ballotId) {
		final TypedQuery<BallotBox> query = entityManager.createQuery(
				"SELECT b FROM BallotBox b WHERE b.tenantId = :tenantId AND b.electionEventId = :electionEventId "
						+ "AND b.votingCardId = :votingCardId AND b.ballotId = :ballotId AND b.ballotBoxId = :ballotBoxId", BallotBox.class);
		query.setParameter(PARAMETER_TENANT_ID, tenantId);
		query.setParameter(PARAMETER_ELECTION_EVENT_ID, electionEventId);
		query.setParameter(PARAMETER_VOTING_CARD_ID, votingCardId);
		query.setParameter(PARAMETER_BALLOT_ID, ballotId);
		query.setParameter(PARAMETER_BALLOT_BOX_ID, ballotBoxId);
		return query.getResultList();
	}

	/**
	 * Find and write voting cards that have votes in ballot boxes (used voting cards).
	 *
	 * @param tenantId        the tenant id
	 * @param electionEventId the election event id
	 * @param writer          the writer
	 */
	@Override
	public void findAndWriteUsedVotingCards(final String tenantId, final String electionEventId, final VotingCardWriter writer) {
		final Session session = entityManager.unwrap(Session.class);
		final Query query = session.createQuery("SELECT bb.votingCardId FROM BallotBox bb WHERE bb.tenantId = ?1 AND bb.electionEventId = ?2");
		query.setParameter(1, tenantId);
		query.setParameter(2, electionEventId);
		query.setFetchSize(FETCH_SIZE);
		query.setReadOnly(true);
		query.setLockMode("bb", LockMode.NONE);

		try {
			ScrollableResults results = query.scroll(ScrollMode.FORWARD_ONLY);
			while (results.next()) {
				writer.write(new VotingCard((String) results.get(0)));
			}
			results.close();
		} catch (HibernateException e) {
			throw new CryptoLibException("Error getting used voting cards", e);
		} catch (final IOException e) {
			LOGGER.error("Error writing used voting cards in csv file.", e);
		}
	}

}
