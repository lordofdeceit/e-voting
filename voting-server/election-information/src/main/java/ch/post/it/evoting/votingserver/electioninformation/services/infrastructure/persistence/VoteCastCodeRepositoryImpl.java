/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.electioninformation.services.infrastructure.persistence;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import ch.post.it.evoting.votingserver.commons.beans.exceptions.DuplicateEntryException;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.commons.infrastructure.persistence.BaseRepositoryImpl;
import ch.post.it.evoting.votingserver.electioninformation.services.domain.model.castcode.VoteCastCode;
import ch.post.it.evoting.votingserver.electioninformation.services.domain.model.castcode.VoteCastCodeRepository;

/**
 * Implementation of VoteCastCodeRepository.
 */
@Stateless
public class VoteCastCodeRepositoryImpl extends BaseRepositoryImpl<VoteCastCode, Integer> implements VoteCastCodeRepository {

	private static final String PARAMETER_TENANT_ID = "tenantId";
	private static final String PARAMETER_ELECTION_EVENT_ID = "electionEventId";
	private static final String PARAMETER_VOTING_CARD_ID = "votingCardId";

	/**
	 * Searches for a vote cast code with the given tenant, election event and voting card id. This implementation uses database access by executing a
	 * SQL-query to select the data to be retrieved.
	 *
	 * @param tenantId        - the identifier of the tenant.
	 * @param electionEventId - the identifier of the election event.
	 * @param votingCardId    - the voting card identifier.
	 * @return a entity representing the vote cast code.
	 */
	@Override
	public VoteCastCode findByTenantIdElectionEventIdVotingCardId(String tenantId, String electionEventId, String votingCardId)
			throws ResourceNotFoundException {
		TypedQuery<VoteCastCode> query = entityManager.createQuery(
				"SELECT vcc FROM VoteCastCode vcc WHERE vcc.tenantId = :tenantId AND vcc.electionEventId = :electionEventId AND vcc.votingCardId = :votingCardId",
				VoteCastCode.class);
		query.setParameter(PARAMETER_TENANT_ID, tenantId);
		query.setParameter(PARAMETER_ELECTION_EVENT_ID, electionEventId);
		query.setParameter(PARAMETER_VOTING_CARD_ID, votingCardId);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			throw new ResourceNotFoundException("", e);
		}
	}

	/**
	 * Stores a vote cast code.
	 *
	 * @param tenantId        - the identifier of the tenant id.
	 * @param electionEventId - the identifier of the election event id.
	 * @param votingCardId    - the voting card id.
	 * @param voteCastCode    - the vote cast code.
	 * @throws DuplicateEntryException if the object exists for the given tenant, election event and voting card.
	 */
	@Override
	public void save(String tenantId, String electionEventId, String votingCardId, VoteCastCode voteCastCode) throws DuplicateEntryException {
		// set some values to object
		voteCastCode.setTenantId(tenantId);
		voteCastCode.setElectionEventId(electionEventId);
		voteCastCode.setVotingCardId(votingCardId);
		super.save(voteCastCode);
	}

}
