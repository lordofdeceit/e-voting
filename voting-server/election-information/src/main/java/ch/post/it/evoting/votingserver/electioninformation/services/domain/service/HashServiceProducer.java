/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.electioninformation.services.domain.service;

import javax.enterprise.inject.Produces;

import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;

/**
 * Produces a {@link Hash} with default message digest.
 */
public class HashServiceProducer {

	@Produces
	public Hash getInstance() {
		return HashFactory.createHash();
	}

}
