/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.voting.sendvote;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.cryptoprimitives.utils.Validations.allEqual;
import static ch.post.it.evoting.votingserver.voteverification.voting.sendvote.ExtractCRCAlgorithm.NUMBER_OF_CONTROL_COMPONENTS;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.function.Function;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;

/**
 * Regroups the inputs of the ExtractCRC algorithm.
 */
public class ExtractCRCInput {
	private final List<GroupVector<GqElement, GqGroup>> longChoiceReturnCodeShares;
	private final String verificationCardId;
	private final TreeMap<String, String> returnCodesMappingTable;

	/**
	 * @param longChoiceReturnCodeShares (lCC<sub>1,id</sub>, lCC<sub>2,id</sub>, lCC<sub>3,id</sub>, lCC<sub>4,id</sub>) ∈
	 *                                   (G<sub>q</sub><sup>&#x1D713;</sup>)<sup>4</sup>, CCR long Choice Return Codes shares.
	 * @param verificationCardId         vc<sub>id</sub>, the verification card id.
	 * @param returnCodesMappingTable    CMtable
	 * @throws NullPointerException      if any of the fields is null or any list contains null value.
	 * @throws IllegalArgumentException  if
	 *                                   <ul>
	 *                                       <li>The {@code longChoiceReturnCodeShares} size is not {@value NUMBER_OF_CONTROL_COMPONENTS}.</li>
	 *                                       <li>The GqGroup of each GroupVector of {@code longChoiceReturnCodeShares} is not the same.</li>
	 *                                   </ul>
	 * @throws FailedValidationException if the verification card id is not a valid UUID.
	 */
	public ExtractCRCInput(final List<GroupVector<GqElement, GqGroup>> longChoiceReturnCodeShares, final String verificationCardId,
			final Map<String, String> returnCodesMappingTable) {
		checkNotNull(longChoiceReturnCodeShares);
		validateUUID(verificationCardId);
		checkNotNull(returnCodesMappingTable);

		final List<GroupVector<GqElement, GqGroup>> longChoiceReturnCodeSharesCopy = List.copyOf(longChoiceReturnCodeShares);
		final TreeMap<String, String> returnCodesMappingTableCopy = new TreeMap<>(returnCodesMappingTable);

		checkArgument(longChoiceReturnCodeSharesCopy.size() == NUMBER_OF_CONTROL_COMPONENTS,
				String.format("There must be long Choice Return Code shares from %s control-components.", NUMBER_OF_CONTROL_COMPONENTS));

		// Cross group checks.
		final List<GqGroup> gqGroups = longChoiceReturnCodeSharesCopy.stream().map(GroupVector::getGroup).toList();
		checkArgument(allEqual(gqGroups.stream(), Function.identity()), "All long Choice Return Code Shares must have the same Gq group.");

		this.longChoiceReturnCodeShares = longChoiceReturnCodeSharesCopy;
		this.verificationCardId = verificationCardId;
		this.returnCodesMappingTable = returnCodesMappingTableCopy;
	}

	public List<GroupVector<GqElement, GqGroup>> getLongChoiceReturnCodeShares() {
		return longChoiceReturnCodeShares;
	}

	public String getVerificationCardId() {
		return verificationCardId;
	}

	public SortedMap<String, String> getReturnCodesMappingTable() {
		return returnCodesMappingTable;
	}

	public GqGroup getGroup() {
		return this.longChoiceReturnCodeShares.get(0).getGroup();
	}

	public static class Builder {
		private List<GroupVector<GqElement, GqGroup>> longChoiceReturnCodeShares;
		private String verificationCardId;
		private SortedMap<String, String> returnCodesMappingTable;

		public Builder setLongChoiceReturnCodeShares(final List<GroupVector<GqElement, GqGroup>> longChoiceReturnCodeShares) {
			this.longChoiceReturnCodeShares = longChoiceReturnCodeShares;
			return this;
		}

		public Builder setVerificationCardId(final String verificationCardId) {
			this.verificationCardId = verificationCardId;
			return this;
		}

		public Builder setReturnCodesMappingTable(final SortedMap<String, String> returnCodesMappingTable) {
			this.returnCodesMappingTable = returnCodesMappingTable;
			return this;
		}

		public ExtractCRCInput build() {
			return new ExtractCRCInput(longChoiceReturnCodeShares, verificationCardId, returnCodesMappingTable);
		}
	}
}
