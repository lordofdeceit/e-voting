/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.ws.application.operation;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkState;

import java.security.SignatureException;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.ChannelSecurityContextData;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ApplicationException;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ApplicationExceptionMessages;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.DuplicateEntryException;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RetrofitException;
import ch.post.it.evoting.votingserver.commons.tracking.TrackIdInstance;
import ch.post.it.evoting.votingserver.commons.ui.Constants;
import ch.post.it.evoting.votingserver.commons.ui.ws.rs.persistence.ErrorCodes;
import ch.post.it.evoting.votingserver.voteverification.service.ElectionEventContextService;
import ch.post.it.evoting.votingserver.voteverification.service.ElectionEventService;

/**
 * Web service for saving the election event context.
 */
@Stateless
public class ElectionEventContextResource {

	static final String RESOURCE_PATH = "api/v1/configuration/electioncontext";

	private static final Logger LOGGER = LoggerFactory.getLogger(ElectionEventContextResource.class);

	@Inject
	private ElectionEventContextService electionEventContextService;

	@Inject
	private ElectionEventService encryptionParametersService;

	@Inject
	private SignatureKeystore<Alias> signatureKeystoreService;

	@Inject
	private TrackIdInstance tackIdInstance;

	@Path("api/v1/configuration/electioncontext/electionevent/{electionEventId}")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveElectionEventContext(
			@PathParam(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@NotNull
			@HeaderParam(Constants.PARAMETER_X_REQUEST_ID)
			final String trackingId,
			@NotNull
			final ElectionEventContextPayload electionEventContextPayload,
			@Context
			final HttpServletRequest request) throws ApplicationException {

		validateInput(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID, electionEventId);
		verifyPayloadSignature(electionEventContextPayload);
		tackIdInstance.setTrackId(trackingId);

		try {
			encryptionParametersService.save(electionEventId, electionEventContextPayload.getEncryptionGroup());
			electionEventContextService.saveElectionEventContext(electionEventContextPayload);
		} catch (final DuplicateEntryException e) {
			LOGGER.warn("Duplicate entry tried to be inserted. [electionEventId: {}]", electionEventId);
			return Response.noContent().build();
		} catch (final RetrofitException e) {
			LOGGER.warn("Error trying to save election event context. [electionEventId: {}]", electionEventId);
			return Response.status(e.getHttpCode()).build();
		}

		LOGGER.info("Successfully uploaded and saved the election event context. [electionEventId: {}]", electionEventId);

		return Response.ok().build();
	}

	private void validateInput(final String field, final String pathId) throws ApplicationException {
		try {
			validateUUID(pathId);
		} catch (final FailedValidationException | NullPointerException e) {
			throw new ApplicationException(ApplicationExceptionMessages.EXCEPTION_MESSAGE_QUERY_PARAMETER_IS_NULL, RESOURCE_PATH,
					ErrorCodes.MANDATORY_FIELD, field);
		}
	}

	private void verifyPayloadSignature(final ElectionEventContextPayload electionEventContextPayload) {
		final String electionEventId = electionEventContextPayload.getElectionEventContext().electionEventId();

		final CryptoPrimitivesSignature signature = electionEventContextPayload.getSignature();

		checkState(signature != null, "The signature of the election event context payload is null. [electionEventId: %s]", electionEventId);

		final Hashable additionalContextData = ChannelSecurityContextData.electionEventContext(electionEventId);

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreService.verifySignature(Alias.SDM_CONFIG, electionEventContextPayload,
					additionalContextData, signature.signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not verify the signature of the election event context. [electionEventId: %s]", electionEventId));
		}

		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(ElectionEventContextPayload.class,
					String.format("[electionEventId: %s]", electionEventId));
		}
	}
}
