/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.service.crypto;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.Base64;
import ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.utils.KeyDerivation;
import ch.post.it.evoting.cryptoprimitives.utils.KeyDerivationFactory;

@Dependent
public class BeanProducer {

	@Produces
	public ObjectMapper objectMapper() {
		return DomainObjectMapper.getNewInstance();
	}

	@Produces
	public Random getRandom() {
		return RandomFactory.createRandom();
	}

	@Produces
	public Hash hash(){
		return HashFactory.createHash();
	}

	@Produces
	public KeyDerivation keyDerivation() {
		return KeyDerivationFactory.createKeyDerivation();
	}

	@Produces
	public Base64 base64() {
		return BaseEncodingFactory.createBase64();
	}

}
