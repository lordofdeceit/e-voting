/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.service;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.UncheckedIOException;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.post.it.evoting.cryptoprimitives.domain.election.ElectionEventContext;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.DuplicateEntryException;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RetrofitConsumer;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RetrofitException;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.ElectionEventContextEntity;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.ElectionEventContextRepository;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.ElectionEventEntity;
import ch.post.it.evoting.votingserver.voteverification.infrastructure.remote.MessageBrokerOrchestratorClient;

/**
 * Saves the election event context - in interaction with the control components.
 */
@Stateless
public class ElectionEventContextService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ElectionEventContextService.class);
	private final ElectionEventContextRepository electionEventContextRepository;
	private final ElectionEventService electionEventService;
	private final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient;

	@Inject
	ElectionEventContextService(final ElectionEventContextRepository electionEventContextRepository,
			final ElectionEventService electionEventService,
			final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient) {
		this.electionEventContextRepository = electionEventContextRepository;
		this.electionEventService = electionEventService;
		this.messageBrokerOrchestratorClient = messageBrokerOrchestratorClient;
	}

	/**
	 * Saves the election event context and uploads it to the control components.
	 *
	 * @param electionEventContextPayload the request payload. Must be non null.
	 * @throws NullPointerException             if {@code electionEventContextPayload} is null.
	 * @throws IllegalStateException            if an error occurred while verifying the signature of the election event context payload.
	 * @throws InvalidPayloadSignatureException if the signature of the election event context payload is invalid.
	 * @throws DuplicateEntryException          if the election event context is already saved in the database.
	 * @throws UncheckedIOException             if an error occurs while serializing the election event context.
	 * @throws RetrofitException                if an error occurs while saving from the message broker orchestrator.
	 */
	public void saveElectionEventContext(final ElectionEventContextPayload electionEventContextPayload)
			throws DuplicateEntryException, RetrofitException {
		checkNotNull(electionEventContextPayload);

		final String electionEventId = electionEventContextPayload.getElectionEventContext().electionEventId();

		// save election event context in vote verification
		electionEventContextRepository.save(createElectionEventContextEntity(electionEventContextPayload.getElectionEventContext()));
		LOGGER.info("Election event context successfully saved in vote verification. [electionEventId: {}]", electionEventId);

		// upload election event context to control components
		RetrofitConsumer.processResponse(
				messageBrokerOrchestratorClient.uploadElectionEventContext(electionEventId, electionEventContextPayload));

		LOGGER.info("Election event context successfully uploaded to the control components. [electionEventId: {}]", electionEventId);
	}

	/**
	 * Gets the election event context entity.
	 *
	 * @param electionEventEntity 			the related election event entity. Must be non-null.
	 * @throws NullPointerException         if {@code ElectionEventEntity} is null.
	 * @throws IllegalStateException 		if the election event context entity does not exist.
	 * @return the election event context entity.
	 */
	public ElectionEventContextEntity getElectionEventContextEntity(final ElectionEventEntity electionEventEntity) {
		checkNotNull(electionEventEntity);
		return electionEventContextRepository.findByElectionEvent(electionEventEntity)
				.orElseThrow(() -> new IllegalStateException(
						String.format("No election event context entity found. [electionEventId: %s]", electionEventEntity.getElectionEventId())));
	}

	private ElectionEventContextEntity createElectionEventContextEntity(final ElectionEventContext electionEventContext) {
		final ElectionEventEntity electionEventEntity;
		try {
			electionEventEntity = electionEventService.retrieveElectionEventEntity(electionEventContext.electionEventId());
		} catch (final ResourceNotFoundException e) {
			throw new IllegalStateException(
					String.format("Election event encryption parameters not found. [electionEventId: %s]", electionEventContext.electionEventId()),
					e);
		}

		return new ElectionEventContextEntity(electionEventEntity, electionEventContext.startTime(), electionEventContext.finishTime());
	}

}
