/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.infrastructure.remote.producer;

import javax.enterprise.inject.Produces;

import ch.post.it.evoting.votingserver.voteverification.infrastructure.remote.ElectionInformationClient;

public class ElectionInformationRemoteClientProducer {

	private static final String URI_ELECTION_INFORMATION = System.getenv("ELECTION_INFORMATION_CONTEXT_URL");

	@Produces
	ElectionInformationClient electionInformationClient() {
		return RemoteClientProducer.createRestClient(URI_ELECTION_INFORMATION, ElectionInformationClient.class);
	}
}
