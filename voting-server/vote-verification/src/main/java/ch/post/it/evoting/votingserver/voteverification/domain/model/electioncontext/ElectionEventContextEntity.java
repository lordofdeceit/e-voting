/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext;

import static com.google.common.base.Preconditions.checkNotNull;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "ELECTION_EVENT_CONTEXT")
public class ElectionEventContextEntity {

	@Id
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	@JoinColumn(name = "ELECTION_EVENT_FK_ID")
	private ElectionEventEntity electionEventEntity;

	@Column(name = "START_TIME")
	@NotNull
	private LocalDateTime startTime;

	@Column(name = "FINISH_TIME")
	@NotNull
	private LocalDateTime finishTime;

	@Version
	@Column(name = "CHANGE_CONTROL_ID")
	@NotNull
	private Integer changeControlId;

	public ElectionEventContextEntity() {
		// needed by the repository.
	}

	public ElectionEventContextEntity(final ElectionEventEntity electionEventEntity, final LocalDateTime startTime, final LocalDateTime finishTime) {
		this.electionEventEntity = checkNotNull(electionEventEntity);
		this.startTime = checkNotNull(startTime);
		this.finishTime = checkNotNull(finishTime);
	}

	public ElectionEventEntity getElectionEventEntity() {
		return electionEventEntity;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public LocalDateTime getFinishTime() {
		return finishTime;
	}

	public Integer getChangeControlId() {
		return changeControlId;
	}

}
