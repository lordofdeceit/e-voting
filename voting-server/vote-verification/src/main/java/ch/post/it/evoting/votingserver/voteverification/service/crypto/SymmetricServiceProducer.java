/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.service.crypto;

import javax.enterprise.inject.Produces;

import ch.post.it.evoting.cryptoprimitives.symmetric.Symmetric;
import ch.post.it.evoting.cryptoprimitives.symmetric.SymmetricFactory;

/**
 * Produces a {@link Symmetric}
 */
public class SymmetricServiceProducer {

	@Produces
	public Symmetric getInstance() {
		return SymmetricFactory.createSymmetric();
	}
}
