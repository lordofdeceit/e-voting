/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.service;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.DuplicateEntryException;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.ElectionEventEntity;
import ch.post.it.evoting.votingserver.voteverification.infrastructure.persistence.ElectionEventRepository;

/**
 * Saves and retrieves the election event context - in interaction with the control components.
 */
@Stateless
public class ElectionEventService {

	private final ElectionEventRepository electionEventRepository;
	private final ObjectMapper objectMapper;

	@Inject
	ElectionEventService(final ElectionEventRepository electionEventRepository,
			final ObjectMapper objectMapper) {
		this.electionEventRepository = electionEventRepository;
		this.objectMapper = objectMapper;
	}

	/**
	 * Saves the given election event.
	 *
	 * @param electionEventId the election event identifier. Must be non-null.
	 * @param encryptionGroup the G<sub>q</sub> group. Must be non-null.
	 * @return the election event encryption parameters entity that was saved.
	 * @throws DuplicateEntryException if the entity exists already in the database
	 */
	public ElectionEventEntity save(final String electionEventId, final GqGroup encryptionGroup) throws DuplicateEntryException {
		checkNotNull(electionEventId);
		checkNotNull(encryptionGroup);

		final byte[] encryptionGroupBytes;
		try {
			encryptionGroupBytes = objectMapper.writeValueAsBytes(encryptionGroup);
		} catch (final JsonProcessingException e) {
			throw new IllegalArgumentException("Encryption group could not be deserialized.", e);
		}

		final ElectionEventEntity electionEventEntity = new ElectionEventEntity(electionEventId, encryptionGroupBytes);
		return electionEventRepository.save(electionEventEntity);
	}

	/**
	 * Retrieves the election event entity for a given election event id.
	 *
	 * @param electionEventId the election event id.
	 * @return the election event context.
	 * @throws ResourceNotFoundException if the election event context does not exist for the given id.
	 */
	public ElectionEventEntity retrieveElectionEventEntity(final String electionEventId)
			throws ResourceNotFoundException {
		validateUUID(electionEventId);

		// retrieve the election event context
		return electionEventRepository.findByElectionEventId(electionEventId);
	}
}
