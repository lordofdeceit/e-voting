/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.infrastructure.persistence;

import ch.post.it.evoting.votingserver.commons.infrastructure.persistence.BaseRepositoryImpl;
import ch.post.it.evoting.votingserver.voteverification.domain.model.content.SetupComponentVerificationCardKeystoreEntity;
import ch.post.it.evoting.votingserver.voteverification.domain.model.content.SetupComponentVerificationCardKeystoreRepository;

/**
 * Implementation of the repository with JPA
 */
public class SetupComponentVerificationCardKeystoreRepositoryImpl extends BaseRepositoryImpl<SetupComponentVerificationCardKeystoreEntity, String>
		implements SetupComponentVerificationCardKeystoreRepository {

}
