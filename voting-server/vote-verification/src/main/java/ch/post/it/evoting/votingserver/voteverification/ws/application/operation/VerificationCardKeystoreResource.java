/*
 * (c) Copyright 2022 Swiss Post Ltd
 */
package ch.post.it.evoting.votingserver.voteverification.ws.application.operation;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;

import java.io.IOException;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.domain.configuration.VerificationCardKeystore;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.commons.tracking.TrackIdInstance;
import ch.post.it.evoting.votingserver.commons.ui.Constants;
import ch.post.it.evoting.votingserver.voteverification.service.SetupComponentVerificationCardKeystoreService;

import jakarta.validation.constraints.NotNull;

/**
 * Web service for retrieving a verification card keystore.
 */
@Path("/api/v1/voting/sendvote/verificationcardkeystores")
@Stateless
public class VerificationCardKeystoreResource {

	private static final String PARAMETER_VALUE_ELECTION_EVENT_ID = "electionEventId";
	private static final String PARAMETER_VALUE_VERIFICATION_CARD_ID = "verificationCardId";
	private static final String PARAMETER_VALUE_VERIFICATION_CARD_SET_ID = "verificationCardSetId";
	private final SetupComponentVerificationCardKeystoreService setupComponentVerificationCardKeystoreService;
	private final TrackIdInstance trackIdInstance;

	@Inject
	public VerificationCardKeystoreResource(final TrackIdInstance trackIdInstance,
			final SetupComponentVerificationCardKeystoreService setupComponentVerificationCardKeystoreService) {
		this.trackIdInstance = trackIdInstance;
		this.setupComponentVerificationCardKeystoreService = setupComponentVerificationCardKeystoreService;
	}

	/**
	 * Returns a verification card keystore for a given election event, verification card set and verification card.
	 *
	 * @param trackingId            The track id to be used for logging purposes.
	 * @param electionEventId       The election event identifier.
	 * @param verificationCardSetId The verification card set identifier.
	 * @param verificationCardId    The verification card identifier.
	 * @param request               The http servlet request.
	 * @return a verification card keystore.
	 * @throws ResourceNotFoundException If verification card keystore is not found.
	 * @throws IOException               Conversion exception from object to json material data.
	 */
	@Path("/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/verificationcard/{verificationCardId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getVerificationCardKeystore(
			@NotNull
			@HeaderParam(Constants.PARAMETER_X_REQUEST_ID)
			final String trackingId,
			@PathParam(PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@PathParam(PARAMETER_VALUE_VERIFICATION_CARD_SET_ID)
			final String verificationCardSetId,
			@PathParam(PARAMETER_VALUE_VERIFICATION_CARD_ID)
			final String verificationCardId,
			@Context
			final HttpServletRequest request) throws IOException {

		trackIdInstance.setTrackId(trackingId);

		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		validateUUID(verificationCardId);

		final VerificationCardKeystore verificationCardKeystore = setupComponentVerificationCardKeystoreService.loadVerificationCardKeystore(
				electionEventId, verificationCardSetId, verificationCardId);

		final String verificationCardKeystoreJson = DomainObjectMapper.getNewInstance().writeValueAsString(verificationCardKeystore);
		return Response.ok().entity(verificationCardKeystoreJson).build();
	}

}
