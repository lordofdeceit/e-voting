/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.infrastructure.persistence;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.commons.infrastructure.persistence.BaseRepositoryImpl;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.ElectionEventEntity;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.SetupComponentPublicKeysEntity;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.SetupComponentPublicKeysRepository;

/**
 * Implementation of the repository with JPA
 */
public class SetupComponentPublicKeysRepositoryImpl extends BaseRepositoryImpl<SetupComponentPublicKeysEntity, Integer> implements
		SetupComponentPublicKeysRepository {

	@Override
	public SetupComponentPublicKeysEntity findByElectionEvent(final ElectionEventEntity electionEventEntity) throws ResourceNotFoundException {
		TypedQuery<SetupComponentPublicKeysEntity> query = entityManager.createQuery(
				"SELECT a FROM SetupComponentPublicKeysEntity a WHERE a.electionEventEntity = :electionEventEntity", SetupComponentPublicKeysEntity.class);
		query.setParameter("electionEventEntity", electionEventEntity);

		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			throw new ResourceNotFoundException(String.format("No setup component public keys found for electionElectionId: %s", electionEventEntity.getElectionEventId()), e);
		}
	}
}
