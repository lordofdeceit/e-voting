/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.service;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.EncryptionGroupUtils;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.DuplicateEntryException;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RetrofitConsumer;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RetrofitException;
import ch.post.it.evoting.votingserver.voteverification.domain.common.VotingClientPublicKeys;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.ElectionEventEntity;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.SetupComponentPublicKeysEntity;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.SetupComponentPublicKeysRepository;
import ch.post.it.evoting.votingserver.voteverification.infrastructure.remote.MessageBrokerOrchestratorClient;

@Stateless
public class SetupComponentPublicKeysService {
	private static final Logger LOGGER = LoggerFactory.getLogger(SetupComponentPublicKeysService.class);

	private static final String GROUP = "group";

	private final ObjectMapper objectMapper;
	private final SetupComponentPublicKeysRepository setupComponentPublicKeysRepository;
	private final ElectionEventService electionEventService;
	private final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient;

	@Inject
	SetupComponentPublicKeysService(final ObjectMapper objectMapper,
			final SetupComponentPublicKeysRepository setupComponentPublicKeysRepository,
			final ElectionEventService electionEventService,
			final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient) {
		this.objectMapper = objectMapper;
		this.setupComponentPublicKeysRepository = setupComponentPublicKeysRepository;
		this.electionEventService = electionEventService;
		this.messageBrokerOrchestratorClient = messageBrokerOrchestratorClient;
	}

	/**
	 * Saves the setup component public keys and uploads it to the control components.
	 *
	 * @param setupComponentPublicKeysPayload the request payload. Must be non null.
	 * @throws NullPointerException             if {@code setupComponentPublicKeysPayload} is null.
	 * @throws IllegalStateException            if an error occurred while verifying the signature of the election event context payload.
	 * @throws InvalidPayloadSignatureException if the signature of the election event context payload is invalid.
	 * @throws DuplicateEntryException          if the election event context is already saved in the database.
	 * @throws UncheckedIOException             if an error occurs while serializing the election event context.
	 * @throws RetrofitException                if an error occurs while saving from the message broker orchestrator.
	 */
	public void saveSetupComponentPublicKeys(final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload)
			throws DuplicateEntryException, RetrofitException {
		checkNotNull(setupComponentPublicKeysPayload);

		final String electionEventId = setupComponentPublicKeysPayload.getElectionEventId();

		// save setup component public keys in vote verification
		setupComponentPublicKeysRepository.save(
				getSetupComponentPublicKeysEntity(electionEventId, setupComponentPublicKeysPayload.getSetupComponentPublicKeys()));
		LOGGER.info("Setup component public keys successfully saved in vote verification. [electionEventId: {}]", electionEventId);

		// upload setup component public keys to control components
		RetrofitConsumer.processResponse(
				messageBrokerOrchestratorClient.uploadSetupComponentPublicKeys(electionEventId, setupComponentPublicKeysPayload));

		LOGGER.info("Setup component public keys successfully uploaded to the control components. [electionEventId: {}]", electionEventId);
	}

	private SetupComponentPublicKeysEntity getSetupComponentPublicKeysEntity(final String electionEventId,
			final SetupComponentPublicKeys setupComponentPublicKeys) {
		final byte[] serializedCombinedControlComponentPublicKeys;
		final byte[] serializedElectoralBoardPublicKey;
		final byte[] serializedElectoralBoardSchnorrProofs;
		final byte[] serializedElectionPublicKey;
		final byte[] serializedChoiceReturnCodesPublicKey;
		final ElectionEventEntity electionEventEntity;
		try {
			serializedCombinedControlComponentPublicKeys = objectMapper.writeValueAsBytes(
					setupComponentPublicKeys.combinedControlComponentPublicKeys());
			serializedElectoralBoardPublicKey = objectMapper.writeValueAsBytes(setupComponentPublicKeys.electoralBoardPublicKey());
			serializedElectoralBoardSchnorrProofs = objectMapper.writeValueAsBytes(setupComponentPublicKeys.electoralBoardSchnorrProofs());
			serializedElectionPublicKey = objectMapper.writeValueAsBytes(setupComponentPublicKeys.electionPublicKey());
			serializedChoiceReturnCodesPublicKey = objectMapper.writeValueAsBytes(setupComponentPublicKeys.choiceReturnCodesEncryptionPublicKey());
			electionEventEntity = electionEventService.retrieveElectionEventEntity(electionEventId);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException("Failed to serialize setup component public keys.", e);
		} catch (final ResourceNotFoundException e) {
			throw new IllegalStateException(
					String.format("Election event encryption parameters not found. [electionEventId: %s]", electionEventId),
					e);
		}

		return new SetupComponentPublicKeysEntity(electionEventEntity, serializedCombinedControlComponentPublicKeys,
				serializedElectoralBoardPublicKey,
				serializedElectoralBoardSchnorrProofs, serializedElectionPublicKey, serializedChoiceReturnCodesPublicKey);
	}

	/**
	 * Retrieves the election event context for a given election event id and recovers the voting client public keys using the given tenant id,
	 * election event id and verification card set id.
	 *
	 * @param tenantId              the tenant id.
	 * @param electionEventId       the election event id.
	 * @return the voting client public keys.
	 * @throws ResourceNotFoundException if
	 *                                   <ul>
	 *                                   	<li>There is no election event context with the given election event id.</li>
	 *                                   	<li>There is no verification content with the given tenant id, election event id and verification card set id.</li>
	 *                                   </ul>
	 */
	public VotingClientPublicKeys getVotingClientPublicKeys(final String tenantId, final String electionEventId) throws ResourceNotFoundException {
		checkNotNull(tenantId);
		validateUUID(electionEventId);

		// retrieve the setup component public keys
		final SetupComponentPublicKeysEntity setupComponentPublicKeysEntity;
		final ElectionEventEntity electionEventEntity = electionEventService.retrieveElectionEventEntity(electionEventId);
		try {
			setupComponentPublicKeysEntity = setupComponentPublicKeysRepository.findByElectionEvent(electionEventEntity);
			LOGGER.info("Setup component public keys found. [electionEventId: {}]", electionEventId);
		} catch (final ResourceNotFoundException e) {
			LOGGER.error("Setup component public keys not found. [electionEventId: {}]", electionEventId);
			throw e;
		}

		// retrieve group
		final JsonNode encryptionGroupNode;
		try {
			encryptionGroupNode = objectMapper.readTree(electionEventEntity.getEncryptionGroup());
		} catch (final IOException e) {
			throw new UncheckedIOException("Could not retrieve the group.", e);
		}
		final GqGroup encryptionGroup = EncryptionGroupUtils.getEncryptionGroup(objectMapper, encryptionGroupNode);
		LOGGER.info("Successfully retrieved the group.");

		// deserialize the voting client public keys
		final ElGamalMultiRecipientPublicKey electionPublicKey;
		final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey;
		try {
			electionPublicKey = objectMapper.reader()
					.withAttribute(GROUP, encryptionGroup)
					.readValue(setupComponentPublicKeysEntity.getElectionPublicKey(), ElGamalMultiRecipientPublicKey.class);
			choiceReturnCodesEncryptionPublicKey = objectMapper.reader()
					.withAttribute(GROUP, encryptionGroup)
					.readValue(setupComponentPublicKeysEntity.getChoiceReturnCodesEncryptionPublicKey(), ElGamalMultiRecipientPublicKey.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(String.format("Failed to deserialize the voting client public keys. [electionEventId: %s]",
					setupComponentPublicKeysEntity.getElectionEventEntity().getElectionEventId()), e);
		}

		LOGGER.info("Voting client public keys recovered. [electionEventId: {}]", electionEventId);
		return new VotingClientPublicKeys(encryptionGroup, electionPublicKey, choiceReturnCodesEncryptionPublicKey);
	}
}
