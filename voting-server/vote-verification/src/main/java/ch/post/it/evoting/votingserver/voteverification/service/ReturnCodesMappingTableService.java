/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.service;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.configuration.SetupComponentCMTablePayload;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.DuplicateEntryException;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.voteverification.domain.model.content.ReturnCodesMappingTableEntity;
import ch.post.it.evoting.votingserver.voteverification.domain.model.content.ReturnCodesMappingTableRepository;

/**
 * Saves and retrieves the return codes mapping table.
 */
@Stateless
public class ReturnCodesMappingTableService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReturnCodesMappingTableService.class);

	private final ObjectMapper objectMapper;
	private final ReturnCodesMappingTableRepository returnCodesMappingTableRepository;

	@Inject
	public ReturnCodesMappingTableService(final ObjectMapper objectMapper,
			final ReturnCodesMappingTableRepository returnCodesMappingTableRepository) {
		this.objectMapper = objectMapper;
		this.returnCodesMappingTableRepository = returnCodesMappingTableRepository;
	}

	/**
	 * Saves the return codes mapping table.
	 *
	 * @param setupComponentCMTablePayload the request payload. Must be non-null.
	 * @throws DuplicateEntryException if the return code mapping table is already saved in the database.
	 * @throws UncheckedIOException    if an error occurs while serializing the return code mapping table.
	 */
	public void saveReturnCodesMappingTable(final SetupComponentCMTablePayload setupComponentCMTablePayload)
			throws DuplicateEntryException, UncheckedIOException {
		checkNotNull(setupComponentCMTablePayload);

		final ReturnCodesMappingTableEntity returnCodesMappingTableEntity = new ReturnCodesMappingTableEntity();

		returnCodesMappingTableEntity.setVerificationCardSetId(setupComponentCMTablePayload.getVerificationCardSetId());
		returnCodesMappingTableEntity.setElectionEventId(setupComponentCMTablePayload.getElectionEventId());

		final byte[] serializedReturnCodesMappingTable;
		try {
			serializedReturnCodesMappingTable = objectMapper.writeValueAsBytes(
					new TreeMap<>(setupComponentCMTablePayload.getReturnCodesMappingTable()));
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException("Failed to serialize return codes mapping table.", e);
		}
		returnCodesMappingTableEntity.setMappingTable(serializedReturnCodesMappingTable);

		// save return codes mapping table in vote verification
		returnCodesMappingTableRepository.save(returnCodesMappingTableEntity);
		LOGGER.info("Return codes mapping table successfully saved in vote verification. [electionEventId: {}, verificationCardSetId: {}]",
				returnCodesMappingTableEntity.getElectionEventId(), returnCodesMappingTableEntity.getVerificationCardSetId());
	}

	/**
	 * Retrieves the return codes mapping table for given a verification card set id.
	 *
	 * @param verificationCardSetId the verification card set id. Must be non-null and a valid UUID.
	 * @return the return codes mapping table.
	 * @throws FailedValidationException if {@code verificationCardSetId} is invalid.
	 * @throws ResourceNotFoundException if return codes mapping table is not found.
	 */
	public SortedMap<String, String> retrieveReturnCodesMappingTable(final String verificationCardSetId) throws ResourceNotFoundException {
		validateUUID(verificationCardSetId);

		final ReturnCodesMappingTableEntity returnCodesMappingTableEntity = returnCodesMappingTableRepository.find(verificationCardSetId);
		if (returnCodesMappingTableEntity == null) {
			throw new ResourceNotFoundException(
					String.format("No return codes mapping table found. [verificationCardSetId: %s]", verificationCardSetId));
		}

		try {
			return objectMapper.readValue(returnCodesMappingTableEntity.getMappingTable(), new TypeReference<TreeMap<String, String>>() {
			});
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Cannot deserialize the return codes mapping table. [electionEventId: %s, verificationCardSetId: %s]",
							returnCodesMappingTableEntity.getElectionEventId(), returnCodesMappingTableEntity.getVerificationCardSetId()), e);
		}
	}
}
