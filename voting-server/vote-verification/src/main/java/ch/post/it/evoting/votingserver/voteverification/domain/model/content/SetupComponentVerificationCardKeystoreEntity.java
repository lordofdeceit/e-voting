/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.domain.model.content;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "SETUP_COMPONENT_VERIFICATION_CARD_KEYSTORE", uniqueConstraints = {
		@UniqueConstraint(name = "SETUP_COMPONENT_VERIFICATION_CARD_KEYSTORE_UK1", columnNames = { "ELECTION_EVENT_ID", "VERIFICATION_CARD_SET_ID",
				"VERIFICATION_CARD_ID" }) })
public class SetupComponentVerificationCardKeystoreEntity {

	@Id
	@Column(name = "VERIFICATION_CARD_ID")
	private String verificationCardId;

	@Column(name = "ELECTION_EVENT_ID")
	@NotNull
	private String electionEventId;

	@Column(name = "VERIFICATION_CARD_SET_ID")
	@NotNull
	private String verificationCardSetId;

	@Lob
	@Column(name = "VERIFICATION_CARD_KEYSTORE")
	@NotNull
	private byte[] verificationCardKeystore;

	@Version
	@Column(name = "CHANGE_CONTROL_ID")
	private Integer changeControlId;

	public String getVerificationCardId() {
		return verificationCardId;
	}

	public void setVerificationCardId(final String verificationCardId) {
		this.verificationCardId = verificationCardId;
	}

	public String getElectionEventId() {
		return electionEventId;
	}

	public void setElectionEventId(final String electionEventId) {
		this.electionEventId = electionEventId;
	}

	public String getVerificationCardSetId() {
		return verificationCardSetId;
	}

	public void setVerificationCardSetId(final String verificationCardSetId) {
		this.verificationCardSetId = verificationCardSetId;
	}

	public byte[] getVerificationCardKeystore() {
		return verificationCardKeystore;
	}

	public void setVerificationCardKeystore(final byte[] verificationCardKeyStore) {
		this.verificationCardKeystore = verificationCardKeyStore;
	}

	public Integer getChangeControlId() {
		return changeControlId;
	}

	public void setChangeControlId(final Integer changeControlId) {
		this.changeControlId = changeControlId;
	}

}
