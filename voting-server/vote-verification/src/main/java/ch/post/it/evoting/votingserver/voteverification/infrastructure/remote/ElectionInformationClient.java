/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.infrastructure.remote;

import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.commons.ui.Constants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * The Interface for election information client.
 */
public interface ElectionInformationClient {

	/**
	 * Gets the ballot.
	 *
	 * @param trackId         the track id
	 * @param pathBallot      the path ballot
	 * @param tenantId        the tenant id
	 * @param electionEventId the election event id
	 * @param ballotId        the ballot id
	 * @return the ballot
	 * @throws ResourceNotFoundException the resource not found exception
	 */
	@GET("{pathBallot}/tenant/{tenantId}/electionevent/{electionEventId}/ballot/{ballotId}")
	Call<String> getBallot(
			@Header(Constants.PARAMETER_X_REQUEST_ID)
					String trackId,
			@Path(Constants.PARAMETER_PATH_BALLOT)
					String pathBallot,
			@Path(Constants.PARAMETER_VALUE_TENANT_ID)
					String tenantId,
			@Path(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
					String electionEventId,
			@Path(Constants.PARAMETER_VALUE_BALLOT_ID)
					String ballotId) throws ResourceNotFoundException;
}
