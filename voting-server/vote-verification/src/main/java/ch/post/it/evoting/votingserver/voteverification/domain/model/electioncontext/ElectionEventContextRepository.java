/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext;

import java.util.Optional;

import javax.ejb.Local;

import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.commons.domain.model.BaseRepository;

/**
 * Repository for handling ElectionEventContext entities
 */
@Local
public interface ElectionEventContextRepository extends BaseRepository<ElectionEventContextEntity, Integer> {

	/**
	 * Searches for a setup component public keys with the given election event id.
	 *
	 * @param electionEventEntity the election event
	 * @return an entity representing the election event context
	 * @throws ResourceNotFoundException if the election event context with the given election event id is not found.
	 */
	Optional<ElectionEventContextEntity> findByElectionEvent(final ElectionEventEntity electionEventEntity);

}
