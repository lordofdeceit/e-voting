/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import ch.post.it.evoting.votingserver.commons.domain.model.Constants;

@Entity
@Table(name = "ELECTION_EVENT")
public class ElectionEventEntity {

	@Id
	@Column(name = "ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ELECTION_EVENT_SEQ_GENERATOR")
	@SequenceGenerator(sequenceName = "ELECTION_EVENT_SEQ", allocationSize = 1, name = "ELECTION_EVENT_SEQ_GENERATOR")
	private Long id;

	@Column(name = "ELECTION_EVENT_ID")
	@NotNull
	@Size(max = Constants.COLUMN_LENGTH_100)
	private String electionEventId;

	@Column(name = "ENCRYPTION_GROUP")
	@NotNull
	private byte[] encryptionGroup;

	@Version
	@Column(name = "CHANGE_CONTROL_ID")
	@NotNull
	private Integer changeControlId;

	public ElectionEventEntity() {
		// Needed by repository.
	}

	public ElectionEventEntity(final String electionEventId, final byte[] encryptionGroup) {
		this.electionEventId = checkNotNull(electionEventId);
		this.encryptionGroup = checkNotNull(encryptionGroup);
	}

	public String getElectionEventId() {
		return electionEventId;
	}

	public void setElectionEventId(final String electionEventId) {
		this.electionEventId = electionEventId;
	}

	public byte[] getEncryptionGroup() {
		return encryptionGroup;
	}

	public void setEncryptionGroup(final byte[] encryptionGroup) {
		this.encryptionGroup = encryptionGroup;
	}

	public Integer getChangeControlId() {
		return changeControlId;
	}

}
