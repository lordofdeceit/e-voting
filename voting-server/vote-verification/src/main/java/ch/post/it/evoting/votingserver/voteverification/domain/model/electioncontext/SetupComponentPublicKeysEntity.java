/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "SETUP_COMPONENT_PUBLIC_KEYS")
public class SetupComponentPublicKeysEntity {

	@Id
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	@JoinColumn(name = "ELECTION_EVENT_FK_ID")
	private ElectionEventEntity electionEventEntity;

	@Lob
	@Column(name = "COMBINED_CONTROL_COMPONENT_PUBLIC_KEYS")
	@NotNull
	private byte[] combinedControlComponentPublicKeys;

	@Lob
	@Column(name = "ELECTORAL_BOARD_PUBLIC_KEY")
	@NotNull
	private byte[] electoralBoardPublicKey;

	@Lob
	@Column(name = "ELECTORAL_BOARD_SCHNORR_PROOFS")
	@NotNull
	private byte[] electoralBoardSchnorrProofs;

	@Lob
	@Column(name = "ELECTION_PUBLIC_KEY")
	@NotNull
	private byte[] electionPublicKey;

	@Lob
	@Column(name = "CHOICE_RETURN_CODES_ENCRYPTION_PUBLIC_KEY")
	@NotNull
	private byte[] choiceReturnCodesEncryptionPublicKey;

	@Version
	@Column(name = "CHANGE_CONTROL_ID")
	@NotNull
	private Integer changeControlId;

	public SetupComponentPublicKeysEntity() {
		// needed by the repository.
	}

	public SetupComponentPublicKeysEntity(final ElectionEventEntity electionEventEntity, final byte[] combinedControlComponentPublicKeys,
			final byte[] electoralBoardPublicKey, final byte[] electoralBoardSchnorrProofs, final byte[] electionPublicKey,
			final byte[] choiceReturnCodesEncryptionPublicKey) {
		this.electionEventEntity = checkNotNull(electionEventEntity);
		this.combinedControlComponentPublicKeys = checkNotNull(combinedControlComponentPublicKeys);
		this.electoralBoardPublicKey = checkNotNull(electoralBoardPublicKey);
		this.electoralBoardSchnorrProofs = checkNotNull(electoralBoardSchnorrProofs);
		this.electionPublicKey = checkNotNull(electionPublicKey);
		this.choiceReturnCodesEncryptionPublicKey = checkNotNull(choiceReturnCodesEncryptionPublicKey);
	}

	public ElectionEventEntity getElectionEventEntity() {
		return electionEventEntity;
	}

	public byte[] getCombinedControlComponentPublicKeys() {
		return combinedControlComponentPublicKeys;
	}

	public byte[] getElectoralBoardPublicKey() {
		return electoralBoardPublicKey;
	}

	public byte[] getElectoralBoardSchnorrProofs() {
		return electoralBoardSchnorrProofs;
	}

	public byte[] getElectionPublicKey() {
		return electionPublicKey;
	}

	public byte[] getChoiceReturnCodesEncryptionPublicKey() {
		return choiceReturnCodesEncryptionPublicKey;
	}

	public Integer getChangeControlId() {
		return changeControlId;
	}

}
