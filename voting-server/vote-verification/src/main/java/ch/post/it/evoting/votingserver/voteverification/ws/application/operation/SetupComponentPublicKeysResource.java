/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.ws.application.operation;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.security.SignatureException;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.ChannelSecurityContextData;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ApplicationException;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ApplicationExceptionMessages;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.DuplicateEntryException;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RetrofitException;
import ch.post.it.evoting.votingserver.commons.tracking.TrackIdInstance;
import ch.post.it.evoting.votingserver.commons.ui.Constants;
import ch.post.it.evoting.votingserver.commons.ui.ws.rs.persistence.ErrorCodes;
import ch.post.it.evoting.votingserver.voteverification.domain.common.VotingClientPublicKeys;
import ch.post.it.evoting.votingserver.voteverification.service.SetupComponentPublicKeysService;

/**
 * Web service for retrieving the voting client public keys.
 */
@Stateless
public class SetupComponentPublicKeysResource {

	static final String RESOURCE_PATH = "api/v1/configuration/setupkeys";

	private static final Logger LOGGER = LoggerFactory.getLogger(SetupComponentPublicKeysResource.class);

	@Inject
	private SetupComponentPublicKeysService setupComponentPublicKeysService;

	@Inject
	private SignatureKeystore<Alias> signatureKeystoreService;

	@Inject
	private TrackIdInstance tackIdInstance;

	@Path("api/v1/configuration/setupkeys/electionevent/{electionEventId}")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveSetupComponentPublicKeys(
			@PathParam(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@NotNull
			@HeaderParam(Constants.PARAMETER_X_REQUEST_ID)
			final String trackingId,
			@NotNull
			final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload,
			@Context
			final HttpServletRequest request) throws ApplicationException {

		validateInput(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID, electionEventId);
		verifyPayloadSignature(setupComponentPublicKeysPayload);
		tackIdInstance.setTrackId(trackingId);

		try {
			setupComponentPublicKeysService.saveSetupComponentPublicKeys(setupComponentPublicKeysPayload);
		} catch (final DuplicateEntryException e) {
			LOGGER.warn("Duplicate entry tried to be inserted. [electionEventId: {}]", electionEventId);
			return Response.noContent().build();
		} catch (final RetrofitException e) {
			LOGGER.warn("Error trying to save setup component public keys. [electionEventId: {}]", electionEventId);
			return Response.status(e.getHttpCode()).build();
		}

		LOGGER.info("Successfully uploaded and saved the setup component public keys. [electionEventId: {}]", electionEventId);

		return Response.ok().build();
	}

	@Path("/setupkeys/tenant/{tenantId}/electionevent/{electionEventId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getVotingClientPublicKeys(
			@NotNull
			@HeaderParam(Constants.PARAMETER_X_REQUEST_ID)
			final String trackingId,
			@PathParam(Constants.PARAMETER_VALUE_TENANT_ID)
			final String tenantId,
			@PathParam(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@Context
			final HttpServletRequest request) throws IOException, ApplicationException {

		validateInput(tenantId);
		validateInput(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID, electionEventId);

		tackIdInstance.setTrackId(trackingId);

		final VotingClientPublicKeys votingClientPublicKeys;
		try {
			votingClientPublicKeys = setupComponentPublicKeysService.getVotingClientPublicKeys(tenantId, electionEventId);
		} catch (final ResourceNotFoundException e) {
			LOGGER.warn("No voting client public keys found. [electionEventId: {}]", electionEventId);
			return Response.noContent().build();
		}

		final String jsonVotingClientPublicKeys = DomainObjectMapper.getNewInstance().writeValueAsString(votingClientPublicKeys);
		return Response.ok().entity(jsonVotingClientPublicKeys).build();
	}

	private void validateInput(final String field, final String pathId) throws ApplicationException {
		try {
			validateUUID(pathId);
		} catch (final FailedValidationException | NullPointerException e) {
			throw new ApplicationException(ApplicationExceptionMessages.EXCEPTION_MESSAGE_QUERY_PARAMETER_IS_NULL, RESOURCE_PATH,
					ErrorCodes.MANDATORY_FIELD, field);
		}
	}

	private void validateInput(final String tenantId) throws ApplicationException {
		try {
			checkNotNull(tenantId);
		} catch (final FailedValidationException | NullPointerException e) {
			throw new ApplicationException(ApplicationExceptionMessages.EXCEPTION_MESSAGE_QUERY_PARAMETER_IS_NULL, RESOURCE_PATH,
					ErrorCodes.MANDATORY_FIELD, Constants.PARAMETER_VALUE_TENANT_ID);
		}
	}

	private void verifyPayloadSignature(final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload) {
		final String electionEventId = setupComponentPublicKeysPayload.getElectionEventId();

		final CryptoPrimitivesSignature signature = setupComponentPublicKeysPayload.getSignature();

		checkState(signature != null, "The signature of the setup component public keys payload is null. [electionEventId: %s]", electionEventId);

		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentPublicKeys(electionEventId);

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreService.verifySignature(Alias.SDM_CONFIG, setupComponentPublicKeysPayload,
					additionalContextData, signature.signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not verify the signature of the setup component public keys. [electionEventId: %s]", electionEventId));
		}

		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(SetupComponentPublicKeysPayload.class,
					String.format("[electionEventId: %s]", electionEventId));
		}
	}
}
