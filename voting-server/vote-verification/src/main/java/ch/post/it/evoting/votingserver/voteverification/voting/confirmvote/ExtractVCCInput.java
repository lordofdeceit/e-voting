/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.voting.confirmvote;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.votingserver.voteverification.voting.confirmvote.ExtractVCCAlgorithm.NUMBER_OF_CONTROL_COMPONENTS;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collections;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;

/**
 * Regroups the inputs needed by the ExtractVCC algorithm.
 */
@SuppressWarnings("java:S115")
public class ExtractVCCInput {
	private static final int l_HB64 = 44;
	private final GroupVector<GqElement, GqGroup> longVoteCastReturnCodeShares;
	private final String verificationCardId;
	private final TreeMap<String, String> returnCodesMappingTable;

	/**
	 * @param longVoteCastReturnCodeShares (lCC<sub>1,id</sub>, lCC<sub>2,id</sub>, lCC<sub>3,id</sub>, lCC<sub>4,id</sub>) ∈
	 *                                     G<sub>q</sub><sup>4</sup>, CCR long Vote Cast Return Code shares.
	 * @param verificationCardId           vc<sub>id</sub>, the verification card id.
	 * @param returnCodesMappingTable      CMtable, the return codes mapping table.
	 * @throws NullPointerException      if any of the fields is null.
	 * @throws IllegalArgumentException  if
	 *                                   <ul>
	 *                                       <li>The {@code longVoteCastReturnCodeShares} size is not {@value NUMBER_OF_CONTROL_COMPONENTS}.</li>
	 *                                       <li>The {@code returnCodesMappingTable} keys length are not {@value l_HB64}</li>
	 *                                   </ul>
	 * @throws FailedValidationException if the {@code verificationCardId} do not comply the UUID format.
	 */
	private ExtractVCCInput(final GroupVector<GqElement, GqGroup> longVoteCastReturnCodeShares, final String verificationCardId,
			final SortedMap<String, String> returnCodesMappingTable) {
		checkNotNull(longVoteCastReturnCodeShares);
		validateUUID(verificationCardId);
		checkNotNull(returnCodesMappingTable);

		final List<GqElement> longVoteCastReturnCodeSharesCopy = List.copyOf(longVoteCastReturnCodeShares);
		final TreeMap<String, String> returnCodesMappingTableCopy = new TreeMap<>(returnCodesMappingTable);

		checkArgument(longVoteCastReturnCodeSharesCopy.size() == NUMBER_OF_CONTROL_COMPONENTS,
				String.format("There must be long Vote Cast Return Code shares from %s control-components.", NUMBER_OF_CONTROL_COMPONENTS));

		// Values length check
		checkArgument(returnCodesMappingTableCopy.keySet().stream().allMatch(key -> key.length() == l_HB64),
				"Return Codes Mapping table keys must have a length of " + l_HB64);

		this.longVoteCastReturnCodeShares = longVoteCastReturnCodeShares;
		this.verificationCardId = verificationCardId;
		this.returnCodesMappingTable = returnCodesMappingTableCopy;
	}

	public List<GqElement> getLongVoteCastReturnCodeShares() {
		return longVoteCastReturnCodeShares;
	}

	public String getVerificationCardId() {
		return verificationCardId;
	}

	public SortedMap<String, String> getReturnCodesMappingTable() {
		return Collections.unmodifiableSortedMap(returnCodesMappingTable);
	}

	public GqGroup getGroup() {
		return this.longVoteCastReturnCodeShares.get(0).getGroup();
	}

	public static class Builder {
		private GroupVector<GqElement, GqGroup> longVoteCastReturnCodeShares;
		private String verificationCardId;
		private SortedMap<String, String> returnCodesMappingTable;

		public Builder setLongVoteCastReturnCodeShares(final GroupVector<GqElement, GqGroup> longVoteCastReturnCodeShares) {
			this.longVoteCastReturnCodeShares = longVoteCastReturnCodeShares;
			return this;
		}

		public Builder setVerificationCardId(final String verificationCardId) {
			this.verificationCardId = verificationCardId;
			return this;
		}

		public Builder setReturnCodesMappingTable(final SortedMap<String, String> returnCodesMappingTable) {
			this.returnCodesMappingTable = Collections.unmodifiableSortedMap(returnCodesMappingTable);
			return this;
		}

		public ExtractVCCInput build() {
			return new ExtractVCCInput(longVoteCastReturnCodeShares, verificationCardId, returnCodesMappingTable);
		}
	}

}
