/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.voting.confirmvote;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

/**
 * Regroups the context values needed by the ExtractVCC algorithm.
 */
public record ExtractVCCContext(GqGroup encryptionGroup, String electionEventId) {
	/**
	 * @throws NullPointerException      if any of the fields is null.
	 * @throws FailedValidationException if the {@code electionEventId} do not comply the UUID format.
	 */
	public ExtractVCCContext {
		checkNotNull(encryptionGroup);
		validateUUID(electionEventId);
	}

}
