/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.service;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.security.SignatureException;
import java.util.Arrays;
import java.util.List;
import java.util.SortedMap;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.annotations.VisibleForTesting;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.EncryptionGroupUtils;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.PlaintextEqualityProof;
import ch.post.it.evoting.domain.ChannelSecurityContextData;
import ch.post.it.evoting.domain.ContextIds;
import ch.post.it.evoting.domain.EncryptedVerifiableVote;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.election.model.vote.Vote;
import ch.post.it.evoting.domain.returncodes.ComputeResults;
import ch.post.it.evoting.domain.returncodes.ShortChoiceReturnCodeAndComputeResults;
import ch.post.it.evoting.domain.returncodes.VoteAndComputeResults;
import ch.post.it.evoting.domain.voting.sendvote.CombinedControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentLCCSharePayload;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.LongChoiceReturnCodesShare;
import ch.post.it.evoting.domain.voting.sendvote.PartiallyDecryptedEncryptedPCC;
import ch.post.it.evoting.domain.voting.sendvote.VotingServerEncryptedVotePayload;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RetrofitConsumer;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RetrofitException;
import ch.post.it.evoting.votingserver.voteverification.domain.model.content.SetupComponentVerificationCardKeystoreEntity;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.ElectionEventEntity;
import ch.post.it.evoting.votingserver.voteverification.infrastructure.remote.MessageBrokerOrchestratorClient;
import ch.post.it.evoting.votingserver.voteverification.voting.sendvote.ExtractCRCAlgorithm;
import ch.post.it.evoting.votingserver.voteverification.voting.sendvote.ExtractCRCContext;
import ch.post.it.evoting.votingserver.voteverification.voting.sendvote.ExtractCRCInput;
import ch.post.it.evoting.votingserver.voteverification.voting.sendvote.ExtractCRCOutput;

/**
 * Generate the short choice return codes based on the encrypted partial choice return codes - in interaction with the control components.
 */
@Stateless
public class ChoiceReturnCodesService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChoiceReturnCodesService.class);
	private static final String GROUP_ATTRIBUTE = "group";

	private final ObjectMapper objectMapper;
	private final ExtractCRCAlgorithm extractCRCAlgorithm;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final ReturnCodesMappingTableService returnCodesMappingTableService;
	private final ElectionEventService electionEventEncryptionParametersService;
	private final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient;
	private final SetupComponentVerificationCardKeystoreService setupComponentVerificationCardKeystoreService;

	@Inject
	ChoiceReturnCodesService(
			final ObjectMapper objectMapper,
			final ExtractCRCAlgorithm extractCRCAlgorithm,
			final SignatureKeystore<Alias> signatureKeystoreService,
			final ElectionEventService electionEventEncryptionParametersService,
			final ReturnCodesMappingTableService returnCodesMappingTableService,
			final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient,
			final SetupComponentVerificationCardKeystoreService setupComponentVerificationCardKeystoreService) {

		this.objectMapper = objectMapper;
		this.extractCRCAlgorithm = extractCRCAlgorithm;
		this.signatureKeystoreService = signatureKeystoreService;
		this.electionEventEncryptionParametersService = electionEventEncryptionParametersService;
		this.returnCodesMappingTableService = returnCodesMappingTableService;
		this.messageBrokerOrchestratorClient = messageBrokerOrchestratorClient;
		this.setupComponentVerificationCardKeystoreService = setupComponentVerificationCardKeystoreService;
	}

	/**
	 * Retrieves the Short Choice Return Codes from the vote with the help of the control components.
	 *
	 * @param tenantId              the tenant id.
	 * @param electionEventId       the election event id.
	 * @param verificationCardId    the verification card id.
	 * @param voteAndComputeResults the object containing the vote and optionally previous computation results.
	 * @return the Short Choice Return Codes.
	 * @throws IOException               if any deserialization error occurs.
	 * @throws ResourceNotFoundException if an error occurs while accessing the database or calling the orchestrator.
	 * @throws IllegalStateException     if an error occurs signing the voting server encrypted vote payload.
	 */
	@SuppressWarnings("java:S117")
	public ShortChoiceReturnCodeAndComputeResults retrieveShortChoiceReturnCodes(final String tenantId, final String electionEventId,
			final String verificationCardId, final VoteAndComputeResults voteAndComputeResults) throws IOException, ResourceNotFoundException {

		final Vote vote = voteAndComputeResults.getVote();
		final SetupComponentVerificationCardKeystoreEntity setupComponentVerificationCardKeystoreEntity = setupComponentVerificationCardKeystoreService.load(
				electionEventId, verificationCardId);
		final String verificationCardSetId = setupComponentVerificationCardKeystoreEntity.getVerificationCardSetId();

		checkArgument(vote.getElectionEventId().equals(electionEventId));
		checkArgument(vote.getVerificationCardSetId().equals(verificationCardSetId));
		checkArgument(vote.getVerificationCardId().equals(verificationCardId));

		final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);

		final List<ControlComponentLCCSharePayload> controlComponentLCCSharePayloads;

		final ComputeResults computeResults = voteAndComputeResults.getComputeResults();
		if (computeResults != null && computeResults.getComputationResults() != null && !computeResults.getComputationResults().isEmpty()) {
			LOGGER.info("Long Choice Return Codes previously computed, extracting short codes. [contextIds: {}]", contextIds);

			// The long Choice Return Codes have been previously computed. This can happen during a re-login event.
			final String computationResultsString = computeResults.getComputationResults();
			controlComponentLCCSharePayloads = Arrays.asList(
					objectMapper.readValue(computationResultsString, ControlComponentLCCSharePayload[].class));
		} else {
			LOGGER.info("Long Choice Return Codes not previously computed, requesting control components. [contextIds: {}]", contextIds);

			// Transform Vote to EncryptedVerifiableVote.
			final EncryptedVerifiableVote encryptedVerifiableVote = voteToVerifiableVote(electionEventId, verificationCardSetId,
					verificationCardId, vote);

			// Create and sign VotingServerEncryptedVotePayload with secret signing key.
			final GqGroup gqGroup = encryptedVerifiableVote.encryptedVote().getGroup();
			final VotingServerEncryptedVotePayload votingServerEncryptedVotePayload = new VotingServerEncryptedVotePayload(gqGroup,
					encryptedVerifiableVote);

			final CryptoPrimitivesSignature signature = getPayloadSignature(votingServerEncryptedVotePayload);
			votingServerEncryptedVotePayload.setSignature(signature);

			// Ask the control components to partially decrypt the pCC.
			final List<ControlComponentPartialDecryptPayload> controlComponentPartialDecryptPayloads = decryptEncryptedPartialChoiceReturnCodes(
					contextIds, votingServerEncryptedVotePayload);

			verifyPCCPayloads(controlComponentPartialDecryptPayloads);

			// Combine response payloads.
			final CombinedControlComponentPartialDecryptPayload combinedPCCPayloads = new CombinedControlComponentPartialDecryptPayload(
					controlComponentPartialDecryptPayloads);

			// Ask the control components to compute the Long Choice Return Codes shares. The DecryptPCC_j will be done at same time by the CCs.
			controlComponentLCCSharePayloads = retrieveLongChoiceReturnCodesShares(contextIds, combinedPCCPayloads);
		}

		// Retrieve short Choice Return Codes by combining CCR shares and looking up the CMTable.
		final List<GroupVector<GqElement, GqGroup>> lCCShares = getLCCShares(controlComponentLCCSharePayloads);
		final ExtractCRCContext extractCRCContext = new ExtractCRCContext.Builder()
				.setEncryptionGroup(lCCShares.get(0).getGroup())
				.setTenantId(tenantId)
				.setElectionEventId(electionEventId)
				.setBallotId(vote.getBallotId())
				.build();

		final SortedMap<String, String> CMtable = returnCodesMappingTableService.retrieveReturnCodesMappingTable(verificationCardSetId);
		final ExtractCRCInput extractCRCInput = new ExtractCRCInput.Builder()
				.setLongChoiceReturnCodeShares(lCCShares)
				.setVerificationCardId(verificationCardId)
				.setReturnCodesMappingTable(CMtable)
				.build();
		final ExtractCRCOutput shortChoiceReturnCodesOutput = extractCRCAlgorithm.extractCRC(extractCRCContext, extractCRCInput);

		LOGGER.info("Short Choice Return Codes successfully retrieved. [contextIds: {}]", contextIds);

		return createResponse(shortChoiceReturnCodesOutput.getShortChoiceReturnCodes(), controlComponentLCCSharePayloads);
	}

	private CryptoPrimitivesSignature getPayloadSignature(final VotingServerEncryptedVotePayload payload) {
		final ContextIds contextIds = payload.getEncryptedVerifiableVote().contextIds();

		final Hashable additionalContextData = ChannelSecurityContextData.votingServerEncryptedVote(contextIds.electionEventId(),
				contextIds.verificationCardSetId(), contextIds.verificationCardId());
		try {
			final byte[] signature = signatureKeystoreService.generateSignature(payload, additionalContextData);
			return new CryptoPrimitivesSignature(signature);
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not generate voting server encrypted vote payload signature. [contextIds: %s]", contextIds));
		}
	}

	private void verifyPCCPayloads(final List<ControlComponentPartialDecryptPayload> controlComponentPartialDecryptPayloads) {

		final boolean isGqGroupEquals = controlComponentPartialDecryptPayloads.stream()
				.map(ControlComponentPartialDecryptPayload::getEncryptionGroup)
				.distinct()
				.count() == 1;

		if (!isGqGroupEquals) {
			throw new IllegalStateException("GqGroup is not identical for all the payloads.");
		}

		final boolean isContextIdsEquals = controlComponentPartialDecryptPayloads.stream()
				.map(ControlComponentPartialDecryptPayload::getPartiallyDecryptedEncryptedPCC)
				.map(PartiallyDecryptedEncryptedPCC::contextIds)
				.distinct()
				.count() == 1;

		if (!isContextIdsEquals) {
			throw new IllegalStateException("ContextIds are not identical for all the payloads.");
		}
	}

	/**
	 * Converts the {@link Vote} to a {@link EncryptedVerifiableVote}.
	 */
	private EncryptedVerifiableVote voteToVerifiableVote(final String electionEventId, final String verificationCardSetId,
			final String verificationCardId, final Vote vote) throws IOException, ResourceNotFoundException {

		// Retrieve group.
		final ElectionEventEntity electionEventEncryptionParametersEntity = electionEventEncryptionParametersService.retrieveElectionEventEntity(
				electionEventId);
		final JsonNode encryptionGroupNode = objectMapper.readTree(electionEventEncryptionParametersEntity.getEncryptionGroup());
		final GqGroup encryptionGroup = EncryptionGroupUtils.getEncryptionGroup(objectMapper, encryptionGroupNode);

		// Read ids, encrypted vote and proofs.
		final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);

		final ElGamalMultiRecipientCiphertext encryptedVote = objectMapper.reader()
				.withAttribute(GROUP_ATTRIBUTE, encryptionGroup)
				.readValue(vote.getEncryptedVote(), ElGamalMultiRecipientCiphertext.class);

		final ElGamalMultiRecipientCiphertext exponentiatedEncryptedVote = objectMapper.reader()
				.withAttribute(GROUP_ATTRIBUTE, encryptionGroup)
				.readValue(vote.getExponentiatedEncryptedVote(), ElGamalMultiRecipientCiphertext.class);

		final ElGamalMultiRecipientCiphertext encryptedPartialChoiceReturnCodes = objectMapper.reader()
				.withAttribute(GROUP_ATTRIBUTE, encryptionGroup)
				.readValue(vote.getEncryptedPartialChoiceReturnCodes(), ElGamalMultiRecipientCiphertext.class);

		final ExponentiationProof exponentiationProof = objectMapper.reader()
				.withAttribute(GROUP_ATTRIBUTE, encryptionGroup)
				.readValue(vote.getExponentiationProof(), ExponentiationProof.class);

		final PlaintextEqualityProof plaintextEqualityProof = objectMapper.reader()
				.withAttribute(GROUP_ATTRIBUTE, encryptionGroup)
				.readValue(vote.getPlaintextEqualityProof(), PlaintextEqualityProof.class);

		return new EncryptedVerifiableVote(contextIds, encryptedVote, exponentiatedEncryptedVote, encryptedPartialChoiceReturnCodes,
				exponentiationProof, plaintextEqualityProof);
	}

	/**
	 * Calls the orchestrator which asks the control components to execute PartialDecryptPCC_j algorithm and collects their contributions.
	 */
	private List<ControlComponentPartialDecryptPayload> decryptEncryptedPartialChoiceReturnCodes(final ContextIds contextIds,
			final VotingServerEncryptedVotePayload votingServerEncryptedVotePayload) throws RetrofitException {

		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();

		// Call orchestrator.
		final List<ControlComponentPartialDecryptPayload> controlComponentPartialDecryptPayloads = RetrofitConsumer.processResponse(
				messageBrokerOrchestratorClient.getChoiceReturnCodesPartialDecryptContributions(electionEventId, verificationCardSetId,
						verificationCardId, votingServerEncryptedVotePayload));
		LOGGER.info("Partial decryptions received from the control-components. contextIds: {}]", contextIds);

		return controlComponentPartialDecryptPayloads;
	}

	/**
	 * Extracts the lCC shares from the contributions.
	 */
	@VisibleForTesting
	List<GroupVector<GqElement, GqGroup>> getLCCShares(final List<ControlComponentLCCSharePayload> longReturnCodesSharePayloads) {
		return longReturnCodesSharePayloads.stream()
				.map(ControlComponentLCCSharePayload::getLongChoiceReturnCodesShare)
				.map(LongChoiceReturnCodesShare::longChoiceReturnCodeShare)
				.toList();
	}

	/**
	 * Asks the control components to compute the Long Choice Return Codes.
	 */
	private List<ControlComponentLCCSharePayload> retrieveLongChoiceReturnCodesShares(final ContextIds contextIds,
			final CombinedControlComponentPartialDecryptPayload combinedControlComponentPartialDecryptPayload) throws ResourceNotFoundException {

		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();

		final List<ControlComponentLCCSharePayload> controlComponentLCCSharePayloads = RetrofitConsumer.processResponse(
				messageBrokerOrchestratorClient.getLongChoiceReturnCodesContributions(electionEventId, verificationCardSetId, verificationCardId,
						combinedControlComponentPartialDecryptPayload));

		// Choice Return Codes computation response correctly received.
		LOGGER.info("Retrieved the long Choice Return Code shares payloads. [contextIds: {}]", contextIds);
		verifySharesPayloadSignatures(contextIds, controlComponentLCCSharePayloads);

		return controlComponentLCCSharePayloads;
	}

	/**
	 * Verifies the signatures of the received {@link ControlComponentLCCSharePayload}s.
	 */
	private void verifySharesPayloadSignatures(final ContextIds contextIds,
			final List<ControlComponentLCCSharePayload> controlComponentLCCSharePayloads) {
		for (final ControlComponentLCCSharePayload payload : controlComponentLCCSharePayloads) {
			final int nodeId = payload.getLongChoiceReturnCodesShare().nodeId();
			final Hashable additionalContextData = ChannelSecurityContextData.controlComponentLCCShare(nodeId, contextIds.electionEventId(),
					contextIds.verificationCardSetId(), contextIds.verificationCardId());

			final CryptoPrimitivesSignature signature = payload.getSignature();

			checkState(signature != null, "The signature of the long return codes share payload is null. [nodeId: %s, contextIds: %s]",
					nodeId, contextIds);

			final boolean isSignatureValid;
			try {
				isSignatureValid = signatureKeystoreService.verifySignature(Alias.getControlComponentByNodeId(nodeId), payload, additionalContextData,
						signature.signatureContents());
			} catch (final SignatureException e) {
				throw new IllegalStateException(
						String.format("Could not verify the signature of the long return codes share payload. [nodeId: %s, contextIds: %s]",
								nodeId, contextIds));
			}

			if (!isSignatureValid) {
				throw new InvalidPayloadSignatureException(ControlComponentLCCSharePayload.class,
						String.format("[nodeId: %s, contextIds: %s]", nodeId, contextIds));
			}
		}
	}

	/**
	 * Creates response containing the computed Short Choice Return Codes.
	 */
	private ShortChoiceReturnCodeAndComputeResults createResponse(final List<String> shortChoiceReturnCodes,
			final List<ControlComponentLCCSharePayload> controlComponentLCCSharePayloads) throws JsonProcessingException {

		final ShortChoiceReturnCodeAndComputeResults shortChoiceReturnCodesComputeResults = new ShortChoiceReturnCodeAndComputeResults();
		shortChoiceReturnCodesComputeResults.setComputationResults(objectMapper.writeValueAsString(controlComponentLCCSharePayloads));

		final String serializedShortChoiceReturnCodes = StringUtils.join(shortChoiceReturnCodes, ';');
		shortChoiceReturnCodesComputeResults.setShortChoiceReturnCodes(serializedShortChoiceReturnCodes);

		return shortChoiceReturnCodesComputeResults;
	}

}
