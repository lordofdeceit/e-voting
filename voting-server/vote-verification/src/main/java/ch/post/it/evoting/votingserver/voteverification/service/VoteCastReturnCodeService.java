/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.service;

import static ch.post.it.evoting.cryptoprimitives.math.GqElement.GqElementFactory;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.SignatureException;
import java.util.Base64;
import java.util.List;
import java.util.SortedMap;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.ChannelSecurityContextData;
import ch.post.it.evoting.domain.ContextIds;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.voting.confirmvote.LongVoteCastReturnCodesShare;
import ch.post.it.evoting.domain.election.model.confirmation.TraceableConfirmationMessage;
import ch.post.it.evoting.domain.returncodes.ShortVoteCastReturnCodeAndComputeResults;
import ch.post.it.evoting.domain.voting.confirmvote.ConfirmationKey;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponentlVCCSharePayload;
import ch.post.it.evoting.domain.voting.confirmvote.VotingServerConfirmPayload;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RetrofitConsumer;
import ch.post.it.evoting.votingserver.voteverification.domain.model.content.SetupComponentVerificationCardKeystoreEntity;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.ElectionEventEntity;
import ch.post.it.evoting.votingserver.voteverification.infrastructure.remote.MessageBrokerOrchestratorClient;
import ch.post.it.evoting.votingserver.voteverification.voting.confirmvote.ExtractVCCContext;
import ch.post.it.evoting.votingserver.voteverification.voting.confirmvote.ExtractVCCInput;
import ch.post.it.evoting.votingserver.voteverification.voting.confirmvote.ExtractVCCOutput;
import ch.post.it.evoting.votingserver.voteverification.voting.confirmvote.ExtractVCCAlgorithm;

/**
 * Generate the short vote cast return code based on the confirmation message - in interaction with the control components.
 */
@Stateless
public class VoteCastReturnCodeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VoteCastReturnCodeService.class);

	private final ObjectMapper objectMapper;
	private final ExtractVCCAlgorithm extractVCCAlgorithm;
	private final ElectionEventService electionEventService;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final ReturnCodesMappingTableService returnCodesMappingTableService;
	private final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient;
	private final SetupComponentVerificationCardKeystoreService setupComponentVerificationCardKeystoreService;

	@Inject
	public VoteCastReturnCodeService(
			final ObjectMapper objectMapper,
			final ExtractVCCAlgorithm extractVCCAlgorithm,
			final ElectionEventService electionEventService,
			final SignatureKeystore<Alias> signatureKeystoreService,
			final ReturnCodesMappingTableService returnCodesMappingTableService,
			final MessageBrokerOrchestratorClient messageBrokerOrchestratorClient,
			final SetupComponentVerificationCardKeystoreService setupComponentVerificationCardKeystoreService
	) {
		this.objectMapper = objectMapper;
		this.extractVCCAlgorithm = extractVCCAlgorithm;
		this.electionEventService = electionEventService;
		this.signatureKeystoreService = signatureKeystoreService;
		this.returnCodesMappingTableService = returnCodesMappingTableService;
		this.messageBrokerOrchestratorClient = messageBrokerOrchestratorClient;
		this.setupComponentVerificationCardKeystoreService = setupComponentVerificationCardKeystoreService;
	}

	/**
	 * Calculates in interaction with the control components the short vote cast return code based on the confirmation message received by the voting
	 * client.
	 *
	 * @param electionEventId     election event identifier.
	 * @param verificationCardId  verification card id
	 * @param confirmationMessage The confirmation message received by the voting client
	 * @return An object cast code message that contains the short vote cast return code
	 */
	@SuppressWarnings("java:S117")
	public ShortVoteCastReturnCodeAndComputeResults retrieveShortVoteCastCode(final String electionEventId, final String verificationCardId,
			final TraceableConfirmationMessage confirmationMessage) throws ResourceNotFoundException, IOException {

		final SetupComponentVerificationCardKeystoreEntity setupComponentVerificationCardKeystoreEntity = setupComponentVerificationCardKeystoreService.load(
				electionEventId, verificationCardId);
		final String verificationCardSetId = setupComponentVerificationCardKeystoreEntity.getVerificationCardSetId();

		final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);

		LOGGER.info("Generating the vote cast code... [contextIds: {}]", contextIds);

		// Create VotingServerConfirmPayload to send.
		final VotingServerConfirmPayload votingServerConfirmPayload = createVotingServerConfirmPayload(confirmationMessage, contextIds);

		// Ask the control components to compute the long vote cast return code shares lCC_j_id.
		final List<LongVoteCastReturnCodesShare> longVoteCastReturnCodesShares =
				collectLongVoteCastReturnCodeShares(contextIds, votingServerConfirmPayload);

		// Retrieve short codes by combining CCR shares and looking up the CMTable.
		final GroupVector<GqElement, GqGroup> lVCCShares = longVoteCastReturnCodesShares.stream()
				.map(LongVoteCastReturnCodesShare::longVoteCastReturnCodeShare)
				.collect(GroupVector.toGroupVector());

		final ExtractVCCContext extractVCCContext = new ExtractVCCContext(votingServerConfirmPayload.getEncryptionGroup(), electionEventId);

		final SortedMap<String, String> CMtable = returnCodesMappingTableService.retrieveReturnCodesMappingTable(verificationCardSetId);
		final ExtractVCCInput extractVCCInput = new ExtractVCCInput.Builder()
				.setLongVoteCastReturnCodeShares(lVCCShares)
				.setVerificationCardId(verificationCardId)
				.setReturnCodesMappingTable(CMtable)
				.build();
		final ExtractVCCOutput voteCastReturnCodeOutput = extractVCCAlgorithm.extractVCC(extractVCCContext, extractVCCInput);

		// Prepare response.
		final ShortVoteCastReturnCodeAndComputeResults shortVoteCastReturnCodeComputeResults = new ShortVoteCastReturnCodeAndComputeResults();
		shortVoteCastReturnCodeComputeResults.setShortVoteCastReturnCode(voteCastReturnCodeOutput.shortVoteCastReturnCode());
		shortVoteCastReturnCodeComputeResults.setComputationResults(objectMapper.writeValueAsString(longVoteCastReturnCodesShares));

		return shortVoteCastReturnCodeComputeResults;
	}

	private VotingServerConfirmPayload createVotingServerConfirmPayload(final TraceableConfirmationMessage confirmationMessage,
			final ContextIds contextIds) throws ResourceNotFoundException, IOException {

		final String electionEventId = contextIds.electionEventId();

		// Retrieve group.
		final ElectionEventEntity electionEventEntity = electionEventService.retrieveElectionEventEntity(electionEventId);
		final GqGroup encryptionGroup = objectMapper.readValue(electionEventEntity.getEncryptionGroup(), GqGroup.class);

		// Create confirmation key.
		final String confirmationCodeString = new String(Base64.getDecoder().decode(confirmationMessage.getConfirmationKey()),
				StandardCharsets.UTF_8);
		final BigInteger confirmationCodeValue = new BigInteger(confirmationCodeString);
		final GqElement element = GqElementFactory.fromValue(confirmationCodeValue, encryptionGroup);

		final ConfirmationKey confirmationKey = new ConfirmationKey(contextIds, element);

		// Create and sign payload.
		final VotingServerConfirmPayload votingServerConfirmPayload = new VotingServerConfirmPayload(encryptionGroup, confirmationKey);
		final CryptoPrimitivesSignature signature = generateSignature(votingServerConfirmPayload, contextIds);
		votingServerConfirmPayload.setSignature(signature);

		LOGGER.info("Successfully signed the voting server confirm payload. [contextIds: {}]", contextIds);

		return votingServerConfirmPayload;
	}

	private CryptoPrimitivesSignature generateSignature(final VotingServerConfirmPayload votingServerConfirmPayload, final ContextIds contextIds) {
		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();

		final Hashable additionalContextData = ChannelSecurityContextData.votingServerConfirm(electionEventId, verificationCardSetId,
				verificationCardId);

		try {
			final byte[] signature = signatureKeystoreService.generateSignature(votingServerConfirmPayload, additionalContextData);

			return new CryptoPrimitivesSignature(signature);

		} catch (final SignatureException se) {
			final String message = String.format("Failed to sign the voting server confirm payload. [contextIds: %s]", contextIds);
			throw new IllegalStateException(message, se);
		}
	}

	private List<LongVoteCastReturnCodesShare> collectLongVoteCastReturnCodeShares(final ContextIds contextIds,
			final VotingServerConfirmPayload votingServerConfirmPayload)
			throws ResourceNotFoundException {

		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();

		final List<ControlComponentlVCCSharePayload> longReturnCodesSharePayloads = RetrofitConsumer.processResponse(
				messageBrokerOrchestratorClient.getLongVoteCastReturnCodesContributions(electionEventId, verificationCardSetId, verificationCardId,
						votingServerConfirmPayload));

		// Vote Cast Return Code computation response correctly received.
		LOGGER.info("Successfully retrieved the Long Vote Cast Return Code shares. [contextIds: {}]", contextIds);

		verifyPayloadSignatures(contextIds, longReturnCodesSharePayloads);
		LOGGER.info("Successfully verified the Long Vote Cast Return Code shares. [contextIds: {}]", contextIds);

		return longReturnCodesSharePayloads.stream()
				.map(ControlComponentlVCCSharePayload::getLongVoteCastReturnCodesShare)
				.map(o -> o.orElseThrow(() -> new IllegalStateException("We should not reach this state.")))
				.toList();

	}

	private void verifyPayloadSignatures(final ContextIds contextIds,
			final List<ControlComponentlVCCSharePayload> controlComponentlVCCSharePayloads) {
		for (final ControlComponentlVCCSharePayload payload : controlComponentlVCCSharePayloads) {
			final int nodeId = payload.getNodeId();
			final CryptoPrimitivesSignature signature = payload.getSignature();

			checkState(signature != null, "The signature of the Control Component lVCC Share payload is null. [nodeId: %s, contextIds: %s]",
					nodeId, contextIds);

			final Hashable additionalContextData = ChannelSecurityContextData.controlComponentlVCCShare(nodeId, contextIds.electionEventId(),
					contextIds.verificationCardSetId(), contextIds.verificationCardId());

			final boolean isSignatureValid;
			try {
				isSignatureValid = signatureKeystoreService.verifySignature(Alias.getControlComponentByNodeId(nodeId), payload, additionalContextData,
						signature.signatureContents());
			} catch (final SignatureException e) {
				throw new IllegalStateException(
						String.format("Could not verify the signature of the Control Component lVCC Share payload. [nodeId: %s, contextIds: %s]",
								nodeId, contextIds));
			}
			if (!isSignatureValid) {
				throw new InvalidPayloadSignatureException(ControlComponentlVCCSharePayload.class,
						String.format("[nodeId: %s, contextIds: %s]", nodeId, contextIds));
			}
		}
	}

}
