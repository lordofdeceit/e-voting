/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.voting.sendvote;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

/**
 * Regroups the context values needed by the ExtractCRC algorithm.
 */
public record ExtractCRCContext(GqGroup encryptionGroup, String tenantId, String electionEventId, String ballotId) {
	/**
	 * @throws NullPointerException      if any of the fields is null.
	 * @throws FailedValidationException if the {@code tenantId}, {@code electionEventId} and {@code ballotId} do not comply the UUID format.
	 */
	public ExtractCRCContext {
		checkNotNull(encryptionGroup);
		checkNotNull(tenantId);
		validateUUID(electionEventId);
		validateUUID(ballotId);

	}

	public static class Builder {
		private GqGroup encryptionGroup;
		private String tenantId;
		private String electionEventId;
		private String ballotId;

		public Builder setEncryptionGroup(final GqGroup encryptionGroup) {
			this.encryptionGroup = encryptionGroup;
			return this;
		}

		public Builder setTenantId(final String tenantId) {
			this.tenantId = tenantId;
			return this;
		}

		public Builder setElectionEventId(final String electionEventId) {
			this.electionEventId = electionEventId;
			return this;
		}

		public Builder setBallotId(final String ballotId) {
			this.ballotId = ballotId;
			return this;
		}

		public ExtractCRCContext build() {
			return new ExtractCRCContext(encryptionGroup, tenantId, electionEventId, ballotId);
		}
	}
}
