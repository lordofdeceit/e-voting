/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.infrastructure.persistence;

import java.io.UncheckedIOException;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RetrofitConsumer;
import ch.post.it.evoting.votingserver.commons.tracking.TrackIdInstance;
import ch.post.it.evoting.votingserver.commons.util.PropertiesFileReader;
import ch.post.it.evoting.votingserver.voteverification.infrastructure.remote.ElectionInformationClient;

/**
 * This the implementation of the ballot repository.
 */
@Stateless(name = "vv-BallotRepositoryImpl")
public class BallotRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(BallotRepository.class);
	private static final PropertiesFileReader PROPERTIES = PropertiesFileReader.getInstance();
	private static final String BALLOTS_PATH = PROPERTIES.getPropertyValue("BALLOTS_PATH");

	private final ObjectMapper mapper;
	private final TrackIdInstance trackId;
	private final ElectionInformationClient electionInformationClient;

	@Inject
	public BallotRepository(final ObjectMapper mapper,
			final TrackIdInstance trackId,
			final ElectionInformationClient electionInformationClient) {
		this.mapper = mapper;
		this.trackId = trackId;
		this.electionInformationClient = electionInformationClient;
	}

	/**
	 * Searches for a ballot identified by tenant, election event and ballot identifier. The implementation is a REST Client invoking a get operation
	 * of a REST web service to obtain the ballot for the given parameters.
	 *
	 * @param tenantId        - the identifier of the tenant.
	 * @param electionEventId - the identifier of the election event.
	 * @param ballotId        - the identifier of the ballot.
	 * @return a Ballot of the tenant identified by tenant id, and identified by ballot id.
	 * @throws ResourceNotFoundException if ballot is not found.
	 */
	public Ballot findByTenantIdAndElectionEventIdAndBallotId(final String tenantId, final String electionEventId, final String ballotId)
			throws ResourceNotFoundException {
		LOGGER.info("Getting ballot.... [tenant: {}, electionEventId: {}, ballotId: {}]", tenantId, electionEventId, ballotId);

		final String ballotAsJson;
		try {
			ballotAsJson = RetrofitConsumer.processResponse(
					electionInformationClient.getBallot(trackId.getTrackId(), BALLOTS_PATH, tenantId, electionEventId, ballotId));
			LOGGER.info("Ballot found. [tenant: {}, electionEventId: {}, ballotId: {}]", tenantId, electionEventId, ballotId);
		} catch (final ResourceNotFoundException e) {
			LOGGER.error("Ballot not found. [tenant: {}, electionEventId: {}, ballotId: {}]", tenantId, electionEventId, ballotId);
			throw e;
		}

		try {
			return mapper.readValue(ballotAsJson, Ballot.class);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(
					String.format("Failed to deserialize ballot json. [tenant: %s, electionEventId: %s, ballotId: %s]", tenantId, electionEventId,
							ballotId), e);
		}

	}

}
