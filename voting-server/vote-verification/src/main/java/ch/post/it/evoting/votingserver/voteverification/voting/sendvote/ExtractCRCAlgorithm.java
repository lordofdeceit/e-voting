/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.voting.sendvote;

import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.byteArrayToString;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;

import javax.inject.Inject;

import ch.post.it.evoting.cryptoprimitives.domain.election.Ballot;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableByteArray;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.cryptoprimitives.math.Base64;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.symmetric.Symmetric;
import ch.post.it.evoting.cryptoprimitives.utils.KeyDerivation;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.voteverification.infrastructure.persistence.BallotRepository;

/**
 * Implements the ExtractCRC algorithm.
 */
public class ExtractCRCAlgorithm {

	public static final int CHOICE_RETURN_CODES_LENGTH = 4;
	public static final int NUMBER_OF_CONTROL_COMPONENTS = 4;

	private static final int KEY_DERIVATION_BYTES_LENGTH = 32;

	private final Hash hash;
	private final BallotRepository ballotRepository;
	private final Symmetric symmetric;
	private final KeyDerivation keyDerivation;
	private final Base64 base64;

	@Inject
	public ExtractCRCAlgorithm(final Hash hash, final BallotRepository ballotRepository, final Symmetric symmetric, final KeyDerivation keyDerivation,
			final Base64 base64) {
		this.hash = hash;
		this.ballotRepository = ballotRepository;
		this.symmetric = symmetric;
		this.keyDerivation = keyDerivation;
		this.base64 = base64;
	}

	/**
	 * Extracts the short Choice Return Codes CC<sub>id</sub> from the Return Codes Mapping table CMtable.
	 *
	 * @param context the {@link ExtractCRCContext} containing necessary group and ids. Must be non-null.
	 * @param input   the {@link ExtractCRCInput} containing all needed inputs. Must be non-null.
	 * @return the short Choice Return Codes CC<sub>id</sub>.
	 * @throws NullPointerException      if any of the fields is null.
	 * @throws IllegalArgumentException  if the context and input do not have the same group.
	 * @throws ResourceNotFoundException if
	 *                                   <ul>
	 *                                       <li>the corresponding ballot is not found.</li>
	 *                                       <li>the corresponding key is not found in the CMTable.</li>
	 *                                   </ul>
	 */
	@SuppressWarnings("java:S117")
	public ExtractCRCOutput extractCRC(final ExtractCRCContext context, final ExtractCRCInput input) throws ResourceNotFoundException {
		checkNotNull(context);
		checkNotNull(input);

		// Cross group check.
		checkArgument(context.encryptionGroup().equals(input.getGroup()), "The context and input must have the same group.");

		// Variables.
		final int psi = input.getLongChoiceReturnCodeShares().get(0).size();
		final GqGroup gqGroup = context.encryptionGroup();
		final GqElement identity = GqElement.GqElementFactory.fromValue(BigInteger.ONE, gqGroup);
		final String ee = context.electionEventId();
		final String vc_id = input.getVerificationCardId();
		final TreeMap<String, String> CMtable = new TreeMap<>(input.getReturnCodesMappingTable());

		final CombinedCorrectnessInformation combinedCorrectnessInformation = new CombinedCorrectnessInformation(getBallot(context));

		// Operations.
		final List<String> CC_id = new ArrayList<>();

		for (int i = 0; i < psi; i++) {

			final int final_i = i;
			final GqElement pC_id_i = input.getLongChoiceReturnCodeShares().stream().map(lCC_j_id -> lCC_j_id.get(final_i))
					.reduce(identity, GqElement::multiply);

			final byte[] lCC_id_i = hash.recursiveHash(pC_id_i, HashableString.from(vc_id), HashableString.from(ee),
					HashableString.from(combinedCorrectnessInformation.getCorrectnessIdForSelectionIndex(i)));

			final String key = base64.base64Encode(hash.recursiveHash(HashableByteArray.from(lCC_id_i)));

			if (!CMtable.containsKey(key)) {
				throw new ResourceNotFoundException(
						String.format(
								"Encrypted short Choice Return Code not found in CMtable. [electionEventId: %s, verificationCardId: %s, index: %s]",
								context.electionEventId(), input.getVerificationCardId(), i));

			} else {
				final String ctCC_id_i_encoded = CMtable.get(key);

				final byte[] ctCC_id_i_combined = base64.base64Decode(ctCC_id_i_encoded);

				final int length = ctCC_id_i_combined.length;

				final int split = length - symmetric.getNonceLength();

				final byte[] ctCC_id_i_ciphertext = Arrays.copyOfRange(ctCC_id_i_combined, 0, split);

				final byte[] ctCC_id_i_nonce = Arrays.copyOfRange(ctCC_id_i_combined, split, length);

				final byte[] skcc_id_i = keyDerivation.KDF(lCC_id_i, List.of(), KEY_DERIVATION_BYTES_LENGTH);

				final byte[] CC_id_i_bytes = symmetric.getPlaintextSymmetric(skcc_id_i, ctCC_id_i_ciphertext, ctCC_id_i_nonce, List.of());

				final String CC_id_i = byteArrayToString(CC_id_i_bytes);
				CC_id.add(CC_id_i);
			}
		}

		return new ExtractCRCOutput(CC_id);
	}

	private Ballot getBallot(final ExtractCRCContext context) throws ResourceNotFoundException {
		return ballotRepository.findByTenantIdAndElectionEventIdAndBallotId(context.tenantId(), context.electionEventId(),
				context.ballotId());
	}

}
