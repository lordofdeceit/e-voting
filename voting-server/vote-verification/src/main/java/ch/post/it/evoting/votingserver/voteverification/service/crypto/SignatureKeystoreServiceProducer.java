/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.service.crypto;

import java.io.IOException;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystoreFactory;

/**
 * Produces a {@link SignatureKeystore} loaded with keystore.
 */
public class SignatureKeystoreServiceProducer {

	@Inject
	private KeystoreRepository repository;

	@Produces
	public SignatureKeystore<Alias> getInstance() throws IOException {
		return SignatureKeystoreFactory.createSignatureKeystore(repository.getKeyStore(), "PKCS12", repository.getKeystorePassword(),
				keystore -> KeyStoreValidator.validateKeyStore(keystore, repository.getKeystoreAlias()), repository.getKeystoreAlias());
	}
}
