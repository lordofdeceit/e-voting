/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.service.crypto;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.System.getenv;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.ejb.Stateful;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;

@Stateful
public class KeystoreRepository {

	private final String keystoreLocation;
	private final String keystorePasswordLocation;
	private final Alias alias;

	public KeystoreRepository() {
		this.keystoreLocation = checkNotNull(getenv("DIRECT_TRUST_KEYSTORE_LOCATION"));
		this.keystorePasswordLocation = checkNotNull(getenv("DIRECT_TRUST_KEYSTORE_PASSWORD_LOCATION"));
		this.alias = Alias.VOTING_SERVER;
	}

	public InputStream getKeyStore() throws IOException {
		return Files.newInputStream(Paths.get(keystoreLocation));
	}

	public char[] getKeystorePassword() throws IOException {
		return new String(Files.readAllBytes(Paths.get(keystorePasswordLocation)), StandardCharsets.UTF_8).toCharArray();
	}

	public Alias getKeystoreAlias() {
		return alias;
	}
}
