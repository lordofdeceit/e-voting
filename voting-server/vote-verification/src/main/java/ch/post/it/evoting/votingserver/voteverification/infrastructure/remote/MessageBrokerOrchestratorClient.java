/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.infrastructure.remote;

import java.util.List;

import javax.ws.rs.core.MediaType;

import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponentlVCCSharePayload;
import ch.post.it.evoting.domain.voting.confirmvote.VotingServerConfirmPayload;
import ch.post.it.evoting.domain.voting.sendvote.CombinedControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentLCCSharePayload;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.VotingServerEncryptedVotePayload;
import ch.post.it.evoting.votingserver.commons.ui.Constants;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Defines the methods to access via REST to a set of operations.
 */
public interface MessageBrokerOrchestratorClient {

	/**
	 * Requests the control components' partial decryption of the encrypted partial Choice Return Codes.
	 *
	 * @param electionEventId       the election event id.
	 * @param verificationCardSetId the verification card set id.
	 * @param verificationCardId    the verification card id.
	 * @return the list of ControlComponentPartialDecryptPayloads (one per control component).
	 */
	@POST("api/v1/voting/sendvote/partialdecrypt/electionevent/{electionEventId}/verificationCardSetId/{verificationCardSetId}/verificationCardId/{verificationCardId}")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<List<ControlComponentPartialDecryptPayload>> getChoiceReturnCodesPartialDecryptContributions(
			@Path(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@Path(Constants.PARAMETER_VALUE_VERIFICATION_CARD_SET_ID)
			final String verificationCardSetId,
			@Path(Constants.VERIFICATION_CARD_ID)
			final String verificationCardId,
			@Body
			final VotingServerEncryptedVotePayload votingServerEncryptedVotePayload);

	/**
	 * Requests the Control Components contributions for the computation of the partial choice return codes.
	 *
	 * @param electionEventId       the election event id.
	 * @param verificationCardSetId the verification card set id.
	 * @param verificationCardId    the verification card id.
	 * @return the list of LongReturnCodesSharePayload (one per control component).
	 */
	@POST("api/v1/voting/sendvote/lccshare/electionevent/{electionEventId}/verificationCardSetId/{verificationCardSetId}/verificationCardId/{verificationCardId}")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<List<ControlComponentLCCSharePayload>> getLongChoiceReturnCodesContributions(
			@Path(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@Path(Constants.PARAMETER_VALUE_VERIFICATION_CARD_SET_ID)
			final String verificationCardSetId,
			@Path(Constants.VERIFICATION_CARD_ID)
			final String verificationCardId,
			@Body
			final CombinedControlComponentPartialDecryptPayload combinedControlComponentPartialDecryptPayload);

	/**
	 * Requests the Control Components contributions for the computation of the Long Vote Cast Return Code.
	 *
	 * @param electionEventId            the election event id.
	 * @param verificationCardSetId      the verification card set id.
	 * @param verificationCardId         the verification card id.
	 * @param votingServerConfirmPayload the payload with the confirmation key.
	 * @return the list of LongReturnCodesSharePayload (one per control component).
	 */
	@POST("api/v1.1/voting/confirmvote/longvotecastreturncodeshare/electionevent/{electionEventId}/verificationCardSetId/{verificationCardSetId}/verificationCardId/{verificationCardId}")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<List<ControlComponentlVCCSharePayload>> getLongVoteCastReturnCodesContributions(
			@Path(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@Path(Constants.PARAMETER_VALUE_VERIFICATION_CARD_SET_ID)
			final String verificationCardSetId,
			@Path(Constants.VERIFICATION_CARD_ID)
			final String verificationCardId,
			@Body
			final VotingServerConfirmPayload votingServerConfirmPayload);

	/**
	 * Requests the Control Components to save the election event context.
	 *
	 * @param electionEventId             the election event id.
	 * @param electionEventContextPayload the payload with the election event context.
	 * @return the list of ElectionContextResponsePayload (one per control component)
	 */
	@POST("api/v1/configuration/electioncontext/electionevent/{electionEventId}")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<Void> uploadElectionEventContext(
			@Path(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@Body
			final ElectionEventContextPayload electionEventContextPayload);

	/**
	 * Requests the Control Components to save the setup component public keys.
	 *
	 * @param electionEventId             the election event id.
	 * @param setupComponentPublicKeysPayload the payload with the setup component public keys.
	 * @return the list of ElectionContextResponsePayload (one per control component)
	 */
	@POST("api/v1/configuration/setupkeys/electionevent/{electionEventId}")
	@Headers("Accept:" + MediaType.APPLICATION_JSON)
	Call<Void> uploadSetupComponentPublicKeys(
			@Path(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@Body
			final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload);
}
