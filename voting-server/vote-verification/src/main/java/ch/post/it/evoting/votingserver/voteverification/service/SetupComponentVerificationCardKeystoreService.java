/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.service;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.time.LocalDateTime;
import java.util.function.Supplier;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.annotations.VisibleForTesting;

import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.configuration.SetupComponentVerificationCardKeystoresPayload;
import ch.post.it.evoting.domain.configuration.VerificationCardKeystore;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.DuplicateEntryException;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.voteverification.domain.model.content.SetupComponentVerificationCardKeystoreEntity;
import ch.post.it.evoting.votingserver.voteverification.domain.model.content.SetupComponentVerificationCardKeystoreRepository;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.ElectionEventContextEntity;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.ElectionEventEntity;

/**
 * Saves the setup component verification card keystores.
 */
@Stateless
public class SetupComponentVerificationCardKeystoreService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SetupComponentVerificationCardKeystoreService.class);

	private final ObjectMapper objectMapper;
	private final SetupComponentVerificationCardKeystoreRepository setupComponentVerificationCardKeystoreRepository;
	private final ElectionEventContextService electionEventContextService;
	private final ElectionEventService electionEventService;

	@Inject
	public SetupComponentVerificationCardKeystoreService(final ObjectMapper objectMapper,
			final SetupComponentVerificationCardKeystoreRepository setupComponentVerificationCardKeystoreRepository,
			final ElectionEventContextService electionEventContextService,
			final ElectionEventService electionEventService) {
		this.objectMapper = objectMapper;
		this.setupComponentVerificationCardKeystoreRepository = setupComponentVerificationCardKeystoreRepository;
		this.electionEventContextService = electionEventContextService;
		this.electionEventService = electionEventService;
	}

	/**
	 * Saves the setup component verification card keystores.
	 *
	 * @param setupComponentVerificationCardKeystoresPayload the setup component verification card keystores payload. Must be non-null.
	 * @throws NullPointerException    if the setup component verification card keystores is null.
	 * @throws DuplicateEntryException if the setup component verification card keystores is already saved in the database.
	 * @throws UncheckedIOException    if an error occurs while serializing the setup component verification card keystores.
	 */
	public void save(final SetupComponentVerificationCardKeystoresPayload setupComponentVerificationCardKeystoresPayload)
			throws DuplicateEntryException, UncheckedIOException {

		checkNotNull(setupComponentVerificationCardKeystoresPayload);

		final String electionEventId = setupComponentVerificationCardKeystoresPayload.getElectionEventId();
		final String verificationCardSetId = setupComponentVerificationCardKeystoresPayload.getVerificationCardSetId();

		for (final VerificationCardKeystore verificationCardKeystore : setupComponentVerificationCardKeystoresPayload.getVerificationCardKeystores()) {
			saveVerificationCardKeystore(electionEventId, verificationCardSetId, verificationCardKeystore);
		}

		LOGGER.info(
				"Setup component verification card keystores successfully saved in vote verification. [electionEventId: {}, verificationCardSetId: {}]",
				electionEventId, verificationCardSetId);
	}

	private void saveVerificationCardKeystore(final String electionEventId, final String verificationCardSetId,
			final VerificationCardKeystore verificationCardKeystore) throws DuplicateEntryException {

		final SetupComponentVerificationCardKeystoreEntity setupComponentVerificationCardKeystoreEntity = new SetupComponentVerificationCardKeystoreEntity();
		setupComponentVerificationCardKeystoreEntity.setVerificationCardId(verificationCardKeystore.verificationCardId());
		setupComponentVerificationCardKeystoreEntity.setVerificationCardSetId(verificationCardSetId);
		setupComponentVerificationCardKeystoreEntity.setElectionEventId(electionEventId);

		final byte[] serializedVerificationCardKeystore;
		try {
			serializedVerificationCardKeystore = objectMapper.writeValueAsBytes(verificationCardKeystore);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(String.format(
					"Failed to serialize the setup component verification card keystore. [electionEventId: %s, verificationCardSetId: %s, verificationCardId: %s]",
					electionEventId, verificationCardSetId, verificationCardKeystore.verificationCardId()), e);
		}
		setupComponentVerificationCardKeystoreEntity.setVerificationCardKeystore(serializedVerificationCardKeystore);

		setupComponentVerificationCardKeystoreRepository.save(setupComponentVerificationCardKeystoreEntity);
	}

	/**
	 * Loads the verification card keystore related to the given ids.
	 *
	 * @param electionEventId       the election event id. Must be non-null and a valid UUID.
	 * @param verificationCardSetId the verification card set id. Must be non-null and a valid UUID.
	 * @param verificationCardId    the verification card id. Must be non-null and a valid UUID.
	 * @return the verification card keystore.
	 * @throws NullPointerException      if any input is null.
	 * @throws FailedValidationException if any input is not a valid UUID.
	 * @throws IllegalStateException     if the related election event is closed.
	 */
	public VerificationCardKeystore loadVerificationCardKeystore(final String electionEventId, final String verificationCardSetId,
			final String verificationCardId) {
		return loadVerificationCardKeystore(electionEventId, verificationCardSetId, verificationCardId, LocalDateTime::now);
	}

	@VisibleForTesting
	protected VerificationCardKeystore loadVerificationCardKeystore(final String electionEventId, final String verificationCardSetId,
			final String verificationCardId, final Supplier<LocalDateTime> now) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		validateUUID(verificationCardId);

		try {
			final ElectionEventEntity electionEventEntity = electionEventService.retrieveElectionEventEntity(electionEventId);
			final ElectionEventContextEntity electionEventContext = electionEventContextService.getElectionEventContextEntity(electionEventEntity);

			final LocalDateTime electionStartTime = electionEventContext.getStartTime();
			final LocalDateTime electionEndTime = electionEventContext.getFinishTime();
			final LocalDateTime currentTime = now.get();

			final boolean afterElectionStart = currentTime.isAfter(electionStartTime) || currentTime.isEqual(electionStartTime);
			final boolean beforeElectionEnd = currentTime.isBefore(electionEndTime) || currentTime.isEqual(electionEndTime);

			checkState(afterElectionStart && beforeElectionEnd,
					"Cannot load verification card keystore outside the opened election time window. [electionEventId: %s, verificationCardId: %s, "
							+ "startTime: %s, finishTime: %s]",
					electionEventId, verificationCardSetId, electionStartTime, electionEndTime);

		} catch (ResourceNotFoundException e) {
			throw new IllegalStateException("Fail to retrieve the context.");
		}

		final SetupComponentVerificationCardKeystoreEntity entity = setupComponentVerificationCardKeystoreRepository.find(verificationCardId);

		try {
			return objectMapper.readValue(entity.getVerificationCardKeystore(), VerificationCardKeystore.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(String.format(
					"Failed to deserialize the setup component verification card keystore. [electionEventId: %s, verificationCardSetId: %s, verificationCardId: %s]",
					electionEventId, verificationCardSetId, verificationCardId), e);
		}
	}

	/**
	 * Loads the setup component verification card keystore entity related to the given ids.
	 *
	 * @param electionEventId    the election event id. Must be non-null and a valid UUID.
	 * @param verificationCardId the verification card id. Must be non-null and a valid UUID.
	 * @return the setup component verification card keystore entity.
	 * @throws NullPointerException      if any input is null.
	 * @throws FailedValidationException if any input is not a valid UUID.
	 */
	public SetupComponentVerificationCardKeystoreEntity load(final String electionEventId, final String verificationCardId) {

		validateUUID(electionEventId);
		validateUUID(verificationCardId);

		return setupComponentVerificationCardKeystoreRepository.find(verificationCardId);
	}
}
