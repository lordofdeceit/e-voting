/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.infrastructure.persistence;

import java.util.Optional;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import ch.post.it.evoting.votingserver.commons.infrastructure.persistence.BaseRepositoryImpl;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.ElectionEventContextEntity;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.ElectionEventContextRepository;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.ElectionEventEntity;

/**
 * Implementation of the repository with JPA
 */
public class ElectionEventContextRepositoryImpl extends BaseRepositoryImpl<ElectionEventContextEntity, Integer>
		implements ElectionEventContextRepository {

	@Override
	public Optional<ElectionEventContextEntity> findByElectionEvent(final ElectionEventEntity electionEventEntity) {
		TypedQuery<ElectionEventContextEntity> query = entityManager.createQuery(
				"SELECT a FROM ElectionEventContextEntity a WHERE a.electionEventEntity = :electionEventEntity", ElectionEventContextEntity.class);
		query.setParameter("electionEventEntity", electionEventEntity);

		try {
			return Optional.of(query.getSingleResult());
		} catch (NoResultException e) {
			return Optional.empty();
		}
	}
}
