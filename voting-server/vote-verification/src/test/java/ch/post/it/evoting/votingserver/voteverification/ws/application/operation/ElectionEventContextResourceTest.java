/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.ws.application.operation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URL;
import java.security.SignatureException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.SecurityLevelTestingOnly;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ApplicationException;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.DuplicateEntryException;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.commons.tracking.TrackIdInstance;
import ch.post.it.evoting.votingserver.voteverification.service.ElectionEventContextService;
import ch.post.it.evoting.votingserver.voteverification.service.ElectionEventService;

@SecurityLevelTestingOnly
@DisplayName("ElectionEventContextResource")
@ExtendWith(MockitoExtension.class)
class ElectionEventContextResourceTest {

	private static ElectionEventContextPayload electionEventContextPayload;
	private static String tenantId;
	private static String electionEventId;
	private static String verificationCardSetId;
	private final String trackingId = new TrackIdInstance().getTrackId();

	@InjectMocks
	ElectionEventContextResource electionEventContextResource;

	@Mock
	private HttpServletRequest mockServletRequest;

	@Mock
	private ElectionEventContextService electionEventContextService;

	@Mock
	private ElectionEventService encryptionParametersService;

	@Mock
	private SignatureKeystore<Alias> signatureKeystoreService;

	@Mock
	private TrackIdInstance trackIdInstance;

	@BeforeAll
	static void setUpAll() throws IOException {
		final ObjectMapper mapper = DomainObjectMapper.getNewInstance();
		final URL electionContextPayloadUrl = ElectionEventContextResourceTest.class.getResource(
				"/electionEventContextServiceTest/election-event-context-payload.json");
		electionEventContextPayload = mapper.readValue(electionContextPayloadUrl, ElectionEventContextPayload.class);

		electionEventId = electionEventContextPayload.getElectionEventContext().electionEventId();
		tenantId = electionEventId;
		verificationCardSetId = electionEventId;
	}

	@Test
	@DisplayName("save election event context with valid parameters")
	void saveElectionEventContextHappyPath() throws ResourceNotFoundException, DuplicateEntryException, ApplicationException, SignatureException {
		when(signatureKeystoreService.verifySignature(any(), eq(electionEventContextPayload), any(), any())).thenReturn(true);

		final Response response = electionEventContextResource.saveElectionEventContext(electionEventId, trackingId, electionEventContextPayload,
				mockServletRequest);

		verify(electionEventContextService, times(1)).saveElectionEventContext(any());

		assertNotNull(response);
		assertEquals(200, response.getStatus());
	}

	@Test
	@DisplayName("Error verifying payload signature while saving throws IllegalStateException")
	void savingErrorVerifyingSignatureThrows() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), eq(electionEventContextPayload), any(), any())).thenThrow(SignatureException.class);

		final IllegalStateException exception = assertThrows(IllegalStateException.class,
				() -> electionEventContextResource.saveElectionEventContext(electionEventId, trackingId, electionEventContextPayload,
						mockServletRequest));
		assertEquals(String.format("Could not verify the signature of the election event context. [electionEventId: %s]", electionEventId),
				exception.getMessage());
	}

	@Test
	@DisplayName("Saving payload with invalid signature throws InvalidPayloadSignatureException")
	void savingInvalidSignatureThrows() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), eq(electionEventContextPayload), any(), any())).thenReturn(false);
		final InvalidPayloadSignatureException exception = assertThrows(InvalidPayloadSignatureException.class,
				() -> electionEventContextResource.saveElectionEventContext(electionEventId, trackingId, electionEventContextPayload,
						mockServletRequest));
		assertEquals(String.format("Signature of payload %s is invalid. [electionEventId: %s]", ElectionEventContextPayload.class.getSimpleName(),
				electionEventId), exception.getMessage());
	}
}
