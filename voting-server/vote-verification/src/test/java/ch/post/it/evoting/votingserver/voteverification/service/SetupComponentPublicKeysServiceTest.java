/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.service;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URL;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.cryptoprimitives.domain.validations.FailedValidationException;
import ch.post.it.evoting.domain.SecurityLevelTestingOnly;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.voteverification.domain.common.VotingClientPublicKeys;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.ElectionEventEntity;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.SetupComponentPublicKeysEntity;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.SetupComponentPublicKeysRepository;
import ch.post.it.evoting.votingserver.voteverification.infrastructure.remote.MessageBrokerOrchestratorClient;

@SecurityLevelTestingOnly
@DisplayName("ElectionEventContextServiceTest")
class SetupComponentPublicKeysServiceTest {

	private static final ObjectMapper OBJECT_MAPPER = mock(ObjectMapper.class);
	private static final SetupComponentPublicKeysRepository SETUP_COMPONENT_PUBLIC_KEYS_REPOSITORY = mock(SetupComponentPublicKeysRepository.class);
	private static final ElectionEventService ELECTION_EVENT_SERVICE = mock(ElectionEventService.class);
	private static final MessageBrokerOrchestratorClient MESSAGE_BROKER_ORCHESTRATOR_CLIENT = mock(MessageBrokerOrchestratorClient.class);

	private static SetupComponentPublicKeysService setupComponentPublicKeysService;
	private static SetupComponentPublicKeysPayload setupComponentPublicKeysPayload;
	private static SetupComponentPublicKeys setupComponentPublicKeys;
	private static String tenantId;
	private static String electionEventId;
	private static ElectionEventEntity electionEventEntity;

	@BeforeAll
	static void setUpAll() throws IOException, ResourceNotFoundException {
		final ObjectMapper mapper = DomainObjectMapper.getNewInstance();
		final URL setupComponentPublicKeysPayloadUrl = SetupComponentPublicKeysServiceTest.class.getResource(
				"/setupComponentPublicKeysServiceTest/setup-component-public-keys-payload.json");
		setupComponentPublicKeysPayload = mapper.readValue(setupComponentPublicKeysPayloadUrl, SetupComponentPublicKeysPayload.class);
		setupComponentPublicKeys = setupComponentPublicKeysPayload.getSetupComponentPublicKeys();
		tenantId = "100";
		electionEventId = setupComponentPublicKeysPayload.getElectionEventId();
		setupComponentPublicKeysService = new SetupComponentPublicKeysService(mapper, SETUP_COMPONENT_PUBLIC_KEYS_REPOSITORY, ELECTION_EVENT_SERVICE,
				MESSAGE_BROKER_ORCHESTRATOR_CLIENT);

		final byte[] encryptionGroupBytes = mapper.writeValueAsBytes(setupComponentPublicKeysPayload.getEncryptionGroup());
		electionEventEntity = new ElectionEventEntity(electionEventId, encryptionGroupBytes);
		final byte[] electionPublicKey = mapper.writeValueAsBytes(setupComponentPublicKeys.electionPublicKey());
		final byte[] ccrEncryptionPublicKey = mapper.writeValueAsBytes(setupComponentPublicKeys.choiceReturnCodesEncryptionPublicKey());
		final SetupComponentPublicKeysEntity setupComponentPublicKeysEntity = new SetupComponentPublicKeysEntity(electionEventEntity, new byte[] {},
				new byte[] {}, new byte[] {}, electionPublicKey, ccrEncryptionPublicKey);
		when(SETUP_COMPONENT_PUBLIC_KEYS_REPOSITORY.findByElectionEvent(electionEventEntity)).thenReturn(setupComponentPublicKeysEntity);

		when(ELECTION_EVENT_SERVICE.retrieveElectionEventEntity(electionEventId)).thenReturn(electionEventEntity);
	}

	@Test
	@DisplayName("Retrieving the voting client public keys with invalid IDs throws")
	void retrievingInvalidIdsThrows() {
		assertAll(
				() -> assertThrows(NullPointerException.class,
						() -> setupComponentPublicKeysService.getVotingClientPublicKeys(null, electionEventId)),
				() -> assertThrows(NullPointerException.class,
						() -> setupComponentPublicKeysService.getVotingClientPublicKeys(tenantId, null)),
				() -> assertThrows(FailedValidationException.class,
						() -> setupComponentPublicKeysService.getVotingClientPublicKeys(tenantId, "invalid electionEventId"))
		);
	}

	@Test
	@DisplayName("Retrieving not saved election event context throws")
	void retrievingNotSavedElectionEventContextThrows() throws ResourceNotFoundException {
		when(SETUP_COMPONENT_PUBLIC_KEYS_REPOSITORY.findByElectionEvent(electionEventEntity)).thenThrow(ResourceNotFoundException.class);
		assertThrows(ResourceNotFoundException.class,
				() -> setupComponentPublicKeysService.getVotingClientPublicKeys(tenantId, electionEventId));
	}

	@Test
	@DisplayName("Retrieving the voting client public keys for a saved election event context does not throw")
	void retrievingSavedElectionEventContextDoesNotThrow() throws ResourceNotFoundException {
		final VotingClientPublicKeys expected = new VotingClientPublicKeys(setupComponentPublicKeysPayload.getEncryptionGroup(),
				setupComponentPublicKeys.electionPublicKey(), setupComponentPublicKeys.choiceReturnCodesEncryptionPublicKey());

		final VotingClientPublicKeys result;
		result = assertDoesNotThrow(
				() -> setupComponentPublicKeysService.getVotingClientPublicKeys(tenantId, electionEventId));
		verify(SETUP_COMPONENT_PUBLIC_KEYS_REPOSITORY, times(1)).findByElectionEvent(any());
		assertEquals(expected.encryptionParameters(), result.encryptionParameters());
		assertEquals(expected.electionPublicKey().getKeyElements(), result.electionPublicKey().getKeyElements());
		assertEquals(expected.choiceReturnCodesEncryptionPublicKey().getKeyElements(),
				result.choiceReturnCodesEncryptionPublicKey().getKeyElements());
	}
}
