/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.domain.SecurityLevelTestingOnly;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.DuplicateEntryException;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.ElectionEventContextEntity;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.ElectionEventContextRepository;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.ElectionEventEntity;
import ch.post.it.evoting.votingserver.voteverification.infrastructure.remote.MessageBrokerOrchestratorClient;

import retrofit2.Call;
import retrofit2.Response;

@SecurityLevelTestingOnly
@DisplayName("ElectionEventContextServiceTest")
class ElectionEventContextServiceTest {

	private static final ObjectMapper OBJECT_MAPPER = mock(ObjectMapper.class);
	private static final ElectionEventContextRepository ELECTION_EVENT_CONTEXT_REPOSITORY = mock(ElectionEventContextRepository.class);
	private static final ElectionEventService ELECTION_EVENT_SERVICE = mock(ElectionEventService.class);
	private static final MessageBrokerOrchestratorClient MESSAGE_BROKER_ORCHESTRATOR_CLIENT = mock(MessageBrokerOrchestratorClient.class);

	private static ElectionEventContextService electionEventContextService;
	private static ElectionEventContextPayload electionEventContextPayload;
	private static String electionEventId;
	private static ElectionEventEntity electionEventEntity;

	@BeforeAll
	static void setUpAll() throws IOException, ResourceNotFoundException {
		final ObjectMapper mapper = DomainObjectMapper.getNewInstance();
		final URL electionContextPayloadUrl = ElectionEventContextServiceTest.class.getResource(
				"/electionEventContextServiceTest/election-event-context-payload.json");
		electionEventContextPayload = mapper.readValue(electionContextPayloadUrl, ElectionEventContextPayload.class);
		electionEventId = electionEventContextPayload.getElectionEventContext().electionEventId();
		electionEventContextService = new ElectionEventContextService(ELECTION_EVENT_CONTEXT_REPOSITORY, ELECTION_EVENT_SERVICE,
				MESSAGE_BROKER_ORCHESTRATOR_CLIENT);

		final byte[] encryptionGroupBytes = mapper.writeValueAsBytes(electionEventContextPayload.getEncryptionGroup());
		electionEventEntity = new ElectionEventEntity(electionEventId, encryptionGroupBytes);
		final ElectionEventContextEntity electionEventContextEntity = new ElectionEventContextEntity(electionEventEntity, LocalDateTime.now(), LocalDateTime.now());
		when(ELECTION_EVENT_CONTEXT_REPOSITORY.findByElectionEvent(electionEventEntity)).thenReturn(Optional.of(electionEventContextEntity));

		when(ELECTION_EVENT_SERVICE.retrieveElectionEventEntity(electionEventId)).thenReturn(electionEventEntity);
	}

	@Test
	@DisplayName("Saving with null parameters throws NullPointerException")
	void savingNullThrows() {
		assertThrows(NullPointerException.class, () -> electionEventContextService.saveElectionEventContext(null));
	}

	@Test
	@DisplayName("Saving an already saved election event context throws DuplicateEntryException")
	void savingDuplicateThrows() throws DuplicateEntryException {
		when(ELECTION_EVENT_CONTEXT_REPOSITORY.save(any())).thenThrow(DuplicateEntryException.class);
		assertThrows(DuplicateEntryException.class, () -> electionEventContextService.saveElectionEventContext(electionEventContextPayload));
	}

	@Test
	@DisplayName("Saving a valid election event context does not throw")
	void savingValidElectionEventContextDoesNotThrow() throws IOException, DuplicateEntryException {
		@SuppressWarnings("unchecked")
		final Call<Void> callMockElectionContextResponsePayloads = (Call<Void>) mock(Call.class);
		when(callMockElectionContextResponsePayloads.execute()).thenReturn(Response.success(null));
		when(MESSAGE_BROKER_ORCHESTRATOR_CLIENT.uploadElectionEventContext(any(), any())).thenReturn(callMockElectionContextResponsePayloads);

		assertDoesNotThrow(() -> electionEventContextService.saveElectionEventContext(electionEventContextPayload));

		verify(ELECTION_EVENT_CONTEXT_REPOSITORY, times(1)).save(any());
		verify(MESSAGE_BROKER_ORCHESTRATOR_CLIENT, times(1)).uploadElectionEventContext(any(), any());
	}
}
