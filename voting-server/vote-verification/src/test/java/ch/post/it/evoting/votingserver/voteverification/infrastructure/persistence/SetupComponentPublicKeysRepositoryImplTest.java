/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.infrastructure.persistence;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.commons.infrastructure.persistence.BaseRepositoryImplTest;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.ElectionEventEntity;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.SetupComponentPublicKeysEntity;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.SetupComponentPublicKeysRepository;

@RunWith(MockitoJUnitRunner.class)
public class SetupComponentPublicKeysRepositoryImplTest extends BaseRepositoryImplTest<SetupComponentPublicKeysEntity, Integer> {

	@InjectMocks
	private static final SetupComponentPublicKeysRepository SETUP_COMPONENT_PUBLIC_KEYS_REPOSITORY = new SetupComponentPublicKeysRepositoryImpl();
	private final String electionEventId = "e3e3c2fd8a16489291c5c24e7b74b26e";
	@Mock
	private TypedQuery<SetupComponentPublicKeysEntity> queryMock;

	public SetupComponentPublicKeysRepositoryImplTest() throws InstantiationException, IllegalAccessException {
		super(SetupComponentPublicKeysEntity.class, SETUP_COMPONENT_PUBLIC_KEYS_REPOSITORY.getClass());
	}

	@Test
	public void testFindByElectionEventId() throws ResourceNotFoundException {
		final byte[] bytes = {};
		final ElectionEventEntity electionEventEntity = new ElectionEventEntity(electionEventId, bytes);
		final SetupComponentPublicKeysEntity setupComponentPublicKeysEntity = new SetupComponentPublicKeysEntity(electionEventEntity, bytes, bytes,
				bytes, bytes, bytes);
		when(entityManagerMock.createQuery(anyString(), eq(SetupComponentPublicKeysEntity.class))).thenReturn(queryMock);
		when(queryMock.getSingleResult()).thenReturn(setupComponentPublicKeysEntity);

		assertNotNull(SETUP_COMPONENT_PUBLIC_KEYS_REPOSITORY.findByElectionEvent(electionEventEntity));
	}

	@Test
	public void testFindByTenantIdElectionEventIdVerificationCardIdNotFound() throws ResourceNotFoundException {
		when(entityManagerMock.createQuery(anyString(), eq(SetupComponentPublicKeysEntity.class))).thenReturn(queryMock);
		when(queryMock.getSingleResult()).thenThrow(new NoResultException());

		expectedException.expect(ResourceNotFoundException.class);

		final ElectionEventEntity electionEventEntity = new ElectionEventEntity(electionEventId, new byte[] {});
		SETUP_COMPONENT_PUBLIC_KEYS_REPOSITORY.findByElectionEvent(electionEventEntity);
	}
}
