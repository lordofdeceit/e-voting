/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.ws.application.operation;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.SecurityLevelTestingOnly;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ApplicationException;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.commons.tracking.TrackIdInstance;
import ch.post.it.evoting.votingserver.voteverification.domain.common.VotingClientPublicKeys;
import ch.post.it.evoting.votingserver.voteverification.service.ElectionEventService;
import ch.post.it.evoting.votingserver.voteverification.service.SetupComponentPublicKeysService;

@SecurityLevelTestingOnly
@DisplayName("ElectionEventContextResource")
@ExtendWith(MockitoExtension.class)
class SetupComponentPublicKeysResourceTest {

	private static SetupComponentPublicKeysPayload setupComponentPublicKeysPayload;
	private static String tenantId;
	private static String electionEventId;
	private static String verificationCardSetId;
	private final String trackingId = new TrackIdInstance().getTrackId();

	@InjectMocks
	SetupComponentPublicKeysResource setupComponentPublicKeysResource;

	@Mock
	private HttpServletRequest mockServletRequest;

	@Mock
	private SetupComponentPublicKeysService setupComponentPublicKeysService;

	@Mock
	private ElectionEventService encryptionParametersService;

	@Mock
	private SignatureKeystore<Alias> signatureKeystoreService;

	@Mock
	private TrackIdInstance trackIdInstance;

	@BeforeAll
	static void setUpAll() throws IOException {
		final ObjectMapper mapper = DomainObjectMapper.getNewInstance();
		final URL setupComponentPublicKeysPayloadUrl = SetupComponentPublicKeysResourceTest.class.getResource(
				"/setupComponentPublicKeysServiceTest/setup-component-public-keys-payload.json");
		setupComponentPublicKeysPayload = mapper.readValue(setupComponentPublicKeysPayloadUrl, SetupComponentPublicKeysPayload.class);

		electionEventId = setupComponentPublicKeysPayload.getElectionEventId();
		tenantId = electionEventId;
		verificationCardSetId = electionEventId;
	}

	@Test
	@DisplayName("retrieve voting client public keys with valid parameters")
	void retrieveVotingClientPublicKeysHappyPath() throws ResourceNotFoundException, ApplicationException, IOException {
		final SetupComponentPublicKeys setupComponentPublicKeys = setupComponentPublicKeysPayload.getSetupComponentPublicKeys();
		final VotingClientPublicKeys votingClientPublicKeys = new VotingClientPublicKeys(setupComponentPublicKeysPayload.getEncryptionGroup(),
				setupComponentPublicKeys.electionPublicKey(), setupComponentPublicKeys.choiceReturnCodesEncryptionPublicKey());

		final String expected = DomainObjectMapper.getNewInstance().writeValueAsString(votingClientPublicKeys);

		when(setupComponentPublicKeysService.getVotingClientPublicKeys(tenantId, electionEventId)).thenReturn(
				votingClientPublicKeys);

		final Response response = setupComponentPublicKeysResource.getVotingClientPublicKeys(trackingId, tenantId, electionEventId,
				mockServletRequest);

		verify(setupComponentPublicKeysService, times(1)).getVotingClientPublicKeys(anyString(), anyString());

		assertNotNull(response);
		assertEquals(expected, response.getEntity());
		assertEquals(200, response.getStatus());
	}

	@Test
	@DisplayName("retrieve voting client public keys with invalid parameters throws")
	void retrieveVotingClientPublicKeysInvalidParametersThrows() {
		assertAll(
				() -> assertThrows(ApplicationException.class,
						() -> setupComponentPublicKeysResource.getVotingClientPublicKeys(trackingId, null, electionEventId, mockServletRequest)),
				() -> assertThrows(ApplicationException.class,
						() -> setupComponentPublicKeysResource.getVotingClientPublicKeys(trackingId, tenantId, null, mockServletRequest)),
				() -> assertThrows(ApplicationException.class,
						() -> setupComponentPublicKeysResource.getVotingClientPublicKeys(trackingId, tenantId, "invalid electionEventId",
								mockServletRequest))
		);
	}
}
