/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.returncodes.ShortChoiceReturnCodeAndComputeResults;
import ch.post.it.evoting.domain.returncodes.VoteAndComputeResults;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentLCCSharePayload;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentPartialDecryptPayload;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.voteverification.domain.model.content.SetupComponentVerificationCardKeystoreEntity;
import ch.post.it.evoting.votingserver.voteverification.domain.model.electioncontext.ElectionEventEntity;
import ch.post.it.evoting.votingserver.voteverification.infrastructure.remote.MessageBrokerOrchestratorClient;
import ch.post.it.evoting.votingserver.voteverification.voting.sendvote.ExtractCRCAlgorithm;
import ch.post.it.evoting.votingserver.voteverification.voting.sendvote.ExtractCRCOutput;

import retrofit2.Call;
import retrofit2.Response;

@DisplayName("ChoiceReturnCodesService")
@ExtendWith(MockitoExtension.class)
class ChoiceReturnCodesServiceTest {

	private final String tenantId = "100";
	private final String electionEventId = "1882e6ff0c6e4557808272f8841b6af0";
	private final String verificationCardId = "e4a26a470347c9f54324866a185893d2";
	private final String verificationCardSetId = "22484e401f6b4bb188ea9e2ef24e7b3f";

	private final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
	private ExtractCRCAlgorithm mockExtractCRCAlgorithm;
	private ChoiceReturnCodesService choiceReturnCodesService;
	private SetupComponentVerificationCardKeystoreService mockSetupComponentVerificationCardKeystoreService;
	private SignatureKeystore<Alias> mockSignatureKeystoreService;
	private MessageBrokerOrchestratorClient mockMessageBrokerOrchestratorClient;
	private VoteAndComputeResults voteAndComputeResults;

	private static InputStream getResourceAsStream(final String name) {
		return ChoiceReturnCodesServiceTest.class.getResourceAsStream("/choiceReturnCodesServiceTest/" + name);
	}

	@BeforeEach
	void setUp() throws SignatureException, ResourceNotFoundException, IOException {
		mockExtractCRCAlgorithm = mock(ExtractCRCAlgorithm.class);
		mockSetupComponentVerificationCardKeystoreService = mock(SetupComponentVerificationCardKeystoreService.class);
		mockSignatureKeystoreService = mock(SignatureKeystore.class);
		final ElectionEventService mockElectionEventService = mock(ElectionEventService.class);
		final ReturnCodesMappingTableService mockReturnCodesMappingTableService = mock(ReturnCodesMappingTableService.class);
		mockMessageBrokerOrchestratorClient = mock(MessageBrokerOrchestratorClient.class);

		choiceReturnCodesService = new ChoiceReturnCodesService(
				objectMapper,
				mockExtractCRCAlgorithm,
				mockSignatureKeystoreService,
				mockElectionEventService,
				mockReturnCodesMappingTableService,
				mockMessageBrokerOrchestratorClient,
				mockSetupComponentVerificationCardKeystoreService);

		final String dummySignature = "dummySignature";
		when(mockSignatureKeystoreService.generateSignature(any(), any())).thenReturn(dummySignature.getBytes(StandardCharsets.UTF_8));

		final GqGroup gqGroup = new GqGroup(
				new BigInteger(
						"4688924687101842747043789622943639451238379583421515083490992727662966887592566806385937818968022125389969548661587837554751555551125316712096348517237427873704786293289327916804886782297937842786976257115026543950184245775805780806466220397371589271121288423507399259602000829340247207828163695625078614543895796426520749021726851028889703185286047412971103954221566262244551464311742715148749253272752397456639673970809661134301137187709504653404337846916552289737947354931132013578668381751783880929044628310827581093820299384525520498865891279620245835238216578248104372858793870818677470303875907983918128169426975079981824932085456362035949171853839641915630629131160070262536188292899390408699336228786000874475423989873636678599713537438189405718684830889918695758274995475275472436678812224043372657209577592652124268309300980776740510688847875393810499063675510899395946989871928027318991345508642195522561212328039"),
				new BigInteger(
						"2344462343550921373521894811471819725619189791710757541745496363831483443796283403192968909484011062694984774330793918777375777775562658356048174258618713936852393146644663958402443391148968921393488128557513271975092122887902890403233110198685794635560644211753699629801000414670123603914081847812539307271947898213260374510863425514444851592643023706485551977110783131122275732155871357574374626636376198728319836985404830567150568593854752326702168923458276144868973677465566006789334190875891940464522314155413790546910149692262760249432945639810122917619108289124052186429396935409338735151937953991959064084713487539990912466042728181017974585926919820957815314565580035131268094146449695204349668114393000437237711994936818339299856768719094702859342415444959347879137497737637736218339406112021686328604788796326062134154650490388370255344423937696905249531837755449697973494935964013659495672754321097761280606164019"),
				BigInteger.valueOf(2));
		final ElectionEventEntity electionEventEncryptionParametersEntity = new ElectionEventEntity(electionEventId, objectMapper.writeValueAsBytes(gqGroup));
		when(mockElectionEventService.retrieveElectionEventEntity(electionEventId))
				.thenReturn(electionEventEncryptionParametersEntity);

		final SetupComponentVerificationCardKeystoreEntity setupComponentVerificationCardKeystoreEntity = new SetupComponentVerificationCardKeystoreEntity();
		setupComponentVerificationCardKeystoreEntity.setVerificationCardSetId(verificationCardSetId);
		when(mockSetupComponentVerificationCardKeystoreService.load(electionEventId, verificationCardId))
				.thenReturn(setupComponentVerificationCardKeystoreEntity);

		voteAndComputeResults = objectMapper.readValue(getResourceAsStream("voteAndComputeResults.json"), VoteAndComputeResults.class);
	}

	@Test
	@DisplayName("retrieveShortChoiceReturnCodes with valid parameters and happy path")
	void retrieveShortChoiceReturnCodesHappyPath() throws IOException, ResourceNotFoundException, SignatureException {
		// given
		setupMockMessageBrokerOrchestratorClient("partiallyDecryptedEncryptedPCCPayloads/happyPath.json");

		final Call<List<ControlComponentLCCSharePayload>> callMockLongReturnCodesSharePayload = mock(Call.class);
		final List<ControlComponentLCCSharePayload> longReturnCodesSharePayload = Arrays.asList(
				objectMapper.readValue(getResourceAsStream("longReturnCodesSharePayloads.json"), ControlComponentLCCSharePayload[].class));
		final Response<List<ControlComponentLCCSharePayload>> longReturnCodesSuccess = Response.success(longReturnCodesSharePayload);
		when(callMockLongReturnCodesSharePayload.execute()).thenReturn(longReturnCodesSuccess);
		final List<GroupVector<GqElement, GqGroup>> lccShares = choiceReturnCodesService.getLCCShares(longReturnCodesSharePayload);
		when(mockExtractCRCAlgorithm.extractCRC(any(), any())).thenReturn(getExtractCRCOutput(lccShares.get(0).size()));
		when(mockMessageBrokerOrchestratorClient.getLongChoiceReturnCodesContributions(eq(electionEventId), eq(verificationCardSetId),
				eq(verificationCardId), any())).thenReturn(callMockLongReturnCodesSharePayload);

		when(mockSignatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(true);

		// when
		final ShortChoiceReturnCodeAndComputeResults shortChoiceReturnCodeAndComputeResultsResponse = choiceReturnCodesService.retrieveShortChoiceReturnCodes(
				tenantId, electionEventId, verificationCardId, voteAndComputeResults);

		// then
		verify(mockSetupComponentVerificationCardKeystoreService).load(electionEventId, verificationCardId);
		verify(mockSignatureKeystoreService, atLeast(1)).verifySignature(any(), any(), any(), any());
		verify(mockSignatureKeystoreService).generateSignature(any(), any());

		assertNotNull(shortChoiceReturnCodeAndComputeResultsResponse);
		assertNotNull(shortChoiceReturnCodeAndComputeResultsResponse.getComputationResults());
	}

	@Test
	@DisplayName("retrieveShortChoiceReturnCodes with different gqGroups")
	void allGqGroupsAreNotTheSame() throws IOException {
		// given
		setupMockMessageBrokerOrchestratorClient("partiallyDecryptedEncryptedPCCPayloads/gqGroupDifferent.json");

		// when / then
		assertThrows(IllegalStateException.class, () -> choiceReturnCodesService.retrieveShortChoiceReturnCodes(
				tenantId, electionEventId, verificationCardId, voteAndComputeResults), "GqGroup is not identical for all the payloads.");
	}

	@Test
	@DisplayName("retrieveShortChoiceReturnCodes with different gqGroups")
	void allContextIdsAreNotTheSame() throws IOException {
		// given
		setupMockMessageBrokerOrchestratorClient("partiallyDecryptedEncryptedPCCPayloads/contextIdsDifferent.json");

		// when / then
		assertThrows(IllegalStateException.class, () -> choiceReturnCodesService.retrieveShortChoiceReturnCodes(
				tenantId, electionEventId, verificationCardId, voteAndComputeResults), "ContextIds are not identical for all the payloads.");
	}

	private void setupMockMessageBrokerOrchestratorClient(final String path) throws IOException {

		final InputStream is = getResourceAsStream(path);
		final List<ControlComponentPartialDecryptPayload> controlComponentPartialDecryptPayloads = Arrays.asList(
				objectMapper.readValue(is, ControlComponentPartialDecryptPayload[].class));
		final Response<List<ControlComponentPartialDecryptPayload>> partiallyDecryptedSuccess = Response.success(
				controlComponentPartialDecryptPayloads);

		final Call<List<ControlComponentPartialDecryptPayload>> callMockControlComponentPartialDecryptPayload = mock(Call.class);
		when(callMockControlComponentPartialDecryptPayload.execute()).thenReturn(partiallyDecryptedSuccess);
		when(mockMessageBrokerOrchestratorClient.getChoiceReturnCodesPartialDecryptContributions(eq(electionEventId), eq(verificationCardSetId),
				eq(verificationCardId), any())).thenReturn(callMockControlComponentPartialDecryptPayload);
	}

	private ExtractCRCOutput getExtractCRCOutput(final int psi) {
		final List<String> shortChoiceReturnCodes = new ArrayList<>();
		for (int i = 0; i < psi; i++) {
			shortChoiceReturnCodes.add("1234");
		}
		return new ExtractCRCOutput(shortChoiceReturnCodes);
	}
}
