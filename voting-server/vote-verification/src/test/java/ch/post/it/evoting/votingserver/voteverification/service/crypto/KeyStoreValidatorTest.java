/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.service.crypto;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import ch.post.it.evoting.cryptoprimitives.domain.signature.Alias;

class KeyStoreValidatorTest {

	@TempDir
	static Path tempKeystorePath;

	/**
	 * When the KeyStoreValidator will be migrated to crypto primitives, refactor this test to use in memory keystore/password instead of files.
	 */
	@Test
	void test() throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
		// given
		final String keystoreLocation = tempKeystorePath.resolve("signing_keystore_test.p12").toString();
		final String keystorePasswordLocation = tempKeystorePath.resolve("signing_pw_test.txt").toString();
		final Alias alias = Alias.CONTROL_COMPONENT_1;
		KeystoreFilesCreator.create(keystoreLocation, keystorePasswordLocation, alias.get());
		final KeyStore keyStore = KeyStore.getInstance("PKCS12");
		keyStore.load(Files.newInputStream(Paths.get(keystoreLocation)),
				new String(Files.readAllBytes(Paths.get(keystorePasswordLocation)), StandardCharsets.UTF_8).toCharArray());

		// when
		final boolean isValid = KeyStoreValidator.validateKeyStore(keyStore, alias);

		// then
		Assertions.assertTrue(isValid);
	}
}