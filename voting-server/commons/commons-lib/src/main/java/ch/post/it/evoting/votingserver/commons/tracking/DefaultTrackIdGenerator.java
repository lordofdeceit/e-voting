/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.commons.tracking;

import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;

public class DefaultTrackIdGenerator implements TrackIdGenerator {

	private static final int LENGTH_IN_CHARS = 16;

	private final Random random = RandomFactory.createRandom();

	@Override
	public String generate() {
		return generate(LENGTH_IN_CHARS);
	}

	@Override
	public String generate(final int length) {
		return random.genRandomBase32String(length);
	}
}
