/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.commons.beans.utils;

import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import java.util.Base64;

import ch.post.it.evoting.cryptolib.api.asymmetric.AsymmetricServiceAPI;
import ch.post.it.evoting.cryptolib.api.exceptions.GeneralCryptoLibException;

/**
 * Allows a password (string) to be encrypted and decrypted.
 */
public final class PasswordEncrypter {

	private final AsymmetricServiceAPI asymmetricService;

	public PasswordEncrypter(final AsymmetricServiceAPI asymmetricService) {

		this.asymmetricService = asymmetricService;
	}

	/**
	 * Decrpyt the received encrypted password using the received private key.
	 *
	 * @param encryptedPassword the encrypted password to be decrypted.
	 * @param privateKey        a private key.
	 * @return the decrypted password.
	 * @throws GeneralCryptoLibException
	 */
	public String decryptPassword(final String encryptedPassword, final PrivateKey privateKey) throws GeneralCryptoLibException {

		final byte[] ciphertextAsBytes = Base64.getDecoder().decode(encryptedPassword);

		final byte[] plaintextAsBytes = asymmetricService.decrypt(privateKey, ciphertextAsBytes);

		return new String(plaintextAsBytes, StandardCharsets.UTF_8);
	}
}
