/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.commons.sign.beans;

public class SignatureFieldsConstants {

	public static final String SIG_FIELD_VERSION = "version";

	public static final String SIG_FIELD_SIGNED = "signed";

	public static final String SIG_FIELD_SIGNATURE = "signature";

	private SignatureFieldsConstants() {
	}
}
