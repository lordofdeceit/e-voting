/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.commons.beans;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public record TestConstructorBean(String fieldOne, Integer fieldTwo) {

	@JsonCreator
	public TestConstructorBean(
			@JsonProperty("fieldOne")
			String fieldOne,
			@JsonProperty("fieldTwo")
			Integer fieldTwo) {
		this.fieldOne = fieldOne;
		this.fieldTwo = fieldTwo;
	}

}
