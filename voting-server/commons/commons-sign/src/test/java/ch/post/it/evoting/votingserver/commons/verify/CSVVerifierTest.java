/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.commons.verify;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.KeyPair;
import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import ch.post.it.evoting.cryptolib.api.exceptions.GeneralCryptoLibException;
import ch.post.it.evoting.cryptolib.asymmetric.service.AsymmetricService;
import ch.post.it.evoting.votingserver.commons.sign.CSVSigner;

public class CSVVerifierTest {

	private static final AsymmetricService asymmetricService = new AsymmetricService();
	private static final Path sourceCSVNotSigned = Paths.get("src/test/resources/example.csv");
	private static final Path outputCSV = Paths.get("target/signedExample.csv");
	private static final Path outputCSVNotSigned = Paths.get("target/exampleNotSigned.csv");
	private static CSVVerifier csvVerifier;
	private static KeyPair keyPairForSigning;

	@BeforeClass
	public static void setUp() throws GeneralCryptoLibException, IOException {

		csvVerifier = new CSVVerifier();

		keyPairForSigning = asymmetricService.getKeyPairForSigning();

		Files.copy(sourceCSVNotSigned, outputCSV, StandardCopyOption.REPLACE_EXISTING);
		Files.copy(sourceCSVNotSigned, outputCSVNotSigned, StandardCopyOption.REPLACE_EXISTING);
	}

	@Test
	public void verify_a_given_signed_csv_file() throws IOException, GeneralCryptoLibException {
		CSVSigner signer = new CSVSigner();

		KeyPair keyPairForSigning = asymmetricService.getKeyPairForSigning();
		int originalNumberOfLines = Files.readAllLines(outputCSV).size();
		signer.sign(keyPairForSigning.getPrivate(), outputCSV);

		int numberLines = Files.readAllLines(outputCSV).size();
		boolean verified = csvVerifier.verify(keyPairForSigning.getPublic(), outputCSV);

		assertEquals(numberLines - 2, Files.readAllLines(outputCSV).size());
		assertEquals(numberLines - 2, originalNumberOfLines);
		assertTrue(verified);
	}

	@Test
	public void not_verify_a_given_signed_csv_file_with_a_modified_signature() throws IOException, GeneralCryptoLibException {

		KeyPair otherKeyPair;
		do {
			otherKeyPair = asymmetricService.getKeyPairForSigning();
		} while (otherKeyPair.equals(keyPairForSigning));

		CSVSigner signer = new CSVSigner();
		signer.sign(otherKeyPair.getPrivate(), outputCSV);

		boolean verified = csvVerifier.verify(keyPairForSigning.getPublic(), outputCSV);

		assertFalse(verified);
	}

	@Test
	public void not_verify_a_given_signed_csv_file_with_a_modified_content() throws IOException, GeneralCryptoLibException {

		BufferedReader csvReader = new BufferedReader(new FileReader(outputCSV.toFile()));
		List<String> data = new ArrayList<>();
		String row;
		while ((row = csvReader.readLine()) != null) {
			data.add(row);
		}
		csvReader.close();
		try (FileWriter fileWriter = new FileWriter(outputCSV.toFile())) {
			data.forEach(line -> {
				try {
					fileWriter.append(line);
					fileWriter.append("\n");
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			});
			fileWriter.write("\n");
			fileWriter.write("modified");
		}
		boolean verified = csvVerifier.verify(keyPairForSigning.getPublic(), outputCSV);

		assertFalse(verified);
	}

	@Test
	public void throw_exception_when_non_existing_file_is_passed() {
		assertThrows(IOException.class, () -> csvVerifier.verify(keyPairForSigning.getPublic(), Paths.get("blabla.csv")));
	}

	@Test
	public void throw_exception_when_null_file_path_is_passed() {
		assertThrows(IOException.class, () -> csvVerifier.verify(keyPairForSigning.getPublic(), null));
	}

}
