/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.commons.infrastructure.csv;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.csv.CsvFactory;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

/**
 * Abstract CSV writer for a generic Class
 *
 * @param <T>
 */
public abstract class AbstractCSVWriter<T> implements Closeable, Flushable {

	protected ObjectWriter csvWriter;

	protected OutputStream outputStream;

	/**
	 * Creates a CSVWriter for a given outputStream
	 *
	 * @param outputStream
	 */
	protected AbstractCSVWriter(final OutputStream outputStream) {
		this(outputStream, CSVConstants.DEFAULT_CHARSET, CSVConstants.DEFAULT_SEPARATOR, CSVConstants.NO_QUOTE_CHARACTER,
				CSVConstants.NO_ESCAPE_CHARACTER);
	}

	/**
	 * Creates a CSVWriter for the given parameters
	 *
	 * @param outputStream
	 * @param charset      - accepted charset
	 * @param separator    - separator that splits into columns
	 * @param quote
	 */
	protected AbstractCSVWriter(final OutputStream outputStream, final Charset charset, final char separator, final char quote, final char escape) {
		createCSVWriter(outputStream, charset, separator, quote, escape);
	}

	/**
	 * @param outputStream
	 * @param charset      - accepted charset
	 * @param separator    - separator that splits into columns
	 * @param quote
	 */
	protected void createCSVWriter(final OutputStream outputStream, final Charset charset, final char separator, final char quote,
			final char escape) {
		final CsvSchema csvSchema = CsvSchema.builder()
				.setColumnSeparator(separator)
				.disableQuoteChar()
				.setEscapeChar(escape)
				.setLineSeparator(String.valueOf(CsvSchema.DEFAULT_LINEFEED))
				.build();

		final CsvFactory csvFactory = new CsvFactory();
		csvFactory.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
		this.csvWriter = new CsvMapper(csvFactory).writer(csvSchema);
		this.outputStream = outputStream;
	}

	/**
	 * Write an object in the csv
	 *
	 * @param object
	 */
	public void write(final T object) throws IOException {
		final String[] line = extractValues(object);
		csvWriter.writeValue(outputStream, line);
	}

	/**
	 * Extracts the values of an object into an array
	 *
	 * @param object
	 * @return
	 */
	protected abstract String[] extractValues(T object);

	/**
	 * Closes the writer
	 *
	 * @throws IOException
	 */
	@Override
	public void close() throws IOException {
		// close already flushes
		outputStream.close();
	}

	/**
	 * Flush the content in the writer
	 *
	 * @throws IOException
	 */
	@Override
	public void flush() throws IOException {
		outputStream.flush();
	}

}
