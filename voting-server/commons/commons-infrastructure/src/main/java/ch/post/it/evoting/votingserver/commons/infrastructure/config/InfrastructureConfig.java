/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.commons.infrastructure.config;

public class InfrastructureConfig {

	private InfrastructureConfig() {
	}

	public static String getEnvWithDefaultOption(String environmentalVariableName, String defaultValue) {
		String environmentalVariableValue = System.getenv(environmentalVariableName);
		return environmentalVariableValue == null ? defaultValue : environmentalVariableValue;
	}
}
