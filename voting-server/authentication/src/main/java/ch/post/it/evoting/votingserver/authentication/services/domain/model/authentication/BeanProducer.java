/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.authentication.services.domain.model.authentication;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;

import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;

@Dependent
public class BeanProducer {

	@Produces
	public Random getRandom() {
		return RandomFactory.createRandom();
	}
}
