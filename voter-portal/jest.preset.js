/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
const nxPreset = require('@nrwl/jest/preset');

module.exports = {
  ...nxPreset,
  setupFilesAfterEnv: ['./global-test-setup.ts'],
};
