/*
 * (c) Copyright 2022 Swiss Post Ltd
 */
const {getJestProjects} = require('@nrwl/jest');

module.exports = {
  projects: getJestProjects(),
};
