/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {Store} from '@ngrx/store';

import * as Actions from '@swiss-post/shared/state';
import {IBackendConfig} from '@swiss-post/types';
import {TranslateService} from '@ngx-translate/core';
import {ConfigurationService} from '@swiss-post/shared/configuration';

@Component({
  selector: 'swp-legal-terms',
  templateUrl: './legal-terms.component.html',
  styleUrls: ['./legal-terms.component.scss'],
})
export class LegalTermsComponent implements OnInit {
  private readonly tenantId = '100';
  private readonly libPath = 'crypto.ov-api.min.js';
  electionEventId!: string;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly store: Store,
    private readonly configurationService: ConfigurationService,
    private readonly translate: TranslateService
  ) {
  }

  ngOnInit(): void {
    this.electionEventId = this.route.snapshot.paramMap.get(
      'electionEventId'
    ) as string;
  }

  agree(): void {
    const platformRootCA = this.configurationService.configuration?.platformRootCA;

    if (!platformRootCA || platformRootCA.trim() === '') {
      throw new Error("The platformRootCA is undefined or empty.")
    }

    const config: IBackendConfig = {
      lib: this.libPath,
      lang: this.translate.currentLang,
      tenantId: this.tenantId,
      platformRootCA: platformRootCA,
      electionEventId: this.electionEventId,
    };

    this.store.dispatch(Actions.agree({config}));
    this.router.navigate(['/start-voting']);
  }
}
