/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {provideMockStore} from '@ngrx/store/testing';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';

import {LegalTermsComponent} from './legal-terms.component';
import {ConfigurationService} from "@swiss-post/shared/configuration";
import {MockProvider} from "ng-mocks";
import {Identification, IVoterPortalConfig} from "@swiss-post/types";

describe('LegalTermsComponent', () => {
	let component: LegalTermsComponent;
	let fixture: ComponentFixture<LegalTermsComponent>;
	let voterPortalConfig: IVoterPortalConfig;

	beforeEach(async () => {
		voterPortalConfig = {
			identification: Identification.YEAR_OF_BIRTH,
			contestsCapabilities: {
				writeIns: true,
			},
			platformRootCA: ''
		};

		await TestBed.configureTestingModule({
			imports: [
				RouterTestingModule,
				HttpClientTestingModule,
				TranslateTestingModule.withTranslations({}),
			],
			declarations: [LegalTermsComponent],
			providers: [
				provideMockStore({}),
				MockProvider(ConfigurationService, {
					configuration: voterPortalConfig
				}),
			],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(LegalTermsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('platform root ca undefined', () => {
		expect(() => component.agree()).toThrowError("The platformRootCA is undefined or empty.")
	});
});
