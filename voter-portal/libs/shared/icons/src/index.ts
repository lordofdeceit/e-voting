/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
export {SharedIconsModule} from './lib/shared-icons.module';
export * from './lib/icon/icon.component';
