/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import * as fromSharedState from './+state/shared-state.reducer';
import {SharedStateEffects} from './+state/shared-state.effects';
import {BackendModule} from '@swiss-post/backend';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(
      fromSharedState.SHARED_FEATURE_KEY,
      fromSharedState.reducer
    ),
    EffectsModule.forFeature([SharedStateEffects]),
    BackendModule
  ],
})
export class SharedStateModule {
}
