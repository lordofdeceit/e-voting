/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {createAction, props} from '@ngrx/store';
import {
  IVoter,
  IBallotResponse,
  IBackendConfig,
  BackendError,
  IBallotUserData,
  ISendVoteResponse,
  IConfirmVote,
  IConfirmVoteResponse,
} from '@swiss-post/types';

export enum LanguageSelectorActionTypes {
  SetLanguage = '[Language Selector] Language click',
  TranslateBallot = '[Language Selector] Translate Ballot',
  TranslateBallotSuccess = '[Language Selector/API] Translate Ballot Success',
  TranslateBallotFailure = '[Language Selector/API] Translate Ballot Failure',
}

export enum LegalTermsActionTypes {
  Agree = '[Legal Terms Page] Agree click',
}

export enum SharedActionTypes {
  Logout = '[Logout] Navigate away or page reload',
  ClearBackendError = '[ClearBackendError] Clear error from server',
}

export enum StartVotingActionTypes {
  Start = '[StartVoting Page] Start(Login) click',
  LoadSvkSuccess = '[StartVoting/API] Load Start Voting Key Success',
  LoadSvkFailure = '[StartVoting/API] Load Start Voting Key Failure',
  LoadBallotSuccess = '[StartVoting/API] Load Ballot Success',
  LoadBallotFailure = '[StartVoting/API] Load Ballot Failure',
  RequestChoiceCodes = '[StartVoting Page] Request Choice Codes',
  LoadChoiceCodesSuccess = '[StartVoting Page] Load Choice Codes Success',
  LoadChoiceCodesFailure = '[StartVoting Page] Load Choice Codes Failure',
  RequestVoteCastCode = '[StartVoting] Request Vote Cast Code',
  LoadVoteCastCodeSuccess = '[StartVoting/API] Load Vote Cast Code Success',
  LoadVoteCastCodeFailure = '[StartVoting/API] Load Vote Cast Code Failure',
}

export enum ChooseActionTypes {
  Review = '[Choose Page] Review click',
  SaveForm = '[Save Form] Unload component',
}

export enum ReviewActionTypes {
  Seal = '[ReviewSend Page] Seal Vote click',
  LoadSealedVoteSuccess = '[ReviewSend/API] Load Sealed Vote Success',
  LoadSealedVoteFailure = '[ReviewSend/API] Load Sealed Vote Failure',
}

export enum VerifyActionTypes {
  ConfirmVote = '[Verify Page] Confirm Vote click',
  LoadConfirmedVoteSuccess = '[Verify/API] Load Confirmed Vote Success',
  LoadConfirmedVoteFailure = '[Verify/API] Load Confirmed Vote Failure',
}

export const setLanguage = createAction(
  LanguageSelectorActionTypes.SetLanguage,
  props<{ lang: string }>()
);

export const translateBallotSuccess = createAction(
  LanguageSelectorActionTypes.TranslateBallotSuccess,
  props<{ ballot: IBallotResponse }>()
);

export const translateBallotFailure = createAction(
  LanguageSelectorActionTypes.TranslateBallotFailure,
  props<{ error: BackendError }>()
);

export const translateBallot = createAction(
  LanguageSelectorActionTypes.TranslateBallot
);

export const agree = createAction(
  LegalTermsActionTypes.Agree,
  props<{ config: IBackendConfig }>()
);

export const start = createAction(
  StartVotingActionTypes.Start,
  props<{ voter: IVoter }>()
);

export const loadSvkSuccess = createAction(
  StartVotingActionTypes.LoadSvkSuccess,
  props<{ startVotingKey: string }>()
);

export const clearBackendError = createAction(
  SharedActionTypes.ClearBackendError
);

export const loadSvkFailure = createAction(
  StartVotingActionTypes.LoadSvkFailure,
  props<{ error: BackendError }>()
);

export const loadBallotSuccess = createAction(
  StartVotingActionTypes.LoadBallotSuccess,
  props<{ ballot: IBallotResponse; requestAdditionalData: boolean }>()
);

export const loadChoiceCodesSuccess = createAction(
  StartVotingActionTypes.LoadChoiceCodesSuccess,
  props<{ choiceCodes: ISendVoteResponse }>()
);

export const loadVoteCastCodeSuccess = createAction(
  StartVotingActionTypes.LoadVoteCastCodeSuccess,
  props<{ confirmVoteCode: IConfirmVoteResponse }>()
);

export const loadBallotFailure = createAction(
  StartVotingActionTypes.LoadBallotFailure,
  props<{ error: BackendError }>()
);

export const loadChoiceCodesFailure = createAction(
  StartVotingActionTypes.LoadChoiceCodesFailure,
  props<{ error: BackendError }>()
);

export const loadVoteCastCodeFailure = createAction(
  StartVotingActionTypes.LoadVoteCastCodeFailure,
  props<{ error: BackendError }>()
);

export const review = createAction(ChooseActionTypes.Review);

export const saveForm = createAction(
  ChooseActionTypes.SaveForm,
  props<{ ballotUserData: IBallotUserData }>()
);

export const seal = createAction(ReviewActionTypes.Seal);

export const loadSealedVoteSuccess = createAction(
  ReviewActionTypes.LoadSealedVoteSuccess,
  props<{ voteResponse: ISendVoteResponse }>()
);

export const loadSealedVoteFailure = createAction(
  ReviewActionTypes.LoadSealedVoteFailure,
  props<{ error: BackendError }>()
);

export const confirmVote = createAction(
  VerifyActionTypes.ConfirmVote,
  props<{ confirmVote: IConfirmVote }>()
);

export const loadConfirmVoteSuccess = createAction(
  VerifyActionTypes.LoadConfirmedVoteSuccess,
  props<{ confirmVoteResponse: IConfirmVoteResponse }>()
);

export const loadConfirmVoteFailure = createAction(
  VerifyActionTypes.LoadConfirmedVoteFailure,
  props<{ error: BackendError }>()
);

export const requestChoiceCodes = createAction(
  StartVotingActionTypes.RequestChoiceCodes
);

export const requestVoteCastCode = createAction(
  StartVotingActionTypes.RequestVoteCastCode
);

export const logout = createAction(SharedActionTypes.Logout);
