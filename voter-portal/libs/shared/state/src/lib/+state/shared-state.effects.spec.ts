/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {provideMockActions} from '@ngrx/effects/testing';
import {Action} from '@ngrx/store';
import {provideMockStore} from '@ngrx/store/testing';
import {NxModule} from '@nrwl/angular';
import {IVoter} from '@swiss-post/types';
import {Observable, of} from 'rxjs';

import * as StartVotingActions from './shared-state.actions';
import {SharedStateEffects} from './shared-state.effects';
import {hot} from 'jasmine-marbles';
import {ProgressService} from '@swiss-post/shared/ui';
import {BackendService} from '@swiss-post/backend';

describe('SharedStateEffects', () => {
  let actions: Observable<Action>;
  let effects: SharedStateEffects;
  let authService: BackendService;
  let progressService: ProgressService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NxModule.forRoot(), RouterTestingModule],
      providers: [
        SharedStateEffects,
        provideMockActions(() => actions),
        provideMockStore(),
        BackendService,
      ],
    });

    effects = TestBed.inject(SharedStateEffects);
    authService = TestBed.inject(BackendService);
    progressService = TestBed.inject(ProgressService);
  });

  describe('auhenticateVoter$', () => {
    it('should work', () => {
      const mockVoter: IVoter = {
        initializationCode: 'mockInitializationCode',
        dobOrYob: 'mockDob',
      };
      const mockStartVotingKey = 'mockStartVotingKey';
      actions = hot('-a-|', {
        a: StartVotingActions.start({voter: mockVoter}),
      });

      authService.authenticate = jest
        .fn()
        .mockImplementationOnce(() => of(mockStartVotingKey));

      progressService.open = jest.fn().mockImplementationOnce(() => { /* do nothing */
      });

      const expected = hot('-a-|', {
        a: StartVotingActions.loadSvkSuccess({
          startVotingKey: 'mockStartVotingKey',
        }),
      });

      expect(effects.authenticateVoter$).toBeObservable(expected);
    });
  });
});
