/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {createReducer, on, Action} from '@ngrx/store';
import {
  IBackendConfig,
  IBallotResponse,
  BackendError,
  IBallotUserData,
  ISendVoteResponse,
  IConfirmVote,
  IConfirmVoteResponse,
} from '@swiss-post/types';
import * as Actions from './shared-state.actions';

export const SHARED_FEATURE_KEY = 'sharedState';

export interface SharedState {
  isAuthenticated: boolean;
  config: IBackendConfig;
  isRequestingBallot?: boolean;
  isRequestingAdditionalData: boolean;
  error?: BackendError | null; // last known error (if any)
  startVotingKey?: string;
  ballot?: IBallotResponse | null;
  ballotUserData?: IBallotUserData | null;
  voteResponse?: ISendVoteResponse | null;
  confirmVote?: IConfirmVote | null;
  confirmVoteResponse?: IConfirmVoteResponse | null;
  loading: boolean;
  sentButNotCast: boolean; //User sent vote in last session.
  cast: boolean; //User casted vote in last session
}

export const initialState: SharedState = {
  // set initial required properties
  isAuthenticated: false,
  loading: false,
  error: null,
  config: {
    lib: '',
    lang: '',
    tenantId: '',
    electionEventId: '',
    platformRootCA: '',
  },
  sentButNotCast: false,
  cast: false,
  isRequestingAdditionalData: false,
};

const stateReducer = createReducer(
  initialState,
  on(Actions.setLanguage, (state, {lang}) => ({
    ...state,
    config: {...state.config, lang},
  })),
  on(Actions.translateBallot, (state) => ({
    ...state,
    loading: true,
  })),
  on(Actions.translateBallotSuccess, (state, {ballot}) => ({
    ...state,
    ballot,
    loading: false,
  })),
  on(Actions.translateBallotFailure, (state, {error}) => ({
    ...state,
    error,
    loading: false,
  })),
  on(Actions.agree, (state, {config}) => ({
    ...state,
    config,
  })),
  on(Actions.start, (state) => ({
    ...state,
    loading: true,
    isAuthenticated: false,
    ballot: null,
    cast: false,
    sentButNotCast: false,
    error: null,
  })),
  on(Actions.loadSvkSuccess, (state, {startVotingKey}) => ({
    ...state,
    startVotingKey: startVotingKey,
    isAuthenticated: true,
    error: null,
    isRequestingBallot: true,
    isRequestingAdditionalData: true,
  })),
  on(Actions.loadSvkFailure, (state, {error}) => ({
    ...state,
    error,
    loading: false,
    isAuthenticated: false,
  })),
  on(Actions.clearBackendError, (state) => ({
    ...state,
    loading: false,
    error: null,
  })),
  on(Actions.loadBallotSuccess, (state, {ballot, requestAdditionalData}) => ({
    ...state,
    ballot,
    loading: false,
    isRequestingBallot: false,
    isRequestingAdditionalData: requestAdditionalData,
    error: null,
    ballotUserData: null,
  })),
  on(Actions.loadBallotFailure, (state, {error}) => ({
    ...state,
    error,
    loading: false,
    isRequestingBallot: false,
  })),
  on(Actions.requestChoiceCodes, (state) => ({
    ...state,
    loading: true,
    voteResponse: null,
  })),
  on(Actions.loadChoiceCodesSuccess, (state, {choiceCodes}) => ({
    ...state,
    voteResponse: choiceCodes,
    loading: false,
    sentButNotCast: true,
    isRequestingAdditionalData: false,
    ballotUserData: null,
    error: null,
  })),
  on(Actions.loadChoiceCodesFailure, (state, {error}) => ({
    ...state,
    error,
    loading: false,
    isRequestingAdditionalData: false,
  })),
  on(Actions.requestVoteCastCode, (state) => ({
    ...state,
    loading: true,
    confirmVoteResponse: null,
  })),
  on(Actions.loadVoteCastCodeSuccess, (state, {confirmVoteCode}) => ({
    ...state,
    confirmVoteResponse: confirmVoteCode,
    loading: false,
    isRequestingAdditionalData: false,
    cast: true,
    error: null,
  })),
  on(Actions.loadVoteCastCodeFailure, (state, {error}) => ({
    ...state,
    error,
    loading: false,
  })),
  on(Actions.saveForm, (state, {ballotUserData: userVote}) => ({
    ...state,
    ballotUserData: userVote,
  })),
  on(Actions.seal, (state) => ({
    ...state,
    voteResponse: null,
    loading: true,
    error: null,
  })),
  on(Actions.loadSealedVoteSuccess, (state, {voteResponse}) => ({
    ...state,
    voteResponse,
    loading: false,
    error: null,
  })),
  on(Actions.loadSealedVoteFailure, (state, {error}) => ({
    ...state,
    voteResponse: null,
    loading: false,
    error: error,
  })),
  on(Actions.confirmVote, (state, {confirmVote}) => ({
    ...state,
    confirmVote,
    loading: true,
    error: null,
  })),
  on(Actions.loadConfirmVoteSuccess, (state, {confirmVoteResponse}) => ({
    ...state,
    confirmVoteResponse,
    loading: false,
    error: null,
  })),
  on(Actions.loadConfirmVoteFailure, (state, {error}) => ({
    ...state,
    error,
    loading: false,
  })),
  on(Actions.logout, (state) => ({
    ...initialState,
    config: state.config,
    error: state.error,
  }))
);

export function reducer(state: SharedState | undefined, action: Action) {
  return stateReducer(state, action);
}
