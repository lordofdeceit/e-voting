/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {from, merge, of} from 'rxjs';
import {catchError, map, tap, filter, exhaustMap, delay} from 'rxjs/operators';
import {createEffect, Actions, ofType, concatLatestFrom} from '@ngrx/effects';

import * as SharedActions from './shared-state.actions';
import * as SharedReducers from './shared-state.reducer';
import * as SharedSelectors from './shared-state.selectors';
import * as SharedOperators from './shared-state.operators';

import {BackendService, environment} from '@swiss-post/backend';
import {BallotResponseStatus} from '@swiss-post/types';

@Injectable()
export class SharedStateEffects {
  clearBackendError$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SharedActions.agree),
      map(() => SharedActions.clearBackendError())
    )
  );

  setlanguage$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SharedActions.setLanguage),
      concatLatestFrom(() => [
        this.store.pipe(SharedOperators.getDefinedBallot),
      ]),
      exhaustMap(() => {
        return of(SharedActions.translateBallot());
      })
    )
  );

  translateBallot$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SharedActions.translateBallot),
      concatLatestFrom(() => [
        this.store.pipe(SharedOperators.getDefinedBallot),
        this.store.pipe(SharedOperators.getDefinedConfig),
      ]),
      exhaustMap(([_action, ballot, config]) => {
        return from(
          this.backendService.translateBallot(ballot, config.lang)
        ).pipe(
          map((translatedBallot) => {
            return SharedActions.translateBallotSuccess({
              ballot: translatedBallot,
            });
          }),
          catchError((error) => {
            return of(SharedActions.translateBallotFailure({error}));
          })
        );
      })
    )
  );

  authenticateVoter$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SharedActions.start),
      concatLatestFrom(() => this.store.select(SharedSelectors.getConfig)),
      exhaustMap(([action, config]) =>
        from(this.backendService.authenticate(action.voter, config)).pipe(
          map((startVotingKey) => {
            return SharedActions.loadSvkSuccess({
              startVotingKey,
            });
          }),
          catchError((error) => of(SharedActions.loadSvkFailure({error})))
        )
      )
    )
  );

  requestBallot$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SharedActions.loadSvkSuccess),
      exhaustMap((action) =>
        from(this.backendService.requestBallot(action.startVotingKey)).pipe(
          map((ballot) =>
            SharedActions.loadBallotSuccess({
              ballot,
              requestAdditionalData: [
                BallotResponseStatus.SENT_BUT_NOT_CAST,
                BallotResponseStatus.CAST,
              ].includes(ballot.ballot.status),
            })
          ),
          catchError((error) => of(SharedActions.loadBallotFailure({error})))
        )
      )
    )
  );

  requestAdditionalBallotData$ = createEffect(() => {
    const retrievedBallot$ = this.actions$.pipe(
      ofType(SharedActions.loadBallotSuccess),
      map((action) => action.ballot.ballot.status)
    );

    const requestChoiceCodes$ = retrievedBallot$.pipe(
      filter((status) => status === BallotResponseStatus.SENT_BUT_NOT_CAST),
      map(SharedActions.requestChoiceCodes)
    );

    const requestVoteCastCode$ = retrievedBallot$.pipe(
      filter((status) => status === BallotResponseStatus.CAST),
      map(SharedActions.requestVoteCastCode)
    );

    return merge(requestChoiceCodes$, requestVoteCastCode$);
  });

  requestChoiceCodes$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SharedActions.requestChoiceCodes),
      exhaustMap(() =>
        from(this.backendService.requestChoiceCodes()).pipe(
          map((choiceCodes) =>
            SharedActions.loadChoiceCodesSuccess({choiceCodes})
          ),
          catchError((error) =>
            of(SharedActions.loadChoiceCodesFailure({error}))
          )
        )
      )
    )
  );

  requestVoteCastCode$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SharedActions.requestVoteCastCode),
      exhaustMap(() =>
        from(this.backendService.requestVoteCastCode()).pipe(
          map((confirmVoteCode) =>
            SharedActions.loadVoteCastCodeSuccess({confirmVoteCode})
          ),
          catchError((error) =>
            of(SharedActions.loadVoteCastCodeFailure({error}))
          )
        )
      )
    )
  );

  sendVote$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SharedActions.seal),
      concatLatestFrom(() => [
        this.store.pipe(SharedOperators.getDefinedBallot),
        this.store.pipe(SharedOperators.getDefinedBallotUserData),
      ]),
      exhaustMap(([_action, ballotResponse, ballotUserData]) =>
        from(this.backendService.sendVote(ballotResponse, ballotUserData)).pipe(
          map((voteResponse) =>
            SharedActions.loadSealedVoteSuccess({voteResponse})
          ),
          catchError((error) =>
            of(SharedActions.loadSealedVoteFailure({error}))
          )
        )
      )
    )
  );

  sendVoteError$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SharedActions.loadSealedVoteFailure),
        concatLatestFrom(() =>
          this.store.select(SharedSelectors.getBackendError)
        ),
        filter(([_action, error]) => error?.authTokenExpired === true),
        tap(() => this.router.navigate(['/start-voting']))
      ),
    {dispatch: false}
  );

  confirmVote$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SharedActions.confirmVote),
      exhaustMap((action) =>
        from(this.backendService.confirmVote(action.confirmVote)).pipe(
          map((confirmVoteResponse) =>
            SharedActions.loadConfirmVoteSuccess({confirmVoteResponse})
          ),
          catchError((error) =>
            of(SharedActions.loadConfirmVoteFailure({error}))
          )
        )
      )
    )
  );

  confirmVoteError$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SharedActions.loadConfirmVoteFailure),
        concatLatestFrom(() =>
          this.store.select(SharedSelectors.getBackendError)
        ),
        filter(([_action, error]) => error?.authTokenExpired === true),
        tap(() => this.router.navigate(['/start-voting']))
      ),
    {dispatch: false}
  );

  navigateToChoose$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SharedActions.loadBallotSuccess),
        filter(
          (action) =>
            action.ballot.ballot.status === BallotResponseStatus.NOT_SENT
        ),
        delay(environment.progressOverlayNavigateDelay),
        tap(() => this.router.navigate(['/choose']))
      ),
    {dispatch: false}
  );

  navigateToReview$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SharedActions.review),
        tap(() => this.router.navigate(['/review']))
      ),
    {dispatch: false}
  );

  navigateToVerify$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          SharedActions.loadSealedVoteSuccess,
          SharedActions.loadChoiceCodesSuccess
        ),
        delay(environment.progressOverlayNavigateDelay),
        tap(() => this.router.navigate(['/verify']))
      ),
    {dispatch: false}
  );

  navigateToConfirm$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          SharedActions.loadConfirmVoteSuccess,
          SharedActions.loadVoteCastCodeSuccess
        ),
        delay(environment.progressOverlayNavigateDelay),
        tap(() => this.router.navigate(['/confirm']))
      ),
    {dispatch: false}
  );

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store<SharedReducers.SharedState>,
    private readonly backendService: BackendService,
    private readonly router: Router
  ) {
  }
}
