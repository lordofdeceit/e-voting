/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {createFeatureSelector, createSelector} from '@ngrx/store';
import {SHARED_FEATURE_KEY, SharedState} from './shared-state.reducer';

export const getSharedState =
  createFeatureSelector<SharedState>(SHARED_FEATURE_KEY);

export const getLoading = createSelector(
  getSharedState,
  (state: SharedState) => state.loading
);

export const getIsAuthenticated = createSelector(
  getSharedState,
  (state: SharedState) => state?.isAuthenticated
);

export const getBackendError = createSelector(
  getSharedState,
  (state: SharedState) => state.error
);

export const hasBackendError = createSelector(
  getSharedState,
  (state: SharedState) => !!state.error
);

export const getBallot = createSelector(
  getSharedState,
  (state: SharedState) => state.ballot
);

export const getContests = createSelector(
  getSharedState,
  (state: SharedState) => state.ballot?.ballot.contests
);

export const getContestsUserData = createSelector(
  getSharedState,
  (state: SharedState) => state.ballotUserData?.contests
);

export const getBallotUserData = createSelector(
  getSharedState,
  (state: SharedState) => state.ballotUserData
);

export const getContestsAndContestsUserData = createSelector(
  getContests,
  getContestsUserData,
  (contests, contestsUserData) => ({
    contests,
    contestsUserData: contestsUserData,
  })
);

export const getBallotResponseAndBallotUserData = createSelector(
  getBallot,
  getBallotUserData,
  (ballotResponse, ballotUserData) => ({
    ballotResponse,
    ballotUserData,
  })
);

export const getConfig = createSelector(
  getSharedState,
  (state: SharedState) => state?.config
);

export const getSendVoteResponse = createSelector(
  getSharedState,
  (state: SharedState) => state.voteResponse
);

export const getVerifyData = createSelector(
  getSendVoteResponse,
  getContests,
  getContestsUserData,
  (sendVoteResponse, contests, contestsUserData) => ({
    sendVoteResponse,
    contests,
    contestsUserData,
  })
);

export const getConfirmVoteResponse = createSelector(
  getSharedState,
  (state: SharedState) => state.confirmVoteResponse
);

export const getSentButNotCast = createSelector(
  getSharedState,
  (state: SharedState) => state.sentButNotCast
);

export const getVoteCastedInPreviousSession = createSelector(
  getSharedState,
  (state: SharedState) => state.cast
);

export const getIsBallotDataLoaded = createSelector(
  getSharedState,
  getBallot,
  (state: SharedState, ballotResponse) =>
    !state.isRequestingAdditionalData && !!ballotResponse
);

export const getElectionEventId = createSelector(
  getSharedState,
  (state: SharedState) => state.config.electionEventId
);

export const getWriteInAlphabet = createSelector(
  getBallot,
  (ballotResponse) => ballotResponse?.ballot.writeInAlphabet
);
