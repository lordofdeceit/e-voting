/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {select} from '@ngrx/store';
import {pipe} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import * as SharedSelectors from './shared-state.selectors';

function isDefined<T>(item: T | null | undefined): item is T {
  return !!item;
}

export const getDefinedBallot = pipe(
  select(SharedSelectors.getBallot),
  filter(isDefined)
);

export const getDefinedBallotUserData = pipe(
  select(SharedSelectors.getBallotUserData),
  filter(isDefined)
);

export const getDefinedConfig = pipe(
  select(SharedSelectors.getConfig),
  filter(isDefined)
);

export const getHasVoteResponse = pipe(
  select(SharedSelectors.getSendVoteResponse),
  map((vote) => isDefined(vote))
);

export const getHasConfirmVoteResponse = pipe(
  select(SharedSelectors.getConfirmVoteResponse),
  map((confirmVoteResponse) => isDefined(confirmVoteResponse))
);

export const getDefinedWriteInAlphabet = pipe(
  select(SharedSelectors.getWriteInAlphabet),
  filter(isDefined)
);
