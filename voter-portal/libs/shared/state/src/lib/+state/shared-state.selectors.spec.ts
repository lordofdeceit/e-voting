/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {BallotResponseStatus, IContest} from '@swiss-post/types';
import {SharedState, initialState} from './shared-state.reducer';
import * as SharedStateSelectors from './shared-state.selectors';

describe('SharedState Selectors', () => {
  let state: SharedState;

  beforeEach(() => {
    state = {
      ...initialState,
      error: null,
      ballot: {
        ballot: {
          id: '100',
          correctnessIds: {},
          status: BallotResponseStatus.NOT_SENT,
          contests: [
            {
              id: '0',
              defaultTitle: 'Bund',
              questions: [],
              template: 'options',
            },
            {
              id: '1',
              defaultTitle: 'Kanton St. Gallen',
              questions: [],
              template: 'options',
            },
            {
              id: '2',
              defaultTitle: 'Standeratswahl',
              questions: [],
              template: 'options',
            },
          ],
        },
      },
    };
  });

  describe('SharedState Selectors', () => {
    it('getContests() should return the list of Contests', () => {
      const contests: IContest[] = <IContest[]>(
        SharedStateSelectors.getContests.projector(state)
      );

      expect(contests.length).toBe(3);
    });

    it('getLoading() should return the current "loading" status', () => {
      const loading: boolean =
        SharedStateSelectors.getLoading.projector(state);

      expect(loading).toBe(false);
    });
  });
});
