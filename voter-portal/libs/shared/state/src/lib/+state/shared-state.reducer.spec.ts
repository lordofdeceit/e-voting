/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {Action} from '@ngrx/store';

import * as StartVotingActions from './shared-state.actions';
import {SharedState, initialState, reducer} from './shared-state.reducer';

describe('SharedState Reducer', () => {

  describe('valid SharedState actions', () => {
    it('loadSvkSuccess should return authentication token', () => {

      const mockStartVotingKey = 'mockStartVotingKey';
      const action = StartVotingActions.loadSvkSuccess({startVotingKey: mockStartVotingKey});

      const result: SharedState = reducer(initialState, action);

      expect(result.startVotingKey?.length).toBeGreaterThan(0);
    });
  });

  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as Action;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
