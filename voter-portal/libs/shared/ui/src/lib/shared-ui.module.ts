/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgbAccordionModule, NgbModalModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { SharedConfigurationModule } from '@swiss-post/shared/configuration';
import { SharedIconsModule } from '@swiss-post/shared/icons';
import { AccordionAccessibilityDirective } from './accordion/accordion-accessibility.directive';
import { ContestAccordionComponent } from './accordion/contest-accordion/contest-accordion.component';
import { BackendErrorComponent } from './backend-error/backend-error.component';
import { ClearableInputComponent } from './clearable-input/clearable-input.component';
import { ConfirmationModalComponent } from './confirmation/confirmation-modal/confirmation-modal.component';
import { FAQModalComponent } from './faq/faq-modal/faq-modal.component';
import { MarkdownPipe } from './markdown/markdown.pipe';
import { ParagraphsDirective } from './paragraphs/paragraphs.directive';
import { ProgressModalComponent } from './progress/progress-modal/progress-modal.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    NgbModalModule,
    NgbAccordionModule,
    NgbTooltipModule,
    SharedIconsModule,
    SharedConfigurationModule,
  ],
  declarations: [
    FAQModalComponent,
    BackendErrorComponent,
    ProgressModalComponent,
    MarkdownPipe,
    ContestAccordionComponent,
    ClearableInputComponent,
    ConfirmationModalComponent,
    ParagraphsDirective,
    AccordionAccessibilityDirective,
  ],
  exports: [
    FAQModalComponent,
    BackendErrorComponent,
    ProgressModalComponent,
    MarkdownPipe,
    ContestAccordionComponent,
    ClearableInputComponent,
    ConfirmationModalComponent,
    ParagraphsDirective,
    AccordionAccessibilityDirective,
  ],
})
export class SharedUiModule {
}
