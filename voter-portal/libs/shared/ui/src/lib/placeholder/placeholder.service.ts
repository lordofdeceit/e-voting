/*
 * Copyright 2022 by Swiss Post, Information Technology
 */
import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ConfigurationService} from '@swiss-post/shared/configuration';
import {
  ITranslatePlaceholderObject,
  ITranslatePlaceholders,
} from '@swiss-post/types';

@Injectable({
  providedIn: 'root',
})
export class PlaceholderService {
  private readonly _placeholders?: ITranslatePlaceholders;

  get currentValue(): ITranslatePlaceholderObject | null {
    return this._placeholders
      ? this._placeholders[this.translate.currentLang as keyof ITranslatePlaceholders]
      : null;
  }

  constructor(
    private readonly configurationService: ConfigurationService,
    private readonly translate: TranslateService
  ) {
    this._placeholders = this.configurationService.configuration?.translatePlaceholders;
  }
}
