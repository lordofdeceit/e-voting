/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {TestBed} from '@angular/core/testing';
import {TranslateService} from '@ngx-translate/core';
import {ConfigurationService} from '@swiss-post/shared/configuration';
import {Identification} from '@swiss-post/types';
import {MockProvider} from 'ng-mocks';
import {TranslateTestingModule} from 'ngx-translate-testing';

import {PlaceholderService} from './placeholder.service';

describe('PlacehoderService', () => {
  let service: PlaceholderService;
  let translateService: TranslateService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule.withTranslations({}).withDefaultLanguage('fr'),
      ],
      providers: [
        MockProvider(ConfigurationService, {
          configuration: {
            identification: Identification.YEAR_OF_BIRTH,
            contestsCapabilities: {
              writeIns: true,
            },
            platformRootCA: '',
            translatePlaceholders: {
              en: {
                support: 'en@support',
              },
              fr: {
                support: 'fr@support',
              },
              it: {
                support: 'it@support',
              },
              de: {
                support: 'de@support',
              },
              rm: {
                support: 'rm@support',
              },
            },
          },
        }),
      ],
    });
    service = TestBed.inject(PlaceholderService);
    translateService = TestBed.inject(TranslateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get placeholders', () => {
    translateService.use('fr');
    const placeholders = service.currentValue;
    expect(placeholders).toBeTruthy();
    if (placeholders) {
      expect(placeholders['support']).toBe('fr@support');
    }
  });
});
