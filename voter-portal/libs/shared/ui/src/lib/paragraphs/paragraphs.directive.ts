/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Directive({
  selector: '[swpParagraphs]'
})
export class ParagraphsDirective {
  constructor(
      private readonly templateRef: TemplateRef<{ paragraphKey: string }>,
      private readonly viewContainer: ViewContainerRef,
      private readonly translateService: TranslateService,
  ) {}

  @Input() set swpParagraphs(translationKey: string | undefined) {
    if (!translationKey) {
      return;
    }

    if (this.translationExists(translationKey)) {
      this.viewContainer.createEmbeddedView(this.templateRef, { paragraphKey: translationKey });
    } else {
      let paragraphKey: string;
      let position = 1;
      while ((paragraphKey = `${translationKey}.p${position}`) && this.translationExists(paragraphKey)) {
        this.viewContainer.createEmbeddedView(this.templateRef, { paragraphKey });
        position++;
      }
    }
  }

  private translationExists(key: string): boolean {
    const translation = this.translateService.instant(key);
    return translation && translation !== key;
  }
}
