/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { environment } from '@swiss-post/backend';
import { IProgressData, IProgressStep } from '@swiss-post/types';
import { combineLatest, interval, merge, Observable, timer } from 'rxjs';
import { delay, filter, map, startWith, take, takeUntil, withLatestFrom } from 'rxjs/operators';

@Component({
  selector: 'swp-progress',
  templateUrl: './progress-modal.component.html',
  styleUrls: ['./progress-modal.component.scss'],
})
export class ProgressModalComponent implements IProgressData, OnInit {
  @Input() titleLabel!: string;
  @Input() titleSlowLabel!: string;
  @Input() waitingTimeBeforeTitleChanges!: number;
  @Input() steps!: IProgressStep[];
  @Input() error!: Observable<boolean>;
  estimatedProgress = 0;
  progressIsSlow = false;

  constructor(
    public readonly activeModal: NgbActiveModal,
  ) {}

  ngOnInit() {
    const allStepCompleted$ = combineLatest(
      this.steps.map((step) => step.isDone)
    ).pipe(filter((steps) => steps.every((isDone) => isDone)));

    const errorOccurred$ = this.error.pipe(filter((isError) => isError));

    const progressStopped$ = merge(allStepCompleted$, errorOccurred$).pipe(take(1));

    progressStopped$
      .pipe(delay(environment.progressOverlayCloseDelay)) // wait before closing the modal in order to let the user read
      .subscribe(() => {
        this.activeModal.dismiss('Closing progress dialog');
      });

    timer(this.waitingTimeBeforeTitleChanges)
      .pipe(takeUntil(progressStopped$))
      .subscribe(() => {
        this.progressIsSlow = true;
      });

    const progressSection = 100 / this.steps.length;
    const confirmedProgress$ = merge(...this.steps.map((step, i) => step.isDone.pipe(
      filter(isDone => isDone),
      map(() => i + 1),
      startWith(0),
      map(completedStepCount => completedStepCount * progressSection)
    )));

    confirmedProgress$.pipe(
      takeUntil(progressStopped$)
    ).subscribe(confirmedProgress => {
      this.estimatedProgress = confirmedProgress;
    });

    interval(500).pipe(
      withLatestFrom(confirmedProgress$),
      filter(([_time, confirmedProgress]) => this.estimatedProgress < confirmedProgress + progressSection),
      takeUntil(progressStopped$)
    ).subscribe(() => {
      this.estimatedProgress++;
    });
  }
}
