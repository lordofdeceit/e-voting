/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {MockComponent, MockProvider} from 'ng-mocks';
import {of, Subject} from 'rxjs';
import {By} from '@angular/platform-browser';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {IconComponent} from '@swiss-post/shared/icons';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';

import {ProgressModalComponent} from './progress-modal.component';


describe('ProgressComponent', () => {
  let component: ProgressModalComponent;
  let fixture: ComponentFixture<ProgressModalComponent>;
  let dismiss: jest.Mock<() => void>;
  let error: Subject<boolean>;

  beforeEach(async () => {
    dismiss = jest.fn();

    await TestBed.configureTestingModule({
      declarations: [
        ProgressModalComponent,
        MockComponent(IconComponent)
      ],
      imports: [
        TranslateTestingModule.withTranslations({}),
      ],
      providers: [
        MockProvider(NgbModal),
        MockProvider(NgbActiveModal, {dismiss})
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressModalComponent);
    component = fixture.componentInstance;
    error = new Subject();

    Object.assign(component, {
      titleLabel: 'startvoting.progress.title',
      titleSlowLabel: 'startvoting.progress.titleslow',
      waitingTimeBeforeTitleChanges: 5000,
      steps: [
        {
          isDone: of(true),
          inProgressLabel: 'startvoting.progress.step.authentication.inprogress',
          doneLabel: 'startvoting.progress.step.authentication.done',
        },
        {
          isDone: of(false),
          inProgressLabel: 'startvoting.progress.step.retrieveballot.inprogress',
          doneLabel: 'startvoting.progress.step.retrieveballot.done',
        },
      ],
      error
    });

    fixture.detectChanges();
  });

  it('should show progress', () => {
    fixture.detectChanges();
    const progressSteps = fixture.debugElement.queryAll(By.css('li'));
    expect(progressSteps.length).toBe(2);
  });

  it('should close dialog on error', fakeAsync(() => {
    error.next(true);
    tick(1000);
    expect(dismiss.mock.calls.length).toBe(1);
  }));
});
