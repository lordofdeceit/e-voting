/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {Injectable} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {IProgressData} from '@swiss-post/types';
import {ProgressModalComponent} from './progress-modal/progress-modal.component';

@Injectable({
  providedIn: 'root',
})
export class ProgressService {
  constructor(private readonly modalService: NgbModal) {
  }

  open(config: IProgressData) {
    const modalRef = this.modalService.open(ProgressModalComponent, {
      size: 'xl',
      backdrop: 'static',
      keyboard: false,
    });

    Object.assign(modalRef.componentInstance, config);
  }
}
