/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {ComponentFixture, TestBed} from '@angular/core/testing';
import { MockDirective } from 'ng-mocks';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import '@angular/localize/init';
import { ParagraphsDirective } from '../paragraphs/paragraphs.directive';

import {BackendErrorComponent} from './backend-error.component';

describe('BackendErrorComponent', () => {
    let component: BackendErrorComponent;
    let fixture: ComponentFixture<BackendErrorComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                TranslateTestingModule.withTranslations({}).withDefaultLanguage('fr'),
                HttpClientTestingModule,
            ],
            declarations: [
                BackendErrorComponent,
                MockDirective(ParagraphsDirective)
            ],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(BackendErrorComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
