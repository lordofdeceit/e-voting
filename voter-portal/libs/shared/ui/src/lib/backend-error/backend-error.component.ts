/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import { Component, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ConfigurationService } from '@swiss-post/shared/configuration';
import { PlaceholderService } from '@swiss-post/shared/ui';
import { BackendError, ErrorMessage } from '@swiss-post/types';

@Component({
  selector: 'swp-backenderror',
  templateUrl: './backend-error.component.html',
  styleUrls: ['./backend-error.component.scss'],
})
export class BackendErrorComponent {
  errorMessageKey: string | undefined;
  errorMessageArgs: Record<string, unknown>;

  @Input() set error(value: BackendError | null | undefined) {
    if (value) {
      let key: string;
      switch (value?.validationErrorMsg) {
        case ErrorMessage.ELECTION_OVER_DATE:
        case ErrorMessage.AUTH_TOKEN_EXPIRED:
        case ErrorMessage.ELECTION_NOT_STARTED:
        case ErrorMessage.ERROR_NORETRIES:
        case ErrorMessage.INACTIVE_KEY:
        case ErrorMessage.CONNECTION_ERROR:
        case ErrorMessage.WRONG_BALLOT_CASTING_KEY_FORMAT:
        case ErrorMessage.WRONG_BALLOT_CASTING_KEY:
        case ErrorMessage.BCK_ATTEMPTS_EXCEEDED:
        case ErrorMessage.VALIDATION_ERROR:
          key = value.validationErrorMsg;
          break;
        case ErrorMessage.ERROR_RETRY:
          key = `ERROR_RETRY.${this.configurationService.configuration?.identification}`;
          break;
        default:
          key = 'default';
      }

      this.errorMessageKey = `backenderror.${key}`;
      this.errorMessageArgs['retries'] = value.numberOfRemainingAttempts;
    }
  }

  constructor(
      public readonly translate: TranslateService,
      public readonly placeholders: PlaceholderService,
      private readonly configurationService: ConfigurationService,
  ) {
    this.errorMessageArgs = this.placeholders.currentValue ?? {};
  }
}
