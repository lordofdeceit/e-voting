/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {Injectable} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FAQSection} from '@swiss-post/types';
import {FAQModalComponent} from './faq-modal/faq-modal.component';

@Injectable({
  providedIn: 'root',
})
export class FAQService {
  constructor(
    private readonly modalService: NgbModal
  ) {
  }

  public showFAQ(section?: FAQSection) {
    const modalOption = {fullscreen: 'xl', size: 'xl'};
    const modalRef = this.modalService.open(FAQModalComponent, modalOption);
    modalRef.componentInstance.activeFAQSection = section;
  }
}
