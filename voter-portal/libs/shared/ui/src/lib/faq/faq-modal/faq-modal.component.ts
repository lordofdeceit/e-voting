/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {AfterViewInit, Component, Input} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ConfigurationService} from '@swiss-post/shared/configuration';
import {FAQSection, Identification} from '@swiss-post/types';
import {PlaceholderService} from '../../placeholder/placeholder.service';

@Component({
  selector: 'swp-faq',
  templateUrl: './faq-modal.component.html',
  styleUrls: ['./faq-modal.component.scss'],
})
export class FAQModalComponent implements AfterViewInit {
  @Input() activeFAQSection: FAQSection | undefined;
  identificationMethod: string | null;

  readonly FAQSection = FAQSection;
  readonly Identification = Identification;
  readonly allowedCharacters = "ÀÁÂÃÄÅ àáâãäå ÈÉÊË èéêë ÌÍÎÏ ìíîï ÒÓÔÕÖ òóôõö ÙÚÛÜ ùúûü Ææ Çç Œœ Þþ Ññ Øø Šš Ýý Ÿÿ Žž ¢ () ð Ð ß ' , - . /";

  public constructor(
    public readonly activeModal: NgbActiveModal,
    public readonly placeholders: PlaceholderService,
    private readonly configurationService: ConfigurationService
  ) {
    this.identificationMethod = this.configurationService.configuration?.identification || null;
  }

  ngAfterViewInit() {
    if (this.activeFAQSection) {
      const activeSectionButton = document.querySelector<HTMLButtonElement>(`button[aria-controls=${this.activeFAQSection}]`);
      activeSectionButton?.focus();
    }
  }
}
