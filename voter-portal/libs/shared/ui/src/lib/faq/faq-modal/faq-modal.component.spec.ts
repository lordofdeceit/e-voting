/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {NgbAccordionModule, NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ConfigurationService} from '@swiss-post/shared/configuration';
import {RandomKey} from '@swiss-post/shared/testing';
import {FAQSection, Identification, IVoterPortalConfig} from '@swiss-post/types';
import { MockDirective, MockProvider } from 'ng-mocks';
import {TranslateTestingModule} from 'ngx-translate-testing';
import { ParagraphsDirective } from '../../paragraphs/paragraphs.directive';
import {PlaceholderService} from '../../placeholder/placeholder.service';

import {FAQModalComponent} from './faq-modal.component';

describe('FAQModalComponent', () => {
  let component: FAQModalComponent;
  let fixture: ComponentFixture<FAQModalComponent>;
  let dismiss: jest.Mock<() => void>;
  let voterPortalConfig: IVoterPortalConfig;

  beforeEach(async () => {
    voterPortalConfig = {
      identification: Identification.YEAR_OF_BIRTH,
      contestsCapabilities: {
        writeIns: true,
      },
      platformRootCA: ''
    };

    await TestBed.configureTestingModule({
      declarations: [
          FAQModalComponent,
          MockDirective(ParagraphsDirective)
      ],
      imports: [
        TranslateTestingModule.withTranslations({}),
        NgbAccordionModule,
      ],
      providers: [
        MockProvider(NgbModal),
        MockProvider(NgbActiveModal, {dismiss}),
        MockProvider(ConfigurationService, {
          configuration: voterPortalConfig
        }),
        MockProvider(PlaceholderService, {
          currentValue: {}
        }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FAQModalComponent);
    component = fixture.componentInstance;

    component.activeFAQSection = FAQSection.HowToStart;

    fixture.detectChanges();
  });

  it('should show all the FAQ', () => {
    const faqTitles = fixture.debugElement.queryAll(By.css('.accordion-item'));
    expect(faqTitles.length).toBe(12);
  });

  it('should properly adapt to the portal configuration', () => {
    const howtoStartSection: HTMLElement = fixture.debugElement.query(
      By.css(`#${FAQSection.HowToStart}-content`)
    ).nativeElement;

    expect(howtoStartSection.textContent).toContain('faq.howtostart.content.' + voterPortalConfig.identification);
  });

  describe('without an active section', () => {
    beforeEach(() => {
      component.activeFAQSection = undefined;
      fixture.detectChanges();
    });

    it('should not show any accordion content', () => {
      const accordionItems = fixture.debugElement.queryAll(By.css('.accordion-item'));
      accordionItems.forEach(accordionItem => {
        const accordionBody = accordionItem.query(By.css('.accordion-body'));

        expect(accordionBody).toBeFalsy();
      });
    });
  });

  describe('with an active section', () => {
    let activeSection: FAQSection;

    beforeEach(() => {
      activeSection = component.activeFAQSection = FAQSection[RandomKey(FAQSection)];
      fixture.detectChanges();
    });

    it('should show only accordion content matching the active section provided', () => {
      const accordionContents = fixture.debugElement.queryAll(By.css('.accordion-item > .collapse'));
      expect(accordionContents.length).toBe(1);
      expect(accordionContents[0].nativeElement.id).toBe(activeSection);
    });

    it('should focus on the active section\'s accordion header button', () => {
      const activeSectionButton: HTMLElement = fixture.debugElement.query(
        By.css(`#${activeSection}-header > button`)
      ).nativeElement;
      jest.spyOn(activeSectionButton, 'focus');

      component.ngAfterViewInit();

      expect(activeSectionButton.focus).toHaveBeenCalled();
    });
  });

});
