/*
 * Copyright 2022 by Swiss Post, Information Technology
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {QuestionsTestingComponent} from './components/questions-testing.component';

@NgModule({
  declarations: [
    QuestionsTestingComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    QuestionsTestingComponent
  ]
})
export class SharedTestingModule {
}
