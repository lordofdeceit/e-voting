/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {BallotResponseStatus, IBallot, IContest, IContestUserData, IOption, IQuestion, IQuestionUserData, TemplateType} from '@swiss-post/types';
import {RandomArray, RandomBetween, RandomItem} from './random';

let contestIndex = 0;
let questionIndex = 0;
let optionIndex = 0;
let ballotIndex = 0;

const MockOption = (isBlank = false): IOption => {
  optionIndex++;

  return {
    attrIndex: 0,
    attribute: '',
    id: `option-${optionIndex}`,
    chosen: false,
    details: {},
    ordinal: 0,
    isBlank,
    representation: `option-${optionIndex}-representation`,
    prime: `option-${optionIndex}-prime`,
    text: `option-${optionIndex}-text`,
  };
};

const MockQuestion = (text: string): IQuestion => {
  questionIndex++;

  return {
    id: `question-${questionIndex}`,
    attrIndex: 0,
    attribute: '',
    details: {},
    blankOption: MockOption(true),
    options: RandomArray(() => MockOption(), 5, 2),
    optionsMaxChoices: 1,
    optionsMinChoices: 0,
    ordinal: 0,
    text,
  };
};

const MockQuestionUserData = (
  question: IQuestion,
  blankAnswerProbability = 1 / (question.options.length + 1)
): IQuestionUserData => {
  const optionIds: string[] = question.options.map(option => option.id);
  return {
    id: question.id,
    chosenOption: RandomBetween(RandomItem(optionIds), '', blankAnswerProbability),
  };
};

export const MockContest = (questions: IQuestion[]): IContest => {
  contestIndex++;

  return {
    id: `contest-${contestIndex}`,
    template: TemplateType.OPTIONS,
    questions,
    title: `contest-${contestIndex}-title`,
  }
};

export const MockBallot = (contests: IContest[]): IBallot => {
  ballotIndex++;

  return {
    id: `ballot-${ballotIndex}`,
    contests: contests,
    correctnessIds: {},
    status: BallotResponseStatus.NOT_SENT,
    writeInAlphabet: '# \'(),-./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ¢ŠšŽžŒœŸÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿ',
  };
};

export const MockQuestions = (questionTexts: string[]): IQuestion[] => {
  return questionTexts.map(text => MockQuestion(text))
};

export const MockContestUserData = (
  questions: IQuestion[],
  options: { noBlankAnswers?: boolean, blankAnswerOnly?: boolean } = {}
): IContestUserData => {
  if (questions.length && options.noBlankAnswers && options.blankAnswerOnly) {
    throw new Error('It is not possible to have only blank answers while having no blank answers!');
  }

  let blankAnswerCount = 0;
  let blankAnswerProbability: number;

  const questionsUserData = questions.map((question, index) => {
    const lastItem = (index === questions.length - 1);

    if (options.noBlankAnswers || lastItem && options.blankAnswerOnly === false && blankAnswerCount === questions.length - 1) {
      blankAnswerProbability = 0;
    }

    if (options.blankAnswerOnly || lastItem && options.noBlankAnswers === false && !blankAnswerCount) {
      blankAnswerProbability = 1;
    }

    const questionUserData = MockQuestionUserData(question, blankAnswerProbability);

    if (!questionUserData.chosenOption) {
      blankAnswerCount++;
    }

    return questionUserData;
  });

  return {questions: questionsUserData};
};
