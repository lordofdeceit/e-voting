/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {RandomString} from "./random";

export const RandomElectionEventId = () => {
  return RandomString(32, '0123456789abcdefABCDEF');
};
