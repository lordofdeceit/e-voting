/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {IBallot} from '@swiss-post/types';

let choiceCodeIndex = 0;

const MockChoiceCode = (): string => {
  choiceCodeIndex++;
  return `choicecode-${choiceCodeIndex}`;
};

export const MockChoiceCodes = (ballot: IBallot): string[] => {
  return ballot.contests.reduce((allChoiceCodes, contest) => {
    let choiceCodes: string[] = [];

    if (contest.questions) {
      const questionChoiceCodes = contest.questions.map(MockChoiceCode);
      choiceCodes = [...choiceCodes, ...questionChoiceCodes];
    }

    if (contest.listQuestion?.maxChoices) {
      const listChoiceCodes = Array.from({length: contest.listQuestion?.maxChoices}, MockChoiceCode);
      choiceCodes = [...choiceCodes, ...listChoiceCodes];
    }

    if (contest.candidatesQuestion?.maxChoices) {
      const candidateChoiceCodes = Array.from({length: contest.candidatesQuestion?.maxChoices}, MockChoiceCode);
      choiceCodes = [...choiceCodes, ...candidateChoiceCodes];
    }

    return [...allChoiceCodes, ...choiceCodes];
  }, [] as string[]);
};
