/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {RandomString} from './random';

export const RandomInitializationCode = () => {
  return Array.from({length: 6}, () => RandomString(4)).join(' ');
};
