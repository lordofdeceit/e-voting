/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {IBallot} from './ballot-response';

export interface IOvApi {
  requestChoiceCodes(): Promise<string[]>;

  requestVoteCastCode(): Promise<{ voteCastCode: string }>;

  confirmVote(ballotCastingKey: string): Promise<{ voteCastCode: string }>;

  init(): Promise<void>;

  translateBallot(ballot: IBallot, lang: string): void;

  parseBallot(obj: object): IBallot;

  requestBallot(startVotingKey: string): Promise<object>;

  authenticate(
    svk: string,
    dobOrYob: string,
    tenantId: string,
    electionEventId: string
  ): Promise<string>;

  sendVote(
    primes: string[],
    writeIns: string[],
    correctnessIds: { [key: string]: string[] }
  ): Promise<string[]>;

  terminate(): void;
}
