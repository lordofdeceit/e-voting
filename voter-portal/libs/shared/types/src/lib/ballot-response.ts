/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
export interface IBallotResponse {
  ballot: IBallot;
}

export enum BallotResponseStatus {
  NOT_SENT = 'NOT_SENT',
  SENT_BUT_NOT_CAST = 'SENT_BUT_NOT_CAST',
  CAST = 'CAST',
  BLOCKED = 'BLOCKED',
  WRONG_BALLOT_CASTING_KEY = 'WRONG_BALLOT_CASTING_KEY',
}

export interface IBallot {
  id: string;
  contests: IContest[];
  correctnessIds: { [key: string]: string[] };
  description?: string;
  status: BallotResponseStatus;
  title?: string;
  writeInAlphabet?: string;
}

export interface IContest {
  id: string;
  defaultTitle?: string;
  description?: string;
  template: string;
  title?: string;
  howToVote?: string;
  questions?: IQuestion[];
  candidatesQuestion?: ICandidatesQuestion;
  listQuestion?: IListQuestion;
  allowFullBlank?: boolean;
  lists?: ICandidateList[];
}

export interface IListQuestion {
  id: string;
  minChoices: number;
  maxChoices: number;
  cumul: number;
}

export interface ICandidateList {
  id: string;
  blankId?: string;
  isBlank: boolean;
  isWriteIn: boolean;
  description?: string[];
  candidates: ICandidate[];
  details?: ICandidateListDetails;
  text?: string;
  ordinal?: number;
  prime?: string;
}

export interface ICandidateListDetails {
  text?: string;
  listType_apparentment: string;
  listType_sousApparentment: string;
  listType_attribute1: string;
  listType_attribute2: string;
  listType_attribute3: string;
}

export interface ICandidate {
  id: string;
  alias?: string;
  allIds: string[] | null;
  allRepresentations: string[] | null;
  details: ICandidateDetail;
  isBlank: boolean;
  isWriteIn: boolean;
  ordinal?: number;
  prime?: string;
}

export interface ICandidateDetail {
  candidateType_initialAccumulation?: string;
  candidateType_attribute1?: string;
  candidateType_attribute2?: string;
  candidateType_attribute3?: string;
  candidateType_positionOnList?: string;
  text?: string;
}

export interface ICandidatesQuestion {
  id: string;
  minChoices: number;
  maxChoices: number;
  hasWriteIns: boolean;
  cumul: number;
  fusions: string[][];
}

export interface IQuestion {
  id: string;
  attrIndex: number;
  attribute: string;
  details: IQuestionDetail;
  blankOption: IOption;
  options: IOption[];
  optionsMaxChoices: number;
  optionsMinChoices: number;
  ordinal: number;
  text?: string;
  title?: string;
  chosen?: string;
}

export interface IQuestionDetail {
  questionType_text?: string;
}

export interface IOption {
  attrIndex: number;
  attribute: string;
  id: string;
  chosen: false;
  description?: string;
  details: IOptionDetail;
  isBlank: boolean;
  ordinal: number;
  prime?: string;
  representation?: string;
  text?: string;
  title?: string;
}

export interface IOptionDetail {
  answerType_text?: string;
  text?: string;
}
