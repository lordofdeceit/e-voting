/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
export enum TemplateType {
  LISTS_AND_CANDIDATES = 'listsAndCandidates',
  OPTIONS = 'options',
}
