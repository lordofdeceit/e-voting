/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {ErrorMessage} from './error-message';

type OvApiNumberOfAttemptsError = {
  numberOfRemainingAttempts: number;
};

type OvApiShortValidationError = {
  validationErrorMsg: string;
}

type OvApiValidationError = {
  validationError: { validationErrorType: string; errorArgs?: string[] };
  httpStatus: number;
};

type OvHttpStatusError = {
  httpStatus: number;
};

export class BackendError extends Error {
  validationErrorMsg: string = ErrorMessage.ERROR;
  numberOfRemainingAttempts?: number;
  badKey?: boolean;
  authTokenExpired?: boolean;

  constructor(error?: unknown) {
    super(`Backend Error: ${error}`);

    if (typeof error === 'number') {
      this.parseNumberError(error);
    }

    if (error && typeof error === 'object') {
      if ('numberOfRemainingAttempts' in error) {
        this.numberOfRemainingAttempts = (error as OvApiNumberOfAttemptsError).numberOfRemainingAttempts;
        this.validationErrorMsg = ErrorMessage.ERROR_RETRY;
      }

      if ('validationErrorMsg' in error) {
        this.validationErrorMsg = (error as OvApiShortValidationError).validationErrorMsg;
      }

      if ('validationError' in error) {
        this.parseValidationError(error as OvApiValidationError);
      } else if ('httpStatus' in error && [401, 412, 422].includes((error as OvHttpStatusError).httpStatus)) {
        this.validationErrorMsg = ErrorMessage.AUTH_TOKEN_EXPIRED;
        this.authTokenExpired = true;
      }
    }
  }

  private parseNumberError(error: number): void {
    switch (error) {
      case 0:
        this.validationErrorMsg = ErrorMessage.CONNECTION_ERROR;
        break;
      case 401:
        this.validationErrorMsg = ErrorMessage.ERROR_RETRY;
        break;
      case 403:
        this.validationErrorMsg = ErrorMessage.ERROR_NORETRIES;
        break;
      case 404:
        this.validationErrorMsg = ErrorMessage.BAD_KEY;
        this.badKey = true;
    }
  }

  private parseValidationError(error: OvApiValidationError): void {
    if (error.validationError.errorArgs && error.validationError.errorArgs[0] === ErrorMessage.BLOCKED) {
      this.validationErrorMsg = ErrorMessage.BLOCKED;
    } else {
      this.validationErrorMsg = error.validationError.validationErrorType;

      switch (this.validationErrorMsg) {
        case ErrorMessage.AUTH_TOKEN_EXPIRED:
          this.authTokenExpired = true;
          break;
        case ErrorMessage.WRONG_BALLOT_CASTING_KEY_FORMAT:
        case ErrorMessage.BCK_ATTEMPTS_EXCEEDED:
          this.badKey = true;
          break;
        case ErrorMessage.WRONG_BALLOT_CASTING_KEY:
          this.badKey = true;
          if (error.validationError.errorArgs?.length) {
            this.numberOfRemainingAttempts = parseInt(error.validationError?.errorArgs[0]);
          }
      }
    }
  }
}
