/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
export enum Identification {
  YEAR_OF_BIRTH = 'yob',
  DATE_OF_BIRTH = 'dob',
}
