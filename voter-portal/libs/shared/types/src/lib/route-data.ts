/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
export interface RouteData {
  stepKey: string;
  allowedBackPaths?: string[];
  backAction?: BackAction;
}

export enum BackAction {
  ShowCancelVoteDialog = 'ShowCancelDialog',
  ShowLeaveProcessDialog = 'ShowLeaveDialog',
  GoToStartVotingPage = 'GoToStartVotingPage',
  GoToLegalTermsPage = 'GoToLegalTermsPage',
}
