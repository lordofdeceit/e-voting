/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
export enum FAQSection {
  HowToStart = 'how-to-start',
  WhatIsVotingCard = 'what-is-voting-card',
  WhatIsInitializationCode = 'what-is-initialization-code',
  HowToMakeAChoice = 'how-to-make-a-choice',
  CanIChangeSelection = 'can-i-change-selection',
  WhatAreChoiceCodes = 'what-are-choice-codes',
  WhatIsBallotCastingKey = 'what-is-ballot-casting-key',
  WhatIsVoteCastReturnCode = 'what-is-vote-cast-return-code',
  WhatIfCodesDoNotMatch = 'what-if-codes-do-not-match',
  WhatIsDifferenceSealAndCast = 'what-is-difference-seal-and-cast',
  CanILeaveAnytime = 'can-i-leave-anytime',
  HowToUseWriteIns = 'how-to-use-write-ins',
}
