/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import { BackendError } from './backend-error';
import { ErrorMessage } from './error-message';

describe('BackendError', () => {
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  jest.spyOn(console, 'error').mockImplementation(() => {});

  describe('number-errors', () => {
    it('should create correct mesage for 0', () => {
      const error = new BackendError(0);
      expect(error.validationErrorMsg).toBe(ErrorMessage.CONNECTION_ERROR);
    });
    it('should create correct mesage for 401', () => {
      const error = new BackendError(401);
      expect(error.validationErrorMsg).toBe(ErrorMessage.ERROR_RETRY);
    });
    it('should create correct mesage for 403', () => {
      const error = new BackendError(403);
      expect(error.validationErrorMsg).toBe(ErrorMessage.ERROR_NORETRIES);
    });
    it('should create correct mesage for 404', () => {
      const error = new BackendError(404);
      expect(error.validationErrorMsg).toBe(ErrorMessage.BAD_KEY);
    });
  });

  describe('OvApiValidationError', () => {
    it('should create corret message for OvApiValidationError, BLOCKED', () => {
      const error = new BackendError({
        validationError: { errorArgs: ['BLOCKED'] },
      });
      expect(error.validationErrorMsg).toBe(ErrorMessage.BLOCKED);
    });

    it('should create corret message for OvApiValidationError, AUTH_TOKEN_EXPIRED', () => {
      const error = new BackendError({
        validationError: { validationErrorType: 'AUTH_TOKEN_EXPIRED' },
      });
      expect(error.validationErrorMsg).toBe(ErrorMessage.AUTH_TOKEN_EXPIRED);
    });

    it('should create corret message for OvApiValidationError, WRONG_BALLOT_CASTING_KEY_FORMAT', () => {
      const error = new BackendError({
        validationError: {
          validationErrorType: 'WRONG_BALLOT_CASTING_KEY_FORMAT',
        },
      });
      expect(error.badKey).toBe(true);
      expect(error.validationErrorMsg).toBe(
        ErrorMessage.WRONG_BALLOT_CASTING_KEY_FORMAT
      );
    });

    it('should create corret message for OvApiValidationError, BCK_ATTEMPTS_EXCEEDED', () => {
      const error = new BackendError({
        validationError: { validationErrorType: 'BCK_ATTEMPTS_EXCEEDED' },
      });
      expect(error.badKey).toBe(true);
      expect(error.validationErrorMsg).toBe(ErrorMessage.BCK_ATTEMPTS_EXCEEDED);
    });

    it('should create corret message for OvApiValidationError, WRONG_BALLOT_CASTING_KEY', () => {
      const error = new BackendError({
        validationError: {
          validationErrorType: 'WRONG_BALLOT_CASTING_KEY',
          errorArgs: [10],
        },
      });
      expect(error.badKey).toBe(true);
      expect(error.validationErrorMsg).toBe(
        ErrorMessage.WRONG_BALLOT_CASTING_KEY
      );
      expect(error.numberOfRemainingAttempts).toBe(10);
    });
  });

  describe('OvApiNumberOfAttemptsError, OvApiShortValidationError and httpStatus', () => {
    it('should create corret message for OvApiNumberOfAttemptsError', () => {
      const error = new BackendError({ numberOfRemainingAttempts: 3 });
      expect(error.numberOfRemainingAttempts).toBe(3);
      expect(error.validationErrorMsg).toBe(ErrorMessage.ERROR_RETRY);
    });

    it('should create corret message for OvApiShortValidationError', () => {
      const error = new BackendError({ validationErrorMsg: 'TEST' });
      expect(error.validationErrorMsg).toBe('TEST');
    });

    it('should create message AUTH_TOKEN_EXPIRED for httpStatus 401, 412 and 433', () => {
      [401, 412, 422].forEach((httpStatus) => {
        const error = new BackendError({ httpStatus: httpStatus });
        expect(error.validationErrorMsg).toBe(ErrorMessage.AUTH_TOKEN_EXPIRED);
      });
    });

    it('should create default message ERROR for unknown httpStatus', () => {
      const error = new BackendError({ httpStatus: 123 });
      expect(error.validationErrorMsg).toBe(ErrorMessage.ERROR);
    });
  });
});
