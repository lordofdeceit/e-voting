/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {Observable} from 'rxjs';

export interface IProgressData {
  titleLabel: string;
  titleSlowLabel: string;
  waitingTimeBeforeTitleChanges: number;
  steps: IProgressStep[];
  error: Observable<boolean>;
}

export interface IProgressStep {
  isDone: Observable<boolean>;
  inProgressLabel: string;
  doneLabel: string;
}
