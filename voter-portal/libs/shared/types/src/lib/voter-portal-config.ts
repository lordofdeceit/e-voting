/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import { Identification } from './identification';

export interface IVoterPortalConfig {
  identification: Identification;
  contestsCapabilities: IContestsCapabilities;
  platformRootCA: string;
  translatePlaceholders?: ITranslatePlaceholders;

}

export interface IContestsCapabilities {
  writeIns: boolean;
}

export interface ITranslatePlaceholders {
  en: ITranslatePlaceholderObject,
  de: ITranslatePlaceholderObject,
  fr: ITranslatePlaceholderObject,
  it: ITranslatePlaceholderObject,
  rm: ITranslatePlaceholderObject,
}

export interface ITranslatePlaceholderObject {
  [key: string]: string
}
