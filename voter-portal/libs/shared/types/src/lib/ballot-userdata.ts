/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {ICandidate, ICandidateList, IContest} from './ballot-response';

export interface IBallotUserData {
  contests: IContestUserData[];
}

export interface IContestUserData {
  questions?: IQuestionUserData[];
  candidates?: ICandidateUserData[];
  listId?: string | null;
}

export interface ICandidateUserData {
  candidateId: string | null;
  writeIn: string | null;
}

export interface IQuestionUserData {
  id: string;
  chosenOption: string | null;
}

export interface IContestAndContestUserData {
  contest: IContest;
  contestUserData: IContestUserData;
}

export interface IContestVerifyData {
  contest: IContest;
  contestUserData?: IContestUserData;
  choiceCodes: string[];
}

export interface ICandidateSlotData {
  candidate?: ICandidate;
  canCumulate?: boolean;
}

export interface IListSlotData {
  list?: ICandidateList;
}
