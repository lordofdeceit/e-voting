/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
export interface IVoter {
  initializationCode: string;
  dobOrYob: string;
}
