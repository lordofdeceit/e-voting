/*
 * Copyright 2022 by Swiss Post, Information Technology
 */
export interface ICandidateFilters {
  searchTerm: string;
  listId?: string;
  selectedCandidatesOnly: boolean;
}
