/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
export interface IBackendConfig {
  lib: string;
  lang: string;
  tenantId: string;
  electionEventId: string;
  platformRootCA: string;
}
