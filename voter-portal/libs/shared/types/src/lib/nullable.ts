/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
export type Nullable<T> = T | null | undefined;
