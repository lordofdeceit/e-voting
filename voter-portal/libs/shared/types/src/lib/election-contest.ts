/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {
  ICandidate,
  ICandidateList,
  IContest,
  IQuestion,
  ICandidatesQuestion,
  IListQuestion,
} from './ballot-response';
import {TemplateType} from './template-type';

export class ElectionContest implements IContest {
  id!: string;
  defaultTitle?: string;
  description?: string;
  template!: string;
  title?: string;
  howToVote?: string;
  questions?: IQuestion[];
  candidatesQuestion?: ICandidatesQuestion;
  listQuestion?: IListQuestion;
  allowFullBlank?: boolean;
  lists?: ICandidateList[];

  constructor(contest: IContest) {
    if (contest.template !== TemplateType.LISTS_AND_CANDIDATES) {
      throw new Error(
        'Unable to create a ListAndCandidateContest with a contest whose template is of type "OPTIONS".'
      );
    }

    Object.assign(this, contest);
  }

  get hasListQuestion(): boolean {
    return !!this.listQuestion && this.listQuestion.maxChoices > 0;
  }

  private _allCandidates?: ICandidate[];

  get allCandidates(): ICandidate[] {
    if (!this._allCandidates) {
      const lists = this.lists ?? [];
      this._allCandidates = lists.reduce((candidates, list) => {
        return list.candidates
          ? [...candidates, ...list.candidates]
          : candidates;
      }, [] as ICandidate[]);
    }
    return this._allCandidates;
  }

  private _allCandidatesById?: ReadonlyMap<string, ICandidate>;

  get allCandidatesById(): ReadonlyMap<string, ICandidate> {
    if (!this._allCandidatesById) {
      this._allCandidatesById = this.allCandidates.reduce((candidateMap, candidate) => {
        return candidateMap.set(candidate.id, candidate);
      }, new Map<string, ICandidate>());
    }
    return this._allCandidatesById;
  }

  private _realCandidates?: ICandidate[];

  get realCandidates(): ICandidate[] {
    if (!this._realCandidates) {
      this._realCandidates = this.allCandidates.filter(
        (candidate) => !candidate.isBlank && !candidate.isWriteIn
      );
    }
    return this._realCandidates;
  }

  private _blankCandidates?: ICandidate[];

  get blankCandidates(): ICandidate[] {
    if (!this._blankCandidates) {
      this._blankCandidates = this.allCandidates.filter(
        (candidate) => candidate.isBlank
      );
    }
    return this._blankCandidates;
  }

  get blankCandidate(): ICandidate | undefined {
    return this.blankCandidates[0];
  }

  private _writeInCandidates?: ICandidate[];

  get writeInCandidates(): ICandidate[] {
    if (!this._writeInCandidates) {
      this._writeInCandidates = this.allCandidates.filter(
        (candidate) => candidate.isWriteIn
      );
    }
    return this._writeInCandidates;
  }

  get writeInCandidate(): ICandidate | undefined {
    return this.writeInCandidates[0];
  }

  private _realLists?: ICandidateList[];

  get realLists(): ICandidateList[] {
    if (!this._realLists) {
      this._realLists = this.lists?.filter(
        (list) => !list.isBlank && !list.isWriteIn
      ) ?? [];
    }
    return this._realLists;
  }

  private _blankLists?: ICandidateList[];

  get blankLists(): ICandidateList[] {
    if (!this._blankLists) {
      this._blankLists = this.lists?.filter(
        (list) => list.isBlank
      ) ?? [];
    }

    return this._blankLists;
  }

  get blankList(): ICandidateList | undefined {
    return this.blankLists[0];
  }

  getCandidate(candidateId: string | null): ICandidate | null {
    return candidateId && this.allCandidatesById.has(candidateId)
      ? (this.allCandidatesById.get(candidateId) as ICandidate)
      : null;
  }

  getList(listId: string | null | undefined): ICandidateList | null {
    if (listId) {
      return this.lists?.find((list) => list.id === listId) ?? null;
    }

    return null;
  }
}
