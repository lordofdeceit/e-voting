/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
export interface ISendVoteResponse {
  choiceCodes: string[];
}
