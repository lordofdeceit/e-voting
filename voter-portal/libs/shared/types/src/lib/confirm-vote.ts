/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
export interface IConfirmVote {
  ballotCastingKey: string;
}

export interface IConfirmVoteResponse {
  voteCastCode?: string;
}
