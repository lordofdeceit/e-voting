/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {Injectable} from '@angular/core';
import {IVoterPortalConfig} from '@swiss-post/types';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {shareReplay, tap} from 'rxjs/operators';
import {environment} from '@swiss-post/backend';

@Injectable({
  providedIn: 'root',
})
export class ConfigurationService {
  private config: IVoterPortalConfig | undefined; // Initialized by app.module.ts.
  private config$: Observable<IVoterPortalConfig> | null = null;

  constructor(
    private readonly http: HttpClient
  ) {
  }

  get configuration$(): Observable<IVoterPortalConfig> {
    if (!this.config$) {
      this.config$ = this.http
        .get<IVoterPortalConfig>(environment.voterPortalConfig)
        .pipe(
          tap((config) => {
            this.config = config;
          }),
          // Cache: replay the response on each subscription.
          shareReplay(1)
        );
    }
    return this.config$;
  }

  get configuration() {
    return this.config;
  }
}
