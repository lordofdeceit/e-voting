/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
export * from './lib/candidate.module';

export * from './lib/candidate-selection-modal/candidate-selection-modal.component';
export * from './lib/candidates/candidates.component';
