/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ElectionContest, ICandidate, IContestUserData} from '@swiss-post/types';
import {MockComponent, MockProvider} from 'ng-mocks';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {CandidateDetailsComponent} from '../candidate-details/candidate-details.component';

import {CandidateSelectorComponent} from './candidate-selector.component';

describe('CandidateSelectorComponent', () => {
  let component: CandidateSelectorComponent;
  let fixture: ComponentFixture<CandidateSelectorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        CandidateSelectorComponent,
        MockComponent(CandidateDetailsComponent),
      ],
      providers: [MockProvider(NgbActiveModal)],
      imports: [TranslateTestingModule.withTranslations({})]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidateSelectorComponent);
    component = fixture.componentInstance;

    component.electionContest = {} as ElectionContest;
    component.contestUserData = {} as IContestUserData;
    component.candidate = {} as ICandidate;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
