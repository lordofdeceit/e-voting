/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { FAQService } from '@swiss-post/shared/ui';
import { ElectionContest, FAQSection, ICandidate, IContest, IContestUserData } from '@swiss-post/types';
import { merge, Observable, of } from 'rxjs';

interface ICandidateTemplateContext {
  candidate: ICandidate | null;
  candidateIndex: number;
}

@Component({
  selector: 'swp-candidates',
  templateUrl: './candidates.component.html',
  styleUrls: [ './candidates.component.scss' ],
})
export class CandidatesComponent implements OnInit {
  readonly FAQSection = FAQSection;
  @Input() electionContest!: ElectionContest;
  @Input() contestFormGroup: FormGroup | undefined;
  @Input() rightColumnTemplate: TemplateRef<ICandidateTemplateContext> | undefined;
  @Input() showCandidateDetails = false;
  @Input() showErrors = false;
  candidateSelection!: (ICandidate | null)[];

  constructor(
    public readonly faqService: FAQService,
    private readonly translate: TranslateService
  ) {}

  private _contestUserData: IContestUserData | undefined;

  get contestUserData(): IContestUserData | undefined {
    return this._contestUserData || this.contestFormGroup?.value;
  }

  @Input() set contestUserData(value: IContestUserData | undefined) {
    this._contestUserData = value;
  }

  @Input() set contest(value: IContest | undefined) {
    if (value) {
      this.electionContest = new ElectionContest(value);
    }
  }

  ngOnInit() {
    merge(of(this.contestUserData), this.contestFormGroup?.valueChanges as Observable<IContestUserData>)
      .subscribe(() => {
        this.candidateSelection = Array.from(
          { length: this.electionContest.candidatesQuestion?.maxChoices ?? 0 },
          (_, i) => this.getCandidate(i),
        );
      });
  }

  getWriteInControl(candidateIndex: number): FormControl | undefined {
    const candidatesFromArray = this.contestFormGroup?.get('candidates') as FormArray;
    const candidateFormGroup = candidatesFromArray?.controls[candidateIndex];
    return candidateFormGroup?.get('writeIn') as FormControl;
  }

  getWriteInValue(candidateIndex: number): string | null {
    return this.contestUserData?.candidates ? this.contestUserData?.candidates[candidateIndex].writeIn : null;
  }

  getCandidatePlaceholder(index: number): string {
    if (!this.contestUserData) {
      return this.translate.instant('listandcandidates.selection', { position: index + 1 });
    }

    return this.electionContest.blankCandidate?.details?.text || this.translate.instant(
      'listandcandidates.nocandidatechosen');
  }

  getScreenReaderLabel(index: number): string | null {
    const positionDetails = {
      position: index + 1, maxChoices: this.electionContest.candidatesQuestion?.maxChoices,
    };

    return this.contestUserData
      ? this.translate.instant('listandcandidates.candidatepositionof', positionDetails)
      : null;
  }

  private getCandidate(candidateIndex: number): ICandidate | null {
    const candidateId = this.contestUserData?.candidates
      ? this.contestUserData.candidates[candidateIndex]?.candidateId
      : null;

    return this.electionContest.getCandidate(candidateId);
  }
}
