/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {
  getContestsAndContestsUserData,
  getLoading,
  getBackendError,
  hasBackendError,
  getHasVoteResponse,
} from '@swiss-post/shared/state';
import {BackendError, ConfirmationModalConfig, IContestAndContestUserData} from '@swiss-post/types';
import {Observable, of, partition} from 'rxjs';
import {map} from 'rxjs/operators';
import * as Actions from '@swiss-post/shared/state';
import {ConfirmationService, ProcessCancellationService, ProgressService} from '@swiss-post/shared/ui';

@Component({
  selector: 'swp-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss'],
})
export class ReviewComponent {
  contestsAndValues$: Observable<IContestAndContestUserData[] | undefined>;
  isLoading$: Observable<boolean> = this.store.select(getLoading);
  error$: Observable<BackendError | null | undefined> =
    this.store.select(getBackendError);

  constructor(
    private readonly store: Store,
    private readonly progress: ProgressService,
    private readonly cancelProcessService: ProcessCancellationService,
    private readonly confirmationService: ConfirmationService,
    private readonly router: Router,
  ) {
    this.contestsAndValues$ = this.store
      .select(getContestsAndContestsUserData)
      .pipe(
        map(({contests, contestsUserData}) =>
          contests?.map((contest, i) => {
            const contestUserData = contestsUserData
              ? contestsUserData[i]
              : {questions: []};
            return {contest, contestUserData};
          })
        )
      );
  }

  confirmCancel() {
    this.cancelProcessService.cancelVote();
  }

  confirmSeal(): void {
    const sealingModalConfig: ConfirmationModalConfig = {
      title: 'review.confirm.title',
      content: ['review.confirm.questionsealvote', 'review.confirm.hintconfirm'],
      confirmLabel: 'review.confirm.yes',
      cancelLabel: 'review.confirm.no',
      modalOptions: {size: 'lg'},
    };

    const [sealingConfirmed$, sealingRejected$] = partition(
      this.confirmationService.confirm(sealingModalConfig),
      (wasSealingConfirmed) => wasSealingConfirmed
    );

    sealingConfirmed$.subscribe(() => this.seal());
    sealingRejected$.subscribe(() => this.router.navigate(['/choose']));
  }

  private seal() {
    this.store.dispatch(Actions.seal());

    this.progress.open({
      titleLabel: 'review.progress.title',
      titleSlowLabel: 'review.progress.titleslow',
      waitingTimeBeforeTitleChanges: 2000,
      steps: [
        {
          isDone: this.store.pipe(getHasVoteResponse),
          inProgressLabel: 'review.progress.step.sealing.inprogress',
          doneLabel: 'review.progress.step.sealing.done',
        },
        {
          isDone: of(true),
          inProgressLabel: 'review.progress.step.generatechoices.inprogress',
          doneLabel: 'review.progress.step.generatechoices.done',
        },
      ],
      error: this.store.select(hasBackendError),
    });
  }
}
