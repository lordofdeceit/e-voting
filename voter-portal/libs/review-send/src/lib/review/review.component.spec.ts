/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import { ParagraphsDirective } from '../../../../shared/ui/src/lib/paragraphs/paragraphs.directive';
import {ReviewComponent} from './review.component';
import {TranslateTestingModule} from 'ngx-translate-testing';
import { MockComponent, MockDirective, MockProvider } from 'ng-mocks';
import {ReviewContestContainerComponent} from '../review-contest-container/review-contest-container.component';
import '@angular/localize/init';
import {IconComponent} from '@swiss-post/shared/icons';
import {Location} from '@angular/common';
import {
  BackendError,
  BallotResponseStatus,
  IBallotResponse,
  IContest,
  IContestUserData,
} from '@swiss-post/types';
import {SHARED_FEATURE_KEY} from '@swiss-post/shared/state';
import {Component, DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';
import {
  MockContest,
  MockContestUserData,
  MockQuestions,
  RandomArray,
} from '@swiss-post/shared/testing';
import {
  BackendErrorComponent,
  ConfirmationService,
  ProcessCancellationService,
} from '@swiss-post/shared/ui';
import {of} from 'rxjs';

@Component({
  selector: 'swp-review',
  template: '',
})
class ChooseComponent {
}

class MockState {
  error: BackendError | undefined;
  ballot: IBallotResponse;
  ballotUserData: { contests: IContestUserData[] } | undefined;
  loading = false;

  constructor() {
    this.ballot = {
      ballot: {
        contests: [],
        id: '1',
        correctnessIds: {},
        status: BallotResponseStatus.NOT_SENT,
      },
    };
  }
}

describe('ReviewComponent', () => {
  let component: ReviewComponent;
  let fixture: ComponentFixture<ReviewComponent>;
  const initialState: MockState = Object.freeze(new MockState());
  let store: MockStore;
  let confirmationService: ConfirmationService;
  let processCancellationService: ProcessCancellationService;
  let location: Location;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ReviewComponent,
        MockComponent(ReviewContestContainerComponent),
        MockComponent(IconComponent),
        MockComponent(BackendErrorComponent),
        MockDirective(ParagraphsDirective)
      ],
      imports: [
        RouterTestingModule.withRoutes([
          {path: 'choose', component: ChooseComponent},
        ]),
        TranslateTestingModule.withTranslations({}),
      ],
      providers: [
        provideMockStore({
          initialState: {[SHARED_FEATURE_KEY]: initialState},
        }),
        MockProvider(ConfirmationService, {confirm: () => of(true)}),
        MockProvider(ProcessCancellationService),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    store = TestBed.inject(MockStore);
    confirmationService = TestBed.inject(ConfirmationService);
    processCancellationService = TestBed.inject(ProcessCancellationService);
    location = TestBed.inject(Location);
    fixture = TestBed.createComponent(ReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('propagate data to child components', () => {
    let contestsUserData: IContestUserData[];
    let contestContainerComponents: DebugElement[];
    let contests: IContest[];
    let newState: MockState;

    function setContests(): void {
      contests = RandomArray(
        (contestIndex) => {
          const questionTexts = RandomArray(
            (questionIndex) => {
              return `Contest ${contestIndex} - Question ${questionIndex}`;
            },
            3,
            5
          );

          return MockContest(MockQuestions(questionTexts));
        },
        5,
        1
      );

      contestsUserData = contests.map((contest) =>
        MockContestUserData(contest.questions ?? [])
      );

      newState = {
        ...initialState,
        ballotUserData: {contests: contestsUserData},
      };
      newState.ballot.ballot.contests = contests;
      store.setState({[SHARED_FEATURE_KEY]: newState});
      fixture.detectChanges();
      contestContainerComponents = fixture.debugElement.queryAll(
        By.css('swp-review-contest-container')
      );
    }

    it('should display as many contests as there are in the store', () => {
      setContests();
      expect(contestContainerComponents.length).toBe(contests.length);
    });

    it('should pass the proper contest to the contest container components', () => {
      setContests();
      contests.forEach((contest, i) => {
        const {
          contest: receivedContest,
          contestUserData: receivedContestUserData,
        } = contestContainerComponents[i].componentInstance.contestAndValues;

        expect(receivedContest).toBe(contest);
        expect(receivedContestUserData).toBe(contestsUserData[i]);
      });
    });
  });

  describe('backend-error messages', () => {
    let allErrorComponents: DebugElement[];
    const setError = (error: BackendError | undefined) => {
      store.setState({[SHARED_FEATURE_KEY]: {...initialState, error}});
      fixture.detectChanges();
      allErrorComponents = fixture.debugElement.queryAll(
        By.css('swp-backenderror')
      );
    };

    it('should show error in case there is a backend error', () => {
      setError({
        badKey: false,
      } as BackendError);
      expect(allErrorComponents.length).toBe(1);
      expect(allErrorComponents[0].attributes['id']).toBe('backend_error');
    });
  });

  describe('sealing vote', () => {
    it('check call to dispatch store action', () => {
      jest.spyOn(store, 'dispatch');
      component.confirmSeal();
      expect(store.dispatch).toBeCalledWith({
        type: '[ReviewSend Page] Seal Vote click',
      });
    });

    it('should call confirmSeal on seal button click', () => {
      jest.spyOn(confirmationService, 'confirm');
      const buttonElement = fixture.debugElement.query(
        By.css('#btn_seal_vote')
      ).nativeElement;
      buttonElement.click();
      fixture.detectChanges();

      expect(confirmationService.confirm).toBeCalled();
    });
  });

  describe('buttons', () => {
    it('should show the sealing confirmation modal when the "Seal" button is clicked', () => {
      jest.spyOn(confirmationService, 'confirm');
      const buttonElement = fixture.debugElement.query(
        By.css('#btn_seal_vote')
      ).nativeElement;
      buttonElement.click();
      fixture.detectChanges();

      expect(confirmationService.confirm).toBeCalled();
    });

    it('should call confirmCancel on cancel-button click', () => {
      jest.spyOn(processCancellationService, 'cancelVote');
      const buttonElement = fixture.debugElement.query(
        By.css('#btn_cancel')
      ).nativeElement;
      buttonElement.click();
      fixture.detectChanges();

      expect(processCancellationService.cancelVote).toBeCalled();
    });

    it('should redirect to "/choose" when the "Back" button is clicked', fakeAsync(() => {
      const buttonElement = fixture.debugElement.query(
        By.css('#btn_back_to_choose')
      ).nativeElement;
      buttonElement.click();

      tick();

      expect(location.path()).toBe('/choose');
    }));
  });
});
