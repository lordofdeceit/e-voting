/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {ListModule} from '@swiss-post/list';
import {SharedIconsModule} from '@swiss-post/shared/icons';
import {SharedUiModule} from '@swiss-post/shared/ui';
import {ReviewQuestionListComponent} from './review-question-list/review-question-list.component';
import {ReviewSendRoutingModule} from './review-send-routing.module';
import {ReviewComponent} from './review/review.component';
import {ReviewContestContainerComponent} from './review-contest-container/review-contest-container.component';
import {QuestionsModule} from '@swiss-post/questions';
import {ReviewCandidateListComponent} from './review-candidate-list/review-candidate-list.component';
import {CandidateModule} from '@swiss-post/candidate';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    ReviewSendRoutingModule,
    QuestionsModule,
    CandidateModule,
    ListModule,
    SharedUiModule,
    SharedIconsModule,
  ],
  declarations: [
    ReviewComponent,
    ReviewContestContainerComponent,
    ReviewQuestionListComponent,
    ReviewCandidateListComponent,
  ],
})
export class ReviewSendModule {
}
