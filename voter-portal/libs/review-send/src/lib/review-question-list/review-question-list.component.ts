/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {Component, Input} from '@angular/core';
import {IContestAndContestUserData} from '@swiss-post/types';

@Component({
  selector: 'swp-review-question-list',
  templateUrl: './review-question-list.component.html',
  styleUrls: ['./review-question-list.component.scss'],
})
export class ReviewQuestionListComponent {
  @Input() contestAndValues: IContestAndContestUserData | undefined;
}
