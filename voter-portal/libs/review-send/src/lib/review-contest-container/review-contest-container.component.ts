/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {Component, Input} from '@angular/core';
import {IContestAndContestUserData, TemplateType} from '@swiss-post/types';

@Component({
  selector: 'swp-review-contest-container',
  templateUrl: './review-contest-container.component.html',
  styleUrls: ['./review-contest-container.component.scss'],
})
export class ReviewContestContainerComponent {
  @Input() contestAndValues: IContestAndContestUserData | undefined;
  readonly TemplateType = TemplateType;
}
