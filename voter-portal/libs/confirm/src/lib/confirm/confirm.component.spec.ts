/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {IconComponent} from '@swiss-post/shared/icons';
import {
  ProcessCancellationService,
  PlaceholderService,
  FAQService,
} from '@swiss-post/shared/ui';
import {MockComponent, MockProvider} from 'ng-mocks';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {ConfirmComponent} from './confirm.component';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import '@angular/localize/init';
import {FAQSection, IConfirmVoteResponse} from '@swiss-post/types';
import {SHARED_FEATURE_KEY} from '@swiss-post/shared/state';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';

class MockState {
  confirmVoteResponse?: IConfirmVoteResponse | null;
  cast = false;

  constructor() {
    this.confirmVoteResponse = {
      voteCastCode: '12345678',
    };
  }
}

describe('ConfirmComponent', () => {
  let fixture: ComponentFixture<ConfirmComponent>;
  let store: MockStore;
  const initialState = Object.freeze(new MockState());
  let cancelProcessService: ProcessCancellationService;
  let faqService: FAQService;

  beforeEach(async () => {

    await TestBed.configureTestingModule({
      declarations: [ConfirmComponent, MockComponent(IconComponent)],
      imports: [TranslateTestingModule.withTranslations({})],
      providers: [
        provideMockStore({
          initialState: {[SHARED_FEATURE_KEY]: initialState},
        }),
        MockProvider(PlaceholderService),
        MockProvider(ProcessCancellationService),
        MockProvider(FAQService),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    store = TestBed.inject(MockStore);
    cancelProcessService = TestBed.inject(ProcessCancellationService);
    faqService = TestBed.inject(FAQService);
    fixture = TestBed.createComponent(ConfirmComponent);
    fixture.detectChanges();
  });

  describe('confirm-code', () => {
    it('should show cast vote return code formatted correctly', () => {
      const crvtElement = fixture.debugElement.query(
        By.css('#cvrt')
      ).nativeElement;
      expect(crvtElement.textContent).toContain('1234 5678');
    });
  });

  describe('messages "vote casted"', () => {
    let messageDiv: DebugElement;

    const setCastFlag = (flag: boolean) => {
      store.setState({[SHARED_FEATURE_KEY]: {...initialState, cast: flag}});
      fixture.detectChanges();
      messageDiv = fixture.debugElement.query(By.css('#message-vote-casted'));
    };

    it('should show message "confirm.main.thx" if user did not already cast vote in previous session', () => {
      setCastFlag(false);
      expect(messageDiv.nativeElement.textContent).toContain(
        'confirm.main.thx'
      );
    });

    it('should show message "confirm.main.votecasted" if user did cast vote in previous session', () => {
      setCastFlag(true);
      expect(messageDiv.nativeElement.textContent).toContain(
        'confirm.main.votecasted'
      );
    });
  });

  describe('quit button', () => {
    it('should call quit on click on quit-button', () => {
      jest.spyOn(cancelProcessService, 'quit');

      const quitButtonElement = fixture.debugElement.query(
        By.css('#quit-button')
      ).nativeElement;

      quitButtonElement.click();

      expect(cancelProcessService.quit).toBeCalled();
    });
  });

  describe('help buttons', () => {
    const clickHelpButton = (helpButtonId: string) => {
      jest.spyOn(faqService, 'showFAQ');
      const faqButtonElement = fixture.debugElement.query(
        By.css(`#${helpButtonId}`)
      ).nativeElement;
      faqButtonElement.click();
    };

    it('should call showFAQ on click on show-faq-vcrt-link', () => {
      clickHelpButton('show-faq-vcrt-link');
      expect(faqService.showFAQ).toBeCalledWith(
        FAQSection.WhatIsVoteCastReturnCode
      );
    });

    it('should call showFAQ on click on show-faq-vcrt-link', () => {
      clickHelpButton('show-faq-codes-do-not-match-link');
      expect(faqService.showFAQ).toBeCalledWith(
        FAQSection.WhatIfCodesDoNotMatch
      );
    });
  });
});
