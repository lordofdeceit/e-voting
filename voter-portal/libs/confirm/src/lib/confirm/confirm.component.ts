/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  PlaceholderService,
  FAQService,
  ProcessCancellationService,
} from '@swiss-post/shared/ui';
import { getVoteCastedInPreviousSession, getConfirmVoteResponse } from '@swiss-post/shared/state';
import { FAQSection } from '@swiss-post/types';
import { map } from 'rxjs/operators';

@Component({
  selector: 'swp-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss'],
})
export class ConfirmComponent {
  readonly FAQSection = FAQSection;

  voteCastCode$ = this.store
    .select(getConfirmVoteResponse)
    .pipe(map((response) => this.formatVoteCastCode(response?.voteCastCode)));

  voteCastedInPreviousSession$ = this.store.select(getVoteCastedInPreviousSession);

  constructor(
    private readonly store: Store,
    public readonly placeholders: PlaceholderService,
    private readonly faqService: FAQService,
    private readonly cancelProcessService: ProcessCancellationService
  ) {}

  showFAQ(section: FAQSection): void {
    this.faqService.showFAQ(section);
  }

  formatVoteCastCode(voteCastCode: string | undefined): string | undefined {
    if (voteCastCode) {
      const regexVoteCastCode = /^(\d{4})(\d{4})$/;
      const match = voteCastCode.match(regexVoteCastCode);
      if (match?.length === 3) {
        return `${match[1]} ${match[2]}`;
      }
    }
    return voteCastCode;
  }

  quit() {
    this.cancelProcessService.quit();
  }
}
