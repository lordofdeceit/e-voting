/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ConfirmComponent} from './confirm/confirm.component';
import {ConfirmRoutingModule} from './confirm-routing/confirm-routing.module';
import {TranslateModule} from '@ngx-translate/core';
import {SharedIconsModule} from '@swiss-post/shared/icons';

@NgModule({
  imports: [
    CommonModule,
    ConfirmRoutingModule,
    TranslateModule,
    SharedIconsModule
  ],
  declarations: [ConfirmComponent],
})
export class ConfirmModule {
}
