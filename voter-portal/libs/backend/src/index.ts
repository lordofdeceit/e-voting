/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
export * from './lib/backend.module'
export * from './lib/backend.service'
export * from './lib/environments/environment'
