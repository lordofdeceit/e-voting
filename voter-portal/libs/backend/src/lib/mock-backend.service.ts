/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {RandomInt} from '@swiss-post/shared/testing';
import {
  BackendError,
  BallotResponseStatus,
  ErrorMessage,
  IBackendConfig,
  IBallot,
  IBallotResponse,
  IBallotUserData,
  IConfirmVote,
  IConfirmVoteResponse,
  ISendVoteResponse,
  IVoter,
} from '@swiss-post/types';
import {of} from 'rxjs';
import {delay, map} from 'rxjs/operators';
import {BackendService} from './backend.service';
import mockBallot from './mock-ballot.json';

function generateId(length: number) {
  return Array.from({length}, () => RandomInt(10)).join('');
}

function throwBackendError(errorProperties: object) {
  const backendError = new BackendError();
  Object.assign(backendError, errorProperties);
  throw backendError;
}

function getMockTranslation(
  item: unknown,
  lang: string,
  translateStrings = true,
): unknown {
  if (
    typeof item === 'number' ||
    typeof item === 'boolean' ||
    (typeof item === 'string' && !translateStrings) ||
    !item
  ) {
    return item;
  }

  if (typeof item === 'string') {
    const stringToTranslate = item.replace(/^\*{3}(DE|FR|IT|RM|EN) /g, '');
    return `***${lang.toUpperCase()} ${stringToTranslate}`;
  }

  if (Array.isArray(item)) {
    return item.map((i) => getMockTranslation(i, lang));
  }

  if (item && typeof item === 'object') {
    return Object.keys(item).reduce((translatedObject, key) => {
      const translatableKeys = [
        'title',
        'description',
        'text',
        'questionType_text',
        'answerType_text',
      ];
      return {
        ...translatedObject,
        [key]: getMockTranslation(
          item[key as keyof typeof item],
          lang,
          translatableKeys.includes(key),
        ),
      };
    }, {});
  }

  return null;
}

enum TokenExpiredScenario {
  None,
  ExpiredReview,
  ExpiredVerify,
}

export class MockBackendService implements BackendService {
  ballotScenario: BallotResponseStatus = BallotResponseStatus.NOT_SENT;
  tokenExpiredScenario = TokenExpiredScenario.None;

  async requestChoiceCodes(): Promise<ISendVoteResponse> {
    const choiceCodes = await this.getChoiceCodes();

    const choiceCodesResponse: ISendVoteResponse = {
      choiceCodes,
    };
    return of(choiceCodesResponse).pipe(delay(500)).toPromise();
  }

  requestVoteCastCode(): Promise<IConfirmVoteResponse> {
    const voteCastCode: IConfirmVoteResponse = {
      voteCastCode: generateId(8),
    };
    return of(voteCastCode).pipe(delay(500)).toPromise();
  }

  requestBallot(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    startVotingKey: string,
  ): Promise<IBallotResponse> {
    const ballot: IBallotResponse = {
      ballot: {...mockBallot, status: this.ballotScenario} as IBallot,
    };

    const ballotObservable = of(ballot);
    return ballotObservable.pipe(delay(500)).toPromise();
  }

  translateBallot(
    ballot: IBallotResponse,
    lang: string,
  ): Promise<IBallotResponse> {
    return of(getMockTranslation(ballot, lang) as IBallotResponse)
      .pipe(delay(500))
      .toPromise();
  }

  authenticate(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    voter: IVoter, config: IBackendConfig,
  ): Promise<string> {
    this.ballotScenario = BallotResponseStatus.NOT_SENT;
    this.tokenExpiredScenario = TokenExpiredScenario.None;

    if (voter.initializationCode.substring(0, 1) === 's') {
      this.ballotScenario = BallotResponseStatus.SENT_BUT_NOT_CAST;
    } else if (voter.initializationCode.substring(0, 1) === 'c') {
      this.ballotScenario = BallotResponseStatus.CAST;
    }

    if (voter.initializationCode.substring(1, 2) === 'r') {
      this.tokenExpiredScenario = TokenExpiredScenario.ExpiredReview;
    } else if (voter.initializationCode.substring(1, 2) === 'v') {
      this.tokenExpiredScenario = TokenExpiredScenario.ExpiredVerify;
    }

    const startVotingKey = of('mocking-backend-service');
    return startVotingKey
      .pipe(
        delay(500),
        map((token) => {
          if (voter.initializationCode.substring(2, 3) === 'r') {
            throwBackendError({
              numberOfRemainingAttempts: 3,
              validationErrorMsg: ErrorMessage.ERROR_RETRY,
              badKey: false,
            });
          } else if (voter.initializationCode.substring(2, 3) === 'b') {
            throwBackendError({
              validationErrorMsg: ErrorMessage.VALIDATION_ERROR,
              badKey: true,
            });
          }

          return token;
        }),
      )
      .toPromise();
  }

  async sendVote(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    ballotResponse: IBallotResponse, userData: IBallotUserData,
  ): Promise<ISendVoteResponse> {
    const choiceCodes = await this.getChoiceCodes();

    const response: ISendVoteResponse = {
      choiceCodes,
    };
    return of(response)
      .pipe(
        delay(100),
        map((sendVoteResponse) => {
          if (
            this.tokenExpiredScenario === TokenExpiredScenario.ExpiredReview
          ) {
            throwBackendError({
              validationErrorMsg: ErrorMessage.AUTH_TOKEN_EXPIRED,
              badKey: false,
              authTokenExpired: true,
            });
          }
          return sendVoteResponse;
        }),
      )
      .toPromise();
  }

  confirmVote(confirmVote: IConfirmVote): Promise<IConfirmVoteResponse> {
    const response: IConfirmVoteResponse = {
      voteCastCode: generateId(8),
    };
    return of(response)
      .pipe(
        delay(3000),
        map((confirmVoteResponse) => {
          if (
            this.tokenExpiredScenario === TokenExpiredScenario.ExpiredVerify
          ) {
            throwBackendError({
              validationErrorMsg: ErrorMessage.AUTH_TOKEN_EXPIRED,
              badKey: false,
              authTokenExpired: true,
            });
          } else if (confirmVote.ballotCastingKey.substring(0, 1) === '9') {
            throwBackendError({
              validationErrorMsg: ErrorMessage.CONNECTION_ERROR,
              badKey: false,
            });
          } else if (confirmVote.ballotCastingKey.substring(0, 1) === '8') {
            throwBackendError({
              numberOfRemainingAttempts: 3,
              validationErrorMsg: ErrorMessage.BCK_ATTEMPTS_EXCEEDED,
              badKey: true,
            });
          }
          return confirmVoteResponse;
        }),
      )
      .toPromise();
  }

  private async getChoiceCodes(): Promise<string[]> {
    const ballotResponse = await this.requestBallot('');
    return ballotResponse.ballot.contests.reduce((choices, contest) => {
      let numChoiceCodeForContest = 0;
      numChoiceCodeForContest += contest.questions?.length ?? 0;
      numChoiceCodeForContest += contest.listQuestion?.maxChoices ?? 0;
      numChoiceCodeForContest += contest.candidatesQuestion?.maxChoices ?? 0;

      for (let i = 0; i < numChoiceCodeForContest; i++) {
        choices.push(generateId(4));
      }

      return choices;
    }, [] as string[]);
  }
}
