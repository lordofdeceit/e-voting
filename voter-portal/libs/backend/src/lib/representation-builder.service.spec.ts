/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {TestBed} from '@angular/core/testing';
import {
  MockBallot,
  MockCandidatesContest,
  MockCandidatesContestUserData,
  MockContest,
  MockContestUserData,
  MockQuestions,
} from '@swiss-post/shared/testing';
import {
  ElectionContest,
  IBallot,
  IBallotUserData,
  IContest,
  IContestUserData,
  IQuestion,
  IQuestionUserData,
} from '@swiss-post/types';
import {RepresentationBuilderService} from './representation-builder.service';

describe('RepresentationBuilderService', () => {
  let service: RepresentationBuilderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RepresentationBuilderService);
  });

  describe('representations for candidates and lists', () => {
    let ballot: IBallot;
    let contest: IContest;
    let contestUserData: IContestUserData;
    let ballotUserData: IBallotUserData;
    let electionContest: ElectionContest;

    beforeEach(() => {
      contest = MockCandidatesContest({
        numSeats: 12,
        cumulAllowed: true,
        numLists: 3,
        numCandidatesPerList: 4,
        hasListQuestion: true,
      });
      contestUserData = MockCandidatesContestUserData(contest);
      ballot = MockBallot([contest]);
      ballotUserData = {
        contests: [contestUserData],
      };
      electionContest = new ElectionContest(contest);
    });

    it('should get representations for candidates and list, random selection', () => {
      //act
      const representations = service.getPrimesAndWriteIns(
        ballot,
        ballotUserData
      );
      // 1 list and 12 seats.
      expect(representations.primes.length).toBe(13);
    });

    it('should get representations for candidates and list', () => {
      //prepare
      ballotUserData.contests[0].listId = electionContest.realLists[0].id;
      ballotUserData.contests[0].candidates?.forEach(
        (candidateUserData, i) =>
          (candidateUserData.candidateId = electionContest.realCandidates[i].id)
      );

      //act
      const representations = service.getPrimesAndWriteIns(
        ballot,
        ballotUserData
      );

      // assert
      representations.primes.forEach((prime, i) => {
        if (i === 0) {
          expect(prime).toBe(electionContest.realLists[0].prime);
        } else {
          const allRepresentations =
            electionContest.realCandidates[i - 1].allRepresentations ?? [];
          expect(prime).toBe(allRepresentations[0]);
        }
      });
    });

    it('should get unique representations for for cumulated candidates', () => {
      //prepare
      ballotUserData.contests[0].candidates?.forEach((candidateUserData, i) => {
        candidateUserData.candidateId =
          electionContest.realCandidates[Math.floor(i / 2)].id;
      });

      //act
      const representations = service.getPrimesAndWriteIns(
        ballot,
        ballotUserData
      );

      const hasDuplicates =
        representations.primes.length !== new Set(representations.primes).size;
      expect(hasDuplicates).toBeFalsy();
    });

    it('should throw error if candidate is cumulated more than allowed', () => {
      //prepare
      ballotUserData.contests[0].candidates?.forEach((candidateUserData) => {
        candidateUserData.candidateId =
          electionContest.realCandidates[0].id;
      });

      //act and check
      expect(() => service.getPrimesAndWriteIns(
        ballot,
        ballotUserData
      )).toThrowError(`No unused representation has been found for candidate ${electionContest.realCandidates[0].id}.`);
    });

    it('should throw error for unknown template', () => {

      //prepare
      ballot.contests[0].template = 'non existing template';

      //act and check
      expect(() => service.getPrimesAndWriteIns(
        ballot,
        ballotUserData
      )).toThrowError(`Unknown template for contest with id ${ballot.contests[0].id}`);
    });


    it('should get representations for candidates and list, no selection', () => {
      //prepare
      ballotUserData.contests[0].listId = null;
      ballotUserData.contests[0].candidates?.forEach(
        (candidateUserData) => (candidateUserData.candidateId = null)
      );

      //act
      const representations = service.getPrimesAndWriteIns(
        ballot,
        ballotUserData
      );

      // assert
      const blankList = electionContest.blankList;
      expect(representations.primes[0]).toBe(blankList?.prime);

      blankList?.candidates?.forEach((candidate, i) => {
        expect(representations.primes[i + 1]).toBe(candidate.prime);
      });
    });
  });

  describe('representations and writeIns for candidates only', () => {
    let ballot: IBallot;
    let contest: IContest;
    let contestUserData: IContestUserData;
    let ballotUserData: IBallotUserData;
    let electionContest: ElectionContest;

    beforeEach(() => {
      contest = MockCandidatesContest({
        numSeats: 4,
        cumulAllowed: false,
        numLists: 12,
        numCandidatesPerList: 1,
        hasListQuestion: false,
      });
      contestUserData = MockCandidatesContestUserData(contest);
      ballot = MockBallot([contest]);
      ballotUserData = {
        contests: [contestUserData],
      };
      electionContest = new ElectionContest(contest);
    });

    it('should get representations for candidates only, random selection', () => {
      //act
      const representations = service.getPrimesAndWriteIns(
        ballot,
        ballotUserData
      );

      //assert
      expect(representations.primes.length).toBe(4);
    });

    it('should get writeins', () => {
      //prepare
      ballotUserData.contests[0].candidates?.forEach((candidateUserData, i) => {
        candidateUserData.candidateId =
          electionContest.writeInCandidate?.id ?? null;
        candidateUserData.writeIn = `writeIn-${i}`;
      });

      //act
      const representations = service.getPrimesAndWriteIns(
        ballot,
        ballotUserData
      );

      //assert
      ballotUserData.contests[0].candidates?.forEach((candidateUserData, i) => {
        const prime = electionContest.writeInCandidates[i].prime;
        const expectedWriteIn = `${prime}#${candidateUserData.writeIn}`;
        expect(representations.writeIns[i]).toBe(expectedWriteIn);
      });
    });
  });

  describe('representations for options', () => {
    let ballot: IBallot;
    let ballotUserData: IBallotUserData;
    let allQuestions: IQuestion[];
    let allQuestionsUserData: IQuestionUserData[];

    beforeEach(() => {
      const questionTexts1 = ['question 1', 'question 2', 'question 3'];
      const mockQuestions1 = MockQuestions(questionTexts1);
      const contest1 = MockContest(mockQuestions1);

      const questionTexts2 = ['question 1', 'question 2'];
      const mockQuestions2 = MockQuestions(questionTexts2);
      const contest2 = MockContest(mockQuestions2);

      const contestUserData1 = MockContestUserData(mockQuestions1, {
        noBlankAnswers: true,
      });
      const contestUserData2 = MockContestUserData(mockQuestions2, {
        noBlankAnswers: true,
      });

      ballot = MockBallot([contest1, contest2]);
      ballotUserData = {
        contests: [contestUserData1, contestUserData2],
      };

      allQuestions = [...mockQuestions1, ...mockQuestions2];
      allQuestionsUserData = [
        ...(contestUserData1.questions ?? []),
        ...(contestUserData2.questions ?? []),
      ];
    });

    it('should get representations for options, no blank option chosen', () => {
      //act
      const representation = service.getPrimesAndWriteIns(ballot, ballotUserData);

      //assert
      expect(representation.primes.length).toBe(5);

      allQuestions.forEach((question, i) => {
        const chosenOptionId = allQuestionsUserData[i].chosenOption;
        const chosenOption = question.options.find(
          (option) => option.id === chosenOptionId
        );
        expect(representation.primes[i]).toBe(chosenOption?.prime);
      });
    });

    it('should get representations for options, blank options chosen', () => {
      //setup
      allQuestionsUserData.forEach(
        (questionUserData) => (questionUserData.chosenOption = '')
      );

      //act
      const representation = service.getPrimesAndWriteIns(ballot, ballotUserData);

      //assert
      allQuestions.forEach((question, i) => {
        expect(representation.primes[i]).toBe(question.blankOption.prime);
      });
    });
  });
});
