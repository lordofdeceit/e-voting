/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {
  BallotResponseStatus,
  IBackendConfig,
  BackendError,
  IBallot,
  IBallotResponse,
  IBallotUserData,
  IConfirmVote,
  IConfirmVoteResponse,
  ISendVoteResponse,
  IVoter,
  IOvApi,
  ErrorMessage,
} from '@swiss-post/types';
import { BackendService } from './backend.service';
import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';
import { RepresentationBuilderService } from './representation-builder.service';

declare global {
  const OvApi: (config: IBackendConfig) => IOvApi;
  interface Window {
    ovApi: IOvApi;
  }
}

function deepClone<T>(obj: T): T {
  return JSON.parse(JSON.stringify(obj));
}

@Injectable({
  providedIn: 'root',
})
export class OvBackendService implements BackendService {
  ballot: IBallot | null = null;
  private _ovApi?: IOvApi;

  private set ovApi(value: IOvApi) {
    this._ovApi = window.ovApi = value;
  }

  private get ovApi(): IOvApi {
    if (this._ovApi === undefined) {
      throw new BackendError({
        validationErrorMsg: ErrorMessage.OV_NOT_INITIALIZED,
      });
    }
    return this._ovApi;
  }

  constructor(
    private readonly translate: TranslateService,
    private readonly representationBuilder: RepresentationBuilderService
  ) {}

  async authenticate(voter: IVoter, config: IBackendConfig): Promise<string> {
    try {
      await this.initializeOv(config);
      return await this.ovApi.authenticate(
        voter.initializationCode.toLowerCase(),
        voter.dobOrYob,
        config.tenantId,
        config.electionEventId
      );
    } catch (error) {
      throw new BackendError(error);
    }
  }

  async requestBallot(startVotingKey: string): Promise<IBallotResponse> {
    try {
      const rawBallot = await this.ovApi.requestBallot(startVotingKey);
      this.ballot = this.ovApi.parseBallot(rawBallot);
    } catch (error) {
      throw new BackendError(error);
    }

    if (
      this.ballot &&
      [
        BallotResponseStatus.BLOCKED,
        BallotResponseStatus.WRONG_BALLOT_CASTING_KEY,
      ].includes(this.ballot.status)
    ) {
      throw new BackendError({ validationErrorMsg: ErrorMessage.INACTIVE_KEY });
    }

    // this object has cyclic properties (parent), so it can not be 'serialized'.
    this.removeAllPropertiesNamedParent(this.ballot);

    this.ovApi.translateBallot(this.ballot, `${this.translate.currentLang}-CH`);

    // The object has to be cloned because it will be immutable after inserted in the ngrx state store.
    // The ovApi does an "in place" translate.
    return { ballot: deepClone(this.ballot) };
  }

  async translateBallot(
    ballot: IBallotResponse,
    lang: string
  ): Promise<IBallotResponse> {
    if (this.ballot) {
      this.ovApi.translateBallot(this.ballot, `${lang}-CH`);
      return { ballot: deepClone(this.ballot) };
    }

    return ballot;
  }

  async sendVote(
    ballotResponse: IBallotResponse,
    ballotUserData: IBallotUserData
  ): Promise<ISendVoteResponse> {
    const { primes, writeIns } =
      this.representationBuilder.getPrimesAndWriteIns(
        ballotResponse.ballot,
        ballotUserData
      );
    try {
      const choiceCodes = await this.ovApi.sendVote(
        primes,
        writeIns,
        ballotResponse.ballot.correctnessIds
      );
      return { choiceCodes };
    } catch (error) {
      throw new BackendError(error);
    }
  }

  async confirmVote(confirmVote: IConfirmVote): Promise<IConfirmVoteResponse> {
    try {
      const { voteCastCode } = await this.ovApi.confirmVote(
        confirmVote.ballotCastingKey
      );
      return { voteCastCode };
    } catch (error) {
      throw new BackendError(error);
    }
  }

  async requestChoiceCodes(): Promise<ISendVoteResponse> {
    try {
      const choiceCodes = await this.ovApi.requestChoiceCodes();
      return { choiceCodes };
    } catch (error) {
      throw new BackendError(error);
    }
  }

  async requestVoteCastCode(): Promise<IConfirmVoteResponse> {
    try {
      const { voteCastCode } = await this.ovApi.requestVoteCastCode();
      return { voteCastCode };
    } catch (error) {
      throw new BackendError(error);
    }
  }

  private async initializeOv(config: IBackendConfig): Promise<void> {
    if (typeof window.ovApi !== 'undefined' && window.ovApi) {
      this.ovApi.terminate();
    }

    this.ovApi = OvApi(config);

    await this.ovApi.init();
  }

  private removeAllPropertiesNamedParent(obj: unknown) {
    if (obj && typeof obj === 'object') {
      delete obj['parent' as keyof typeof obj];

      Object.keys(obj).forEach((key) => {
        this.removeAllPropertiesNamedParent(obj[key as keyof typeof obj]);
      });
    }
  }
}
