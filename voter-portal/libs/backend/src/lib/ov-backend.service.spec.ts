/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {TestBed, waitForAsync} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {MockProvider} from 'ng-mocks';
import {OvBackendService} from './ov-backend.service';
import {RepresentationBuilderService} from './representation-builder.service';
import {
  BackendError,
  BallotResponseStatus,
  ErrorMessage,
  IBackendConfig,
  IBallot,
  IBallotResponse,
  IBallotUserData,
  IConfirmVote,
  IOvApi,
} from '@swiss-post/types';
import {of} from 'rxjs';

class MockOvApi implements IOvApi {
  requestChoiceCodes = jest.fn().mockImplementation(() => of(['choice-code-1']).toPromise());
  requestVoteCastCode = jest.fn().mockImplementation(() => of({voteCastCode: 'voteCastCode'}).toPromise());
  confirmVote = jest.fn().mockImplementation(() => of({voteCastCode: 'voteCastCode'}).toPromise());
  init = jest.fn().mockImplementation(() => of().toPromise());
  translateBallot = jest.fn();
  parseBallot = jest.fn().mockImplementation((ballot) => ballot);
  requestBallot = jest.fn().mockImplementation(() => of({}).toPromise());
  authenticate = jest.fn().mockImplementation(() => of('start-voting-key').toPromise());
  sendVote = jest.fn().mockImplementation(() => of(['choice-code-1']).toPromise());
  terminate = jest.fn();
}

describe('BackendService', () => {
  let service: OvBackendService;
  let mockOvApi: MockOvApi;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateTestingModule.withTranslations({}),
      ],
      providers: [
        OvBackendService,
        MockProvider(RepresentationBuilderService, {
          getPrimesAndWriteIns: () => {
            return {
              primes: [],
              writeIns: [],
            };
          },
        }),
      ],
    });
    service = TestBed.inject(OvBackendService);

    mockOvApi = new MockOvApi();
    type OvApiInit = { OvApi: (config: IBackendConfig) => IOvApi };
    (global as unknown as OvApiInit).OvApi = () => mockOvApi;

    service['_ovApi'] = mockOvApi;
    service.ballot = {} as IBallot;
  });

  describe('authenticate', () => {
    it(
      'should authenticate',
      waitForAsync(async () => {
        const startVotingKey = await service.authenticate(
          {dobOrYob: '', initializationCode: ''},
          {
            electionEventId: 'electionEventId',
            lang: 'de',
            lib: 'lib',
            platformRootCA: 'platformRootCA',
            tenantId: 'tenantId',
          }
        );
        expect(startVotingKey).toBeTruthy();
        expect(mockOvApi.authenticate).toHaveBeenCalled();
      })
    );

    it(
      'should not authenticate with backend exception',
      waitForAsync(async () => {
        mockOvApi.authenticate.mockImplementationOnce(() => {
          throw 0;
        });

        expect.assertions(1);
        try {
          await service.authenticate(
            {dobOrYob: '', initializationCode: ''},
            {
              electionEventId: 'electionEventId',
              lang: 'de',
              lib: 'lib',
              platformRootCA: 'platformRootCA',
              tenantId: 'tenantId',
            }
          );
        } catch (e) {
          expect(e).toBeInstanceOf(BackendError);
        }
      })
    );
  });

  describe('request ballot', () => {
    it(
      'should request ballot',
      waitForAsync(async () => {
        const ballot = await service.requestBallot('startVotingKey');
        expect(ballot).toBeTruthy();

        expect(mockOvApi.requestBallot).toBeCalled();
        expect(mockOvApi.parseBallot).toBeCalled();
        expect(mockOvApi.translateBallot).toBeCalled();
      })
    );

    it(
      'should throw error with message INACTIVE_KEY for BLOCKED ballot',
      waitForAsync(async () => {
        mockOvApi.requestBallot.mockImplementationOnce(() => {
          return of({status: BallotResponseStatus.BLOCKED}).toPromise();
        });

        expect.assertions(2);
        try {
          await service.requestBallot('startVotingKey');
        } catch (e) {
          expect(e).toBeInstanceOf(BackendError);
          expect((e as BackendError).validationErrorMsg).toBe(
            ErrorMessage.INACTIVE_KEY
          );
        }
      })
    );

    it(
      'should throw a backend error in case there is an error in ovApi',
      waitForAsync(async () => {
        mockOvApi.requestBallot.mockImplementationOnce(() => {
          throw new Error();
        });

        expect.assertions(1);
        try {
          await service.requestBallot('startVotingKey');
        } catch (e) {
          expect(e).toBeInstanceOf(BackendError);
        }
      })
    );
  });

  describe('translate', () => {
    it(
      'should translate ballot',
      waitForAsync(async () => {
        await service.translateBallot({} as IBallotResponse, 'fr');
        expect(mockOvApi.translateBallot).toHaveBeenCalled();
      })
    );

    it(
      'should not translate ballot if ballot is not available',
      waitForAsync(async () => {
        service.ballot = null;
        await service.translateBallot({} as IBallotResponse, 'fr');
        expect(mockOvApi.translateBallot).not.toBeCalled();
      })
    );
  });

  describe('sendVote', () => {
    it(
      'should send vote',
      waitForAsync(async () => {
        const sendVoteResponse = await service.sendVote(
          {
            ballot: {
              correctnessIds: {},
            } as IBallot,
          } as IBallotResponse,
          {} as IBallotUserData
        );
        expect(sendVoteResponse).toBeTruthy();
        expect(mockOvApi.sendVote).toBeCalled();
      })
    );

    it(
      'should throw error for incorrect data on send vote',
      waitForAsync(async () => {
        mockOvApi.sendVote.mockImplementationOnce(() => {
          throw 0;
        });

        try {
          await service.sendVote(
            {
              ballot: {
                correctnessIds: {},
              } as IBallot,
            } as IBallotResponse,
            {} as IBallotUserData
          );
        } catch (e) {
          expect(e).toBeInstanceOf(BackendError);
        }
      })
    );
  });

  describe('confirm vote', () => {
    it(
      'should confirm vote',
      waitForAsync(async () => {
        const confirmVoteResponse = await service.confirmVote(
          {} as IConfirmVote
        );
        expect(confirmVoteResponse).toBeTruthy();
        expect(mockOvApi.confirmVote).toBeCalled();
      })
    );

    it(
      'should throw error for incorrect data on confirm vote',
      waitForAsync(async () => {
        mockOvApi.confirmVote.mockImplementationOnce(() => {
          throw 0;
        });

        try {
          await service.confirmVote({} as IConfirmVote);
        } catch (e) {
          expect(e).toBeInstanceOf(BackendError);
        }
      })
    );
  });

  describe('request choice-codes', () => {
    it(
      'should request choice-codes',
      waitForAsync(async () => {
        const spyRequestChoiceCodes = jest.spyOn(
          mockOvApi,
          'requestChoiceCodes'
        );
        const sentVoteResponse = await service.requestChoiceCodes();
        expect(sentVoteResponse).toBeTruthy();
        expect(spyRequestChoiceCodes).toBeCalled();
      })
    );

    it(
      'should throw backend-error in case of error while requesting choice-codes',
      waitForAsync(async () => {
        mockOvApi.requestChoiceCodes.mockImplementationOnce(() => {
          throw new Error();
        });

        try {
          await service.requestChoiceCodes();
        } catch (e) {
          expect(e).toBeInstanceOf(BackendError);
        }
      })
    );
  });

  describe('request vote-cast-code', () => {
    it(
      'should request vote-cast-code',
      waitForAsync(async () => {
        const spyRequestVoteCastCode = jest.spyOn(
          mockOvApi,
          'requestVoteCastCode'
        );
        const confirmVoteResponse = await service.requestVoteCastCode();
        expect(confirmVoteResponse).toBeTruthy();
        expect(spyRequestVoteCastCode).toBeCalled();
      })
    );

    it(
      'should throw backend-error in case of error while requesting choice-codes',
      waitForAsync(async () => {
        mockOvApi.requestVoteCastCode.mockImplementationOnce(() => {
          throw new Error();
        });

        try {
          await service.requestVoteCastCode();
        } catch (e) {
          expect(e).toBeInstanceOf(BackendError);
        }
      })
    );
  });
});
