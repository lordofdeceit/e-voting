/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {
  IBackendConfig,
  IBallotResponse,
  IBallotUserData,
  IConfirmVote,
  IConfirmVoteResponse,
  ISendVoteResponse,
  IVoter,
} from '@swiss-post/types';

export abstract class BackendService {
  /**
   * @throws BackendError
   */
  abstract authenticate(voter: IVoter, config: IBackendConfig): Promise<string>;

  abstract requestBallot(startVotingKey: string): Promise<IBallotResponse>;

  abstract translateBallot(ballot: IBallotResponse, lang: string): Promise<IBallotResponse>;

  abstract sendVote(ballotResponse: IBallotResponse, userData: IBallotUserData): Promise<ISendVoteResponse>;

  abstract confirmVote(confirmVote: IConfirmVote): Promise<IConfirmVoteResponse>;

  abstract requestChoiceCodes(): Promise<ISendVoteResponse>;

  abstract requestVoteCastCode(): Promise<IConfirmVoteResponse>;
}
