# backend

This library was generated with [Nx](https://nx.dev).

It provides a connection to the backend through the Online Voting API.

`ovApi` requires voter's `startVotingKey`, `dateOfBirth` & `config` object to initiate;

```typescript
    // working voter
    const voter: IVoter = {
        intitialCode: '9et8hsech5jbb47yhdhs',
        dob: '01061944'
    }

    // working config
    const config = {
      lang: 'FR',
      tenantId: '100',
      electionEventId: '1806033169574409bf7cc5753246dd8a',
      platformRootCA: '-----BEGIN CERTIFICATE-----MIIDTzCCAjegAwIBAgIUGXP9nL43cyeV3q3NVYYSgyPl9+UwDQYJKoZIhvcNAQELBQAwTzELMAkGA1UEBhMCQ0gxEjAQBgNVBAoMCVN3aXNzUG9zdDEWMBQGA1UECwwNT25saW5lIFZvdGluZzEUMBIGA1UEAwwLZGV2IFJvb3QgQ0EwHhcNMjAwMTAxMjMwMDAwWhcNMjQwMTAxMjMwMDAwWjBPMQswCQYDVQQGEwJDSDESMBAGA1UECgwJU3dpc3NQb3N0MRYwFAYDVQQLDA1PbmxpbmUgVm90aW5nMRQwEgYDVQQDDAtkZXYgUm9vdCBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAPQBIH/RK3zlyCq30fluv8Ls8q2MjDYSp3t1bq0TfbRvgUUGcwbVFfGrD0z8Q5F+febHEiVX5o4PWj+FzQ+gpluOYWwHZRkVrhG50NL8PfOVrWi8MTtQ35JejZDwU8LJmmLKFDrwyfGxnFpDHTTdAV6R6EeOsdhnenHHkav4n9VGbigEkdptkg3+oP0qCvVn4fiQrp5U0rutgALqFe4nF9mfXMiu5CaZsD5H6qG3swOH0lnAhESz+28qunicS1J4C2mNeX3Tz4Pc9uMsRo5UBf3Dp3aznzuldw+QYA6s8l5zimT0DqwT+5dWrsr0mRZjNmG9cSEQX4c7VW/g+VYj8bMCAwEAAaMjMCEwDgYDVR0PAQH/BAQDAgEGMA8GA1UdEwEB/wQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBACgWAvyFw2cxt6iRGmEHanbi7ZkVEFR8J19XCMbNrRs+0BwaJZ5cbSB6m6UgboD8pVDvMizS/imPtzzN38j1E5W6rXgpAzgJXc5qi5Pk7Djvga+QVuT0HKjLYqhasYCWAD5GVglzYbe9qlNcHwxNwZbN4bSoq3M8yep/c495BCRU1d44piHlxR/F3kQEw1m/vPIRwRbUl5cJNZeLys8c9Tp4esjXF+F/kYf/E7tABtuikvwSoTSvgPHFK6QxXMbjEsRWlX8Z6pVZf8PDKdu/pakyk6By43dXzkRm9nlRHxgA/r+m0r4y9/QZOpRuiDEYGPEElW4VN3OaAaoJ/3FI+BE=-----END CERTIFICATE-----',
    };
```

## Running unit tests

Run `nx test backend` to execute the unit tests.
