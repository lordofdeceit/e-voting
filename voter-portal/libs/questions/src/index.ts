/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
export * from './lib/questions/questions.component';

export * from './lib/questions.module';
