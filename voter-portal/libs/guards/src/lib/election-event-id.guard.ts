/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  UrlTree,
} from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class ElectionEventIdGuard implements CanActivate {
  constructor(private readonly router: Router) {}

  canActivate(route: ActivatedRouteSnapshot): boolean | UrlTree {
    const electionEventId = route.paramMap.get('electionEventId');
    const invalidElectionEventId =
      !electionEventId ||
      (electionEventId && !/^[0-9a-fA-F]{32}$/.test(electionEventId));

    if (invalidElectionEventId) {
      return this.router.createUrlTree(['page-not-found']);
    }

    return true;
  }
}
