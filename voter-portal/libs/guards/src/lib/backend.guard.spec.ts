/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {getConfig, getIsAuthenticated} from '@swiss-post/shared/state';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {BackendGuard} from './backend.guard';
import {take} from 'rxjs/operators';

describe('BackendGuard', () => {
  let guard: BackendGuard;
  let store: MockStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [provideMockStore({})],
    });
    guard = TestBed.inject(BackendGuard);
    store = TestBed.inject(MockStore);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  it('should be authenticated', () => {
    store.overrideSelector(getIsAuthenticated, true);
    guard
      .canLoad()
      .pipe(take(1))
      .subscribe((authenticated) => {
        expect(authenticated).toBeTruthy();
      });
  });

  it('should not be authenticated', () => {
    store.overrideSelector(getIsAuthenticated, false);
    store.overrideSelector(getConfig, {
      electionEventId: '123',
      lang: '',
      platformRootCA: '',
      tenantId: '',
      lib: '',
    });

    guard
      .canLoad()
      .pipe(take(1))
      .subscribe((urlTree) => {
        expect(urlTree.toString()).toBe('legal-terms/123');
      });
  });

  it('should not be authenticated, no config', () => {
    store.overrideSelector(getIsAuthenticated, false);
    guard
      .canLoad()
      .pipe(take(1))
      .subscribe((urlTree) => {
        expect(urlTree.toString()).toBe('');
      });
  });
});
