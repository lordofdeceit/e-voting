/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {Component, ElementRef, ViewChild} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import {createMask} from '@ngneat/input-mask';
import * as Actions from '@swiss-post/shared/state';
import {Store} from '@ngrx/store';
import {
  getLoading,
  getSentButNotCast,
  getBackendError,
  getVerifyData,
  hasBackendError,
  getHasConfirmVoteResponse,
} from '@swiss-post/shared/state';
import {
  ProcessCancellationService,
  FAQService,
  PlaceholderService,
  ProgressService,
} from '@swiss-post/shared/ui';
import {
  FAQSection,
  IContest,
  IContestUserData,
  ISendVoteResponse,
  IContestVerifyData,
  IConfirmVote,
  BackendError,
} from '@swiss-post/types';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'swp-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss'],
})
export class VerifyComponent {
  @ViewChild('bckInput') bckInput: ElementRef<HTMLInputElement> | null = null;

  readonly FAQSection = FAQSection;

  isLoading$: Observable<boolean> = this.store.select(getLoading);
  error$: Observable<BackendError | null | undefined> =
    this.store.select(getBackendError);

  sentButNotCast$ = this.store.select(getSentButNotCast);

  ballotCastingKeyForm: FormGroup;
  bckMask = createMask('9999 9999 9');

  sendVoteResponseAndContests$ = this.store
    .select(getVerifyData)
    .pipe(map((data) => this.buildModel(data)));

  get bck(): FormControl {
    return this.ballotCastingKeyForm.get('ballotCastingKey') as FormControl;
  }

  constructor(
    private readonly store: Store,
    private readonly fb: FormBuilder,
    private readonly cancelProcessService: ProcessCancellationService,
    private readonly faqService: FAQService,
    public readonly placeholders: PlaceholderService,
    private readonly progress: ProgressService,
    public readonly translate: TranslateService
  ) {
    this.ballotCastingKeyForm = this.fb.group({
      ballotCastingKey: ['', [Validators.required]],
    });
  }

  private buildModel(data: {
    sendVoteResponse: ISendVoteResponse | null | undefined;
    contests: IContest[] | undefined;
    contestsUserData: IContestUserData[] | undefined;
  }): IContestVerifyData[] {
    const {contests, contestsUserData, sendVoteResponse} = data;

    if (!contests || !sendVoteResponse?.choiceCodes) {
      return [];
    }

    let currentChoiceCodeIndex = 0;
    return contests?.map((contest, i) => {
      const numChoiceCodesForContest = this.getNumChoiceCodesOfContest(contest);
      const choiceCodes =
        sendVoteResponse?.choiceCodes.slice(
          currentChoiceCodeIndex,
          currentChoiceCodeIndex + numChoiceCodesForContest
        ) ?? [];
      currentChoiceCodeIndex += numChoiceCodesForContest;

      return {
        contest: contest,
        choiceCodes: choiceCodes,
        contestUserData: contestsUserData ? contestsUserData[i] : undefined,
      };
    });
  }

  private getNumChoiceCodesOfContest(contest: IContest): number {
    let numChoiceCodeForContest = 0;
    numChoiceCodeForContest += contest.questions?.length ?? 0;
    numChoiceCodeForContest += contest.listQuestion?.maxChoices ?? 0;
    numChoiceCodeForContest += contest.candidatesQuestion?.maxChoices ?? 0;
    return numChoiceCodeForContest;
  }

  confirmCancel() {
    this.cancelProcessService.leaveProcess();
  }

  showFAQ(section: FAQSection) {
    this.faqService.showFAQ(section);
  }

  cast() {
    if (this.ballotCastingKeyForm.invalid) {
      this.ballotCastingKeyForm.reset();
      const firstInvalid = document.querySelector<HTMLInputElement>('input.ng-invalid');
      if (firstInvalid) {
        firstInvalid.focus();
      }
      return;
    }

    const confirmVote: IConfirmVote = {
      ballotCastingKey: this.bck.value.replace(/\s/g, ''),
    };

    this.store.dispatch(Actions.confirmVote({confirmVote}));
    this.progress.open({
      titleLabel: 'verify.progress.title',
      titleSlowLabel: 'verify.progress.titleslow',
      waitingTimeBeforeTitleChanges: 2000,
      steps: [
        {
          isDone: this.store.pipe(getHasConfirmVoteResponse),
          inProgressLabel: 'verify.progress.step.confirming.inprogress',
          doneLabel: 'verify.progress.step.confirming.done',
        },
      ],
      error: this.store.select(hasBackendError),
    });
  }
}
