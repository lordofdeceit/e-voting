/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {IconComponent} from '@swiss-post/shared/icons';
import {VerifyComponent} from './verify.component';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {InputMaskModule} from '@ngneat/input-mask';
import {MockComponent, MockProvider} from 'ng-mocks';
import '@angular/localize/init';
import {
  ProcessCancellationService,
  ClearableInputComponent,
  FAQService,
  PlaceholderService,
  ProgressService,
  BackendErrorComponent,
} from '@swiss-post/shared/ui';
import {
  BallotResponseStatus,
  BackendError,
  IBallotResponse,
  IContest,
  IContestUserData,
  ISendVoteResponse,
  FAQSection,
  IBallot,
} from '@swiss-post/types';
import {
  MockChoiceCodes,
  MockContest,
  MockContestUserData,
  MockQuestions,
  RandomArray,
} from '@swiss-post/shared/testing';
import {SHARED_FEATURE_KEY} from '@swiss-post/shared/state';
import {VerifyContestContainerComponent} from '../verify-contest-container/verify-contest-container.component';
import {DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';

class MockState {
  ballot: IBallotResponse;
  ballotUserData: { contests: IContestUserData[] } | undefined;
  voteResponse: ISendVoteResponse;
  sentButNotCast: boolean | undefined;
  error: BackendError | undefined;
  loading = false;

  constructor() {
    this.ballot = {
      ballot: {
        contests: [],
        id: '1',
        correctnessIds: {},
        status: BallotResponseStatus.NOT_SENT,
      },
    };
    this.voteResponse = {choiceCodes: []};
  }
}

describe('VerifyComponent', () => {
  let fixture: ComponentFixture<VerifyComponent>;
  let store: MockStore;
  const initialState: MockState = Object.freeze(new MockState());
  let contests: IContest[];
  let faqService: FAQService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        InputMaskModule,
        RouterTestingModule,
        TranslateTestingModule.withTranslations({})
      ],
      declarations: [
        VerifyComponent,
        MockComponent(ClearableInputComponent),
        MockComponent(VerifyContestContainerComponent),
        MockComponent(BackendErrorComponent),
        MockComponent(IconComponent),
      ],
      providers: [
        provideMockStore({
          initialState: {[SHARED_FEATURE_KEY]: initialState},
        }),
        MockProvider(PlaceholderService),
        MockProvider(ProcessCancellationService),
        MockProvider(FAQService),
        MockProvider(ProgressService),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    store = TestBed.inject(MockStore);
    faqService = TestBed.inject(FAQService);
    fixture = TestBed.createComponent(VerifyComponent);
    fixture.detectChanges();
  });

  describe('propagate data to child components', () => {
    let contestsUserData: IContestUserData[];
    let contestContainerComponents: DebugElement[];
    let newState: MockState;

    function setContests(): void {
      contests = RandomArray(
        (contestIndex) => {
          const questionTexts = RandomArray(
            (questionIndex) => {
              return `Contest ${contestIndex} - Question ${questionIndex}`;
            },
            3,
            5
          );

          return MockContest(MockQuestions(questionTexts));
        },
        5,
        1
      );

      newState = {
        ...initialState,
      };
      newState.ballot.ballot.contests = contests;
      newState.voteResponse.choiceCodes = MockChoiceCodes(
        newState.ballot.ballot as IBallot
      );
      store.setState({[SHARED_FEATURE_KEY]: newState});
    }

    describe('without user data', () => {
      beforeEach(() => {
        setContests();
        fixture.detectChanges();
        contestContainerComponents = fixture.debugElement.queryAll(
          By.css('swp-verify-contest-container')
        );
      });

      it('should display as many contests as there are in the store', () => {
        expect(contestContainerComponents.length).toBe(contests.length);
      });

      it('should pass the proper contest to the contest container components', () => {
        contests.forEach((contest, i) => {
          const {contest: receivedContest} =
            contestContainerComponents[i].componentInstance.contestVerifyData;
          expect(receivedContest).toBe(contest);
        });
      });

      it('should slice choice-codes properly for each contest', () => {
        let currentIndexInAllChoiceCodes = 0;
        contests.forEach((contest, i) => {
          const {choiceCodes: choiceCodes} =
            contestContainerComponents[i].componentInstance.contestVerifyData;
          expect((choiceCodes as string[]).length).toBe(
            contest.questions?.length
          );

          const choiceCodesSlice = initialState.voteResponse.choiceCodes.slice(
            currentIndexInAllChoiceCodes,
            contest.questions?.length
          );
          expect(choiceCodes).toEqual(expect.arrayContaining(choiceCodesSlice));
          currentIndexInAllChoiceCodes += contest.questions?.length ?? 0;
        });
      });
    });

    describe('with user data', () => {
      function setContestsUserData(): void {
        contestsUserData = contests.map((contest) =>
          MockContestUserData(contest.questions ?? [])
        );
        store.setState({
          [SHARED_FEATURE_KEY]: {
            ...initialState,
            ballotUserData: {contests: contestsUserData},
          },
        });
      }

      beforeEach(() => {
        setContests();
        setContestsUserData();

        fixture.detectChanges();
        contestContainerComponents = fixture.debugElement.queryAll(
          By.css('swp-verify-contest-container')
        );
      });

      it('should properly apply the contest user data to each contest form group', () => {
        contestsUserData.forEach((contestUserData, i) => {
          const {contestUserData: receivedContestUserData} =
            contestContainerComponents[i].componentInstance.contestVerifyData;
          expect(receivedContestUserData).toEqual(contestUserData);
        });
      });
    });
  });

  describe('"abandonned process in previous session" message', () => {
    let sentButNotCastDiv: DebugElement;

    const setFlagSentButNotCast = (flag: boolean) => {
      store.setState({
        [SHARED_FEATURE_KEY]: {...initialState, sentButNotCast: flag},
      });
      fixture.detectChanges();

      sentButNotCastDiv = fixture.debugElement.query(
        By.css('#warningSentButNotCast')
      );
    };

    it('show "abandonned process in previous session" message if flag is set', () => {
      setFlagSentButNotCast(true);
      expect(sentButNotCastDiv.nativeElement.textContent).toContain(
        'verify.warning.abandonedprocess'
      );
    });

    it('do not show "abandonned process in previous session" message if flag is not set', () => {
      setFlagSentButNotCast(false);
      expect(sentButNotCastDiv).toBeFalsy();
    });
  });

  describe('backend-error messages', () => {
    let allErrorComponents: DebugElement[];
    const setError = (error: BackendError | undefined) => {
      store.setState({[SHARED_FEATURE_KEY]: {...initialState, error}});
      fixture.detectChanges();
      allErrorComponents = fixture.debugElement.queryAll(
        By.css('swp-backenderror')
      );
    };

    it('should not show any backend-error by default', () => {
      setError(undefined);
      expect(allErrorComponents.length).toBe(0);
    });

    it('should show backend-error in top area', () => {
      setError({
        badKey: false,
      } as BackendError);
      expect(allErrorComponents.length).toBe(1);
      expect(allErrorComponents[0].attributes['id']).toBe('backend_error');
    });

    it('should show backend-error below by the ballot casting key', () => {
      setError({
        badKey: true,
      } as BackendError);
      expect(allErrorComponents.length).toBe(1);
      expect(allErrorComponents[0].attributes['id']).toBe(
        'backend_error_key_related'
      );
    });
  });

  describe('form validation', () => {
    beforeEach(() => {
      fixture.detectChanges();
    });

    describe('initialState', () => {
      it('should not show validation error without clicking submit button', () => {
        const validationError = fixture.debugElement.query(
          By.css('#bck-required')
        );
        expect(validationError).toBeFalsy();
      });
    });

    describe('validators', () => {
      const setInputValue = (value: string) => {
        const bckInputElement = fixture.debugElement.query(
          By.css('#bck')
        ).nativeElement;
        bckInputElement.value = value;
        bckInputElement.dispatchEvent(new Event('input'));
        fixture.detectChanges();
      };

      it('should not validate with partial input', () => {
        setInputValue('1234');
        expect(fixture.debugElement.componentInstance.bck.invalid).toBeTruthy();
      });

      it('should validate with valid input', () => {
        setInputValue('123456789');
        expect(fixture.debugElement.componentInstance.bck.invalid).toBeFalsy();
      });
    });

    describe('validation error messages', () => {
      let validationErrorDiv: DebugElement;

      const setupBckInputValueAndClickSubmitButton = (bckValue: string) => {
        fixture.debugElement.componentInstance.bck.setValue(bckValue);
        fixture.detectChanges();

        const confirmButtonElement = fixture.debugElement.query(
          By.css('#btn_confirm_vote')
        ).nativeElement;

        confirmButtonElement.click();
        fixture.detectChanges();

        validationErrorDiv = fixture.debugElement.query(
          By.css('#bck-required')
        );
      };

      it('should show validation error without input after submit button is clicked', () => {
        setupBckInputValueAndClickSubmitButton('');
        expect(validationErrorDiv).toBeTruthy();
      });

      it('should not show validation error with correct input after submit button is clicked', () => {
        setupBckInputValueAndClickSubmitButton('123456789');
        expect(validationErrorDiv).toBeFalsy();
      });
    });

    describe('check call to dispatch store action', () => {
      const callCastOnComponent = (value: string) => {
        fixture.debugElement.componentInstance.bck.setValue(value);
        fixture.detectChanges();
        fixture.debugElement.componentInstance.cast();
      };

      beforeEach(() => {
        jest.spyOn(store, 'dispatch');
      });

      it('should call dispatch action if input was valid', () => {
        callCastOnComponent('123456789');
        expect(store.dispatch).toHaveBeenCalledWith({
          confirmVote: {ballotCastingKey: '123456789'},
          type: '[Verify Page] Confirm Vote click',
        });
      });

      it('should not call dispatch action if input was not valid', () => {
        callCastOnComponent('');
        expect(store.dispatch).not.toHaveBeenCalled();
      });
    });
  });
  describe('disable submit button while loading', () => {
    let confirmButton: DebugElement;
    const setLoadingState = (isLoading: boolean) => {
      store.setState({
        [SHARED_FEATURE_KEY]: {...initialState, loading: isLoading},
      });
      fixture.detectChanges();

      confirmButton = fixture.debugElement.query(By.css('#btn_confirm_vote'));
    };

    it('should not disable button while not loading data', () => {
      setLoadingState(false);
      expect(confirmButton.nativeElement.disabled).toBeFalsy();
    });

    it('should disable button while loading data', () => {
      setLoadingState(true);
      expect(confirmButton.nativeElement.disabled).toBeTruthy();
    });
  });

  describe('help buttons', () => {
    const clickHelpButton = (helpButtonId: string) => {
      jest.spyOn(faqService, 'showFAQ');
      const faqButton = fixture.debugElement.query(By.css(`#${helpButtonId}`));
      faqButton.nativeElement.click();
    };

    it('should call showFAQ on click on show-faq-choice-codes-link', () => {
      clickHelpButton('show-faq-choice-codes-link');
      expect(faqService.showFAQ).toBeCalledWith(FAQSection.WhatAreChoiceCodes);
    });

    it('should call showFAQ on click on show-faq-codes-do-not-match-link', () => {
      clickHelpButton('show-faq-codes-do-not-match-link');
      expect(faqService.showFAQ).toBeCalledWith(
        FAQSection.WhatIfCodesDoNotMatch
      );
    });

    it('should call showFAQ on click on show-faq-whatis-bck-link', () => {
      clickHelpButton('show-faq-whatis-bck-link');
      expect(faqService.showFAQ).toBeCalledWith(
        FAQSection.WhatIsBallotCastingKey
      );
    });
  });
});
