/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {Component, Input} from '@angular/core';
import {IContestVerifyData} from '@swiss-post/types';

@Component({
  selector: 'swp-verify-question-list',
  templateUrl: './verify-question-list.component.html',
  styleUrls: ['./verify-question-list.component.scss'],
})
export class VerifyQuestionListComponent {
  @Input() contestVerifyData: IContestVerifyData | undefined;
}
