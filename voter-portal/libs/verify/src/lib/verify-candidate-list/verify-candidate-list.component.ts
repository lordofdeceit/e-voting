/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {Component, Input} from '@angular/core';
import {IContestVerifyData} from '@swiss-post/types';

@Component({
  selector: 'swp-verify-candidate-list',
  templateUrl: './verify-candidate-list.component.html',
  styleUrls: ['./verify-candidate-list.component.scss'],
})
export class VerifyCandidateListComponent {
  @Input() contestVerifyData: IContestVerifyData | undefined;

  get hasList(): boolean {
    const listMaxChoices = this.contestVerifyData?.contest.listQuestion?.maxChoices ?? 0;
    return listMaxChoices > 0;
  }

  getCandidateChoiceCode(candidateIndex: number): string {
    const choiceCodeIndex = this.hasList ? candidateIndex + 1 : candidateIndex;
    return this.contestVerifyData?.choiceCodes[choiceCodeIndex] ?? '';
  }

  getListChoiceCode(): string {
    return this.contestVerifyData?.choiceCodes[0] ?? '';
  }
}
