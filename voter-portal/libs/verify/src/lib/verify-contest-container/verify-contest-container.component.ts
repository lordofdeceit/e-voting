/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {Component, Input} from '@angular/core';
import {IContestVerifyData, TemplateType} from '@swiss-post/types';

@Component({
  selector: 'swp-verify-contest-container',
  templateUrl: './verify-contest-container.component.html',
  styleUrls: ['./verify-contest-container.component.scss'],
})
export class VerifyContestContainerComponent {
  readonly TemplateType = TemplateType;

  @Input() contestVerifyData: IContestVerifyData | undefined;
}
