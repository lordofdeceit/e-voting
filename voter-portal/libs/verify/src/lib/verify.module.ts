/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListModule} from '@swiss-post/list';
import {VerifyQuestionListComponent} from './verify-question-list/verify-question-list.component';
import {VerifyComponent} from './verify/verify.component';
import {VerifyRoutingModule} from './verify-routing-module';
import {TranslateModule} from '@ngx-translate/core';
import {ReactiveFormsModule} from '@angular/forms';
import {InputMaskModule} from '@ngneat/input-mask';
import {VerifyContestContainerComponent} from './verify-contest-container/verify-contest-container.component';
import {QuestionsModule} from '@swiss-post/questions';
import {SharedIconsModule} from '@swiss-post/shared/icons';
import {SharedUiModule} from '@swiss-post/shared/ui';
import {VerifyCandidateListComponent} from './verify-candidate-list/verify-candidate-list.component';
import {CandidateModule} from '@swiss-post/candidate';

@NgModule({
  imports: [
    CommonModule,
    VerifyRoutingModule,
    TranslateModule,
    ReactiveFormsModule,
    InputMaskModule,
    SharedIconsModule,
    SharedUiModule,
    QuestionsModule,
    CandidateModule,
    ListModule,
  ],
  declarations: [
    VerifyComponent,
    VerifyContestContainerComponent,
    VerifyQuestionListComponent,
    VerifyCandidateListComponent,
  ],
})
export class VerifyModule {
}
