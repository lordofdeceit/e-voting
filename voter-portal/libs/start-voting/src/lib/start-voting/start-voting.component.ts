/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { createMask } from '@ngneat/input-mask';
import { Store } from '@ngrx/store';
import { ConfigurationService } from '@swiss-post/shared/configuration';
import * as Actions from '@swiss-post/shared/state';
import {
  getBackendError, getElectionEventId, getIsAuthenticated, getIsBallotDataLoaded, getLoading, hasBackendError,
} from '@swiss-post/shared/state';
import {
  CancelState, FAQService, PlaceholderService, ProcessCancellationService, ProgressService,
} from '@swiss-post/shared/ui';
import { BackendError, FAQSection, Identification } from '@swiss-post/types';
import { Observable } from 'rxjs';

@Component({
  selector: 'swp-start-voting',
  templateUrl: './start-voting.component.html',
  styleUrls: [ './start-voting.component.scss' ],
})
export class StartVotingComponent implements OnInit {
  @ViewChild('startVotingInput') startVotingInput: ElementRef<HTMLInputElement> | null = null;
  readonly Identification = Identification;
  readonly CancelState = CancelState;
  isLoading$: Observable<boolean> = this.store.select(getLoading);
  error$: Observable<BackendError | null | undefined> = this.store.select(getBackendError);
  electionEventId$: Observable<string | null | undefined> = this.store.select(getElectionEventId);
  voterForm: FormGroup;
  identification: Identification | undefined;
  initializationCodeMask = createMask({
    mask: '**** **** **** **** **** ****',
    parser: (value: string) => value.replace(/[^a-zA-Z0-9]/g, ''),
  });

  constructor(
    private readonly store: Store,
    private readonly fb: FormBuilder,
    private readonly configurationService: ConfigurationService,
    private readonly progress: ProgressService,
    private readonly faqService: FAQService,
    public readonly cancelProcessService: ProcessCancellationService,
    public readonly placeholders: PlaceholderService,
  ) {
    this.voterForm = this.fb.group({
      initializationCode: [ '', Validators.required ],
    });

    this.identification = this.configurationService.configuration?.identification;

    if (this.identification) {
      this.voterForm.addControl('dobOrYob', this.fb.control('', Validators.required));
    }
  }

  get initializationCode(): FormControl {
    return this.voterForm.get('initializationCode') as FormControl;
  }

  ngOnInit(): void {
    if (this.cancelProcessService.cancelState !== CancelState.NO_CANCEL_VOTE_OR_LEAVE_PROCESS) {
      this.store.dispatch(Actions.clearBackendError());
    }
    this.store.dispatch(Actions.logout());
  }

  start(): void {
    if (this.voterForm.invalid) {
      this.resetInvalidControls();
      const firstInvalid = document.querySelector<HTMLInputElement>('input.ng-invalid');
      if (firstInvalid) {
        firstInvalid.focus();
      }
      return;
    }

    this.cancelProcessService.reset();
    this.store.dispatch(Actions.start({ voter: this.voterForm.value }));
    this.progress.open({
      titleLabel: 'startvoting.progress.title',
      titleSlowLabel: 'startvoting.progress.titleslow',
      waitingTimeBeforeTitleChanges: 5000,
      steps: [
        {
          isDone: this.store.select(getIsAuthenticated),
          inProgressLabel: 'startvoting.progress.step.authentication.inprogress',
          doneLabel: 'startvoting.progress.step.authentication.done',
        }, {
          isDone: this.store.select(getIsBallotDataLoaded),
          inProgressLabel: 'startvoting.progress.step.retrieveballot.inprogress',
          doneLabel: 'startvoting.progress.step.retrieveballot.done',
        },
      ],
      error: this.store.select(hasBackendError),
    });
  }

  showFAQ(): void {
    this.faqService.showFAQ(FAQSection.WhatIsInitializationCode);
  }

  private resetInvalidControls() {
    Object.values(this.voterForm.controls)
      .filter(control => control.invalid)
      .forEach(control => control.reset());
  }
}
