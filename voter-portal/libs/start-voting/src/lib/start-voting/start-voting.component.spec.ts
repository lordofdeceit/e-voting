/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { InputMaskModule } from '@ngneat/input-mask';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { ConfigurationService } from '@swiss-post/shared/configuration';
import { SharedIconsModule } from '@swiss-post/shared/icons';
import { SHARED_FEATURE_KEY } from '@swiss-post/shared/state';
import { RandomInitializationCode, RandomInt } from '@swiss-post/shared/testing';
import {
  BackendErrorComponent, CancelState, ClearableInputComponent, FAQService, ProcessCancellationService, ProgressService,
} from '@swiss-post/shared/ui';
import { BackendError, Identification, IVoterPortalConfig } from '@swiss-post/types';
import { MockComponent, MockModule, MockProvider } from 'ng-mocks';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { IdentificationComponent } from '../identification/identification.component';

import { StartVotingComponent } from './start-voting.component';

class MockState {
  error: BackendError | undefined;
}

describe('StartVotingComponent', () => {
  let fixture: ComponentFixture<StartVotingComponent>;
  let component: StartVotingComponent;
  let store: MockStore;
  let processCancellationService: ProcessCancellationService;
  let faqService: FAQService;

  const initialState: MockState = Object.freeze(new MockState());

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        InputMaskModule,
        MockModule(SharedIconsModule),
        MockModule(NgbDatepickerModule),
        HttpClientTestingModule,
        RouterTestingModule,
        TranslateTestingModule.withTranslations({}),
      ], providers: [
        MockProvider(ProgressService),
        provideMockStore({
          initialState: { [SHARED_FEATURE_KEY]: initialState },
        }),
        MockProvider(ProcessCancellationService, {
          cancelState: CancelState.NO_CANCEL_VOTE_OR_LEAVE_PROCESS,
        }),
        MockProvider(FAQService), MockProvider(ConfigurationService, {
          configuration: {
            identification: '' as Identification,
          } as IVoterPortalConfig,
        }),
      ], declarations: [
        StartVotingComponent,
        MockComponent(ClearableInputComponent),
        MockComponent(IdentificationComponent),
        MockComponent(BackendErrorComponent),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StartVotingComponent);
    component = fixture.componentInstance;

    store = TestBed.inject(MockStore);
    processCancellationService = TestBed.inject(ProcessCancellationService);
    faqService = TestBed.inject(FAQService);

    fixture.detectChanges();
  });

  it('should properly show a title', () => {
    const title = fixture.debugElement.query(By.css('h1')).nativeElement;
    expect(title?.textContent).toContain('startvoting.title');
  });

  it('should open the FAQ when clicking the corresponding button', () => {
    const faqButton = fixture.debugElement.query(By.css(`#show-faq-link`)).nativeElement;
    jest.spyOn(faqService, 'showFAQ');

    faqButton.click();

    expect(faqService.showFAQ).toBeCalled();
  });

  describe('initialization code input', () => {
    function getValidationError(): DebugElement {
      return fixture.debugElement.query(By.css('#initializationCode-required'));
    }

    function submitForm(): void {
      const submitButton = fixture.debugElement.query(By.css('#btn_start_voting'));
      submitButton.nativeElement.click();
    }

    it('should not show any validation error by default', () => {
      expect(getValidationError()).toBeFalsy();
    });

    describe('correct value', () => {
      beforeEach(() => {
        component.initializationCode.setValue(RandomInitializationCode());
        fixture.detectChanges();
      });

      it('should mark the initialisation code form control as valid', () => {
        expect(component.initializationCode.valid).toBeTruthy();
      });

      it('should not show a validation error on submit', () => {
        submitForm();
        fixture.detectChanges();
        expect(getValidationError()).toBeFalsy();
      });
    });

    describe('incorrect value', () => {
      beforeEach(() => {
        const validCode = RandomInitializationCode();
        const invalidCode = validCode.substring(0, RandomInt(validCode.length - 1));
        component.initializationCode.setValue(invalidCode);
        fixture.detectChanges();
      });

      it('should mark the initialisation code form control as invalid', () => {
        expect(component.initializationCode.valid).toBeFalsy();
      });

      it('should show a validation error on submit', () => {
        submitForm();
        fixture.detectChanges();
        expect(getValidationError()).toBeTruthy();
      });
    });
  });

  describe('backend errors', () => {
    let pageLevelError: DebugElement;
    let initialisationCodeError: DebugElement;

    function setBackendError(error: object | undefined) {
      store.setState({ [SHARED_FEATURE_KEY]: { ...initialState, error } });

      fixture.detectChanges();

      pageLevelError = fixture.debugElement.query(By.css('swp-backenderror'));
      initialisationCodeError = fixture.debugElement.query(By.css('#initializationCode-invalid'));
    }

    it('should not show any backend error by default', () => {
      expect(pageLevelError).toBeFalsy();
      expect(initialisationCodeError).toBeFalsy();
    });

    it('should show the backend error at page level if it does not concern the initialisation code', () => {
      setBackendError({ badKey: false });
      expect(pageLevelError).toBeTruthy();
      expect(initialisationCodeError).toBeFalsy();
    });

    it('should show the backend error at form level if it does concern the initialisation code', () => {
      setBackendError({ badKey: true });
      expect(pageLevelError).toBeFalsy();
      expect(initialisationCodeError).toBeTruthy();
    });
  });

  describe('information messages', () => {
    function getInfoMessage(id: string): DebugElement {
      return fixture.debugElement.query(By.css(`#${id}`));
    }

    it('should not show any information message by default', () => {
      ['cancelMessage', 'leaveMessage', 'quitMessage'].forEach(messageId => {
        expect(getInfoMessage(messageId)).toBeFalsy();
      });
    });

    it('should show the proper message if the voting process was cancelled', () => {
      processCancellationService.cancelState = CancelState.CANCEL_VOTE;

      fixture.detectChanges();

      const cancelMessage = getInfoMessage('cancelMessage');
      expect(cancelMessage).toBeTruthy();
      expect(cancelMessage.nativeElement.textContent).toContain('startvoting.cancel');
    });

    it('should show the proper message if the voting process was left', () => {
      processCancellationService.cancelState = CancelState.LEAVE_PROCESS;

      Object.values(Identification).forEach(identification => {
        component.identification = identification;
        fixture.detectChanges();

        const leaveMessage = getInfoMessage('leaveMessage');
        expect(leaveMessage).toBeTruthy();
        expect(leaveMessage.nativeElement.textContent).toContain(`startvoting.leave.${identification}`);
      });
    });

    it('should show the proper message if the voting process was quit', () => {
      processCancellationService.cancelState = CancelState.QUIT;

      fixture.detectChanges();

      const quitMessage = getInfoMessage('quitMessage');
      expect(quitMessage).toBeTruthy();
      expect(quitMessage.nativeElement.textContent).toContain('startvoting.quittedsuccessfully');
    });
  });
});
