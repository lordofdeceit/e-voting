/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {NgbDatepickerModule} from '@ng-bootstrap/ng-bootstrap';
import {InputMaskModule} from '@ngneat/input-mask';
import {SharedIconsModule} from '@swiss-post/shared/icons';
import {SharedUiModule} from '@swiss-post/shared/ui';
import {StartVotingComponent} from './start-voting/start-voting.component';
import {StartVotingRoutingModule} from './start-voting-routing.module';
import {SharedStateModule} from '@swiss-post/shared/state';
import {TranslateModule} from '@ngx-translate/core';
import {IdentificationComponent} from './identification/identification.component';

@NgModule({
  imports: [
    CommonModule,
    StartVotingRoutingModule,
    SharedStateModule,
    ReactiveFormsModule,
    NgbDatepickerModule,
    InputMaskModule,
    SharedIconsModule,
    SharedUiModule,
    TranslateModule
  ],
  declarations: [StartVotingComponent, IdentificationComponent],
})
export class StartVotingModule {
}
