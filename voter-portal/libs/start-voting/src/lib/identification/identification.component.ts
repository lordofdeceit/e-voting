/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {Component, Input} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {createMask, InputmaskOptions} from '@ngneat/input-mask';
import {TranslateService} from '@ngx-translate/core';
import {SwpDateAdapter, SwpDateParserFormatter} from '@swiss-post/shared/ui';
import {Identification} from '@swiss-post/types';
import {startWith} from 'rxjs/operators';

@Component({
  selector: 'swp-identification',
  templateUrl: './identification.component.html',
  styleUrls: ['./identification.component.scss'],
  providers: [SwpDateAdapter, SwpDateParserFormatter]
})
export class IdentificationComponent {
  @Input() voterForm!: FormGroup;
  @Input() showErrors = false;
  @Input() identification: Identification | undefined;
  yearOfBirthMask = createMask('9999');
  dateOfBirthMask: InputmaskOptions<string>;

  readonly Identification = Identification;

  get dobOrYob(): FormControl {
    return this.voterForm.get('dobOrYob') as FormControl;
  }

  constructor(
    private readonly translate: TranslateService,
    private readonly dateAdapter: SwpDateAdapter,
    private readonly dateParserFormatter: SwpDateParserFormatter,
  ) {
    this.translate.onLangChange
      .pipe(
        startWith(this.translate.currentLang)
      )
      .subscribe(() => {
        this.dateOfBirthMask = createMask({
          alias: 'datetime',
          inputFormat: this.translate.instant('datepicker.placeholder'),
          prefillYear: false,
          parser: (value: string) => {
            const parsedValue = this.dateParserFormatter.parse(value);
            return this.dateAdapter.toModel(parsedValue);
          },
        });
      });
  }

  setDateOfBirth(date: NgbDateStruct) {
    this.dobOrYob.setValue(this.dateAdapter.toModel(date));
  }
}
