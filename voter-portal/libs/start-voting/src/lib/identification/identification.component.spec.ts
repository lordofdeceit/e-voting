/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {DebugElement} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import {By} from '@angular/platform-browser';
import {Identification} from '@swiss-post/types';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {IdentificationComponent} from './identification.component';
import {MockComponent, MockModule} from 'ng-mocks';
import {SharedIconsModule} from '@swiss-post/shared/icons';
import {NgbDatepickerModule} from '@ng-bootstrap/ng-bootstrap';
import {InputMaskModule} from '@ngneat/input-mask';
import {ClearableInputComponent} from '@swiss-post/shared/ui';
import {RandomString} from '@swiss-post/shared/testing';

describe('IdentificationComponent', () => {
  let component: IdentificationComponent;
  let fixture: ComponentFixture<IdentificationComponent>;
  let dateOfBirthInput: DebugElement;
  let yearOfBirthInput: DebugElement;

  function getContestFormGroup(): FormGroup {
    return new FormGroup({
      initializationCode: new FormControl('', [Validators.required]),
      dobOrYob: new FormControl('', [Validators.required]),
    });
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        TranslateTestingModule.withTranslations('fr', {
          datepicker: {placeholder: 'dd.mm.yyyy'},
        }).withDefaultLanguage('fr'),
        InputMaskModule,
        MockModule(SharedIconsModule),
        MockModule(NgbDatepickerModule),
      ],
      declarations: [
        IdentificationComponent,
        MockComponent(ClearableInputComponent),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IdentificationComponent);
    component = fixture.componentInstance;
    component.voterForm = getContestFormGroup();
    component.showErrors = false;
  });

  it('should not crash if there is no identification method', () => {
    component.identification = undefined;
    fixture.detectChanges();
    const divs = fixture.debugElement.queryAll(By.css('div'));
    expect(divs.length).toBe(0);
  });

  describe('date of birth identification cases', () => {
    const getDobRequiredAlert = () => {
      return fixture.debugElement.query(By.css('#dateOfBirth-required'));
    };

    beforeEach(() => {
      component.identification = Identification.DATE_OF_BIRTH;
      fixture.detectChanges();
      dateOfBirthInput = fixture.debugElement.query(By.css('#dateOfBirth'));
      yearOfBirthInput = fixture.debugElement.query(By.css('#yearOfBirth'));
    });

    it('should show date of birth identification', () => {
      expect(dateOfBirthInput).toBeTruthy();
    });

    it('should not show year of birth identification', () => {
      expect(yearOfBirthInput).toBeFalsy();
    });

    it('should not show validation error "dateOfBirth-required" if form is not submitted yet', () => {
      expect(getDobRequiredAlert()).toBeFalsy();
    });

    it('should show validation error "dateOfBirth-required" if form is submitted and invalid', () => {
      component.showErrors = true;
      fixture.detectChanges();
      expect(getDobRequiredAlert()).toBeTruthy();
    });

    it('should not show validation error "dateOfBirth-required" if form is submitted and valid', () => {
      component.showErrors = true;
      component.dobOrYob.setValue('01011975');

      fixture.detectChanges();
      expect(getDobRequiredAlert()).toBeFalsy();
    });

    it('should format day-of-birth properly', () => {
      const dateOfBirthInputElem = fixture.debugElement.query(
        By.css('#dateOfBirth')
      ).nativeElement;
      dateOfBirthInputElem.value = '05.01.1975';
      dateOfBirthInputElem.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      const formValue = component.dobOrYob.value;
      expect(formValue).toBe('05011975');
    });
  });

  describe('year of birth identification cases', () => {
    const getYobRequiredAlert = () => {
      return fixture.debugElement.query(By.css('#yearOfBirth-required'));
    };

    beforeEach(() => {
      component.identification = Identification.YEAR_OF_BIRTH;
      fixture.detectChanges();
      dateOfBirthInput = fixture.debugElement.query(By.css('#dateOfBirth'));
      yearOfBirthInput = fixture.debugElement.query(By.css('#yearOfBirth'));
    });

    it('should show year of birth identification', () => {
      expect(yearOfBirthInput).toBeTruthy();
    });

    it('should not show date of birth identification', () => {
      expect(dateOfBirthInput).toBeFalsy();
    });

    it('should not show validation error "yearOfBirth-required" if form is not submitted yet', () => {
      expect(getYobRequiredAlert()).toBeFalsy();
    });

    it('should show validation error "yearOfBirth-required" if form is submitted and invalid', () => {
      component.showErrors = true;
      fixture.detectChanges();
      expect(getYobRequiredAlert()).toBeTruthy();
    });

    it('should not show validation error "yearOfBirth-required" if form is submitted and valid', () => {
      component.showErrors = true;
      component.dobOrYob.setValue('1975');
      fixture.detectChanges();
      expect(getYobRequiredAlert()).toBeFalsy();
    });

    it('should format year-of-birth properly', () => {
      const yearOfBirthInputElem = fixture.debugElement.query(
        By.css('#yearOfBirth')
      ).nativeElement;
      const randomYearOfBirth = RandomString(4, '0123456789');
      yearOfBirthInputElem.value = randomYearOfBirth;
      yearOfBirthInputElem.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      const formValue = component.dobOrYob.value;
      expect(formValue).toBe(randomYearOfBirth);
    });
  });
});
