/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormArray, FormGroup} from '@angular/forms';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CandidateSelectionModalComponent} from '@swiss-post/candidate';
import { FAQService } from '@swiss-post/shared/ui';
import { ElectionContest, FAQSection, ICandidate, ICandidateUserData, IContest } from '@swiss-post/types';
import {Subscription} from 'rxjs';

@Component({
  selector: 'swp-choose-candidates',
  templateUrl: './choose-candidates.component.html',
  styleUrls: ['./choose-candidates.component.scss'],
})
export class ChooseCandidatesComponent implements OnInit, OnDestroy {
  @Input() contestFormGroup!: FormGroup;
  @Input() showErrors = false;
  electionContest!: ElectionContest;
  candidatesCumulMap!: Map<string, number>;
  unoccupiedSeat: FormGroup | undefined;
  private subscription!: Subscription;

  @Input() set contest(value: IContest | undefined) {
    if (value) {
      this.electionContest = new ElectionContest(value);
    }
  }

  get candidates(): FormArray {
    return this.contestFormGroup?.get('candidates') as FormArray;
  }

  get cumulationAllowed(): boolean {
    return !!this.electionContest.candidatesQuestion && this.electionContest.candidatesQuestion.cumul > 1;
  }

  constructor(
    private readonly modalService: NgbModal,
    private readonly fAQService: FAQService
  ) {
  }

  ngOnInit() {
    this.subscription = this.candidates.valueChanges.subscribe((candidatesUserData: ICandidateUserData[]) => {
      this.candidatesCumulMap = this.getCandidatesCumulMap(candidatesUserData);
      this.unoccupiedSeat = this.getUnoccupiedSeat();
    })
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  showWriteInsFAQ() {
    this.fAQService.showFAQ(FAQSection.HowToUseWriteIns);
  }

  getCandidateMaxCumul(candidate: ICandidate | null): number {
    return candidate?.allRepresentations?.length ?? 0;
  }

  openCandidateSelectionModal(candidateIndex: number): void {
    const modalOptions = {fullscreen: 'xl', size: 'xl'};
    const modalRef = this.modalService.open(CandidateSelectionModalComponent, modalOptions);

    Object.assign(modalRef.componentInstance, {
      contest: this.electionContest, contestUserData: this.contestFormGroup.value, candidateIndex
    });

    modalRef.result
      .then((selectedCandidate: ICandidate) => {
        this.getCandidateFormGroup(candidateIndex)?.setValue({
          candidateId: selectedCandidate.id,
          writeIn: null,
        })
      })
      .catch(() => null);
  }

  candidateCanCumulate(candidate: ICandidate) {
    const candidateCumul = this.candidatesCumulMap.get(candidate.id) ?? 0;
    const candidateMaxCumul = candidate.allRepresentations?.length ?? 0;
    return candidateCumul < candidateMaxCumul;
  }

  cumulateCandidate(candidate: ICandidate) {
    if (this.unoccupiedSeat) {
      this.unoccupiedSeat.get('candidateId')?.setValue(candidate.id);
    }
  }

  clearCandidate(candidateIndex: number): void {
    this.getCandidateFormGroup(candidateIndex)?.setValue({
      candidateId: null,
      writeIn: null,
    });
  }

  private getCandidateFormGroup(candidateIndex: number): FormGroup | undefined {
    return this.candidates?.controls[candidateIndex] as FormGroup;
  }

  private getUnoccupiedSeat(): FormGroup | undefined {
    return this.candidates.controls.find((control) => {
      return !control.value.candidateId
    }) as FormGroup;
  }

  private getCandidatesCumulMap(candidatesUserData: ICandidateUserData[]) {
    return candidatesUserData.reduce((cumulMap: Map<string, number>, {candidateId}: ICandidateUserData) => {
      if (candidateId) {
        const previousValue = cumulMap.get(candidateId) ?? 0;
        cumulMap.set(candidateId, previousValue + 1);
      }
      return cumulMap;
    }, new Map());
  }
}
