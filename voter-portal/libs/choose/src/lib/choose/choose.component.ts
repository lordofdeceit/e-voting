/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {
  getContests,
  getContestsAndContestsUserData,
} from '@swiss-post/shared/state';
import {
  ICandidatesQuestion,
  IContest,
  IQuestion,
  TemplateType,
} from '@swiss-post/types';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import * as Actions from '@swiss-post/shared/state';
import {ProcessCancellationService} from '@swiss-post/shared/ui';
import {take} from 'rxjs/operators';

@Component({
  selector: 'swp-choose',
  templateUrl: './choose.component.html',
  styleUrls: ['./choose.component.scss'],
})
export class ChooseComponent implements OnInit, OnDestroy {
  ballotFormGroup!: FormGroup;
  contests$ = this.store.select(getContests);

  constructor(
    private readonly store: Store,
    private readonly cancelProcessService: ProcessCancellationService,
    private readonly fb: FormBuilder
  ) {
  }

  ngOnInit() {
    this.store
      .select(getContestsAndContestsUserData)
      .pipe(take(1))
      .subscribe(({contests, contestsUserData}) => {
        this.ballotFormGroup = this.fb.group({
          contests: this.getContestsFormArray(contests),
        });

        if (contestsUserData) {
          this.ballotFormGroup.patchValue({contests: contestsUserData});
        }
      });
  }

  ngOnDestroy(): void {
    this.store.dispatch(
      Actions.saveForm({ballotUserData: this.ballotFormGroup.value})
    );
  }

  confirmCancel() {
    this.cancelProcessService.cancelVote();
  }

  review() {
    if (this.ballotFormGroup.invalid) {
      const firstInvalid = document.querySelector<HTMLInputElement>('input.ng-invalid');
      if (firstInvalid) {
        firstInvalid.focus();
      }
      return;
    }
    this.store.dispatch(
      Actions.review()
    );
  }

  getContestFormGroup(index: number): FormGroup {
    const contestFromArray = this.ballotFormGroup.get('contests') as FormArray;
    return contestFromArray.controls[index] as FormGroup;
  }

  private getContestsFormArray(contests: IContest[] | undefined): FormArray {
    const contestsFormGroups = contests?.map((contest) => {
      if (contest.template === TemplateType.OPTIONS) {
        return this.fb.group({
          questions: this.getOptionsFormArray(contest.questions),
        });
      }

      const listAndCandidatesFormGroup = this.fb.group({
        candidates: this.getCandidatesFormArray(contest.candidatesQuestion),
      });

      if (contest.listQuestion && contest.listQuestion.maxChoices > 0) {
        listAndCandidatesFormGroup.setControl('listId', this.fb.control(null));
      }

      return listAndCandidatesFormGroup;
    });

    return this.fb.array(contestsFormGroups || []);
  }

  private getCandidatesFormArray(
    candidateQuestions: ICandidatesQuestion | undefined
  ): FormArray {
    const candidateFormGroups = Array.from(
      {length: candidateQuestions?.maxChoices || 0},
      () => {
        return this.fb.group({
          candidateId: this.fb.control(null),
          writeIn: this.fb.control(null),
        });
      }
    );

    return this.fb.array(candidateFormGroups);
  }

  private getOptionsFormArray(questions: IQuestion[] | undefined): FormArray {
    const questionFormGroups = questions?.map((question) => {
      return this.fb.group({
        id: this.fb.control(question.id),
        chosenOption: this.fb.control(null),
      });
    });

    return this.fb.array(questionFormGroups ?? []);
  }
}
