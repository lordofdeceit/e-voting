/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {DebugElement} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {RouterTestingModule} from '@angular/router/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {IconComponent} from '@swiss-post/shared/icons';
import {SHARED_FEATURE_KEY} from '@swiss-post/shared/state';
import {MockContest, MockContestUserData, MockQuestions, RandomArray} from '@swiss-post/shared/testing';
import {ProcessCancellationService} from '@swiss-post/shared/ui';
import {IContest, IContestUserData} from '@swiss-post/types';
import {MockComponent, MockProvider} from 'ng-mocks';
import {ChooseContestContainerComponent} from '../choose-contest-container/choose-contest-container.component';
import {ChooseComponent} from './choose.component';
import {TranslateTestingModule} from 'ngx-translate-testing';

class MockState {
  ballot: { ballot: { contests: IContest[] } };
  ballotUserData: ({ contests: IContestUserData[] }) | undefined;

  constructor() {
    this.ballot = {ballot: {contests: []}};
  }
}

describe('ChooseComponent', () => {
  let fixture: ComponentFixture<ChooseComponent>;
  let store: MockStore;
  let currentState: MockState;
  let contests: IContest[];
  let contestsUserData: IContestUserData[];
  let contestContainerComponents: DebugElement[];

  function setContests(): void {
    contests = RandomArray(contestIndex => {
      const questionTexts = RandomArray(questionIndex => {
        return `Contest ${contestIndex} - Question ${questionIndex}`;
      }, 10, 2);

      return MockContest(MockQuestions(questionTexts));
    }, 5, 1);

    currentState.ballot.ballot.contests = contests;
    store.setState({[SHARED_FEATURE_KEY]: currentState});
  }

  function setContestsUserData(): void {
    contestsUserData = contests.map(contest => MockContestUserData(contest.questions ?? []));
    currentState.ballotUserData = {contests: contestsUserData};
    store.setState({[SHARED_FEATURE_KEY]: currentState});
  }

  beforeEach(async () => {
    currentState = new MockState();

    await TestBed.configureTestingModule({
      declarations: [
        ChooseComponent,
        MockComponent(ChooseContestContainerComponent),
        MockComponent(IconComponent),
      ],
      imports: [
        ReactiveFormsModule,
        TranslateTestingModule.withTranslations({}),
        RouterTestingModule,
      ],
      providers: [
        provideMockStore({initialState: {[SHARED_FEATURE_KEY]: currentState}}),
        MockProvider(ProcessCancellationService)
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    store = TestBed.inject(MockStore);
    fixture = TestBed.createComponent(ChooseComponent);
  });

  describe('without user data', () => {
    beforeEach(() => {
      setContests();
      fixture.detectChanges();

      contestContainerComponents = fixture.debugElement.queryAll(By.css('swp-choose-contest-container'));
    });

    it('should display as many contests as there are in the store', () => {
      expect(contestContainerComponents.length).toBe(contests.length);
    });

    it('should pass the proper contest to the contest container components', () => {
      contests.forEach((contest, i) => {
        expect(contestContainerComponents[i].componentInstance.contest).toBe(contest);
      });
    });

    it('should pass the proper contest from group to the contest container components', () => {
      contests.forEach((contest, i) => {
        const contestFormGroup = contestContainerComponents[i].componentInstance.contestFormGroup;
        expect(contestFormGroup.value.questions).toEqual(contest.questions?.map(({id}) => ({id, chosenOption: null})));
      });
    });
  });

  describe('with user data', () => {
    beforeEach(() => {
      setContests();
      setContestsUserData();
      fixture.detectChanges();

      contestContainerComponents = fixture.debugElement.queryAll(By.css('swp-choose-contest-container'));
    });

    it('should properly apply the contest user data to each contest form group', () => {
      contestsUserData.forEach((contestUserData, i) => {
        const contestFormGroup = contestContainerComponents[i].componentInstance.contestFormGroup;
        expect(contestFormGroup.value.questions).toEqual(contestUserData.questions);
      });
    });
  });
});
