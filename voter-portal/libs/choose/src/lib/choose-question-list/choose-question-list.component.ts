/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {Component, Input} from '@angular/core';
import {FormArray, FormGroup} from '@angular/forms';
import {IContest} from '@swiss-post/types';

@Component({
  selector: 'swp-choose-question-list',
  templateUrl: './choose-question-list.component.html',
  styleUrls: ['./choose-question-list.component.scss'],
})
export class ChooseQuestionListComponent {
  @Input() contest: IContest | undefined;
  @Input() contestFormGroup: FormGroup | undefined;

  getQuestionFormGroup(questionIndex: number): FormGroup | undefined {
    const contestQuestions = this.contestFormGroup?.get('questions') as FormArray;
    return contestQuestions?.controls[questionIndex] as FormGroup;
  }

  getChosenOption(questionIndex: number): string | undefined {
    return this.getQuestionFormGroup(questionIndex)?.get('chosenOption')?.value;
  }

  resetRadioButtons(questionIndex: number): void {
    this.getQuestionFormGroup(questionIndex)?.get('chosenOption')?.reset();

    const question = document.querySelectorAll('.question--options').item(questionIndex) as HTMLElement;
    const firstOption = question?.querySelectorAll('.question--option').item(0) as HTMLElement;
    firstOption.focus();
  }
}
