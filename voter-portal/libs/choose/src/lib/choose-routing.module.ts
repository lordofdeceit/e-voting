/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {NgModule} from '@angular/core';
import {ChooseComponent} from './choose/choose.component';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {path: "", component: ChooseComponent}
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChooseRoutingModule {
}
