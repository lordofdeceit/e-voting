/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListModule} from '@swiss-post/list';
import {SharedIconsModule} from '@swiss-post/shared/icons';
import {SharedUiModule} from '@swiss-post/shared/ui';
import {ChooseComponent} from './choose/choose.component';
import {ChooseRoutingModule} from './choose-routing.module';
import {SharedStateModule} from '@swiss-post/shared/state';
import {QuestionsModule} from '@swiss-post/questions';
import {ChooseContestContainerComponent} from './choose-contest-container/choose-contest-container.component';
import {NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {ChooseQuestionListComponent} from './choose-question-list/choose-question-list.component';
import {ChooseCandidatesComponent} from './choose-candidates/choose-candidates.component';
import {CandidateModule} from '@swiss-post/candidate';
import {ChooseListComponent} from './choose-list/choose-list.component';

@NgModule({
  imports: [
    CommonModule,
    ChooseRoutingModule,
    ReactiveFormsModule,
    TranslateModule,
    NgbAlertModule,
    SharedStateModule,
    SharedUiModule,
    SharedIconsModule,
    QuestionsModule,
    CandidateModule,
    ListModule,
  ],
  declarations: [
    ChooseComponent,
    ChooseContestContainerComponent,
    ChooseQuestionListComponent,
    ChooseCandidatesComponent,
    ChooseListComponent,
  ],
  exports: [ChooseListComponent],
})
export class ChooseModule {
}
