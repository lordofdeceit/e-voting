/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {Component, Input} from '@angular/core';
import {FormArray, FormControl, FormGroup} from '@angular/forms';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ConfirmationService} from '@swiss-post/shared/ui';
import {
  ConfirmationModalConfig,
  ElectionContest,
  ICandidate,
  ICandidateList,
  IContestUserData,
} from '@swiss-post/types';
import {EMPTY, Observable, of} from 'rxjs';
import {fromPromise} from 'rxjs/internal-compatibility';
import {catchError, filter, map, switchMap} from 'rxjs/operators';
import {ListSelectionModalComponent} from '@swiss-post/list';

@Component({
  selector: 'swp-choose-list',
  templateUrl: './choose-list.component.html',
  styleUrls: ['./choose-list.component.scss'],
})
export class ChooseListComponent {
  @Input() electionContest!: ElectionContest;
  @Input() contestFormGroup!: FormGroup;

  get listId(): FormControl {
    return this.contestFormGroup.get('listId') as FormControl;
  }

  get candidates(): FormArray {
    return this.contestFormGroup.get('candidates') as FormArray;
  }

  get contestUserData(): IContestUserData {
    return this.contestFormGroup.value as IContestUserData;
  }

  constructor(
    private readonly modalService: NgbModal,
    private readonly confirmationService: ConfirmationService,
  ) {
  }

  openListSelectionModal() {
    const modalOptions = {fullscreen: 'xl', size: 'xl'};
    const modalRef = this.modalService.open(ListSelectionModalComponent, modalOptions);
    modalRef.componentInstance.electionContest = this.electionContest;

    const hasListOrCandidatesSelection =
      !!this.contestUserData.listId ||
      this.contestUserData.candidates?.some(
        (candidate) => !!candidate.candidateId
      );

    fromPromise(modalRef.result)
      .pipe(
        switchMap((selectedList) =>
          !hasListOrCandidatesSelection
            ? of(selectedList)
            : this.confirmListChange(selectedList)
        ),
        catchError(() => EMPTY),
        filter((selectedList): selectedList is ICandidateList => !!selectedList)
      )
      .subscribe((selectedList) => {
        this.listId.setValue(selectedList.id);

        const orderedCandidates: (ICandidate | null)[] = Array.from(
          {length: this.candidates.controls.length},
          () => null
        );

        selectedList.candidates.forEach((candidate) => {
          const positions = candidate.details.candidateType_positionOnList?.split(',') ?? [];
          positions.forEach((position) => {
            orderedCandidates[parseInt(position) - 1] = candidate;
          });
        });

        orderedCandidates.forEach((candidate, i) => {
          const candidateSeat = this.candidates.controls[i].get('candidateId');
          candidateSeat?.setValue(candidate?.id);
        });
      });
  }

  clearList() {
    const deletionModalConfig: ConfirmationModalConfig = {
      content: 'listandcandidates.clearlistquestion.text',
      confirmLabel: 'listandcandidates.clearlistquestion.yes',
    };

    this.confirmationService.confirm(deletionModalConfig)
      .pipe(
        filter((wasDeletionConfirmed) => wasDeletionConfirmed)
      )
      .subscribe(() => {
        this.listId.setValue(null);
        this.candidates.controls.forEach((candidateFormGroup) => {
          const candidateIdControl = candidateFormGroup.get('candidateId') as FormControl;
          candidateIdControl.setValue(null);
        });
      });
  }

  private confirmListChange(newList: ICandidateList): Observable<ICandidateList | null> {
    const changeModalConfig = {
      content: 'listandcandidates.changelistquestion.text',
      confirmLabel: 'listandcandidates.changelistquestion.yes',
    };

    return this.confirmationService.confirm(changeModalConfig)
      .pipe(
        map((wasChangeConfirmed) => wasChangeConfirmed ? newList : null)
      );
  }
}
