/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {DebugElement} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {Route} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {RandomArray, RandomBetween, RandomItem} from '@swiss-post/shared/testing';
import { Nullable } from '@swiss-post/types';
import {TranslateTestingModule} from 'ngx-translate-testing';
import { of } from 'rxjs';
import {routes} from '../app-routing.module';
import {StepperComponent} from './stepper.component';

// Mock app routes
jest.mock('../app-routing.module', () => {
  const RandomRoute = (routeIndex: number, noStepNameProbability = 0.2) => ({
    data: {stepKey: RandomBetween(`Step ${routeIndex}`, null, noStepNameProbability)},
  });

  let routes = RandomArray(RandomRoute, 10, 1);

  // Make sure there is at least one route with a step name
  const routesWithStepName = routes.filter((route) => !!route.data?.stepKey);
  if (!routesWithStepName.length) {
    routes = [...routes, RandomRoute(routes.length, 0)];
  }

  return {routes};
});

describe('stepperComponent', () => {
  let component: StepperComponent;
  let fixture: ComponentFixture<StepperComponent>;
  let stepper: DebugElement;
  let currentStep: Nullable<Route>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateTestingModule.withTranslations({}),
      ],
      declarations: [
        StepperComponent,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StepperComponent);
    component = fixture.componentInstance;
  });

  function setCurrentStep(step: object | null) {
    currentStep = step;
    component.currentStep$ = of(step);
    fixture.detectChanges();
    stepper = fixture.debugElement.query(By.css('#voting-progress-stepper'));
  }

  describe('stepper not rendered', () => {
    it('should not show the stepper if there is no current step', () => {
      setCurrentStep(null);

      expect(stepper).toBeFalsy();
    });

    it('should not show the stepper if the current step has no step name', () => {
      setCurrentStep({data: {stepKey: null}});

      expect(stepper).toBeFalsy();
    });
  });


  describe('stepper rendered', () => {
    let steps: DebugElement[];
    let routesWithStepName: Route[];

    beforeEach(() => {
      setCurrentStep(RandomItem(routes, (route) => !!route.data?.stepKey));
      steps = stepper.queryAll(By.css('.stepper-item'));
      routesWithStepName = routes.filter((route) => !!route.data?.stepKey);
    });

    it('should show the stepper if the current step has a step name', () => {
      expect(stepper).toBeTruthy();
    });

    it('should show all the steps that have a step name', () => {
      routesWithStepName.forEach((route, i) => {
        expect(steps[i].nativeElement.textContent).toContain(route.data?.stepKey);
      });
    });

    it('should add an "aria-current" attribute to the current step only', () => {
      routesWithStepName.forEach((route, i) => {
        const expectedAriaCurrent = route.data?.stepKey === currentStep?.data?.stepKey ? 'step' : null;
        expect(steps[i].nativeElement.getAttribute('aria-current')).toBe(expectedAriaCurrent);
      });
    });
  });
});
