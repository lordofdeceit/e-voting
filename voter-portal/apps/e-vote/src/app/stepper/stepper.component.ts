/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */

import { Component, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Route, Router } from '@angular/router';
import { concatLatestFrom } from '@ngrx/effects';
import { TranslateService } from '@ngx-translate/core';
import { Nullable } from "@swiss-post/types";
import { Observable, Subscription } from 'rxjs';
import { filter, map, switchMap, tap } from 'rxjs/operators';
import { routes } from '../app-routing.module';

@Component({
  selector: 'swp-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements OnInit, OnDestroy {
  steps!: Route[];
  currentStep$: Observable<Nullable<Route>>;
  stepNameSubscription!: Subscription;

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly translate: TranslateService,
    private readonly titleService: Title
  ) {
    this.steps = routes.filter(this.getStepKey);

    this.currentStep$ = this.router.events.pipe(
      filter(routerEvent => routerEvent instanceof NavigationEnd),
      map(() => this.activatedRoute.snapshot.firstChild?.routeConfig),
      tap(() => this.setFirstFocus())
    );
  }

  ngOnInit() {
    this.stepNameSubscription = this.currentStep$.pipe(
      map(currentStep => this.getStepKey(currentStep)),
      switchMap(currentStepKey => this.translate.stream(currentStepKey ?? '')),
      concatLatestFrom(() => this.translate.get('common.pageTitle')),
    ).subscribe(([stepName, pageTitle]) => {
      this.titleService.setTitle(pageTitle + (stepName ? ` - ${stepName}` : ''));
    });
  }

  ngOnDestroy() {
    this.stepNameSubscription.unsubscribe();
  }

  getStepKey(route: Nullable<Route>): string | undefined {
    return route?.data?.stepKey;
  }

  private setFirstFocus(): void {
    window.requestAnimationFrame(() => {
      const stepper = document.querySelector<HTMLElement>('#voting-progress-stepper [aria-current="step"]');

      if (stepper) {
        stepper.focus();
      }
    });
  }
}
