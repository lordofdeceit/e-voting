/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {DebugElement} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {By, Title} from '@angular/platform-browser';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {TranslateService} from '@ngx-translate/core';
import {environment} from '@swiss-post/backend';
import {IconComponent} from '@swiss-post/shared/icons';
import * as Actions from '@swiss-post/shared/state';
import {SHARED_FEATURE_KEY} from '@swiss-post/shared/state';
import {RandomArray, RandomItem} from '@swiss-post/shared/testing';
import {FAQService} from '@swiss-post/shared/ui';
import {Language} from '@swiss-post/types';
import {MockComponent, MockProvider} from 'ng-mocks';
import {TranslateTestingModule} from 'ngx-translate-testing';

import {HeaderComponent} from './header.component';

// Mock environment configuration
jest.mock('@swiss-post/backend');
environment.availableLanguages = RandomArray((i) => {
  return {id: `lang-${i}`, label: `Language ${i}`};
}, 6, 3);

describe('HeaderComponent', () => {
  let fixture: ComponentFixture<HeaderComponent>;

  const defaultLanguage: Language = RandomItem(environment.availableLanguages);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule
          .withTranslations({})
          .withDefaultLanguage(defaultLanguage.id)
      ],
      declarations: [HeaderComponent, MockComponent(IconComponent)],
      providers: [
        provideMockStore({initialState: {}}),
        MockProvider(FAQService),
        MockProvider(Title),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    fixture.detectChanges();
  });

  describe('help', () => {
    let faqService: FAQService;

    beforeEach(() => {
      faqService = TestBed.inject(FAQService);
    });

    it('should open the FAQ with no specific section opened when the help button is clicked', () => {
      const showFAQSpy = jest.spyOn(faqService, 'showFAQ');
      const helpButton: HTMLButtonElement = fixture.debugElement.query(By.css('#mainHelpButton')).nativeElement;

      helpButton.click();

      expect(showFAQSpy).toHaveBeenNthCalledWith(1/*, called without any arguments*/);
    });
  });

  describe('languages', () => {
    let store: MockStore;
    let translate: TranslateService;
    let langageOptions: DebugElement[];
    let newLanguage: Language;

    beforeEach(() => {
      store = TestBed.inject(MockStore);
      translate = TestBed.inject(TranslateService);

      langageOptions = fixture.debugElement.queryAll(By.css('.language-selector > li'));

      newLanguage = RandomItem(environment.availableLanguages, (language) => {
        return language.id !== defaultLanguage.id;
      });
    });

    function setConfigLanguage(language?: Language) {
      store.setState({[SHARED_FEATURE_KEY]: {config: {lang: language?.id}}});
    }

    it('should show as all languages configured in the environment', () => {
      expect(langageOptions.length).toBe(environment.availableLanguages.length);
      environment.availableLanguages.forEach((language, i) => {
        expect(langageOptions[i].nativeElement.textContent).toContain(language.label);
      });
    });

    it('should show the default language as active if there is no language in the current config', () => {
      setConfigLanguage(undefined);

      fixture.detectChanges();

      const activeLanguage = fixture.debugElement.query(By.css('.language-selector .active'));
      expect(activeLanguage.nativeElement.textContent).toContain(defaultLanguage.label);
    });

    it('should show the language from the config as active if there is one', () => {
      setConfigLanguage(newLanguage);

      fixture.detectChanges();

      const activeLanguage = fixture.debugElement.query(By.css('.language-selector .active'));
      expect(activeLanguage.nativeElement.textContent).toContain(newLanguage.label);
    });

    describe('language change', () => {
      let newLanguageButton: HTMLButtonElement;

      beforeEach(() => {
        const newLanguageIndex = environment.availableLanguages.findIndex((language) => {
          return language.id === newLanguage.id;
        });

        newLanguageButton = langageOptions[newLanguageIndex].query(By.css('button')).nativeElement;
      });

      it('should dispatch a language change action to the store', () => {
        const dispatchSpy = jest.spyOn(store, 'dispatch');

        newLanguageButton.click();

        expect(dispatchSpy).toHaveBeenNthCalledWith(1, Actions.setLanguage({lang: newLanguage.id}));
      });

      it('should display the selected language as active', () => {
        setConfigLanguage(newLanguage);

        fixture.detectChanges();

        expect(newLanguageButton.classList).toContain('active');
      });

      it('should update the langage used by the translate service', () => {
        const translateUseSpy = jest.spyOn(translate, 'use');

        setConfigLanguage(newLanguage);

        expect(translateUseSpy).toHaveBeenNthCalledWith(1, newLanguage.id);
      });

      it('should update the document langage', () => {
        setConfigLanguage(newLanguage);

        expect(document.documentElement.lang).toBe(newLanguage.id);
      });
    });
  });
});
