/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */

import {Component} from '@angular/core';
import {Store} from '@ngrx/store';
import * as Actions from '@swiss-post/shared/state';
import {TranslateService} from '@ngx-translate/core';
import {environment} from '@swiss-post/backend';
import {getConfig} from '@swiss-post/shared/state';
import {Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {FAQService} from '@swiss-post/shared/ui';

@Component({
  selector: 'swp-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  currentLang$: Observable<string>;
  availableLanguages = environment.availableLanguages;

  constructor(
    private readonly store: Store,
    private readonly translate: TranslateService,
    private readonly faqService: FAQService
  ) {
    this.currentLang$ = this.store
      .select(getConfig)
      .pipe(
        map(config => config?.lang || translate.defaultLang),
        tap(lang => {
          translate.use(lang);
          document.documentElement.lang = lang;
        })
      );
  }

  translateApp(lang: string) {
    this.store.dispatch(Actions.setLanguage({lang}));
  }

  showFAQ(): void {
    this.faqService.showFAQ();
  }
}
