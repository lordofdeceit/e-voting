/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {Component} from '@angular/core';
import {PlaceholderService} from '@swiss-post/shared/ui';

@Component({
  selector: 'swp-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss'],
})
export class PageNotFoundComponent {
  constructor(
    public readonly placeholders: PlaceholderService
  ) {
  }
}
