/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {PlaceholderService} from '@swiss-post/shared/ui';
import {MockProvider} from 'ng-mocks';
import {TranslateTestingModule} from 'ngx-translate-testing';

import {PageNotFoundComponent} from './page-not-found.component';

describe('PageNotFoundComponent', () => {
  let fixture: ComponentFixture<PageNotFoundComponent>;
  let placeholderService: PlaceholderService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TranslateTestingModule.withTranslations({})],
      declarations: [PageNotFoundComponent],
      providers: [MockProvider(PlaceholderService)]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageNotFoundComponent);
    placeholderService = TestBed.inject(PlaceholderService);
    fixture.detectChanges();
  });

  it('should the configured placeholders', () => {
    const currentValueGetterSpy = jest.spyOn(placeholderService, 'currentValue', 'get');

    fixture.detectChanges();

    expect(currentValueGetterSpy).toHaveBeenCalled();
  });

  it('should show message incorrecturl', () => {
    expect(fixture.nativeElement.textContent).toContain(
      'pagenotfound.incorrecturl'
    );
  });
});
