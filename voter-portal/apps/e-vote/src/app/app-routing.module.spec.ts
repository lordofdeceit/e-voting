/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {Location} from '@angular/common';
import {fakeAsync, TestBed, tick} from '@angular/core/testing';
import {Router} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {ChooseModule} from '@swiss-post/choose';
import {ConfirmModule} from '@swiss-post/confirm';
import {BackendGuard, ElectionEventIdGuard} from '@swiss-post/guards';
import {LegalTermsModule} from '@swiss-post/legal-terms';
import {ReviewSendModule} from '@swiss-post/review-send';
import {RandomElectionEventId} from '@swiss-post/shared/testing';
import {StartVotingModule} from '@swiss-post/start-voting';
import {VerifyModule} from '@swiss-post/verify';
import {MockModule, MockProvider} from 'ng-mocks';
import {of} from 'rxjs';

import {routes} from './app-routing.module';

describe('AppRoutingModule', () => {
  let router: Router;
  let location: Location;
  let lastLoadedModule: string;

  // List of all modules that can be lazy loaded by the router
  const lazyLoadedModules = [
    {module: LegalTermsModule, source: '@swiss-post/legal-terms'},
    {module: StartVotingModule, source: '@swiss-post/start-voting'},
    {module: ChooseModule, source: '@swiss-post/choose'},
    {module: ReviewSendModule, source: '@swiss-post/review-send'},
    {module: VerifyModule, source: '@swiss-post/verify'},
    {module: ConfirmModule, source: '@swiss-post/confirm'},
  ];

  // Create a mock for each lazy loaded modules
  lazyLoadedModules.forEach(({module, source}) => {
    jest.mock(source, () => {
      lastLoadedModule = module.name;
      return {[module.name]: MockModule(module)};
    });
  });

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(routes),
      ],
      providers: [
        MockProvider(BackendGuard, {canLoad: () => of(true)}),
        MockProvider(ElectionEventIdGuard, {canActivate: () => true}),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    router = TestBed.inject(Router);
    location = TestBed.inject(Location);
    router.initialNavigation();
  });

  it('should redirect to the page not found without loading any module if no route is set', () => {
    expect(location.path()).toBe('/page-not-found');
    expect(lastLoadedModule).toBeFalsy();
  });

  it('should redirect to the page not found without loading any module when an invalid route is set', fakeAsync(() => {
    router.navigate(['/invalid-path']);

    tick();

    expect(location.path()).toBe('/page-not-found');
    expect(lastLoadedModule).toBeFalsy();
  }));

  it('should redirect to the page not found without loading any module when trying to reach legal terms without an election event id', fakeAsync(() => {
    router.navigate(['/legal-terms']);

    tick();

    expect(location.path()).toBe('/page-not-found');
    expect(lastLoadedModule).toBeFalsy();
  }));

  it('should correctly load the LegalTermsModule when an election event id is provided', fakeAsync(() => {
    const electionEventId = RandomElectionEventId();
    router.navigate(['/legal-terms', electionEventId]);

    tick();

    expect(location.path()).toBe(`/legal-terms/${electionEventId}`);
    expect(lastLoadedModule).toBe(LegalTermsModule.name);
  }));

  const otherRoutes = [
    {route: '/start-voting', expectedModule: StartVotingModule},
    {route: '/choose', expectedModule: ChooseModule},
    {route: '/review', expectedModule: ReviewSendModule},
    {route: '/verify', expectedModule: VerifyModule},
    {route: '/confirm', expectedModule: ConfirmModule},
  ];

  otherRoutes.forEach(({route, expectedModule}) => {
    it(`should correctly load the ${expectedModule.name}`, fakeAsync(() => {
      router.navigate([route]);

      tick();

      expect(location.path()).toBe(route);
      expect(lastLoadedModule).toBe(expectedModule.name);
    }));
  });
});
