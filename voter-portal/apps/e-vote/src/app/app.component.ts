/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {LocationStrategy} from '@angular/common';
import {Component, HostListener, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {TranslateService} from '@ngx-translate/core';
import * as Actions from '@swiss-post/shared/state';
import {ProcessCancellationService} from '@swiss-post/shared/ui';

declare global {
  interface Window {
    Cypress?: object;
    store?: Store;
  }
}

@Component({
  selector: 'swp-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(
    private readonly store: Store,
    private readonly translate: TranslateService,
    private readonly cancellationService: ProcessCancellationService,
    private readonly location: LocationStrategy
  ) {
  }

  ngOnInit() {
    if (window.Cypress) {
      window.store = this.store;
    }

    this.location.onPopState(() => {
      this.cancellationService.backButtonPressed = true;
      return false;
    });
  }

  @HostListener('window:beforeunload', ['$event']) beforeUnload($event: BeforeUnloadEvent) {
    $event.returnValue = this.translate.instant('common.questionnavigateaway');
    this.store.dispatch(Actions.logout());
  }
}
