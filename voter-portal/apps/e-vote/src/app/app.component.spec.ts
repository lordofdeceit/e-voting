/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {LocationStrategy} from '@angular/common';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {Store} from '@ngrx/store';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import * as Actions from '@swiss-post/shared/state';
import {ProcessCancellationService} from '@swiss-post/shared/ui';
import {MockComponent, MockProvider} from 'ng-mocks';
import {TranslateTestingModule} from 'ngx-translate-testing';

import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {StepperComponent} from './stepper/stepper.component';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let app: AppComponent;
  let store: Store;
  let popStateCallback: () => void;

  beforeEach(async () => {
    const onPopState = jest.fn().mockImplementation((callback) => popStateCallback = callback);

    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateTestingModule.withTranslations({}),
      ],
      declarations: [
        AppComponent,
        MockComponent(HeaderComponent),
        MockComponent(StepperComponent),
      ],
      providers: [
        provideMockStore({}),
        MockProvider(ProcessCancellationService),
        MockProvider(LocationStrategy, {onPopState}),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
    store = TestBed.inject(MockStore);
  });

  describe('store registration', () => {
    it('should not register a store in the window object if "Cypress" is undefined', () => {
      window.Cypress = undefined;

      app.ngOnInit();

      expect(window.store).toBeFalsy();
    });

    it('should register the store in the window object if "Cypress" is defined', () => {
      window.Cypress = {};

      app.ngOnInit();

      expect(window.store).toEqual(store);
    });
  });

  describe('route exit handling', () => {
    let cancellationService: ProcessCancellationService;

    beforeEach(() => {
      cancellationService = TestBed.inject(ProcessCancellationService);
      app.ngOnInit();
    });

    it('should mark the back button as pressed with every new "popstate" event', () => {
      expect(cancellationService.backButtonPressed).toBeFalsy();

      popStateCallback();

      expect(cancellationService.backButtonPressed).toBeTruthy();
    });

    it('should dispatch a logout action with every new "beforeunload" event', () => {
      const dispatchSpy = jest.spyOn(store, 'dispatch');

      window.dispatchEvent(new Event('beforeunload'));

      expect(dispatchSpy).toHaveBeenNthCalledWith(1, Actions.logout());
    });
  });
});
