/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {SharedIconsModule} from '@swiss-post/shared/icons';
import {
  SwpDateAdapter,
  SwpDateParserFormatter,
  SwpDatepickerI18n,
} from '@swiss-post/shared/ui';
import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '@swiss-post/backend';
import {
  NgbDateAdapter,
  NgbDateParserFormatter,
  NgbDatepickerI18n
} from '@ng-bootstrap/ng-bootstrap';
import {HeaderComponent} from './header/header.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {metaReducers} from '@swiss-post/shared/state';
import {registerLocaleData} from '@angular/common';
import localeDeCH from '@angular/common/locales/de-CH';
import localeItCH from '@angular/common/locales/it-CH';
import localeFrCH from '@angular/common/locales/fr-CH';
import localeRm from '@angular/common/locales/rm';
import localeEnGB from '@angular/common/locales/en-GB';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {ConfigurationService} from '@swiss-post/shared/configuration';
import {StepperComponent} from './stepper/stepper.component';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';

registerLocaleData(localeDeCH, 'de');
registerLocaleData(localeItCH, 'it');
registerLocaleData(localeFrCH, 'fr');
registerLocaleData(localeRm, 'rm');
registerLocaleData(localeEnGB, 'en');

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    StepperComponent,
    PageNotFoundComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot(
      {},
      {
        metaReducers,
        runtimeChecks: {
          strictActionImmutability: true,
          strictStateImmutability: true,
        },
      }
    ),
    EffectsModule.forRoot([]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    HttpClientModule,
    TranslateModule.forRoot({
      defaultLanguage:
        environment.availableLanguages
          .map((l) => l.id)
          .find((l) => navigator.language.includes(l)) || 'de',
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    SharedIconsModule,
  ],
  providers: [
    // Dynamic environment configurations
    {
      provide: APP_INITIALIZER,
      useFactory: (configService: ConfigurationService) => () =>
        configService.configuration$ // Ensure that configurations were initialized.
          .toPromise(),
      deps: [ConfigurationService],
      multi: true,
    },
    {provide: NgbDateAdapter, useClass: SwpDateAdapter},
    {provide: NgbDateParserFormatter, useClass: SwpDateParserFormatter},
    {provide: NgbDatepickerI18n, useClass: SwpDatepickerI18n},
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
