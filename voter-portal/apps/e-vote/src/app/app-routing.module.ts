/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {
  BackButtonGuard,
  BackendGuard,
  ElectionEventIdGuard,
} from '@swiss-post/guards';
import {BackAction, RouteData} from '@swiss-post/types';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';

export const routes: Routes = [
  {
    path: 'legal-terms/:electionEventId',
    data: {
      stepKey: 'stepper.termsAndConditions',
      allowedBackPaths: ['start-voting'],
    } as RouteData,
    canActivate: [ElectionEventIdGuard],
    canDeactivate: [BackButtonGuard],
    loadChildren: () =>
      import('@swiss-post/legal-terms').then((m) => m.LegalTermsModule),
  },
  {
    path: 'start-voting',
    data: {
      stepKey: 'stepper.startVoting',
      allowedBackPaths: ['legal-terms'],
      backAction: BackAction.GoToLegalTermsPage,
    } as RouteData,
    canDeactivate: [BackButtonGuard],
    loadChildren: () =>
      import('@swiss-post/start-voting').then((m) => m.StartVotingModule),
  },
  {
    path: 'choose',
    data: {
      stepKey: 'stepper.selectAnswers',
      allowedBackPaths: ['review'],
      backAction: BackAction.ShowCancelVoteDialog,
    } as RouteData,
    canLoad: [BackendGuard],
    canDeactivate: [BackButtonGuard],
    loadChildren: () =>
      import('@swiss-post/choose').then((m) => m.ChooseModule),
  },
  {
    path: 'review',
    data: {
      stepKey: 'stepper.reviewAndSeal',
      allowedBackPaths: ['choose'],
    } as RouteData,
    canLoad: [BackendGuard],
    canDeactivate: [BackButtonGuard],
    loadChildren: () =>
      import('@swiss-post/review-send').then((m) => m.ReviewSendModule),
  },
  {
    path: 'verify',
    data: {
      stepKey: 'stepper.verifyAndCast',
      allowedBackPaths: [],
      backAction: BackAction.ShowLeaveProcessDialog,
    } as RouteData,
    canLoad: [BackendGuard],
    canDeactivate: [BackButtonGuard],
    loadChildren: () =>
      import('@swiss-post/verify').then((m) => m.VerifyModule),
  },
  {
    path: 'confirm',
    data: {
      stepKey: 'stepper.voteCast',
      allowedBackPaths: [],
      backAction: BackAction.GoToStartVotingPage,
    } as RouteData,
    canLoad: [BackendGuard],
    canDeactivate: [BackButtonGuard],
    loadChildren: () =>
      import('@swiss-post/confirm').then((m) => m.ConfirmModule),
  },
  {
    path: 'page-not-found',
    component: PageNotFoundComponent
  },
  {path: '**', redirectTo: 'page-not-found'},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      useHash: true,
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
