/*
 * (c) Copyright 2022 Swiss Post Ltd.
 */
package ch.post.it.evoting.commandmessaging;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface CommandRepository extends CrudRepository<Command, CommandId> {

	boolean existsByContextIdAndContext(String contextId, String context);

	@Transactional(isolation = Isolation.SERIALIZABLE)
	List<Command> findAllByContextIdAndContextAndNodeId(String contextId, String context, Integer nodeId);

	@Transactional(isolation = Isolation.SERIALIZABLE)
	List<Command> findAllByCorrelationId(String correlationId);

	@Transactional(isolation = Isolation.SERIALIZABLE)
	List<Command> findAllByCorrelationIdAndResponsePayloadIsNotNull(String correlationId);

	@Transactional(isolation = Isolation.SERIALIZABLE)
	Integer countByCorrelationIdAndResponsePayloadIsNotNull(String correlationId);
}
