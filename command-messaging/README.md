# Command Messaging Library

The command-messaging library is a small library for defining the structure of a
[Command](https://www.enterpriseintegrationpatterns.com/patterns/messaging/CommandMessage.html)
sent between the Voting Server and Control Components. Additionally, it contains a service and repository. Commands, their unique identification (
through a combination of
**context and contextid**) and their persistence should be clearly coordinated between voting server and control components. The importance of this
coordination is the reason for the shared library's existence. This avoids splitting its contents (although small) three ways, partially duplicated
over the domain library plus the voting server and control components.

# Usage

One can use the command-messaging as a library in other components.

# Development

Check the build instructions in the readme of the repository 'evoting' for compiling the components.