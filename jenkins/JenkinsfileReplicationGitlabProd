#!/bin/bash +x
/*
 * (c) Copyright 2022 Swiss Post Ltd.
 *
 */
@Library(['pipeline-library@master', 'evoting-pipeline@master']) _

replicationConfig = [
		[
            sourceBranch: 'publication-master',
            targetBranch: 'master',
            remoteURL   : 'https://gitlab.com/swisspost-evoting/e-voting/e-voting.git'
    ],
    [
            sourceBranch: 'publication-develop',
            targetBranch: 'develop',
            remoteURL   : 'https://gitlab.com/swisspost-evoting/e-voting/e-voting.git'
    ],
]

credentialsIdGitit = 's-cicd-evoting'
credentialsIdGitlab = 'GITLAB_CRED_ID'
currentJenkinsFileName = '"JenkinsfileReplicationGitlabInt" "JenkinsfileReplicationGitlabProd"'
webhookUrl = null
def THREE_VERSION_REGEXP = '(\\d+)?.(\\d+)?.(\\d+)'

pipeline {

	agent {
		label 'apps-slaves-evoting'
	}

  parameters {
    booleanParam(name: 'ADD_THREE_VERSION_TAG', defaultValue: false, description: 'Add an additional tag containing only a three-digit version [ex: project-1.0.0]')
  }

	stages {
		stage('Ask password') {
			steps {
				script {
					if (env.BRANCH_NAME == 'publication-develop' || env.BRANCH_NAME == 'publication-master' || env.BRANCH_NAME == 'test') {
						def askpass = input(
								message: 'Please enter the password',
								parameters: [
										password(defaultValue: '',
												description: '',
												name: 'password')],
								submitterParameter: 'submitter')
						usernameVariable = askpass.submitter
						passwordVariable = askpass.password
						usernameVariable = 'USERPROD'
					} else {
						echo "No password required for integration"
					}
				}
			}
		}

		stage('Get source code') {
			when {
				beforeAgent true;
				expression { replicationConfig.any { it.sourceBranch == env.BRANCH_NAME } }
			}
			steps {
				cleanWs()
				clone(
						branch: env.BRANCH_NAME,
						url: GIT_URL,
						credentialsId: credentialsIdGitit,
						repositoryPath: 'work')
				dir('work') {
					sh "git tag -l"
				}
			}
		}

		stage('Publish on Gitlab prod') {
			environment {
				https_proxy = 'outappl.pnet.ch:3128'
				http_proxy = 'outappl.pnet.ch:3128'
				no_proxy = '.pnet.ch'
				proxyHost = 'outappl.pnet.ch'
				proxyPort = '3128'
			}
			when {
				beforeAgent true;
				expression { replicationConfig.any { it.sourceBranch == env.BRANCH_NAME } }
				anyOf {
					branch 'publication-develop'
					branch 'publication-master'
				}
			}
			steps {
				script {
					wrap([$class: 'MaskPasswordsBuildWrapper', varPasswordPairs: [[password: "${passwordVariable}", var: 'passwordVariable']]]) {
						dir('work') {

              def PROJECT_VERSION = readMavenPom(file: 'pom.xml').version
              def THREE_VERSION = (PROJECT_VERSION =~ THREE_VERSION_REGEXP)[0][0]

              def PROJECT_PATH = "${env.JOB_NAME.substring(0, env.JOB_NAME.lastIndexOf('/'))}"
              def PROJECT_NAME = "${PROJECT_PATH.substring(PROJECT_PATH.lastIndexOf('/') + 1, PROJECT_PATH.length())}"

              if (params.ADD_THREE_VERSION_TAG) {
                  sh "git tag -af ${PROJECT_NAME}-${THREE_VERSION} -m 'tag by jenkins ci'"
              }

							replicationConfig
									.findAll { it.sourceBranch == env.BRANCH_NAME }
									.each { config ->
										sh """
                                       git filter-branch -f --env-filter \
                                       '' --index-filter 'git rm -r --cached --ignore-unmatch ${currentJenkinsFileName}' \
                                       --tag-name-filter cat -- --all 
                                       """
										forcePushWithUserPass(
												branch: config.targetBranch,
												url: config.remoteURL,
												usernameVariable: usernameVariable,
												passwordVariable: passwordVariable
										)
									}
						}
					}
				}
				notifyWebhook(webhookUrl, 'OK')
			}
		}
	}
	post {
		unstable {
			notifyWebhook(webhookUrl, 'NOK')
		}
		failure {
			notifyWebhook(webhookUrl, 'NOK')
		}
	}
}

private void notifyWebhook(String webhookUrl, String message) {
	if (webhookUrl?.trim()) {
		sh "curl -X POST -d '${message}' ${webhookUrl}"
	}
}